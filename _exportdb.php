<?
//include('classes/core.php');

include ('connect.php');


function conv($str) {
	$str = str_replace(array(chr(13), chr(10)), '', $str);
	//return iconv("windows-1251", "utf-8", $str);
	return $str;
}



$doc = new DomDocument('1.0','utf-8');

$export = $doc->createElement("export");
$attr = $doc->createAttribute("date");
$attr->value = date("d.m.Y H:i");
$export->appendChild($attr);


$qresult = mysql_query("
	SELECT id, pod, title FROM cat
");
$cats = $doc->createElement("catset");
while($line = mysql_fetch_array($qresult)){
	$item = $doc->createElement('cat');
	
	$attr = $doc->createAttribute("id");
	$attr->value = $line["id"];
	$item->appendChild($attr);
	
	$attr = $doc->createAttribute("pod");
	$attr->value = $line["pod"];
	$item->appendChild($attr);
	
	$item->appendChild($doc->createTextNode(conv($line["title"])));
	
	$cats->appendChild($item);
}


$nomens = $doc->createElement("catalogset");


$qresult = mysql_query("
	SELECT id, title, price, art FROM catalog
");

while($line = mysql_fetch_array($qresult)){
	$item = $doc->createElement('catalog');
	
	$attr = $doc->createAttribute("catalog_id");
	$attr->value = $line["id"];
	$item->appendChild($attr);
	
	
	$price = $doc->createElement("price");
	$price->appendChild($doc->createTextNode($line["price"]));
	$item->appendChild($price);
	
	$art = $doc->createElement("art");
	$art->appendChild($doc->createTextNode(conv($line["art"])));
	$item->appendChild($art);
	
	$title = $doc->createElement("title");
	$title->appendChild($doc->createTextNode(conv($line["title"])));
	$item->appendChild($title);
	
	$nomens->appendChild($item);
}


$doc->appendChild($export);
$export->appendChild($cats);
$export->appendChild($nomens);

header ("Content-type: text/xml; charset='utf-8'");

//echo "<?xml version=\"1.0\">";

//echo "<?xml-stylesheet type=\"text/xml\" href=\"_export.php\">"; 

echo $doc->saveXml();

?>