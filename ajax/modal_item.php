<?php
include ('../connect.php');
include ('../func/core.php');
header('Content-Type: text/html; charset=utf-8');
$id = $_POST['item'];
// $id='8949';
//$itemQuery = mysql_query('SELECT * FROM `catalog` WHERE `id`='.$id);
//$item = mysql_fetch_array($itemQuery);
$item = getProduct($id);
?>
<!--  ###  МОДАЛЬНОЕ ОКНО  ###   -->
<script>
	$(function() {
		$(".image-small").aeImageResize({
			height : 100,
			width : 100
		});
		$(".image").aeImageResize({
			height : 195,
			width : 188
		});
	}); 
</script>
<a href="#" class="cat_modal-bg-overlay"></a>
<div class="cat_modal-body">
    <a href="#" title="Закрыть" class="cat_modal-close"></a>
    <!--  СОДЕРЖИМОЕ МОДАЛЬНОГО ОКНА  -->

    <div class="product-item-view">
        <?
        if ($item['sale'] == 1) { ?>
        <div class="product-item-mark sale"></div>
        <?	} ?>

        <div class="product-item-view">
            <? if ($item['sale'] == 1) { ?>
            <div class="product-item-mark sale"></div>
            <? } ?>

            <div class="product-item-img">
                <img src="<?= getImageWebPath('product_preview') . $item['id']; ?>.jpg" alt="" title="<?= $item['title'] ?>">
            </div>

            <div class="product-item-view-detailed">
                <div class="dynamic-cont">

                    <div class="dynamic-setof-items active">
                        <div class="image-wrap2">
                            <a data-original="<?= getImageWebPath('product_original') . $item['id']; ?>.jpg" href="<?= getImageWebPath('product_preview') . $item['id']; ?>.jpg"><img class="image-small" src="<?= getImageWebPath('product_small') . $item['id']; ?>.jpg" alt=""></a>
                        </div>
                        <?
                        $num = 0;
                        foreach ($item['extra_photo'] as $id)  {
                        $num++;
                        ?>
                        <div class="image-wrap2">		
                            <a data-original="<?= getImageWebPath('product_extra_original') . $id; ?>.jpg" href="<?= getImageWebPath('product_extra_medium') . $id; ?>.jpg"><img class="image-small" src="<?= getImageWebPath('product_extra_preview') . $id; ?>.jpg" alt=""></a>
                        </div>	
                        <? } ?>

                    </div>
                    <?if ($num > 2){?>
                    <div class="dynamic-cont-controls paginator-product photo-slide" style="background: none; position: static;">
                        <ul>
                            <li class="prev"><span><a href="/"></a></span></li>
                            <li class="next"><span><a href="/"></a></span></li>
                        </ul> 
                    </div>
                    <?} ?>
                </div>

            </div>
        </div>
    </div>


    <div class="product-item-info">
        <div class="product-title" style="text-align: left;">
            <h1><?= $item['title'] ?></h1>
        </div>

        <div class="product-item-price" style="margin-top: 5px;">
            <span><?= moneyFormat($item['price_after_discount']); ?><span>р.</span></span>
        </div>
        <div class="product-item-buy" style="background: none; width: 148px; float: left; margin-top: 0; clear: none;">
            <a class="catalog buy-it-product"  data-product_id="<?= $item['id'] ?>" data-product_price="<?= $item['price_after_discount']; ?>" href="<?= getTemplateLink(array('chpu' => 'korzina'), 'cart'); ?>">
                <div class="buy" id="buyb"></div>
            </a>

        </div>
        <?
		if (empty($item['short_des'])) {
			
			$des = substr($item['des'], 0, strpos($item['des'], '<h'));
			$length = strlen($des);
			$point = strpos($des, '.', 250);
			
			if ($point && ($point < 450))
				$des = substr($des, 0, $point + 1);
			elseif ($length >= 450) {
				$point = strpos($des, '.');
				if ($point)
					$des = substr($des, 0, $point + 1);
			}
			
		} else
			$des = $item['short_des'];
        ?>
        <div class="product-description" style="background: none; margin-top: 0;">
            <?= $des; ?>

            <p><a class="podrobnee" href="<?= getTemplateLink($item, 'catalog'); ?>" style="font-size: 16px;">Подробнее о товаре</a></p>

            <?
            if ( ($item['rests_main']['summ']) > 0 || ($item['fake_in_stock'] == 1) ) { ?>
            <div class="ico-item ico-stock instock" style="margin-top: 20px;">
                <div class="ico"></div>
                <span>Есть в наличии</span>
            </div>
            <?	} else { ?>
            <div class="ico-item ico-stock tailor-made" style="margin-top: 20px;">
                <div class="ico"></div>
                <span>На заказ</span>
            </div>	
            <?} ?>


            <?php

			function getFullUrl($s) {

				$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;

				$sp = strtolower($s['SERVER_PROTOCOL']);

				$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');

				$port = $s['SERVER_PORT'];
				$port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;

				$host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : $s['SERVER_NAME'];

				return $protocol . '://' . $host . $port . $s['REQUEST_URI'];
			}

			// Получаем текущий путь в адресной строке браузера с очисткой
			$GRM_ThisUrl = filter_var(getFullUrl($_SERVER), FILTER_SANITIZE_URL);
			// Получаем элементы текущего пути в адресной строке браузера
			$GRM_ThisUrlParts = parse_url($GRM_ThisUrl);

			// Получаем элементы текущего доменного имени
			$GRM_ThisDomainPartsTMP = explode('.', trim($GRM_ThisUrlParts['host'], '.'));
			// ... и выводим доменный префикс для домена третьего уровня
			$GRM_ThisSubDomain = (sizeof($GRM_ThisDomainPartsTMP) > 2) ? $GRM_ThisDomainPartsTMP[0] : FALSE;
            ?>
            
                
                <?php if($GRM_ThisSubDomain === FALSE or $GRM_ThisSubDomain === 'spb'): 
                    
                    $trial_city = ($GRM_ThisSubDomain === FALSE)?'Москве':'Санкт-Петербургу';
                    ?>
            
            <div class="product-item-delivery" style="margin-top: 10px; padding: 0; background: none;">
                <div class="ico-item ico-delivery free">
                    <div class="ico"></div>
                </div>
                <?
					if ($item['deliveryPrice'] !== false) {
						//$deliveryText = $item['deliveryPrice'] > 0 ? $item['deliveryPrice'] . ' руб.' : 'бесплатно';
						$deliveryText = $item['deliveryPrice'] > 0 ? $item['deliveryPrice'] . ' руб.' : 'бесплатно';
					} else {
						$deliveryText = 'от 300 руб';
					}
                ?>						

                <div class="item-delivery-info" style="margin-top: 0;">

                    <span>Доставка по <?=$trial_city; ?>:&nbsp;<span><?= $deliveryText; ?></span></span>
                    <span>Курьером:&nbsp;<span>завтра</span></span>
                </div> 
                
            </div>
                <?php endif; ?>

        </div>

        <div class="clearfix"></div>
    </div>

</div>
<script>
	$("#buyb").bind("click", function() {
		if ($(this).hasClass("buy")) {
			var link = $(this).closest("a");
			var price = link.data("product_price");
			var productId = link.data("product_id");

			addProductToBasket(productId, price);

			$(this).removeClass("buy").addClass("in-cart");

			return false;
		}
	});

</script>

