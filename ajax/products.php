<?php
require_once('../connect.php');
require_once('../func/core.php');
?>
<script type="text/javascript">

        function addToFavorites(productId) {
                //получаем данные из кук
                var products = parseProductsString( getCookie('fav_products') );
                products.push({
                        "id" : productId,
                        "amount" : 1
                });
                setCookie('fav_products', generateProductsString(products), '', '/');
                $.ajax({
                    url : "/ajax/products.php?action=add_to_fav",
                    type : "POST",
                    dataType : "json",
                    data : {"id" : productId},
                    beforeSend: function() {},
                    success : function (data, textStatus, jqXHR) {
                        console.log('success');
                    },
                    complete : function() {},
                    error : function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                    }
                });
                return false;
        } 

	function refreshBasketInfo() {
		//получим данные из кук
		var products = parseProductsString( getCookie('cart_products') );
		var totalAmount = parseInt(getCookie('total_amount'));
		if (isNaN(totalAmount)) {
			totalAmount = 0;
					$( "#small-cart-link" ).addClass( "inactive" );
		}else if(totalAmount == 0){
					$( "#small-cart-link" ).addClass( "inactive" );
				}else{
					$( "#small-cart-link" ).removeClass( "inactive" );
			}
		var productsCount = 0;
		for (var i in products) {
			productsCount += products[i].amount;
		}
		
		$('#basket-summ-products').text(productsCount);
		$('#basket-summ-products-float').text(productsCount);
		$('#basket-summ-price').text(moneyFormat(totalAmount) + ' руб.');
		$('#basket-summ-price-float').text(moneyFormat(totalAmount) + ' руб.');
		$('#basket-summ-products-text').text(declOfNum(productsCount, ['товар', 'товара', 'товаров']));
	}
	
	function setCookie (name, value, expires, path, domain, secure) {
		  document.cookie = name + "=" + escape(value) +
			((expires) ? "; expires=" + expires : "") +
			((path) ? "; path=" + path : "") +
			((domain) ? "; domain=" + domain : "") +
			((secure) ? "; secure" : "");
	}
	
	function getCookie(c_name) {
		if (document.cookie.length > 0 ) {
			c_start = document.cookie.indexOf(c_name + "=");
			if (c_start != -1) {
				c_start = c_start + c_name.length+1;
				c_end = document.cookie.indexOf(";", c_start);
				if (c_end == -1) c_end = document.cookie.length;
				return unescape(document.cookie.substring(c_start,c_end));
			}
		}
		return "";
	}

	function parseProductsString( string ) {
		var returnArray = new Array();
		
		if (string.length > 0 ) {
			var products = string.split('|');
			for (var i in products) {
				var temp = products[i].split(':');
				
				returnArray.push({
					'id' : parseInt(temp[0]),
					'amount' : parseInt(temp[1])
				});
			}
		}
		return returnArray;
	}
	
	function generateProductsString( products ) {
		var tempArray = new Array();
		for (var i in products) {
			tempArray.push(products[i].id + ':' +  products[i].amount);
		}
		return tempArray.join('|');
	}
	
	function addProductToBasket(productId, price) {
		//получим данные из кук
		var products = parseProductsString( getCookie('cart_products') );
		var totalAmount = parseInt(getCookie('total_amount'));
		if (!isNaN(totalAmount)) {
			totalAmount += price;
		} else {
			totalAmount = price;
		}
		
		//Если товар уже есть в куках, добавляем ему количество
		//Заодно подсчитаем количество всех товаров
		var foundSame = false;
		var productsCount = 0;
		for (var i in products) {
			if (products[i].id == productId) {
				products[i].amount++;
				foundSame = true;
			}
			productsCount += products[i].amount;
		}
		if (!foundSame) {
			products.push({
					"id" : productId,
					"amount" : 1
				});
			productsCount += 1;
		}
		
	 
		setCookie('cart_products', generateProductsString(products), '', '/');
		setCookie('total_amount', totalAmount, '', '/');
		//Изменить оформление
		refreshBasketInfo();
			
			
	}
	
	function catModalOpenLink(item) {

		$.ajax({
		url: "/ajax/modal_item.php",
		cache: false,
		type: "POST",
		data: ({item: item}),
		// dataType: "html",
		success: function(html) {
			$("#mdlcls").append(html).show();
		}
		});

		return false;

	}

	$('.cat_modal-open-link').on('click', function(){
		var item = $(this).closest('.catalogue-item-wrap').find('.catalogue-item-btn a').data('product_id');
		catModalOpenLink(item);
	});

	$(function(){
		$('.buy-it-listing').on('click', function() {
			console.log(1);
		})
	});
	
	$(function() {
			$( ".image" ).aeImageResize({ height: 195, width: 188 });
    });


	// Catalogue Product
	$('.catalogue-item, .catalogue-item a img').mouseenter(function(event) {
			title = $(this).parents('.catalogue-item').find('.catalogue-item-title');

			$(this).addClass('active');
			$(title).addClass('active');
			$(this).find('.description-overlay').hide();ы
		}).mouseleave(function(event) {
			title = $(this).parents('.catalogue-item').find('.catalogue-item-title');

			$(this).removeClass('active');
			$(title).removeClass('active');
			$(this).find('.description-overlay').show();
		});
	
	//Лисенер кнопки "избранное"
	$('.favorites-heart').bind('click', function(){
		var big_heart = $('.favorites-heart.big');
		var modal = $(this).find('div.heart-popup-left');
		var link = $(this).find('a');
		var id = link.data('product_id');
		
		if ( $(this).hasClass('empty') && !$(this).hasClass('big')) {
			modal.html('Уже&nbsp;в&nbsp;избранном');
			$(this).removeClass('empty');
			big_heart.removeClass('empty');
			addToFavorites(id);
		}	
		else {
			href = link.attr('href');
			location.href = href;
		} 
		return false;
	});		
</script>
<?php

switch ($_GET['action']) {
        case 'add_to_fav' :
            if (isset($_POST['id']) && is_numeric($_POST['id'])) {
                $id = $_POST['id'] * 1;
                mysql_query("UPDATE `catalog` "
                        . "SET `favorites_rating` = `favorites_rating` + 1 "
                        . "WHERE `id` = {$id}");
                echo json_encode(array('error' => false));
            }
            else
                echo json_encode(array('error' => true));
        break;
        case 'mcat' :
		$filter = array();
		
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if ($_REQUEST['filter']['cat']) {
			foreach($_REQUEST['filter']['cat'] as $key => $value) {
				if (is_numeric($key)) $filter['cat'][] = $key;
			}
		}
		if ($_REQUEST['filter']) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if ($_REQUEST['filter_range']) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}
		if (!isset($filter['cat']) || count($filter['cat']) <= 0) {
			$tempCats = getCatsInMenuItem($_REQUEST['menu_item']);
			foreach ($tempCats as $cat) {
					$filter['cat'][] = $cat['cat_id'];
				}
		}
		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		
		//$temp = getProducts(false, $filter, $sort, $page);
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
		}
		
		if (count($temp['products']) > 0) {
			foreach ($temp['products'] as $product) {
				include('../tpl/element_product_list.tpl.php');
			}
			switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			}
		} else {
			echo '<div style="margin-left: 20px;">Нет товаров, удовлетворяющих выбранным условиям.</div>';
		}
		break;
	case 'cat' :
		//Фильтр для выбора товаров
		$filter = array();
		$_REQUEST['min_price'] = (int)$_REQUEST['min_price'];
		$_REQUEST['max_price'] = (int)$_REQUEST['max_price'];
		$_POST['novinki'] = (int)$_POST['novinki'];
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if ($_REQUEST['filter']) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if ($_REQUEST['filter_range']) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}

		if (isset($_REQUEST['tag'])) {
			foreach($_REQUEST['tag'] as $key => $value) {
				if (is_numeric($key)) $filter['tag'][] = $key;
			}
		}
		$catInfo = getCatInfo($_REQUEST['cat']);
		if ($catInfo['id']) {
			$filter['cat'] = $catInfo['id'];
		}

		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
			//file_put_contents('extra.txt', $filter['params']);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
			//file_put_contents('normal.txt', $sort);
		}
		if (count($temp['products']) > 0) {
			foreach ($temp['products'] as $product) {
				include('../tpl/element_product_list.tpl.php');
			}
			switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			}
		} else {
			echo '<div style="margin-left: 20px;">Нет товаров, удовлетворяющих выбранным условиям.</div>';
		}
		break;
		case 'tag' :

		$filter = array();
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if ($_REQUEST['filter']) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if ($_REQUEST['filter_range']) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}
		
		$filter['tag'] = is_numeric($_REQUEST['tag']) ? $_REQUEST['tag'] : 0;

		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
		}
		if (count($temp['products']) > 0) {
			foreach ($temp['products'] as $product) {
				include('../tpl/element_product_list.tpl.php');
			}
			switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			}
		} else {
			echo '<div style="margin-left: 20px;">Нет товаров, удовлетворяющих выбранным условиям.</div>';
		}
		break;
}