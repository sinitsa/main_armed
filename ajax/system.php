<?php

include('../connect.php');
include('../func/core.php');


switch ($_REQUEST['method']) {
	case 'sendmail' :
		session_start();
		$error = 0;

		if ($_REQUEST['captcha'] == $_SESSION['sendMailCaptcha']) {
//			$name = iconv("utf-8", "windows-1251", $_REQUEST['name']);
//			$email = iconv("utf-8", "windows-1251", $_REQUEST['email']);
//			$messageBody = iconv("utf-8", "windows-1251", $_REQUEST['message_body']);
			$name = $_REQUEST['name'];
			$email = $_REQUEST['email'];
			$messageBody = $_REQUEST['message_body'];

			do {
				if (!validateEmail($email)) {
					$error = 1;
					break;
				}
				if (isLinksContain($messageBody) || isLinksContain($name)) {
					$error = 2;
					break;
				}

				$name = htmlspecialchars($name);
				$messageBody = htmlspecialchars($messageBody);
				$message = "
					<html>
						<div>Имя: {$name}</div>
						<div>E-mail: {$email}</div>
						<div style=\"margin-top: 10px;\">{$messageBody}</div>
					</html>
				";

				sendMail( array(
						'emailFrom' => $email,
						'emailTo' =>  Config::get('site.email_manager'),
						'subject' => 'Сообщение через форму с сайта "' . Config::get('site.name') . '"',
						'body' => $message
					)
				);

				$_SESSION['sendMailCaptcha'] = rand(9999,999999);
			} while (false);
		} else {
			$error = 3;
		}
		echo json_encode(array(
				'captcha' => $_SESSION['sendMailCaptcha'],
				'error' => $error
				)
			);
	break;
	
	case 'addfeedback' : 
//		$name = trim(iconv("utf-8", "windows-1251", $_REQUEST['name']));
//		$email = trim(iconv("utf-8", "windows-1251", $_REQUEST['email']));
//		$comment = trim(iconv("utf-8", "windows-1251", $_REQUEST['comment']));
		$name = trim($_REQUEST['name']);
		$email = trim($_REQUEST['email']);
		$comment = trim($_REQUEST['comment']);
		$productId = $_REQUEST['product_id'];
		
		if (!is_numeric($productId) || $productId <= 0)
			break;
		
		if (empty($name) || empty($email) || empty($comment)) {
			echo json_encode(array( 'error' => true, 'error_code' => 1));
			break;
		}
		
		if (!validateEmail($email)) {
			echo json_encode(array( 'error' => true, 'error_code' => 2));
			break;
		}
		if (isLinksContain($name) || isLinksContain($comment) || isLinksContain($email)) {
			echo json_encode(array( 'error' => true, 'error_code' => 3));
			break;
		}
		
		addProductFeedback($productId, array(
			'name' => $name,
			'email' => $email,
			'comment' => $comment
		));
		
		echo json_encode(array( 'error' => false));
	break;
	
	case 'getshopfeedback' :
		$currentPage = (isset($_REQUEST['page']) && is_numeric($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
		//Отзывов на странцу
		$onPage = Config::get('feedback.onPage');
		if ($_REQUEST['page'] == 'all') {
			$limitString = '';
		} else {
			//Всего отзывов
			$feedsCount = fetchOne("SELECT COUNT(*) FROM `shop_otzyv` WHERE `confirm` = '1'");
			//Всего страниц
			$allPages = ceil($feedsCount / $onPage);
			
			//Если номер текущей страницы больше, чем страниц всего, то текущая страница 1
			if ($currentPage > $allPages) {
				$currentPage = 1;
			}
			//Высчитываем offset записей
			$limit = ($currentPage - 1) * $onPage;
			//Подготавливаем строку для запроса
			$limitString = 'LIMIT '.$limit.','.$onPage;
			
			//Подготавливаем данные для скармливания pageGenerator'у
			$paginData = array(
				'pagesCount' => $allPages,
				'currentPage' => $currentPage
			);
		}
	
		$sel = mysql_query("SELECT * FROM `shop_otzyv` WHERE `confirm` = '1' ORDER BY `id` DESC {$limitString}");
		while ($res = mysql_fetch_assoc($sel)) {
			$messages[] = array(
				'id' => $res['id'],
//				'name' => trim(iconv("windows-1251", "utf-8", $res['name'])),
//				'des' => trim(iconv("windows-1251", "utf-8", $res['des'])),
//				'otvet' => trim(iconv("windows-1251", "utf-8", $res['otvet'])),
				'name' => trim($res['name']),
				'des' => trim($res['des']),
				'otvet' => trim($res['otvet']),
				'date' => $res['date']
			);
		}
		echo json_encode(array(
			'messages' => $messages,
			'pagin_data' => $paginData
		));
		
	break;
	
	case 'addshopfeedback' : 
		$fb_validation = addFeedBack();
		
		if ($fb_validation != null)
			$error = false;
		else
			$error  = true;
			
		echo json_encode(array(
			'error' => $error
		));
	break;
}