<?php 

function multiCurl($data) 
		{
        $time1=time();
        $curls = array();
        $result = array();
        $mh = curl_multi_init();
        $uagent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208";
        $header[] = "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Accept-Language: ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3";
        $header[] = "Accept_charset: utf-8;q=0.7,*;q=0.7";
        $header[] = "Keep_alive: 300";
        $header[] = "Connection: keep-alive";
			$numes = 0;
			$sourceIp = 0;
				foreach ($data as $IdRequest => $DataRequest) 
				{
					$curls[$IdRequest] = curl_init(); 
				if($numes == 10)	
				{
					$numes = 0;
					$sourceIp++;
				}
				$numes++;
                curl_setopt($curls[$IdRequest], CURLOPT_URL, $DataRequest);
                curl_setopt($curls[$IdRequest], CURLOPT_RETURNTRANSFER, 1);		
                curl_setopt($curls[$IdRequest], CURLOPT_HEADER, 0);
				curl_setopt($curls[$IdRequest], CURLOPT_HTTPHEADER , $header);
				curl_setopt($curls[$IdRequest], CURLOPT_TIMEOUT, 0);
				curl_setopt($curls[$IdRequest], CURLOPT_FAILONERROR, 1);
				curl_setopt($curls[$IdRequest], CURLOPT_AUTOREFERER, 1);
				curl_setopt($curls[$IdRequest], CURLOPT_VERBOSE, 1);
                curl_setopt($curls[$IdRequest], CURLOPT_ENCODING, "");        
                curl_setopt($curls[$IdRequest], CURLOPT_USERAGENT, $uagent); 
                curl_setopt($curls[$IdRequest], CURLOPT_FOLLOWLOCATION, 1);   
                curl_multi_add_handle($mh, $curls[$IdRequest]);
				}

				$running = null;
				$time2=time();
				do
				{
						curl_multi_exec($mh, $running);
						$info = curl_multi_info_read($mh);

						usleep(50);
				} while($running > 0);
				foreach($curls as $id => $c)
				{
						$result[$id] = json_decode(curl_multi_getcontent($c),true);
						if(curl_errno($c))
						{
							echo 'error: ' . curl_error($c);
						}
						curl_multi_remove_handle($mh, $c);
				}
				$time3=time()-$time2;
				curl_multi_close($mh);
				ksort($result);
				return $result;
		}


	
	$json = file_get_contents('php://input');
	//echo $json;
	
	$obj = json_decode($json,true);
	//var_dump($obj);
	$result = multiCurl($obj);
	echo json_encode($result);


?>