<?php

// Подключаем файлы конфигурации компонента
// ... константы
if (file_exists(ROOT_PATH . 'components/geolocation/settings/constants.cfg.php')) {
    require ( ROOT_PATH . 'components/geolocation/settings/constants.cfg.php' );
}

// ... список поисковых роботов
if (file_exists(GRM_CFG_PATH . 'robots.cfg.php')) {
    require ( GRM_CFG_PATH . 'robots.cfg.php' );
}

// Подключаем хэлперы
require( GRM_HELP_PATH . 'general.helper.php');

// Если зашел не робот - выполняем требования к УРЛу
if (!isRobot($grm_config['robots'])) {

    //  Подключаем базовую конфигурацию
    if (file_exists(GRM_CFG_PATH . 'general.cfg.php')) {
        require ( GRM_CFG_PATH . 'general.cfg.php' );
    }

    // ... конфигурацию мультидомена
    if (file_exists(GRM_CFG_PATH . 'domains.cfg.php')) {
        require ( GRM_CFG_PATH . 'domains.cfg.php' );
    }

    // ... конфигурацию географических баз
    foreach ($grm_config['general_data']['com_method'] as $grm_method_key => $grm_method_value) {
        if (file_exists(GRM_CFG_PATH . $grm_method_value . '.cfg.php')) {
            require ( GRM_CFG_PATH . $grm_method_value . '.cfg.php' );
        }
    }

    // ... список защищенных адресов
    if (file_exists(GRM_CFG_PATH . 'securelinks.cfg.php')) {
        require ( GRM_CFG_PATH . 'securelinks.cfg.php' );
    }

    // собираем конфиг воедино
    $GRM_CFG = $grm_config;

    // Подключаем классы для работы с географическими базами
    foreach ($GRM_CFG['general_data']['com_method'] as $grm_method_key => $grm_method_value) {
        require_once( GRM_CORE_PATH . 'GRM_' . $grm_method_value . '.class.php' );
    }

    // Инициализация класса IPGeoBase
    $GRM_Obj = new GRM_IPGeoBase();


    # Устанавливаем (получаем) значение IP адреса
    # $GRM_IP = getClientIp();
    # -------------------------------------------

    $GRM_IP = getClientIp();

    # -------------------------------------------
    // Ukraine
    // $GRM_IP = '5.255.160.10'; 
    # -------------------------------------------
    // GB
    // $GRM_IP = '2.24.0.10';
    # -------------------------------------------
    // RU Chelyabinsk
    // $GRM_IP = '46.147.192.10';
    # -------------------------------------------
    // RU Balashikha
    // $GRM_IP = '130.255.8.10';
    # -------------------------------------------
    // RU Tver
    // $GRM_IP = '109.197.136.10';
    # -------------------------------------------
    // RU Tver - Bologoe
    // $GRM_IP = '194.28.160.10';
    // $GRM_IP = '109.197.136.10';
    # -------------------------------------------
    // RU Belgorod
    // $GRM_IP = '37.208.65.10';
    # -------------------------------------------
    // Получаем данные геолокации по IP
    $GRM_RealData = $GRM_Obj->getRecord($GRM_IP); //getClientIp());
    // Переводим данные в utf
    $GRM_RealDataResult = array_map("winToUtf", $GRM_RealData);

    // Получаем текущий путь в адресной строке браузера с очисткой
    $GRM_ThisUrl = filter_var(getFullUrl($_SERVER), FILTER_SANITIZE_URL);
    // Получаем элементы текущего пути в адресной строке браузера
    $GRM_ThisUrlParts = parse_url($GRM_ThisUrl);

    // Получаем элементы текущего доменного имени
    $GRM_ThisDomainPartsTMP = explode('.', trim($GRM_ThisUrlParts['host'], '.'));
    // ... и выводим доменный префикс для домена третьего уровня
    $GRM_ThisSubDomain = (sizeof($GRM_ThisDomainPartsTMP) > 2) ? $GRM_ThisDomainPartsTMP[0] : FALSE;

    // Инициализируем массив с актуальной информацией о пользователе (user agent + IPGeo)
    $GRM_UserRealData = array(
        'user_ip_address' => $GRM_IP,
        'user_country' => (isset($GRM_RealDataResult['cc'])) ? $GRM_RealDataResult['cc'] : FALSE,
        'user_district' => (isset($GRM_RealDataResult['district'])) ? $GRM_RealDataResult['district'] : FALSE,
        'user_region' => (isset($GRM_RealDataResult['region'])) ? $GRM_RealDataResult['region'] : FALSE,
        'user_city' => (isset($GRM_RealDataResult['city'])) ? $GRM_RealDataResult['city'] : FALSE,
        'user_scheme' => $GRM_ThisUrlParts['scheme'],
        'user_host' => $GRM_ThisUrlParts['host'],
        'user_subhost' => $GRM_ThisSubDomain,
        'user_path' => $GRM_ThisUrlParts['path'],
        'user_query' => (isset($GRM_ThisUrlParts['query'])) ? $GRM_ThisUrlParts['query'] : FALSE
    );

    // тест
//    echo 'Информация о посетителе.<br /><pre>';
//    var_dump($GRM_UserRealData);
//    echo '</pre>';
//    
//    
     
    if(!$GRM_ThisSubDomain){
    // Вычисляем необходимость перманентного редиректа на защищенный порт SSL или обратно
    if (in_array($GRM_UserRealData['user_path'], $GRM_CFG['secure_links'])) {
        // УРЛ в списке защищенных
        if ($GRM_UserRealData['user_scheme'] !== 'https') {
            // но не защищен
            header('Location: https://' . $GRM_UserRealData['user_host'] . $GRM_UserRealData['user_path'] . $GRM_UserRealData['user_query'], 'refresh', 301);
            exit(0);
        }
    } else {
        // УРЛ не в списке защищенных
        if ($GRM_UserRealData['user_scheme'] !== 'http') {
            // но защищен
            header('Location: http://' . $GRM_UserRealData['user_host'] . $GRM_UserRealData['user_path'] . $GRM_UserRealData['user_query'], 'refresh', 301);
            exit(0);
        }
    }
    }

    // Проверяем страну
    $GRM_SetCountry = 'RU';
    if ($GRM_UserRealData['user_country'] !== 'RU') {
        // Если не Россия - то РОССИЯ! Установка базового домена.
        $GRM_SetSubDomain = 'msk';
    } else {

        // Все-таки Россия =)
        $GRM_SetDistrict = array();
        $GRM_SetRegion = array();
        $GRM_SetSubDomain = array();

        // Проверяем административный округ
        foreach ($GRM_CFG['domains_data']['regions'] as $GRM_DD_Abbr => $GRM_DD_Data) {
            if (in_array($GRM_UserRealData['user_district'], $GRM_DD_Data['user_district'])) {
                $GRM_SetDistrict[] = $GRM_UserRealData['user_district'];
                $GRM_SetSubDomain[] = $GRM_DD_Abbr;
            }
        }

        // Считаем совпадения по поддоменам
        if (sizeof($GRM_SetDistrict) === 0) {
            $GRM_SetSubDomain = 'msk';
        } else {

            $GRM_SetSubDomain = $GRM_SetSubDomain[0];

            // Унифицируем значение округа
            $GRM_SetDistrict = $GRM_SetDistrict[0];

            // Проверяем регион
            foreach ($GRM_CFG['domains_data']['regions'] as $GRM_DD_Abbr => $GRM_DD_Data) {
                if (in_array($GRM_UserRealData['user_region'], $GRM_DD_Data['user_region'])) {
                    $GRM_SetRegion[] = $GRM_UserRealData['user_region'];
                    $GRM_SetSubDomain2[] = $GRM_DD_Abbr;
                }
            }

            // Считаем совпадения по поддоменам
            if (sizeof($GRM_SetRegion) !== 0) {
                $GRM_SetSubDomain = $GRM_SetSubDomain2[0];
            }
        }
    }

    // Собираем массив с рекоммендуемыми данными для пользователя
    $GRM_UserRecommendData = array(
        'user_country' => $GRM_SetCountry,
        'user_subhost' => $GRM_SetSubDomain
    );

    // Собираем рекоммендуемый адрес
    // Заменяем поддомен (если нужен)
    // Текущая страница не на поддомене
    if (!$GRM_ThisSubDomain) {

        // ссылаемся на корневой домен
        if ($GRM_SetSubDomain === $GRM_CFG['domains_data']['base_domain']) {
            // TODO: Ничего не делаем
        }
        // ссылаемся на поддомен
        else {
            array_unshift($GRM_ThisDomainPartsTMP, $GRM_SetSubDomain);
        }
    }
    // Текущая страница на поддомене
    else {

        // ссылаемся на корневой домен
        if ($GRM_SetSubDomain === $GRM_CFG['domains_data']['base_domain']) {
            array_shift($GRM_ThisDomainPartsTMP);
        }
        // ссылаемся на поддомен
        else {
            $GRM_ThisDomainPartsTMP[0] = $GRM_SetSubDomain;
        }
    }

    // тест
//    echo 'Рекоммендуемые данные для посетителя.<br /><pre>';
//    var_dump($GRM_UserRecommendData);
//    echo '</pre>';
    // Собираем строку доменного имени
    $GRM_ThisDomainNeed = implode('.', $GRM_ThisDomainPartsTMP);
    // Собираем Query String
    $GRM_ThisQueryStringNeed = FALSE;
    if ($GRM_UserRealData['user_query']) {
        $GRM_ThisQueryStringNeed = '?' . $GRM_UserRealData['user_query'];
    }

    $GRM_NeedNewUrl = $GRM_UserRealData['user_scheme'] . '://' . $GRM_ThisDomainNeed . $GRM_UserRealData['user_path'] . $GRM_ThisQueryStringNeed;

//    // тест
//    echo 'Рекоммендуемый url.<br /><pre>';
//    print_r($GRM_NeedNewUrl);
//    echo '</pre>';

    if ($GRM_ThisDomainNeed !== $GRM_UserRealData['user_host']) {
        define('CHANGE_REGION', TRUE);
        define('CHANGE_REGION_URL', $GRM_NeedNewUrl);
    }

    // тест
//    echo 'Редирект?<br /><pre>';
//
//    if (defined('CHANGE_REGION')) {
//	print_r(CHANGE_REGION . '<br />');
//    }
//    if (defined('CHANGE_REGION_URL')) {
//	print_r(CHANGE_REGION_URL);
//    }
//    echo '</pre>';



    //require ( ROOT_PATH . 'components/geolocation/GRM_GL_RU.php' );
}
