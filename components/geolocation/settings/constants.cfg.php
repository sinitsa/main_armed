<?php

// Устанавливаем предел "работоспособности" скриптов
if (!defined('IN_UFO')) {
    define('IN_UFO', TRUE);
}

// Устанавливаем константу директории компонента GeoRegions
if (!defined('GRM_PATH')) {
    define('GRM_PATH', ROOT_PATH . 'components/geolocation/');
}

// Устанавливаем константу директории файлов конфигурации для компонента GeoRegions
if (!defined('GRM_CFG_PATH')) {
    define('GRM_CFG_PATH', GRM_PATH . 'settings/');
}

// Устанавливаем константу директории классов для компонента GeoRegions
if (!defined('GRM_CORE_PATH')) {
    define('GRM_CORE_PATH', GRM_PATH . 'core/');
}

// Устанавливаем константу директории хэлперов для компонента GeoRegions
if (!defined('GRM_HELP_PATH')) {
    define('GRM_HELP_PATH', GRM_PATH . 'helpers/');
}

// Устанавливаем константу директории данных geoIP для компонента GeoRegions
if (!defined('GRM_DATA_PATH')) {
    define('GRM_DATA_PATH', GRM_PATH . 'data/');
}