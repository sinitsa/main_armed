<?php

defined('IN_UFO') OR exit('No direct script access allowed');

$grm_config['domains_data'] = array(
    'base_domain' => 'msk',
    'regions' => array(
	'msk' => array(
	    'user_city' => array(
		'Москва',
		'Электросталь',
		'Тверь'
	    ),
	    'user_region' => array(
		'Москва',
		'Московская область',
		'Тверская область'
	    ),
	    'user_district' => array(
		'Центральный федеральный округ'
	    )
	),
	'rzn' => array(
	    'user_city' => array(
		'Рязань'
	    ),
	    'user_region' => array(
		'Рязанская область',
		'Белгородская область'
	    ),
	    'user_district' => array(
		'Центральный федеральный округ',
		'Приволжский федеральный округ',
		'Южный федеральный округ',
		'Северо-Кавказский федеральный округ'
	    )
	),
	'spb' => array(
	    'user_city' => array(
		'Санкт-Петербург'
	    ),
	    'user_region' => array(
		'Санкт-Петербург',
		'Ленинградская область'
	    ),
	    'user_district' => array(
		'Северо-Западный федеральный округ',
		'Уральский федеральный округ',
		'Сибирский федеральный округ',
		'Дальневосточный федеральный округ'
	    )
	)
    )
);
