<?php

defined('IN_UFO') OR exit('No direct script access allowed');

$grm_config['ipgeobase_data'] = array(
    'type_conn' => 'local',
    'type_db' => 'file',
    'data_db' => array(
	'cidr_optim.txt',
	'cities.txt'
    )
);
