<?php

defined('IN_UFO') OR exit('No direct script access allowed');

function winToUtf($n) {
    return iconv("CP1251", "UTF-8", $n);
}

function getClientRealIp() {

    $ipaddress = NULL;

    if (getenv('HTTP_CLIENT_IP')) {
        $ipaddress = getenv('HTTP_CLIENT_IP');
    } else if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    } else if (getenv('HTTP_X_FORWARDED')) {
        $ipaddress = getenv('HTTP_X_FORWARDED');
    } else if (getenv('HTTP_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    } else if (getenv('HTTP_FORWARDED')) {
        $ipaddress = getenv('HTTP_FORWARDED');
    } else if (getenv('REMOTE_ADDR')) {
        $ipaddress = getenv('REMOTE_ADDR');
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}

function isRobot($spiders = FALSE) {

    if ($spiders) {

        foreach ($spiders as $spider) {

            if (stripos($_SERVER['HTTP_USER_AGENT'], $spider) !== false) {
                return true;
            }
        }
    }

    return false;
}

function getFullUrl($s) {

    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;

    $sp = strtolower($s['SERVER_PROTOCOL']);

    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');

    $port = $s['SERVER_PORT'];

    $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;

    $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : $s['SERVER_NAME'];

    return $protocol . '://' . $host . $port . $s['REQUEST_URI'];
}
