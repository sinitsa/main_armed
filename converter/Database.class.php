<?php
class Database extends mysqli {
	private $connected = false;
	private $settings, $prefix;
	protected $debug = false;

	public function __construct($settings = null) {
		if ($settings != null) $this->setSettings($settings);
	}
	public function setSettings($settings) {
		$this->settings = $settings;
		$this->prefix = $settings['prefix'];
	}
	public function makeConnection() {
		@parent::connect(
			$this->settings['host'],
			$this->settings['user'],
			$this->settings['password'],
			$this->settings['base'],
			isset($this->settings['port']) ? $this->settings['port'] : 3306
			);
			if (mysqli_connect_error()) {
				$this->connected = false;
				die('Connect Error ('.mysqli_connect_errno().') '.mysqli_connect_error());
			} else {
				$this->connected = true;
			}
			if (isset($this->settings['charset'])) $this->set_charset($this->settings['charset']);
	}
	public function query($sql) {
		if (!$this->connected) $this->makeConnection();
		if ($this->debug && $this->error) echo $this->error;
		return parent::query($sql);
	}
	public function escapeString($str, $htmlspec = true) {
		if (!$this->connected) $this->makeConnection();
		$escaped = (get_magic_quotes_gpc() ? stripslashes($str) : $str);
		if ($htmlspec) $escaped = htmlspecialchars($escaped, ENT_QUOTES); 
		return $this->real_escape_string($escaped);
	}
	public function escape($str, $htmlspec = true) {
		return $this->escapeString($str, $htmlspec);
	}
	public function fetch($obj) {
		if (is_string($obj)) $obj = $this->query($obj);
		return $obj->fetch_assoc();
	}
	public function fetchArray($obj) {
		if (is_string($obj)) $obj = $this->query($obj);
		return $obj->fetch_array();
	}
	public function fetchAll($obj) {
		if (is_string($obj)) $obj = $this->query($obj);
		if (is_object($obj)) {
			$array = array();
			while ($row = $obj->fetch_assoc()) {
				$array[] = $row;
			}
			return $array;
		}
		return false;
	}
	public function fetchOne($obj) {
		$data = $this->fetchArray($obj);
		return $data[0];
	}

	public function insert($tableName, $data, $escape = true) {
		$elements = array();
		foreach ($data as $key => $value) {
			if ($value === null) {
				$elements[] = "`{$key}` = NULL";
			} else {
				if (!$this->connected) $this->makeConnection();
				$elements[] = "`{$key}` = '" . ($escape ? $this->real_escape_string($value) : $value) . "'";
			}
		}
		$setString = implode(',', $elements);

		$this->query("
			INSERT INTO `{$tableName}`
			SET
				{$setString}
		");
		return $this->affected_rows;
	}
	
	public function setPrefix($prefix) {
		$this->prefix = $prefix;
	}
	public function isConnected() {
		return $this->connected;
	}
	public function close() {
		return $this->connected ? parent::close() : false;
	}

	public function debug($bool) {
		$this->debug = $bool;
	}
}