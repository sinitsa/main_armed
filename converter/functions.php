<?php

function iLog($message) {
	echo $message . PHP_EOL;
}

function parseKey($key) {
	if (empty($key)) return false;
	$temp = explode('.', $key);
	$count = count($temp);
	if ($count == 2) {
		$keyGroup = $temp[0];
		$key = $temp[1];
	} elseif ($count == 1) {
		$keyGroup = '';
		$key = $temp;
	} else {
		return false;
	}
	return array('keyGroup' => $keyGroup, 'key' => $key);
}