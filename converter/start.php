<?php
error_reporting(E_ALL ^ E_NOTICE);
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');

header("Content-Type: text/plain; charset=utf-8');"); 

require('config.php');
require('functions.php');
require('Database.class.php');
require('simple_html_dom.php');


$dbOld = new Database($config['dbOld']);
$dbNew = new Database($config['dbNew']);


//Переносим категории

$dbNew->query("TRUNCATE `cat`");

$sel = $dbOld->query("SELECT * FROM `cat` ORDER BY `id` ASC");
while ($row = $dbOld->fetch($sel)) {
	//2 попытки чтобы вставить категорию, если вдруг совпало чпу
	for ($i = 0; $i < 2; $i++) {
		if ($dbNew->insert('cat', $row) > 0) {
			break;
		} else {
			iLog('Cat: совпало chpu ' . $row['chpu'] . ' . Добавляем двойку');
			$row['chpu'] .= '2';
		}
	}
}

//Переносим товары

$dbNew->query("TRUNCATE `catalog`");

$sel = $dbOld->query("SELECT * FROM `catalog` ORDER BY `id` ASC");
while ($row = $dbOld->fetch($sel)) {

	$row['short_des'] = $row['short'];
	unset($row['short']);

	$row['price_after_discount'] = $row['price'];

	$row['best'] = $row['hit'];
	unset($row['hit']);

	$row['sale'] = $row['prior'];
	unset($row['prior']);

	unset($row['instruction']);

	//Несколько попыток не используется, потому что в старой базе стоит уникальный ключ на чпу
	if ($dbNew->insert('catalog', $row) <= 0) {
		iLog('Calalog: Error : ' . $dbNew->error . ' id = ' . $row['id']);
	}

}

//Переносим заказы

$dbNew->query("TRUNCATE `orders`");
//$dbNew->query("TRUNCATE `orders_log`");

$sel = $dbOld->query("SELECT * FROM `pay_history` ORDER BY `id` ASC");
while ($row = $dbOld->fetch($sel)) {
	$newRow = array();

	$newRow['id'] = $row['id'];

	preg_match("/([0-9]{2}).([0-9]{2}).([0-9]{4})(?:.([0-9]{2}).([0-9]{2}))?/", $row['date'], $date);
	$newRow['date'] = mktime(!empty($date[4]) ? $date[4] : 0 , !empty($date[5]) ? $date[5] : 0, 0, $date[2], $date[1], $date[3]);

	/* Товары 
	$products = 
		Array
		(
		    [0] => Array
		        (
		            [id] => 969
		            [amount] => 1
		            [price] => 650
		            [price_after_discount] => 650
		        )

		    [1] => Array
		        (
		            [id] => 404
		            [amount] => 1
		            [price] => 10500
		            [price_after_discount] => 10500
		        )

		    [2] => Array
		        (
		            [id] => 579
		            [amount] => 1
		            [price] => 800
		            [price_after_discount] => 800
		        )

		)

	$order_price = 
		Array
		(
		    [discount_type] => percent
		    [discount_value] => 0
		    [price_after_global_discount] => 700
		    [price_before_global_discount] => 700
		    [delivery_price] => 300
		)
	*/
	
	//Парсим поле tovar, вычленяем товары, количество, сумму заказа, доставку
	$body = trim($row['tovar']);

	$products = array();
	$order_price = array(
			'discount_type' => 'percent',
		    'discount_value' => 0,
		    'price_after_global_discount' => 0,
		    'price_before_global_discount' => 0,
		    'delivery_price' => false
		);

	if (strpos($body, 'Общая сумма') !== false ) {
		//Определен первый тип записи заказа
		$html = new simple_html_dom();
		$html->load('<html>' . $body . '</html>');
		foreach ( $html->find('table') as $t ) {
			$src = $t->find('td img', 0)->src;
			preg_match("/([0-9]+)\.jpg/si", $src, $f);
			$id = $f[1];

			$title = trim(strip_tags( $t->find('td', 1)->innertext) );
			$title = preg_replace("/\s+/", " ", $title);

			preg_match("/\(([0-9]{1,2}) шт\.\).?([,0-9]+).руб\./", $title, $tmp);
			$amount = $tmp[1];
			$price = $tmp[2];

			$products[] = array(
				'id' => $id,
				'amount' => $amount,
				'price' => $price,
				'price_after_discount' => $price
			);

		}
		preg_match("/Общая сумма.*?([,0-9]+).*?/si", strip_tags($body), $tmp);
		$priceSum = $tmp[1];

		preg_match("/Скидка ([0-9]+)%/si", strip_tags($body), $tmp);
		$discount = empty($tmp[1]) ? 0 : $tmp[1];

		preg_match("/Доставка.*?([,0-9]+|Бесплатно).*?/si", strip_tags($body), $tmp);

		if (!empty($tmp[1])) {
			if ($tmp[1] == 'Бесплатно' || $tmp[1] == 'бесплатно') {
				$delivery = 0;
			} elseif (is_numeric($tmp[1])) {
				$delivery = $tmp[1];
			} else {
				$delivery = false;
			}
		} else {
			$delivery = false;
		}



		$order_price = array(
			'discount_type' => 'percent',
		    'discount_value' => $discount,
		    'price_after_global_discount' => $priceSum,
		    'price_before_global_discount' => $priceSum,
		    'delivery_price' => $delivery
		);

	} elseif (strpos($body, 'Обратный звонок')) {
		//Обратный звонок
		$newRow['extra_information'] = 'Обратный звонок';
		
	} elseif (preg_match("/<a href.*?\/catalog\/(.*?)\.html.*?>.*?<\/a>(.*?)<br>/si", $body)) {
		//Еще один тип
		preg_match_all("/<a href.*?\/catalog\/(.*?)\.html.*?>.*?<\/a>(.*?)<br>/si", $body, $tmp);

		$priceSum = 0;

		for ($i = 0; $i < count($tmp[0]); $i++) {
			$chpu = $dbNew->real_escape_string($tmp[1][$i]);
			$id = $dbNew->fetchOne("SELECT `id` FROM `catalog` WHERE `chpu` = '{$chpu}' LIMIT 1");

			preg_match("/([0-9]+)шт.*?([0-9]+) рублей/si", $tmp[2][$i], $tmp2);

			$amount = $tmp2[1];
			$price = $tmp2[2] / $amount;
			$priceSum += $tmp2[2];

			$products[] = array(
					'id' => $id,
					'amount' => $amount,
					'price' => $price,
					'price_after_discount' => $price
				);
		}

		$order_price = array(
			'discount_type' => 'percent',
			'discount_value' => 0,
			'price_after_global_discount' => $priceSum,
			'price_before_global_discount' => $priceSum,
			'delivery_price' => false
		);
	} else {
		//Не удалось определить тип сообщения. Просто оставим данные из tovar в коментах
		$newRow['extra_information'] = $body;
	}

	$newRow['products'] = serialize($products);
	$newRow['order_price'] = serialize($order_price);
	$newRow['name'] = $row['name'];

	//Определяем тип доставки
	/*
	1 - Курьером по Москве
	2 - Курьером по Московской области
	3 - Почтой (доставка по всей России)
	4 - Траспортной компанией в регионы (для крупногабаритных грузов)
	5 - Самовывоз из московского офиса
	6 - По зарубежью
	*/

	$row['dostavka'] = mb_strtolower($row['dostavka']);
	$d = $row['dostavka'];
	if (strrpos($d, 'области') !== false) {
		//2 - Курьером по Московской области
		$newRow['delivery_type'] = 2;
	} elseif (strrpos($d, 'москве') !== false) {
		//1 - Курьером по Москве
		$newRow['delivery_type'] = 1;
	} elseif (strrpos($d, 'почт')  !== false || strrpos($d, 'россии')  !== false) {
		//3 - Почтой (доставка по всей России)
		$newRow['delivery_type'] = 3;
	} elseif (strrpos($d, 'самовывоз')  !== false || strrpos($d, 'самомывоз')  !== false) {
		//5 - Самовывоз из московского офиса
		$newRow['delivery_type'] = 5;
	} elseif (strrpos($d, 'траспортной') !== false) {
		//4 - Траспортной компанией в регионы (для крупногабаритных грузов)
		$newRow['delivery_type'] = 4;
	} elseif (strrpos($d, ' зарубежью')  !== false) {
		//6 - По зарубежью
		$newRow['delivery_type'] = 6;
	} else {
		$newRow['delivery_type'] = 0;
	}

	$newRow['phone'] = $row['tel'];
	$newRow['email'] = $row['email'];
	$newRow['adress'] = $row['adres'];

	$newRow['status'] = $row['status'];

	//Добавляем коммент, если есть
	/*
	if (!empty($row['comment'])) {
		$dbNew->insert(
			'orders_log',
			array(
				'order_id' => $row['id'],
				'date' => date("Y-m-d H:i:s", $newRow['date']),
				'action' => 1,
				'descr' => $row['comment']
			)
		);
	}*/
	$newRow['comment'] = $row['comment'];

	$newRow['h'] = $row['h'];

	$newRow['remind'] = $row['remind'];
	$newRow['attented'] = $row['attented'];
	$newRow['partner_id'] = $row['partner_id'];

	if ($dbNew->insert('orders', $newRow) <= 0) {
		iLog('orders: Error : ' . $dbNew->error . ' id = ' . $row['id']);
	}

}

// коменты к заказам
$dbNew->query("TRUNCATE `orders_log`");

$sel = $dbOld->query("SELECT * FROM `orders_log` ORDER BY `id` ASC");
while ($row = $dbOld->fetch($sel)) {
	$aid = $row['id'];

	unset($row['id']);

	if ($dbNew->insert('orders_log', $row) <= 0) {
		iLog('orders_log: Error : ' . $dbNew->error . ' id = ' . $aid);
	}

}

//Переносим отзывы о магазине

$dbNew->query("TRUNCATE `shop_otzyv`");

$sel = $dbOld->query("SELECT * FROM `shop_otzyv` ORDER BY `id` ASC");
while ($row = $dbOld->fetch($sel)) {

	if ($dbNew->insert('shop_otzyv', $row) <= 0) {
		iLog('shop_otzyv: Error : ' . $dbNew->error . ' id = ' . $row['id']);
	}

}

//Переносим отзывы о товарах

$dbNew->query("TRUNCATE `otzyv`");

$sel = $dbOld->query("SELECT * FROM `otzyv` ORDER BY `id` ASC");
while ($row = $dbOld->fetch($sel)) {
	
	$day = substr($row['date'], 0, 2);
	$month = substr($row['date'], 3, 2);
	$year = substr($row['date'], 6, 2);
	
	$row['date'] = mktime(0, 0, 0, $month, $day, $year);
	
	$row['comment'] = $row['des'];
	unset($row['des']);
	
	if ($dbNew->insert('otzyv', $row) <= 0) {
		iLog('otzyv: Error : ' . $dbNew->error . ' id = ' . $row['id']);
	}

}

//Переносим меню

$dbNew->query("TRUNCATE `menuleft`");

$sel = $dbOld->query("SELECT * FROM `menuleft` ORDER BY `id` ASC");
while ($row = $dbOld->fetch($sel)) {

	if ($dbNew->insert('menuleft', $row) <= 0) {
		iLog('menuleft: Error : ' . $dbNew->error . ' id = ' . $row['id']);
	}

}

//Переносим страницы

$dbNew->query("TRUNCATE `pages`");

$sel = $dbOld->query("SELECT * FROM `pages` ORDER BY `id` ASC");
while ($row = $dbOld->fetch($sel)) {
	if ($dbNew->insert('pages', $row) <= 0) {
		iLog('pages: Error : ' . $dbNew->error . ' id = ' . $row['id']);
	}
}

//Вносим значения в конфигурацию
$siteConfig = array(
	'site.web_addr' => 'http://med-serdce.ru/',
	'site.email_from' => 'robot@med-serdce.ru',
	'site.email_manager' => $dbOld->fetchOne("SELECT `email` FROM `config` LIMIT 1"),
	'site.email_support' => 'info@med-serdce.ru',
	'site.name' => $dbOld->fetchOne("SELECT `name` FROM `config` LIMIT 1"),
	'site.phone' => $dbOld->fetchOne("SELECT `tel` FROM `config` LIMIT 1"),
	'site.work_mode' => $dbOld->fetchOne("SELECT `rezhim` FROM `config` LIMIT 1"),

	'order.prefix' => 'M-',

	'yandex_search.user' => 'med-serdce',
	'yandex_search.key' => '03.127610843:15295b18edca6e2bdc363d04008f7873',

	'path.images_cats' => 'cat/',
	'path.images_cats_menu' => 'cat/pod/',
	'path.images_menu' => 'cat/main/',
	'path.images_product_extra_medium' => 'more/medium/',
	'path.images_product_extra_original' => 'more/',
	'path.images_product_extra_preview' => 'more/small/',
	'path.images_product_medium' => 'medium/',
	'path.images_product_original' => '',
	'path.images_product_preview' => 'big/',
	'path.images_product_small' => 'small/',
	'path.images_tags' => 'tags/',

	'template.link_articles' => '/articles/[chpu].html',
	'template.link_cart' => '/pay/[chpu].html',
	'template.link_cat' => '/cat/[chpu].html',
	'template.link_catalog' => '/catalog/[chpu].html',
	'template.link_catalog_old' => '/catalog/[chpu].html',
	'template.link_cat_old' => '/cat/[chpu].html',
	'template.link_mcat' => '/mcat/[chpu].html',
	'template.link_page' => '/page/[chpu].html',
	'template.link_tags' => '/tags/[chpu].html'

);

foreach ($siteConfig as $key => $value) {
	$tmp = parseKey($key);
	$value = $dbNew->real_escape_string($value);

	$dbNew->query("UPDATE `config` SET `value` = '{$value}' WHERE `key_group` = '{$tmp['keyGroup']}' AND `key` = '{$tmp['key']}'");
}

//Страницу на главной
$des = $dbNew->real_escape_string( $dbOld->fetchOne("SELECT `seo_text` FROM `config` LIMIT 1") );
$seoTitle = $dbNew->real_escape_string( $dbOld->fetchOne("SELECT `seo_title_text` FROM `config` LIMIT 1") );
$seoDes = $dbNew->real_escape_string( $dbOld->fetchOne("SELECT `seo_des` FROM `config` LIMIT 1") );
$seoKey = $dbNew->real_escape_string(  $dbOld->fetchOne("SELECT `seo_key` FROM `config` LIMIT 1") );

$dbNew->query("
	REPLACE INTO
		`pages`
	SET
		`chpu` = 'page-on-main',
		`title` = 'Текст на главной',
		`des` = '{$des}',
		`seo_title` = '{$seoTitle}',
		`seo_des` = '{$seoDes}',
		`seo_key` = '{$seoKey}'
	");


//Сетка габаритов
//Сетку габаритов лучше перенести в ручную, сохраняя структуру таблиц

$tables = array(
	'cat_gabarits',
	'cat_gabarits_2_delivery_price',
	'cat_gabarits_discount_2_delivery_price',
	'category_2_delivery_group',
	'delivery',
	'delivery_group',
	'delivery_group_2_delivery_price',
	'delivery_price'
	);

iLog('Импорт почти завершен. Следующие ' . count($tables) . ' необходимо перенести вручную: ' . implode(", ", $tables) . '.');