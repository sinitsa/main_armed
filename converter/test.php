<?php
error_reporting(E_ALL);
set_time_limit(0);
date_default_timezone_set('Europe/Moscow');

header("Content-Type: text/plain; charset=utf-8');"); 

require('config.php');
require('functions.php');
require('Database.class.php');
require('simple_html_dom.php');

$dbOld = new Database($config['dbOld']);
$dbNew = new Database($config['dbNew']);


/*
$tovar = $dbOld->fetchOne("SELECT tovar FROM pay_history WHERE id = '20024'");

var_dump($tovar);*/

$data = <<<EOF
<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
	<tr>
		<td width="80" align="left" valign="top">
			<a href="http://med-serdce.ru/catalog/hodunki-ly-970.html">
				<img src="http://med-serdce.ru/upload/small/2130.jpg" border="0" align="left" />
			</a>
		</td>
		<td align="left" valign="top">
			<a href="http://med-serdce.ru/catalog/hodunki-ly-970.html" style="color:#333;">Ходунки Optimal-Kappa с подмышечной опорой LY-970</a> (1 шт.)<br />
			<br />
			<span  style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;">7450</span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">руб.</span>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
	<tr>
		<td width="80" align="left" valign="top">
			<a href="http://med-serdce.ru/catalog/hodunki-ly-971.html">
				<img src="http://med-serdce.ru/upload/small/2131.jpg" border="0" align="left" />
			</a>
		</td>
		<td align="left" valign="top">
			<a href="http://med-serdce.ru/catalog/hodunki-ly-971.html" style="color:#333;">Ходунки Optimal-Kappa с опорой под локоть LY-971</a> (1 шт.)<br />
			<br />
			<span  style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;">7250</span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">руб.</span>
		</td>
	</tr>
</table>Общая сумма: 13965 (скидка 5%)<br>
EOF;

$data = <<<EOF
<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
<tr>
	<td width="80" align="left" valign="top"><a href="http://med-serdce.ru/catalog/massazhnaya-podushka-beurer-mg-140.html"><img src="http://med-serdce.ru/upload/small/256.jpg" border="0" align="left" /></a></td>
	<td align="left" valign="top">
		<a href="http://med-serdce.ru/catalog/massazhnaya-podushka-beurer-mg-140.html" style="color:#333;">Массажная подушка Beurer MG140</a> (1 шт.)<br />
		<br />
		<span  style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;">2900</span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">руб.</span>
	</td>
</tr>
</table><br />
<p><i>Доставка</i>: <span class="free">Бесплатно</span></p>
<p>Общая сумма: 7702,8 (скидка 2%)</p>
<p><i>Доставка</i>: <span class="sum">300</span> руб.</p>
<p>Скидка 5%</p>
EOF;

$data = <<<EOF
<a href=http://med-serdce.ru/catalog/vakuumnyi-massazher.html>Вакуумный массажер cellules (целлюлес)</a> -  1шт. На сумму 1100 рублей<br>
<a href=http://med-serdce.ru/catalog/massazher-dlya-litsa-s-infrakrasnym-progrevom.html>Массажер – для лица с инфракрасным прогревом FR22-R</a> -  1шт. На сумму 1000 рублей<br>
<a href=http://med-serdce.ru/catalog/massazhnoe-ustroistvo-relax-tone.html>Массажер скульптор тела Relax & Tone (Релакс анд Тон)</a> -  1шт. На сумму 1490 рублей<br>
EOF;

$data = <<<EOF
<a href=http://med-serdce.ru/catalog/massazhnoe-ustroistvo-relax-tone.html>Массажер скульптор тела Relax & Tone (Релакс анд Тон)</a> -  1шт. На сумму 1490 рублей<br>
EOF;

$dbNew->makeConnection();
if (preg_match("/<a href.*?\/catalog\/(.*?)\.html.*?>.*?<\/a>(.*?)<br>/si", $data)) {
		//Еще один тип
		preg_match_all("/<a href.*?\/catalog\/(.*?)\.html.*?>.*?<\/a>(.*?)<br>/si", $data, $tmp);

		$priceSum = 0;

		for ($i = 0; $i < count($tmp[0]); $i++) {
			$chpu = $dbNew->real_escape_string($tmp[1][$i]);
			$id = $dbNew->fetchOne("SELECT `id` FROM `catalog` WHERE `chpu` = '{$chpu}' LIMIT 1");

			preg_match("/([0-9]+)шт.*?([0-9]+) рублей/si", $tmp[2][$i], $tmp2);

			$amount = $tmp2[1];
			$price = $tmp2[2] / $amount;
			$priceSum += $tmp2[2];

			$products[] = array(
					'id' => $id,
					'amount' => $amount,
					'price' => $price,
					'price_after_discount' => $price
				);
		}

		$order_price = array(
			'discount_type' => 'percent',
			'discount_value' => 0,
			'price_after_global_discount' => $priceSum,
			'price_before_global_discount' => $priceSum,
			'delivery_price' => false
		);

		var_dump($products);
}

/*
foreach ($dbOld->fetchAll("SELECT id, dostavka FROM `pay_history` GROUP BY `dostavka` LIMIT 50") as $row) {
	$row['dostavka'] = mb_strtolower($row['dostavka']);
	$d = $row['dostavka'];
	if (strrpos($d, 'области') !== false) {
		iLog("2 - Курьером по Московской области");
	} elseif (strrpos($d, 'москве') !== false) {
		iLog("1 - Курьером по Москве");
	} elseif (strrpos($d, 'почт')  !== false || strrpos($d, 'россии')  !== false) {
		iLog("3 - Почтой (доставка по всей России)");
	} elseif (strrpos($d, 'самовывоз')  !== false || strrpos($d, 'самомывоз')  !== false) {
		iLog("5 - Самовывоз из московского офиса");
	} elseif (strrpos($d, 'траспортной') !== false) {
		iLog("4 - Траспортной компанией в регионы (для крупногабаритных грузов)");
	} elseif (strrpos($d, ' зарубежью')  !== false) {
		iLog("6 - По зарубежью");
	} else {
		iLog("unknow");
	}
	var_dump($row);
}
*/