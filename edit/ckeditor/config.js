/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	 config.language = 'en';
	// config.uiColor = '#AADC6E';
	
	config.toolbar = 'Basic'; //С„СѓРЅРєС†РёРѕРЅР°Р»СЊРЅРѕСЃС‚СЊ СЂРµРґР°РєС‚РѕСЂР°, Basic-РјРёРЅРёРјСѓРј, Full-РјР°РєСЃРёРјСѓРј
	config.toolbar_Basic = //РёРЅРґРёРІРёРґСѓР°Р»СЊРЅР°СЏ РЅР°СЃС‚СЂРѕР№РєР° СЂРµР¶РёРјР° Basic
	[
['Source','-', 'Bold', 'Italic','Underline', '-', 'Link', 'Unlink','-','NumberedList','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Image','Table','HorizontalRule','SpecialChar', 'Format']

	];
	config.extraPlugins = 'stylescombo';
};