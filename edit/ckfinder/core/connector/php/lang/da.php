<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Danish language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'Det var ikke muligt at fuldfГёre handlingen. (Fejl: %1)',
	'Errors' => array (
		'10' => 'Ugyldig handling.',
		'11' => 'Ressourcetypen blev ikke angivet i anmodningen.',
		'12' => 'Ressourcetypen er ikke gyldig.',
		'102' => 'Ugyldig fil eller mappenavn.',
		'103' => 'Det var ikke muligt at fuldfГёre handlingen pГҐ grund af en begrГ¦nsning i rettigheder.',
		'104' => 'Det var ikke muligt at fuldfГёre handlingen pГҐ grund af en begrГ¦nsning i filsystem rettigheder.',
		'105' => 'Ugyldig filtype.',
		'109' => 'Ugyldig anmodning.',
		'110' => 'Ukendt fejl.',
		'115' => 'En fil eller mappe med det samme navn eksisterer allerede.',
		'116' => 'Mappen blev ikke fundet. OpdatГ©r listen eller prГёv igen.',
		'117' => 'Filen blev ikke fundet. OpdatГ©r listen eller prГёv igen.',
		'118' => 'Originalplacering og destination er ens.',
		'201' => 'En fil med det samme filnavn eksisterer allerede. Den uploadede fil er blevet omdГёbt til "%1".',
		'202' => 'Ugyldig fil.',
		'203' => 'Ugyldig fil. FilstГёrrelsen er for stor.',
		'204' => 'Den uploadede fil er korrupt.',
		'205' => 'Der er ikke en midlertidig mappe til upload til rГҐdighed pГҐ serveren.',
		'206' => 'Upload annulleret af sikkerhedsmГ¦ssige ГҐrsager. Filen indeholder HTML-lignende data.',
		'207' => 'Den uploadede fil er blevet omdГёbt til "%1".',
		'300' => 'Flytning af fil(er) fejlede.',
		'301' => 'Kopiering af fil(er) fejlede.',
		'500' => 'Filbrowseren er deaktiveret af sikkerhedsmГ¦ssige ГҐrsager. Kontakt systemadministratoren eller kontrollГ©r CKFinders konfigurationsfil.',
		'501' => 'UnderstГёttelse af thumbnails er deaktiveret.',
	)
);
