<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Spanish language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'No ha sido posible completar la solicitud. (Error %1)',
	'Errors' => array (
		'10' => 'Comando incorrecto.',
		'11' => 'El tipo de recurso no ha sido especificado en la solicitud.',
		'12' => 'El tipo de recurso solicitado no es vГЎlido.',
		'102' => 'Nombre de fichero o carpeta no vГЎlido.',
		'103' => 'No se ha podido completar la solicitud debido a las restricciones de autorizaciГіn.',
		'104' => 'No ha sido posible completar la solicitud debido a restricciones en el sistema de ficheros.',
		'105' => 'La extensiГіn del archivo no es vГЎlida.',
		'109' => 'PeticiГіn invГЎlida.',
		'110' => 'Error desconocido.',
		'115' => 'Ya existe un fichero o carpeta con ese nombre.',
		'116' => 'No se ha encontrado la carpeta. Por favor, actualice y pruebe de nuevo.',
		'117' => 'No se ha encontrado el fichero. Por favor, actualice la lista de ficheros y pruebe de nuevo.',
		'118' => 'Las rutas origen y destino son iguales.',
		'201' => 'Ya existГ­a un fichero con ese nombre. El fichero subido ha sido renombrado como "%1".',
		'202' => 'Fichero invГЎlido.',
		'203' => 'Fichero invГЎlido. El peso es demasiado grande.',
		'204' => 'El fichero subido estГЎ corrupto.',
		'205' => 'La carpeta temporal no estГЎ disponible en el servidor para las subidas.',
		'206' => 'La subida se ha cancelado por razones de seguridad. El fichero contenГ­a cГіdigo HTML.',
		'207' => 'El fichero subido ha sido renombrado como "%1".',
		'300' => 'Ha fallado el mover el(los) fichero(s).',
		'301' => 'Ha fallado el copiar el(los) fichero(s).',
		'500' => 'El navegador de archivos estГЎ deshabilitado por razones de seguridad. Por favor, contacte con el administrador de su sistema y compruebe el fichero de configuraciГіn de CKFinder.',
		'501' => 'El soporte para iconos estГЎ deshabilitado.',
	)
);
