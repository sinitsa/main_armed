<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Croatian language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'Nije moguД‡e zavrЕЎiti zahtjev. (GreЕЎka %1)',
	'Errors' => array (
		'10' => 'Nepoznata naredba.',
		'11' => 'Nije navedena vrsta u zahtjevu.',
		'12' => 'ZatraЕѕena vrsta nije vaЕѕeД‡a.',
		'102' => 'Neispravno naziv datoteke ili direktoija.',
		'103' => 'Nije moguД‡e izvrЕЎiti zahtjev zbog ograniДЌenja pristupa.',
		'104' => 'Nije moguД‡e izvrЕЎiti zahtjev zbog ograniДЌenja postavka sustava.',
		'105' => 'Nedozvoljena vrsta datoteke.',
		'109' => 'Nedozvoljen zahtjev.',
		'110' => 'Nepoznata greЕЎka.',
		'115' => 'Datoteka ili direktorij s istim nazivom veД‡ postoji.',
		'116' => 'Direktorij nije pronaД‘en. OsvjeЕѕite stranicu i pokuЕЎajte ponovo.',
		'117' => 'Datoteka nije pronaД‘ena. OsvjeЕѕite listu datoteka i pokuЕЎajte ponovo.',
		'118' => 'Putanje izvora i odrediЕЎta su jednake.',
		'201' => 'Datoteka s istim nazivom veД‡ postoji. Poslana datoteka je promjenjena u "%1".',
		'202' => 'Neispravna datoteka.',
		'203' => 'Neispravna datoteka. VeliДЌina datoteke je prevelika.',
		'204' => 'Poslana datoteka je neispravna.',
		'205' => 'Ne postoji privremeni direktorij za slanje na server.',
		'206' => 'Slanje je poniЕЎteno zbog sigurnosnih postavki. Naziv datoteke sadrЕѕi HTML podatke.',
		'207' => 'Poslana datoteka je promjenjena u "%1".',
		'300' => 'PremjeЕЎtanje datoteke(a) nije uspjelo.',
		'301' => 'Kopiranje datoteke(a) nije uspjelo.',
		'500' => 'PretraЕѕivanje datoteka nije dozvoljeno iz sigurnosnih razloga. Molimo kontaktirajte administratora sustava kako bi provjerili postavke CKFinder konfiguracijske datoteke.',
		'501' => 'The thumbnails support is disabled.',
	)
);
