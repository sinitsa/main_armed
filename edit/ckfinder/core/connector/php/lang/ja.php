<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Japanese language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'гѓЄг‚Їг‚Ёг‚№гѓ€гЃ®е‡¦зђ†гЃ«е¤±ж•—гЃ—гЃѕгЃ—гЃџгЂ‚ (Error %1)',
	'Errors' => array (
		'10' => 'дёЌж­ЈгЃЄг‚ігѓћгѓігѓ‰гЃ§гЃ™гЂ‚',
		'11' => 'гѓЄг‚Ѕгѓјг‚№г‚їг‚¤гѓ—гЃЊз‰№е®љгЃ§гЃЌгЃѕгЃ›г‚“гЃ§гЃ—гЃџгЂ‚',
		'12' => 'и¦Ѓж±‚гЃ•г‚ЊгЃџгѓЄг‚Ѕгѓјг‚№гЃ®г‚їг‚¤гѓ—гЃЊж­ЈгЃ—гЃЏгЃ‚г‚ЉгЃѕгЃ›г‚“гЂ‚',
		'102' => 'гѓ•г‚Ўг‚¤гѓ«еђЌ/гѓ•г‚©гѓ«гѓЂеђЌгЃЊж­ЈгЃ—гЃЏгЃ‚г‚ЉгЃѕгЃ›г‚“гЂ‚',
		'103' => 'гѓЄг‚Їг‚Ёг‚№гѓ€г‚’е®Њдє†гЃ§гЃЌгЃѕгЃ›г‚“гЃ§гЃ—гЃџгЂ‚иЄЌиЁјг‚Ёгѓ©гѓјгЃ§гЃ™гЂ‚',
		'104' => 'гѓЄг‚Їг‚Ёг‚№гѓ€г‚’е®Њдє†гЃ§гЃЌгЃѕгЃ›г‚“гЃ§гЃ—гЃџгЂ‚гѓ•г‚Ўг‚¤гѓ«гЃ®гѓ‘гѓјгѓџгѓѓг‚·гѓ§гѓігЃЊиЁ±еЏЇгЃ•г‚ЊгЃ¦гЃ„гЃѕгЃ›г‚“гЂ‚',
		'105' => 'ж‹Ўејµе­ђгЃЊж­ЈгЃ—гЃЏгЃ‚г‚ЉгЃѕгЃ›г‚“гЂ‚',
		'109' => 'дёЌж­ЈгЃЄгѓЄг‚Їг‚Ёг‚№гѓ€гЃ§гЃ™гЂ‚',
		'110' => 'дёЌжЋгЃЄг‚Ёгѓ©гѓјгЃЊз™єз”џгЃ—гЃѕгЃ—гЃџгЂ‚',
		'115' => 'еђЊгЃеђЌе‰ЌгЃ®гѓ•г‚Ўг‚¤гѓ«/гѓ•г‚©гѓ«гѓЂгЃЊгЃ™гЃ§гЃ«е­ењЁгЃ—гЃ¦гЃ„гЃѕгЃ™гЂ‚',
		'116' => 'гѓ•г‚©гѓ«гѓЂгЃЊи¦‹гЃ¤гЃ‹г‚ЉгЃѕгЃ›г‚“гЃ§гЃ—гЃџгЂ‚гѓљгѓјг‚ёг‚’ж›ґж–°гЃ—гЃ¦е†Ќеє¦гЃЉи©¦гЃ—дё‹гЃ•гЃ„гЂ‚',
		'117' => 'гѓ•г‚Ўг‚¤гѓ«гЃЊи¦‹гЃ¤гЃ‹г‚ЉгЃѕгЃ›г‚“гЃ§гЃ—гЃџгЂ‚гѓљгѓјг‚ёг‚’ж›ґж–°гЃ—гЃ¦е†Ќеє¦гЃЉи©¦гЃ—дё‹гЃ•гЃ„гЂ‚',
		'118' => 'еЇѕи±ЎгЃЊз§»е‹•е…ѓгЃЁеђЊгЃе ґж‰Ђг‚’жЊ‡е®љгЃ•г‚ЊгЃ¦гЃ„гЃѕгЃ™гЂ‚',
		'201' => 'еђЊгЃеђЌе‰ЌгЃ®гѓ•г‚Ўг‚¤гѓ«гЃЊгЃ™гЃ§гЃ«е­ењЁгЃ—гЃ¦гЃ„гЃѕгЃ™гЂ‚"%1" гЃ«гѓЄгѓЌгѓјгѓ гЃ—гЃ¦дїќе­гЃ•г‚ЊгЃѕгЃ—гЃџгЂ‚',
		'202' => 'дёЌж­ЈгЃЄгѓ•г‚Ўг‚¤гѓ«гЃ§гЃ™гЂ‚',
		'203' => 'гѓ•г‚Ўг‚¤гѓ«гЃ®г‚µг‚¤г‚єгЃЊе¤§гЃЌгЃ™гЃЋгЃѕгЃ™гЂ‚',
		'204' => 'г‚ўгѓѓгѓ—гѓ­гѓјгѓ‰гЃ•г‚ЊгЃџгѓ•г‚Ўг‚¤гѓ«гЃЇеЈЉг‚ЊгЃ¦гЃ„гЃѕгЃ™гЂ‚',
		'205' => 'г‚µгѓјгѓђе†…гЃ®дёЂж™‚дЅњжҐ­гѓ•г‚©гѓ«гѓЂгЃЊе€©з”ЁгЃ§гЃЌгЃѕгЃ›г‚“гЂ‚',
		'206' => 'г‚»г‚­гѓҐгѓЄгѓ†г‚ЈдёЉгЃ®зђ†з”±гЃ‹г‚‰г‚ўгѓѓгѓ—гѓ­гѓјгѓ‰гЃЊеЏ–г‚Љж¶€гЃ•г‚ЊгЃѕгЃ—гЃџгЂ‚гЃ“гЃ®гѓ•г‚Ўг‚¤гѓ«гЃ«гЃЇHTMLгЃ«дјјгЃџгѓ‡гѓјг‚їгЃЊеђ«гЃѕг‚ЊгЃ¦гЃ„гЃѕгЃ™гЂ‚',
		'207' => 'гѓ•г‚Ўг‚¤гѓ«гЃЇ "%1" гЃ«гѓЄгѓЌгѓјгѓ гЃ—гЃ¦дїќе­гЃ•г‚ЊгЃѕгЃ—гЃџгЂ‚',
		'300' => 'гѓ•г‚Ўг‚¤гѓ«гЃ®з§»е‹•гЃ«е¤±ж•—гЃ—гЃѕгЃ—гЃџгЂ‚',
		'301' => 'гѓ•г‚Ўг‚¤гѓ«гЃ®г‚ігѓ”гѓјгЃ«е¤±ж•—гЃ—гЃѕгЃ—гЃџгЂ‚',
		'500' => 'гѓ•г‚Ўг‚¤гѓ«гѓ–гѓ©г‚¦г‚¶гЃЇг‚»г‚­гѓҐгѓЄгѓ†г‚ЈдёЉгЃ®е€¶й™ђгЃ‹г‚‰з„ЎеЉ№гЃ«гЃЄгЃЈгЃ¦гЃ„гЃѕгЃ™гЂ‚г‚·г‚№гѓ†гѓ ж‹…еЅ“иЂ…гЃ«йЂЈзµЎг‚’гЃ—гЃ¦гЂЃCKFinderгЃ®иЁ­е®љг‚’гЃ”зўєиЄЌдё‹гЃ•гЃ„гЂ‚',
		'501' => 'г‚µгѓ гѓЌг‚¤гѓ«ж©џиѓЅгЃЇз„ЎеЉ№гЃ«гЃЄгЃЈгЃ¦гЃ„гЃѕгЃ™гЂ‚',
	)
);
