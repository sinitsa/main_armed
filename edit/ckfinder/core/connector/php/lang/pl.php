<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Polish language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'Wykonanie operacji zakoЕ„czyЕ‚o siД™ niepowodzeniem. (BЕ‚Д…d %1)',
	'Errors' => array (
		'10' => 'NieprawidЕ‚owe polecenie (command).',
		'11' => 'Brak wymaganego parametru: typ danych (resource type).',
		'12' => 'NieprawidЕ‚owy typ danych (resource type).',
		'102' => 'NieprawidЕ‚owa nazwa pliku lub folderu.',
		'103' => 'Wykonanie operacji nie jest moЕјliwe: brak uprawnieЕ„.',
		'104' => 'Wykonanie operacji nie powiodЕ‚o siД™ z powodu niewystarczajД…cych uprawnieЕ„ do systemu plikГіw.',
		'105' => 'NieprawidЕ‚owe rozszerzenie.',
		'109' => 'NieprawiЕ‚owe ЕјД…danie.',
		'110' => 'Niezidentyfikowany bЕ‚Д…d.',
		'115' => 'Plik lub folder o podanej nazwie juЕј istnieje.',
		'116' => 'Nie znaleziono folderu. OdЕ›wieЕј panel i sprГіbuj ponownie.',
		'117' => 'Nie znaleziono pliku. OdЕ›wieЕј listД™ plikГіw i sprГіbuj ponownie.',
		'118' => 'ЕљcieЕјki ЕєrГіdЕ‚owa i docelowa sД… jednakowe.',
		'201' => 'Plik o podanej nazwie juЕј istnieje. Nazwa przesЕ‚anego pliku zostaЕ‚a zmieniona na "%1".',
		'202' => 'NieprawidЕ‚owy plik.',
		'203' => 'NieprawidЕ‚owy plik. Plik przekracza dozwolony rozmiar.',
		'204' => 'PrzesЕ‚any plik jest uszkodzony.',
		'205' => 'Brak folderu tymczasowego na serwerze do przesyЕ‚ania plikГіw.',
		'206' => 'PrzesyЕ‚anie pliku zakoЕ„czyЕ‚o siД™ niepowodzeniem z powodГіw bezpieczeЕ„stwa. Plik zawiera dane przypominajД…ce HTML.',
		'207' => 'Nazwa przesЕ‚anego pliku zostaЕ‚a zmieniona na "%1".',
		'300' => 'Przenoszenie nie powiodЕ‚o siД™.',
		'301' => 'Kopiowanie nie powiodo siД™.',
		'500' => 'MenedЕјer plikГіw jest wyЕ‚Д…czony z powodГіw bezpieczeЕ„stwa. Skontaktuj siД™ z administratorem oraz sprawdЕє plik konfiguracyjny CKFindera.',
		'501' => 'Tworzenie miniaturek jest wyЕ‚Д…czone.',
	)
);
