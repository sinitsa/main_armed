<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Brazilian Portuguese language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'NГЈo foi possГ­vel completer o seu pedido. (Erro %1)',
	'Errors' => array (
		'10' => 'Comando invГЎlido.',
		'11' => 'O tipo de recurso nГЈo foi especificado na solicitaГ§ГЈo.',
		'12' => 'O recurso solicitado nГЈo Г© vГЎlido.',
		'102' => 'Nome do arquivo ou pasta invГЎlido.',
		'103' => 'NГЈo foi possГ­vel completar a solicitaГ§ГЈo por restriГ§Гµes de acesso.',
		'104' => 'NГЈo foi possГ­vel completar a solicitaГ§ГЈo por restriГ§Гµes de acesso do sistema de arquivos.',
		'105' => 'ExtensГЈo de arquivo invГЎlida.',
		'109' => 'SolicitaГ§ГЈo invГЎlida.',
		'110' => 'Erro desconhecido.',
		'115' => 'Uma arquivo ou pasta jГЎ existe com esse nome.',
		'116' => 'Pasta nГЈo encontrada. Atualize e tente novamente.',
		'117' => 'Arquivo nГЈo encontrado. Atualize a lista de arquivos e tente novamente.',
		'118' => 'Origem e destino sГЈo iguais.',
		'201' => 'Um arquivo com o mesmo nome jГЎ estГЎ disponГ­vel. O arquivo enviado foi renomeado para "%1".',
		'202' => 'Arquivo invГЎlido.',
		'203' => 'Arquivo invГЎlido. O tamanho Г© muito grande.',
		'204' => 'O arquivo enviado estГЎ corrompido.',
		'205' => 'Nenhuma pasta temporГЎria para envio estГЎ disponГ­vel no servidor.',
		'206' => 'TransmissГЈo cancelada por razГµes de seguranГ§a. O arquivo contem dados HTML.',
		'207' => 'O arquivo enviado foi renomeado para "%1".',
		'300' => 'NГЈo foi possГ­vel mover o(s) arquivo(s).',
		'301' => 'NГЈo foi possГ­vel copiar o(s) arquivos(s).',
		'500' => 'A navegaГ§ГЈo de arquivos estГЎ desativada por razГµes de seguranГ§a. Contacte o administrador do sistema.',
		'501' => 'O suporte a miniaturas estГЎ desabilitado.',
	)
);
