<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Romanian language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'Nu a fost posibilДѓ finalizarea cererii. (Eroare %1)',
	'Errors' => array (
		'10' => 'ComandДѓ invalidДѓ.',
		'11' => 'Tipul de resursДѓ nu a fost specificat Г®n cerere.',
		'12' => 'Tipul de resursДѓ cerut nu este valid.',
		'102' => 'Nume fiИ™ier sau nume dosar invalid.',
		'103' => 'Nu a fost posibiliДѓ finalizarea cererii din cauza restricИ›iilor de autorizare.',
		'104' => 'Nu a fost posibiliДѓ finalizarea cererii din cauza restricИ›iilor de permisiune la sistemul de fiИ™iere.',
		'105' => 'Extensie fiИ™ier invalidДѓ.',
		'109' => 'Cerere invalidДѓ.',
		'110' => 'Eroare necunoscutДѓ.',
		'115' => 'ExistДѓ deja un fiИ™ier sau un dosar cu acelaИ™i nume.',
		'116' => 'Dosar negДѓsit. Te rog Г®mprospДѓteazДѓ И™i Г®ncearcДѓ din nou.',
		'117' => 'FiИ™ier negДѓsit. Te rog Г®mprospДѓteazДѓ lista de fiИ™iere И™i Г®ncearcДѓ din nou.',
		'118' => 'Calea sursei И™i a И›intei sunt egale.',
		'201' => 'Un fiИ™ier cu acelaИ™i nume este deja disponibil. FiИ™ierul Г®ncДѓrcat a fost redenumit cu "%1".',
		'202' => 'FiИ™ier invalid.',
		'203' => 'FiИ™ier invalid. MДѓrimea fiИ™ierului este prea mare.',
		'204' => 'FiИ™ierul Г®ncДѓrcat este corupt.',
		'205' => 'Niciun dosar temporar nu este disponibil pentru Г®ncДѓrcarea pe server.',
		'206' => 'ГЋncДѓrcare anulatДѓ din motive de securitate. FiИ™ierul conИ›ine date asemДѓnДѓtoare cu HTML.',
		'207' => 'FiИ™ierul Г®ncДѓrcat a fost redenumit cu "%1".',
		'300' => 'Mutare fiИ™ier(e) eИ™uatДѓ.',
		'301' => 'Copiere fiИ™ier(e) eИ™uatДѓ.',
		'500' => 'Browser-ul de fiИ™iere este dezactivat din motive de securitate. Te rog contacteazДѓ administratorul de sistem И™i verificДѓ configurarea de fiИ™iere CKFinder.',
		'501' => 'FuncИ›ionalitatea de creat thumbnails este dezactivatДѓ.',
	)
);
