<?php
// Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
// For licensing, see LICENSE.html or http://ckfinder.com/license

//  Defines the object for the Vietnamese language.

$GLOBALS['CKFLang'] = array (
	'ErrorUnknown' => 'KhГґng thб»ѓ hoГ n tбєҐt yГЄu cбє§u. (Lб»—i %1)',
	'Errors' => array (
		'10' => 'Lб»‡nh khГґng hб»Јp lб»‡.',
		'11' => 'Kiб»ѓu tГ i nguyГЄn khГґng Д‘Ж°б»Јc chб»‰ Д‘б»‹nh trong yГЄu cбє§u.',
		'12' => 'Kiб»ѓu tГ i nguyГЄn yГЄu cбє§u khГґng hб»Јp lб»‡.',
		'102' => 'TГЄn tбє­p tin hay thЖ° mб»Ґc khГґng hб»Јp lб»‡.',
		'103' => 'KhГґng thб»ѓ hoГ n tбєҐt yГЄu cбє§u vГ¬ giб»›i hбєЎn quyб»Ѓn.',
		'104' => 'KhГґng thб»ѓ hoГ n tбєҐt yГЄu cбє§u vГ¬ giб»›i hбєЎn quyб»Ѓn cб»§a hб»‡ thб»‘ng tбє­p tin.',
		'105' => 'Phбє§n mб»џ rб»™ng tбє­p tin khГґng hб»Јp lб»‡.',
		'109' => 'YГЄu cбє§u khГґng hб»Јp lб»‡.',
		'110' => 'Lб»—i khГґng xГЎc Д‘б»‹nh.',
		'115' => 'Tбє­p tin hoбє·c thЖ° mб»Ґc cГ№ng tГЄn Д‘ГЈ tб»“n tбєЎi.',
		'116' => 'KhГґng thбєҐy thЖ° mб»Ґc. HГЈy lГ m tЖ°ЖЎi vГ  thб»­ lбєЎi.',
		'117' => 'KhГґng thбєҐy tбє­p tin. HГЈy lГ m tЖ°ЖЎi vГ  thб»­ lбєЎi.',
		'118' => 'ДђЖ°б»ќng dбє«n nguб»“n vГ  Д‘Г­ch giб»‘ng nhau.',
		'201' => 'Tбє­p tin cГ№ng tГЄn Д‘ГЈ tб»“n tбєЎi. Tбє­p tin vб»«a tбєЈi lГЄn Д‘Ж°б»Јc Д‘б»•i tГЄn thГ nh "%1".',
		'202' => 'Tбє­p tin khГґng hб»Јp lб»‡.',
		'203' => 'Tбє­p tin khГґng hб»Јp lб»‡. Dung lЖ°б»Јng quГЎ lб»›n.',
		'204' => 'Tбє­p tin tбєЈi lГЄn bб»‹ hб»Џng.',
		'205' => 'KhГґng cГі thЖ° mб»Ґc tбєЎm Д‘б»ѓ tбєЈi tбє­p tin.',
		'206' => 'Huб»· tбєЈi lГЄn vГ¬ lГ­ do bбєЈo mбє­t. Tбє­p tin chб»©a dб»Ї liб»‡u giб»‘ng HTML.',
		'207' => 'Tбє­p tin Д‘Ж°б»Јc Д‘б»•i tГЄn thГ nh "%1".',
		'300' => 'Di chuyб»ѓn tбє­p tin thбєҐt bбєЎi.',
		'301' => 'ChГ©p tбє­p tin thбєҐt bбєЎi.',
		'500' => 'TrГ¬nh duyб»‡t tбє­p tin bб»‹ vГґ hiб»‡u vГ¬ lГ­ do bбєЈo mбє­t. Xin liГЄn hб»‡ quбєЈn trб»‹ hб»‡ thб»‘ng vГ  kiб»ѓm tra tбє­p tin cбєҐu hГ¬nh CKFinder.',
		'501' => 'Chб»©c nДѓng hб»— trб»Ј бєЈnh mбє«u bб»‹ vГґ hiб»‡u.',
	)
);
