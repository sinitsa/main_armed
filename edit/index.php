<?php
include ('connect.php');
include ('../func/core.php');

//Перевод даты на русский
$days['Mon'] = 'Понедельник';
$days['Tue'] = 'Вторник';
$days['Wed'] = 'Среда';
$days['Thu'] = 'Четверг';
$days['Fri'] = 'Пятница';
$days['Sat'] = 'Суббота';
$days['Sun'] = 'Воскресенье';

$months = array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
//=========

session_start();

$_SESSION['edit_hash'] = true;
$_SESSION['auth'] = 'edit';

include ('up.php');

query_all('pages');

$fd = $global_massiv[0]['title'];
query_email();
?>
<script>
	$(function(){
		$(".oplata").on("click", function(){
			var delivery = $(this).parent().find("input[type=checkbox]").prop("checked");
			$.ajax({
				url : '/edit/sendletter.php',
				data : 'oid='+$(this).data('id')+'&omail='+$(this).data('mail')+'&delivery='+ delivery,
				type : "POST", 
				success : function (data, textStatus, jqXHR) {
					alert(data);
				}
			});
		});
		$(".icon-ok.price").on("click", function(){
			var el = $(this);
			var newPrice = el.parent().find("input").val();
			var idOrder = el.data('id');
			if(newPrice === "" || newPrice === {} || newPrice === NaN || newPrice === undefined){
				alert('Введите новую цену.');
				return false;
			}
			var $this = this;
			$.ajax({
				url: '/edit/editOrder.php', 
				data: {method: "editPrice", newPrice: newPrice, idOrder: idOrder}, 
				type: "POST", 
				success: function(data){
					$($this).parent().parent().find(".all_price_to_pay").text("Общая сумма: " + newPrice);
					$($this).parent().parent().find(".all_price_to_pay").append('<div class="alert alert-warning fade in" style="font-size: 12px; margin: 0px;"><button type="button" class="mini close" data-dismiss="alert" aria-hidden="true" style="font-size: 11px;">x</button>Общая сумма изменена<div>');
				}	
			})
		});
		$(".icon-ok.delivery").on("click", function(){
			var el = $(this);
			var newDel = el.parent().find("input").val();
			if(newDel === 0){
				newDel = "0";
			}
			var idOrder = el.data('id');
			if(newDel === "" || newDel === {} || newDel === NaN || newDel === undefined){
				alert('Введите новую стоимость доставки.');
				return false;
			}
			var $this = this;
			$.ajax({
				url: '/edit/editOrder.php', 
				data: {method: "editDel", newDel: newDel, idOrder: idOrder}, 
				type: "POST", 
				success: function(data){
					$($this).parent().parent().find(".dellcPr").text(newDel);
					$($this).parent().parent().find(".all_price_to_pay").append('<div class="alert alert-warning fade in" style="font-size: 12px; margin: 0px;"><button type="button" class="mini close" data-dismiss="alert" aria-hidden="true" style="font-size: 11px;">x</button>Сумма доставки изменена<div>');
				}	
			})
		});
	});

</script>
<style>
.reserved{
	display: inline-block;
	width: 150px;
	font-size: 11px;
	position: relative;
	top: 7px;
	left: 20px;
}
</style>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
<?php if ($_SESSION['auth_type'] != 'manager') { ?>
	<tr>
		<td width="33%" valign="top">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="70" align="left" valign="top"><img src="icons/page.jpg" width="60" height="60" alt="" /></td>
					<td align="left" valign="top">
						<table width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td align="left" valign="bottom" class="big_menu">Страницы</td>
							</tr>
							<tr>
								<td align="left"><i class="icon-plus"></i> <a href="m_pages/add.php" class="txt_spisok">Добавить страницу</a></td>
							</tr>
							<tr>
								<td align="left"></td>
							</tr>

						</table></td>
				</tr>
				<tr>
					<td colspan="2" align="left" valign="top">
						<table width="80%" class="table-condensed" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td align="left"><i class="icon-pencil"></i> <em>Изменить:</em></td>
							</tr>
<?php
// Спивок страниц
$pages = getStaticPages();
foreach ($pages as $page) {
	echo ('
								<tr>
									<td align="left"><a href="/edit/m_pages/edit.php?id=' . $page['id'] . '" class="txt_spisok">' . $page['title'] . '</a></td>
								</tr>
							');
}
?>
						</table>
					</td>
				</tr>
			</table>

		</td>
		<td width="33%" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="0">
				<tr>
					<td width="70" rowspan="7" align="left" valign="top"><img src="icons/cat.jpg" width="60" height="60" alt="" /></td>
					<td align="left" valign="bottom" class="big_menu">Каталог</td>
				</tr>
				<tr>
					<td align="left"><i class="icon-pencil"></i> <a href="m_mcat/" class="txt_spisok">Изменить каталог</a></td>
				</tr>
				<tr>
					<td align="left"><i class="icon-pencil"></i> <a href="m_delivery/" class="txt_spisok">Редактор доставки</a></td>
				</tr>
			</table>
			<br />
			<table width="100%" border="0" cellpadding="1" cellspacing="0">
				<tr>
					<td width="70" rowspan="5" align="left" valign="top"><img src="icons/config.jpg" width="60" height="60" alt="" /></td>
					<td align="left" valign="bottom" class="big_menu">Настройки</td>
				</tr>
				<tr>
					<td align="left"><i class="icon-cog"></i> <a href="m_configuration/" class="txt_spisok">Конфигурация</a></td>
				</tr>
				<tr>
					<td align="left"><i class="icon-cog"></i> <a href="m_seo/" class="txt_spisok">SEO description</a></td>
				</tr>
				<!-- <tr>
					<td align="left"><i class="icon-cog"></i> <a href="m_basket/" class="txt_spisok">Настройки корзины</a></td>
				</tr>
				-->
				<tr>
					<td align="left">&nbsp;</td>
				</tr>
				<tr>
					<td align="left">&nbsp;</td>
				</tr>
			</table>
		</td>
		<td width="33%" valign="top">
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td colspan="2" align="left" valign="bottom" class="big_menu">Разное</td>
				</tr>

				<tr>
					<td align="left" class="txt_spisok"><img src="icons/mini_foto.jpg" width="30" height="30" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_resizer/"  class="txt_spisok">Ресайз картинок</a></td>
				</tr>
				<!--
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/menu_mini.jpg" width="30" height="30" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_filter/"  class="txt_spisok">Редактировать фильтры товаров</a></td>
				</tr>
				-->
				<tr>
					<td width="30" align="left" valign="top" class="txt_spisok"><img src="icons/blag_mini.jpg" width="30" height="29" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="m_feedback/feedback.php" class="txt_spisok">Отзывы о магазине</a></td>
				</tr>
				<? $feedNewCount = count(getProductsFeedback()); ?>
				<tr>
					<td width="30" align="left" valign="top" class="txt_spisok"><img src="icons/blag_mini.jpg" width="30" height="29" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="m_catalog_feedback/" class="txt_spisok">Отзывы о товарах<?=($feedNewCount>0?" <strong>({$feedNewCount})</strong>":'')?></a></td>
				</tr>
			   <tr>
					<td width="30" align="left" valign="top" class="txt_spisok"><img src="icons/blag_mini.jpg" width="30" height="29" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="m_opt_deals/" class="txt_spisok">Предложения оптовиков</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/ya_mid.jpg" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_market" class="txt_spisok">Генератор YML</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/ya_webmaster.jpg" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_webmaster" class="txt_spisok">Генератор Яндекс.Вебмастер</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/google.jpg" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_google" class="txt_spisok">Генератор Google Merchant Center</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/mailru.jpg" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_mailtorg" class="txt_spisok">Генератор mail.ru</a></td>
				</tr>	
				<tr>
					<td align="left" class="txt_spisok"><img src="/edit/m_tiu/ico/market_big.jpg" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_tiu" class="txt_spisok">Генератор tiu.ru</a></td>
				</tr>						
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/mini_foto.jpg" width="30" height="30" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_main_banner/" class="txt_spisok">Редактор банеров на главной</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="" width="30" height="30" alt="" /></td>
                                        <td align="left" class="txt_spisok"><a href="/edit/favorites.php" class="txt_spisok">Топ избранных товаров</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="" width="30" height="30" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_main_blocks/" class="txt_spisok">Редактор блоков на главной</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="" width="30" height="30" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_delivery_new/pickups.php" class="txt_spisok">Пункты самовывоза</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="" width="30" height="30" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_emp/" class="txt_spisok">Промсвязьбанк</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="" width="30" height="30" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/edit/m_sitemap/" class="txt_spisok">SITEMAP</a></td>
				</tr>
				<!--
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/lupa_mini.jpg" width="30" height="30" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="/func/autocomplete.php" class="txt_spisok" target="_blank">Обновить подсказки для поиска</a></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/ya_mid.jpg" width="30" height="30" alt="" /></td>
					<td align="left" ><a href="/func/price/yandex.php" class="txt_spisok" target="_blank">Обновить Яндекс YML</a><br />
						<?php //query_all('config'); ?>
						<em>последнее обновление (<?php // echo ('  ' . $all[0]['date_yml'] . ''); ?>)</em></td>
				</tr>
				<tr>
					<td align="left" class="txt_spisok"><img src="icons/ya_mid.jpg" width="30" height="30" alt="" /></td>
					<td align="left" ><a href="/edit/func/price/yml.php" class="txt_spisok" target="_blank">Обновить Яндекс YML (новый)</a><br />
				</tr>

				<tr>
					<td align="left" class="txt_spisok"><img src="icons/hit_1.jpg"  alt="" /></td>
					<td align="left">Лучшие товары <br>
						<a href="best_new.php" class="txt_spisok">Все</a> | 
						<a href="best.php" class="txt_spisok">Обработанные</a>
					</td>
				</tr>
				<tr>
					<td width="30" align="left" valign="top" class="txt_spisok"><img src="icons/blag_mini.jpg" width="30" height="29" alt="" /></td>
					<td align="left" class="txt_spisok"><a href="m_log/" class="txt_spisok">Лог удаления</a></td>
				</tr>
				-->

				<tr>
					<td align="left">&nbsp;</td>
					<td align="left">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td valign="top">&nbsp;</td>
		<td valign="top">&nbsp;</td>
		<td valign="top">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" valign="top">

		</td>
		<td valign="top">


		</td>
	</tr>
	<tr>
		<td colspan="3" valign="top">

			<table width="100%" border="0" cellpadding="1" cellspacing="0">
				<tr>
					<td align="left" valign="middle">           
					</td>
				</tr>
				<tr>
					<td align="left" valign="middle">		  
<?php include ('../tpl/admin/nav_zakaz.tpl.php'); ?>
					</td>
				</tr>
            </table>

<?php
	// Листинг
	$k = $_GET['k'];
	if ($k == '') {
		$k = 0;
	}

		
	//Какие заказы выводить
	switch ($_GET['status']) {
	 case '0' :
	 case '1' :
	 case '4' :
		$status = $_GET['status'];
		break;
	default:
		$status = 0;
	}
	// Постраничность
	$listing = 40;
	$result = mysql_query("SELECT COUNT(*) as c FROM `orders` WHERE `status` = '{$status}' ");
	$all_pay = mysql_fetch_assoc($result);
	$all_pay = $all_pay['c'];
	$num_page = $all_pay / $listing;
	$num_page = round($num_page);
	
	$previusOrderDay = 0;
	
	//Выбираем не обработанные заказы
	
	if (isset($_POST['order'])) {
		$search_str = mysql_real_escape_string($_POST['order']);
		$query = "
			SELECT
				`orders`.*,
				`order_delivery_types`.`name` as `delivery`
			FROM
				`orders`
			LEFT JOIN `order_delivery_types`
				ON `orders`.`delivery_type` = `order_delivery_types`.`id`
			WHERE
				`orders`.`id` LIKE '%{$search_str}%'
			LIMIT 1";	
	}
	else {
		$query = "
		SELECT
			`orders`.*,
			`order_delivery_types`.`name` as `delivery`,
			`order_payment_types`.`name` as `payment`,
			`order_payment_types`.`id` as `paymentId`
		FROM
			`orders`
		LEFT JOIN `order_delivery_types`
			ON `orders`.`delivery_type` = `order_delivery_types`.`id`
		LEFT JOIN `order_payment_types`
			ON `orders`.`payment_type` = `order_payment_types`.`id`
		WHERE
			`status` = '{$status}'
		ORDER BY
			`orders`.`id` DESC
		LIMIT {$k},{$listing}";
		include ('../tpl/admin/list_zakaz.tpl.php'); // Постраничность заказов 
	}
	$sel = mysql_query($query);
		
	while ($order = mysql_fetch_assoc($sel)) {
		//Подготавливаем данные для вывода
		$order['products'] = unserialize($order['products']);
		fillProductsWithInfo($order['products']);
		$order['order_price'] = unserialize($order['order_price']);
		
		//print_r($order);
		$day = date('d', $order['date']);
		if ($previusOrderDay != $day) echo ('<div align="center" class="big_menu">' . date('d.m.Y', $order['date']) . '</div><br> ');
		$previusOrderDay = $day;
		?>
		<div class="order" data-id="<?=$order['id'];?>">
		<span  class="txt_zag_zakaz"><strong><?php echo getOrderCode($order['id']); ?> </strong></span> <span class="txt_zakaz">(<?php echo date('d.m.Y H:i', $order['date']); ?>)</span>
		<?php if( $order['reservedId'] !== NULL ){ ?>
			<div class="label label-success reserved">Заказ зарезервирован<br />номер в 1С: <?= $order['reservedId']?></div>
		<?php } ?>
		<table width="900" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="500" align="left" valign="top" class="txt_zakaz">
				<div style="padding-top:10px">
					<?php switch ($order['status']) {
						case '0' : echo '<span class="label label-info">новый </span>&nbsp;&nbsp;&nbsp;&nbsp;';
							break; 
						case '1' : echo '<span class="label label-success">обработан </span>&nbsp;&nbsp;&nbsp;&nbsp;';
							break;
						case '4' : echo '<span class="label label-important">отказ </span>&nbsp;&nbsp;&nbsp;&nbsp;';
					}
					?>
			<? if ($order['status'] != '1') { ?>
				<img src="/tpl/icons/ok.gif" align="absbottom" >
				<a href="#" class="processed" style="color:#333;">Обработан</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<? } ?>
			<? if ($order['status'] != '4') { ?>
				<img src="/tpl/icons/del.gif" align="absbottom" >
				<a href="#" class="refusal" style="color:#333;">Отказ</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<? } ?>
			</div>
			
			<table>
				<tr>
					<td>
						<?php 
						foreach ($order['products'] as $p) { $info = $p['info'];?>
							<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
							  <tr>
								<td width="80" align="left" valign="top"><a target="_blank" href="<?php echo getTemplateLink($info, 'catalog'); ?>"><img src="<?=getImageWebPath('product_small').$info['id']?>.jpg"  border="0" align="left" /></a></td>
								<td align="left" valign="top"><a target="_blank" href="<?php echo getTemplateLink($info, 'catalog'); ?>" style="color:#333;"><?=$info['title']?></a> <span style="font-size: 1.3em;">(<?=$p['amount']?> шт.)</span><br />
								  <br />
								  <? if (!empty($p['info']['art'])) {?>
								  <span style="font-size: 14px;color: grey;font-family: Verdana, Geneva, sans-serif;">Артикул: <?=$p['info']['art']?></span>
								  </br></br>
								  <? } ?>
								  <span style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;"><?=$p['price_after_discount']?></span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">руб.</span>
								 </td>
								</tr>
							</table>
						<?php } ?>
						<span style="font-size: 1.2em;">Общая сумма: <?=$order['order_price']['price_after_global_discount']?> (скидка <?=$order['order_price']['discount_value']?> <?=getDiscountTypeString($order['order_price']['discount_type'])?>)</span><br />
						<br />
						<!-- начало изменение суммы и доставки -->
						<?php if($order['status'] == 0){?>
							<span>Изменить сумму
							<br/>
								<!-- <span style="color: red;"> [не работает]</span>:  -->
								<input type="text" placeholder="<?=$order['order_price']['price_after_global_discount']?>"/>
								<i class="icon-ok price" data-id= "<?=$order['id']?>" style="cursor: pointer; position: relative; top: -4px;-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></i>
							</span>
							<br/>
							<span>Изменить стоимость доставки
							<br/>
								<!-- <span style="color: red;"> [не работает]</span>:  -->
								<input type="text" placeholder="<?=$order['order_price']['delivery_price']?>"/>
								<i class="icon-ok delivery" data-id="<?=$order['id']?>" style="cursor: pointer; position: relative; top: -4px;-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></i>
							</span>
							<br />
						<?php }?>
						<!-- конец изменение суммы и доставки -->
						<?php if(((int)$order['payment_type']) === 2){ ?>
						<span style="text-decoreation: underline;">Учитывать стоимость доставки(промсвязьбанк):<span>
						<input type="checkbox" class="i55" value="yes" name="plus_dev" <?php if($order["total_price"] == 1){echo 'checked';}?> >
						<br/>
						<?php }?>
						<br/>
						<? if ($order['delivery_type'] == 1) { ?>
						<strong>Стоимость доставки:</strong> 
						<? if (!isset($order['order_price']['delivery_price']) || $order['order_price']['delivery_price'] === false) {
								echo 'Рассчитать не удалось';
							} elseif ($order['order_price']['delivery_price'] == 0 || $order['order_price']['price_after_global_discount']>=2000) {
								echo 'Бесплатно';
							} else {
								echo $order['order_price']['delivery_price'] . ' руб.';
							}
						} elseif ($order['delivery_type']  == 2) {
							echo '<strong>Базовая ставка доставки:</strong> ';
							if (!isset($order['order_price']['delivery_price']) || $order['order_price']['delivery_price'] === false) {
								echo 'Рассчитать не удалось';
							} elseif ($order['order_price']['delivery_price'] == 0 || $order['order_price']['price_after_global_discount']>=2000) {
								echo 'Бесплатно';
							} else {
								echo $order['order_price']['delivery_price'] . ' руб.';
							}
						} ?>
						<br />
						<strong>Имя:</strong> <?=$order['name']?><br />
						<strong>Тел.:</strong> <?=$order['phone']?><br />
						<strong>Доставка:</strong> <?=$order['dev_type']?><br />
						<strong>Способ оплаты:</strong> <?=$order['payment']?>
							<?if($order['paymentId']==2){?>
								<button data-id='<?=$order['id']?>' class='sendBtn' data-mail='<?=$order['email']?>'>Оправить счет для оплаты</button>
							<?}?>
						<br />
						<strong>Адрес:</strong> <?=$order['adress']?><br />
						<?php
							if ($order['email'] != '') echo ('<i class="icon-envelope"></i> ' . $order['email'] . '<br />');
						?>
						<?if ($order['msg']) {?>
						<strong>Сообщение:</strong><div class="order-msg" style="width: 300px; height: 100px; overflow: auto; word-wrap: break-word;"><?=$order['msg']?><br /></div>
						<?}?>
						<strong>Дополнительно:</strong> <?=$order['extra_information']?><br />
						<br />
					</td>
				</tr>
			</table>

			</td>
			<td align="left" valign="top" width="30">&nbsp;</td>
			<td align="left" valign="top" width="375">



			<div class="txt_zakaz" style="padding-left:20px; padding-right:10px; padding-bottom:10px;  padding-top:10px;  background-color:#fdf6f0">
			<table class="table table-condensed order-log">
			<?php
			$s = mysql_query("SELECT * FROM `orders_log` WHERE `order_id`='{$order['id']}' AND `action` <> '0' ORDER BY `date` ASC");
			$lastActionTime = $order['date'];
			$actionsCount = mysql_num_rows($s);

			while ($row = mysql_fetch_assoc($s)) {
				$actionTime = strtotime($row['date']);
				
				$dateDiff = $actionTime - $lastActionTime;
				$dH = floor($dateDiff / 3600);
				$dM = ($dateDiff - $dH * 3600) / 60;
				$delta = $dH . ' ч. ' . round($dM) . ' м.';
				
				$lastActionTime = $actionTime;

				$month = $months[date('n', $actionTime) - 1];
				$day_of_week = $days[date('D', $actionTime)];
				$day = date('d', $actionTime);
				$year = date('Y', $actionTime);
				$time = date('H:i', $actionTime);
				?>
					<tr>
						<td width='100'><b><?=$time?></b>, <?=$day?> <?=$month?><br /><?=$day_of_week?> </td>
						<td><img src='/tpl/icons/comment.jpg' border='0' align='absbottom' ></td>
						<td><?=$row['descr']?></td>
						<td width="75px"><?=$delta?></td>
					</tr>
				<?php
			}
			$keyw = '';
			if ($actionsCount > 1) {
					$dd = $lastActionTime - $order['date'];
					$dh = floor($dd / 3600);
					$dm = ($dd - $dh * 3600) / 60;
					$delta = $dh . ' ч. ' . round($dm) . ' м.';
					echo "<tr><td colspan='3'>&nbsp;</td><td><strong>" . $delta . "</strong></td></tr>";
				}
				//Откуда пришел
				$hrefer = explode("/", $order['h']);
				$site = "<img src=http://favicon.yandex.net/favicon/" . $hrefer[2] . " align=absmiddle  height=15 width=15>";
				if ($hrefer[2] == "yandex.ru" or $hrefer[2] == "www.yandex.ru") {
					$site = "<img src=/tpl/icons/ya_mini.jpg align=absmiddle height=15 width=15 >";
					$hrefer2 = explode("text=", $hrefer[3]);
					$keyw = $hrefer2[1];
					$keyw = urldecode($keyw);
					// $keyw = iconv('UTF-8', 'Windows-1251', $keyw);
					$keyw = $keyw;
					$hrefer3 = explode("&", $keyw);
					$keyw = $hrefer3[0];

					//echo "<pre>"; print_r($hrefer2); echo "</pre>";
				}
				if ($hrefer[2] == "www.google.ru" or $hrefer[2] == "google.ru" or $hrefer[2] == "www.google.by" or $hrefer[2] == "www.google.com") {
					$site = "<img src=/tpl/icons/goog_mini.jpg align=absmiddle  height=15 width=15 >";
					$hrefer2 = explode("q=", $hrefer[3]);

					$keyw = $hrefer2[1];
					$keyw = urldecode($keyw);
					// $keyw = iconv('UTF-8', 'Windows-1251', $keyw);
					$keyw =  $keyw;
					$hrefer3 = explode("&", $keyw);
					$keyw = $hrefer3[0];

					//$keyw = str_replace("&hl", "", $keyw );
				}
				//echo "<pre>"; print_r($hrefer); echo "</pre>";
			?>
			</table>
				<img src="/tpl/icons/comment.jpg" border="0" align="absbottom" >
				<a href="javascript:void(0)" class="comment-button" style="color:#333;">Комментировать</a>
				<div style="display: none;" class="comment-form">
					<textarea rows="3" cols="20" class="comment-field" style="width: 300px;"></textarea><br />
					<input type="button" name="s" class="send-button btn"  value="Написать" />
				</div>
				<br /><br />
			</div>

			<br>
				<table width="270" border="0" cellspacing="10" cellpadding="0">
				<?php
					//Пока не работает
					if ($order['status'] == 1 AND false) {
						if (($order['remind'] != 1) && ($order['email'] != '')) {
							echo('<tr><td><a class="btn btn-warning reminder-link" pay_id="' . $order['id'] . '"><i class="icon-warning-sign"></i> Запросить отзыв</a></td></tr>');
						} elseif ($order['email'] == '') {
							//	Мыла нет, нет и отзыва
						} else {
							echo('<tr><td><a class="btn btn-success disabled"><i class="icon-ok"></i> Отзыв запрошен</a></td></tr>');
						}
					}
				?>
					<tr>
					  <td class="txt_zakaz_small" ><?=$site?> <?=$keyw?> <a href="<?=$order['h']?>" style="color:#333;" target="_blank">>>></a></td>
					</tr>
				<?php if($order['partner_id'] != 0) {
					echo('<tr>
						<td class="txt_zakaz_small">Заказ по партнерской ссылке.</td>
					</tr>');
				}
				?>
				</table>
			</td>
		  </tr>
		</table>
		<hr style="margin: 50px 0;"/>
		</div>
		<?php
	}
?> 
		</td>
	</tr>
	<? if (!isset($_POST['order'])) {?>
	<tr>
		<td colspan="3" valign="top" >
			<?php include ('../tpl/admin/list_zakaz.tpl.php'); // Постраничность заказов   ?>
		</td>
	</tr>
	<? } ?>
	<tr>
		<td colspan="3" valign="top" class="big_menu">&nbsp;</td>
	</tr>
</table>
</td>
</tr>
<tr>
    <td><?php
include ('down.php');
?></td>
</tr>
</table>
<script>
	$(document).ready( function() {
		//Аякс на выставление статуса обработан
		$('.processed').bind('click', function () {
			var order = $(this).closest('.order');
			
			$.ajax({
				url : '/edit/m_zakaz/ajax.php',
				data : {"method" : "setstatus", "order_id" : order.data('id'), status : "<?=STATUS_PROCESSED;?>"},
				type : "POST", 
				//dataType : 
				beforeSend : function () {
					order.css('opacity', .5);
				},
				success : function (data, textStatus, jqXHR) {
					order.fadeOut('slow', function () {
						$(this).remove();
					});
				}
			});
			return false;
		});
		$('.sendBtn').bind('click', function () {
			var delivery = $(this).parent().find("input[type=checkbox]").prop("checked");
			$.ajax({
				url : '/edit/sendInvoice.php',
				data : 'oid='+$(this).data('id')+'&omail='+$(this).data('mail')+'&delivery='+ delivery,
				type : "POST", 
				beforeSend : function () {
					
				},
				success : function (data, textStatus, jqXHR) {
					// alert('Письмо отправлено');
					alert(data);
				}
			});
		});
		//Аякс на выставление статуса отказ
		$('.refusal').bind('click', function () {
			var order = $(this).closest('.order');
			
			$.ajax({
				url : '/edit/m_zakaz/ajax.php',
				data : {"method" : "setstatus", "order_id" : order.data('id'), status : "<?=STATUS_REFUSAL;?>"},
				type : "POST", 
				//dataType : 
				beforeSend : function () {
					order.css('opacity', .5);
				},
				success : function (data, textStatus, jqXHR) {
					order.fadeOut('slow', function () {
						$(this).remove();
					});
				}
			});
			return false;
		});
		
		//Отправка сообщений
		$('.comment-button').bind('click', function() {
			$(this).next('.comment-form').slideToggle();
		});
		
		$('.send-button').bind('click', function () {
			var _this = this;
			var order = $(this).closest('.order');
			var orderId = order.data('id');
			
			var field = $(this).closest('.comment-form').find('.comment-field');
			var button = $(this);
			
			var comment = field.val();
			$.ajax({
				url : '/edit/m_zakaz/ajax.php',
				type : "POST",
				data : {"method" : "add_comment", "order_id" : orderId, "comment" : comment},
				dataType : 'json',
				beforeSend : function () {
					field.attr('disabled', 'disabled');
					button.attr('disabled', 'disabled');
				},
				success : function (data, textStatus, jqXHR) {
					field.removeAttr('disabled');
					button.removeAttr('disabled');
					
					$(_this).closest('.order').find('.order-log').append($('<tr></tr>').html(
					"<td width='100'>" + data.date + " </td>" +
					"<td><img src='/tpl/icons/comment.jpg' border='0' align='absbottom' ></td>" +
					"<td>" + comment + "</td>" +
					"<td>" + data.delta + "</td>"
					));
					field.val('');
					$(_this).closest('.comment-form').slideToggle();
				}
			});
		});
	})
</script>
</body>
</html>
