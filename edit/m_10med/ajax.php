<?php 
header('Content-Type: text/html; charset=utf-8');
// ВАЖнЫЕ ПЕРЕМЕННЫЕ;
$mysql_charset = 'utf8';

/*
 ini_set('error_reporting', E_ALL);
 ini_set('display_errors', 1);
 ini_set('display_startup_errors', 1);
*/

require_once('../../connect.php');
mysql_query("SET CHARACTER SET {$mysql_charset}");
mysql_query("SET NAMES {$mysql_charset}");
require_once('functions.php');

$myapi10med = new api10med("conf.ini");
$mysql_charset = $myapi10med->getconfig('sql_charset');


require_once('../../connect.php');
require_once('functions.php');
mysql_query("SET CHARACTER SET {$mysql_charset}");
mysql_query("SET NAMES {$mysql_charset}");


switch ($_REQUEST['method']) {
	case 'autosync':
		$query = "	SELECT id, linked_with_10med
					FROM catalog
					WHERE linked_with_10med > 0";
		$result = mysql_query($query);
		$counter = 0;
		$linksData = array();
		while ($row = mysql_fetch_assoc($result)) {
			$linksData[] = array('siteId' => $row['id'], 'id10med' => $row['linked_with_10med']);	
		}
		// Отправляем запрос на слинковку
		$response = api10med::createLinks($linksData);
		echo $response;
		break;
	
	case 'autounlink':
		$response = $myapi10med->clearLinks();
		echo $response;
		break;


	case 'getItemData': 
		$id = $_REQUEST['item_id'];
		$query = "	SELECT id, title, art, chpu, price_after_discount AS price, linked_with_10med, nomencl_10med AS key1c
					FROM catalog 
					WHERE id = {$id} LIMIT 1";
		$result = mysql_query($query);
		$row = mysql_fetch_assoc($result);
		echo json_encode($row);
		break;

	case 'setItemData':
		$id = $_REQUEST['item_id'];
		$art = $_REQUEST['art'];
		$id10med = empty($_REQUEST['id10med'])? 0 : $_REQUEST['id10med'];
		$nomencl = $_REQUEST['nomencl']; 
		$query = "	UPDATE catalog 
					SET art = '{$art}', linked_with_10med = {$id10med}, nomencl_10med='{$nomencl}'
					WHERE id = {$id}";
		if ($result = mysql_query($query)) {
			echo json_encode(array('error' => false, 'message' => 'обновление успешно завершено!'));
		} else {
			echo json_encode(array('error' => true, 'message' => "Ошибка в запросе: ".$query));
		}
		break;	
	case 'search':
		$search_query = $_REQUEST['search_text'];
		$response = $myapi10med->search($search_query, true);
		echo json_encode(array(
			'error' => false,
			'suggestion' => json_decode($response),
		));

		break;

	case 'link':
		//ID товара в магазине
		$localId = $_REQUEST['local_id'];
		//ID товара на 10мед
		$remoteId = $_REQUEST['remote_id'];
		
		if (!is_numeric($localId) || !is_numeric($remoteId)) {
			echo json_encode(array('error' => true, 'error_code' => 0));
			break;
		}
		
		$params = array(
			'local_id' => $localId ,
			'remote_id' => $remoteId
		);
		
		//Отправляем запрос на слинковку
		$response = json_decode($myapi10med->link($localId, $remoteId), true);
		if ($response['code'] == 200) {
			if (!isset($_REQUEST['relink']) and ($localId > 0)) {
				mysql_query("UPDATE `catalog` SET `linked_with_10med` = '{$response['linkedWith']}' WHERE `id` = '{$localId}' LIMIT 1");
				$message = 'Слинковка прошла успешно!';
			} else if(isset($_REQUEST['relink']) and ($localId > 0)){
				$message = 'Товар перелинкован';
			} else if ($localId == 0) {
				mysql_query("UPDATE `catalog` SET `linked_with_10med` = 0 WHERE `linked_with_10med` = '{$remoteId}' LIMIT 1");
				$message = 'Связь успешно удалена!';
			}
			$response['error'] = false;
			$response['error_code'] = 0;
		} else if ($response['code'] == 23000) {
			$response['error'] = true;
			$response['error_code'] = 23000;
			$message = 'Товар уже зарегистрирован в 10мед под другим ID';
		} else {
			$response['error'] = true;
			$message = 'неверный id10med, проверьте правильность заполненых данных элемента';
		}
		echo json_encode(array('error' => $response['error'], 'error_code' => $response['error_code'], 'message' => $message));
		break;

	case 'showstatus':
		$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
		$message =  "sfsdfsdwegewtwe123523523: " . $id;

		$query = "	SELECT linked_with_10med
					FROM catalog 
					WHERE id = {$id} LIMIT 1";
		$result = mysql_query($query);
		$row = mysql_fetch_row($result);
		
		$response = $myapi10med->product($row);
		var_dump(json_decode($response));


		echo json_encode(array('message' => $message, 'id10med' => $row, /*'response' => $response*/));	

		break;

	case 'get10medProductData':
		$key10med = isset($_REQUEST['key10med']) ? $_REQUEST['key10med'] : 0;
		$id = isset($_REQUEST['localId']) ? $_REQUEST['localId'] : 0;
		$nomencl = null;
		$error = true;
		if ($id > 0) {
			$response = $myapi10med->product($key10med);
			$result = json_decode($response);
			if (isset($result->key1c)) {
				$nomencl = $result->key1c;
				$query = "UPDATE catalog SET nomencl_10med = '{$nomencl}' WHERE id = {$id} LIMIT 1";
				if (mysql_query($query)) {
					$message = 'done';
					$error = false;
				} else {
					$message = 'Ошибка обновления номенклатуры!';
				}
			} else {
				$message = 'данные номенклатуры не найдены!';
			}

		} else {
			$message = 'не верный локальный id';
		}

		echo json_encode(array('key1c' => $nomencl, 'error' => $error, 'message' => $message));
		break;
		
		case 'importlinks':
		$response = $myapi10med->get10medLinks();
		$response = json_decode($response);
		$count = 0;
		foreach ($response as $value) {
			$idLocal = $response['remoteProductId'];
			$id10med = $response['localProductId'];
			mysql_query("UPDATE catalog SET linked_with_10med = {$id10med} WHERE id = {$idLocal}");
			$count = $count + mysql_affected_rows();
		}
		echo json_encode(array('count' => $count));
		break;

	default:
		# code...
		break;
}
 ?>