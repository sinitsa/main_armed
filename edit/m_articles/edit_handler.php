<?
$errors = array();

if ($articleId > 0) {
	file_put_contents('files.txt', var_export($_FILES, true));
	$data = array();
	$data['chpu'] = mysql_real_escape_string($_REQUEST['chpu']);
	$data['title'] = mysql_real_escape_string($_REQUEST['title']);
	$data['seo_title'] = mysql_real_escape_string($_REQUEST['seo_title']);
	$data['cat_id'] = mysql_real_escape_string($_REQUEST['cat_id']);
	$data['text'] = mysql_real_escape_string($_REQUEST['text']);
	$data['short'] = mysql_real_escape_string($_REQUEST['short']);
	$data['show_in_list'] = isset($_REQUEST['show_in_list']) ? 1 : 0 ;

	if (strlen($data['title']) < 2) $errors[] = 'Не введен заголовок';
	
	if (count($errors) <= 0) {
		mysql_query("
			UPDATE
				`articles`
			SET
				".getSetString($data)."
			WHERE
				`id` = '{$articleId}'
			LIMIT 1
		");
		if (mysql_affected_rows() < 0) {
			$errors[] = 'Ошибка изменения в базе';
		}
		if ($_FILES['image']) {
			$size = getConfigImageSize('article');
			$iWidthDest = $size['width'];
			$iHeightDest = $size['height'];
			$iQuality = getConfigImageQuality('article');
			file_put_contents('pth.txt', var_export(getImagePath('article'), true));
			$res = uploadAndResize($_FILES['image'], getImagePath('article'), $articleId, $iWidthDest, $iHeightDest, 1, $iQuality);
			file_put_contents('res.txt', var_export($res, true));
		}		
		//redirect('/edit/m_catalog/list.php?id=' . $catId);
	}
	
}