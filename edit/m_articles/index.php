<?php include ("../connect.php");?>
<?php include ("../../func/core.php");?>
<?php 
	$cssOl = true;
	include ("../up.php"); 
?>
<?php 
	$articles = getAllArticles();
?>
<script>
$(function() {
	$('#art_chck').change(function() {		
		$('input[type="checkbox"].art_box').attr('checked', this.checked).trigger('change');
	});
});

	//Удалить выбранные статьи
	function deleteSelectedArticles() {
		if (confirm("Вы уверены, что хотите удалить выбранные статьи?")) {
			var ids = [];
			$('.element .art_box:checked').each(function (index, element) {
				var tr = $(element).closest('.element');
				var id = $(tr).data('id');				
				ids.push(id);
			});
			console.log(ids);
			$.ajax({
				url: 'ajax.php?method=deletearticle',
				type : 'POST',
				dataType : 'json',
				data : {"ids" : ids},
				//beforeSend : function () {},
				success : function() {
					console.log('success!!');
					$('.element .art_box:checked').each(function (index, element) {	
						var tr = $(element).closest('.element');
						$(tr).fadeOut('slow', function () {
							$(tr).remove();
						});
					});	
				},
				//error : function (jqXHR, textStatus, errorThrown) {}
			});
		}
	}	
</script>
<div class="limain2" style="margin-left: 100px;"><a href="javascript:void(0);" onclick="deleteSelectedArticles();">Удалить выделенные статьи</a></div> 
 <table width="90%" border="0" align="center" class="txt">
<tr>
              <td width="10">&nbsp;</td>
              <td></td>
        </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
      </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td>

<table width="100%" border="0" align="center" cellPadding="0" cellSpacing="0"  class="table" id="table-1">
	<thead>   
	<tr class="well">
		<td width="10"><input id="art_chck" type="checkbox"/></td>
		<td width="300">Название статьи</td>
		<td width="100">&nbsp;</td>
		<td width="30"></td>
	</tr> 
	</thead> 
	<tbody>
		<?php foreach ($articles as $article) { ?>
		<tr class="element" data-id="<?=$article['id']?>">
			<td width="10"><input class="art_box" type="checkbox" /></td>
			<td width="300"><a href="/edit/m_articles/edit.php?id=<?=$article['id']?>"><?=$article['title']?></a></td>
			<td width="100">&nbsp;</td>
			<td width="30"></td>			
		</tr>
		<?php } ?>
	</tbody>
</table>

</td>
        </tr>
          </table>

<div class="modal hide fade" id="twitter-modal">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Modal header</h3>
  </div>
  <div class="modal-body"></div>
  <div class="modal-footer">
    <a href="#" class="btn">Close</a>
    <a href="#" class="btn btn-primary">Save changes</a>
  </div>
</div>

<?php include ("../down.php");	?>