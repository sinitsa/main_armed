<?php include ("../connect.php");?>
<?php
	include ("../../func/core.php");
	
	//Массив для замены знаков равенства словами
	$replace = array(
		'[>]' => 'больше',
		'[>=]' => 'больше либо равно',
		'[<]' => 'меньше',
		'[<=]' => 'меньше либо равно',
		'[=]' => 'равно',
		'[no]' => 'не важно',
		
		'[percent]' => '%',
		'[value]' => 'руб'
	);
	
	//Добавление правила
	if (isset($_POST['add_rule'])) {
		$productsCondition = mysql_real_escape_string($_POST['products_condition']);
		$productsCount = mysql_real_escape_string($_POST['products_count']);
		$priceCondition = mysql_real_escape_string($_POST['price_condition']);
		$priceSumm = mysql_real_escape_string($_POST['price_summ']);
		$discountType = mysql_real_escape_string($_POST['discount_type']);
		$discountValue = mysql_real_escape_string($_POST['discount_value']);
		
		mysql_query("
			INSERT INTO
				`order_discount_rules`
			SET
				`products_condition` = '{$productsCondition}',
				`products_count` = '{$productsCount}',
				`price_condition` = '{$priceCondition}',
				`price_summ` = '{$priceSumm}',
				`discount_type` = '{$discountType}',
				`discount_value` = '{$discountValue}'
		");
	}
	//Удаление правила
	if (isset($_GET['del']) && is_numeric($_GET['del'])) {
		mysql_query("DELETE FROM `order_discount_rules` WHERE `id` = '{$_GET['del']}' LIMIT 1");
	}
	
	//Сортировка приоритетов
	if (isset($_POST['save_order'])) {
		if (isset($_POST['order'])) {
			foreach ($_POST['order'] as $id => $order) {
				if (is_numeric($id) && $id > 0 && is_numeric($order)) {
					mysql_query("
						UPDATE
							`order_discount_rules`
						SET
							`order` = '{$order}'
						WHERE
							`id` = '{$id}'
						LIMIT 1
					");
				}
			}
			$orderSave = '<span style="color: green;">Порядок приоритета сохранен</span>';
		}
	}
	
	
	//Выберем список правил
	$rules = array();
	$sel = mysql_query("SELECT * FROM `order_discount_rules` ORDER BY `order` DESC");
	while ($row = mysql_fetch_assoc($sel)) {
		$rules[] = $row;
	}

include ("../up.php");
?>
<script type="text/javascript" src="/js/jquery-ui-1.8.13.js"></script>
<script>
	$(function() {
		$("#order-rules").sortable({
			connectWith: 'div',
			tolerance : 'pointer',
			update: function(event, ui) {
				var orderMax = $("#order-rules div").length;
				$('#order-input-field').empty();
				
				$("#order-rules div").each(function (index, element) {
					$('#order-input-field').append(
						$('<input />').
						attr({
							"type" : "hidden",
							"name" : "order[" + $(element).data('id') + "]",
							"value" : orderMax--
						})
					);
				});
			}
		});
		$("#order-rules").disableSelection();
	});
</script>
<style>
	#order-rules {
		width: 900px;
	}
	#order-rules div {
		padding: 3px 2px;
		background: #fff;
		cursor: move;
		margin: 1px 0;
	}
	#order-rules div:hover {
		background: #eee;
	}
</style>
 <table width="90%" border="0" align="center" class="txt">
            <tr>
              <td width="10">&nbsp;</td>
              <td>
				<h4>Скидки</h4>
				<h4>Новое правильно для скидок</h4>
				<form action="" method="post">
					Если количество товаров в корзине 
						<select name="products_condition" style="width: 145px;">
							<option value=">">больше</option>
							<option value=">=">больше либо равно</option>
							<option value="<">меньше</option>
							<option value="<=">меньше либо равно</option>
							<option value="=">равно</option>
							<option value="no">не важно</option>
						</select>
						<input type="text" name="products_count" value="0" size="3" style="width: 25px;" maxlength="5"/>
					и сумма заказа 
						<select name="price_condition" style="width: 145px;">
							<option value=">">больше</option>
							<option value=">=">больше либо равно</option>
							<option value="<">меньше</option>
							<option value="<=">меньше либо равно</option>
							<option value="=">равно</option>
							<option value="no">не важно</option>
						</select>
						<input type="text" name="price_summ" value="0" size="4" style="width: 45px;" maxlength="7" /> руб,
					то скидка составит
						<input type="text" name="discount_value" value="0" size="4" style="width: 45px;" maxlength="7"/>
						<select name="discount_type" style="width: 65px;">
							<option value="percent">%</option>
							<option value="value">руб</option>
						</select>
					<br />
					<input type="submit" name="add_rule" value="Добавить правило" class="btn" />
				</form>
				
				<h4>Список правил</h4>
				<div>
					Чем выше правило в списке, тем выше его приоритет. Скидка выставляется при первом совпадении с условиями правила
				</div>
				<div>&nbsp;</div>
				<form action="" method="post">
					<div id="order-rules">
						<?php foreach ($rules as $rule) {
							echo strtr('<div data-id="'.$rule['id'].'">&mdash; Если количество товаров <strong>['.$rule['products_condition'].'] '.$rule['products_count'].'</strong> и сумма заказа <strong>['.$rule['price_condition'].'] '.$rule['price_summ'].'</strong> руб, то скидка составит <strong>'.$rule['discount_value'].' ['.$rule['discount_type'].']</strong>', $replace).' <a href="?del='.$rule['id'].'">Удалить</a></div>';
						} ?>
					</div>
					<div id="order-input-field"></div>
					<div>
						<input type="submit" name="save_order" value="Сохранить приоритет правил" class="btn" />
					</div>
				</form>
				<?php echo $orderSave;?>
			  </td>
        </tr>
</table>
<?php include ("../down.php"); ?>