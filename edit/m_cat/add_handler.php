<?php


if (isset($_POST['add'])) {
	//Перетаскиваем переменные
	$data = array();
	$errors = array();
	
	//используем цикл вместо эксцепшенов
	do {
		if (strlen($_POST['title']) < 2) 
			$errors[] = 'Не введено название категории';
		if (strlen($_POST['chpu']) < 2)
			$errors[] = 'Не задан чпу для категории';
		if (getCatInfo($_POST['chpu']) || getTagInfo($_POST['chpu']))
			$errors[] = 'Введенный ЧПУ уже занят. Придумайте другой.';
		if (count($errors) > 0) break;
		
		
		$data['title'] = mysql_real_escape_string($_POST['title']);
		$data['chpu'] = mysql_real_escape_string($_POST['chpu']);
		$data['pod'] = 0;
		$data['seo_title'] = mysql_real_escape_string($_POST['seo_title']);
		$data['seo_des'] = mysql_real_escape_string($_POST['seo_des']);
		$data['seo_key'] = mysql_real_escape_string($_POST['seo_key']);
		$data['seo_title_text'] = mysql_real_escape_string($_POST['seo_title_text']);
		$data['seo_text'] = mysql_real_escape_string($_POST['seo_text']);
		$data['rang'] = 0;
		$data['compensation'] = ($_POST['compensation'] == 1) ? 1 : 0; 
		$data['hidelink'] = ($_POST['hidelink'] == 1) ? 1 : 0; 
		 
		//Запрос
		//подготавливаем запрос
		$setString = getSetString($data);
		
		mysql_query("
			INSERT INTO
				`cat`
			SET
				{$setString}
		");
		echo mysql_error();

		$catId = mysql_insert_id();

		if (!$catId) {
			mysql_error();
			die();
		}
		
		$paramsIds = array();
		foreach ($_POST['param'] as $key => $value) {
			$paramsIds[] = $value;
		}

		if (count($paramsIds)) {
			foreach ($paramsIds as $pid) {
				mysql_query("
					INSERT INTO
						`params_cat_links`
					SET
						`cat_id` = '{$catId}',
						`param_id` = '{$pid}',
						`enable` = '1'
				");
			}
		}
		//Добавляем картинки
		
		$dir = getImagePath('cats_menu');
		// Загружаем картинку для вывода меню
		if(isset($_FILES["upfile"])) { 
			$sFileDest = $catId;
			$iWidthDest = null;
			$iHeightDest = null;
			$iResizeMode = 1;
			uploadAndResize($_FILES["upfile"], $dir, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode);
		}

		// Загружаем картигку для вывода в категории
		$dir = getImagePath('cats');
		if(isset($_FILES["upfile2"])) { 
			$sFileDest = $catId;
			$iWidthDest = null;
			$iHeightDest = null;
			$iResizeMode = 1;
			uploadAndResize($_FILES["upfile2"], $dir, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode);
		}
		
		//Готово %)
		header("Location: /edit/m_mcat/");
		die();
	} while (false);
}