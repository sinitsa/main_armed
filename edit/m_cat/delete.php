<?php
include ("../connect.php");
include ('../../func/core.php');

if(isset($_GET['code']) && !empty($_GET['code']) && isset($_GET['id'])) {
	if (!is_numeric($_GET['id'])) die();
	$id = $_GET['id'];
	$code = mysql_real_escape_string($_GET['code']);
	$key = 'cat_'.$id;
	
	$code = fetchOne("SELECT `code` FROM `confirm_delete` WHERE `key` = '{$key}'");
	
	if($code == $_GET['code']) {
		//#TODO
		$catInfo = getCatInfo($id);
		if ($catInfo == false) die('Категория не существует');
		
		//СНести категорию в базе и все картинке к ней
	
		mysql_query("DELETE FROM `cat` WHERE `id` = '{$catInfo['id']}'");
		deleteCatImages($id);
		
		//Снести товары в базе, картинки к ним, слинковки с параметрами, слинковки с тегами
		$sel = mysql_query("SELECT `id` FROM `catalog` WHERE `cat` = '{$catInfo['id']}'");
		while ($res = mysql_fetch_assoc($sel)) {
			deleteProduct($res['id']);
		}
		mysql_query("DELETE FROM `params_cat_links` WHERE `cat_id` = '{$id}'");
		mysql_query("DELETE FROM `tags` WHERE `cat_id` = '{$id}'");
		
		//Выписать пункт из меню
		mysql_query("DELETE FROM `menuleft` WHERE `cat_id` = '{$id}'");
		//Снести ключ удаления
		mysql_query("DELETE FROM `confirm_delete` WHERE `key` = '{$key}'");
	} else {
		die('Ключ доступа не подошел');
	}
} else {
	die ('Не указан ключ или id категории');
}

header("Location: /edit/ok.php");
?>