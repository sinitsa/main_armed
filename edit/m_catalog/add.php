<?php
include ("../connect.php");
include ("../../func/core.php");

$catId = isset($_GET['cat_id']) && is_numeric($_GET['cat_id']) ? $_GET['cat_id'] : 0 ;

include("sql_add.php");
?>
<?php 
$cssOl = true;
include ("../up.php");
?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
<script type="text/javascript">
var errorsKeeper = {};

$(function(){
	stylizeCheckbox();
	$('#title').bind('change', function() {
		var title = $('#title');
		var chpu = $('#chpu');
		var price = $('#price');
		
		$.post("/ajax/autochpu.php", {
			"set_chpu" : 1,
			"set_string" : document.getElementById('title').value
		}, function(data) {
			//alert(data);
			$('#chpu').val(data);
		});

		//chpu.value = input.value.translit().toLowerCase();
		$('#seo-title').val(title.val() + ' купить на Армед-Маркет.ру');
		$('#seo-des, #seo-key').val('Официальный сайт Армед Товары для медицины и инвалидов ' + title.val() + ' Доставка и оплата, Мск, СПБ Россия ' + price.val() + 'руб. Отзывы Видео, Фото');
		$('#chpu').trigger('change');
		
	});
	//Автопросцет % и значений скиндки в форме
	$('#price').bind('change', function(){
		$('#discount_value').trigger('keyup');
	})
	$('#discount_percent').bind('keyup', function() {
		var price = $('#price').val();
		var percent = $(this).val();
		
		if (price == '') price = 0;
		$('#discount_value').val(Math.round(percent * price / 100));
	});
	$('#discount_value').bind('keyup', function() {
		var price = $('#price').val();
		var value = $(this).val();

		if (price == '') price = 0;
		$('#discount_percent').val(Math.round(value * 100 / price));
	});
	
	$('#title').bind('change', function () {
		var title = $(this).val();
		
		if (title.length <= 0) return;
		
		$.ajax({
				url: 'ajax.php?method=checktitle',
				type : 'POST',
				dataType : 'json',
				data : {"title" : title},
				//beforeSend : function () {},
				success : function (data, textStatus, jqXHR) {
					if (data.is_double) {
						$('#title-warning-message').show();
						//errorsKeeper.title_double = 1;
						formEnable(false);
					} else {
						$('#title-warning-message').hide();
						//errorsKeeper.title_double = 0;
						formEnable(true);
					}
				}
				//error : function (jqXHR, textStatus, errorThrown) {}
		});
	});
	$('#chpu').bind('change', function () {
		var chpu = $(this).val();
		
		if (chpu.length <= 0) return;
		
		$.ajax({
				url: 'ajax.php?method=checkchpu',
				type : 'POST',
				dataType : 'json',
				data : {"chpu" : chpu},
				//beforeSend : function () {},
				success : function (data, textStatus, jqXHR) {
					if (data.is_double) {
						$('#chpu-warning-message').show();
						errorsKeeper.chpu_double = 1;
						formEnable(false);
					} else {
						$('#chpu-warning-message').hide();
						errorsKeeper.chpu_double = 0;
						formEnable(true);
					}
				}
				//error : function (jqXHR, textStatus, errorThrown) {}
		});
	});
	
	
});
	function formEnable(bool) {
		if (bool) {
			for (var i in errorsKeeper) {
				if (errorsKeeper[i] == 1) return;
			}
			$('#add-button').removeAttr('disabled');
		} else {
			$('#add-button').attr('disabled', 'disabled');
		}
	}
	//Управление ярлычками new, sale и тд
	function setButton(id, onOff) {
		if (onOff == 'on') {
			$('#' + id + ' .off').hide();
			$('#' + id + ' .on').show();
			$('#' + id + '-checkbox').attr('checked', 'checked');
		} else {
			$('#' + id + ' .off').show();
			$('#' + id + ' .on').hide();
			$('#' + id + '-checkbox').removeAttr('checked');
		}
	}
</script>
<div class="ol" style="width:100%; height:100%; position:absolute; z-index:-2;" align="center">
<div style="width:100%; height:100%; max-width:1270px; min-width:1000px;" align="center">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="99%" align="center" valign="top">
	<table width="96%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="200" align="left" valign="top">
			<div style="margin-top:-10px;"></div>
			<?php foreach (getMenu() as $item) { ?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0"  style="margin-bottom:5px; margin-top:12px;">
					  <tr>
						<td width="14"><img src="/img/mainl.gif" width="14" height="31"></td>
						<td class="lmainrast">
						  <div class="txt_banner" style="margin-top:-3px;"><?php echo $item['title']; ?></div>
						  </td>
						<td width="14"><img src="/img/mainr.gif" width="14" height="31" /></td>
						</tr>
				</table>
				<?php if (count($item['submenu']) > 0) { ?>
					<?php foreach ($item['submenu'] as $submenu) { ?>
						<div class="limain"><a href="/edit/m_catalog/list.php?id=<?php echo $submenu['cat_id']; ?>"><?php echo $submenu['title']; ?> </a></div> 
					<?php } ?>
				<?php } ?>
			<?php }	?>
        </td>
        <td align="left" valign="top">
			<form action="" method="post" enctype="multipart/form-data">
				<div style="margin-left: 20px;">
					<h2>Добавление товара</h2>
					<h2>Название:</h2>
					<div>
						<input id="title" type="text" name="title" class="p_name" value="" />
						<div id="title-warning-message" class="error" style="display: none;">Товар с таким названием уже существует</div>
					</div>
					<h2>Чпу:</h2>
					<div>
						<input id="chpu" type="text" name="chpu" class="p_name" value="" />
						<div id="chpu-warning-message" class="error" style="display: none;">Товар с таким ЧПУ уже существует</div>
					</div>
					
					<div style="width:200px; margin: 10px 0;">
						<input type="checkbox" name="fake_in_stock" value="1"><span style="margin-left: 5px;">Всегда в наличии</span>
					</div>
					
					<div>
						<input type="hidden" name="cat_id" value="<?=$catId;?>" />
					</div>
					<h2>Основное фото:</h2>
					<div>
						<input type="file" name="main_image" class="" />
					</div>
					<div class="info-text">(Авторесайз: вкл; Размеры превью: <?php echo Config::get('image_size.product_preview'); ?>)</div>
					<div>
						<input type="text" name="price" id="price" class="txtpricebig" value="" />
						<span class="txtrub">руб.</span>
					</div>
					<div>
						<input type="text" name="article" class="txtpricesmfr" value="" /> <span class="txtrub">арт.</span>
					</div>
					<div>
						<input type="text" name="warranty" class="txtpricesmfr" value="0" /> <span class="txtrub">Гарантия, лет</span>
					</div>
					
					<h2>Описание:</h2>
					<div>
						<textarea name="desc" cols="70" rows="7" style="width: 400px;"><?php echo $product['des']; ?></textarea>
						<script type="text/javascript">
							var editor = CKEDITOR.replace( 'desc' );
							CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
						</script>
					</div>
					<div style="margin-top:20px;">
						<h2>Видео:</h2>
						<textarea name="iframe" class="p_video"><?echo schars($product['iframe']);?></textarea>
						<h2>Заголовок видео:</h2>
						<input  name="iframe_des" class="p_video_des" value="<?=$product['iframe_des'];?>"/>
					</div>
					<h2>Короткое описание:</h2>
					<div>
						<textarea name="short_des" cols="70" rows="7" style="width: 400px;"><?php echo $product['short_des']; ?></textarea>
						<script type="text/javascript">
							var editor = CKEDITOR.replace( 'short_des' );
							CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
						</script>
					</div>

							<div style="margin-top:20px;">
		<h2>Описание (на вкладке):</h2>
		<div>
			<textarea name="under_price_des" cols="70" rows="7" style="width: 400px;"><?php echo $product['under_price_des']; ?></textarea>
			<script type="text/javascript">
				var editor = CKEDITOR.replace( 'under_price_des' );
				CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
			</script>
		</div>
	</div>

		<div style="margin-top:20px;">
		<h2>Комплектация:</h2>
		<div>
			<textarea name="adv_des" cols="70" rows="7" style="width: 400px;"><?php echo $product['adv_des']; ?></textarea>
			<script type="text/javascript">
				var editor = CKEDITOR.replace( 'adv_des' );
				CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
			</script>
		</div>
	</div>
					<h2>Технические характеристики:</h2>
					<div>
						<textarea name="like_desc" cols="70" rows="7" style="width: 400px;"><?php echo $product['like_desc']; ?></textarea>
						<script type="text/javascript">
							var editor = CKEDITOR.replace( 'like_desc' );
							CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
						</script>
					</div>
					<div>
					<div style="margin-top:30px;">
						<?php
							$params = getCatParams($catId);
							foreach($params as $param) {
						?>
						<div style="clear: both;"></div>
						<h2><?php echo $param['title']; ?>:</h2>
						<div class="params-field" style="margin-top:8px;">
							<?php
							if ($param['type'] == 'set') {
								$values = getCatAvailableValues($param['param_id'], $catId);
								foreach($values as $value):
							?>
								<div class="filter-checkbox-wrapper">
									<label>
										<div class="filter-checkbox filter-checkbox-off">
											<input class="hidden-checkbox" type="checkbox" name="param_checkbox[<?php echo $value['id'];?>]" <?php echo ($value['enable'] ? 'checked="checked"':'')?>/>
										</div>
										<div class="divrazmer" style="float: left;">&nbsp;<?php echo $value['value']; ?></div>
									</label>
								</div>
								<?php endforeach; ?>
							<?php } elseif ($param['type'] == 'value') { ?>
								<select name="param_select[]">
									<option value="0">Не выбрано</option>
								<?php
								$values = getCatAvailableValues($param['param_id'], $catId);
								foreach($values as $value): ?>
									<option value="<?=$value['id']?>" <?=($value['enable'] ? 'selected="selected"' : '')?>><?=$value['value']?></option>
								<?php endforeach; ?>
								</select>
							<? } ?>
							<div style="clear: both;"></div>
						</div>
						<?php } ?>
					</div>
					<div style="clear: both;"></div>
					<div>
						<h2>Дополнительные категории: </h2>
						<h4>Ctrl + Клик для снятия выделения или выбора нескольких категорий</h4>
						<?php 
							$cats = getCats();
							
						?>	
						<select name="additional_cats[]" size="15" style="width: 350px" multiple>
							<?php foreach($cats as $cat):  ?>
							<?php if ($cat['id'] != $catId):  ?>
								<option value="<?=$cat['id']?>"><?=$cat['title']?></option>
							<?php endif; 
								  endforeach;?>
						</select>
					</div>
					<div>
						<h2>Подкатегории:</h2>
						<div>
							<?php foreach(getTagsInCat($catId) as $tag) { ?>
								<div class="filter-checkbox-wrapper">
									<label>
										<div class="filter-checkbox filter-checkbox-off">
											<input class="hidden-checkbox" type="checkbox" name="tags[<?php echo $tag['id'];?>]" />
										</div>
										<div class="divrazmer" style="float: left;">&nbsp;<?php echo $tag['title']; ?></div>
									</label>
								</div>
							<?php } ?>
						</div>
						<div style="clear: both;"></div>
					</div>
					<div>&nbsp;</div>
						<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
						  <tr>
							<td width="75" align="left" class="txtskidksm">Скидка</td>
							<td align="left" class="txtskidksm" width="65">
								<input type="text" name="discount_percent" id="discount_percent" class="txtpricesmf" value="<?php echo ($product['discount_type'] == 'percent' ? $product['discount_value']:'');?>">%
							</td>
							<td align="left" >
								<input type="text" name="discount_value" id="discount_value" class="txtpricesmfr" value="<?php echo ($product['discount_type'] == 'value' ? $product['discount_value']:'');?>">
								<span class="txtrub">руб.</span>
							</td>
						  </tr>
						</table>
						<div style="margin-top:8px;">
							<span id="best">
								<span class="on" style="display: none;">
									<a href="javascript:void(0)"><img onclick="setButton('best', 'off');" src="/img/best.png" width="42" height="43" class="imgzn" /></a>
								</span>
								<span class="off">
									<a href="javascript:void(0)"><img onclick="setButton('best', 'on');" src="/img/bests.png" width="42" height="43" class="imgzn" /></a>
								</span>
							</span>
							<span id="sale">
								<span class="on" style="display: none;">
									<a href="javascript:void(0)"><img onclick="setButton('sale', 'off');" src="/img/sale.png" width="43" height="42" class="imgzn" /></a>
								</span>
								<span class="off">
									<a href="javascript:void(0)"><img onclick="setButton('sale', 'on');" src="/img/sales.png" width="43" height="42" class="imgzn" /></a>
								</span>
							</span>
							<span id="new">
								<span class="on" style="display: none;">
									<a href="javascript:void(0)"><img onclick="setButton('new', 'off');" src="/img/new.png" width="42" height="43" class="imgzn" /></a>
								</span>
								<span class="off">
									<a href="javascript:void(0)"><img onclick="setButton('new', 'on');" src="/img/news.png" width="42" height="43" class="imgzn" /></a>
								</span>
							</span>
							<div style="display: none;">
								<input id="best-checkbox" type="checkbox" name="best" />
								<input id="sale-checkbox" type="checkbox" name="sale" />
								<input id="new-checkbox" type="checkbox" name="new" />
							</div>
						</div>
					</div>
					<div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:30px;">
							<tr>
								<td colspan="2"><h2>SEO</h2></td>
							</tr>
							<tr>
								<td class="" width="100" height="40">&lt;title&gt;:</td>
								<td><input id="seo-title" name="seo_title" type="text" class="p_name" id="" value="<?php echo $product['seo_title']; ?>"></td>
							</tr>
							<tr>
								<td class="" width="100" height="40">&lt;description&gt;:</td>
								<td><input id="seo-des" name="seo_des" type="text" class="p_name" id="" value="<?php echo $product['seo_des']; ?>"></td>
							</tr>
							<tr>
								<td class="" width="100" height="40">&lt;keywords&gt;:</td>
								<td><input id="seo-key" name="seo_key" type="text" class="p_name" value="<?php echo $product['seo_key']; ?>" /></td>
							</tr>
						</table>
					</div>
					<div>
						Дополнительные фото:
					</div>
					<div>
						<input type="file" name="extra_photos[]" multiple="multiple" />
					</div>
					<div class="info-text">(Авторесайз: вкл. Размеры превью: <?php echo Config::get('image_size.product_extra_preview'); ?>)</div>
					<div>
						<input id="add-button" type="submit" name="add" class="" value="Добавить" />
					</div>
				</div>
			</form>
      </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="80">&nbsp;</td>
  </tr>
</table>

</div>
</div>
<?php include ("../down.php"); ?>