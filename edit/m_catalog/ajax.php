<?php
include('../../connect.php');
include('../../func/core.php');

switch ($_REQUEST['method']) {
	//Переключатель статусов товара (лучшее, новинка, распродажа)
	case 'toggle' :
		$availableAttrs = array('best','novinka','sale');
		$id = $_REQUEST['id'];
		$attr = $_REQUEST['attr'];
		$toggle = $_REQUEST['toggle'];
		if (is_numeric($id) && in_array($attr, $availableAttrs) && ($toggle == '1' || $toggle == '0')) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`{$attr}` = '{$toggle}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Установка стоимости товару
	case 'setprice' :
		$id = $_REQUEST['id'];
		$price = $_REQUEST['price'];
		
		if (is_numeric($id) && is_numeric($price)) {
			setPrice($id, $price);
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Установка артикла товару
	case 'setart' :
		$id = $_REQUEST['id'];
		// $art = mysql_real_escape_string(iconv("utf-8", "windows-1251", urldecode($_REQUEST['art'])));
		$art = mysql_real_escape_string(urldecode($_REQUEST['art']));
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`art` = '{$art}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	case 'set_field' :
		$id = $_REQUEST['id'];
		$value =$_REQUEST['value'];
		$field = mysql_real_escape_string($_REQUEST['field']);
		
		if ($field == 'delivery_cost') {
			//$value = empty($value) && $value != '0' ? 'NULL' : "'".mysql_real_escape_string(iconv("utf-8", "windows-1251", $value))."'";
			$value = empty($value) && $value != '0' ? 'NULL' : "'".mysql_real_escape_string($value)."'";
		} else {
			// $value = mysql_real_escape_string(iconv("utf-8", "windows-1251", $value));
			$value = mysql_real_escape_string($value);
			$value = "'{$value}'";
		}
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`{$field}` = {$value}
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => (mysql_affected_rows() < 0)));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Установка ставки маркета товару
	case 'setbid' :
		$id = $_REQUEST['id'];
		$bid = mysql_real_escape_string($_REQUEST['bid']);
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`market_cost_per_click` = '{$bid}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
		//Удаление товара
	case 'deleteproduct' :
		$id = $_REQUEST['id'];
		if (is_numeric($id)) {
			deleteProduct($id);
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	
		//Удаление товара из доп. категории
	case 'unlinkfromcat' :
		$id = $_REQUEST['id'];
		$cat_id = $_REQUEST['cat_id'];

		if (is_numeric($id) &&
			is_numeric($cat_id)) {
			$tags_id = array();
			$tags_id = getTagsInCat($cat_id, 'id');
			file_put_contents('tagsid.txt', var_export($tags_id, true));
			unlinkCatalogAndTags($id, $tags_id);
			unlinkFromAdditionalCat($id, $cat_id);
		}
		else {
			echo json_encode(array('error' => true));
		}
	break;
	
	//Получить стоимость доставки для товара
	case 'get_delivery_cost' :
		$id = $_REQUEST['id'];
		if (is_numeric($id)) {
			echo json_encode(array(
				'error' => false,
				'delivery_price' => deliveryPrice::getByCatAndPrice(0, 0, $id)
				));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//Загрузка главного фото к товару
	case 'uploadmainimage' :
		if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
			$id = $_REQUEST['id'];
			

			$a = saveMainImage($_FILES['main_image'], $id);
			
			if ($a) {
				echo json_encode(array('error' => true));
			} else {
				echo json_encode(array('error' => false));
			}
			
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	case 'uploadextraimages' :
		if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
			$catalogId = $_REQUEST['id'];
						
			$idAdded = array();
		
			for ($i = 0; $i < count($_FILES['extra_photos']['name']); $i++) {
				$photo = array(
					'name' => $_FILES['extra_photos']['name'][$i],
					'type' => $_FILES['extra_photos']['type'][$i],
					'tmp_name' => $_FILES['extra_photos']['tmp_name'][$i],
					'error' => $_FILES['extra_photos']['error'][$i],
					'size' => $_FILES['extra_photos']['size'][$i]
				);
				
				$idAdded[] = saveExtraPhoto($photo, $catalogId) ;
			}
			echo json_encode(array('error' => false, 'images' => $idAdded));
		} else {
			echo json_encode(array('error' => true, 'code' => 1));
		}
	break;
	
	case 'deleteextraimages' :
		$ids = array();
		
		foreach ($_POST['extra_photo'] as $id => $v) {
			$ids[] = $id;
			deleteExtraPhoto($id);
		}
		echo json_encode(array('error' => false, 'images' => $ids));
	break;
	
	case 'uploadposter' :
		if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
			$catalogId = $_REQUEST['id'];
						
			$postFile = $_FILES['poster'];
			$imgPath = getImagePath('product_poster');
			$size = getConfigImageSize('product_poster');
			$sFileDest = $catalogId;
			$iWidthDest = $size['width'];
			$iHeightDest = $size['height'];
			$iResizeMode = 1;
			
			
			$error = (uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, 100) == false);
			if (!$error) {
				mysql_query("UPDATE `catalog` SET `poster` = '1' WHERE `id` = '{$catalogId}'");
			}
			
			echo json_encode(array('error' => $error));
		} else {
			echo json_encode(array('error' => true, 'code' => 1));
		}
	break;
	
	case 'deleteposter' :
		if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
			$catalogId = $_REQUEST['id'];
			@unlink(getImagePath('product_poster') . $catalogId . '.jpg');
			mysql_query("UPDATE `catalog` SET `poster` = '0' WHERE `id` = '{$catalogId}'");
		}
		echo json_encode(array('error' => false));
	break;
	
	case 'changecat' :
		$ids = $_REQUEST['products'];
		$catId = $_REQUEST['cat_id'];
		
		foreach($ids as $id) {
			if (catalogChangeCat($id, $catId))
				$success[] = $id;
		}
		
		echo json_encode(array('error' => false, 'products' => $success));
	break;
	//Проверка чпу на повторение
	case 'checkchpu' : 
		$chpu = mysql_real_escape_string($_REQUEST['chpu']);
		$id = is_numeric($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;
		$count = fetchOne("SELECT COUNT(*) FROM `catalog` WHERE `chpu`='{$chpu}' AND `id` <> '{$id}'");
		
		echo json_encode(array('is_double' => $count > 0));
	break;
	//Проверка названия на повторение
	case 'checktitle' : 
//		$title = mysql_real_escape_string(iconv('utf-8', 'cp1251', $_REQUEST['title']));
		$title = mysql_real_escape_string($_REQUEST['title']);
		$id = is_numeric($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;
		$count = fetchOne("SELECT COUNT(*) FROM `catalog` WHERE `title`='{$title}' AND `id` <> '{$id}'");
		
		echo json_encode(array('is_double' => $count > 0));
	break;
	
	//Сохранение сортировки товаров
	case 'setsort' :
		$sort = $_REQUEST['sort_list'];
		
		if (is_numeric($_REQUEST['tag_id']))
			$tag_id = $_REQUEST['tag_id'];
		
		if (is_numeric($_REQUEST['cat_id']))
			$cat_id = $_REQUEST['cat_id'];
			
		//file_put_contents('sort.txt', var_export($_REQUEST, true));
		$count = count($sort);
		for ($i = 0; $i < $count; $i++) {
			if (!$tag_id) {
				if (is_numeric($sort[$i]['id']) && 
					is_numeric($sort[$i]['sort'])) {
						$id = $sort[$i]['id'];
						$rang = $sort[$i]['sort'];
						//$cat_id = $sort[$i]['cat_id'];
						mysql_query ("
							UPDATE
								`catalog_2_cat`
							SET
								`spec_rang` = '{$rang}'
							WHERE
								`catalog_id` = '{$id}'
							AND
								`cat_id` = '{$cat_id}'
						");
				}
			}
			else {
				if (is_numeric($sort[$i]['id']) && 
					is_numeric($sort[$i]['sort'])) {
						$id = $sort[$i]['id'];
						$rang = $sort[$i]['sort'];
						mysql_query ("
							UPDATE
								`tags_links`
							SET
								`catalog_rang` = '{$rang}'
							WHERE
								`catalog_id` = '{$id}'
							AND
								`tag_id` = '{$tag_id}'
						");
				}				
			} 
		}
		echo json_encode(array('error' => false));
	break;
	//Слинковка товара с тегом\подкатегорией\псевдокатегорией
	case 'linkwithtag' :
		$tagId = $_REQUEST['tag_id'];
		if (!is_numeric($tagId)) {
			echo json_encode(array('error' => true, 'message' => 'No tag ID given'));
			break;
		}
		foreach ($_REQUEST['products'] as $pid) {
			linkCatalogAndTags($pid ,$tagId);
		}
		echo json_encode(array('error' => false, "products" => $_REQUEST['products']));
	break;
	//Разлинковка товара и тега\подкатегории\псевдокатегории
	case 'unlinkfromtag' :
		$tagId = $_REQUEST['tag_id'];
		if (!is_numeric($tagId)) {
			echo json_encode(array('error' => true, 'message' => 'No tag ID given'));
			break;
		}
		foreach ($_REQUEST['products'] as $pid) {
			unlinkCatalogAndTags($pid ,$tagId);
		}
		echo json_encode(array('error' => false, "products" => $_REQUEST['products']));
	break;
	case "sorttags" :
		$sort = $_REQUEST['sort_list'];
		$count = count($sort);
		
		for ($i = 0; $i < $count; $i++) {

			if (is_numeric($sort[$i]['id']) && is_numeric($sort[$i]['sort'])) {
				$id = $sort[$i]['id'];
				$rang = $sort[$i]['sort'];
				mysql_query ("
					UPDATE
						`tags`
					SET
						`rang` = '{$rang}'
					WHERE
						`id` = '{$id}'
				");
			}
		}
		echo json_encode(array('error' => false));
	break;
	
	//Возвращает шаблон с параметрами для товара (в редактор товаров)
	case 'getparams' :
		if (isset($_REQUEST['product_id']) && is_numeric($_REQUEST['product_id'])) {
			$id = $_REQUEST['product_id'];
			
			$product = getProduct($id);
			
			include('params_tpl.php');
			if (strtotime($product['actia']) == 0){ $dat =''; }else{$dat=$product['actia'];}
			?>
			<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
			  <tr>
				<td width="75" align="left" class="txtskidksm">Скидка</td>
				<td align="left" class="txtskidksm" width="65">
					<input type="text" name="discount_percent" id="discount_percent" class="txtpricesmf" value="<?php echo ($product['discount_type'] == 'percent' ? $product['discount_value']:'');?>">%
				</td>
				<td align="left" >
					<input type="text" name="discount_value" id="discount_value" class="txtpricesmfr" value="<?php echo ($product['discount_type'] == 'value' ? $product['discount_value']:'');?>">
					<span class="txtrub">руб.</span>
				</td>
			  </tr>
              <tr>
              <td width="75" align="left" class="txtskidksm">Акция </td>
				<td align="left" class="txtskidksm"  colspan="2">
					<input type="text" name="actia_time" id="datep"  class="txtpricesmfr" style="width:170px; text-align:left;" value="<?php echo $dat;?>">
				</td>
              </tr>
			</table>
			<input type="hidden" id="price" value="<?php echo $product['price']; ?>" />
			<?php
		}
	break;
	case 'setparams' :
		if (isset($_REQUEST['product_id']) && is_numeric($_REQUEST['product_id'])) {
			$id = $_REQUEST['product_id'];
			$product = getProduct($id);
			//Массив с ID параметров
			$paramsIds = array();
			
			//Собираем id параметров с чекбоксов и селектов
			if (count($_REQUEST['param_checkbox']) > 0 ) {
				foreach	($_REQUEST['param_checkbox'] as $key => $value) {
					$paramsIds[] = $key;
				}
			}
			if (count($_REQUEST['param_select']) > 0) {
				foreach	($_REQUEST['param_select'] as $key => $value) {
					if ($value > 0) 
						$paramsIds[] = $value;
				}
			}
			
			if (count($_REQUEST['param_range']) > 0) {
				foreach	($_REQUEST['param_range'] as $key => $value) {
					if ($value['value'] != '') {
						//$value['value'] = iconv("utf-8", "windows-1251", urldecode($value['value']));
						$loloId = addAvailableParam(array(
							'paramId' => $key,
							'catId' => $value['global'] ? 0 : $value['cat'],
							'value' => '',
							'valueFloat' => $value['value']
						));
						if ($loloId) $paramsIds[] = $loloId;
					}
				}
			}
			
			setProductsParams($id, $paramsIds);
			
			//Обновляем скидки.
			$data = array();
			$data['price'] = $product['price'];
			$data['actia'] = $_REQUEST['actia_time'];
			
			$dp = $_REQUEST['discount_percent'];
			$dv = $_REQUEST['discount_value'];
			if (!empty($dp) && $dp > 0) {
				$data['discount_type'] = 'percent';
				$data['discount_value'] = $dp;
			} elseif (!empty($dv) && $dv > 0 ) {
				$data['discount_type'] = 'value';
				$data['discount_value'] = $dv;
			} else {
				$data['discount_value'] = 0;
			}
			
			$data['price_after_discount'] = getPriceAfterDiscount($data['price'], $data['discount_type'] , $data['discount_value']);
			mysql_query("
				UPDATE
					`catalog`
				SET	".getSetString($data)."
				WHERE
					`id` = '{$id}'
			");
		}
	break;
	
	//Сортировка дополнительных фото к товару
	case 'sortextraphoto' :
		$sort = $_REQUEST['sort_list'];
		$count = count($sort);
		
		for ($i = 0; $i < $count; $i++) {

			if (is_numeric($sort[$i]['id']) && is_numeric($sort[$i]['sort'])) {
				$id = $sort[$i]['id'];
				$rang = $sort[$i]['sort'];
				mysql_query ("
					UPDATE
						`foto`
					SET
						`rang` = '{$rang}'
					WHERE
						`id` = '{$id}'
				");
			}
		}
		echo json_encode(array('error' => false));
	break;
	
	//Обработка запроса на начало слинковки товара с 10мед
	case 'want_to_link' :
		$id = $_REQUEST['id'];
		if (!is_numeric($id)) {
			echo json_encode(array('error' => true, 'error_code' => 0));
			break;
		}
		//Выбираем данные о товаре
		$product = getProduct($id);
		
		//Отправляем api запрос
		// $response = api_10med($method = 'want_to_link', $params);
		$response = api10med::search($product['title']);

		//Разбираем ответ
		echo $response;
	break;
	
	case 'get_linked_product':
		//ID товара в магазине
		$localId = $_REQUEST['local_id'];
		
		if (!is_numeric($localId)) {
			echo json_encode(array('error' => true, 'error_message' => "Bad ID given"));
			break;
		}
		$remoteId = fetchOne("SELECT `linked_with_10med` FROM `catalog` WHERE `id` = '{$localId}'");
		if ($remoteId) {
			
			//Отправляем запрос на слинковку
			echo api10med::product($remoteId);
		} else {
			echo json_encode(array('error' => true, 'error_message' => "Product not linked"));
			break;
		}
	break;
	
	case 'link':
		//ID товара в магазине
		$localId = $_REQUEST['local_id'];
		//ID товара на 10мед
		$remoteId = $_REQUEST['remote_id'];
		
		if (!is_numeric($localId) || !is_numeric($remoteId)) {
			echo json_encode(array('error' => true, 'error_code' => 0));
			break;
		}
		
		$params = array(
			'local_id' => $localId ,
			'remote_id' => $remoteId
		);
		
		//Отправляем запрос на слинковку
		$response = json_decode(api10med::link($localId, $remoteId), true);
		if ($response['code'] == 200) {
			mysql_query("UPDATE `catalog` SET `linked_with_10med` = '{$response['linkedWith']}' WHERE `id` = '{$localId}' LIMIT 1");
		}
		echo json_encode(array('error' => $response['error'], 'error_code' => 0));
		break;
	break;
	
	case 'unlink' :
		//ID товара в магазине
		$localId = $_REQUEST['local_id'];
		
		if (!is_numeric($localId)) {
			echo json_encode(array('error' => true, 'error_message' => 'Bad ID given'));
			break;
		}
		
		$params = array(
			'local_id' => $localId
		);
		$id10med = mysql_result(mysql_query('select `linked_with_10med` from `catalog` where `id` = '.$localId.' limit 1'), 0);
		
		//Отправляем запрос на слинковку
		$response = json_decode(api10med::link(0, $id10med),true);
		if ($response['code'] == 200) {
			mysql_query("UPDATE `catalog` SET `linked_with_10med` = '0' WHERE `id` = '{$localId}' LIMIT 1");
		}
		
		echo json_encode(array('error' => $response['error'], 'error_message' => $response['error_message']));
	break;
	
	case 'manual_search' :
		$params = array(
			'search_text' => $_REQUEST['search_text']
		);
		
		//Отправляем запрос на поиск
		$response = api_10med('manual_search', $params);
		
		echo json_encode(array(
				'error' => false,
				'suggestion' => $response['suggestion']
				));
	break;
	
	//Добавить товары в доп категории
	case 'link_with_add_cats' : 
	
		if (is_array($_REQUEST['products']) && is_array($_REQUEST['categories']) &&
			!empty($_REQUEST['products']) && !empty($_REQUEST['categories'])) {
			$products = $_REQUEST['products'];
			$categories = $_REQUEST['categories'];
			
			$product_string = implode(', ', $products);
			$cat_string = implode(', ', $categories);

	
			//выбираем уже сущуствующие пары товар-категория и добавляем их в массив
			$query = "SELECT `cc`.`cat_id`, `cc`.`catalog_id` FROM `catalog_2_cat` as `cc` 
					  WHERE `cc`.`catalog_id` IN ({$product_string})
					  AND `cc`.`cat_id` IN ({$cat_string})";
			
			$res = mysql_query($query);
			while ($row = mysql_fetch_assoc($res)) {
				$products_in_cat[] = $row;
			}
			//file_put_contents('prod_in_cat.txt', var_export($products_in_cat, true));
		
			foreach($categories as $cat) {
				foreach($products as $product) {
					foreach($products_in_cat as $pr_cat) {
						//Если пара товар-категория уже существует,
						//не добавляем её в БД
						if (in_array($product, $pr_cat) && in_array($cat, $pr_cat)) {
							continue 2;
						}
					}					
					$values[] = "({$cat}, {$product})"; 
				}
			}
			//Добавляем товар в доп. категорию
			$values_string = implode(', ', $values);
			$query = "INSERT INTO `catalog_2_cat`
						(`cat_id`, `catalog_id`)
					  VALUES
						{$values_string}";
			mysql_query($query);
			//file_put_contents('values_string.txt', $query);
			echo json_encode(array(
					'error' => false,
					'suggestion' => $response['suggestion']
					));			
		}

	break;
	
	case 'fake_in_stock' :
		
		if (isset($_REQUEST['id']) && 
			is_numeric($_REQUEST['id']) &&
			isset($_REQUEST['check'])) {
		
			$fake_in_stock = ($_REQUEST['check'] === 'true') ? 1 : 0;
			$id = $_REQUEST['id'];
			
			$query = "UPDATE catalog 
					  SET fake_in_stock = {$fake_in_stock}
					  WHERE id = {$id}";			
			mysql_query($query);
			
			if (mysql_error()) 
				echo json_encode(array("error" => true, 
										"msg" => "Database error"));
		}
		else 
			echo json_encode(array("error" => true, 
									"msg" => "Wrong data")); 
	break;
			case 'hide_products' :
		
		if (isset($_REQUEST['id']) && 
			is_numeric($_REQUEST['id']) &&
			isset($_REQUEST['check'])) {
		
			$hide = ($_REQUEST['check'] === 'true') ? 1 : 0;
			$id = $_REQUEST['id'];
			
			$query = "UPDATE catalog 
					  SET is_hide = {$hide}
					  WHERE id = {$id}";			
			mysql_query($query);
			
			if (mysql_error()) 
				echo json_encode(array("error" => true, 
										"msg" => "Database error"));
		}
		else 
			echo json_encode(array("error" => true, 
									"msg" => "Wrong data")); 
	break;
}