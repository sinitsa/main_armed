<?php

include($_SERVER['DOCUMENT_ROOT'] . '/' . 'connect.php');
//include(ROOT_DIR . 'func/core.php');


switch ($_REQUEST['method']) {

    case 'getRelatedProducts':

        $productId = $_REQUEST['id'] ;

        $q = "SELECT a.`id`, a.`related_product_id`, b.title, b.price
              FROM `catalog_related` as a 
              LEFT JOIN `catalog` as b on a.related_product_id = b.id 
              WHERE `catalog_id` = {$productId} ;" ;

        $res = mysql_query($q);

        $result = [] ;

        while ($row = mysql_fetch_assoc($res)){
            $result[] = $row;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);

	break;

    case 'searchProds':

        $search = $_REQUEST['search'] ;

        $q = "SELECT id, title, price 
              FROM catalog 
              WHERE title like '%{$search}%' and `is_hide` = 0 ;" ;

        $res = mysql_query($q);


        if (!$res) {
            echo json_encode(mysql_error(), JSON_UNESCAPED_UNICODE);
            return false ;
        }

        while ($row = mysql_fetch_assoc($res)){
            $result[] = $row;
        }

        echo json_encode($result, JSON_UNESCAPED_UNICODE);

        break;
    case 'addRelatedProduct' :

        $productId = $_REQUEST['productId'] ;
        $relatedProductId = $_REQUEST['relatedProductId'] ;

        $is_uniq = checkProductRelations($productId, $relatedProductId);

        if (!$is_uniq){
            echo json_encode("Link already exists!");
            return false;
        }

        $q = "INSERT INTO catalog_related (catalog_id, related_product_id) VALUES ({$productId}, {$relatedProductId}) ; " ;

        $res = mysql_query($q);

        echo json_encode($res);

        break;

    case 'deleteRelatedProduct' :

        $linkId = $_REQUEST['linkId'] ;

        if (!is_numeric($linkId)){
            echo json_encode("bad link id!");
            return false ;
        }

        $q = "DELETE FROM catalog_related WHERE id = {$linkId} " ;

        $res = mysql_query($q);

        echo json_encode($res);

        break;
}



function checkProductRelations($productId, $relatedProductId){

    $q = "SELECT * from catalog_related WHERE catalog_id = {$productId} AND related_product_id = {$relatedProductId} LIMIT 1 ; " ;

    $result = false;

    if (mysql_num_rows(mysql_query($q)) < 1) {
        $result = true ;
    }
    return $result;
}