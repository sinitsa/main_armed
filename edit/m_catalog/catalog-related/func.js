$(document).ready(function(){

    var productId = $('#catalog-extra-image-upload').find('input[name="id"]').val();
    var myModal = new Modal('.related-products-modal', productId);

    $('#related_prods').click(function(){
        myModal.showModal();
    });

});





function Modal(modal_selector, productId, top_offset){

    var modalBody = $(modal_selector);
    var self = this ;
    var overlay = $('.overlay');
    var ajaxUrl = 'catalog-related/ajax.php' ;

    getRelatedProducts(productId);

    addEvents();
    styleModal();
    this.productId = productId ;
    this.modalBody = $(modal_selector);

    this.hideModal = function(speed){
        var speed = speed || 300 ;
        modalBody.slideUp(speed);
        overlay.slideUp(speed);
    }
    this.showModal = function(speed){
        styleModal();
        var speed = speed || 300 ;
        modalBody.slideDown(speed);
        overlay.slideDown(speed);
    }

    function addEvents(){
        modalBody.find('.close_modal').click(function(){
            self.hideModal();
        });
        overlay.click(function(){
            self.hideModal();
        })

        modalBody.find('.search-prods').one('click',function(){

            var search = modalBody.find('input[name="title"]').val();
            searchProds(search);
        });

        modalBody.find('.clear-result').one('click',function(){
            modalBody.find('.result').html('');
        });


        modalBody.find('.related-prods ul button').one('click',function(){
            var linkId = $(this).attr('data-link-id') - 0 ;
            deleteRelatedProduct(linkId);
        })
    }


    function getRelatedProducts(product_id){
        var params = {
            method: 'getRelatedProducts',
            id: product_id,
        };

        $.ajax({
            url:ajaxUrl,
            type:'POST',
            data:params,
            dataType:'json',
            success:function(data){
                //console.log(data);
                renderProductsList(data);
                addEvents();
            }
        })
    }

    function renderProductsList(data){
        var html = '' ;

        data.forEach(function(elem){
            html += '<li><span>'+ elem.title +'</span><button data-link-id="' + elem.id + '" >X</button></li>';
        });

        modalBody.find('.related-prods ul').html(html);
    }

    function searchProds(search){
        var params = {
            method: 'searchProds',
            search: search,
        };

        $.ajax({
            url:ajaxUrl,
            type:'POST',
            data:params,
            dataType:'json',
            success:function(data){
                //console.log(data);
                renderProductsSearchList(data);

                modalBody.find('.result button').one('click',function(){
                    var relatedProductId = $(this).attr('data-prod-id') - 0 ;
                    addRelatedProduct(productId, relatedProductId);
                });
            }
        })
    }

    function renderProductsSearchList(data){

        var html = '<ul>' ;

        if (data == null || data.length < 1) {
            return false ;
        }

        data.forEach(function(elem){
            html += '<li><span>'+ elem.title +'</span><button data-prod-id="' + elem.id + '" >Добавить</button></li>';
        });

        html += '</ul>' ;

        modalBody.find('.result').html(html);
    }

    function addRelatedProduct(prodId, relatedProdId){
        var params = {
            method: 'addRelatedProduct',
            productId: prodId,
            relatedProductId: relatedProdId,
        };
        $.ajax({
            url:ajaxUrl,
            type:'POST',
            data:params,
            dataType:'json',
            success:function(data){
                console.log(data);
                getRelatedProducts(prodId);
            }
        })
    }

    function deleteRelatedProduct(linkId){
        var params = {
            method: 'deleteRelatedProduct',
            linkId: linkId
        };
        $.ajax({
            url:ajaxUrl,
            type:'POST',
            data:params,
            dataType:'json',
            success:function(data){
                console.log(data);
                getRelatedProducts(productId);
            }
        })
    }

    function calculateCoords(top){
        var top = top || 100 ;

        var top_scroll = $(window).scrollTop() + top ;

        var modalWidth = modalBody.width();
        var windowWidth = $(window).width();

        var left = windowWidth/2 - modalWidth/2 ;

        return [top_scroll, left];
    }

    function styleModal(top_offset){
        var coords = calculateCoords(top_offset);
        modalBody.css({
            top: coords[0],
            left: coords[1],
        });
    }

}