<div class="related-products-modal">
    <div class="close_modal">X</div>
    <h2>Сопутствующие товары:</h2>
    <div class="search-cont">

        <p>Поиск:</p>
        <input type="text" name="title" placeholder="название товара">
        <button class="search-prods">Искать</button>
        <button class="clear-result">Очистить</button>
        <div class="result">

        </div>
    </div>
    <h3>Связанные товары:</h3>
    <div class="related-prods">
        <ul></ul>
    </div>
</div>
<div class="overlay"></div>