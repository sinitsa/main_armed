<?php
include ("../connect.php");
include ("../../func/core.php");
?>
<?php 
$cssOl = true;
include ("../up.php");

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
	$id = $_GET['id'];
	$catalogId = $_GET['id'];

	include('sql_edit.php');
	
	$product = getProduct($id);
	if ($product) {
		
		if (isset($_GET['id']) && is_numeric($_GET['cat_id']))
			$cat_id = $_GET['cat_id'];	
		else
			$cat_id = $product['cat'];
			
		$catInfo = getCatInfo($cat_id);
		//Теги, слинкованные с товаром
		$tagsActive = array();
		$tags = getTagsInCatalog($catalogId);
		foreach ($tags as $tag) {
			$tagsActive[] = $tag['id'];
		}
		unset($tags, $tag);
		
		$data['feedback'] = getProductFeedback($product['id'], true);
		
	} else {
		die('Product not found');
	}	
} else {
	die('bad link');
}

?>
<link rel="stylesheet" href="ui-lightness/jquery-ui-1.10.3.custom.css">

    <!-- подключаем функционал сопутствующих товаров -->
    <link rel="stylesheet" href="catalog-related/styles.css">
    <script type="text/javascript" src="catalog-related/func.js"></script>

<script type="text/javascript" src="/edit/ckeditor/ckeditor.js" charset="UTF-8"></script>
<script type="text/javascript" src="/edit/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js" charset="UTF-8"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.13.js"></script>
<script type="text/javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript">
$(function() {
	stylizeCheckbox();
	//====== Аплод файлов
	//Главного фото
	$("#catalog-main-image-upload input:file").change(function (){
            $('#catalog-main-image-upload form').ajaxSubmit({
				url : 'ajax.php?method=uploadmainimage',
				data : {"id" : <?php echo $id; ?>},
				dataType : "json",
				error : function () {
					alert('some error here');
				},
				success: function(data) { 
						 var a = document.createElement('a');
						 a.href = $('#main_image').attr('src');
						 $('#main_image').attr('src', a.pathname + '?' + Math.random() );
				}
			});
	});
	//Дополнительные фото
	$("#catalog-extra-image-upload input:file").change(function () {
	         $('#catalog-extra-image-upload form').ajaxSubmit({
				url : 'ajax.php?method=uploadextraimages',
				dataType : "json",
				error : function () {
					alert('Не все изображения были загружены');
					location.reload();
				},
				beforeSend : function () {$('.extra-loading').show();},
				success: function(data) { 
					var field = $('#catalog-extra-images-field');
					
					for (var i = 0; i < data.images.length; i++) {
						var id = data.images[i];
						field.append('' + 
							'<div data-id="' + id + '" class="imgcatm" style="display: none;" id="extra-image-' + id + '">' +
								'<div style="float: left;">'+
									'<img src="<?=getImageWebPath('product_extra_preview')?>' + id + '.jpg" />' +
								'</div>' +
								'<div class="imgcatdel" style="float: left;">' +
									'<img src="/img/dopfotodel.png" width="24" height="24" style="margin-bottom:10px;" />' +
									'<br />' +
									'<div style="margin-right:4px; float: right;">' +
										'<div class="filter-checkbox-wrapper">' +
											'<div class="filter-checkbox filter-checkbox-off">' +
												'<input class="hidden-checkbox" type="checkbox" name="extra_photo[' + id + ']" />' +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
							'</div>');
							$('#extra-image-' + id).fadeIn('slow');
					}
					stylizeCheckbox();
					$('.extra-loading').hide();
				}
			});
	});
	//Сортировка доп фото
	$('#catalog-extra-images-field').sortable({
		connectWith: "div.imgcatm",
		//cancel: ".cl",
		tolerance : "pointer",
		update : function (event, ui) {
		
			$('#tags-field').css('opacity', .5);
			
			var sortList = new Array();
			
			$('#catalog-extra-images-field .imgcatm').each(function (index, element) {
				sortList.push({
					'id' : $(element).data('id'),
					'sort' : index
				});
			});
			
			$.ajax({
				url: 'ajax.php?method=sortextraphoto',
				type : 'POST',
				dataType : 'json',
				data : {"sort_list" : sortList},
				beforeSend : function () {},
				success : function (data, textStatus, jqXHR) {
					if (data.error) {
						//do nothing :)
					} else {
						//хайдим
						$('#tags-field').css('opacity', 1);
					}
				},
				error : function (jqXHR, textStatus, errorThrown) {}
			});
		}
	});
	//Автопросцет % и значений скиндки в форме
	$('#price').bind('change', function(){
		$('#discount_value').trigger('keyup');
	});
	$('#discount_percent').bind('keyup', function() {
		var price = $('#price').val();
		var percent = $(this).val();
		
		if (price == '') price = 0;
		$('#discount_value').val(Math.round(percent * price / 100));
	});
	$('#discount_value').bind('keyup', function() {
		var price = $('#price').val();
		var value = $(this).val();

		if (price == '') price = 0;
		//$('#discount_percent').val(Math.round(value * 100 / price));
	});
	
	//Хак, чтобы заполнить оба поля скидки
	if ($('#discount_percent').val() != '') {
		$('#discount_percent').trigger('keyup');
	} else if ($('#discount_value').val() != '') {
		$('#discount_value').trigger('keyup');
	}
	
	//Проверка ЧПУ на совпадение
	$('#chpu-input').bind('change', function () {
		var chpu = $('#chpu-input').val();
		if (chpu.length <= 0) return;
		
		$.ajax({
				url: 'ajax.php?method=checkchpu',
				type : 'POST',
				dataType : 'json',
				data : {"chpu" : chpu, "product_id" : <?php echo $catalogId; ?>},
				//beforeSend : function () {},
				success : function (data, textStatus, jqXHR) {
					if (data.is_double) {
						$('#chpu-warning-message').show();
					} else {
						$('#chpu-warning-message').hide();
					}
				}
				//error : function (jqXHR, textStatus, errorThrown) {}
		});
	});
	
	//Перересайз фото
	$('.image_make_resize').bind('click', function () {
		$.ajax({
				url: '../m_resizer/ajax.php?method=resizeproducts',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : <?php echo $catalogId; ?>,
					"preview" : 'yes',
					"preview_extra" : 'yes',
					"small" : 'yes',
					"medium" : 'yes'
					},
				//beforeSend : function () {},
				success : function (data, textStatus, jqXHR) { alert('Обработано ' + data.resized + ' изображений');	}
				//error : function (jqXHR, textStatus, errorThrown) {}
		});
	});
	
	
	//Постер
	$("#catalog-poster-image-upload input:file").change(function () {
		var loading = $('#catalog-poster-image-upload .a-loading');
		$('#catalog-poster-image-upload form').ajaxSubmit({
			url : 'ajax.php?method=uploadposter',
			dataType : "json",
			/*error : function () {
				//alert('Не все изображения были загружены');
				//location.reload();
				alert('asd');
			},*/
			beforeSend : function () {
				loading.show();
			},
			success: function(data) {
				var field = $('#catalog-poster-image-upload .content-field');
				
				var src = '<?=getImageWebPath('product_poster').$product['id']?>.jpg';
				field.empty();
				field.append( $('<img>').attr('src', src + '?r=' + Math.random()) );
				//stylizeCheckbox();
				loading.hide();
				
			}
		});
	});
	$("#catalog-poster-image-upload .delete").bind('click', function () {
		var id = $('#catalog-poster-image-upload form input[name="id"]').val();
		var loading = $('#catalog-poster-image-upload .a-loading');
		
		$.ajax({
			url : 'ajax.php?method=deleteposter',
			dataType : "json",
			type: "POST",
			data : {'id' : id},
			error : function () {
				//alert('Не все изображения были загружены');
				//location.reload();
			},
			beforeSend : function () {
				loading.show();
			},
			success: function(data) { 
				var field = $('#catalog-poster-image-upload .content-field');
				field.empty();
				//stylizeCheckbox();
				loading.hide();
			}
		});
	});
});
//Управление чекбоксами
var isAllChecked = false;
function checkAllImages() {
	if (isAllChecked) {
		$('#catalog-extra-images-form input:checkbox').removeAttr("checked");
	} else {
		$('#catalog-extra-images-form input:checkbox').attr("checked","checked");
	}
	isAllChecked = !isAllChecked;
	stylizeCheckbox();
}
//Удаление дополнительных фото
function deleteImages() {
	$('#catalog-extra-images-form').ajaxSubmit({
				url : 'ajax.php?method=deleteextraimages',
				dataType : "json",
				error : function () {
					//alert('some error here');
					$('.extra-loading').hide();
				},
				beforeSend : function () {$('.extra-loading').show();},
				success: function(data) { 
					if (!data.error) {
						for (var i = 0; i < data.images.length; i++) {
							$('#extra-image-' + data.images[i]).fadeOut('slow', function() {
								$(this).remove();
							});
						}
						$('.extra-loading').hide();
					}
				}
			});
}

/* JS ДЛЯ коментов к товарам */
$(function () {
	$('.feed .answer-button').bind('click', function(){
		var feed = $(this).closest('.feed');
		feed.find('.answer-form').slideDown();
	});
	$('.feed input[name="answer"]').bind('click', function () {
		var feed = $(this).closest('.feed');
		var feedId = feed.data('id');
		var loading = feed.find('.loading');
		
		var answer = feed.find('textarea');
		var sendButton = $(this);
		
		$.ajax({
				url: '../m_catalog_feedback/ajax.php?method=add_answer',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : feedId,
					"answer" : answer.val()
					},
				beforeSend : function () {
					loading.show();
					answer.attr('disabled','disabled');
					sendButton.attr('disabled','disabled');
				},
				success : function (data, textStatus, jqXHR) {
					feed.find('.answer').text(answer.val());
					feed.find('.answer-form').slideUp();
				},
				complete : function () {
					loading.hide();
					answer.removeAttr('disabled');
					sendButton.removeAttr('disabled');
				}
		});
		
	});
	
	$('.feed .delete').bind('click', function (){
		var feed = $(this).closest('.feed');
		var button = $(this);
		var feedId  = feed.data('id');
		
		$.ajax({
				url: '../m_catalog_feedback/ajax.php?method=delete_feed',
				type : 'POST',
				dataType : 'json',
				data : { "id" : feedId },
				beforeSend : function () {
					button.attr('disabled','disabled');
					feed.css({"opacity": .5});
				},
				success : function (data, textStatus, jqXHR) {
					feed.fadeOut('fast', function () {
						feed.remove();
					});
				},
				complete : function () {
					button.removeAttr('disabled');
				}
		});
	});
	$('.feed .confirm').bind('click', function (){
		var feed = $(this).closest('.feed');
		var button = $(this);
		var feedId  = feed.data('id');
		
		var confirm = button.attr('disabled') ? 0 : 1; //button.is(":disabled") ? 0 : 1;
		$.ajax({
				url: '../m_catalog_feedback/ajax.php?method=confirm_feed',
				type : 'POST',
				dataType : 'json',
				data : { "id" : feedId, "confirm" : confirm},
				beforeSend : function () {
					feed.css({"opacity": .5});
				},
				success : function (data, textStatus, jqXHR) {
					if (confirm == 1) {
						button.attr('disabled','disabled');
					} else {
						button.removeAttr('disabled');
					}
				},
				complete : function () {
					feed.css({"opacity": 1});
				}
		});
	});
	
	//добавить камент
	$('#add-feed-btn').bind('click', function() {
		var textarea = $('#feed-editor').val();
		var button = $(this);
		var name = $('#fn').val();
		var email = $('#fe').val();
		var date = $('#fd').val();
		$.ajax({
			url: '../m_catalog_feedback/ajax.php?method=add_feed',
			type : 'POST',
			dataType : 'json',
			data : {"name" : name, 
					"email" : email, 
					"date" : date, 
					"text" : textarea,
					"catalog_id" : <?php echo $catalogId; ?>},
			beforeSend : function() {},
			success : function (data, textStatus, jqXHR) {
				//alert ('GREAT SUCCESS!!1');
				if (data.error) 
					alert(data.error);
				else {
					alert('Готово!');
					$("#feed_form").trigger('reset');
					document.location.reload();
				}
			},
			complete : function() {}
		}); 

});
});
	$(function() {
		$("#fd").live("click", function(){
			   $(this).datepicker({
							showOn: 'focus',
							dateFormat: "yy-mm-dd",
							onSelect: function(datetext){
								var d = new Date(); // for now
								datetext=datetext+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
								$(this).val(datetext);
						},
				}).focus();
		}); 
	});	

</script>
<div class="ol" style="width:100%; height:100%; position:absolute; z-index:-2;" align="center">
<div style="width:100%; height:100%; max-width:1270px; min-width:1000px;" align="center">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="99%" align="center" valign="top"><table width="96%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="200" align="left" valign="top">
			<div style="margin-top:-10px;"></div>
			<?php foreach (getMenu() as $item) { ?>
				<table width="100%" border="0" cellpadding="0" cellspacing="0"  style="margin-bottom:5px; margin-top:12px;">
					  <tr>
						<td width="14"><img src="/img/mainl.gif" width="14" height="31"></td>
						<td class="lmainrast">
						  <div class="txt_banner" style="margin-top:-3px;"><?php echo $item['title']; ?></div>
						  </td>
						<td width="14"><img src="/img/mainr.gif" width="14" height="31" /></td>
					  </tr>
				</table>
				<?php if (count($item['submenu']) > 0) { ?>
					<?php foreach ($item['submenu'] as $submenu) { ?>
						<div class="limain"><a href="/edit/m_catalog/list.php?id=<?php echo $submenu['cat_id']; ?>"><?php echo $submenu['title']; ?> </a></div> 
					<?php } ?>
				<?php } ?>
			<?php }	?>
        </td>
        <td align="left" valign="top">
        <div style="margin-left:20px;"><span class="txtcroshki"><a href="/edit/m_catalog/list.php?id=<?php echo $catInfo['id']; ?>"><?php echo $catInfo['title']; ?></a></span>  <span class="txtcroshki">»</span> 
  <h1><?php echo $product['title']; ?></h1></div>
  

<div style="margin-top:25px;margin-left:20px;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="100" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td>
				<img id="main_image" src="<?php echo getImageWebPath('product_preview').$product['id']; ?>.jpg" style="margin-bottom:20px;" />
				<div id="catalog-main-image-upload" style="background: url('/img/foto.gif'); width: 182px; height: 40px;">
					<form action="" method="post" enctype="multipart/form-data">
						<input type="file" name="main_image" />
					</form>
				</div>
				<div class="info-text">(Авторесайз: вкл; Размеры превью: <?php echo Config::get('image_size.product_preview'); ?>)</div>
			</td>
        </tr>
        <tr>
          <td></td>
        </tr>
      </table></td>
      <td width="20">&nbsp;</td>
      <td align="left" valign="top">
	  <form action="" method="post" enctype="multipart/form-data">
	 <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:30px;">
	<tr>
		<td colspan="2">
			<a href="<?php echo schars(getTemplateLink($product, 'catalog'));?>" style="color: #fff; font-weight: bold; text-decoration: none;" class="btn btn-warning" target="_blank">Посмотреть на сайте</a>
            <a id="related_prods" href="javascript:void(0);">Сопутствующие товары</a>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			&nbsp;
		</td>
	</tr>
	<tr>
    <td class="txtbname" width="100" height="40">Категория:</td>
    <td>
		<select name="cat" class="p_cat">
			<?php
			$cats = getCats();
			foreach ($cats as $cat) {
				echo "<option value=\"{$cat['id']}\"".($cat['id'] == $product['cat'] ? ' selected="selected"':'').">{$cat['title']}</option>";
				if (isset($cat['subcats']) && count($cat['subcats']) > 0) {
					foreach ($cat['subcats'] as $subcat) {
						echo "<option value=\"{$subcat['id']}\"".($subcat['id'] == $product['cat'] ? ' selected="selected"':'').">- {$subcat['title']}</option>";
					}
				}
			}
			?>
		</select>
	</td>
  </tr>
  <tr>
    <td class="txtbname" width="100" height="40">Название:</td>
    <td><input name="title" type="text" class="p_name" id="" value="<?php echo htmlspecialchars($product['title']); ?>"></td>
  </tr>
  <tr>
    <td class="txtbname" width="100" height="40">ЧПУ:</td>
    <td><input id="chpu-input" name="chpu" type="text" class="p_name" value="<?php echo schars($product['chpu']); ?>" /></td>
  </tr>
    <tr>
    <td class="txtbname" width="100" height="40">Номенклатура:</td>
    <td><input id="chpu-input" name="nomencl" type="text" class="p_name" value="<?php echo schars($product['nomencl_10med']); ?>" /></td>
  </tr>
  <tr>
	<td></td>
	<td><div id="chpu-warning-message" style="display: none; color: red;">Внимание, такой чпу уже используется у другого товара</div></td>
  </tr>
  <tr>
	<td><div style="width:200px;"><input type="checkbox" name="fake_in_stock" value="1" <? if ($product['fake_in_stock'] == 1) echo "checked"?>><span style="margin-left: 5px;">Всегда в наличии</span></div></td>
  </tr>
</table>

     
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="45"><input type="text" name="price" id="price" class="txtpricebig" value="<?php echo $product['price']; ?>" />
            <span class="txtrub">руб.</span></td>
          </tr>
      </table>
     <div style=" margin-top:15px;">
		<input type="text" name="article" class="txtpricesmfr" value="<?php echo $product['art']; ?>"> <span class="txtrub">арт.</span>
	</div>
	<div style=" margin-top:15px;">
		<input type="text" name="warranty" class="txtpricesmfr" value="<?php echo $product['warranty']; ?>"> <span class="txtrub"> Гарантия, лет </span>
	</div>

      <div style="margin-top:20px;">
		<h2>Описание:</h2>
		<div>
			<textarea name="desc" cols="70" rows="7" style="width: 400px;"><?php echo $product['des']; ?></textarea>
			<script type="text/javascript">
				var editor = CKEDITOR.replace( 'desc' );
				CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
			</script>
		</div>
	</div>
	<div style="margin-top:20px;">
		<h2>Видео:</h2>
		<textarea name="iframe" class="p_video"><?echo schars($product['iframe']);?></textarea>
		<h2>Заголовок видео:</h2>
		<input  name="iframe_des" class="p_video_des" value="<?=$product['iframe_des'];?>"/>
	</div>
	<div style="margin-top:20px;">
		<h2>Короткое описание:</h2>
		<div>
			<textarea name="short_des" cols="70" rows="7" style="width: 400px;"><?php echo $product['short_des']; ?></textarea>
			<script type="text/javascript">
				var editor = CKEDITOR.replace( 'short_des' );
				CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
			</script>
		</div>
	</div>

		<div style="margin-top:20px;">
		<h2>Описание (на вкладке):</h2>
		<div>
			<textarea name="under_price_des" cols="70" rows="7" style="width: 400px;"><?php echo $product['under_price_des']; ?></textarea>
			<script type="text/javascript">
				var editor = CKEDITOR.replace( 'under_price_des' );
				CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
			</script>
		</div>
	</div>

		<div style="margin-top:20px;">
		<h2>Комплектация:</h2>
		<div>
			<textarea name="adv_des" cols="70" rows="7" style="width: 400px;"><?php echo $product['adv_des']; ?></textarea>
			<script type="text/javascript">
				var editor = CKEDITOR.replace( 'adv_des' );
				CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
			</script>
		</div>
	</div>

	<div style="margin-top:20px;">
		<h2>Технические характеристики:</h2>
		<div>
			<textarea name="like_desc" cols="70" rows="7" style="width: 400px;"><?php echo $product['like_desc']; ?></textarea>
			<script type="text/javascript">
				var editor = CKEDITOR.replace( 'like_desc' );
				CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
			</script>
		</div>
	</div>
          <h2>
              Инструкция:
          </h2>
          <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />

          <?php if (isset($product['instruction']) && !empty($product['instruction'])) {?>
              <div><a href="<?=getInstructionPath($product['id']) . urldecode($product['instruction'])?>"><?=$product['instruction']?></a></div>
          <?php } ?>
          <div>
              <input type="file" name="instruction" />
          </div>
	<div>
	<h2>Дополнительные категории:</h2>
	<h4>Ctrl + Клик для снятия выделения или выбора нескольких категорий</h4>
	<?php 
		$cats = getCats();
		$addCats = getAdditionalCatsId($id);
	?>	
		<select name="additional_cats[]" size="15" style="width: 350px" multiple>
	<?php foreach($cats as $cat):  ?>
	<?php if ($cat['id'] != $product['cat']):  ?>
		<option value="<?=$cat['id']?>" <? if (in_array($cat['id'], $addCats)) echo 'selected'; ?>><?=$cat['title']?></option>
	<?php endif; 
		  endforeach;?>
		</select>
	</div>
	<!-- <div class="txtrub" style="margin-top:25px;">Title: <input name="something_that_i_never_know" type="text" class="p_title" id="textfield2"></div> -->

         <div style="margin-top:35px;">
			<input type="image" name="save" src="/img/save.gif" width="137" height="40" />
			<!-- <img src="/img/save.gif" width="137" height="40" /> -->
			<!-- <img src="/img/saves.gif" width="137" height="40" /> -->
		</div>
		<div style="margin-top:30px;">
			<?php include('params_tpl.php'); ?>
		</div>
		<div style="clear: both;"></div>
		<div style="margin-top: 35px; min-height: 150px;">
			<h2>Подкатегории</h2>
			<div>
				<?php foreach(getTagsInCat($cat_id) as $tag) { ?>
					<div class="filter-checkbox-wrapper">
						<label>
							<div class="filter-checkbox filter-checkbox-off">
								<input class="hidden-checkbox" type="checkbox" name="tags[<?php echo $tag['id'];?>]" <?php echo (in_array($tag['id'], $tagsActive) ? 'checked="checked"':'')?> value="<?=$tag['id']?>"/>
							</div>
							<div class="divrazmer" style="float: left;">&nbsp;<?php echo $tag['title']; ?></div>
						</label>
					</div>
				<?php } ?>
			</div>
		</div>
		</div>
        <div style=" margin-top:35px; margin-bottom:25px;">
			<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
			  <tr>
				<td width="75" align="left" class="txtskidksm">Скидка</td>
				<td align="left" class="txtskidksm" width="65">
					<input type="text" name="discount_percent" id="discount_percent" class="txtpricesmf" value="<?php echo ($product['discount_type'] == 'percent' ? $product['discount_value']:'');?>">%
				</td>
				<td align="left" >
					<input type="text" name="discount_value" id="discount_value" class="txtpricesmfr" value="<?php echo ($product['discount_type'] == 'value' ? $product['discount_value']:'');?>">
					<span class="txtrub">руб.</span>
				</td>
			  </tr>
			</table>
			<div style="margin-top:8px;">
				<?php if ($product['best']) { ?>
					<a href="javascript:void(0)"><img data-enabled="1" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'best')" src="/img/best.png" width="42" height="43" class="imgzn" /></a>
				<?php } else { ?>
					<a href="javascript:void(0)"><img data-enabled="0" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'best')" src="/img/bests.png" width="42" height="43" class="imgzn" /></a>
				<?php } ?>
				<?php if ($product['sale']) { ?>
					<a href="javascript:void(0)"><img data-enabled="1" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'sale')" src="/img/sale.png" width="43" height="42" class="imgzn" /></a>
				<?php } else { ?>
					<a href="javascript:void(0)"><img data-enabled="0" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'sale')" src="/img/sales.png" width="43" height="42" class="imgzn" /></a>
				<?php } ?>
				<?php if ($product['novinka']) { ?>
					<a href="javascript:void(0)"><img data-enabled="1" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'novinka')" src="/img/new.png" width="42" height="43" class="imgzn" /></a>
				<?php } else { ?>
					<a href="javascript:void(0)"><img data-enabled="0" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'novinka')" src="/img/news.png" width="42" height="43" class="imgzn" /></a>
				<?php } ?>
			</div>
      </div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:30px;">
			<tr>
				<td colspan="2"><h2>SEO</h2></td>
			</tr>
			<tr>
				<td class="" width="100" height="40">&lt;title&gt;:</td>
				<td><input name="seo_title" type="text" class="p_name" id="" value="<?php echo $product['seo_title']; ?>"></td>
			</tr>
			<tr>
				<td class="" width="100" height="40">&lt;description&gt;:</td>
				<td><input name="seo_des" type="text" class="p_name" id="" value="<?php echo $product['seo_des']; ?>"></td>
			</tr>
			<tr>
				<td class="" width="100" height="40">&lt;keywords&gt;:</td>
				<td><input id="chpu-input" name="seo_key" type="text" class="p_name" value="<?php echo $product['seo_key']; ?>" /></td>
			</tr>
		</table>
     </form>    
         </td>
    </tr>
  </table><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:25px;">
  <tr>
    <td>
	<form id="catalog-extra-images-form" action="" method="post">
	<div id="catalog-extra-images-field">
		<?php if (count($product['extra_photo']) > 0) {
				foreach ($product['extra_photo'] as $id) { ?>
			<!-- big = /upload/more/ -->
			<div data-id="<?=$id;?>" class="imgcatm" id="extra-image-<?php echo $id; ?>">
				<div style="float: left;">
					<img src="<?php echo getImageWebPath('product_extra_preview').$id; ?>.jpg" />
				</div>
				<div class="imgcatdel" style="float: left;">
					<img src="/img/dopfotodel.png" width="24" height="24" style="margin-bottom:10px;" />
					<br />
					<div style="margin-right:4px; float: right;">
						<div class="filter-checkbox-wrapper">
							<div class="filter-checkbox filter-checkbox-off">
								<input class="hidden-checkbox" type="checkbox" name="extra_photo[<?php echo $id;?>]" />
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } 
		} ?>
	</div>
	</form>
    <div style="margin-top:15px;"><div class="limain2"><a href="javascript:void(0);" onclick="checkAllImages();">Выделить все</a></div> 
		<div class="limain3"><a href="javascript:void(0);" onclick="deleteImages();">Удалить</a></div> 
		<div style="margin-top:10px;">
			<!-- <img src="/img/dopfoto.gif" width="142" height="40"> -->
			<div id="catalog-extra-image-upload" style="float: left; background: url('/img/dopfoto.gif'); width: 142px; height: 40px;">
				<form action="" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?php echo $catalogId; ?>" />
					<input type="file" name="extra_photos[]" multiple="multiple" />
				</form>
			</div>
			<img class="extra-loading" style="float: left; display: none;" src="/img/ajax_loading.gif" alt="" />
		</div>
		<div class="cl"></div>
		<div class="info-text">(Авторесайз: вкл. Размеры превью: <?php echo Config::get('image_size.product_extra_preview'); ?>)</div>
		<a href="javascript:void(0)" class="image_make_resize">[Перересайзить изображения]</a>
	</div>
    </td>
  </tr>
  <tr>
	<td>
		&nbsp;
	</td>
  </tr>
  <tr>
	<td>
		<h3>Постер</h3>
		<div id="catalog-poster-image-upload">
			<form action="" method="post" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $catalogId; ?>" />
				<input type="file" name="poster" />
			</form>
			<input type="button" class="delete" value="Удалить постер"/>
			<div class="content-field"><?php if ($product['poster'] == 1) { ?>
				<img src="<?=getImageWebPath('product_poster').$product['id']?>.jpg" />
			<?php } ?></div>
			<img class="a-loading" style="float: left; display: none;" src="/img/ajax_loading.gif" alt="" />
			
		</div>
	</td>
  </tr>
</table>
</div>
<br />
<br />
<div id="feedback-field">
	<h2>Отзывы о товаре</h2>
	<div style="margin-top:20px;">
		<h3>Добавить отзыв:</h3>
		<form id="feed_form">
			<input id="fn" name="feed_name" type="text" style="margin:5px;" placeholder="Имя"></br>
			<input id="fe" name="feed_email" type="text" style="margin-left: 5px;" placeholder="Email"></br>
			<input id="fd" name="feed_date type="text" style="margin-left: 5px;" placeholder="Дата"></br>
			<textarea id="feed-editor" name="add_feed" cols="70" rows="7" style="width: 400px;"><?php //echo $product['like_desc']; ?></textarea>
			<script type="text/javascript">
				$( '#feed-editor' ).ckeditor();
			</script>
		</form>
		<a id="add-feed-btn" style="margin: 10px 0 20px 5px;" class="btn">Добавить</a> 
	</div>
	<? foreach ($data['feedback'] as $feed) { ?>
		<div class="feed" data-id="<?=$feed['id'];?>" id="feed<?=$feed['id']?>">
			<div class="header">
				<div class="manage-buttons">
					<a class="confirm btn btn-success" <?=($feed['confirm']==1?'disabled="disabled"':'')?>><i class="icon-ok"></i></a>&nbsp;
					<a class="delete btn btn-danger"><i class="icon-trash icon-white"></i></a>
				</div>
				<div><span class="name"><?=$feed['name']?></span>&nbsp;<span class="date"><?=date('d.m.Y H:i', $feed['date'])?></span></div>
				<div><span class="email"><?=$feed['email']?></span></div>
			</div>
			<div class="comment"><?=$feed['comment']?></div>
			<div class="answer-field">
				<div class="answer"><?=$feed['answer']?></div>
				<a class="answer-button btn"><i class="icon-comment"></i>&nbsp;Ответить</a> 
				<div class="answer-form">
					<textarea name=""><?=$feed['answer'];?></textarea>
					<br />
					<input type="button" name="answer" value="Ответить" /> <span class="loading"><img src="/img/ajax_loading.gif" alt="Загрузка" /></span>
				</div>
			</div>
		</div>
	<? } ?>
</div>
      
      </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="80">&nbsp;</td>
  </tr>
</table>

</div>
</div>

<?php include ("../down.php"); ?>
<?php include ("catalog-related/related_products_widget.php"); ?>
