<?php
include ("../connect.php");
include ("../../func/core.php");
?>
<?php 
$cssOl = true;
include ("../up.php");

//Супер расчеты тут
$listingType = 'cat';

if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
	//print_r(getSort());
	//print_r(getPage());
	$id = $_REQUEST['id'];
	$cat = getCatInfo($id);
	
	$filter['cat'] = $id;

	/* Если была нажата кнопка пересортировки */
	if (isset($_POST['sort_rests_up'])) {
		$data = getProducts(false, $filter, false, $page);
		$data = $data['products'];
		
		function sort_up($a, $b) {
			/* Все что с наличием поднимаем вверх, потом связанные товары без наличия (т.е. с известной маржой), все остальное вниз */
			$ra = $a['rests_main']['main'] + $a['rests_main']['extra'];
			$rb = $b['rests_main']['main'] + $b['rests_main']['extra'];
			$ma = $a['net_cost'] > 0 ? abs($a['price_after_discount'] - $a['net_cost']) : 0;
			$mb = $b['net_cost'] > 0 ? abs($b['price_after_discount'] - $b['net_cost']) : 0;
			
			if ($ra != 0 || $rb != 0) {
				//Сортируем по наличию
				if ($ra == $rb)
					return 0;
				return ($ra < $rb) ? 1 : -1;
			} elseif ($a['net_cost'] > 0 || $b['net_cost'] > 0) {
				//Сортируем по марже
				if ($ma == $mb)
					return 0;
				return $ma < $mb ? 1 : -1;
					
			} else {
				if ($a['spec_rang'] == $b['spec_rang'])
					return 0;
				return ($a['spec_rang'] < $b['spec_rang']) ? 1 : -1;
			}
		}
		
		//Сортируем
		usort($data, 'sort_up');
		
		//Сохраняем сортировку
		$i = 0;
		
		foreach ($data as $p) {
			mysql_query("
					UPDATE
						`catalog`
					SET
						`spec_rang` = '{$i}'
					WHERE
						`id` = '{$p['id']}'
				");
			$i++;
		}
	}
	$page = array(
			'page' => getPage(),
			'onPage' => 'all'
		);
		
	$data = getProducts(false, $filter, getSort(), $page);
	
	//Выбираем данные о псевдокатегории\подкатегории\тегам (с названием трудно определится, везде по разному)
	$tags = getTagsInCat($id);
	$listingType = 'cat';
} elseif (isset($_REQUEST['tag_id']) && is_numeric($_REQUEST['tag_id'])) {
	$tagId = $_REQUEST['tag_id'];
	
	$tagInfo = getTagInfo($tagId);
	$id = $tagInfo['cat_id'];
	$filter = array();
	$filter['tag'] = $tagId;
	
	$data = getProducts(false, $filter, getSort(), false);
	$listingType = 'tag';
}
//
?>
<script type="text/javascript" src="/js/jquery-ui-1.8.13.js"></script>
<script src="/js/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		
		//Биндим изменение цены
		$('.price').bind('change', function(e) {
			var id = $(this).closest('tr').data('id');
			var price = $(this).val();

			if ($.isNumeric(price) && price >= 0) {
				updatePrice(id, price);
			} else {
				alert('Неверная цена');
			}
		});
		//Биндим изменение bid
		$('.bid').bind('change', function(e) {
			var id = $(this).closest('tr').data('id');
			var art = $(this).val();

			updateBid(id, art);
		});
		
		//Биндим изменение цены доставки
		$('.delivery-cost-online').bind('change', function(e) {
			var id = $(this).closest('tr').data('id');
			var cost = $(this).val();
			var field = 'delivery_cost';
			var deliveryCurrentField = $(this).closest('tr').find('.delivery-cost');
			
			updateField(id, field, cost, function(){
				if (cost == '') {
					//Отправим запрос на новую цену, ту что по дефолту выставляется.
					$.ajax({
						url: 'ajax.php?method=get_delivery_cost',
						type : 'POST',
						dataType : 'json',
						data : {"id" : id},
						beforeSend : function () {
							deliveryCurrentField.text('?');
						},
						success : function (data, textStatus, jqXHR) {
							if (data.error) {
								//do nothing :)
							} else {
								//хайдим
								deliveryCurrentField.text(data.delivery_price);
							}
						},
						error : function (jqXHR, textStatus, errorThrown) {}
					});
				} else {
					deliveryCurrentField.text(cost);
				}
			});
		});
		
		$('.product-name').hover(function(event){
			var row = $(this);
			var img = row.find('.product-image');
			img.css({
				"display" : "block",
				"position" : "absolute",
				"top" : row.position().top + (row.height() / 2) - (img.height() / 2),
				"left" : row.position().left - img.width() - 5
			});
		},function (event) {
			$(this).find('.product-image').hide();
		});
		
		//Hello products sort
		$('.catalog-field-list table tbody').sortable({
			connectWith: "tr.element",
			//connectWith: "tbody tr",
			tolerance : "pointer",
			update : function (event, ui) {
		
				$('.catalog-field-list').css('opacity', .5);
				
				var sortList = new Array();
				var cat_id = <?=$id?>;
				var tag_id = $('#catalog-table').data('tag-id');
				
				$('.catalog-field-list table tr.element').each(function (index, element) {
					sortList.push({
						'id' : $(element).data('id'),
						'sort' : index
					});
				});
				
				$.ajax({
					url: 'ajax.php?method=setsort',
					type : 'POST',
					dataType : 'json',
					data : {"sort_list" : sortList, "cat_id" : cat_id, "tag_id" : tag_id},
					beforeSend : function () {},
					success : function (data, textStatus, jqXHR) {
						if (data.error) {
							//do nothing :)
						} else {
							//хайдим
							$('.catalog-field-list').css('opacity', 1);
						}
						$('.catalog-field-list table tr.element:even').addClass('even').removeClass('odd');
						$('.catalog-field-list table tr.element:odd').addClass('odd').removeClass('even');
					},
					error : function (jqXHR, textStatus, errorThrown) {}
				});
			}
		});
		
		$('.no10med-link').bind('click', function() {
			var no10medButton = $(this);
			var yes10medButton = $(this).closest('td').find('.10med-link');
			var elementId = $(this).closest('tr.element').data('id');
			var popWindow = $('#window-10med-linking');
				popWindow.css({"top" : $(window).scrollTop() + 100});
				
			var productName = $(this).closest('tr.element').find('.product-name a').text();
			var content = $('#window-10med-linking .content');
			var productsList = $('#window-10med-linking .products-list');
			//Выбираем пример оформления товара
			var element = $('#window-10med-linking .element-example').clone();
			
			//Запиливаем название товара в окошко
			popWindow.find('.caption span').text(productName);
			popWindow.fadeIn('fast');
			
			//Скрываем ошибки
			popWindow.find('.error').hide();
			//Скрываем контент
			content.hide();
			
			//Показываем индикатор загрузки
			popWindow.find('.loading').show();
			
			
			//Пошел аякс к m_catalog, чтобы тот запросил данные с 10med и все такое
			$.ajax({
					url: 'ajax.php?method=want_to_link',
					type : 'POST',
					dataType : 'json',
					data : {"id" : elementId},
					beforeSend : function () {},
					success : function (data, textStatus, jqXHR) {
						if (!data.error) {
							//Заполняем данными наше попап окно
							
							//Очищаем список
							productsList.empty();
							//Выводим список подсказок
							for (var i in data) {
								var el = data[i];
								var newElement = $('<div>').html(element.html())
								newElement.addClass('element').data('id', el.id);
								newElement.find('.name a').text(el.title);
								newElement.find('.name a').attr('href', 'http://10med.ru/product/'+el.id);
								newElement.find('.image img').attr('src', el.image);
								newElement.find('.price').text(el.price);
								newElement.find('.link-button button').data('id', el.id).bind('click', function() {
									popWindow.find('.loading').show();
									content.hide();
									/* linkProducts(local_id, remote_id); */

									linkProducts(elementId, $(this).data('id'));
								});

								productsList.append(newElement);
							}
							
							content.show();
							
						} else {
							switch (data.error_code) {
								case 1: popWindow.find('.error.already-linked').show(); break;
								case 2: popWindow.find('.error.not-found').show(); break;
								default: popWindow.find('.error.unknow').show(); break;
							}
						}
					},
					error : function (jqXHR, textStatus, errorThrown) {
						alert("Ошибка при попытке осуществить Ajax запрос");
					},
					complete : function ( jqXHR, textStatus ) {
						popWindow.find('.loading').hide();
					}
				});
			popWindow.find('input[name="manual_search_button"]').unbind('click');
			popWindow.find('input[name="manual_search_button"]').bind('click', function() {
				var inputText = popWindow.find('input[name="manual_search"]');
				var searchButton = $(this);
				
				if (inputText.val().length == 0)
					return;
				
			//Пошел аякс к m_catalog, чтобы тот запросил данные с 10med и все такое
			$.ajax({
					url: 'ajax.php?method=manual_search',
					type : 'POST',
					dataType : 'json',
					data : {"search_text" : inputText.val()},
					beforeSend : function () {
						//Скрываем контент
						content.hide();
						//Показываем индикатор загрузки
						popWindow.find('.loading').show();
					},
					success : function (data, textStatus, jqXHR) {
						if (!data.error) {
							//Заполняем данными наше попап окно
							
							//Очищаем список
							productsList.empty();
							//Выводим список подсказок
							for (var i in data.suggestion) {
								var el = data.suggestion[i];
								var newElement = $('<div>').html( element.html() )
								newElement.addClass('element').data('id',el.id);
								newElement.find('.name a').text(el.name);
								newElement.find('.name a').attr('href', el.catalog_url);
								newElement.find('.image img').attr('src', el.image_url);
								newElement.find('.price').text(el.price);
								newElement.find('.link-button button').data('id', el.id).bind('click', function () {
									popWindow.find('.loading').show();
									content.hide();
									/* linkProducts(local_id, remote_id); */
									
									linkProducts(elementId, $(this).data('id'));
								});
								
								productsList.append(newElement);
							}
							
							content.show();
							
						} else {
							switch (data.error_code) {
								case 1: popWindow.find('.error.already-linked').show(); break;
								case 2: popWindow.find('.error.not-found').show(); break;
								default: popWindow.find('.error.unknow').show(); break;
							}
						}
					},
					error : function (jqXHR, textStatus, errorThrown) {
						alert("Ошибка при попытке осуществить Ajax запрос");
					},
					complete : function ( jqXHR, textStatus ) {
						popWindow.find('.loading').hide();
					}
				});
			});
			
			function linkProducts(local_id, remote_id) {
				$.ajax({
					url: 'ajax.php?method=link',
					type : 'POST',
					dataType : 'json',
					data : {"local_id" : local_id, "remote_id" : remote_id},
					beforeSend : function () {},
					success : function (data, textStatus, jqXHR) {
						if (!data.error) {
							no10medButton.hide();
							yes10medButton.show();
							$('#window-10med-linking .close-button').trigger('click');
						} else {
							alert('Почему то не связал о_О');
						}
					},
					error : function (jqXHR, textStatus, errorThrown) {
						alert("Ошибка при попытке осуществить Ajax запрос");
					},
					complete : function ( jqXHR, textStatus ) {
						popWindow.find('.loading').hide();
					}
				});
			}
		});
		$('#window-10med-linking .close-button').bind('click', function(){
			var popWindow = $('#window-10med-linking');
			popWindow.find('.products-list').empty();
			popWindow.find('input[name="manual_search"]').val('');
			popWindow.fadeOut('fast');
		});
		
		$('#window-10med-unlinking .close-button').bind('click', function(){
			var popWindow = $('#window-10med-unlinking');
			popWindow.fadeOut('fast');
		});
		
		$('.10med-link').bind('click', function() {
			var no10medButton = $(this).closest('td').find('.no10med-link');
			var yes10medButton = $(this);

			var elementId = $(this).closest('tr.element').data('id');
			
			var popWindow = $('#window-10med-unlinking');
			popWindow.css({"top" : $(window).scrollTop() + 100});
			
			var body = popWindow.find('.body');
			body.hide();
			
			var loading = popWindow.find('.loading');
			loading.show();
			
			var loading2 = popWindow.find('.loading-2');
			loading2.hide();
			
			var productName = $(this).closest('tr.element').find('.product-name a').text();
			popWindow.find('h2').text(productName);

			popWindow.show();
			
			var unlinkButton = popWindow.find('.button.unlink a');
			unlinkButton.unbind('click');
			
			$.ajax({
					url: 'ajax.php?method=get_linked_product',
					type : 'POST',
					dataType : 'json',
					data : {"local_id" : elementId},
					beforeSend : function () {},
					success : function (data, textStatus, jqXHR) {
						if (!data.error) {
							popWindow.find('.product-name a').attr('href', data.url).text(data.title);
							popWindow.find('.price span').text(data.price);
							popWindow.find('.image img').attr('src', data.image);
							body.show();
							unlinkButton.bind('click', function() {
								$.ajax({
									url: 'ajax.php?method=unlink',
									type : 'POST',
									dataType : 'json',
									data : {"local_id" : elementId},
									beforeSend : function () {
										loading2.show();
									},
									success : function (data, textStatus, jqXHR) {
										no10medButton.show();
										yes10medButton.hide();
										
										
										popWindow.find('.close-button').trigger('click');
									},
									error : function (jqXHR, textStatus, errorThrown) {
										alert("Ошибка при попытке осуществить Ajax запрос");
									},
									complete : function ( jqXHR, textStatus ) {
										loading2.hide();
									}
								});
							});
						} else {
							alert(data.error_message);
						}
					},
					error : function (jqXHR, textStatus, errorThrown) {
						alert("Ошибка при попытке осуществить Ajax запрос");
					},
					complete : function ( jqXHR, textStatus ) {
						popWindow.find('.loading').hide();
					}
			});
		});
		stylizeCheckbox();
		
				//Выделить всё
					$('#fk_stck_selector').change(function() {
						$('input[type="checkbox"].hide_box').attr('checked', this.checked).trigger('change');		
					});
					//Выделить всё - 2 
					$('#hide_products').change(function() {
						$('input[type="checkbox"].hide_box').attr('checked', this.checked).trigger('change');		
					});
					//Аякс на установку фейкового НЕналичия
					$('input[type="checkbox"].hide_box').change(function() {
						var id = $(this).data('id');
						var check = this.checked;
						$.ajax({
							url : 'ajax.php?method=hide_products',
							type : 'POST',
							dataType : 'json',
							data : {
								"id" : id,
								"check" : check
							},
							beforeSend : function() {
							},
							success : function(data, textStatus, jqXHR) {

							},
							error : function(jqXHR, textStatus, errorThrown) {

							},
							complete : function(jqXHR, textStatus) {
							}
						});

					});

		$('#fk_stck').change(function() {			
				$('input[type="checkbox"].fake_box').attr('checked', this.checked).trigger('change');
				//$('input[type="checkbox"].fake_box').first().trigger('change');
		});
		
		//Аякс на установку фейкового наличия
		$('input[type="checkbox"].fake_box').change(function() {
			var id = $(this).data('id');
			var check = this.checked;
		/*	var checkList = new Array();
			$('input[type="checkbox"].fake_box').each(function (index, element) {
				checkList.push({
					'id' : $(element).data('id'),
					'check' : element.checked			
				});
			});*/
			$.ajax({
				url: 'ajax.php?method=fake_in_stock',
				type: 'POST',
				dataType : 'json',
				data : {"id" : id, "check" : check},
				beforeSend : function () {},
				success : function (data, textStatus, jqXHR) {
				
				},
				error : function (jqXHR, textStatus, errorThrown) {
				
				},
				complete : function ( jqXHR, textStatus ) {}
					
				});
			
		
		
		});
		
	});
	//Удалить товар
	function deleteCatalog(id) {
		if (confirm("Вы уверены, что хотите удалить товар?")) {
			deleteProduct(id, function (data) {
				$('#catalog-' + id).fadeOut('slow', function () {
					$('#catalog-' + id).remove();
				});
			});
		}
	}

</script>
<div id="window-10med-linking">
	<div id="window-10med-linking-wrapper">
		<div class="close-button"><a href="javascript:void(0);">Закрыть</a></div>
		<h2 class="caption">Связываем: <span>Подушка МШК-010</span></h2>
		<div class="loading"><img src="/img/ajax_loading.gif" alt="loading" /> Загружаю...</div>
		<div class="content">
			<div class="manual-search">Ручной поиск: <input type="text" name="manual_search" /><input type="button" name="manual_search_button" value="Найти" /></div>
			<div class="products-list">
			</div>
			<div class="element-example" style="display: none;">
				<div class="image"><img src="http://10med.ru/upload/products/small/28269.jpg" /></div>
				<div class="description">
					<div class="name"><a href="#" target="_blank">Подушка ортопедическая</a></div>
					<div><span class="price">1500</span> руб</div>
					<br />
					<div class="link-button"><button>Связать</button></div>
				</div>
			</div>
		</div>
		<div class="error already-linked">
			Этот товар уже связан
		</div>
		<div class="error not-found">
			Товар не найден
		</div>
		<div class="error unknow">
			Неизвестная ошибка
		</div>
	</div>
</div>

<div id="window-10med-unlinking">
	<div id="window-10med-unlinking-wrapper">
		<div class="close-button"><a href="javascript:void(0);">Закрыть</a></div>
		<h2></h2>
		<div class="loading"><img src="/img/ajax_loading.gif" alt="loading" /> Загружаю...</div>
		<div class="body">
			<div class="image"><img src="" alt="" /></div>
			<div class="content">
				<div class="product-name"><a href="#" target="_blank">[нет данных]</a></div>
				<div class="price"><span>&mdash;</span> руб.</div>
				<div class="buttons">
					<div class="button unlink"><a href="javascript:void(0);" class="btn btn-danger">Развязать</a></div>
				</div>
				<div class="loading-2"><img src="/img/ajax_loading.gif" alt="loading" /></div>
			</div>
		</div>
		<div class="cl"></div>
	</div>
</div>

<div class="ol" style="width:100%; height:100%; position:absolute; z-index: 0;" align="center">
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="99%" align="center" valign="top">
	<table width="96%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="200" align="left" valign="top">
        <div style="margin-top:-10px;"></div>
        
		<?php foreach (getMenu() as $item) { ?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0"  style="margin-bottom:5px; margin-top:12px;">
				  <tr>
					<td width="14"><img src="/img/mainl.gif" width="14" height="31"></td>
					<td class="lmainrast">
					  <div class="txt_banner" style="margin-top:-3px;"><?php echo $item['title']; ?></div>
					  </td>
					<td width="14"><img src="/img/mainr.gif" width="14" height="31" /></td>
					</tr>
			</table>
			<?php if (count($item['submenu']) > 0) { ?>
				<?php foreach ($item['submenu'] as $submenu) { ?>
					<div class="limain"><a href="/edit/m_catalog/list_table_view.php?id=<?php echo $submenu['cat_id']; ?>"><?php echo $submenu['title']; ?> </a></div> 
				<?php } ?>
			<?php } ?>
		<?php }	?>
          
        </td>
        <td align="left" valign="top">
    
	
     <div style="margin-left:20px; margin-bottom:20px;"><h1><?php echo $cat['title']; ?></h1></div>
		<div class="txtzakser" style="margin-top:5px; margin-bottom:4px; float: right;"><a href="/edit/m_catalog/list.php?id=<?php echo $id; ?>">Переключить вид</a></div>
		<div>
			<form action="" method="post">
				<input type="submit" name="sort_rests_up" value="Связанные товары наверх" />
			</form>
		</div>
		<div class="catalog-field-list">
			<table width="100%">
				<thead>
					<tr class="odd">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>цена</td>
						<td>ставка в<br />маркете</td>
						<td>доставка по<br />Москве, руб</td>
						<td>выставить<br />доставку<br />по Москве</td>
						<td>маржа</td>
						<td>оборот</td>
						<td>остатки</td>
						<td>10med</td>
						<td>Наличие<input id="fk_stck" style="display: block; margin-left: 15px;" type="checkbox" name="select_all" value="1"/></td>
						<td>Hide<input id="hide_products" style="display: block; margin-left: 15px;" type="checkbox" name="select_all_selector" value="1"/></td>
					</tr>
				</thead>
				<tbody id="catalog-table" <? if ($tagId) { echo "data-tag-id=$tagId";}?>>
				<? $even = true; ?>
				<?php foreach ($data['products'] as $product) { ?>
					<tr data-id="<?=$product['id'];?>" class="element <?=($even ? 'even' : 'odd');?>" style="<?=($product['cat'] != $id) ? "border: 1px solid green; background-color: ghostwhite;" : ''?>">
						<td>
							<a href="javascript:void(0)"><img data-enabled="<?=($product['best']?1:0)?>" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'best')" src="/img/best<?=($product['best']?'':'s')?>.png" width="20" height="20" class="imgzn"></a>
							<a href="javascript:void(0)"><img data-enabled="<?=($product['sale']?1:0)?>" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'sale')" src="/img/sale<?=($product['sale']?'':'s')?>.png" width="20" height="20" class="imgzn"></a>
							<a href="javascript:void(0)"><img data-enabled="<?=($product['novinka']?1:0)?>" onclick="toggleProductAttr(this, <?php echo $product['id']; ?>, 'novinka')" src="/img/new<?=($product['novinka']?'':'s')?>.png" width="20" height="20" class="imgzn"></a>
						</td>
						<td class="product-name">
							<div class="product-image">
								<img src="<?php echo getImageWebPath('product_small').$product['id']; ?>.jpg" class="" />
							</div>
							<a href="/edit/m_catalog/edit.php?id=<?=$product['id'];?>&cat_id=<?=$id?>"><?=$product['title']?></a>
						</td>
						<td><input type="text" name="price" class="price span1" value="<?php echo $product['price']; ?>" /></td>
						<td><input type="text" name="bid" class="bid span1" value="<?=$product['market_cost_per_click'];?>" /></td>
						<td class="delivery-cost">
							<? /*
								Пока вот таким вот хаком. Не теряю надежды переписать расчет стоимости доставки с нуля.
							*/?>
							<?=deliveryPrice::getByCatAndPrice($product['cat'], $product['price_after_discount'], $product['id']);?>
						</td>
						<td>
							<input type="text" name="delivery_cost" class="delivery-cost-online span1" value="<?=$product['delivery_cost'];?>" />
						</td>
						<td><? if ($product['net_cost'] != 0) {
								$marg = ($product['price_after_discount'] - $product['net_cost']);
								if ($marg < 0) {
									echo "<span style=\"color:red;\">{$marg}</span>";
								} else {
									echo $marg;
								}
							} else {
								echo '&mdash;';
							} ?></td>
						<td>
							<span style="<?=($product['turnover'] > 0 ? 'color:green;':'')?>">
								<?=$product['turnover'];?>
							</span>
						</td>
						<td>
							<span style="<?=($product['rests_main']['main'] > 0 ? 'color:green;':'')?>">
								<?=$product['rests_main']['main']?>
							</span>
							&nbsp;|&nbsp;
							<span style="<?=($product['rests_main']['extra'] > 0 ? 'color:green;':'')?>">
								<?=$product['rests_main']['extra']?>
							</span>
						</td>
						<td>
							<a href="javascript:void(0)" style="<?=($product['linked_with_10med']?'display:none;':'')?>" class="no10med-link"><img src="/edit/icons/no10med.jpg" title="Не связан с 10med" style="vertical-align: middle;" /></a>
							<a href="javascript:void(0)" style="<?=($product['linked_with_10med']?'':'display:none;')?>" class="10med-link"><img src="/edit/icons/10med.jpg" title="Cвязан с 10med" style="vertical-align: middle;" /></a>
						</td>
						<td>
							<input class="fake_box" style="margin-left: 15px;" data-id="<?=$product['id'];?>" type="checkbox" name="fake_in_stock[<?=$product['id'];?>]" value="1" <? if ($product['fake_in_stock']==1)  echo 'checked'; ?>/>
						</td>
												<td>
							<input class="hide_box" style="margin-left: 15px;" data-id="<?=$product['id']; ?>" type="checkbox" name="hide[<?=$product['id']; ?>]" value="1" <?
							if ($product['is_hide'] == 1)
								echo 'checked';
 ?>/>
						</td>
					</tr>
				<? $even = !$even; } ?>
				</tbody>
			</table>
		</div>
      </td>
      </tr>
    </table>
	 
    <?php generatePaginator($data['page']); ?>
  
	</td>
  </tr>
  <tr>
    <td height="80">&nbsp;</td>
  </tr>
</table>

</div>
</div>
<?php include ("../down.php"); ?>