<?php
	$params = getCatParams($product['cat']);

	foreach($params as $param) {
?>
<h2><?php echo $param['title']; ?>:</h2>
<div class="params-field" style="margin-top:8px;">
	<?php
	if ($param['type'] == PARAM_SET) {
		$values = getCatAvailableValues($param['param_id'], $product['cat'], $product['id']);
		foreach($values as $value):
	?>
		<div class="filter-checkbox-wrapper">
			<label>
				<div class="filter-checkbox filter-checkbox-off">
					<input class="hidden-checkbox" type="checkbox" name="param_checkbox[<?php echo $value['id'];?>]" <?php echo ($value['enable'] ? 'checked="checked"':'')?>/>
				</div>
				<div class="divrazmer" style="float: left;">&nbsp;<?php echo $value['value']; ?></div>
			</label>
		</div>
		<?php endforeach; ?>
	<?php } elseif ($param['type'] == PARAM_VALUE) { ?>
		<select name="param_select[]">
			<option value="0">Не выбрано</option>
		<?php
		$values = getCatAvailableValues($param['param_id'], $product['cat'], $product['id']);
		foreach($values as $value): ?>
			<option value="<?=$value['id']?>" <?=($value['enable'] ? 'selected="selected"' : '')?>><?=$value['value']?></option>
		<?php endforeach; ?>
		</select>
	<? } elseif ($param['type'] == PARAM_RANGE) {
		$values = getCatAvailableValues($param['param_id'], $product['cat'], $product['id']);
		$currentVal = '';
		foreach($values as $value) {
			if ($value['enable']) {
				$currentVal = $value['value_float'];
				break;
			}
		}
		?>
		<div class="param-range" data-param_id="<?=$param['id']?>" data-cat_id="<?=($param['global'] ? 0 : $product['cat'])?>">
			<input type="text" name="param_range[<?=$param['id']?>][value]" class="span1 param-input" value="<?=$currentVal;?>" /> <?=$param['unit']?>
			<input type="hidden" name="param_range[<?=$param['id']?>][global]" value="<?=$param['global']?>" />
			<input type="hidden" name="param_range[<?=$param['id']?>][cat]" value="<?=$product['cat']?>" />
		</div>
	<? } ?>
	<div style="clear: both;"></div>
</div>
<?php } ?>
<!--
<script>
	$(function(){
		$('.param-range .param-input').bind('change', function() {
			var root = $(this).closest('.param-range');
			
			var value = $(this).val();
			
			var paramId = root.data('param_id');
			var catId = root.data('cat_id');
			
			if (value != '') {
				//Кидаем ajax на изменение параметра
				
			}
		});
	});
</script>
-->