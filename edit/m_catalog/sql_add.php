<?php
if (isset($_POST['add'])) {
	if (strlen($_POST['title']) > 2 && strlen($_POST['chpu']) > 2 ) {
		//цикл вместо эксцепшенов
		do {
			$data['cat'] = is_numeric($_POST['cat_id']) && $_POST['cat_id'] > 0 ? $_POST['cat_id'] : 0;			
			$data['title'] = mysql_real_escape_string($_REQUEST['title']);
			$data['price'] = is_numeric($_REQUEST['price']) ? $_REQUEST['price'] : 0;
			$data['fake_in_stock'] = ($_REQUEST['fake_in_stock'] == 1) ? 1 : 0;
			$data['art'] = mysql_real_escape_string($_REQUEST['article']);
			$data['short_des'] = mysql_real_escape_string($_REQUEST['short_des']);
			$data['des'] = mysql_real_escape_string($_REQUEST['desc']);
			$data['warranty'] = (int)$_REQUEST['warranty'];
			if (isset($_REQUEST['iframe'])){
				$newStr = $_REQUEST['iframe'];
				$height = 415;
				$width = 740;
				$newStr = preg_replace('|width="\d+"|', 'width="'.$width.'"', $newStr);
				$newStr = preg_replace('|height="\d+"|', 'height="'.$height.'"', $newStr);
				$newStr = preg_replace('|align="\w+"|', '', $newStr);
				$newStr = preg_replace('|style=".*;"|', '', $newStr);
				$data['iframe'] = mysql_real_escape_string($newStr);
			}
			$data['iframe_des'] = mysql_real_escape_string($_REQUEST['iframe_des']);
			$like_desc = trimDesc($_REQUEST['like_desc']);
			if (!empty($like_desc)) {
				$data['like_desc'] = mysql_real_escape_string($_REQUEST['like_desc']);
				$data['like_desc'] = preg_replace('/<br$/','',$data['like_desc']);
			} else {
				$data['like_desc'] = '';
			}

			$under_price_des = trimDesc($_REQUEST['under_price_des']);
	if (!empty($under_price_des)) {
		$data['under_price_des'] = mysql_real_escape_string($_REQUEST['under_price_des']);
		$data['under_price_des'] = preg_replace('/<br$/','',$data['under_price_des']);
	} else {
		$data['under_price_des'] = '';
	}

	$adv_des = trimDesc($_REQUEST['adv_des']);
	if (!empty($adv_des)) {
		$data['adv_des'] = mysql_real_escape_string($_REQUEST['adv_des']);
		$data['adv_des'] = preg_replace('/<br$/','',$data['adv_des']);
	} else {
		$data['adv_des'] = '';
	}
	
			$data['chpu'] = mysql_real_escape_string($_REQUEST['chpu']);
			
			$data['sale'] = isset($_REQUEST['sale']) ? 1 : 0;
			$data['best'] = isset($_REQUEST['best']) ? 1 : 0;
			$data['novinka'] = isset($_REQUEST['new']) ? 1 : 0;
			
			$data['seo_title'] = mysql_real_escape_string($_REQUEST['seo_title']);
			$data['seo_des'] = mysql_real_escape_string($_REQUEST['seo_des']);
			$data['seo_key'] = mysql_real_escape_string($_REQUEST['seo_key']);
			
			$dp = $_REQUEST['discount_percent'];
			$dv = $_REQUEST['discount_value'];
			if (!empty($dp) && $dp > 0) {
				$data['discount_type'] = 'percent';
				$data['discount_value'] = $dp;
			} elseif (!empty($dv) && $dv > 0 ) {
				$data['discount_type'] = 'value';
				$data['discount_value'] = $dv;
			} else {
				$data['discount_value'] = 0;
			}
			$data['price_after_discount'] = getPriceAfterDiscount($data['price'], $data['discount_type'] , $data['discount_value']);
			$data['spec_rang'] = 0;
			//Проверка ЧПУ на дубль
			if ( fetchOne("SELECT * FROM `catalog` WHERE `chpu` = '{$data['chpu']}'") ) break;
			
			//Выполняем сдвиг сортировки, чтобы воткнуть новый элемент в начало списка
			/*
			mysql_query("
				UPDATE
					`catalog`
				SET
					`spec_rang` = `spec_rang` + 1
				WHERE
					`cat` = '{$data['cat']}'
			"); */
			//для главной категории
			mysql_query("
				UPDATE 
					`catalog_2_cat`
				SET
					`spec_rang` = `spec_rang` + 1
				WHERE
					`cat_id` = '{$data['cat']}'
			");
			
			//для доп категорий
			updateSpecRang($_REQUEST['additional_cats']);
			
			//Добавляем запись в каталог
			$setString = getSetString($data);
			mysql_query("INSERT INTO
				`catalog`
				SET {$setString}
			");

			$id = mysql_insert_id();
			
			//Добавляем в `catalog_2_cat` главной категории
			//`spec_rang` по умолчанию = 0;
			mysql_query("INSERT INTO `catalog_2_cat` (`catalog_id`, `cat_id`)
											  VALUES ({$id}, {$data['cat']})");
			
			//Добавление в доп. категории
			linkAdditionalCats($id, $_REQUEST['additional_cats']);
			
			if ($id <= 0) break;
			
			//Сохранение основного фото
			if (isset($_FILES['main_image'])) {
				saveMainImage($_FILES['main_image'], $id);
			}
			//Сохранение дополнительных фото
			if (isset($_FILES['extra_photos'])) {
				for ($i = 0; $i < count($_FILES['extra_photos']['name']); $i++) {
					$photo = array(
						'name' => $_FILES['extra_photos']['name'][$i],
						'type' => $_FILES['extra_photos']['type'][$i],
						'tmp_name' => $_FILES['extra_photos']['tmp_name'][$i],
						'error' => $_FILES['extra_photos']['error'][$i],
						'size' => $_FILES['extra_photos']['size'][$i]
					);
					if ($photo['error'] != '0' || empty($photo['name'])) continue;
					$idAdded[] = saveExtraPhoto($photo, $id) ;
				}
			}
			//Сохранение созданных параметров
			//Массив с ID параметров
			$paramsIds = array();
			
			//Собираем id параметров с чекбоксов и селектов
			if (count($_POST['param_checkbox']) > 0 ) {
				foreach	($_POST['param_checkbox'] as $key => $value) {
					$paramsIds[] = $key;
				}
			}
			if (count($_POST['param_select']) > 0) {
				foreach	($_POST['param_select'] as $key => $value) {
					if ($value > 0) 
						$paramsIds[] = $value;
				}
			}
			
			//Массив для подготовки sql запроса
			$paramsSqlIds = array();

			//Готовим sql запрос
			foreach ($paramsIds as $pid) {
				$paramsSqlIds[] = "('{$id}', '{$pid}')";
			}
			//Добавляем параметры
			if (count($paramsSqlIds) > 0) {
				mysql_query("
					INSERT INTO
						`params_catalog_links` (`catalog_id`,`param_id`)
					VALUES 
					".implode(', ', $paramsSqlIds)
					);
			}
			
			//Сохранение тегов\подкатегорий
			linkCatalogAndTags($id, isset($_POST['tags']) ? array_keys($_POST['tags']) : array());
			
			header("Location: edit.php?id={$id}");
			die();
		} while (false);
	}
}