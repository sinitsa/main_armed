<?php
//====
//при смене категории пусть теряет все характеристики, которые не совпадают с новой категорией
//====



if ( (isset($_POST['save']) || isset($_POST['save_x']) || isset($_POST['save.x'])) && is_numeric($_REQUEST['id'])) {
	$id = $_REQUEST['id'];
	//Подготовливаем и изменяем данные о товаре
	//Категорию меняем отдельно
	file_put_contents('req.txt', var_export($_REQUEST, true));
	$data['title'] = mysql_real_escape_string($_REQUEST['title']);
	$data['fake_in_stock'] = ($_REQUEST['fake_in_stock'] == 1) ? 1 : 0;
	
	$data['price'] = is_numeric($_REQUEST['price']) ? $_REQUEST['price'] : getPrice($id);
	$data['art'] = mysql_real_escape_string($_REQUEST['article']);
	$data['short_des'] = mysql_real_escape_string($_REQUEST['short_des']);
	$data['des'] = mysql_real_escape_string($_REQUEST['desc']);
	$data['nomencl_10med'] = mysql_real_escape_string($_REQUEST['nomencl']);
	$data['warranty'] = (int)$_REQUEST['warranty'];
	if (isset($_REQUEST['iframe'])){
		$newStr = $_REQUEST['iframe'];
		$height = 415;
		$width = 740;
		$newStr = preg_replace('|width="\d+"|', 'width="'.$width.'"', $newStr);
		$newStr = preg_replace('|height="\d+"|', 'height="'.$height.'"', $newStr);
		$newStr = preg_replace('|align="\w+"|', '', $newStr);
		$newStr = preg_replace('|style=".*;"|', '', $newStr);
		$data['iframe'] = mysql_real_escape_string($newStr);
	}
	$data['iframe_des'] = mysql_real_escape_string($_REQUEST['iframe_des']);
	$like_desc = trimDesc($_REQUEST['like_desc']);
	if (!empty($like_desc)) {
		$data['like_desc'] = mysql_real_escape_string($_REQUEST['like_desc']);
		$data['like_desc'] = preg_replace('/<br$/','',$data['like_desc']);
	} else {
		$data['like_desc'] = '';
	}
	$under_price_des = trimDesc($_REQUEST['under_price_des']);
	if (!empty($under_price_des)) {
		$data['under_price_des'] = mysql_real_escape_string($_REQUEST['under_price_des']);
		$data['under_price_des'] = preg_replace('/<br$/','',$data['under_price_des']);
	} else {
		$data['under_price_des'] = '';
	}

	$adv_des = trimDesc($_REQUEST['adv_des']);
	if (!empty($adv_des)) {
		$data['adv_des'] = mysql_real_escape_string($_REQUEST['adv_des']);
		$data['adv_des'] = preg_replace('/<br$/','',$data['adv_des']);
	} else {
		$data['adv_des'] = '';
	}

	$data['chpu'] = mysql_real_escape_string($_REQUEST['chpu']);

	$data['seo_title'] = mysql_real_escape_string($_REQUEST['seo_title']);
	$data['seo_des'] = mysql_real_escape_string($_REQUEST['seo_des']);
	$data['seo_key'] = mysql_real_escape_string($_REQUEST['seo_key']);
	
	$dp = $_REQUEST['discount_percent'];
	$dv = $_REQUEST['discount_value'];
	if (!empty($dp) && $dp > 0) {
		$data['discount_type'] = 'percent';
		$data['discount_value'] = $dp;
	} elseif (!empty($dv) && $dv > 0 ) {
		$data['discount_type'] = 'value';
		$data['discount_value'] = $dv;
	} else {
		$data['discount_value'] = 0;
	}
	$data['price_after_discount'] = getPriceAfterDiscount($data['price'], $data['discount_type'] , $data['discount_value']);
	
	$sqlParams = array();
	foreach ($data as $key => $value) {
		$sqlParams[] = "`{$key}` = '{$value}'";
	}
	mysql_query("UPDATE
		`catalog`
		SET ".(implode(', ', $sqlParams))."
		WHERE `id` = '{$id}'
		");
	
	//echo "<pre>";
	//print_r($sqlParams);
	//echo "</pre>";
	//Массив с ID параметров
	$paramsIds = array();
	
	//Собираем id параметров с чекбоксов и селектов
	if (count($_POST['param_checkbox']) > 0 ) {
		foreach	($_POST['param_checkbox'] as $key => $value) {
			$paramsIds[] = $key;
		}
	}
	if (count($_POST['param_select']) > 0) {
		foreach	($_POST['param_select'] as $key => $value) {
			if ($value > 0) 
				$paramsIds[] = $value;
		}
	}
	if (count($_REQUEST['param_range']) > 0) {
		foreach	($_REQUEST['param_range'] as $key => $value) {
			if ($value['value'] != '') {
				//$value['value'] = iconv("utf-8", "windows-1251", urldecode($value['value']));
				$loloId = addAvailableParam(array(
					'paramId' => $key,
					'catId' => $value['global'] ? 0 : $value['cat'],
					'value' => '',
					'valueFloat' => $value['value']
				));
				if ($loloId) $paramsIds[] = $loloId;
			}
		}
	}
	//Выставляем параметры товару
	setProductsParams($id, $paramsIds);
	
	//Добавление в доп. категории

	linkAdditionalCats($id, $_REQUEST['additional_cats']);
	
	
	//Если у товара сменилась категория, удаляем все параметры, которые не совпадают с новой категорией
	//Вынесено отдельным пунктом, потому что смена категорий влияет на параметры товара
	if (is_numeric($_REQUEST['cat'])) $newCatId = $_REQUEST['cat'];
	$res = mysql_fetch_assoc(mysql_query("SELECT * FROM `catalog` WHERE `id` = '{$id}'"));

	if ($newCatId != $res['cat']) {
		catalogChangeCat($id, $newCatId);
	}
	/**/
	//param_checkbox
	//param_select
	//if (is_numeric($_REQUEST[''])) $data[''] = $_REQUEST[''];
	//$data[''] = mysql_real_escape_string($_REQUEST['']);
	
	/// Добавление или удаление привязки к псевдокатегориям
	foreach(getTagsInCat($_REQUEST['cat_id']) as $t) 
		$tagsInCat[] = $t['id'];
	$tagsToDelete = array_diff($tagsInCat, $_POST['tags']);
	//Сносим все привязки
	unlinkCatalogAndTags($id, $tagsToDelete);
	
	//Создаем новые
	linkCatalogAndTags($id, isset($_POST['tags']) ? array_keys($_POST['tags']) : array());


    if (isset($_FILES['instruction']) && $_FILES['instruction']['name'][0]!='' )
    {
        deleteInstruction($id);
        $res = saveInstruction($_FILES['instruction'], $id);

        //var_dump($res);
    }
}