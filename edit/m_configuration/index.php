<?php
include ("../connect.php");
include("../../func/core.php");
include ("../up.php"); 

if (isset($_POST['save']) && $_POST['save']) {
    foreach ($_POST['params'] as $key => $value) {
        Config::set($key, $value);
    }
}

$config = Config::getAll();
?>

<table width="90%" border="0" align="center" class="txt">

    <tr>
        <td width="10">&nbsp;</td>
        <td>
            <h4>Конфигурация</h4>
            <form action="" method="post">

                <?php
                $mConfig_keys = array_keys($config);

                $mConfig_pattern = array(
                    '_basic' => array(
                        'catalog',
                        'feedback',
                        'order',
                        'site'
                    ),
                    '_catalog' => array(
                        'image_autoresize',
                        'image_quality',
                        'image_size',
                        'path'
                    ),
                    '_templates' => array(
                        'template'
                    ),
                    '_search' => array(
                        'yandex_search'
                    )
                );
                ?>

                <div class="bs-docs-example" style="margin: 40px 0 0;">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active"><a href="#_basic" data-toggle="tab">Базовые настройки</a></li>
                        <li><a href="#_catalog" data-toggle="tab">Каталог</a></li>
                        <li><a href="#_templates" data-toggle="tab">Шаблоны</a></li>
                        <li><a href="#_search" data-toggle="tab">Поиск</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">

                        <?php
                        foreach ($mConfig_pattern as $mcp_key => $mcp_val) {
                            ?>

                            <div class="tab-pane <?= ($mcp_key == '_basic') ? 'fade in active' : ''; ?>" id="<?= $mcp_key; ?>">
                                <table class="table">

                                    <?php
                                    foreach ($config as $ck => $cc) {
                                        
                                        $mcp_need = $cc['key_group'];

                                        if (in_array($mcp_need, $mcp_val, true)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $cc['name']; ?></td>
                                                <td style="width: 250px;"><input type="text" name="params[<?php echo $cc['key_group'] . '.' . $cc['key']; ?>]" value="<?php echo $cc['value']; ?>" /></td>
                                            </tr>

                                            <?php
                                        }
                                    }
                                    ?>

                                </table>
                            </div>
                            <?php
                            ++$cci;
                        }
                        ?>

                    </div>

                    <table class="table">
                        <tr>
                            <td colspan="2">
                                <input class="btn btn-warning btn-large" type="submit" name="save" value="Применить изменения" />
                            </td>
                        </tr>
                    </table>

            </form>
        </td>
    </tr>
</table>
<?php include ("../down.php");	?>