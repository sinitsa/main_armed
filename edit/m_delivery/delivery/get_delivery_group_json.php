<?php

include ("connect_mysqli.php");

$dgRows = array();
$query = "SELECT * FROM `delivery_group` ORDER BY `sort` ASC";
if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
    	//$row['name'] = iconv('cp1251', 'utf-8', $row['name']);
		$dgRows[array_shift($row)] = $row;
    }
    $result->free();
}
$c2dgRows = array();
$query = "SELECT * FROM `category_2_delivery_group`";
if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
		$c2dgRows[$row['category_id']] = $row['delivery_group_id'];
    }
    $result->free();
}
$mysqli->close();

echo json_encode(array(
	'deliveryGroup'=>$dgRows,
	'category2deliveryGroup'=>$c2dgRows
));