<?
include ("../connect.php");
include ("../../func/core.php");

switch($_GET['method']){
	/*
			Пункты самовывоза
	*/
	case 'renameCity':
		$newTitle = mysql_real_escape_string($_REQUEST['newTitle']);
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if(trim($newTitle)!='' && $id!=0){
			mysql_query('update `deliveryPickups` set `title`="'.$newTitle.'" where `id`='.$id);
		}
	break;
	
	case 'deleteCity';
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if($id!=0){
			mysql_query('delete from `deliveryPickups` where `id`='.$id);
		}
	break;
	
	case 'addCity':
		$title = mysql_real_escape_string($_REQUEST['title']);
		if(trim($title)!=''){
			$center = mysql_real_escape_string($_REQUEST['center']);
			if($center==''){
				$center = '55.748523,37.650156';
			}
			$zoom = (is_numeric($_REQUEST['zoom']))?$_REQUEST['zoom']:10;
			mysql_query('insert into `deliveryPickups`(`title`,`adresses`,`count`,`center`,`zoom`,`geoJson`) values("'.$title.'","",0,"'.$center.'",'.$zoom.',"") ');
			$id = mysql_insert_id();
			$res = '<div class="city " data-id="'.$id.'" data-center="'.$center.'" data-zoom="'.$zoom.'"><span>'.$title.'</span> <span class="count">(0)</span><i class="icon-remove"></i></div>|<div class="points " id="p'.$id.'"></div>';
			echo $res;
		}
	break;
	
	case 'refreshCity':
		$title = mysql_real_escape_string($_REQUEST['title']);
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if(trim($title)!='' && $id!=0){
			$center = mysql_real_escape_string($_REQUEST['center']);
			if($center==''){
				$center = '55.748523,37.650156';
			}
			$zoom = (is_numeric($_REQUEST['zoom']))?$_REQUEST['zoom']:10;
			mysql_query('update `deliveryPickups` set `title`="'.$title.'", `center`="'.$center.'",`zoom`="'.$zoom.'" where `id`='.$id);
		}
	break;
	
	case 'refreshPoint':
		$adr = mysql_real_escape_string($_REQUEST['adr']);
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if(trim($adr)!='' && $id!=0){
			$adr = mb_substr($adr,0,mb_strlen($adr,"UTF-8")-1,"UTF-8");
			$adr = str_replace('&amp;','&',$adr);
			$count = substr_count($adr, '&') + 1;
			// $arr = explode('&',$adr);
			// $json = '[';
			// foreach($arr as $a){
				// $color = 0;
				// if(strpos($a,'#red')!==false){
					// $color = 1;
					// $xmlQ = str_replace('#red','',$a);
				// }
				// if(strpos($a,'#green')!==false){
					// $color = 2;
					// $xmlQ = str_replace('#green','',$a);
				// }
				// if(strpos($a,'#yellow')!==false){
					// $color = 3;
					// $xmlQ = str_replace('#yellow','',$a);
				// }
				// $file = simplexml_load_file('http://geocode-maps.yandex.ru/1.x/?geocode='.$xmlQ);
				// $pos = $file->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
				// $pos = str_replace(' ',',',$pos);
				// if($pos!=''){
					// $json .= "[".$pos."],";
				// }
			// }
			// echo $json;
			// $json = mb_substr($json, 0, mb_strlen($json, "UTF-8")-1,"UTF-8");
			// $json .= ']';
			// $json = mysql_real_escape_string($json);
			// echo 'update `deliveryPickups` set `adresses`="'.$adr.'", `count`='.$count.',`geoJson`="'.$json.'" where `id`='.$id;
			
			mysql_query('update `deliveryPickups` set `adresses`="'.$adr.'", `count`='.$count.',`geoJson`="" where `id`='.$id);
		}
	break;
	
	case 'addPoint':
		$adr = mysql_real_escape_string($_REQUEST['adr']);
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if(trim($adr)!='' && $id!=0){
			$count = substr_count($adr, '&') + 1;
			mysql_query('update `deliveryPickups` set `adresses`="'.$adr.'", `count`='.$count.' where `id`='.$id);
			$ad = mysql_result(mysql_query('select `adresses` from `deliveryPickups` where `id`='.$id),0);
			$arr = explode('&',$ad);
			$res = '';
			$count = 0;
			foreach($arr as $a){
				if($a!=''){
					$count++;
					$pos = strpos($a,'#');
					if($pos!==false){
						$color = substr($a, $pos);
						$color = str_replace('#','',$color);
						$text = substr($a, 0, $pos);
						if($color==''){
							$color = 'blue';
						}
					}else{
						$color = 'blue';
						$text = $a;
					}
					$res .= '<div class="point"><span>'.$text.'</span>
						<select class="select '.$color.'">
							<option value="blue" ';
							if($color=='blue'){$res .= 'selected="selected"';}
							$res .= '>Синий</option>
							<option value="red" ';
							if($color=='red'){$res .= 'selected="selected"';}
							$res .= '>Красный</option>
							<option value="green" ';
							if($color=='green'){$res .= 'selected="selected"';}
							$res .= '>Зеленый</option>
							<option value="orange" ';
							if($color=='orange'){$res .= 'selected="selected"';}
							$res .= '>Оранжевый</option>
						</select>
						<i class="icon-remove"></i></div>';
				}
			}
			echo $res;
		}
	break;
	
	
	
	/*
			Редактор доставок
	*/
	case 'renameGroup':
		$title = mysql_real_escape_string($_REQUEST['title']);
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if(trim($title)!='' && $id!=0){
			mysql_query('update `deliveryGroups` set `title`="'.$title.'" where `id`='.$id);
		}
	break;
	
	case 'renameRegion':
		$title = mysql_real_escape_string($_REQUEST['title']);
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if(trim($title)!='' && $id!=0){
			mysql_query('update `deliveryRegions` set `title`="'.$title.'" where `id`='.$id);
		}
	break;
	
	case 'newGroup':
		mysql_query('insert into `deliveryGroups`(`title`) values("Новая группа")');
		$id = mysql_insert_id();
		echo $id;
	break;
	
	case 'newRegion':
		mysql_query('insert into `deliveryRegions`(`title`) values("Новый регион")');
		$id = mysql_insert_id();
		echo $id;
	break;
	
	case 'deleteGroup':
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if($id!=0){
			mysql_query('delete from `deliveryGroups` where `id`='.$id);
			mysql_query('delete from `deliveryPrices` where `groupId`='.$id);
			mysql_query('delete from `deliveryCatLinks` where `groupId`='.$id);
		}
	break;
	
	case 'deleteRegion':
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:0;
		if($id!=0){
			mysql_query('delete from `deliveryRegions` where `id`='.$id);
			mysql_query('delete from `deliveryPrices` where `regionId`='.$id);
			mysql_query('delete from `deliveryRegLinks` where `regionId`='.$id);
		}
	break;
	
	case 'saveValue':
		$val = mysql_real_escape_string($_REQUEST['val']);
		$field = mysql_real_escape_string($_REQUEST['field']);
		$reg = (is_numeric($_REQUEST['reg']))?$_REQUEST['reg']:0;
		$gr = (is_numeric($_REQUEST['gr']))?$_REQUEST['gr']:0;
		if(trim($val)!='' && trim($field)!='' && $reg!=0 && $gr!=0){
			$co = mysql_result(mysql_query('select count(`id`) from `deliveryPrices` where `regionId`='.$reg.' and `groupId`='.$gr),0);
			if($co > 0){
				mysql_query('update `deliveryPrices` set `'.$field.'`='.$val.' where `regionId`='.$reg.' and `groupId`='.$gr);
			}else{
				echo 'insert into `deliveryPrice` set `'.$field.'`='.$val.', `regionId`='.$reg.', `groupId`='.$gr;
				mysql_query('insert into `deliveryPrices` set `'.$field.'`='.$val.', `regionId`='.$reg.', `groupId`='.$gr);
			}
		}
	break;
	
	case 'saveRegLink':
		$title = mysql_real_escape_string($_REQUEST['title']);
		$id = (is_numeric($_REQUEST['id']))?$_REQUEST['id']:false;
		if(trim($title)!='' && $id!==false){
			if($id==0){
				mysql_query('delete from `deliveryRegLinks` where `regionTitle`="'.$title.'"');
			}else{
				$count = mysql_result(mysql_query('select count(`id`) from `deliveryRegLinks` where `regionTitle`="'.$title.'"'),0);
				if($count==0){
					mysql_query('insert into `deliveryRegLinks`(`regionId`,`regionTitle`) values("'.$id.'","'.$title.'") ');
				}else{
					mysql_query('update `deliveryRegLinks` set `regionId`='.$id.' where `regionTitle`="'.$title.'"');
				}
			}
		}
	break;
	
	case 'saveGroupLink':
		$needChpu = (is_numeric($_REQUEST['chpu']))?$_REQUEST['chpu']:0;
		if($needChpu == 0){
			$catId = (is_numeric($_REQUEST['catId']))?$_REQUEST['catId']:0;
		}else{
			$chpu = mysql_real_escape_string($_REQUEST['catId']);
		}
		$groupId = (is_numeric($_REQUEST['groupId']))?$_REQUEST['groupId']:false;
		
		if( ($groupId!==false && $catId!=0 && $needChpu==0) || ($groupId!==false && $chpu!='' && $needChpu==1) ){
			if($groupId==0){
				if( $needChpu==0 ){
					mysql_query('delete from `deliveryCatLinks` where `catId`="'.$catId.'"');
				}else{
					mysql_query('delete from `deliveryCatLinks` where `chpu`="'.$chpu.'"');
				}
			}else{
				if( $needChpu==0 ){
					$count = mysql_result(mysql_query('select count(`id`) from `deliveryCatLinks` where `catId`="'.$catId.'"'),0);
					if($count==0){
						mysql_query('insert into `deliveryCatLinks`(`catId`,`groupId`,`chpu`) values("'.$catId.'","'.$groupId.'","") ');
					}else{
						mysql_query('update `deliveryCatLinks` set `groupId`='.$groupId.' where `catId`="'.$catId.'"');
					}
				}else{
					$count = mysql_result(mysql_query('select count(`id`) from `deliveryCatLinks` where `chpu`="'.$chpu.'"'),0);
					if($count==0){
						mysql_query('insert into `deliveryCatLinks`(`catId`,`groupId`,`chpu`) values(0,"'.$groupId.'","'.$chpu.'") ');
					}else{
						mysql_query('update `deliveryCatLinks` set `groupId`='.$groupId.' where `chpu`="'.$chpu.'"');
					}
				}
			}
		}
	break;


	/*
	* пройти по всем товарам и проставить стоимость доставки в зависимости от цены
	*/
	case 'changeDeliveryPrice':
		$q = mysql_query('select * from `catalog`');
		while( $r = mysql_fetch_array($q) ){
			$newDelPrice = 0;
			if( $r['price_after_discount'] <= 3000 ){
				$newDelPrice = 300;
			}elseif( $r['price_after_discount'] <= 4000 ){
				$newDelPrice = 250;
			}
			mysql_query('update `catalog` set `delivery_cost`='.$newDelPrice.' where `id`='.$r['id']);
		}
	break;
}