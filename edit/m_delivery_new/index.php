<?
include ("../connect.php");
include ("../../func/core.php");
include ("../up.php");

$groups = array();
$grQ = mysql_query('select * from `deliveryGroups`');
while($grR = mysql_fetch_array($grQ)){
	$groups[] = $grR;
}
$regs = array();
$grQ = mysql_query('select * from `deliveryRegions`');
while($grR = mysql_fetch_array($grQ)){
	$regs[] = $grR;
}

$prices = array();
$grQ = mysql_query('select * from `deliveryPrices`');
while($grR = mysql_fetch_array($grQ)){
	$prices[$grR['regionId']][$grR['groupId']] = $grR;
}

$regTitles = array();
$file = fopen('../../components/geolocation/data/ipgeobase/cities.txt','r');
if ($file) {
	while (!feof($file)){
		$temp = explode("\t",trim(fgets($file, 999)));
		if(!in_array(iconv('cp1251','utf-8',$temp[2]),$regTitles) && iconv('cp1251','utf-8',$temp[2])!=''){
			$regTitles[] = iconv('cp1251','utf-8',$temp[2]);			
		}
	}
}
fclose($file);
?>
<script src='/edit/css/nprogress/nprogress.js'></script>
<link rel='stylesheet' href='/edit/css/nprogress/nprogress.css'/>
<style>
	.allTable{
		width: 228px;
		border-left:solid 1px #ddd;
		display: inline-block;
	}
	.allTable thead th{
		text-align: center;
	}
	.allTable .title{
		position: relative;
	}
	.allTable .title .regTitle{
		width:130px;
		padding:0;
		margin:0;
		border: solid 1px rgba(0,0,0,0);
		background:rgba(0,0,0,0);
		outline: none;
		box-shadow:none;
		text-align: center;
		font-weight:bold;
	}
	.allTable .title .regTitle:focus{
		border: solid 1px rgba(0,0,0,0.5);
	}
	.allTable .title .icon-remove{
		position: absolute;
		right: -40px;
		top: 3px;
		cursor: pointer;
		display: none;
	}
	.allTable:hover .icon-remove{
		display: block;
	}
	
	.groups{
		display: inline-block;
		width: 149px;
	}
	.groups .regionsTitleFix{
		height: 21px;
	}
	.groups .field{
		width:130px;
		padding:0;
		margin:0;
		border: solid 1px rgba(0,0,0,0);
		background:rgba(0,0,0,0);
		outline: none;
		box-shadow:none;
	}
	.groups td{
		position: relative;
	}
	.groups td:hover .icon-remove{
		display: block;
	}
	.groups .icon-remove{
		position: absolute;
		right: 10px;
		top: 12px;
		cursor: pointer;
		display: none;
	}
	.groups td:hover .icon-cog{
		display: block;
	}
	.groups .icon-cog{
		position: absolute;
		right: 25px;
		top: 12px;
		cursor: pointer;
		display: none;
	}
	.groups .field:focus{
		border: solid 1px rgba(0,0,0,0.5);
	}
	.groups .newGroupLine > td{
		margin:0;
		padding: 0;
	}
	
	.allTable .regField{
		width: 30px;
		padding:0;
		margin:0;
	}
	
	.holder{
		display: inline-block;
		width:calc(100% - 175px);
		white-space: nowrap;
		overflow: auto;
		position: absolute;
		padding-bottom: 10px;
	}
	
	.addRegion{
		display: inline-block;
		width: 20px;
		float: left;
		height: 23px;
		padding-top: 6px;
		padding-left: 5px;
		cursor: pointer;
		background: #fff;
		position:absolute;
		right:0;
		border-left: solid 1px #ddd;
	}
	.addRegion:hover{
		background-color: rgba(0,0,0,0.2);
	}
	.addGroup{
		text-align: center;
		cursor: pointer;
		display: inline-block;
		height: 20px;
		width: 148px;
		padding-top: 5px;
	}
	.addGroup:hover{
		background: rgba(0,0,0,0.2);
	}
	
	.allTable .regPoint{
		width: 30px;
		padding: 0;
		margin: 0;
	}
</style>
<script>
	$(document).ready(function(){
		resizeBtn();
		
		$('.groups').on('blur','.field',function(){
			if($(this).data('val')!=$(this).val() && $(this).val()!=''){
				var elem = $(this);
				$.ajax({
					url : "ajax.php?method=renameGroup",
					type : "POST",
					data: 'title='+elem.val()+'&id='+elem.data('id'),
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						elem.data('val',elem.val());
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
		$('.holder').on('blur','.regTitle',function(){
			if($(this).data('val')!=$(this).val() && $(this).val()!=''){
				var elem = $(this);
				$.ajax({
					url : "ajax.php?method=renameRegion",
					type : "POST",
					data: 'title='+elem.val()+'&id='+elem.closest('table').data('reg'),
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						elem.data('val',elem.val());
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
		$('.holder').on('blur','.regField',function(){
			if($(this).data('val')!=$(this).val() && $(this).val()!=''){
				var elem = $(this);
				var gr = $( '.groups .'+elem.closest('tr').attr('class')+' .field' ).data('id');
				var field = elem.attr('name');
				$.ajax({
					url : "ajax.php?method=saveValue",
					type : "POST",
					data: 'val='+elem.val()+'&reg='+elem.closest('table').data('reg')+'&gr='+gr+'&field='+field,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						elem.data('val',elem.val());
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
		$('.addGroup').on('click',function(){
			var elem = $(this);
			$.ajax({
				url : "ajax.php?method=newGroup",
				type : "POST",
				beforeSend: function() {
					NProgress.start();
				},
				success : function (data) {
					var count = $('.groups tbody tr').length - 1;
					elem.closest('.newGroupLine').before('<tr id="g'+data+'" class="line'+count+'"><td><input type="text" data-id="'+data+'" data-val="Новая группа" value="Новая группа" class="field"><i class="icon-remove"></i></td></tr>');
					$('.allTable tbody').append('<tr class="line'+count+'"><td><input type="text" data-val="" value="" name="minTime" class="regField"> - <input type="text" name="maxTime" data-val="" value="" class="regField"></td><td><input type="text" data-val="" name="minPrice" value="" class="regField"> - <input type="text" name="maxPrice" data-val="" value="" class="regField"></td><td><input type="text" data-val="" value="" class="regPoint" /></td></tr>');
					resizeBtn();
				},
				complete : function() {
					NProgress.done();
				},
				error : function () {
				}
			});
		});
		$('.addRegion').on('click',function(){
			var elem = $(this);
			$.ajax({
				url : "ajax.php?method=newRegion",
				type : "POST",
				beforeSend: function() {
					NProgress.start();
				},
				success : function (data) {
					var str = '<table class="table table-striped allTable" data-reg="'+data+'"><thead>\
									<tr><th class="title" colspan="2"><input type="text" data-val="Новый регион" value="Новый регион" class="regTitle"><i class="icon-remove"></i></th></tr>\
									<tr><th>Срок</th><th>Цена</th></tr></thead><tbody>';
					var count = $('.groups tbody tr').length-1;
					for(var i=0; i<count; i++){
						str += '<tr class="line'+i+'">\
									<td><input name="minTime" type="text" data-val="" value="" class="regField"> - <input name="maxTime" type="text" data-val="" value="" class="regField"></td>\
									<td><input name="minPrice" type="text" data-val="" value="" class="regField"> - <input name="maxPrice" type="text" data-val="" value="" class="regField"></td>\
									<td><input type="text" data-val="" value="" class="regPoint" /></td>\
								</tr>';
					}
					str += '</tbody></table>';
					$('.holder > div').append(str);
					var left = $('.holder > div').width();
					$('.holder').animate({scrollLeft:left},800);
					resizeBtn();
				},
				complete : function() {
					NProgress.done();
				},
				error : function () {
				}
			});
		});
		
		$('.groups').on('click','.icon-remove',function(){
			if( confirm('Удалить группу?') ){
				var elem = $(this).closest('tr');
				$.ajax({
				url : "ajax.php?method=deleteGroup",
				type : "POST",
				data: 'id='+elem.find('.field').data('id'),
				beforeSend: function() {
					NProgress.start();
				},
				success : function (data) {
					var cl = elem.attr('class');
					$('.'+cl).remove();
					elem.remove();
					resizeBtn();
				},
				complete : function() {
					NProgress.done();
				},
				error : function () {
				}
			});
			}
		});
		
		$('.groups').on('mouseover','.icon-remove',function(){
			$('.groups .icon-remove').closest('td').removeAttr('style');
			var cl = $(this).closest('tr').attr('class');
			$('.allTable td').removeAttr('style');
			$('.allTable .'+cl+' td').css('background-color','rgb(253, 170, 170)');
			$(this).closest('td').css('background-color','rgb(253, 170, 170)');
		});
		$('.groups').on('mouseout','.icon-remove',function(){
			$('.groups .icon-remove').closest('td').removeAttr('style');
			$('.allTable td').removeAttr('style');
		});
		$('.holder').on('mouseover','.icon-remove',function(){
			$('.allTable td').removeAttr('style');
			$(this).closest('.allTable').find('td').css('background-color','rgb(253, 170, 170)');
		});
		$('.holder').on('mouseout','.icon-remove',function(){
			$('.allTable td').removeAttr('style');
		});
		
		$('.holder').on('click','.icon-remove',function(){
			if( confirm('Удалить регион?') ){
				var elem = $(this).closest('table');
				$.ajax({
					url : "ajax.php?method=deleteRegion",
					type : "POST",
					data: 'id='+elem.data('reg'),
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						elem.remove();
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
	});
	function resizeBtn(){
		$('.addRegion').height( $('.allTable').eq(0).height() - 8 );
		$('.addRegion .icon-plus').css('margin-top', Math.round($('.addRegion').height()/2)-14 );
	}
</script>
<?php
	$rCount = count($regs);
	$gCount = count($groups);
?>
<br />
<div style='text-align:center;'>
	<a href='index.php' class='btn active'>Сетка доставок</a>
	<a href='groups.php' class='btn'>Группы</a>
	<a href='regions.php' class='btn'>Регионы</a>
</div>
<br />
	<table class='table table-striped groups'>
		<thead>
			<tr><th class='regionsTitleFix'>Регионы</th></tr>
			<tr><th>Группы</th></tr>
		</thead>
		<?php $i=0; foreach($groups as $g){?>
			<tr id='g<?php echo $g['id'];?>' class='line<?php echo $i;$i++;?>'>
				<td>
					<input type='text' data-id='<?php echo $g['id'];?>' data-val='<?php echo $g['title'];?>' value='<?php echo $g['title'];?>' class='field' />
					<i class="icon-remove"></i>
				</td>
			</tr>
		<?php }?>
		<tr class='newGroupLine'><td><div class='addGroup'><i class="icon-plus"></i></div></td></tr>
	</table>
	<div class='holder'>
	<div>
	<?php foreach($regs as $r){?>
		<table class='table table-striped allTable' data-reg='<?php echo $r['id']?>'>
			<thead>
				<tr><th class='title' colspan='2'><input type='text' data-val='<?php echo $r['title'];?>' value='<?php echo $r['title'];?>' class='regTitle' /><i class="icon-remove"></i></th></tr>
				<tr><th>Срок</th><th>Цена</th></tr>
			</thead>
			<tbody>
				<?php for($i=0;$i<$gCount;$i++){?>
					<tr class='line<?php echo $i;?>'>
						<td><input name="minTime" type='text' data-val='<?php echo $prices[$r['id']][$groups[$i]['id']]['minTime']?>' value='<?php echo $prices[$r['id']][$groups[$i]['id']]['minTime']?>' class='regField' /> - <input name="maxTime" type='text' data-val='<?php echo $prices[$r['id']][$groups[$i]['id']]['maxTime']?>' value='<?php echo $prices[$r['id']][$groups[$i]['id']]['maxTime']?>' class='regField' /></td>
						<td><input name="minPrice" type='text' data-val='<?php echo $prices[$r['id']][$groups[$i]['id']]['minPrice']?>' value='<?php echo $prices[$r['id']][$groups[$i]['id']]['minPrice']?>' class='regField' /> - <input name="maxPrice" type='text' data-val='<?php echo $prices[$r['id']][$groups[$i]['id']]['maxPrice']?>' value='<?php echo $prices[$r['id']][$groups[$i]['id']]['maxPrice']?>' class='regField' /></td>
						<td><input name="pointPrice" type='text' data-val='<?php echo $prices[$r['id']][$groups[$i]['id']]['pointPrice'] ?>' value='<?php echo $prices[$r['id']][$groups[$i]['id']]['pointPrice'] ?>' class='regField' /></td>
					</tr>
				<?php }?>
			</tbody>
		</table>
	<?php }?>
	</div>
	</div>
	<div class='addRegion'><i class="icon-plus"></i></div>

    </td>
  </tr>
</table>

</body>
</html>