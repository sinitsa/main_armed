<?
include ("../connect.php");
include ("../../func/core.php");
include ("../up.php");

$arr = array();
$pQ = mysql_query('select * from `deliveryPickups`');
while($pR = mysql_fetch_array($pQ)){
	$arr[] = $pR;
}
?>

<script src='/edit/css/nprogress/nprogress.js'></script>
<link rel='stylesheet' href='/edit/css/nprogress/nprogress.css'/>
<link rel='stylesheet' href='modal.css'/>

<style>
	.content{
		height: 100%;
		display: inline-block;
		width: 100%;
		border-top: solid 1px black;
	}
	.content .cities{
		width: 250px;
		display: inline-block;
		float: left;
		overflow: auto;
		position: fixed;
		border-bottom: solid 1px black;
	}
	.content .cities .city{
		padding: 5px;
		cursor: pointer;
		position: relative;
	}
	.content .cities .active{
		background: rgb(207, 108, 108);
	}
	.content .cities .city:hover{
		background: rgb(231, 158, 158);
		color: white;
	}
	.content .cities .city .icon-remove{
		position: absolute;
		right: 20px;
		top: 7px;
	}
	.content .cities .city .icon-remove:hover{
		border: solid 1px black;
		border-radius: 7px;
	}
	
	.content .addCity{
		display: inline-block;
		position: fixed;
		bottom: 0;
		left: 0;
		width: 240px;
		padding: 5px;
		text-align: center;
		cursor: pointer;
		background:rgb(171, 174, 231);
		border-right: solid 1px black;
	}
	.content .addCity:hover{
		background:rgb(104, 109, 213);
		color: white;
	}
	.content .addPoint{
		display: inline-block;
		position: fixed;
		bottom: 0;
		left: 251px;
		width: 240px;
		padding: 5px;
		text-align: center;
		cursor: pointer;
		background:rgb(171, 174, 231);
		border-right: solid 1px black;
	}
	.content .addPoint:hover{
		background:rgb(104, 109, 213);
		color: white;
	}
	
	.content .pointsHolder{
		display: inline-block;
		width: calc(100% - 280px);
		float: left;
		overflow: auto;
		position: fixed;
		left:250px;
		padding-left: 30px;
		border-left: solid 1px black;
		border-bottom: solid 1px black;
	}
	.content .pointsHolder .points{
		display: none;
	}
	.content .pointsHolder .points.visible{
		display:block;
	}
	.content .pointsHolder .points .point{
		padding: 5px;
		position: relative;
	}
	.content .pointsHolder .points .point:hover{
		background: rgb(231, 158, 158);
	}
	.content .pointsHolder .points .point .icon-remove{
		position: absolute;
		right: 20px;
		top: 7px;
		cursor: pointer;
	}
	.content .pointsHolder .points .point .select{
		position: absolute;
		right: 50px;
		top: 4px;
		width: 100px;
		cursor: pointer;
		height: 20px;
		padding: 0;
	}
	.content .pointsHolder .points .point .select.blue{background:rgb(115, 115, 245);color: #fff;}
	.content .pointsHolder .points .point .select.green{background:rgb(117, 212, 117);color: #000;}
	.content .pointsHolder .points .point .select.red{background:rgb(246, 153, 153);color: #fff;}
	.content .pointsHolder .points .point .select.orange{background:#ffb20c;color: #000;}
	.content .pointsHolder .points .point .select option{background:#fff;color: #000;}
	.content .pointsHolder .points .point .icon-remove:hover{
		border: solid 1px black;
		border-radius: 7px;
	}
</style>
<script>
var currId = 1;
	$(document).ready(function(){
		resizeAll();
		
		$('.cities').on('click','.city',function(){
			$('.cities .city').removeClass('active');
			$(this).addClass('active');
			$('.pointsHolder .points').removeClass('visible');
			$('#p'+$(this).data('id')).addClass('visible');
			currId = $(this).data('id');
		});
		
		$('.addCity').on('click',function(){
			showModal('city');
		});
		$('.addPoint').on('click',function(){
			showModal('point');
		});
		$('.modalBack').on('click',function(){
			hideModal();
		});
		
		$('.pointsHolder').on('change','.select',function(){
			var elem = $(this);
			var adr = makeAdressStr();
			$.ajax({
				url : "ajax.php?method=refreshPoint",
				type : "POST",
				data: 'adr='+encodeURIComponent(adr)+'&id='+currId,
				beforeSend: function() {
					NProgress.start();
				},
				success : function (data) {
					elem.removeClass('blue').removeClass('green').removeClass('yellow').removeClass('red');
					elem.addClass(elem.val());
				},
				complete : function() {
					NProgress.done();
				},
				error : function () {
				}
			});
			
		});
		
		$('.content .cities').on('click','.city .icon-remove',function(){
			if(confirm('Удалить город?')){
				var id = $(this).closest('.city').data('id');
				var elem = $(this).closest('.city');
				$.ajax({
					url : "ajax.php?method=deleteCity",
					type : "POST",
					data: 'id='+id,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						elem.remove();
						$('#p'+id).remove();
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
			return false;
		});
		$('.pointsHolder .points').on('click','.point .icon-remove',function(){
			if(confirm('Удалить пункт самовывоза?')){
				$(this).closest('.point').remove();
				var adr = makeAdressStr();
				$.ajax({
					url : "ajax.php?method=refreshPoint",
					type : "POST",
					data: 'adr='+encodeURIComponent(adr)+'&id='+currId,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						$('.cities .active').find('.count').text( '('+$('.pointsHolder .visible .point').length+')' );
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
			return false;
		});
		
		$('#newCityBtn').on('click',function(){
			if( $('#newCityTitle').val()!='' ){
				var title = $('#newCityTitle').val();
				var center = $('#newCityCenter').val();
				var zoom = $('#newCityZoom').val();
				$.ajax({
					url : "ajax.php?method=addCity",
					type : "POST",
					data: 'title='+title+'&center='+center+'&zoom='+zoom,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						var res = data.split('|');
						$('.content .cities').append(res[0]);
						$('.pointsHolder').append(res[1]);
						$('#newCityTitle').val('');
						hideModal();
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
		$('#editCityBtn').on('click',function(){
			if( $('#newCityTitle').val()!='' ){
				var title = $('#newCityTitle').val();
				var center = $('#newCityCenter').val();
				var zoom = $('#newCityZoom').val();
				$.ajax({
					url : "ajax.php?method=refreshCity",
					type : "POST",
					data: 'title='+title+'&center='+center+'&zoom='+zoom+'&id='+currId,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						//изменить центр
						$('.cities .active').data('center',center);
						//изменить зум
						$('.cities .active').data('zoom',zoom);
						//изменить титл
						$('.cities .active').find('span').eq(0).text(title);
						
						$('#newCityTitle').val('');
						hideModal();
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
		$('#newPointBtn').on('click',function(){
			if( $('#newPointTitle').val()!='' ){
				var newAdr = $('#newPointTitle').val();
				
				var adr = makeAdressStr();
				
				adr += newAdr;
				if($('#newPointColor').val()!='blue'){
					adr += '#'+$('#newPointColor').val();
				}
				$.ajax({
					url : "ajax.php?method=addPoint",
					type : "POST",
					data: 'adr='+encodeURIComponent(adr)+'&id='+currId,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						$('#p'+currId).empty().html(data);
						$('#newPointTitle').val('');
						hideModal();
						$('.cities .active').find('.count').text( '('+$('.pointsHolder .visible .point').length+')' );
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
		
		$('.content .cities').on('dblclick','.city',function(){
			clearSelection();
			$('#newCityTitle').val( $(this).find('span').eq(0).text() );
			showModal('cityEd');
		});
		$('.content .pointsHolder .points').on('dblclick','.point',function(e){
			clearSelection();
			if(!$(e.target).hasClass('select')){
				var prev = $(this).find('span').text();
				var newTitle = prompt('Переименовать', prev);
				var btn = $(this).find('span');
				if(newTitle!==null && prev != newTitle){
					btn.text(newTitle);
					var adr = '';
					$('.pointsHolder .visible .point').each(function(i,e){
						adr += $(e).find('span').text();
						if($(e).find('.select').val()!='blue'){
							adr += '#'+$(e).find('.select').val();
						}
						adr += '&';
					});
					$.ajax({
						url : "ajax.php?method=refreshPoint",
						type : "POST",
						data: 'adr='+encodeURIComponent(adr)+'&id='+currId,
						beforeSend: function() {
							NProgress.start();
						},
						success : function (data) {
							
						},
						complete : function() {
							NProgress.done();
						},
						error : function () {
						}
					});
				}
			}
		});
		
		$('.content .cities').on('mouseover','.city .icon-remove',function(){
			$(this).closest('.city').css('background','rgb(250, 178, 111)');
		});
		$('.content .cities').on('mouseout','.city .icon-remove',function(){
			$(this).closest('.city').removeAttr('style');
		});
		$('.content .pointsHolder .points').on('mouseover','.point .icon-remove',function(){
			$(this).closest('.point').css('background','rgb(250, 178, 111)');
		});
		$('.content .pointsHolder .points').on('mouseout','.point .icon-remove',function(){
			$(this).closest('.point').removeAttr('style');
		});
	});
	
	window.onresize = function(){resizeAll();}
	
	function resizeAll(){
		$('.cities').height($(window).height() - 116 - $('.addCity').height() );
		$('.pointsHolder').height($(window).height() - 116 - $('.addCity').height() );
	}
	function clearSelection() {
		if (window.getSelection) {
			window.getSelection().removeAllRanges();
		} else { // старый IE
			document.selection.empty();
		}
	}
	function makeAdressStr(){
		var adr = '';
		$('.pointsHolder .visible .point').each(function(i,e){
			adr += $(e).find('span').text();
			if($(e).find('.select').val()!='blue'){
				adr += '#'+$(e).find('.select').val();
			}
			// adr += '&amp;';
			adr += '&';
		});
		
		return adr;
	}
	
	function showModal(type){
		$('.modalBack').show();
		if(type==='city'){
			$('#newCityTitle').val('');
			$('#newCityBtn').show();
			$('#editCityBtn').hide();
			$('.newCity').show();
			init('new');
		}else if(type==='point'){
			$('.newPoint').show();
		}else if(type==='cityEd'){
			$('.newCity').show();
			init('edit');
			$('#editCityBtn').show();
			$('#newCityBtn').hide();
		}
	}
	function hideModal(){
		$('.modalBack').hide();
		$('.newCity').hide();
		$('.newPoint').hide();
		if(myMap!=undefined){
			myMap.destroy();
			myMap = undefined;
		}
	}
	var myMap;var coords;var myCollection;var adressArr=[];
	function init(type){
		if ( !myMap ){
			if(type=='new'){
				var center = '55.748523,37.650156';
				var mapZoom = 10;
			}else{
				var center = $('.cities .active').data('center');
				var mapZoom = $('.cities .active').data('zoom');
				var adr = '';
				var num=false;
				$('.pointsHolder .visible .point').each(function(i,e){
					if(!num){
						num = true;
					}else{
						adr += '&'
					}
					adr += $(e).find('span').text();
					adr += '#'+$(e).find('.select').val();
				});
				adressArr = [];
				adressArr = adr.split('&');
			}
			
			var centerArr = center.split(',');
			$('#newCityCenter').val(center);
			$('#newCityZoom').val(mapZoom);
			
			myMap = new ymaps.Map ("myMap", {
				center: [0,0],
				zoom: 7
			});
			myMap.controls.add(
				new ymaps.control.ZoomControl()
			);
			myCollection = new ymaps.GeoObjectCollection();
			if(type!='new' && adr!=''){
				getMap(adressArr);
			}
			myMap.setCenter(centerArr);
			myMap.setZoom(mapZoom);
			myMap.events.add('boundschange', function (e) {
				$('#newCityCenter').val( e.get('newCenter') );
				$('#newCityZoom').val( e.get('newZoom') );
			});
		}
	}

	function getMap(a){
		myCollection.removeAll();
		myCollection = new ymaps.GeoObjectCollection();
		console.log(a);
		$(adressArr).each(function(i,e){
			var color;
			if(e.indexOf('#')==-1){
				color = 'blueIcon';
			}else{
				color = e.substr(e.indexOf('#')+1)+'Icon';
				e = e.substr(0,e.indexOf('#'));
			}
			var myGeocoder = ymaps.geocode(adressArr[i]);
			color = 'twirl#'+color;
			myGeocoder.then(
				function (res) {
					if(res.geoObjects.get(0)!=null){
					coords = res.geoObjects.get(0).geometry.getCoordinates();
					var myGeocoder = ymaps.geocode(coords, {kind: 'house'});
					myCollection.add(new ymaps.Placemark(coords,{
							balloonContent:e
						},{
							hideIconOnBalloonOpen: true,
							preset: color
						}
					));
					}
			});
		});
		myMap.geoObjects.add(myCollection);
	}
</script>

<div class="content">
	<div class="cities">
	<?
		$num = 0;
		foreach($arr as $pR){ $num++;
	?>
		<div class="city <?if($num==1){echo 'active';}?>" data-id='<?=$pR['id']?>' data-center='<?=$pR['center']?>' data-zoom='<?=$pR['zoom']?>'><span><?=$pR['title']?></span> <span class='count'>(<?=$pR['count']?>)</span><i class="icon-remove"></i></div>
	<?}
        ?>
	</div>
	<div class='addCity'>Добавить город</div>
	
	<div class="pointsHolder">
		<?$num = 0;foreach($arr as $a){$num++;?>
			<div class='points <?if($num==1){echo 'visible';}?>' id='p<?=$a['id']?>'>
				<?
				if($a['adresses']!=''){
				$pArr = explode('&',$a['adresses']);
				foreach($pArr as $p){
					$pos = strpos($p,'#');
					if($pos!==false){
						$color = substr($p, $pos);
						$color = str_replace('#','',$color);
						$text = substr($p, 0, $pos);
						if($color==''){
							$color = 'blue';
						}
					}else{
						$color = 'blue';
						$text = $p;
					}
				?>
					<div class='point'><span><?=$text?></span>
					<select class='select <?=$color?>'>
						<option value='blue' <?if($color=='blue'){echo 'selected="selected"';}?>>Синий</option>
						<option value='red' <?if($color=='red'){echo 'selected="selected"';}?>>Красный</option>
						<option value='green' <?if($color=='green'){echo 'selected="selected"';}?>>Зеленый</option>
						<option value='orange' <?if($color=='orange'){echo 'selected="selected"';}?>>Оранжевый</option>
					</select>
					<i class="icon-remove"></i></div>
				<?}}?>
			</div>
		<?}?>
	</div>
	<div class='addPoint'>Добавить пункт самовывоза</div>
</div>



 </td>
  </tr>
</table>
<div class='modalBack'></div>
<div class="myModal newCity">
	<input type='text' id='newCityTitle' value='' placeholder='город' /><br />
	<input type='text' id='newCityCenter' value='' placeholder='центр' /> <input type='text' id='newCityZoom' value='' placeholder='масштаб' />
	<div id='myMap'></div>
	<br />
	<input type='button' id='newCityBtn' style='display:none;' value='Добавить' />
	<input type='button' id='editCityBtn' style='display:none;' value='Сохранить' />
</div>
<div class="myModal newPoint">
	<input type='text' id='newPointTitle' placeholder='адрес' />
	<select id='newPointColor'>
		<option value='blue'>Синий</option>
		<option value='red'>Красный</option>
		<option value='green'>Зеленый</option>
		<option value='orange'>Оранжевый</option>
	</select>
	<br />
	<input type='button' id='newPointBtn' value='Добавить' />
</div>

<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>

</body>
</html>