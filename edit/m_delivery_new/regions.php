<?
include ("../connect.php");
include ("../../func/core.php");
include ("../up.php");

$regLinks = array();
$grQ = mysql_query('select * from `deliveryRegLinks`');
while($grR = mysql_fetch_array($grQ)){
	$regLinks[$grR['regionTitle']] = $grR['regionId'];
}

$regTitles = array();
$file = fopen('../../components/geolocation/data/ipgeobase/cities.txt','r');
if ($file) {
	while (!feof($file)){
		$temp = explode("\t",trim(fgets($file, 999)));
		if(!in_array(iconv('cp1251','utf-8',$temp[2]),$regTitles) && iconv('cp1251','utf-8',$temp[2])!=''){
			$regTitles[] = iconv('cp1251','utf-8',$temp[2]);			
		}
	}
}
$regs = array();
$grQ = mysql_query('select * from `deliveryRegions`');
while($grR = mysql_fetch_array($grQ)){
	$regs[] = $grR;
}
function getSelect($title,$regs,$regLinks){
	$select = '<select><option value="0"></option>';
	foreach($regs as $r){
		$id = $regLinks[$title];
		$select .= '<option value="'.$r['id'].'"';
		if($id==$r['id']){
			$select .= ' selected="selected" ';
		}
		$select .= '>'.$r['title'].'</option>';
	}
	$select .= '</select>';
	
	echo $select;
}

fclose($file);
?>
<script src='/edit/css/nprogress/nprogress.js'></script>
<link rel='stylesheet' href='/edit/css/nprogress/nprogress.css'/>
<style>
	.list{
		padding: 10px 20px;
	}
	.list > div{
		margin: 5px 0;
	}
	.list > div:hover{
		background: #ddd;
	}
	.list > div > div{
		width:300px;
		display: inline-block;
	}
	.list > div > select{
		margin-left: 30px;
		position: relative;
		top: 4px;
	}
</style>
<script>
	$(document).ready(function(){
		$('.list select').on('change',function(){
			$.ajax({
				url : "ajax.php?method=saveRegLink",
				type : "POST",
				data: 'title='+encodeURI($(this).closest('div').data('title'))+'&id='+$(this).val(),
				beforeSend: function() {
					NProgress.start();
				},
				success : function (data) {
					
				},
				complete : function() {
					NProgress.done();
				},
				error : function () {
				}
			});
		});
	});
</script>
<br />
<div style='text-align:center;'>
	<a href='index.php' class='btn'>Сетка доставок</a>
	<a href='groups.php' class='btn'>Группы</a>
	<a href='regions.php' class='btn active'>Регионы</a>
</div>
<br />
<div class='list'>
	<?php foreach($regTitles as $r){?>
			<div data-title='<?php echo $r;?>'><div><?php echo $r;?></div><?php getSelect($r,$regs,$regLinks);?></div>
	<?php }	?>
</div>
    </td>
  </tr>
</table>

</body>
</html>