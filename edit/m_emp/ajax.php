<?php
require_once("../../func/core.php");
require_once("../connect.php"); 

function getResult($id){
	$query = "SELECT result FROM refer_prom WHERE id_order = $id ORDER BY time DESC";
	$row = mysql_query($query) or die(mysql_error());
	$arr = array();
	while ($t = mysql_fetch_assoc($row)){
		$arr[] = $t; 
	}
	return $arr;
}

switch ($_REQUEST['action']) {
	case 'getAll':
		function caseOpl($a){
			switch ($a) {
				case 0:
					return "Новый";
					break;
				case 1:
					return "Обработанный";
					break;
				case 4:
					return "Отказ";
					break;
				default:
					return "Статус неизвесте - $a";
					break;
			}
		}

		function getResultOP($res){
		    switch ($res) {
		        case '1':
		            return "Запрос идентифицирован как повторный";
		            break;
		        case '2':
		            return "Запрос отклонен банком";
		            break;
		        case '3':
		            return "Запрос отклонен Платежным шлюзом";
		            break;
		        case '4':
		            return "Технический сбой, пожалуйста свяжитесь с нами.";
		            break;
		        default:
		            return "Технический сбой, пожалуйста свяжитесь с нами.";
		            break;
		    }
		}

		$time1 = ($_POST['time1'] !== "") ? $_POST['time1']: 0;
		$time2 = ($_POST['time2'] !== "") ? $_POST['time2']: 0;

		if(isset($time1) && $time1 !== 0 && $time1 && is_string($time1) && $time1 !== "undefined"){
			$t1 = new DateTime();
			$time1 = explode("/", $time1);
			$t1->setDate((int)($time1[2]), (int)$time1[1], (int)$time1[0]);
			$time1 = (int)$t1->format("U");
		}

		if(isset($time2) && $time2 !== 0 && $time2 && is_string($time2) && $time2 !== "undefined"){
			$t2 = new DateTime();
			$time2 = explode("/", $time2);
			$t2->setDate((int)($time2[2]), (int)$time2[1], (int)$time2[0]);
			$time2 = (int)$t2->getTimestamp();
		}

		if($time1 !== 0 && $time2 !== 0 && is_numeric($time1) && is_numeric($time2)){
			$dateStr = "AND (`date` >= $time1 AND `date` <= $time2)";
		}elseif($time1 !== 0 && is_numeric($time1) && $time2 === 0){
			$dateStr = "AND (`date` >= $time1)";
		}elseif ($time1 === 0 && is_numeric($time2) && $time2 !== 0) {
			$dateStr = "AND (`date` <= $time2)";
		}else{
			$dateStr = "";
		}

		$all = $_POST['all']; 

		if($all === "true") {
			$allStr = "";
		}else{
			$allStr = "AND (`status` = 1 OR `status` = 0)";
		}
		//		#LEFT JOIN refer_prom ON orders.id = refer_prom.id_order
		$query = "
		SELECT
			`orders`.*,
			`order_delivery_types`.`name` as `delivery`
		FROM
			`orders`
		LEFT JOIN `order_delivery_types`
			ON `orders`.`delivery_type` = `order_delivery_types`.`id`
		WHERE
			payment_type = 2 {$allStr} {$dateStr}
		ORDER BY
			`orders`.`id` DESC";

		$row = mysql_query($query) or die($query);
		$arr = array();
		$all = array();
		while ($arr = mysql_fetch_assoc($row)) {
			$all[] = $arr;
		}
		$a = "";
		foreach ($all as $key => $value) {
			$text = 'class="info"';
			if($value['status'] == 1){
				$text = 'class="success"';
			}elseif ($value['status'] == 4) {
				$text = 'class="warning"';
			}
			$date = date('Y/m/d H:i', $value['date']);
			$price = unserialize($value['order_price'])['price_after_global_discount'];
			if($value["total_price"] == 1){
				$price += unserialize($value['order_price'])['delivery_price'];
			}
			$status = caseOpl($value['status']);
			$posti = getResult($value['id']);
			$arr_result = [];
			foreach ($posti as $t => $ft) {
				$arr_result[] = $ft['result'];
			}
			if(in_array("0", $arr_result)) {
				$tex = "Оплачено";
			}elseif($arr_result[0]){
				$op = getResultOP($arr_result[0]);
				$tex = "{$op}";
			}else{
				$tex = "Операций с банком не было";
			}
			$a .= "
				<tr $text>
					<td>$key</td>
					<td>{$value['id']}<a class='btn btn-mini kro' style='float:right;' data-id='{$value['id']}'>Инфо</a></td>
					<td>$date</td>
					<td>$price</td>
					<td>$status</td>
					<td>{$tex}<a class='btn btn-mini bro' style='float:right;' data-id='{$value['id']}'>Транзакции</a></td>
				</tr>
			";
		}
		$a .= '
			<script>
				$(".kro").on("click", function(){
					var id = $(this).data("id");
					$.ajax({
						url: "ajax.php?action=one",
						type: "POST",
						data: {id: id},
						success: function(data){
							$(".modal-body").empty().append(data);
							$(".modal").modal();
						}
					})
				});
				$(".bro").on("click", function(){
					var id = $(this).data("id");
					$.ajax({
						url: "ajax.php?action=tran",
						type: "POST",
						data: {id: id},
						success: function(data){
							$(".modal-body").empty().append(data);
							$(".modal").modal();
						}
					});
				});
			</script>
		';
		echo $a;
		break;
	case 'one':
		$id = $_POST['id'];
		$search_str = (int)($id);
		$query = "
			SELECT
				`orders`.*,
				`order_delivery_types`.`name` as `delivery`
			FROM
				`orders`
			LEFT JOIN `order_delivery_types`
				ON `orders`.`delivery_type` = `order_delivery_types`.`id`
			WHERE
				`orders`.`id` = $search_str
			LIMIT 1";

			$sel = mysql_query($query) or die(mysql_error());
				while ($order = mysql_fetch_assoc($sel)) {
		//Подготавливаем данные для вывода
		$order['products'] = unserialize($order['products']);
		fillProductsWithInfo($order['products']);
		$order['order_price'] = unserialize($order['order_price']);
		
		//print_r($order);
		$day = date('d', $order['date']);
		if ($previusOrderDay != $day) echo ('<div align="center" class="big_menu">' . date('d.m.Y', $order['date']) . '</div><br> ');
		$previusOrderDay = $day;
		?>
		<div class="order" data-id="<?=$order['id'];?>">
		<span  class="txt_zag_zakaz"><strong><?php echo getOrderCode($order['id']); ?> </strong></span> <span class="txt_zakaz">(<?php echo date('d.m.Y H:i', $order['date']); ?>)</span>
		<table width="900" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="500" align="left" valign="top" class="txt_zakaz">
				<div style="padding-top:10px">
					<?php switch ($order['status']) {
						case '0' : echo '<span class="label label-info">новый </span>&nbsp;&nbsp;&nbsp;&nbsp;';
							break; 
						case '1' : echo '<span class="label label-success">обработан </span>&nbsp;&nbsp;&nbsp;&nbsp;';
							break;
						case '4' : echo '<span class="label label-important">отказ </span>&nbsp;&nbsp;&nbsp;&nbsp;';
					}
					?>
			<? if ($order['status'] != '1') { ?>
				<img src="/tpl/icons/ok.gif" align="absbottom" >
				<a href="#" class="processed" style="color:#333;">Обработан</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<? } ?>
			<? if ($order['status'] != '4') { ?>
				<img src="/tpl/icons/del.gif" align="absbottom" >
				<a href="#" class="refusal" style="color:#333;">Отказ</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<? } ?>
			</div>
			
			<table>
				<tr>
					<td>
						<?php 
						foreach ($order['products'] as $p) { $info = $p['info'];?>
							<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
							  <tr>
								<td width="80" align="left" valign="top"><a target="_blank" href="<?php echo getTemplateLink($info, 'catalog'); ?>"><img src="<?=getImageWebPath('product_small').$info['id']?>.jpg"  border="0" align="left" /></a></td>
								<td align="left" valign="top"><a target="_blank" href="<?php echo getTemplateLink($info, 'catalog'); ?>" style="color:#333;"><?=$info['title']?></a> <span style="font-size: 1.3em;">(<?=$p['amount']?> шт.)</span><br />
								  <br />
								  <? if (!empty($p['info']['art'])) {?>
								  <span style="font-size: 14px;color: grey;font-family: Verdana, Geneva, sans-serif;">Артикул: <?=$p['info']['art']?></span>
								  </br></br>
								  <? } ?>
								  <span style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;"><?=$p['price_after_discount']?></span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">руб.</span>
								 </td>
								</tr>
							</table>
						<?php } ?>
						<span style="font-size: 1.2em;">Общая сумма: <?=$order['order_price']['price_after_global_discount']?> (скидка <?=$order['order_price']['discount_value']?> <?=getDiscountTypeString($order['order_price']['discount_type'])?>)</span><br />
						<br />
						<? if ($order['delivery_type'] == 1) { ?>
						<strong>Стоимость доставки:</strong> 
						<? if (!isset($order['order_price']['delivery_price']) || $order['order_price']['delivery_price'] === false) {
								echo 'Рассчитать не удалось';
							} elseif ($order['order_price']['delivery_price'] == 0) {
								echo 'Бесплатно';
							} else {
								echo $order['order_price']['delivery_price'] . ' руб.';
							}
						} elseif ($order['delivery_type']  == 2) {
							echo '<strong>Базовая ставка доставки:</strong> ';
							if (!isset($order['order_price']['delivery_price']) || $order['order_price']['delivery_price'] === false) {
								echo 'Рассчитать не удалось';
							} elseif ($order['order_price']['delivery_price'] == 0) {
								echo 'Бесплатно';
							} else {
								echo $order['order_price']['delivery_price'] . ' руб.';
							}
						} ?>
						<br />
						<strong>Имя:</strong> <?=$order['name']?><br />
						<strong>Тел.:</strong> <?=$order['phone']?><br />
						<strong>Доставка:</strong> <?=$order['delivery']?><br />
                                                <? if (isset($order['pickup_point']) && !empty($order['pickup_point'])) { ?>
                                                <strong>Пункт самовывоза:</strong> <?=$order['pickup_point']?><br />
                                                <? } ?>
						<strong>Способ оплаты:</strong> <?=$order['payment']?>
							<?if($order['paymentId']==2){?>
								<button data-id='<?=$order['id']?>' class='sendBtn' data-mail='<?=$order['email']?>'>Оправить счет для оплаты</button>
							<?}?>
						<br />
						<strong>Адрес:</strong> <?=$order['adress']?><br />
						<?php
							if ($order['email'] != '') echo ('<i class="icon-envelope"></i> ' . $order['email'] . '<br />');
						?>
						<?if ($order['msg']) {?>
						<strong>Сообщение:</strong><div class="order-msg" style="width: 300px; height: 100px; overflow: auto; word-wrap: break-word;"><?=$order['msg']?><br /></div>
						<?}?>
						<strong>Дополнительно:</strong> <?=$order['extra_information']?><br />
						<br />
					</td>
				</tr>
			</table>

			</td>
			<td align="left" valign="top" width="30">&nbsp;</td>
			<td align="left" valign="top" width="375">



			<div class="txt_zakaz" style="padding-left:20px; padding-right:10px; padding-bottom:10px;  padding-top:10px;  background-color:#fdf6f0">
			<table class="table table-condensed order-log">
			<?php
			$s = mysql_query("SELECT * FROM `orders_log` WHERE `order_id`='{$order['id']}' AND `action` <> '0' ORDER BY `date` ASC");
			$lastActionTime = $order['date'];
			$actionsCount = mysql_num_rows($s);

			while ($row = mysql_fetch_assoc($s)) {
				$actionTime = strtotime($row['date']);
				
				$dateDiff = $actionTime - $lastActionTime;
				$dH = floor($dateDiff / 3600);
				$dM = ($dateDiff - $dH * 3600) / 60;
				$delta = $dH . ' ч. ' . round($dM) . ' м.';
				
				$lastActionTime = $actionTime;

				$month = $months[date('n', $actionTime) - 1];
				$day_of_week = $days[date('D', $actionTime)];
				$day = date('d', $actionTime);
				$year = date('Y', $actionTime);
				$time = date('H:i', $actionTime);
				?>
					<tr>
						<td width='100'><b><?=$time?></b>, <?=$day?> <?=$month?><br /><?=$day_of_week?> </td>
						<td><img src='/tpl/icons/comment.jpg' border='0' align='absbottom' ></td>
						<td><?=$row['descr']?></td>
						<td width="75px"><?=$delta?></td>
					</tr>
				<?php
			}
			$keyw = '';
			if ($actionsCount > 1) {
					$dd = $lastActionTime - $order['date'];
					$dh = floor($dd / 3600);
					$dm = ($dd - $dh * 3600) / 60;
					$delta = $dh . ' ч. ' . round($dm) . ' м.';
					echo "<tr><td colspan='3'>&nbsp;</td><td><strong>" . $delta . "</strong></td></tr>";
				}
				//Откуда пришел
				$hrefer = explode("/", $order['h']);
				$site = "<img src=http://favicon.yandex.net/favicon/" . $hrefer[2] . " align=absmiddle  height=15 width=15>";
				if ($hrefer[2] == "yandex.ru" or $hrefer[2] == "www.yandex.ru") {
					$site = "<img src=/tpl/icons/ya_mini.jpg align=absmiddle height=15 width=15 >";
					$hrefer2 = explode("text=", $hrefer[3]);
					$keyw = $hrefer2[1];
					$keyw = urldecode($keyw);
					// $keyw = iconv('UTF-8', 'Windows-1251', $keyw);
					$keyw = $keyw;
					$hrefer3 = explode("&", $keyw);
					$keyw = $hrefer3[0];

					//echo "<pre>"; print_r($hrefer2); echo "</pre>";
				}
				if ($hrefer[2] == "www.google.ru" or $hrefer[2] == "google.ru" or $hrefer[2] == "www.google.by" or $hrefer[2] == "www.google.com") {
					$site = "<img src=/tpl/icons/goog_mini.jpg align=absmiddle  height=15 width=15 >";
					$hrefer2 = explode("q=", $hrefer[3]);

					$keyw = $hrefer2[1];
					$keyw = urldecode($keyw);
					// $keyw = iconv('UTF-8', 'Windows-1251', $keyw);
					$keyw =  $keyw;
					$hrefer3 = explode("&", $keyw);
					$keyw = $hrefer3[0];

					//$keyw = str_replace("&hl", "", $keyw );
				}
				//echo "<pre>"; print_r($hrefer); echo "</pre>";
			?>
			</table>
				<img src="/tpl/icons/comment.jpg" border="0" align="absbottom" >
				<a href="javascript:void(0)" class="comment-button" style="color:#333;">Комментировать</a>
				<div style="display: none;" class="comment-form">
					<textarea rows="3" cols="20" class="comment-field" style="width: 300px;"></textarea><br />
					<input type="button" name="s" class="send-button btn"  value="Написать" />
				</div>
				<br /><br />
			</div>

			<br>
				<table width="270" border="0" cellspacing="10" cellpadding="0">
				<?php
					//Пока не работает
					if ($order['status'] == 1 AND false) {
						if (($order['remind'] != 1) && ($order['email'] != '')) {
							echo('<tr><td><a class="btn btn-warning reminder-link" pay_id="' . $order['id'] . '"><i class="icon-warning-sign"></i> Запросить отзыв</a></td></tr>');
						} elseif ($order['email'] == '') {
							//	Мыла нет, нет и отзыва
						} else {
							echo('<tr><td><a class="btn btn-success disabled"><i class="icon-ok"></i> Отзыв запрошен</a></td></tr>');
						}
					}
				?>
					<tr>
					  <td class="txt_zakaz_small" ><?=$site?> <?=$keyw?> <a href="<?=$order['h']?>" style="color:#333;" target="_blank">>>></a></td>
					</tr>
				<?php if($order['partner_id'] != 0) {
					echo('<tr>
						<td class="txt_zakaz_small">Заказ по партнерской ссылке.</td>
					</tr>');
				}
				?>
				</table>
			</td>
		  </tr>
		</table>
		<hr style="margin: 50px 0;"/>
		</div>
		<?php
	}
		break;
	case "tran":
		$id = (int)$_POST['id'];
		$query = "SELECT * FROM refer_prom WHERE id_order = $id ORDER BY time DESC";
		$row = mysql_query($query) or die(mysql_error());
		$arr = array();
		while ($t = mysql_fetch_assoc($row)){
			$t['post'] = unserialize($t['post']);
			$arr[] = $t; 
		}
		echo "<pre>";
		var_export($arr);
		echo "</pre>";
	break;
}