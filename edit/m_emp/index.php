<?php
require_once("../connect.php"); 
require_once("../../func/core.php");
require_once("../up.php"); 
function printAr($arr){
	echo "<pre style='color: #000; background-color: #fff; font-size: 10px;'>".print_r($arr,1)."</pre>";
}
function caseOpl($a){
	switch ($a) {
		case 0:
			return "Новый";
			break;
		case 1:
			return "Обработанный";
			break;
		case 4:
			return "Отказ";
			break;
		default:
			return "Статус неизвесте - $a";
			break;
	}
}

?>
<style>
	.clearfix:after {
	    clear: both;
	    content: ".";
	    display: block;
	    height: 0;
	    visibility: hidden;
	}
	.clearfix {
	    display: inline-block;
	}
	.clearfix {
	    display: block;
	}
</style>

<script src="./datetimepicker.js" type="text/javascript"></script>
<script src="./app.js" type="text/javascript"></script>
<link rel="stylesheet" href="date.css"/>
<center>
	<div class="content clearfix" style="width: 90%;">
		<form action="">
		<div class="well clearfix">
			<label class="checkbox" style="width: 92px;"><input type="checkbox" value="" id="ch1">Даже отказы</label>
			Даты от - 
			<input class="datepicker" type="text" id="d1"/> до <input id="d2" class="datepicker" type="text" />
			<a class="btn btn-small" id="t1" style="position: relative; top: -5px;">Найти</a>		
		</div>
		</form>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>№ Заказа</th>
					<th>Дата</th>
					<th>Сумма</th>
					<th>Статус</th>
					<th>Статус операции</th>
				</tr>
			</thead>
			<tbody id='d4'>
				
			</tbody>
		</table>
	</div> 
</center>

<div class="modal large hide fade" style="width: 80%; margin-left:-40%;">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Информация по заказу</h3>
	</div>
	<div class="modal-body">
		
	</div>
</div>