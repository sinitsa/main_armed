<?php
include ('../connect.php'); 
include ('../../func/core.php');

include ('../up.php'); 
?>

	<script src="/js/jquery.form.js" type="text/javascript"></script>

	<style>
		.r {
			color: #C33B35;
			display: inline;
		}
		.g {
			color: #55AB55;
			display: inline;
		}
	</style>
<script>
$(function(){
	
	//удаление отзыва
	$('.feed .del').bind('click', function(){
			var feed = $(this).closest('.feed');
			var button = $(this);
			var feedId = feed.data('id');
		
		$.ajax({
				url: 'ajax.php?action=delete',
				type : 'POST',
				dataType : 'json',
				data : { "id" : feedId },
				beforeSend : function () {
					button.attr('disabled','disabled');
					feed.css({"opacity": .5});
				},
				success : function (data, textStatus, jqXHR) {
					feed.fadeOut('fast', function () {
						feed.remove();
					});
				},
				complete : function () {
					button.removeAttr('disabled');
				}
		});
	});
	
	//подтверждение отзыва
	$('.feed .conf').bind('click', function (){
		var feed = $(this).closest('.feed');
		var button = $(this);
		var feedId  = feed.data('id');
		var confirm = button.hasClass('disabled') ? 0 : 1; 
		var name = feed.find('.label');
		$.ajax({
				url: 'ajax.php?action=confirm',
				type : 'POST',
				dataType : 'json',
				data : { "id" : feedId, "confirm" : confirm},
				beforeSend : function () {
					feed.css({"opacity": .5});
				},
				success : function (data, textStatus, jqXHR) {
					if (confirm == 1) {
						button.addClass('disabled');
						name.removeClass('label-important');
						name.addClass('label-success');
					} else {
						button.removeClass('disabled');
						name.removeClass('label-success');
						name.addClass('label-important');
					}
				},
				complete : function () {
					feed.css({"opacity": 1});
				}
		});
	});
	
	//ответить на отзыв
	$('.feed .ans').bind('click', function(){
		var feed = $(this).closest('.feed');
		feed.find('.answer-field .answer-form').slideDown();
	});
	$('.feed input[name="answer"]').bind('click', function () {
		var feed = $(this).closest('.feed');
		var feedId = feed.data('id');
		var loading = feed.find('.loading');
		
		var answer = feed.find('textarea');
		var sendButton = $(this);
		
		$.ajax({
				url: 'ajax.php?action=add_answer',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : feedId,
					"answer" : answer.val()
					},
				beforeSend : function () {
					loading.show();
					answer.attr('disabled','disabled');
					sendButton.attr('disabled','disabled');
				},
				success : function (data, textStatus, jqXHR) {
					feed.find('.answer').text(answer.val());
					feed.find('.answer-form').slideUp();
				},
				complete : function () {
					loading.hide();
					answer.removeAttr('disabled');
					sendButton.removeAttr('disabled');
				}
		});
		
	});
});
</script>	
<div class="modal hide fade" id="myModal">
	<div class="modal-header">

    </div>
    <div class="modal-body">
		<center><textarea id="modal_reply" class='reply' style="width: 95%;" rows='10'></textarea></center>
    </div>
    <div class="modal-footer">
		<a id="modalOk" class="btn btn-primary">Написать</a>
		<a data-dismiss="modal" class="btn">Закрыть</a>
    </div>
</div>

<center>
<div style="width: 90%;">
<table border="0" class="table">
		<tr>
			<th align="center">Отзывы</th>
			<th align="center">Заказы</th>
		</tr>
<?php
//	mysql_query("SET NAMES 'cp1251'");
	$query = "SELECT * FROM `shop_otzyv` ORDER BY `id` DESC";
	$result = @mysql_query($query);
	while($row = @mysql_fetch_assoc($result)) {
		?>
		<tr class = "feed" id="feed<?=$row['id']?>" data-id="<? echo $row['id']; ?>">
			<td width="40%">
				<div >
					<?
						/*if($row['confirm'] == 1){
							$style = ('<span class="label label-success" >');
						} else {
							$style = ('<span class="label label-important" >');
						}*/
						$style = "<span class='label ".($row['confirm']==1 ? "label-success'>" : "label-important'>");
						echo $style.$row['name']."</span> <span style=font-size:11px>".$row['date']."</span> ";
					?><br>
					<div class="alert alert-block">
					<div class="otz_txt" data-id="<?=$row['id']?>"><? echo $row['des']; ?></div>
					</div>
					
					<!-- Кнопки -->
					<div class="btn-group">
						<a class='conf btn btn-success<?=(($row['confirm']==1)?' disabled':'')?>' data-id="<? echo $row['id']; ?>" value="Одобрить"><i class="icon-ok"></i></a>
						<a class='ans btn<?=($row['otvet'] == '' ? '' :' disabled');?>' data-id="<? echo $row['id']; ?>" value="Ответить"><i class="icon-comment"></i></a>
						<a class='del btn btn-danger' data-id="<? echo $row['id']; ?>"><i class="icon-trash icon-white"></i></a>
					</div>
					
					<!--Ответ на отзыв
					<div class="reply">
						<? if($row['otvet'] != '') { ?>
							<div class="alert alert-success" style="margin-left: 50px;">
								<a class='del_ans btn btn-danger' data-id="<? echo $row['id']; ?>"><i class="icon-trash icon-white"></i></a>&nbsp;
								<span style="margin-left:"><em>Ответ</em>: <? echo $row['otvet']; ?></span>&nbsp;
							</div>
						<? } ?>
					</div> -->
					<div class="answer-field"  style="margin-top: 5px">
							<div class="answer"><?=$row['otvet']?></div>
							
							<div class="answer-form">
								<textarea name= "" ><?=$row['otvet'];?></textarea>
								<br />
								<input type="button" class="answer-button btn" name="answer" value="Ответить" /> <!--<span class="loading"><img src="/img/ajax_loading.gif" alt="Загрузка" /></span> -->
							</div>
					</div>
					

					<br>
				</div>
			</td>
			
			<!-- Информация о заказах -->
			<td style="font-size: 14px">
				<?
					$sub_query = "
						SELECT
							`orders`.*,
							`order_delivery_types`.`name` as `delivery`
						FROM `orders`
						LEFT JOIN `order_delivery_types`
						ON `orders`.`delivery_type` = `order_delivery_types`.`id`
						WHERE `orders`.`email`='{$row['mail']}'
					";
					$sub_result = mysql_query($sub_query) or die('Ошбика запроса: '.$sub_query);
					while($order = mysql_fetch_assoc($sub_result)) {
						$order['products'] = unserialize($order['products']);
						fillProductsWithInfo($order['products']);
						$order['order_price'] = unserialize($order['order_price']);
					?>
						<i class ="icon-shopping-cart"></i> <a href="/edit/m_zakaz/zakaz.php?id=<?=$order['id']?>" target="_blank">
							<span class="txt_spisok" style="font-size:14px; font-weight: bold"><?=getOrderCode($order['id'])?></span>
						</a> (<?=date('d.m.Y H:i', $order['date'])?>)<br />
						<span style="font-size:11px; font-line:3px">
							<span style="font-size: 1.2em;">Общая сумма: <?=$order['order_price']['price_after_global_discount']?> (скидка <?=$order['order_price']['discount_value']?> <?=getDiscountTypeString($order['order_price']['discount_type'])?>)</span><br />
							<br />
							<strong>Имя:</strong> <?=$order['name']?><br />
							<strong>Тел.:</strong> <?=$order['phone']?><br />
							<strong>Доставка:</strong> <?=$order['delivery']?><br />
							<strong>Адрес:</strong> <?=$order['adress']?><br />
							<ul class="nav nav-tabs nav-stacked">
								<?php foreach ($order['products'] as $p) { ?>
								<li><a href="<?=getTemplateLink($p['info'], 'catalog')?>" target="_blank"><span class="txt_spisok"><?=$p['info']['title']?></span></a></li>
								<?php } ?>
							</ul>
						</span>
					<?
					}
				?>
			</td>
		</tr>
		<?
	}
?>
</table>
</div>
</center>



<?php include ("../down.php"); ?>