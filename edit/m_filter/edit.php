<?php
include ("../connect.php");
include ("../../func/core.php");

$saved = false;

if (isset($_GET['cat_id'])) {
	if (is_numeric($_GET['cat_id']) && $_GET['cat_id'] > 0 ) 
		$catId = $_GET['cat_id'];
	else
		die('Bad request');
} else {
	$catId = 0;
}

if (isset($_GET['id'])) {
	if (is_numeric($_GET['id']) && $_GET['id'] > 0 ) 
		$id = $_GET['id'];
	else
		die('Bad request');
} else {
	die('Bad request');
}

if ($id && $_POST['save']) {
	$data = array();
	$data['title'] = mysql_real_escape_string($_REQUEST['param_name']);
	$data['show_title'] = isset($_REQUEST['param_show_title']) ? '1' : '0';
	
	if (isset($_REQUEST['param_range_use'])) {
		$data['show_input'] = isset($_REQUEST['param_show_input']) ? "1" : "0";
		$data['step'] = mysql_real_escape_string($_REQUEST['param_step']);
		$data['unit'] = mysql_real_escape_string($_REQUEST['param_unit']);
	}
	
	$setString = getSetString($data);
	mysql_query("UPDATE `params_available` SET {$setString} WHERE `id` = '{$id}'");
	$saved = true;
}
if ($id) {
	$param = mysql_fetch_assoc(mysql_query("SELECT * FROM `params_available` WHERE `id` = '{$id}'"));
}
include ("../up.php");
?>
<script type="text/javascript">
$(function() {
	//Add params buttons
	$('.add-param-button').bind('click', function() {
		var addParamField = $(this).closest('.add-param-field');
		var loading = addParamField.find('.loading');
		
		var catId = addParamField.find('input[name="cat_id"]').val();
		var paramId = addParamField.find('input[name="param_id"]').val();
		var paramValue = addParamField.find('input[name="param_value"]').val();
		
		if (paramValue == undefined || paramValue == '') return;
		loading.show();
		//Аяксом добавляем параметры в базу
		addParamValue(paramId, catId, paramValue, function (id) {
			addParamField.closest('.params-list-field').find('.params-list').append(
			$('<li>' + paramValue + '&nbsp;<img class="delete-button" data-id="' + id + '" style="width:12px; vertical-align: middle;" src="/img/dopfotodel.png" alt=""/></li>')
			);
			//очистить поле ввода
			addParamField.find('input[name="param_value"]').val('');
			loading.hide();
		}, function () {
			alert('some error here');
			loading.hide();
		});
	});
	$('.delete-button').live('click', function() {
		//alert('sss' + $(this).data('id'));
		var paramId = $(this).data('id');
		var li = $(this).parent();
		if (confirm('Действительно хотите удалть параметр?')) {
			deleteParamValue(paramId, function() {
				li.remove();
			});
		}
	});
});
</script>
<style>
	.delete-button {cursor: pointer;}
</style>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div><h4>Редактирование параметра:</h4><div>
			<div>	
				<?php if ($saved) { ?>
					<div style="color: green;">Сохранено</div>
				<?php } ?>
				<form action="" method="post">
					<table class="table">
						<thead>
							<tr>
								<td><strong>Название</strong></td>
								<td><strong>Глобальный</strong></td>
								<td><strong>Тип</strong></td>
								<td><strong>Единицы<br />измерения</strong></td>
								<td><strong>Шаг</strong></td>
								<td><strong>Показывать<br />input'ы</strong></td>
								<td><strong>Показывать<br />заголовок</strong></td>
							</tr>
						</thead>
						<tr>
							<td><input type="text" name="param_name" value="<?php echo $param['title']; ?>" /></td>
							<td><?php echo ($param['global'] ? 'Да':'Нет'); ?></td>
							<td>
								<?php
									switch ($param['type']) {
										case PARAM_VALUE : echo 'Выпадающий список'; break;
										case PARAM_SET : echo 'Параметры в ряд'; break;
										case PARAM_RANGE : echo 'Диапазон значений'; break;
										default: echo 'Неизвестный тип';
									}
								?>
							</td>
							<td>
								<? if ($param['type'] == PARAM_RANGE) { ?>
									<input type="text" name="param_unit" value="<?=$param['unit']?>" class="span1" />
								<? } else { ?>
									&mdash;
								<? } ?>
							</td>
							<td>
								<? if ($param['type'] == PARAM_RANGE) { ?>
									<input type="text" name="param_step" value="<?=$param['step']?>" class="span1" />
								<? } else { ?>
									&mdash;
								<? } ?>
							</td>
							<td>
								<? if ($param['type'] == PARAM_RANGE) { ?>
								<input type="checkbox" name="param_show_input" <?=($param['show_input'] == '1' ? 'checked="checked"' : '')?>/>
								<input type="hidden" name="param_range_use" value="yes" />
								<? } else { ?>
									&mdash;
								<?} ?>
							</td>
							<td>
								<input type="checkbox" name="param_show_title" <?=($param['show_title'] == '1' ? 'checked="checked"' : '')?>/>
							</td>
						</tr>
						<tr>
							<td colspan="7"><input type="submit" class="btn" name="save" value="Сохранить"/></td>
						</tr>
					</table>
				</form>
			</div>
			<div><h4>Доступные значения:</h4><div>
			<div>
				<div class="params-list-field">
					<ul class="params-list" style="list-style: none;">
					<?
					$values = getCatAvailableValues($id, $catId);
					foreach ($values as $value) { ?>
						<li><?=($param['type'] == PARAM_RANGE ? $value['value_float'] : $value['value'])?>&nbsp;<img class="delete-button" data-id="<?=$value['id']?>" style="width:12px; vertical-align: middle;" src="/img/dopfotodel.png" alt=""/></li>
					<? } ?>
					</ul>
					<div class="add-param-field">
						Добавить значение:<br />
						<input type="hidden" name="cat_id" value="<?php echo $catId; ?>" />
						<input type="hidden" name="param_id" value="<?php echo $id; ?>" />
						<input type="text" name="param_value" value="" />
						<input class="add-param-button" type="button" value="Добавить" />
						<img class="loading" style="display: none;" src="/img/ajax_loading.gif" alt="" />
					</div>
				</div>
			</div>
			<? $url = $catId > 0 ? '/edit/m_catalog/list.php?id='.$catId : '/edit/m_mcat/'; ?>
			<div><a href="<?=$url;?>" class="btn btn-small"><i class="icon-chevron-left"></i> Назад</a><div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>