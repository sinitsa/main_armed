<?php
include_once("../../func/core.php");

class iYMLParam{
}

class iYML{
	
	var $controlAmount;
	var $controlMargin;
	
	var $siteName;
	var $siteCompany;
	var $currencyName;
	var $currencyRate;
	var $salesNotes;
	
	var $catalogImageType;
	var $selectedCategories;
	var $selectedCategoriesFakeRests;
	var $onRequest;
	var $includeRests;
	
	var $ymlFilePath;
	
	var $lastUpdate;
	
	var $controlPrice;
	var $bidInStock;
	var $bidNotAvailable;
	
	
//	var $__params;
	
	function __construct(){
		
		$this->getYMLConfig();
		
		return $this;
	}
	
	function install(){
		
		$query = "
			CREATE TABLE IF NOT EXISTS `igoogle_config` (
			  `name` varchar(50) NOT NULL,
			  `definition` varchar(50) DEFAULT NULL,
			  `description` varchar(200) DEFAULT NULL,
			  `value` varchar(200) DEFAULT '',
			  `sort` int(4) unsigned NOT NULL default '0',
			  PRIMARY KEY (`name`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8
		";
		mysql_query($query);
		
		return $this;
	}
	
	function getYMLConfig(){
		
		$query = "SELECT `name`, COALESCE(`value`, '') AS `value` FROM `igoogle_config` ORDER BY `sort` ASC";
		
		$result = @mysql_query($query);
		
		while($line = @mysql_fetch_assoc($result)){
			$this->{$line['name']} = $line['value'];
		}
		
		$this->selectedCategories = explode(',', $this->selectedCategories);
	}
	
	function saveParam($paramName, $paramValue){
		

		$query = "INSERT INTO `igoogle_config` SET `name` = '{$paramName}', `value`= '{$paramValue}'
			ON DUPLICATE KEY UPDATE `value` = '{$paramValue}' ";
			
		mysql_query($query);
		
		return mysql_affected_rows()>0;
	}
		
	function saveParams(){ 
			
		foreach($this as $attrName=>$attrValue){
			if ($paramName != "lastUpdate"){
				//$this->saveParam($attrName, iconv("UTF-8","cp1251",$this->{$attrName}));
				$this->saveParam($attrName, $this->{$attrName});
			}
		}
		
		return $this;
	}

	function buildParamHTML(){
		$query = "SELECT `name`, COALESCE(`description`, '') AS `description`, COALESCE(`value`, '') AS `value` FROM `igoogle_config` ORDER BY `sort` ASC";
		
		$result = @mysql_query($query) or die(mysql_error());
		$html = "<table id=\"iYMLParamForm\">";
		while($line = @mysql_fetch_assoc($result)){
			if ($line['name'] == "lastUpdate"){
				continue;
			}			
			//$this->{$line['name']} = $line['value'];
			$descr = $line['description'];
			$caption_class = "description";
			if(!$descr){
				$caption_class = "internal-name";
				$descr = $line['name'];
			}
			$disabled_attribute = "";
			
			$html .= "<tr>";
			$html .= "<td class=\"col1\"><span class=\"{$caption_class}\">{$descr}</span></td>";
			if ($line['name'] == 'catalogImageType') {
				$html .= "<td class=\"col2\"><select name=\"{$line["name"]}\" {$disabled_attribute}>";
				$ar = array();
				$ar[] = array('Превью', 'product_preview');
				$ar[] = array('Средняя', 'product_medium');
				$ar[] = array('Маленькая', 'product_small');
				$ar[] = array('Оригинал', 'product_original');
				foreach ($ar as $option) {
					$html .= "<option ".($option[1] == $line['value']?'selected="selected" ':'')."value=\"{$option[1]}\">{$option[0]}</option>";
				}
				$html .= "</select></td>";
			} elseif (in_array($line['name'], array('selectedCategories', 'selectedCategoriesFakeRests'))) {
				$html .= "<td class=\"col2\"><select name=\"{$line["name"]}\" style=\"width: 380px;\" multiple=\"multiple\" size=\"16\" {$disabled_attribute}>";
				$selected = explode(',', $line['value']);
				$selected = (array) $selected;
				
				foreach (getCats() as $cat) {
					$html .= "<option value=\"{$cat['id']}\"".(in_array($cat['id'], $selected)?' selected="selected"':'').">{$cat['title']}</option>";
				}
				$html .= "</select></td>";
			} else {
				$html .= "<td class=\"col2\"><input {$disabled_attribute} name=\"{$line["name"]}\" type=\"text\" value=\"{$line["value"]}\" /></td>";
			}
			$html .= "</tr>";
			
		}
		$html .= "</table>";
		
		return $html;
	}
	
	function buildYMLFile(){
	
		//$site_title=iconv("CP1251","UTF-8",$this->siteName);
		$site_title=$this->siteName;

		$xml_file= "<?xml version=\"1.0\"?>
					<rss version=\"2.0\" xmlns:g=\"http://base.google.com/ns/1.0\">
					<channel>
					<title>$site_title</title>
					<link>".Config::get('site.web_addr')."</link>
					<description>$site_title</description>";
					
		$query=mysql_query("select *, catalog.id as productId from catalog
		 left join catalog_2_cat on catalog_2_cat.catalog_id = catalog.id
		 where catalog.id in (".implode(',', $this->selectedCategories).")
		 or catalog_2_cat.cat_id in (".implode(',', $this->selectedCategories).")
		 order by catalog.id ") or die("Ошибка при запросе к базе данных 1!");
		while ($row = mysql_fetch_array($query))
		{
			if ($row['price_after_discount'] <= 0) continue;
			$xml_file.= "<item>";
			//$title=iconv("CP1251","UTF-8",$row['title']);
			$title=$row['title'];
			$title=html_entity_decode($title,ENT_QUOTES,"UTF-8");
			$title=htmlspecialchars($title,ENT_QUOTES);
			$title = preg_replace('/\x03/', '', $title);
			
			$xml_file.= "<title>$title</title>";

			$xml_file.= "<g:price>{$row['price_after_discount']} RUB</g:price>";
			$xml_file.= "<link>https://armed-market.ru/catalog/{$row['chpu']}.html</link>";
			$xml_file.= "<g:image_link>".getImageWebPath('product_big').$row['productId'].".jpg</g:image_link>";
			$xml_file.= "<g:id>{$row['productId']}</g:id>";

			$descr=strip_tags($row['des']);
			//$descr=iconv("CP1251","UTF-8",$descr);
			$descr=$descr;
			$descr=html_entity_decode($descr,ENT_QUOTES,"UTF-8");
			$descr=htmlspecialchars($descr,ENT_QUOTES,"UTF-8");
			$descr = preg_replace('/\x03/', '', $descr);
			if (strlen($descr) > 1000) {
				$end = strpos($descr, '. ', 990);
				$descr = substr($descr, 0, ($end+2));
			}	
			
			$descr = str_replace("\r\n",'',$descr);
			$descr = str_replace("\n",'',$descr);
                                    
    		//$descr = trim(preg_replace('/\s{2,}/', ' ', $descr));

			$xml_file.= "<description>".$descr."</description>";
			$q = "SELECT DISTINCT `pav`.value 
						FROM `params_available_values`as `pav` 
						INNER JOIN  `params_catalog_links` as `pcl` 
						ON `pav`.id = `pcl`.param_id 
						INNER JOIN `catalog` as `c`
						ON `pcl`.catalog_id = `c`.id
						WHERE `c`.id  = {$row['id']} 
						AND `pav`.id IN (70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84) 
						LIMIT 1";
			$value = mysql_fetch_row(mysql_query($q));
			
			$brand = $value[0];
			if (!empty($brand)) {
				//$brand=iconv("CP1251","UTF-8",$brand);
				$brand=html_entity_decode($brand,ENT_QUOTES,"UTF-8");
				$brand=htmlspecialchars($brand,ENT_QUOTES);		
				$brand = preg_replace('/\x03/', '', $brand);	
				
				$xml_file.= "<g:brand>{$brand}</g:brand>";
			}	
			
			$xml_file.= "<g:condition>new</g:condition>";
			$xml_file.= "<g:availability>in stock</g:availability>";
			if (file_exists("https://armed-market.ru/upload/{$row['id']}.jpg")) 
				$xml_file.= "<g:image_link>https://armed-market.ru/upload/{$row['id']}.jpg</g:image_link>";
			$xml_file.= "</item>";

			//$offer->appendChild($xml->createElement('brand', $brand));


		}

		$xml_file.="</channel>
			</rss>";
		$fh=fopen($this->ymlFilePath,"w");
		fwrite($fh,$xml_file);

		// Сохраняем в YML
		//$xml->save($this->ymlFilePath);
		//mysql_query("SET NAMES 'cp1251'");



		$this->lastUpdate = date('d.m.Y H:i:s', mktime());
		// echo $this->lastUpdate;
		// echo $this->ymlFilePath;
		$this->saveParam("lastUpdate", $this->lastUpdate);

		return $this;
	}
	
}

?>