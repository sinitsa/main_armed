<?php
//header("Content-Type: text/xml; charset='utf-8'");
//$_SERVER['HTTP_ACCEPT'] = 'text/xml,'.$_SERVER['HTTP_ACCEPT'];
//var_dump($_SERVER['HTTP_ACCEPT']);
include ("../connect.php");
include ("../../func/core.php");

include ("../up.php");
include ("iyml.class.php");


$iy = new iYML; ?>

<script>
	$(document).ready( function(){

		$("#btn_build_yml").click( function(){
			if($(this).attr('_waiting') == 1){
				return false;
			}
			$(this).attr('_waiting',1);
			var _data = { 
				method: "generate",
				arguments: {}
			};
			
			_data.arguments.params = {};
			
			jsondata = JSON.stringify(_data);			
			btn = this;
			$.ajax({
				type: "POST",
				url: "iyml.ajax.php",
				data: { ajax: "1", data: jsondata }
			}).done( function( msg ) {
				data = JSON.parse(msg);
				$(".updated-on").html("Последнее обновление: "+data.lastUpdate);
				$(".ymlFilePath").html(data.ymlFilePath);
				$(".ymlFilePath").attr("href", data.ymlFilePath);
				var msgDiv = $("<div style=\"background-color:#EEE; padding:5px;border:2px solid #DDD; position:absolute; display:none;\">Выполнено</div>")
				$("body").append(msgDiv);
				$(msgDiv).css("left",	parseInt($(btn).position().left)+parseInt($(btn).width())+35+"px");
				$(msgDiv).css("top",	$(btn).position().top);
				$(msgDiv)	.show()
							.fadeOut(3000, function(){
								$(msgDiv).remove();
							});			
				$(btn).attr('_waiting',0);				
			});
			return false;
		})
		
		$("#btn_save_ymlparams").click( function(){
			var _data = { 
				method: "save",
				arguments: {}
			};
			
			_data.arguments.params = {};
			
			$("#iYMLParamForm input, #iYMLParamForm select").each( function(index, el){
				_name = $(el).attr('name');
				if (_name == 'selectedCategories' || _name == 'selectedCategoriesFakeRests') {
					if (jQuery.isEmptyObject($(el).val())) {
						_val = '';
					} else {
						_val = $(el).val().join(',');
					}
				} else {
					_val  = $(el).val();
				}
				_data.arguments.params[_name] = _val;
			});
			jsondata = JSON.stringify(_data);
			
			btn = this;
			$.ajax({
				type: "POST",
				url: "iyml.ajax.php",
				data: { ajax: "1", data: jsondata }
			}).done( function( msg ) {
				var msgDiv = $("<div style=\"background-color:#EEE; padding:5px;border:2px solid #DDD; position:absolute; display:none;\">Сохранено</div>")
				$("body").append(msgDiv);
				$(msgDiv).css("left",	parseInt($(btn).position().left)+parseInt($(btn).width())+35+"px");
				$(msgDiv).css("top",	$(btn).position().top);
				$(msgDiv)	.show()
							.fadeOut(3000, function(){
								$(msgDiv).remove();
							});									
			});
			return false;
		})
		
	})
</script>

<div  style="width: 90%; margin-left: 5%">

	<ul class="nav nav-tabs">
		<li class="active"><a href="#yml_builder" data-toggle="tab">Построитель XML Mail.ru</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="yml_builder">
			<legend>
				<span class="big_menu">
					<img src="ico/market_big.jpg" align="absmiddle" /> Построитель XML Mail.ru. Настройки построителя.
				</span>
			</legend>
			
			<a href="#" class="btn btn-primary" id="btn_build_yml">
				<i class="icon-play"></i>
				Сгенерировать файл
			</a>
			<span class="yml-file-path"><a href="<?php echo $iy->ymlFilePath; ?>" target="_blank" type="text/xml" class="ymlFilePath" title="Открыть файл в новом окне"><?php echo $iy->ymlFilePath; ?></a></span>
			<br>
			<span class="updated-on">Последнее обновление: <?php echo $iy->lastUpdate; ?> </span>
			<hr>
			<?php echo $iy->buildParamHTML(); ?>
			<a href="#" class="btn" id="btn_save_ymlparams">
				<i class="icon-ok"></i>
				Сохранить
			</a>
		</div>
	</div>

	

</div>

<?php include ("../down.php");	?>