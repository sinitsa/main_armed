<?php
include ("../connect.php");
include ('../../func/core.php');

if (isset($_POST['add_banner'])) {

	$url = mysql_real_escape_string($_POST['url']);
	$newWindow = isset($_POST['target']) ? '1' : '0';

	mysql_query(
		"INSERT INTO
			`banners_main`
		SET
			`url` = '{$url}',
			`new_window` = '{$newWindow}'
		");
	if (mysql_affected_rows() >= 0) {
		$bannerId = mysql_insert_id();
		
		mysql_query("UPDATE `banners_main` SET `sort` = '". (fetchOne("SELECT MAX(`sort`) FROM `banners_main`") + 1) ."' WHERE `id` = '{$bannerId}'");

		is_uploaded_file($_FILES['banner_main']['tmp_name']);

		if (is_uploaded_file($_FILES['banner_image']['tmp_name'])) {

			//Оригинал
			uploadAndResize($_FILES['banner_image'], getImagePath('banner_main') , $bannerId);
			//Превью
			uploadAndResize($_FILES['banner_image'], getImagePath('banner_main') , $bannerId . '_s', 150, 150);
		}
		redirect('/edit/m_main_banner/');
	}
}

include ("../up.php"); 
?>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div><a href="/edit/m_main_banner/">&larr; Назад</a></div>
			<br />
			<div><h4>Добавить баннер:</h4><div>
			<div>
				<form action="" method="post" enctype="multipart/form-data">
					Ссылка: <br />
					<input type="text" name="url" value="" /><br />
					<input type="checkbox" name="target" /> Открывать в новом окне <br />
					Изображение: <br/>
					<input type="file" name="banner_image" /><br />
					<input type="submit" name="add_banner" value="Добавить" />
				</form>
			</div>
			<div>&nbsp;</div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>