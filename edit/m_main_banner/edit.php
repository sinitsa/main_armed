<?php
include ("../connect.php");
include ('../../func/core.php');

if (is_numeric($_GET['id'])) {
	$bannerId = $_GET['id'];
} else {
	redirect('/edit/m_main_banner/');
}

if (isset($_POST['save_banner'])) {

	$url = mysql_real_escape_string($_POST['url']);
	$newWindow = isset($_POST['target']) ? '1' : '0';

	mysql_query(
		"UPDATE
			`banners_main`
		SET
			`url` = '{$url}',
			`new_window` = '{$newWindow}'
		WHERE
			`id` = '{$bannerId}'
		");
	
	if (is_uploaded_file($_FILES['banner_image']['tmp_name'])) {
		//Оригинал
		uploadAndResize($_FILES['banner_image'], getImagePath('banner_main') , $bannerId);
		//Превью
		uploadAndResize($_FILES['banner_image'], getImagePath('banner_main') , $bannerId . '_s', 150, 150);
	}
}

$data = getMainBanner($bannerId);

include ("../up.php"); 
?>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div><a href="/edit/m_main_banner/">&larr; Назад</a></div>
			<br />
			<div><h4>Редактировать баннер:</h4><div>
			<div>
				<form action="" method="post" enctype="multipart/form-data">
					Ссылка: <br />
					<input type="text" name="url" value="<?=$data['url']?>" /><br />
					<input type="checkbox" name="target" <?=( $data['new_window']  ? 'checked="checked"':'')?>/> Открывать в новом окне <br />
					Изображение: <br/>
					<img src="<?=getImageWebPath('banner_main') . $data['id']?>.jpg?<?=rand(1,9999);?>" /><br />
					<input type="file" name="banner_image" /><br />
					<input type="submit" name="save_banner" value="Сохранить" />
				</form>
			</div>
			<div>&nbsp;</div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>