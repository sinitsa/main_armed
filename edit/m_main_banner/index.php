<?php
include ("../connect.php");
include ('../../func/core.php');

include ("../up.php"); 


?>
<script type="text/javascript" src="/js/jquery-ui-1.8.13.js"></script>
<script type="text/javascript">
$(function(){
	$('.delete-banner').bind('click', function () {
		var banner = $(this).closest('.banner');
		var id = banner.data('id');
		

		if (confirm("Удалить безвозвратно?")) {
			$.ajax({
				url : 'ajax.php',
				data : {'method' : 'delete', 'id' : id},
				dataType : 'json',
				type : 'post',
				success : function () {
					banner.fadeOut('fast', function () {
						banner.remove();
					});
				}
			});
		}
	});

	$( '#sort-list' ).sortable({
		connectWith : '.banner',
		tolerance : 'intersect',
		handle : '.move-area',
		update : function (event, ui) {
			var sortList = new Array();

			$('#sort-list .banner').each(function (index, element) {
				sortList.push({
					'id' : $(element).data('id'),
					'sort' : index
				});
			});

			$.ajax({
					url: 'ajax.php?method=setsort',
					type : 'POST',
					dataType : 'json',
					data : {"sort_list" : sortList},
					beforeSend : function () {},
					success : function (data, textStatus, jqXHR) {},
					error : function (jqXHR, textStatus, errorThrown) {}
				});
		}
	});
});
</script>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div>
				<a href="add.php">Добавить баннер</a>
			</div>
			<br />
			<div><h4>Список баннеров</h4></div>
			<div id="sort-list">
				<?php foreach (getMainBanners(true) as $banner) { ?>
					<div style="margin-bottom: 5px; background: #fff;" class="banner" data-id="<?=$banner['id']?>">
						<div style="float: left; min-width: 50px; background: #eee; cursor: move;" class="move-area">
							<img src="<?=getImageWebPath('banner_main') . $banner['id']?>_s.jpg" />
						</div>
						<div style="padding-left: 160px; padding-top: 10px;">
							URL: <?=$banner['url']?> <br />
							Открывать в<strong><? if ($banner['new_window']) { ?> новом <? } else { ?> том же <?php } ?></strong>окне <br />
							<a href="edit.php?id=<?=$banner['id']?>">Редактрировать</a><br />
							<a href="javascript:void(0);" class="delete-banner">Удалить</a><br />
						</div>
						<div style="clear: both;"></div>
					</div>
				<?php } ?>
			</div>
			<div>&nbsp;</div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>