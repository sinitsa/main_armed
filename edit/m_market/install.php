<?php 
include ('../../connect.php');
//include ("../../func/core.php");
include ("iyml.class.php");

$iy = new iYML;

$iy->install();

mysql_query("

INSERT INTO `iyml_config` (`name`, `definition`, `description`, `value`, `sort`) VALUES
('siteName', NULL, 'Название сайта', '-', '0'),
('siteCompany', NULL, 'Название компании', '-', '1'),
('currencyName', NULL, 'Валюта', 'RUR', '2'),
('currencyRate', NULL, 'Курс валюты', '1', '3'),
('catalogImageType', NULL, 'Размер изображения товара', 'product_preview', '4'),
('ymlFilePath', NULL, 'Путь к файлу YML', 'yml1.yml', '5'),
('lastUpdate', NULL, 'Последнее обновление', '', '0'),
('controlAmount', NULL, 'Контрольное количество', '0', '6'),
('controlMargin', NULL, 'Контрольная маржа', '100', '7'),
('controlPrice', null, 'Контрольная стоимость', '0', '8'),
('bidInStock', null, 'Ставка (в наличии)', '0', '9'),
('bidNotAvailable', null, 'Ставка (не в наличии)', '0', '10'),
('selectedCategories', NULL, 'Товары в этих категориях', '', '11'),
('selectedCategoriesFakeRests', NULL, 'Для товарах в этих категорий всегда будет наличие', '', '13'),
('onRequest', NULL, 'На заказ', '0', '12'),
('includeRests', NULL, 'Включать информацию о стоимости доставки', '0', '14');

") or die(mysql_error()); 
?>