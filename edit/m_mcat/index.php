<?php
include ("../connect.php");
include ("../../func/core.php");
?>
<?php
$cssOl = true;
include ("../up.php");
?>
<div class="ol" style="width:100%; height:100%; position:absolute; z-index:-2;" align="center">
    <div style="width:100%; height:100%; max-width:1270px; min-width:1000px;" align="center">
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="99%" align="center" valign="top">
                    <table width="96%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" valign="top">
                                <table width="250" border="0" align="right" cellpadding="0" cellspacing="0" class="bordotziv2" style="margin-top:30px;">
                                    <tr>
                                        <td align="left" valign="top">
                                            <div class="bordotziv3">
                                                <div class="txtzakser"><a href="/edit/m_menuleft/">Главное меню</a></div>
                                                <div class="txtzakser"><a href="/edit/m_cat/add.php">Создать категорию</a></div>
                                                <div class="txtzakser"><a href="/edit/m_filter/add.php">Создать фильтр</a></div>
                                                <div class="txtzakser"><a href="/edit/m_articles/add.php">Добавить статью</a></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div style="float: right; clear: both; width: 240px; text-align: left; padding: 5px;">
                                    <div style="padding: 10px;">
                                        <h3 style="color: #a29d9f; font-weight: normal;">Глобальные фильтры</h3>
                                        <ul style="list-style: none; margin: 5px 0 0 15px;">
                                            <?php foreach (getAvailableParams(true) as $param) { ?>
                                                <li class="mcat-li-style">
                                                    <a href="/edit/m_filter/edit.php?id=<?php echo $param['id']; ?>"><?php echo $param['title']; ?></a>&nbsp;
                                                    <a href="/edit/m_filter/delete.php?id=<?php echo $param['id']; ?>"><img src="/img/dopfotodel.png" width="12" height="12" alt="" /></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div style="padding: 10px;">
                                        <h3 style="color: #a29d9f; font-weight: normal;">Статьи</h3>
                                        <ul style="list-style: none; margin: 5px 0 0 15px;">
                                            <?php foreach (getCatArticles(0) as $article) { ?>
                                                <li class="mcat-li-style">
                                                    <a href="/edit/m_articles/edit.php?id=<?php echo $article['id']; ?>"><?php echo $article['title']; ?><a>&nbsp;
                                                            <a href="/edit/m_articles/delete.php?id=<?php echo $article['id']; ?>&cat_id=0"><img src="/img/dopfotodel.png" width="12" height="12" alt="" /></a>
                                                            </li>
                                                        <?php } ?>
                                                        </ul>
                                                        </div>
                                                        </div>
                                                        <!-- -->
                                                        <?php foreach (getMenu() as $item) { ?>



                                                            <div style="margin-bottom:20px; margin-top:30px;" align="left">
                                                                <table border="0" cellspacing="0" cellpadding="0" >
                                                                    <tr>
                                                                        <td><img src="/img/oglleft.jpg" width="14" height="35"></td>
                                                                        <td class="oglrast"><div style="margin-left:10px; margin-right:10px;"><a href="/edit/m_menuleft/edit.php?id=<?php echo $item['id']; ?>"><?php echo $item['title'] ?></a></div></td>
                                                                        <td><img src="/img/oglright.jpg" width="14" height="35"></td>
                                                                    </tr>
                                                                </table>
                                                                <img src="/img/ogln.jpg" width="59" height="7">
                                                            </div>
                                                            <?php if (count($item['submenu']) > 0) { ?>
                                                                <div style="margin-left:-15px;">
                                                                    <?php foreach ($item['submenu'] as $submenu) { ?>

                                                                        <?php
                                                                        //print_r($submenu);
                                                                        ?>
                                                                        <div class="divkat">
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <a href="<?=backendLinks($submenu['chpu'])['view']?>">
																							<div>
																								<img src="<?php echo getCatImagePath($submenu['chpu'])?>" width="114" height="128">
																							</div>
                                                                                            <?php echo $submenu['title'] ?>
																						</a>
																						<div><span class="txt_vgd"><?php echo $submenu['products_count']; ?></span> <span class="teltxt">товаров</span></div>
                                                                                    </td>
                                                                                    <td align="right" valign="top">
                                                                                        <a href="/edit/m_cat/ask_del.php?id=<?php echo $submenu['cat_id']; ?>"><img src="/img/zndelete.gif" width="26" height="25" border="0" style="margin-bottom:5px;"></a><br>
                                                                                        <a href="<?=backendLinks($submenu['chpu'])['edit']?>"><img src="/img/znedit.gif" width="26" height="25" border="0"></a>
                                                                                        <span style="margin: 10px auto 0; display: block; width: 16px; height: 17px; background: url(/img/checkbox-sprite.png); background-repeat: no-repeat; background-position: 0 <?php echo ($submenu['hidelink'] == 1) ? '0' : '-16px'; ?>;" title="<?php echo ($submenu['hidelink'] == 1) ? 'Скрыто от роботов' : 'Доступно для роботов'; ?>">&nbsp;</span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        <div style="margin-bottom:20px; margin-top:30px;" align="left">
                                                            <table border="0" cellspacing="0" cellpadding="0" >
                                                                <tr>
                                                                    <td><img src="/img/oglleft.jpg" width="14" height="35"></td>
                                                                    <td class="oglrast"><div style="margin-left:10px; margin-right:10px;">Категории без привязки к разделам</div></td>
                                                                    <td><img src="/img/oglright.jpg" width="14" height="35"></td>
                                                                </tr>
                                                            </table>
                                                            <img src="/img/ogln.jpg" width="59" height="7">
                                                        </div>
                                                        <div style="margin-left:-15px;">
                                                            <?php foreach (getCatsNotInMenu() as $cat) { ?>
                                                                <div class="divkat">
                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <a href="/edit/m_catalog/list.php?id=<?php echo $cat['id'] ?>"><div><img src="<?php echo getImageWebPath('cats_menu') . $cat['id']; ?>.jpg" width="114" height="128"></div>
                                                                                    <?php echo $cat['title'] ?></a><div><span class="txt_vgd"><?php echo $cat['products_count']; ?></span> <span class="teltxt">товаров</span></div>
                                                                            </td>
                                                                            <td align="right" valign="top">
                                                                                <a href="/edit/m_cat/ask_del.php?id=<?php echo $cat['id']; ?>"><img src="/img/zndelete.gif" width="26" height="25" border="0" style="margin-bottom:5px;"></a><br>
                                                                                <a href="/edit/m_cat/edit.php?id=<?php echo $cat['id']; ?>"><img src="/img/znedit.gif" width="26" height="25" border="0"></a>
                                                                                <span style="margin: 10px auto 0; display: block; width: 16px; height: 17px; background: url(/img/checkbox-sprite.png); background-repeat: no-repeat; background-position: 0 <?php echo ($cat['hidelink'] == 1) ? '0' : '-16px'; ?>;" title="<?php echo ($cat['hidelink'] == 1) ? 'Скрыто от роботов' : 'Доступно для роботов'; ?>">&nbsp;</span>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        </td>
                                                        </tr>
                                                        </table>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="80">&nbsp;</td>
                                                        </tr>
                                                        </table>

                                                        </div>
                                                        </div>



                                                        <?php include ("../down.php"); ?>