<?php
include ("../connect.php");
include ("../../func/core.php");

if ($_GET['del'] || $_POST['menu_add']) {
	include('sql.php');
}
mysql_query("SET NAMES utf8");
mysql_query("SET CHARACTER SET utf8");
mysql_query("SET CHARSET utf8");
mysql_query("DELETE FROM menuleft WHERE title = '' AND chpu = '' AND cat_id = 0");

$cssOl = true;
include ("../up.php");

?>
<script type="text/javascript" src="/js/jquery-ui-1.8.13.js"></script>
<script type="text/javascript">
	$(function() {
		$('.btn.element-add').on('click', function(){
			var pod = $(this).data('id');
			var menu_item = $(this).prevAll('.element-items');
			console.log(menu_item, 'click!');
			$.ajax({
				url: 'ajax.php?method=addelement',
				type : 'POST',
				dataType : 'json',
				data : {
					"pod" : pod
				},
				beforeSend : function () {
				},
				success : function (data, textStatus, jqXHR) {
					console.log(data.id);
					var form = $('<form></form>').addClass('element-item').attr('data-id', data.id).attr('data-cat_id', '1488');
					var input_name = $('<input />').attr('name', 'title').attr('type', 'text').attr('value', '').attr('data-id', data.id);
					var input_chpu = $('<input />').attr('name', 'chpu').attr('type', 'text').attr('value', '').attr('data-id', data.id).css('margin-left', '4px');
					var delete_btn = $('<span></span>').addClass('delete-btn').attr('onClick', "deleteElement(" + data.id + ")");
					var img = $('<img />').attr('src', '/img/dopfotodel.png').css('vertical-align', 'middle');
					delete_btn.append(img);	
					form.append(input_name).append(input_chpu).append(delete_btn).appendTo(menu_item);
				}
				//error : function (jqXHR, textStatus, errorThrown) {}
			});
			
			return false;
		});
		
		$(document).on('change', '.element-item input',function(){
			
			var input = $(this) ;
			//console.log(input.attr('name'));
			
			//$(document).on('blur', '.element-item input', function() {
			
			
	
			var id = ($(this).attr('data-id') - 0);
			var name = $(this).attr('name');
			var value = $(this).val();
			//console.log([id, name, value]);
			
			$.ajax({
				url: 'ajax.php?method=setelementvalue',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : id,
					"name" : name,
					"value" : value
				},
				beforeSend : function () {
					input.attr('disabled', 'disabled');
				},
				success : function (data, textStatus, jqXHR) {
					input.removeAttr('disabled');
					//console.log(data);
				}
				//error : function (jqXHR, textStatus, errorThrown) {}
			});
			
			return false;
			//});
		
		});
	
		$('#menu-items').sortable({
			connectWith: "div.menu-item .header",
			tolerance : "pointer",
			update : function (event, ui) {
				saveMenuOrder();
			}
		});
	
		var sortableIn = 0;
		$('.element-items').sortable({
			//connectWith: ".cat-items",
			items : ".element-item",
			tolerance : "pointer",
			//cancel: ".cl",
			update : function (event, ui) {
				var catId = ui.item.data('cat_id');
				var count = 0;

				if (count <= 1)	saveMenuItemsOrder(ui.item.closest('.element-items')); 
			},
			receive: function(e, ui) {
				sortableIn = 1;
			},
			over: function(e, ui) {
				sortableIn = 1;
				ui.item.css('opacity', 1);
			},
			out: function(e, ui) {
				sortableIn = 0;
				ui.item.css('opacity', .5);
			},
			stop : function(e, ui) {
				ui.item.css('opacity', 1);
			},
			beforeStop: function(e, ui) {
			  if (sortableIn == 0) { 
					var id = ui.item.data('id');
					//ui.item.remove(); 
					//removeMenuItem(id);
			   }
			}
		});  //.disableSelection(); 
		/*
		$( "#all-cats-list .cat-item" ).draggable({
			connectToSortable: ".cat-items",
			helper: "clone",
			revert: "invalid",
			stop : function (event, ui) {
				//alert(ui.helper);
			}
		});
		*/
		refreashCatsCounter();
	});
	//Сейвим порядок следования меню
	function saveMenuOrder() {
		var orderList = new Array();
		
		$('#menu-items .menu-item').each(function (index, element) {
			orderList.push($(element).data('id'));
		});
		
		$.ajax({
			url: 'ajax.php?method=setmenuorder',
			type : 'POST',
			dataType : 'json',
			data : {"order_list" : orderList},
			//beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {}
			//error : function (jqXHR, textStatus, errorThrown) {}
		});
	}
	//Сейвим порядок следования элементов
	function saveMenuItemsOrder(elements) {
		var items = new Array();
		var menuId = elements.closest('.menu-item').data('id');
		
		elements.children('.element-item').each(function (index, element) {
			items.push({
				"id" : $(element).data('id'),
				"cat_id" : $(element).data('cat_id')
			});
		});
		
		$.ajax({
			url: 'ajax.php?method=setmenuitemsorder',
			type : 'POST',
			dataType : 'json',
			data : {"menu_id": menuId, "order_list" : items},
			beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {
				if (!data.error) {
					//заменяем "new" на айдишник
					elements.children('.element-item').each(function (index, element){
						var id = $(element).data('id');
						var catId = $(element).data('cat_id');
						if (id == 'new') {
							//зачем заложил возможность массового апдейта?
							for (var i in data.update) {
								if (data.update[i].cat_id == catId) {
									$(element).data('id', data.update[i].id);
									//hack. с первого раза не работает почему-то
									$(element).attr('data-id', data.update[i].id);
									return false;
									//break;
								}
							}
						}
					});
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {
				
			}
		});
		refreashCatsCounter();
	}
	//Удаляем категорию в пункте меню
	function removeMenuItem(itemId) {
		$.ajax({
			url: 'ajax.php?method=deletemenuitem',
			type : 'POST',
			dataType : 'json',
			data : {"cat_item_id": itemId},
			beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {

			},
			error : function (jqXHR, textStatus, errorThrown) {
				
			}
		});
		refreashCatsCounter();
	}
	function areYouSure() {
		return confirm('Уверены, что хотите удалить пункт меню?');
	}
	
	//Обновить счетчик использования категорий в меню
	function refreashCatsCounter() {
		$('#all-cats-list .element-item').each(function(i, element) {
			var counter = 0;
			var catId = $(element).data('cat_id');
			//alert(catId);
			$('#menu-items .element-item').each(function(i, el) {
				if ($(el).data('cat_id') == catId) counter++;
			})
			
			if (counter > 0) {
				$(element).find('.counter').text(counter).addClass('counter-style');
			} else {
				$(element).find('.counter').text('').removeClass('counter-style');
			}
		});
	}
	
	function deleteElement(id) {
		var form = $(".element-item[data-id='"+id+"']");
		console.log(form);
		$.ajax({
			url: 'ajax.php?method=deleteelement',
			type : 'POST',
			dataType : 'json',
			data : {
				"id" : id
			},
			/*beforeSend : function () {
				
			},*/
			success : function (data, textStatus, jqXHR) {
				form.fadeOut("slow");
			}
			//error : function (jqXHR, textStatus, errorThrown) {}
		});
	}
</script>
<table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="menu-editor">
			<div class="ol">
				<h2>Добавить новый пункт меню</h2>
				<form action="#menu-items" method="post">
					<input type="text" name="menu_name" />
					<input type="submit" name="menu_add" value="Добавить" />
				</form>
			</div>

			
			<div id="menu-items">
				<?php foreach (getMenu(true) as $item) { ?>
						<div class="menu-item" data-id="<?=$item['id'];?>">
							<div class="header">
								<div style="margin-bottom:20px; margin-top:10px;" align="left">
									<table border="0" cellspacing="0" cellpadding="0" >
										<tr>
											<td><img src="/img/oglleft.jpg" width="14" height="35"></td>
											<td class="oglrast">
												<div style="margin-left:10px; margin-right:10px;">
												<a href="/edit/m_menuleft/edit.php?id=<?php echo $item['id']; ?>">
														<?php echo $item['title']?>
												</a>
												&nbsp;
												<a href="?del=<?=$item['id']?>#menu-items" onclick="return areYouSure();">
													<img style="vertical-align: middle;" src="/img/dopfotodel.png" />
												</a>
												</div>	
											</td>
											<td><img src="/img/oglright.jpg" width="14" height="35"></td>
										</tr>
									</table>
									<img src="/img/ogln.jpg" width="59" height="7">
								</div>
							</div>
						
							<div class="element-items">
								<?php if (count($item['submenu']) > 0) { ?>
									<?php foreach ($item['submenu'] as $submenu) { ?>

										<form class="element-item" data-id="<?php echo $submenu['id']; ?>" data-cat_id="<?php echo $submenu['cat_id']; ?>">											
											<input type="text" data-id="<?php echo $submenu['id']; ?>" name="title" value="<?=$submenu['title'];?>"/>
											<input type="text" data-id="<?php echo $submenu['id']; ?>" name="chpu" value="<?=$submenu['chpu'];?>"/>
											<span class="delete-btn" onclick="deleteElement(<?=$submenu['id']?>)">
												<img style="vertical-align: middle;" src="/img/dopfotodel.png" />
											</span>
										</form>
									<?php } ?>
								<?php } ?>
							</div>
							
							<input class="btn element-add" type="submit" value="Добавить элемент" data-id="<?=$item['id']?>" />
							
						</div>
				<?php } ?>
			</div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>