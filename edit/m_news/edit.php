<?php
include ("../connect.php");
include ("../../func/core.php");

$articleId = isset($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : 0 ;

if (isset($_POST['edit'])) {
	include('edit_handler.php');
}
if ($articleId > 0) {
	$article = getArticle($articleId, false, 'news');	
	//paf($article);
}

/*
$query = "ALTER TABLE news ADD show_in_list int(5)";
mysql_query($query) or die(mysql_error());
$query = "ALTER TABLE news ADD short TEXT";
mysql_query($query) or die(mysql_error());
$query = "ALTER TABLE news ADD chpu CHAR(50)";
mysql_query($query) or die(mysql_error());
*/
$cssOl = true;
include ("../up.php");

?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
 <table width="90%" border="0" align="center" class="txt">

	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<div style="float: left;margin: 0 250px 0 0;"><a href="/edit/m_articles">Назад к списку статей</a></div>
			<div><a href="<?php echo getTemplateLink($article, 'articles');?>" style="color: #fff; font-weight: bold; text-decoration: none;" class="btn btn-warning" target="_blank">Посмотреть на сайте</a>
			</div>
			<?php if (count($errors) > 0) {
				foreach($errors as $error) {
					echo '<div class="error">'.$error.'</div>';
				}
			} ?>

			<form action="" method="post" enctype="multipart/form-data">	
				<div class="txtbname">Категории</div>
				<!--<select name="cat_id" class="p_cat">
					<?php /*
					$cats = getCats();
					foreach ($cats as $cat) {
						echo "<option value=\"{$cat['id']}\"".($cat['id'] == $article['cat_id'] ? ' selected="selected"':'').">{$cat['title']}</option>";
						if (isset($cat['subcats']) && count($cat['subcats']) > 0) {
							foreach ($cat['subcats'] as $subcat) {
								echo "<option value=\"{$subcat['id']}\"".($subcat['id'] == $article['cat_id'] ? ' selected="selected"':'').">- {$subcat['title']}</option>";
							}
						}
					}
					*/?>
				</select>-->			
				<div class="txtbname">Заголовок статьи (вывод в списке):</div>
				<div><input class="span4 p_name" type="text" name="title" value="<?=$article['title'];?>" /></div>
				<div class="txtbname">ЧПУ:</div>
				<div><input class="span4 p_name" type="text" name="chpu" value="<?=$article['chpu'];?>" /></div>
				<div class="txtbname">СЕО title:</div>
				<div><input class="span4 p_name" type="text" name="seo_title" value="<?=$article['seo_title'];?>" /></div>
				<div class="txtbname">Текст:</div>
				<div>
					<textarea name="text" cols="70" rows="15" style="width: 400px;"><?php echo $article['text']; ?></textarea>
					<script type="text/javascript">
						console.log('hello!');
						var editor = CKEDITOR.replace( 'text', {stylesSet: 'my_custom_style'} );
						CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
					</script>
				</div>
				<div class="txtbname">Краткое описание:</div>
				<div>
					<textarea name="short" cols="70" rows="7" style="width: 400px;"><?php echo $article['short']; ?></textarea>
					<script type="text/javascript">
						var editor = CKEDITOR.replace( 'short', {height:200} );
						CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
					</script>
				</div>					
				<div style="margin: 10px 0;">Выводить в списке статей <input type="checkbox" name="show_in_list" <?=($article['show_in_list'] == 0 ? '' : 'checked="checked"')?> /></div>
				<!--<div>
					<div><h3>Изображение:</h3></div>
					<div>
						<img src="<?=getImageWebPath('article').$articleId?>.jpg" alt="" />
					</div>
					<div>
						<input type="file" name="image" />
					</div>
					<div class="info-text">(Авторесайз: 'Вкл'. Размер: <?=Config::get('image_size.article');?>)</div>
				</div>-->				
				<div><input type="submit" name="edit" value="Сохранить" /></div>
				<a href="/edit/m_news/delete.php?id=<?=$article['id'];?>" class="btn">Удалить</a>
			</form>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>