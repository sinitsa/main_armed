<?php 
include ('../connect.php'); 
include ('../../func/core.php');
$id = isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] > 0 ? $_GET['id'] : 0;

if ($id > 0 && isset($_GET['confirm']) && $_GET['confirm'] == 'true') {
	deleteStaticPage($id);
	redirect('/edit');
}
$data = getStaticPage($id);
$cssOl = true;
include ('../up.php'); 
?>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="ol">
			<h2>Удаление страницы</h2>
			<div>Вы действительно хотите удалить страницу "<strong><?=$data['title'];?></strong>"?</div>
			<a href="?id=<?=$id?>&confirm=true" class="btn">Удалить безвозвратно</a>&nbsp;
			<a href="/edit/" class="btn">Отмена</a>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>