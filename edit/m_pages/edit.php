<?php 
include ('../connect.php'); 
include ('../../func/core.php');
$id = $_GET['id'];
$staticPage = getStaticPage($id);
$cssOl = true;
include ('../up.php');
?>
	<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
      
      <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" class="ol">
        <tr>
          <td width="70%" align="left" valign="top">

<form  method="post" action="sql_edit.php?page_id=<?php echo $id; ?>">
<table width="90%" border="0" align="center" >
            <tr>
              <td width="77%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                  
                    <td valign="middle" class="big_menu"><h2>Редактировать страницу</h2></td>
                  </tr>
                </table></td>
          </tr>
            <tr>
              <td></td>
          </tr>
            <tr>
              <td>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td align="left" valign="top" class="span6"><input name="title" class="span6 p_name" type="text" id="title" value="<?php echo $staticPage['title']; ?>" size="100" /> </td>
					<td align="left" valign="top"><a href="<?php echo getTemplateLink($staticPage, 'page'); ?>" target="_blank" class="btn btn-info white"><i class="icon-play icon-white"></i> Посмотреть на сайте</a></td>
				  </tr>
				</table>
			  
			  </td>
          </tr>
            <tr>
              <td></td>
          </tr>
            <tr>
              <td><textarea name="des" cols="100" rows="10" id="des"><?php echo $staticPage['des']; ?></textarea>
			  
			<script type="text/javascript">
                 var editor = CKEDITOR.replace( 'des' , {
					width: 800,
					height: 500
					});
                CKFinder.setupCKEditor( editor, '/edit/ckfinder/' ) ;
            </script> 
			
			</td>
          </tr>
            <tr>
              <td><table width="300" border="0" align="left" cellpadding="2" cellspacing="0" class="txt_small">
                <tr>
                  <td bgcolor="#f7f7f7"><h2>SEO</h2></td>
                  <td bgcolor="#f7f7f7">&nbsp;</td>
                </tr>
                <tr>
                  <td bgcolor="#f7f7f7">ЧПУ</td>
                  <td bgcolor="#f7f7f7"><input name="chpu" type="text" id="chpu" value="<?php echo $staticPage['chpu']; ?>" size="100"  class="txt_small p_name" /></td>
                </tr>
                <tr>
                  <td bgcolor="#f7f7f7">&lt;title&gt;
                    &nbsp;</td>
                  <td bgcolor="#f7f7f7"><input name="seo_title" type="text" id="seo_title" value="<?php echo $staticPage['seo_title']; ?>" size="100"  class="txt_small p_name"/></td>
                </tr>
                <tr>
                  <td bgcolor="#f7f7f7">&lt;description&gt;</td>
                  <td bgcolor="#f7f7f7"><input name="seo_des" type="text" id="seo_des" value="<?php echo $staticPage['seo_des']; ?>" size="100"  class="txt_small p_name"/></td>
                </tr>
                <tr>
                  <td bgcolor="#f7f7f7">&lt;keywords&gt;</td>
                  <td bgcolor="#f7f7f7"><input name="seo_keys" type="text" id="seo_keys" value="<?php echo $staticPage['seo_key']; ?>" size="100"  class="txt_small p_name"/></td>
                </tr>
              </table></td>
            </tr>
			<tr>
				<td>
					<h2>Дополнительное поле</h2>
				</td>
			</tr>
			<tr>
				<td>
					<input name="extra" type="text" size="100"  class="txt_small p_name" value="<?=$staticPage['extra']?>"/><br/>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
            <tr>
              <td><table width="700" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>				  
						<button  type="submit" name="Submit" class="btn btn-large" /><i class="icon-ok"></i> Изменить</button>
					</td>
                  <td align="right"></td>
                </tr>
              </table>
			  </td>
            </tr>
            <tr>
              <td align="left" valign="top">&nbsp;</td>
              </tr>
			  <tr>
				<td><a href="delete.php?id=<?=$id;?>" class="btn">Удалить</a></td>
				</tr>
          </table>
      </form></td>
          <td width="30%" align="left" valign="top">
		  
		  <table width="90%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="big_menu">Страницы</td>
            </tr>
            <tr>
              <td><br>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
				<?php
				$staticPages = getStaticPages();
				foreach ($staticPages as $staticPage) {
					if ($staticPage['id'] == $id) {
						echo ('
						<tr>
							<td align="left" class="small_menu"><strong>'.$staticPage['title'].'</strong></td>
						</tr>
						');
					}
					else {
						echo ('
						<tr>
							<td align="left"><a href="/edit/m_pages/edit.php?id='.$staticPage['id'].'" class="small_menu">'.$staticPage['title'].'</a></td>
						</tr>
						');
					}
				}; 
				?>              
					<tr>
						<td align="left" class="small_menu">
						<a href="/edit/m_pages/add.php"  class="btn btn-success white"><i class="icon-plus icon-white"></i> Добавить</a>
						</td>
					</tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table>
		  </td>
        </tr>
      </table>
      <br>
<?php include ("../down.php"); ?>