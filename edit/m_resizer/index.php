<?
include('../connect.php');
include('../../func/core.php');

$cssOl = true;
include('../up.php');
?>
<style>
.mul {
	list-style: none; margin:0 0 0 5px; float: left;
}
.mul li label{
	cursor: pointer;
}
.loading, .complete, .error, .alternative, .a-error {
	display: none;
}
.loading img {
	vertical-align: middle;
}
</style>
<script src="/js/jquery.form.js" type="text/javascript"></script>
<script>
var handled = 0;
var errorCounter = 0;
$(function() {
	$('.resize-form').bind('submit', function () {
		var form = this;
		var oneByOneResizeMode = $(form).find('input[name="one_by_one"]').is(':checked');
		$('.error-log').empty();
		
		if (oneByOneResizeMode) {
			var continue_id = parseInt($(form).find('input[name="continue_id"]').val());
			resizeThisId(--continue_id);
		} else {
			$(this).ajaxSubmit({
				//data : {"page" : page, "sort" : sort},
				dataType : 'json',
				beforeSend : function () {
					$(form).find('.loading').show();
					$(form).find('input[type="submit"]').attr('disabled', 'disabled');
				},
				success : function ( data, statusText, xhr, element) {
					$(form).find('input[type="submit"]').removeAttr('disabled');
					$(form).find('.loading').hide();
					$(form).find('.complete span').text(data.resized);
					$(form).find('.complete').show();
					$(form).find('.error').hide();
					return false;
				},
				error : function (jqXHR, textStatus, errorThrown) {
					$(form).find('input[type="submit"]').removeAttr('disabled');
					$(form).find('.loading').hide();
					$(form).find('.error blockquote').html(jqXHR.responseText);
					$(form).find('.error').show()
					$(form).find('.complete').hide();
					return false;
				}
			});
		}
		return false;
	});
});

function resizeThisId(id) {
	var form = $('.resize-form');
	var tryingId = id;
	
	$(form).ajaxSubmit({
		dataType : 'json',
		data : { "id" : tryingId },
		beforeSend : function () {
			$(form).find('.loading').show();
			$(form).find('input[type="submit"]').attr('disabled', 'disabled');
			$(form).find('.alternative span').text(tryingId);
			$(form).find('.alternative').show();
		},
		success : function ( data, statusText, xhr, element) {
			if (data.last_id == 0) {
				$(form).find('input[type="submit"]').removeAttr('disabled');
				$(form).find('.loading').hide();
				$(form).find('.complete span').text(handled);
				$(form).find('.complete').show();
				handled = 0;
				errorCounter = 0;
			} else {
				handled++;
				resizeThisId(data.last_id);
			}
			return false;
		},
		error : function (jqXHR, textStatus, errorThrown) {
			$(form).find('.alternative blockquote').html(jqXHR.responseText);
			$(form).find('.alternative').show()
			$(form).find('.alternative .a-error span').html(tryingId);
			
			$(form).find('.alternative .a-error').clone().appendTo('.error-log').show();
			
			if (errorCounter < 10) {
				errorCounter++;
				resizeThisId(++tryingId);
			} else {
				alert('Too many errors');
			}
			return false;
		},
		complete : function() {
			$(form).find('input[name="continue_id"]').val(tryingId);
		}
	});
}
</script>
<table width="90%" border="0" align="center" class="txt ol">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<h4>Ресайз изображений</h4>
			<br />
			<h4>Ресайз изображений товаров</h4>
			<div>
				<form class="resize-form" action="ajax.php?method=resizeproducts" method="post">
					<ul class="mul">
						<li><label><input style="display: inline;" type="checkbox" name="preview" /> Превью основного фото (<?=Config::get('image_size.product_preview');?>)</label></li>
						<li><label><input style="display: inline;" type="checkbox" name="preview_extra" /> Превью дополнительного фото (<?=Config::get('image_size.product_extra_preview');?>)</label></li>
						<li><label><input style="display: inline;" type="checkbox" name="extra_medium" /> Средний размер дополнительного фото (<?=Config::get('image_size.product_extra_medium');?>)</label></li>
						<li><label><input style="display: inline;" type="checkbox" name="medium" /> Средний размер (<?=Config::get('image_size.product_medium');?>)</label></li>
						<li><label><input style="display: inline;" type="checkbox" name="small" /> Маленький размер (<?=Config::get('image_size.product_small');?>)</label></li>
						<li><label><input style="display: inline;" type="checkbox" name="one_by_one" /> Альтернативный режим обработки</label></li>
						<li style="padding-left: 20px;">Продолжить с id <input type="text" style="width: 25px;" name="continue_id" value="1"></li>
					</ul>
					<div class="cl"></div>
					<div class="loading">
						<img src="/img/ajax_loading.gif" alt=""/> Идет обработка изображений...
					</div>
					<div class="complete">
						Обработка завершена. Обработано <span>х</span> изображений.
					</div>
					<div class="error">
						Обработка завершена с ошибкой.<br />
							<blockquote></blockquote>
					</div>
					<div class="alternative">
						Обрабатывается товар с id = <span></span>
						<div class="a-error">Проблемы с ресайзом изображений у товара id = <span></span></div>
						<blockquote></blockquote>
					</div>
					<input type="submit" name="" value="Запустить ресайз" />
				</form>
				<div class="error-log"></div>
			</div>
		</td>
	</tr>
</table>
<?
include('../down.php');