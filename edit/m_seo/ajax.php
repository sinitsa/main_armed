<?php
include('../../connect.php');
include('../../func/core.php');

switch ($_REQUEST['method']) {
	case 'change_input' : 
		file_put_contents('request.txt', var_export($_REQUEST, true));
		$arr = get_html_translation_table(HTML_ENTITIES, ENT_COMPAT | ENT_HTML401);
		file_put_contents('trans_table.txt', var_export($arr, true));
		
		if ($_REQUEST['id'] != 486)
			break;
		$value = htmlentities($_REQUEST['value'], ENT_QUOTES, "cp1251");
		//$value = iconv("utf-8", "cp1251", $value);
		file_put_contents('html.txt', $value);
		//$value = html_entity_decode($value, ENT_QUOTES, "utf-8");
		
		//$value = utf8_to_win($value);
		$table_name = ($_REQUEST['field_type'] == 'cat') ? 'cat' : 'tags';
		$value_type = ($_REQUEST['value_type'] == 'seo_title') ? 'seo_title' : 'seo_description';
		if ($value_type === 'seo_description') 
			$column_name = ($table_name === 'cat') ? 'seo_des' : 'seo_description';
		else 
			$column_name = 'seo_title';
		$id = (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) ? $_REQUEST['id'] : false;
		
		if ($id !== false) {
			$query = "UPDATE `{$table_name}`
						SET `{$column_name}` = '{$value}'
						WHERE id = {$id}";
			if (!mysql_query($query))
				echo json_encode(array("error" => true));
			//file_put_contents('query.txt', $query);
		}
		else
			echo json_encode(array("error" => true));
		break;

}