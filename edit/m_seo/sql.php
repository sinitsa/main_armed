<?php
	include ("../connect.php");
	include("../../func/core.php");
	file_put_contents('psto.txt', var_export($_POST, true));
	if (isset($_POST['type']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
		$id = $_POST['id'];
		$seo_title = mysql_real_escape_string($_POST['seo_title']);
		$seo_description = $_POST['seo_description'];
		$table_name = ($_POST['type'] == 'cat') ? 'cat' : 'tags';
		$description_column = ($_POST['type'] == 'cat') ? 'seo_des' : 'seo_description';
		$query = "UPDATE `{$table_name}`
					SET `seo_title` = '{$seo_title}',
						`{$description_column}` = '{$seo_description}'
					WHERE `id` = {$id}";
		file_put_contents('query.txt', $query);
		mysql_query($query);	
	}	
	header("Location: /edit/m_seo/index.php#{$id}");