<?php
include ("../connect.php");
include ("../../func/core.php");
$cssOl = true;
include ("../up.php");

$qu = mysql_query('select * from `catalog`');
$prod = array();
while ($res = mysql_fetch_array($qu)){
	// $desPart = '';
	$p1 = strpos($res['des'], 'ехнические');
	$desPart = 'Т';
	if ($p1==null){
		$p1 = strpos($res['des'], 'арактеристики');
		$desPart = 'Х';
	}
	if ($p1==null){
		$p1 = strpos($res['des'], 'Особенности');
		$desPart = '';
	}
	if($p1!=null){
		$p2 = strpos($res['des'], '<h2>', $p1);
		if ($p2!=null){
			$desPart .= substr($res['des'],$p1,$p2);
		}else{
			$desPart .= substr($res['des'],$p1);
		}
		$desPart = preg_replace('|<\w+/>|', '', $desPart);
		$desPart = preg_replace('|</\w+>|', '', $desPart);
		$desPart = preg_replace('|<\w+>|', '', $desPart);
		$desPart = strip_tags($desPart);
		$desPart = preg_replace("/\s*\r+/", "", $desPart);
		$res['desPart'] = $desPart;
		$prod[] = $res;
	}else{
		$res['desPart'] = '';
		$prod[] = $res;
	}
}
?>
<style>
body{min-width:1000px;}
.mainTitle{text-align:center;font-size:15pt;}
.cats{width:200px;height:100%;overflow:auto;display:inline-block;position:fixed;background:#fff;}
.cats a{text-decoration:none;}
.cats li{margin-bottom:10px;}
.cats .active a{font-style:bolder;color:red;}
.products{width:100%;display:inline-block;}
.products .title{text-align:center;font-size:14pt;}
.products .list{list-style:none;margin:0;margin-top:20px;}
.products .list .head{background: rgb(124, 124, 124);color: white;text-shadow: -1px -1px 2px black;padding: 7px 0;}
.products .list li{padding-top: 7px;}
.products .list li div{width:10%;display:inline-block;text-align:center;vertical-align: middle;}
.products .list li div.des{width:40%;vertical-align:top;}
.products .list li div.link{width:6%;}
.products .list li div.des textarea{margin:0;height:24px;width:100%;padding-bottom:0;-webkit-transition:height 0.3s ease-in;-moz-transition: height 0.3s ease-in;-o-transition: height 0.3s ease-in;transition: height 0.3s ease-in;}
.products .list li div.des textarea.expand{height:300px;}
.products .list .gab{width:50px;}
.products .list .changed{border-color:red;}
.products .list li.active{background:rgb(233, 233, 233);}
.products .list .inc,.products .list .decr{cursor:pointer;}
.products .list .btnHolder{position:relative;top:-3px;}
.products .list .head .btnHolder{position:relative;top:-6px;}

.products .list .sticky{position:fixed;top:0;display:block;width:100%;margin-left:-3px;background:rgb(124, 124, 124);z-index:23;box-shadow: 0px 0px 11px black;color:white;color: white;text-shadow: -1px -1px 2px black;}
.products .list .sticky .link{float: right;margin-right: 9%;}
</style>
<script>
function changeCss(e){
	$(e).css('position','static');
	liId = 0;
}
function offsetPosition(e) {
  var offsetTop = 0;
  do {offsetTop  += e.offsetTop;} while (e = e.offsetParent);
  return offsetTop;
}
$(document).ready(function(){
	window.onbeforeunload = function () {
		var is_data_changed = false;
		$('.changed').each(function(i,e){
			is_data_changed = true;
		});
		return (is_data_changed ? "Измененные данные не сохранены. Закрыть страницу?" : null); 
	} 
	
	var aside = document.getElementById('head');
	OP = offsetPosition(aside);
	window.onscroll = function() {
		aside.className = (OP < window.pageYOffset ? 'head sticky' : 'head');
	}

	// $('.cats').height($(window).height() - $('tbody tr').eq(0).height() - 20);
	
	// $('.cats li').each(function(i,e){
		// if ($(e).data('id')===<?=$id?>){
			// $('.cats').scrollTop($(e).offset().top - $('tbody tr').eq(0).height() - $('.cats').height()/2.5)
		// }
	// });
	var width = 0;
	var offset = 0;
	var timeout;
	var liId=0;
	$('.des textarea, .list .gab').on('focus',function(){
		if ($(this).closest('li').data('id')==liId){
			window.clearTimeout(timeout);
		}
		$(this).closest('li').addClass('active');
		var txtarea = $(this).closest('li').find('textarea');
		liId = $(this).closest('li').data('id');
		width = $(txtarea).width();
		offset = $(txtarea).offset().left;
		$(txtarea).css('position','absolute');
		$(txtarea).offset({left:offset});
		$(txtarea).addClass('expand');
		$(txtarea).width(width);
	});
	$('.des textarea, .list .gab').on('blur',function(){
		var txtarea = $(this).closest('li').find('textarea');
		$(this).closest('li').removeClass('active');
		$(txtarea).removeClass('expand');
		timeout = setTimeout(changeCss, 400, txtarea);
	});
	$('.des textarea, .list .gab').on('keyup',function(e){
		//----- только цифры ------
		var regexp = /[^0-9,.]+$/i;
		if (regexp.test($(this).val())){
			$(this).val($(this).val().replace(/[^0-9,.]+$/,''));
		}
		//------------
		
		var changedCount = 0;
		$(this).closest('li').find('.field').each(function(i,e){
			if ($(e).data('val') != $(e).val()){
				$(this).addClass('changed');
				changedCount++;
			}else{
				$(this).removeClass('changed');
			}
		});
		if(changedCount>0){
			$(this).closest('li').find('.btn').show();
		}else{
			$(this).closest('li').find('.btn').hide();
		}
	});
	$('.saveChanges').on('click',function(){
	if (confirm('Сохранить?')){
		var btn = $(this);
		var query='';
		btn.closest('li').find('.changed').each(function(i,e){
			query += $(e).attr('name') +'='+ $(e).val() +'&';
		});
		$.ajax({
			url : "ajax.php?method=update",
			type : "POST",
			dataType : "json",
			data : "id="+btn.data('id')+'&'+query,
			beforeSend: function() {
				btn.attr('disabled','disabled').removeClass('btn-success');
			},
			success : function (data) {
				btn.removeAttr('disabled').addClass('btn-success').hide();
				if (data.weight!==undefined){
					btn.closest('li').find('input[name="weight"]').attr('value',data.weight).data('val',data.weight).removeClass('changed');
				}
				if (data.len!==undefined){
					btn.closest('li').find('input[name="length"]').attr('value',data.len).data('val',data.len).removeClass('changed');
				}
				if (data.width!==undefined){
					btn.closest('li').find('input[name="width"]').attr('value',data.width).data('val',data.width).removeClass('changed');
				}
				if (data.height!==undefined){
					btn.closest('li').find('input[name="height"]').attr('value',data.height).data('val',data.height).removeClass('changed');
				}
				console.log(data);
			},
			complete : function() {},
			error : function () {
				btn.removeAttr('disabled').addClass('btn-danger');
			}
		});
		return false;
	}else{
		return false;
	}
		return false;
	});
	$('.decr').on('click',function(){
		$(this).closest('.fieldHolder').find('.field').val( parseFloat($(this).closest('.fieldHolder').find('.field').val())/10 );
		$(this).closest('.fieldHolder').find('.field').trigger('keyup').focus();
		try {
			window.getSelection().removeAllRanges();
		}catch(e) {
			document.selection.empty(); // IE<9
		}
		
	});
	$('.inc').on('click',function(){
		$(this).closest('.fieldHolder').find('.field').val( parseFloat($(this).closest('.fieldHolder').find('.field').val())*10 );
		$(this).closest('.fieldHolder').find('.field').trigger('keyup').focus();
		try {
			window.getSelection().removeAllRanges();
		}catch(e) {
			document.selection.empty(); // IE<9
		}
	});
});
</script>
<div class='mainTitle'>Габариты. <?=$currCat['title']?></div>
<?/*<div class='cats'>
	<ul>
		<?foreach ($cats as $cat){?>
			<li data-id='<?=$cat['id']?>' <?if($id===$cat['id']){echo 'class="active"';}?>><a href='index.php?c=<?=$cat['id']?>'><?=$cat['title']?></a></li>
		<?}?>
	</ul>
</div>*/?>
<div class="products">
	<ul class='list'>
		<li class='head' id='head'>
			<div>
				Название
			</div>
			<div>
				Вес (кг)
			</div>
			<div>
				Длина (м)
			</div>
			<div>
				Ширина (м)
			</div>
			<div>
				Высота (м)
			</div>
			<div class='des'>Описание</div>
		</li>
		<?foreach($prod as $p){?>
			<li data-id='<?=$p['id']?>'>
				<div class='fieldHolder'>
					<a href="http://beurer.tv/catalog/<?=$p['chpu']?>.html"><?=$p['title']?></a>
				</div>
				<div class='fieldHolder'>
					<input data-val='<?=$p['weight']?>' name='weight' type='text' value='<?=$p['weight']?>' class='gab field' />
					<div class='btnHolder'>
						<i class="icon-chevron-up inc"></i>
						<i class="icon-chevron-down decr"></i>
					</div>
				</div>
				<div class='fieldHolder'>
					<input data-val='<?=$p['length']?>' name='length' type='text' value='<?=$p['length']?>' class='gab field' />
					<div class='btnHolder'>
						<i class="icon-chevron-up inc"></i>
						<i class="icon-chevron-down decr"></i>
					</div>
				</div>
				<div class='fieldHolder'>
					<input data-val='<?=$p['width']?>' name='width' type='text' value='<?=$p['width']?>' class='gab field' />
					<div class='btnHolder'>
						<i class="icon-chevron-up inc"></i>
						<i class="icon-chevron-down decr"></i>
					</div>
				</div>
				<div class='fieldHolder'>
					<input data-val='<?=$p['height']?>' name='height' type='text' value='<?=$p['height']?>' class='gab field' />
					<div class='btnHolder'>
						<i class="icon-chevron-up inc"></i>
						<i class="icon-chevron-down decr"></i>
					</div>
				</div>
				<div class='des'><textarea tabindex='-1'><?=$p['desPart']?></textarea></div>
				<a class="btn btn-success btn-mini saveChanges" data-id='<?=$p['id']?>' href="#" style='display:none;'><i class="icon-ok icon-white"></i> Сохранить</a>
			</li>
		<?}?>
	</ul>
</div>

<?include("../down.php");?>