<?php 
include ('../connect.php'); 
include ('../../func/core.php');

if (isset($_POST['add'])) {
	if (strlen($_POST['title']) >= 2 && !getCatInfo($_POST['chpu'])	&&	!getTagInfo($_POST['chpu'])) {
		$data['title'] = mysql_real_escape_string(slashes($_POST['title']));
		
		if (strlen($_POST['menu_title']) >= 2)
			$data['menu_title'] = mysql_real_escape_string(slashes($_POST['menu_title']));
		else
			$data['menu_title'] = $data['title'];
		$data['short'] = mysql_real_escape_string($_POST['short']);
		$data['cat_id'] = mysql_real_escape_string($_POST['cat']);
		$data['seo_title'] = mysql_real_escape_string($_POST['seo_title']);
		$data['seo_description'] = mysql_real_escape_string($_POST['seo_desc']);
		$data['seo_text'] = mysql_real_escape_string($_POST['seo_text']);
		$data['seo_key'] = mysql_real_escape_string($_POST['seo_key']);
		$data['chpu'] = mysql_real_escape_string(slashes($_POST['chpu']));
		
echo '<pre>';
//print_r($_GET);
//print_r($catId);
print_r($data);
//print_r($_POST);

		$catId = is_numeric($data['cat_id']) ? $data['cat_id'] : 0;
		if ($catId != 0){ 
			$cat = getCatInfo($catId);
			$catName = $cat['title'];
		}	
		//Массив для подготовки sql запроса
		$paramsSqlIds = array();

		//Готовим sql запрос
		$setString = getSetString($data);

		//Добавляем параметры
		mysql_query("
			INSERT INTO
				`tags` 
			SET
			{$setString}
			");
		
		$id = mysql_insert_id();
		
		$size = getConfigImageSize('tag');
		$size_preview = getConfigImageSize('tag_preview');
		
		if (getConfigImageAutoresize('tags')) {
			$iWidthDest = $size['width'];
			$iHeightDest = $size['height'];
		} else {
			$iWidthDest = null;
			$iHeightDest = null;
		}
		
		$iWidthDest_preview = $size_preview['width'];
		$iHeightDest_preview = $size_preview['height'];
		
		$iQuality = getConfigImageQuality('tag');
		$iQuality_preview = getConfigImageQuality('tag_preview');
		
		uploadAndResize($_FILES['image'], getImagePath('tags'), $id, $iWidthDest, $iHeightDest, 1, $iQuality);
		uploadAndResize($_FILES['image'], getImagePath('tags_preview'), $id, $iWidthDest_preview, $iHeightDest_preview, 1, $iQuality_preview);
		
		if ($_POST['show_img'] == 1 && file_exists(getImagePath('tags_preview').$id.'.jpg')) 
			$show_img = 1;
		else
			$show_img = 0;
				
		$query = "UPDATE `tags` 
					SET
						`img` = {$show_img}
					WHERE 
						`id` = '{$id}'";
		mysql_query($query);	
		
		redirect("/edit/m_catalog/list.php?id=".$catId);
	}
}

$catId = is_numeric($_GET['cat_id']) ? $_GET['cat_id'] : 0;
if ($catId != 0){ 
	$cat = getCatInfo($catId);
	$catName = $cat['title'];	
	}	
$cssOl = true;
include ('../up.php'); 
?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
<script>
	$(function(){
		$('#title').bind('change', function() {
			var title = $(this).val();
			var catName = $('#catName').text();
			var seoTitle = catName + ' ' + title;
			
			$.post("/ajax/autochpu.php", {
				"set_chpu" : 1,
				"set_string" : document.getElementById('title').value
			}, function(data) {
				//alert(data);
				$('#chpu').val(data);
			});
			
			$('#seo_title').val(seoTitle);
		});
	});
</script>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="ol">
			<div><a href="/edit/m_catalog/list.php?id=<?=$catId;?>">Назад к категории "<span id="catName"><?php echo $catName;?></span>"</a></div>
			<div><h2>Добавление подкатегории:</h2><div>
			<form action="" method="post" enctype="multipart/form-data">
			<div><h3>Название:</h3></div>
			<div><input name="title" id="title" type="text" class="p_name" value="<?php echo schars($_POST['title']); ?>" /></div>

			<div><h3>Короткое название:</h3></div>
			<div><input name="short" type="text" class="p_name" value="" /></div>

			<div><h3>Дополнительное название (необязательно, выводится в левом меню):</h3></div>
			<div><input name="menu_title" type="text" class="p_name" value="<?php echo schars($data['menu_title']); ?>" /></div>			
			<div><h3>ЧПУ:</h3></div>
			<div><input name="chpu" id="chpu" type="text" class="p_name" value="<?php echo schars($_POST['chpu']); ?>" /></div>
			<?php if (isset($_GET['cat_id'])) {
				echo '<input type="hidden" name="cat" value="'.$_GET['cat_id'].'" />';
			} else { ?>
			<div><h3>Категория:</h3></div>
			<div>
				<select name="cat" class="p_cat">
				<?php
				foreach (getCats() as $cat) {
					echo "<option value=\"{$cat['id']}\"".($cat['id'] == $_GET['cat'] ? ' selected="selected"':'').">{$cat['title']}</option>";
				}
				?>
				</select>
			</div>
			<?php } ?>
			<div><h3>SEO заголовок:</h3></div>
			<div><input name="seo_title" id="seo_title" type="text" class="p_name span9" value="<?php echo schars($_POST['seo_title']); ?>" /></div>
			<div><h3>SEO описание:</h3></div>
			<div><input name="seo_desc" id="seo_desc" type="text" class="p_name span9" value="<?php echo schars($_POST['seo_desc']); ?>" /></div>	

			<div><h3>SEO keywords:</h3></div>
			<div><input name="seo_key" type="text" class="p_name span9" value="<?php echo schars($_POST['seo_key']); ?>" /></div>	
			
			
			<div><h3>Описание:</h3></div>
			<div>
				<textarea name="seo_text" id="seo_text" cols="70" rows="7" style="width: 400px;"><?=slashes($_POST['seo_text'])?></textarea>
				<script type="text/javascript">
					var editor = CKEDITOR.replace( 'seo_text' );
					CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
				</script>
			</div>
			<div>
				<div><h3>Изображение:</h3></div>
				<div>
					<input type="file" name="image" />
					<input type="checkbox" name="show_img" value="1" <? if ($data['img'] == 1) echo "checked"?>>Отображать картинку тега в главном меню<br>
				</div>
				<div class="info-text">(Авторесайз: <?=(getConfigImageAutoresize('tags') ? 'Вкл' : 'Выкл')?>. Размер: <?=Config::get('image_size.tag');?>)</div>
			</div>
			
	
			
			<div>
				<input class="btn" name="add" type="submit" value="Добавить" />
			</div>
			<form>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>