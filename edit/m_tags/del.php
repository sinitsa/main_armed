<?php 
include ('../connect.php'); 
include ('../../func/core.php');

if (is_numeric($_GET['tag_id'])) {
	$id = $_GET['tag_id'];
	$data = sqlFetch("
		SELECT
			*
		FROM
			`tags`
		WHERE 
			`id` = '{$id}'
	");
}

if (is_numeric($_GET['tag_id']) && $_GET['confirm'] == 'true') {
	$id = $_GET['tag_id'];
	
	$catId = fetchOne("SELECT `cat_id` FROM `tags` WHERE `id` = '{$id}'");
	
	mysql_query("DELETE FROM `tags` WHERE `id` = '{$id}' LIMIT 1");
	mysql_query("DELETE FROM `tags_links` WHERE `tag_id` = '{$id}'");
	
	@unlink(getImagePath('tag').$id.'.jpg');
	@unlink(getImagePath('tags_preview').$id.'.jpg');
	
	redirect("/edit/m_catalog/list.php?id=".$catId);
}

$cssOl = true;
include ('../up.php'); 
?>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="ol">
			<div>Вы действительно хотите удалить подкатегорию?</div>
			<p><strong><?=$data['title']?></strong></p>
			<p><img src="<?=getImageWebPath('tags').$data['id']?>.jpg" alt="" /></p>
			<div style="margin-top: 10px;">
				<a href="?tag_id=<?=$id?>&confirm=true" class="btn">Удалить безвозвратно</a>
				<a href="/edit/m_cat/edit.php?id=<?=$data['cat_id']?>" class="btn">Отмена</a>
			</div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>