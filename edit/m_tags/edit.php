<?php 
include ('../connect.php'); 
include ('../../func/core.php');

$cssOl = true;
include ('../up.php'); 

//Выбор данных
if (is_numeric($_GET['tag_id'])) {
	$data = sqlFetch("
		SELECT
			*
		FROM
			`tags`
		WHERE
			`id` = '{$_GET['tag_id']}'
	");

	$catId = $data['cat_id'];
	//$catInfo = getCatInfo($catId);
}

//echo '<pre>';
//print_r($_GET);
//print_r($catId);
//print_r($data);
//print_r($_POST);

$rand = '';
if (isset($_POST['edit']) && is_numeric($_GET['tag_id']) && !getCatInfo($_POST['chpu'])) {
	if (strlen($_POST['title']) >= 2) {
		$data['title'] = mysql_real_escape_string(slashes($_POST['title']));
		
		if (strlen($_POST['menu_title']) >= 2)
			$data['menu_title'] = mysql_real_escape_string(slashes($_POST['menu_title']));
		else
			$data['menu_title'] = $data['title'];
		$data['short'] = mysql_real_escape_string($_POST['short']);
		$data['cat_id'] = mysql_real_escape_string($_POST['cat']);
		$data['seo_title'] = mysql_real_escape_string($_POST['seo_title']);
		$data['seo_description'] = mysql_real_escape_string($_POST['seo_desc']);
		$data['seo_text'] = $_POST['seo_text'];
		$data['chpu'] = mysql_real_escape_string(slashes($_POST['chpu']));
		$data['seo_key'] = mysql_real_escape_string(slashes($_POST['seo_key']));
		
		$catId = is_numeric($data['cat_id']) ? $data['cat_id'] : 1;
		
		//Добавляем параметры
		mysql_query("
			UPDATE
				`tags`
			SET
				".getSetString($data)."
			WHERE `id` = '{$_GET['tag_id']}'
			"
			);
		$id = $_GET['tag_id'];
		
		if ($_FILES['image']) {
			$size = getConfigImageSize('tag');
			$size_preview = getConfigImageSize('tag_preview');
			
			if (getConfigImageAutoresize('tags')) {
				$iWidthDest = $size['width'];
				$iHeightDest = $size['height'];
			} else {
				$iWidthDest = null;
				$iHeightDest = null;
			}
			
			$iWidthDest_preview = $size_preview['width'];
			$iHeightDest_preview = $size_preview['height'];
			
			$iQuality = getConfigImageQuality('tag');
			$iQuality_preview = getConfigImageQuality('tag_preview');
			
			uploadAndResize($_FILES['image'], getImagePath('tags'), $id, $iWidthDest, $iHeightDest, 1, $iQuality);
			uploadAndResize($_FILES['image'], getImagePath('tags_preview'), $id, $iWidthDest_preview, $iHeightDest_preview, 1, $iQuality_preview);
			
			if ($_POST['show_img'] == 1 && file_exists(getImagePath('tags_preview').$id.'.jpg')) 
				$show_img = 1;
			else
				$show_img = 0;
				
			$query = "UPDATE `tags` 
						SET
							`img` = {$show_img}
						WHERE 
							`id` = '{$_GET['tag_id']}'";
			mysql_query($query);
			
			$rand = '?'.rand(1,9999);
		}
	}
}


//print_r($data);
//echo '</pre>';



?>
<script type="text/javascript" src="/edit/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/edit/ckfinder/ckfinder.js"></script>
<script>
function delExtraImage(id) {
	$.ajax({
		url : 'ajax.php?method=deleteextraimg',
		dataType : 'json',
		
		type : 'POST',
		data : {"id" : id},
		beforeSend : function () {},
		success : function (data) {
		
			alert('success!');
		}
	});
}


</script>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="ol">
			<div><a href="/edit/m_catalog/list.php?id=<?=$catId;?>">Назад к категории</a></div>
			<div><h2>Редактирование подкатегории:</h2><div>
			<form action="" method="post" enctype="multipart/form-data">
			<div><h3>Название:</h3></div>
			<div><input name="title" type="text" class="p_name" value="<?php echo schars($data['title']); ?>" /></div>
			<div><h3>Короткое название:</h3></div>
			<div><input name="short" type="text" class="p_name" value="<?php echo schars($data['short']); ?>" /></div>
			<div><h3>Дополнительное название (необязательно, выводится в левом меню):</h3></div>
			<div><input name="menu_title" type="text" class="p_name" value="<?php echo schars($data['menu_title']); ?>" /></div>
			<div><h3>ЧПУ:</h3></div>
			<div><input name="chpu" type="text" class="p_name" value="<?php echo schars($data['chpu']); ?>" /></div>
			<div><h3>Категория:</h3></div>
			<div>
				<select name="cat" class="p_cat">
				<?php
				foreach (getCats() as $cat) {
					echo "<option value=\"{$cat['id']}\"".($cat['id'] == $data['cat_id'] ? ' selected="selected"':'').">{$cat['title']}</option>";
				}
				?>
				</select>
			</div>
			<div><h3>SEO заголовок:</h3></div>
			<div><input name="seo_title" type="text" class="p_name span9" value="<?php echo schars($data['seo_title']); ?>" /></div>
			<div><h3>SEO описание:</h3></div>
			<div><input name="seo_desc" type="text" class="p_name span9" value="<?php echo schars($data['seo_description']); ?>" /></div>	

			<div><h3>SEO keywords:</h3></div>
			<div><input name="seo_key" type="text" class="p_name span9" value="<?php echo schars($data['seo_key']); ?>" /></div>	
			
			<div><h3>Описание:</h3></div>
			<div>
				<textarea name="seo_text" cols="70" rows="7" style="width: 400px;"><?=schars($data['seo_text'])?></textarea> 
				<script type="text/javascript">
					var editor = CKEDITOR.replace( 'seo_text' );
					CKFinder.setupCKEditor( editor, '/edit/ckfinder/' );
				</script>
			</div>
			<div>
				<div><h3>Изображение:</h3></div>
				<div>
					<img src="<?=getImageWebPath('tags').$data['id']?>.jpg<?=$rand?>" alt="" />
				</div>
				<div>
					<input type="file" name="image" />
					<input type="checkbox" name="show_img" value="1" <? if ($data['img'] == 1) echo "checked"?>>Отображать картинку тега в главном меню<br>
				</div>
				<div class="info-text">(Авторесайз: <?=(getConfigImageAutoresize('tags') ? 'Вкл' : 'Выкл')?>. Размер: <?=Config::get('image_size.tag');?>)</div>
			</div>
			

			<div>
				<input class="btn" name="edit" type="submit" value="Сохранить" />
			</div>
			<form>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>