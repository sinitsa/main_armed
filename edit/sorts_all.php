<?php
// метка таймера
$start = microtime(true);

include ('connect.php');


function getItems($id) {
	$query = '	SELECT A.id, C.cat_id, A.title, C.spec_rang, A.linked_with_10med, B.amount
				FROM catalog AS A
				LEFT JOIN storage_rests AS B ON A.id = B.catalog_id
				LEFT JOIN catalog_2_cat AS C ON A.id = C.catalog_id
				WHERE C.cat_id ='.$id.' 
				ORDER BY spec_rang';
	$result = mysql_query($query);
	$output = array();
	while ($row = mysql_fetch_assoc($result)) {
		$output[] = array(
			'id' => $row['id'],
			'cat_id' => $row['cat_id'],
			'title' => $row['title'],
			'spec_rang' => $row['spec_rang'],
			'10med' => $row['linked_with_10med'] > 0 ? 1 : 0,
			'amount' => $row['amount'] > 0 ? $row['amount'] : 0,
		);
	}
	return $output;
}

// пользовательская функция сортировки
function cmp($a, $b) {
	// Первый приоритет сортировки: Связь с 10 мед
	if ($a['10med'] == $b['10med'] ) {
		// второй приоритет сортировки: наличие
		if ($a['amount'] == $b['amount']) {
			return 0;
		} else {
			return $a['amount'] < $b['amount'] ? 1 : -1;
		}
	} else {
		return $a['10med'] < $b['10med'] ? 1 : -1;
	}
}

// Функция сортировки товаров в категориях
function allSortsByRests($id = NULL) {
	if ($id) {
		$query = 'SELECT id, title FROM cat WHERE id ='. $id;
	} else {
		$query = 'SELECT id, title FROM cat';
	}
	
	$result = mysql_query($query);
	while ($row = mysql_fetch_assoc($result)) {
		$items = getItems($row['id']);

		usort($items, 'cmp');

		// назначение новых рагнов по счетчику $i
		$i = 0;
		foreach ($items as $value) {
			$query = 'UPDATE catalog_2_cat SET spec_rang =' . $i . ' WHERE catalog_id='.$value['id'] . ' AND cat_id='. $value['cat_id'];
			//echo $query . '<br/>';
			$i++;	
			mysql_query($query);
		}
	}
}

echo allSortsByRests();

echo 'Сортировка завершена! Время выполнения скрипта: '.number_format((microtime(true) - $start), 3). ' сек';

?>