<?php include('auth.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Панель администрирования</title>
<script src="https://yandex.st/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="/edit/js/common.js" type="text/javascript"></script>
<script src="/edit/js/jquery.highlight.js" type="text/javascript"></script>
<script src="/edit/js/jquery.scrollTo-1.4.3.1.js" type="text/javascript"></script>

<script type="text/javascript" src="/edit/bootstrap/js/bootstrap.js"></script>
<!--<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-alert.js"></script>
<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="http://twitter.github.com/bootstrap/assets/js/bootstrap-tab.js"></script>-->


<link href="/edit/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="/edit/bootstrap/css/bootstrap.responsive.css" rel="stylesheet">
<?php if (isset($cssOl) && $cssOl) { ?>
	<link href="/edit/css/css_ol.css" rel="stylesheet" />
	<link href="/css/filters.css" rel="stylesheet" type="text/css">
<?php } ?>


<style type="text/css">
	<!
	--

	.txt_zakaz {
		color: #000;
		font-size: 12px;
		width: 460px;
	}

	.txt_zakaz_grey {
		color: #c4c0b1;
		font-size: 12px;
	}

	.txt_zakaz_small {
		color: #000;
		font-size: 10px;
	}

	.txt_small {
		color: #000;
		font-size: 12px;
	}

	.txt_zag_zakaz {
		color: #000;
		font-size: 16px;
	}
	.small_menu {
		font: 14px Arial;
	}
	.big_menu {
		font: 24px Arial;
	}
	.big_menu_grey {
		font: 24px Arial;
		color: #c4c0b1;
	}
	.txt_spisok {
		color: #000;
		text-decoration: underline;
		font-size: 14px;
		font-style: normal;
	}
	.myDragClass {
		background-color: #C7DCE0;
	}
	.partners-block {
		display: inline-block;
		margin: 15px;
		padding: 0px;
	}
	.partners-block #partner-link, .partners-block select {
		margin: 5px 0px;
	}
	.partners-block select {
		width: 225px;
	}

	.partners-block .alert {
		max-width: 375px;
	}
	-- > .highlight {
		background: yellow;
	}

</style>
<script>
	$(document).ready(function() {
		// Глобальная сортировка товаров
		$('.sortall_button').on('click', function(event) {
			event.preventDefault();
			var result = confirm('Сортировка затронет товары во всех категориях. Продолжить?');
			if (result) {
				$.ajax({
					url : '/edit/sorts_all.php',
					type : 'GET',
					success : function(data) {
						alert(data);
					}
				});
			};
		});
	});	
</script>
</head>

<body  >
<table width="100%"  cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><span class="style2"> </span>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30%"><a href="<?php echo Config::get('site.web_addr'); ?>"><img border="0" align="absbottom" src="/edit/logo.png" style = "padding-top:20px;padding-left:30px"></a></td>
          <td width="70%" valign="top">

<br>

		
      <div class="btn-toolbar" style="margin-top: 18px;">
      <?php if ( $_SESSION['auth_type'] != 'manager') { ?>
		<div class="btn-group">      
			<a class="btn btn-info btn-large" href="/edit/"><i class="icon-home"></i>На главную</a>
		</div>
		<div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-file"></i> Страницы <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?
			$pages = getStaticPages();
			foreach ($pages as $page) { ?>
				<li><a href="/edit/m_pages/edit.php?id=<?=$page['id'] ?>"><?=$page['title'] ?></a></li>
			<? } ?>
            <li class="divider"></li>
            <li><a href="/edit/m_pages/add.php"><i class="icon-plus"></i> Добавить страницу</a></li>
          </ul>
        </div>
		<div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-inbox"></i> Статьи <span class="caret"></span></a>
          <ul class="dropdown-menu">
			<li><a href="/edit/m_articles/index.php"><i class="icon-pencil"></i>Все статьи</a></li>
			<li><a href="/edit/m_articles/add.php"><i class="icon-plus"></i> Добавить новую</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-inbox"></i>Новости<span class="caret"></span></a>
          <ul class="dropdown-menu">
			<li><a href="/edit/m_news/index.php"><i class="icon-pencil"></i>Все новости</a></li>
			<li><a href="/edit/m_news/add.php"><i class="icon-plus"></i> Добавить новую</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <a class="btn btn-info btn-large" href="/edit/m_mcat/"><i class="icon-list-alt"></i> Каталог</a>
        </div><!-- /btn-group -->
        

        
        <div class="btn-group">
          <a class="btn btn-large dropdown-toggle" data-toggle="dropdown" href="#"> Разное <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/edit/m_menuleft/"><i class="icon-list-alt"></i> Редактировать меню</a></li>
            <li><a href="/edit/m_catalog_feedback/"><i class="icon-comment"></i> Отзывы о товарах</a></li>
			<li><a href="/edit/m_feedback/feedback.php"><i class="icon-comment"></i> Отзывы о магазине</a></li>
            <li class="divider"></li>
			<li>
				<a href="/edit/m_market/">
					<i class="icon-user"></i> Построитель YML
				</a>
			</li>
            <li class="divider"></li>
			<li class="divider"></li>
			<li><a class="sortall_button" href="javascript: void(0);"><i class="icon-refresh"></i> Сортировка товаров (глоб.)</a></li>
            <li><a href="/edit/m_configuration/"><i class="icon-cog"></i> Настройки</a></li>
			<li class="divider"></li>
          </ul>

        </div><!-- /btn-group -->
        <?php }else{ ?>
        	<div class="btn-group">
				<a class="btn btn-info btn-large" href="/edit/"><i class="icon-home"></i>На главную</a>
			</div>
        	<div class="btn-group">      
				<a class="btn btn-info btn-large" href="/edit/m_emp/">Платежи</a>
			</div>
        <?php } ?>
          <div class="btn-group">      
            <a class="btn btn-danger btn-large" href="/edit/auth.php?action=logout"><i class="icon-off"></i> Выйти</a>
        </div>
      </div>
	  	
	</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>