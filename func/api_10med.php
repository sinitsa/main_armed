<?
function api_10med($method, $params) {
	global $config;
	//В запросе к API 10med нам нужно обязательно передать api ключ 
	$data['api_key'] = $config['10med_apikey'];
	//и идентификатор магазина, который совершает запрос к апи
	$data['shop_id'] = $config['10med_shopid'];
	//а ну еще название метода передаем
	$data['method'] = $method;
	//а тут у нас параметры метода и все такое. ну ты понел
	$data['data'] = $params;
	
	//подготавливаем запрос для передачи
	$data = http_build_query($data);
	
	$context = stream_context_create(array(
         'http' => array(
             'method' => 'POST',
             'content' => $data,
         ),
    ));
	//отправляем запрос
	$response = file_get_contents($config['10med_apiurl'], false, $context);
	file_put_contents('response.txt', var_export($response, true));
	//var_dump($response);
	//die();
	//разбираем ответ, перегоняем его в массив и возвращаем
	return unserialize($response);
}

class api10med{
	private static $siteId = 11;
	private static $url = 'http://46.101.204.133/api/';
	private static $tokenParam = '?access-token=5RJrtyWrOf7KlNc4Hzzj65NQ7Ud0AOKS';

	public static function search($title) {
		$action = 'search';
		$params = [
			'title' => $title,
			'siteId' => self::$siteId,
		];

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}

	public static function link($localId, $remoteId) {
		$action = 'link';
		$params = [
			'siteId' => self::$siteId,
			'id10med' => $remoteId,
			'remoteProductId' => $localId,
		];

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}

	public static function product($remoteId) {
		$action = 'product';
		$params = [
			'siteId' => self::$siteId,
			'id10med' => $remoteId,
		];

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}
}