<?php
//                        _   _    _      _                 
//                       | | | |  | |    | |                
//   __ _ _   _  __ _ ___| |_| |__| | ___| |_ __   ___ _ __ 
//  / _` | | | |/ _` / __| __|  __  |/ _ \ | '_ \ / _ \ '__|
// | (_| | |_| | (_| \__ \ |_| |  | |  __/ | |_) |  __/ |   
//  \__,_|\__,_|\__, |___/\__|_|  |_|\___|_| .__/ \___|_|   
//               __/ |                     | |              
//              |___/                      |_|              
//
//	РљР»Р°СЃСЃ-РѕР±С‘СЂС‚РєР°, РІР·Р°РёРјРѕРґРµР№СЃС‚РІСѓСЋС‰РёР№ СЃ РґРІРёР¶РєРѕРј ufoweb. РўР°Рє Р¶Рµ СЃРѕРґРµСЂР¶РёС‚ РїРѕР»РµР·РЅС‹Рµ
//	СЃС‚Р°С‚РёС‡РµСЃРєРёРµ С„СѓРЅРєС†РёРё Рё РєРѕРЅСЃС‚Р°РЅС‚С‹.
//	

require_once('augstImage.php');
require_once('augstSitemap.php');

class augstHelper {

	const CAPITAL_MODE_ALL   = 999; // РЈР±РёСЂР°С‚СЊ РєР°РїСЃ, РѕСЃС‚Р°РІР»СЏСЏ Р·Р°РіР»Р°РІРЅС‹Рµ Р±СѓРєРІС‹ Сѓ РІСЃРµС… СЃР»РѕРІ
	const CAPITAL_MODE_FIRST = 998; // РЈР±РёСЂР°С‚СЊ РєР°РїСЃ, РѕСЃС‚Р°РІР»СЏСЏ Р·Р°РіР»Р°РІРЅС‹Рµ Р±СѓРєРІС‹ С‚РѕР»СЊРєРѕ Сѓ РїРµСЂРІРѕРіРѕ СЃР»РѕРІР°
	const CAPITAL_MODE_NONE  = 997; // РЈР±РёСЂР°С‚СЊ РєР°РїСЃ, РЅРµ РѕСЃС‚Р°РІР»СЏСЏ Р·Р°РіР»Р°РІРЅС‹С… Р±СѓРєРІ
	
	/**
	 * РЈР±РёСЂР°РµС‚ РєР°РїСЃ РІ СЃС‚СЂРѕРєРµ
	 * @param  string $string     РЎР»РѕРІРѕ, РІ РєРѕС‚РѕСЂРѕРј РЅРµРѕР±С…РѕРґРёРјРѕ СѓР±СЂР°С‚СЊ РљРђРџРЎ
	 * @param  bool $firstCapital РћСЃС‚Р°РІР»СЏС‚СЊ Р»Рё РїРµСЂРІСѓСЋ Р±СѓРєРІСѓ Р·Р°РіР»Р°РІРЅРѕР№
	 * @return string             РЎР»РѕРІРѕ СЃ СѓР±СЂР°РЅРЅС‹Рј РљРђРџРЎ`РѕРј
	 */
	public static function removeCapsWord($string, $firstCapital = false) {
		if (mb_strtoupper($string) == $string) {
			if(!$firstCapital || mb_strlen($string, "WINDOWS-1251") == 1)
				return mb_convert_case($string, MB_CASE_LOWER);
			else
				return mb_convert_case($string, MB_CASE_TITLE, "WINDOWS-1251");
		} elseif (preg_match('/[A-ZРђ-РЇ]{2,}/', $string, $matches))
			return mb_convert_case($string, MB_CASE_LOWER);
		return $string;
	}

	/**
	 * Р¤СѓРЅРєС†РёСЏ СѓР±РёСЂР°РµС‚ РљРђРџРЎ РІ СЃС‚СЂРѕРєРµ	
	 * @param  string $string      РЎС‚СЂРѕРєР°, РІ РєРѕС‚РѕСЂРѕР№ РЅРµРѕР±С…РѕРґРёРјРѕ СѓР±СЂР°С‚СЊ РљРђРџРЎ
	 * @param  int    $capitalMode РђР»РіРѕСЂРёС‚Рј СѓР±РёСЂР°РЅРёСЏ РљРђРџРЎ
	 * @return string              РЎС‚СЂРѕРєР° СЃ СѓР±СЂР°РЅРЅС‹Рј РљРђРџРЎ`РѕРј
	 */
	public static function removeCapsString($string, $capitalMode) {
		$isFirstWord = true;
		$words = explode(" ", $string);
		$result = array();
		if (count($words) > 0) foreach ($words as $word) {
			if (($isFirstWord && $capitalMode == self::CAPITAL_MODE_FIRST) || ($capitalMode == self::CAPITAL_MODE_ALL))
				$result[] = self::removeCapsWord($word, true);
			else $result [] = self::removeCapsWord($word);
			$isFirstWord = false;
		} else {
			return null;
		}
		return implode(" ", $result);
	}

	/**
	 * РћР±СЂРµР·Р°РµС‚ СЃС‚СЂРѕРєСѓ, СѓР±РёСЂР°РµС‚ РєСЂСѓРіР»С‹Рµ СЃРєРѕР±РєРё
	 * @param  string  $string РќР°Р·РІР°РЅРёРµ (СЃС‚СЂРѕРєР°)
	 * @param  integer $limit  РџСЂРµРґРµР» РґР»РёРЅС‹ СЂРµР·СѓР»СЊС‚Р°С‚Р°
	 * @return string          РћР±СЂРµР·Р°РЅРЅРѕРµ РЅР°Р·РІР°РЅРёРµ
	 */
	static function cropTitle($string, $limit = 63) {
		//	РЈР±РёСЂР°РµРј СЃРєРѕР±РєРё
		$result = preg_replace("/\(|\)/", " ", $string);

		//	РЈР±РёСЂР°РµРј РґРІРѕР№РЅС‹Рµ РїСЂРѕР±РµР»С‹ Рё РїСЂРѕР±РµР»+,
		$result = preg_replace("/\s(\s|\,)/", "$1", $result);

		if(mb_strlen($result) <= $limit) return $result;

		$result = mb_substr($result, 0, $limit);

		//	Р’С‹Р±РёСЂР°РµРј СЃРїРѕСЃРѕР± РѕР±СЂРµР·РєРё РїРѕ РґР»РёРЅРµ
		$lastSpacePos = mb_strripos($result, " ", 0);
		$lastDotPos = mb_strripos($result, ".", 0);
		$longestTitle = ($lastDotPos > $lastSpacePos) ? $lastDotPos : $lastSpacePos;

		return mb_substr($result, 0, $longestTitle);
	}

	/**
	 * Р’С‹С‡Р»РµРЅСЏРµС‚ РёР· РїРѕР»РЅРѕРіРѕ РїСѓС‚Рё Рє С„Р°Р№Р»Сѓ РµРіРѕ РёРјСЏ, РѕРїС†РёРѕРЅР°Р»СЊРЅРѕ СЃ СЂР°СЃС€РёСЂРµРЅРёРµРј.
	 * @param  string  $path РџСѓС‚СЊ Рє С„Р°Р№Р»Сѓ.
	 * @param  boolean $ext  Р¤Р»Р°Рі, РµСЃР»Рё true, С‚Рѕ Р±СѓРґРµС‚ РІРѕР·РІСЂР°С‰РµРЅРѕ РёРјСЏ СЃ СЂР°СЃС€РёСЂРµРЅРёРµРј.
	 * @return string        РРјСЏ С„Р°Р№Р»Р°.
	 */
	public static function path2Filename($path = '/example/file.txt', $ext = false) {
		$parts = explode(DIRECTORY_SEPARATOR, $path);
		$fullName = end($parts);
		if ($ext) return $fullName;

		$parts = explode('.', $fullName);
		return array_shift($parts);
	}

	/**
	 * Р—Р°РіСЂСѓР¶Р°РµС‚ РёР· Р‘Р” РёРЅС„РѕСЂРјР°С†РёСЏ Рѕ С„РѕСЂРјР°С‚Рµ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ,
	 * РІРѕР·РІСЂР°С‰Р°СЏ РµС‘ РІ РІРёРґРµ РјР°СЃСЃРёРІР°.
	 * @param  string $formatName РќР°Р·РІР°РЅРёРµ С„РѕСЂРјР°С‚Р°.
	 * @return array              РњР°СЃСЃРёРІ СЃ РёРЅС„РѕСЂРјР°С†РёРµР№ Рѕ С„РѕСЂРјР°С‚Рµ. РЎРѕРґРµСЂР¶РёС‚ РєР»СЋС‡Рё
	 *                                      path, width, height, quality
	 */
	public static function getFormatInfo($formatName) {
		$size = getConfigImageSize($formatName);
		return array(
				'path'    => getImagePath($formatName),
				'width'   => $size['width'],
				'height'  => $size['height'],
				'quality' => getConfigImageQuality($formatName),
			);
	}

	/**
	 * Р—Р°РіСЂСѓР¶Р°РµС‚ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ, РЅР°РєР»Р°РґС‹РІР°РµС‚ РІРѕС‚РµСЂРјР°СЂРєСѓ Рё
	 * СЂРµСЃР°Р№Р·РёС‚ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ РІ РІС‹Р±СЂР°РЅРЅС‹С… С„РѕСЂРјР°С‚Р°С….
	 * @param  string $filesKey РљР»СЋС‡ РјР°СЃСЃРёРІР° $_FILES[].
	 * @param  array  $ids      РњР°СЃСЃРёРІ СЃ ID РєР°СЂС‚РёРЅРѕРє.
	 */
	static function saveImages($filesKey, $id) {
		if ($filesKey == 'main_image') {
			$path = getImagePath('product_original');
			$formats = array(
					'product_preview' => self::getFormatInfo('product_preview'),
					'product_small' => self::getFormatInfo('product_small'),
					'product_medium' => self::getFormatInfo('product_medium'),
				);
			$ids = array($id);
		} elseif ($filesKey == 'extra_photos') {
			$path = getImagePath('product_extra_original');
			$formats = array(
					'product_extra_preview' => self::getFormatInfo('product_extra_preview'),
					'product_extra_medium' => self::getFormatInfo('product_extra_medium'),
				);
			$ids = array();
			for($i = 0; $i < count($_FILES[$filesKey]['tmp_name']); $i++) {
				mysql_query("INSERT INTO `foto` SET `catalog_id` = {$id}");
				$ids[] = mysql_insert_id();
			}
		}
		$sourcePath = $path . 'src/';
		$uploaded = augstImage::upload($filesKey, $sourcePath, $ids);
		if (!$uploaded) return false;

		$watermarkPath = $_SERVER['DOCUMENT_ROOT'] . Config::get('path.images_watermark');
		$watermarkPosition = Config::get('images_watermark.position');
		$watermarkOffset = Config::get('images_watermark.offset');
		foreach ($uploaded as $i => $imagePath) {
			$image = new augstImage($imagePath);
			$watermarkedImage = new augstImage($image->watermark($path . $ids[$i] . $image->ext, $watermarkPath, $watermarkPosition, $watermarkOffset));
			unset($image);
			foreach ($formats as $format)
				$watermarkedImage->resize($format, $ids[$i]);
			unset($watermarkedImage);
		}

		return $uploaded;
	}

	/**
	 * РЎРѕР·РґР°РµС‚ РєР°СЂС‚Сѓ СЃР°Р№С‚Р° Рё СЃРѕС…СЂР°РЅСЏРµС‚ РїРѕ РїСѓС‚Рё, СѓРєР°Р·Р°РЅРЅРѕРјСѓ РІ РЅР°СЃС‚СЂРѕР№РєР°С….
	 * @param  string   $pathRoot РљРѕСЂРЅРµРІР°СЏ РґРёСЂРµРєС‚РѕСЂРёСЏ СЃР°Р№С‚Р°.
	 * @param  array    $rules    РџСЂР°РІРёР»Р° СЃРѕР·РґР°РЅРёСЏ РєР°СЂС‚С‹.
	 * @return int|bool           РљРѕР»РёС‡РµСЃС‚РІРѕ Р±Р°Р№С‚ РІ РєР°СЂС‚Рµ РёР»Рё false.
	 */
	static public function sitemap($pathRoot = '', $rules = array(
			'pages' => array(
					'table' => 'pages',
					'condition' => ' WHERE `sitemap_include` = 1',
					'path' => 'page/',
					'priority' => '0.1',
				),
			'categories' => array(
					'table' => 'cat',
					'path' => 'cat/',
					'priority' => '0.8',
				),
			'tags' => array(
					'table' => 'tags',
					'path' => 'tag/',
					'priority' => '0.8',
				),
			'products' => array(
					'table' => 'catalog',
					'condition' => ' WHERE `is_hide` = 0',
					'path' => 'catalog/',
					'priority' => '0.2',
				),
			'suffix' => '.html',
		)) {
		//	Р—Р°РіСЂСѓР¶Р°РµРј РєРѕРЅС„РёРіСѓСЂР°С†РёСЋ
		$sitemapFilePath = Config::get('path.sitemap');
		$httpHost = Config::get('site.web_addr');	

		//	РЎРѕР·РґР°РµРј РїСѓСЃС‚СѓСЋ РєР°СЂС‚Сѓ
		$sitemap = new augstSitemap();

		//	Р—Р°РїРѕР»РЅСЏРµРј РєР°СЂС‚Сѓ РїРѕ РїСЂР°РІРёР»Р°Рј
		foreach ($rules as $key => $ruleSet) {
			if ($key == 'suffix') continue;
			$query = "SELECT `chpu` FROM `{$ruleSet['table']}`";
			if (isset($ruleSet['condition'])) $query .= $ruleSet['condition'];
			$result = mysql_query($query);
			while ($item = mysql_fetch_assoc($result)) {
				$location = $httpHost . $ruleSet['path'] . $item['chpu'] . $rules['suffix'];
				$sitemap->insertUrl($location,'today','daily',$ruleSet['priority']);
			}
		}

		//	РЎРѕС…СЂР°РЅРµРЅРёРµ РєР°СЂС‚С‹
		return $sitemap->save($pathRoot . $sitemapFilePath);
	}

}
?>