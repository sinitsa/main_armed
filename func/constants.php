<?
if (!defined('ROOT_DIR')) {
	define('ROOT_DIR', dirname(__FILE__).'/../');
}

define('SHORT_IF_EXISTS_ELSE_NONE', 1);
define('SHORT_IF_EXISTS_ELSE_CROP', 2);
define('CROP_FROM_FULL', 3);

//Заказы
define('STATUS_NEW', 0);
define('STATUS_PROCESSED', 1);
define('STATUS_REFUSAL', 4);

//Типы параметров
define('PARAM_VALUE', 'value');
define('PARAM_SET', 'set');
define('PARAM_RANGE', 'range');