<?php
include('constants.php');
define('_DS', DIRECTORY_SEPARATOR);
date_default_timezone_set('Europe/Moscow');

include(ROOT_DIR . 'config.php');
include('api_10med.php');

//Подгружаем хук. если есть
if (file_exists(ROOT_DIR . '/hooks/core.php')) {
	include(ROOT_DIR . '/hooks/core.php');
}

//Подгружаем классы для работы с расчетом доставки

include('classes/cart.php');
include('classes/db.php');
include('geolocation.php');
include('classes/deliveryPrice.php');
include('classes/discountPrice.php');

// ---- /config system/ ------
// key example: catalog.products_on_page
class Config {
	//Переменная для кеширования значений
	public static $data = array();
	
	protected static function parseKey($key) {
		if (empty($key)) return false;
		$temp = explode('.', $key);
		$count = count($temp);
		if ($count == 2) {
			$keyGroup = $temp[0];
			$key = $temp[1];
		} elseif ($count == 1) {
			$keyGroup = '';
			$key = $temp;
		} else {
			return false;
		}
		return array('keyGroup' => $keyGroup, 'key' => $key);
	}
	public static function get($key) {
		//Оставляем обратную совместимость
		if ($key == 'site.server_dir')
			return ROOT_DIR;
		//Достаем из "кеша"
		if (isset(self::$data[$key])) {
			return self::$data[$key];
		}
		$k = self::parseKey($key);
		if ($k == false) return false;
	
		$temp = mysql_fetch_assoc(mysql_query("SELECT `value` FROM `config` WHERE `key_group` = '{$k['keyGroup']}' AND `key` = '{$k['key']}'"));
		//Заносим в "кеш"
		self::$data[$key] = $temp['value'];
		return $temp['value'];
	}
	public static function set($keyFull, $value) {
		$key = self::parseKey($keyFull);
		if ($key == false) return false;
		
		$value = mysql_real_escape_string($value);
		
		mysql_query("UPDATE `config` SET `value` = '{$value}' WHERE `key_group` = '{$key['keyGroup']}' AND `key` = '{$key['key']}' LIMIT 1");
		
		if (mysql_affected_rows() <= 0) {
			mysql_query("INSERT INTO `config` SET
				`value` = '{$value}', 
				`key_group` = '{$key['keyGroup']}',
				`key` = '{$key['key']}'
				");
		}
		self::$data[$keyFull] = $value;
		return true;
	}
	/* deprecated
	public static function add($key, $value, $name = '', $desc = '') {
		$key = mysql_real_escape_string($key);
		$value = mysql_real_escape_string($value);
		$name =  mysql_real_escape_string($name);
		$desc =  mysql_real_escape_string($desc);
		
		if(mysql_fetch_assoc(mysql_query("SELECT * FROM `config_main` WHERE `key` = '{$key}'"))) {
			return false;
		} else {
			mysql_query("INSERT INTO `config_main` SET `value` = '{$value}', `key` = '{$key}', `name` = '{$name}', `description` = '{$desc}'");
			return true;
		}
	}
	*/
	public static function getAll() {
		$array = array();
		
		$sel = mysql_query("SELECT * FROM `config` ORDER BY `key_group`");
		while ($res = mysql_fetch_assoc($sel)) {
			$array[] = $res;
		}
		return $array;
	}
}


//Авторизовался ли пользователь в edit
function isEditAuth() {
	return isset($_SESSION['auth']) && $_SESSION['auth'] == 'edit';
}
function getLinkEditProduct($id) {
	if (is_array($id)) $id = $id['id'];
	return '/edit/m_catalog/edit.php?id=' . $id;
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
// Узнаем электронную почту
function query_email() {
	global $site_email;

	$query = ("SELECT * FROM config WHERE id= '1' LIMIT 1");
	$result = @mysql_query($query);
	while ($line = @mysql_fetch_assoc($result)) {
		$site_email = $line['email'];
	};
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
// -------------------------------------------------------------------------------- /config/ ------------------------------------------------------------------------
// Узнаем конфигурационные переменные сайта
function query_config() {
	$query = ("SELECT * FROM config WHERE id= '1' LIMIT 1");
	$result = @mysql_query($query);
	while ($line = @mysql_fetch_assoc($result)) {

		global $config_name;
		global $config_path;
		global $config_email;
		global $config_tel;
		global $config_rezhim;
		global $config_banner;
		global $config_des;
		global $config_key;
		global $seo_title_text;
		global $seo_text;
		global $config_url;
		global $config_ga;


		$config_name = $line['name'];
		$config_path = $line['path'];
		$config_email = $line['email'];
		$config_tel = $line['tel'];
		$config_rezhim = $line['rezhim'];
		$config_banner = $line['banner'];
		$config_des = $line['seo_des'];
		$config_key = $line['seo_key'];
		$seo_title_text = $line['seo_title_text'];
		$seo_text = $line['seo_text'];
		$config_url = $line['url'];
		$config_ga = $line['ga'];
	};
}
// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
//  ----------------  ОПЕРАЦИИ С ID
// Получаем гетом id
function get_id() {
	if (isset($_REQUEST['id']) && preg_match("|^\d+$|", $_REQUEST['id']))
		$id = $_REQUEST['id'];
	return $id;
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
//  ------------------------------------------------------------------------------------ /foto/ --------------------------------------------------------

function query_foto($id) {
	$query = ("SELECT * FROM foto WHERE id=$id LIMIT 1");
	$result = @mysql_query($query);
	while ($line = @mysql_fetch_assoc($result)) {
		global $cat;
		global $title;

		$cat = $line['cat'];
		$title = $line['title'];
	};
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
//  ------------------------------------------------------------------------------------ /pages/ ---------------------------------------------------------------------------------
//Возвращает данные о статической странице
// $forceChpu принудительный поиск страницы именно по ЧПУ
function getStaticPage($idOrChpu, $forceChpu = false) {
	if (is_numeric($idOrChpu) && $forceChpu == false) {
		$where = "`id` = '{$idOrChpu}'";
	} else {
		$idOrChpu = mysql_real_escape_string($idOrChpu);
		$where = "`chpu` = '{$idOrChpu}'";
	}
	$sel = mysql_query("SELECT * FROM `pages` WHERE {$where}");
	return mysql_fetch_assoc($sel);
}
//Возвращает список статических страниц
function getStaticPages() {
	$pages = array();
	$sel = mysql_query("SELECT * FROM `pages`");
	while ($row = mysql_fetch_assoc($sel)) {
		$pages[] = $row;
	}
	return $pages;
}

//Удаляет статическую страницу
function deleteStaticPage($id) {
	if (!is_numeric($id)) return false;
	mysql_query("DELETE FROM `pages` WHERE `id`='{$id}'");
	return mysql_affected_rows() > 0;
}
// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
//  ---------------- ОПЕРАЦИИ С ПАПКАМИ
function query_path() {
	global $folder;
	$request = ("SELECT path FROM config WHERE id='1' LIMIT 1");
	$result = mysql_query($request);
	while ($row = mysql_fetch_row($result)) {
		$folder = $row[0];
	};
}

// --------------------------------------------------------------------------------  ХЕРНЯ ------------------------------------------------------------------------
// ------------------  Функция ресайза
function imageresize($src, $dest, $w, $quality) {

	$im = imagecreatefromjpeg($src);
	$w_src = imagesx($im);
	$h_src = imagesy($im);
	echo $w_src;
	echo "<br>";
	echo $h_src;
	echo "<br>";
	$ratio = $w_src / $w;
	$width = round($w_src / $ratio);
	$height = round($h_src / $ratio);

	$im1 = imagecreatetruecolor($width, $height);
	imagecopyresampled($im1, $im, 0, 0, 0, 0, $width, $height, $w_src, $h_src);

	imagejpeg($im1, $dest, $quality);

	imagedestroy($im);
	imagedestroy($im1);
}

// --------------------------------------------------------------------------------  ХЕРНЯ ------------------------------------------------------------------------
// ------------------  Функция ресайза 22222222222222222222222222222222222222222222222222222222
function imageres($src, $dest, $w, $quality) {

	$im = imagecreatefromjpeg($src);
// Узнаем ширину исходника
	$w_src = imagesx($im);
// Узнаем высоту исходника
	$h_src = imagesy($im);



	if ($w_src > $h_src) {

// Считаем коэффицент пропорции
		$ratio = $w_src / $w;
		$width = round($w_src / $ratio);
		$height = round($h_src / $ratio);
		echo $ratio;
		echo "<br>";
		echo $w_src;
		echo "<br>";
		echo $h_src;
		echo "<br>";
		echo $width;
		echo "<br>";
		echo $height;
		echo "<br>";
		echo "<br>";

		$im1 = imagecreatetruecolor($width, $height);
		imagecopyresampled($im1, $im, 0, 0, 0, 0, $width, $height, $w_src, $h_src);

		imagejpeg($im1, $dest, $quality);

		imagedestroy($im);
		imagedestroy($im1);
	} else {

// Считаем коэффицент пропорции
		/* $ratio = $h_src/$h;
		  $width = round($w_src/$ratio);
		  $height = round($h_src/$ratio);
		 */
		if ($w_src > $w) {
			$ratio = $w_src / $w;
			$width = round($w_src / $ratio);
			$height = round($h_src / $ratio);
		}

		echo $ratio;
		echo "<br>";
		echo $w_src;
		echo "<br>";
		echo $h_src;
		echo "<br>";
		echo $width;
		echo "<br>";
		echo $height;
		echo "<br>";
		echo "<br>";

		$im1 = imagecreatetruecolor($width, $height);
		imagecopyresampled($im1, $im, 0, 0, 0, 0, $width, $height, $w_src, $h_src);

		imagejpeg($im1, $dest, $quality);

		imagedestroy($im);
		imagedestroy($im1);
	}
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------

function image_res($src, $dest, $w_max, $h_max, $quality) {

	$im = imagecreatefromjpeg($src);

// Узнаем ширину исходника
	$w_src = imagesx($im);

// Узнаем высоту исходника
	$h_src = imagesy($im);

	if ($h_src > $h_max and $h_max != '0') {
		$ratio = $h_src / $h_max;
		$width = round($w_src / $ratio);
		$height = round($h_src / $ratio);

		if ($width > $w_max) {
			$ratio = $width / $w_max;
			$width = round($width / $ratio);
			$height = round($height / $ratio);
		}
	} else {
		if ($w_src > $w_max) {
			$ratio = $w_src / $w_max;
			$width = round($w_src / $ratio);
			$height = round($h_src / $ratio);
		} else {
			$width = $w_src;
			$height = $h_src;
		}
	}
// создаёт новое изображение true color.
	$im1 = imagecreatetruecolor($width, $height);

//
	imagecopyresampled($im1, $im, 0, 0, 0, 0, $width, $height, $w_src, $h_src);

	imagejpeg($im1, $dest, $quality);

	imagedestroy($im);
	imagedestroy($im1);
}
//Новая супер функция аплода и ресайза в одном флаконе

define('IMG_RESIZE_FIT', 0); // вписывание в квадрат (уменьшение по максимальной стороне)
define('IMG_RESIZE_MAX', 1); // уменьшение по максимальной стороне
define('IMG_RESIZE_MIN', 2); // уменьшение по минимальной стороне
define('IMG_RESIZE_CROP', 3); // обрезка исходной картинки: если W>H, то обрезка по бокам, если W<H, то снизу

function uploadAndResize($file,$sDirDest,$sFileDest,$iWidthDest=null, $iHeightDest=null, $iResizeMode=1, $quality = 100) {
	if (!empty($file['error'])) return false;
	if (empty($file['tmp_name']) || $file['tmp_name'] == 'none') return false;

	$sFileSrc = $file['tmp_name'];
	//$quality = 100;

	if (!($aSize=getimagesize($sFileSrc))) {
		return false;
	}
	$img_src=false;
	switch ($aSize[2]) {
		case 3:
			$img_src=imagecreatefrompng($sFileSrc);
			$sFileDest.='.png';
			break;
		case 1:
			$img_src=imagecreatefromgif($sFileSrc);
			$sFileDest.='.gif';
			break;
		case 2:
			$img_src=imagecreatefromjpeg($sFileSrc);
			$sFileDest.='.jpg';
			break;
		default:
			return false;
			break;
	}
	if (!$img_src) {
		return false;
	}

	if ($iWidthDest) {$iScaleW=$iWidthDest/$aSize[0];} else {$iScaleW=1;}
	if ($iHeightDest) {$iScaleH=$iHeightDest/$aSize[1];} else {$iScaleH=1;}

	if ($iResizeMode==IMG_RESIZE_MAX) {
			$iSizeRelation=(($iScaleW<$iScaleH)?$iScaleW:$iScaleH);
			$iWidthNew=round($aSize[0]*$iSizeRelation);
			$iHeightNew=round($aSize[1]*$iSizeRelation);

			$iSizeW=$iWidthNew;
			$iSizeH=$iHeightNew;
			$iDestX=0;
			$iDestY=0;
	} elseif ($iResizeMode==IMG_RESIZE_MIN) {
			$iSizeRelation=(($iScaleW>$iScaleH)?$iScaleW:$iScaleH);
			$iWidthNew=round($aSize[0]*$iSizeRelation);
			$iHeightNew=round($aSize[1]*$iSizeRelation);

			$iSizeW=$iWidthNew;
			$iSizeH=$iHeightNew;
			$iDestX=0;
			$iDestY=0;
	} elseif ($iResizeMode==IMG_RESIZE_CROP) {
			$iSizeRelation=(($iScaleW>$iScaleH)?$iScaleW:$iScaleH);
			$iWidthNew=round($aSize[0]*$iSizeRelation);
			$iHeightNew=round($aSize[1]*$iSizeRelation);

			$iSizeH=$iSizeW=(($iWidthNew<$iHeightNew)?$iWidthNew:$iHeightNew);
			$iDestX=round(($iSizeW-$iWidthNew)/2);
			$iDestY=0;
	} else {
			$iSizeRelation=(($iScaleW<$iScaleH)?$iScaleW:$iScaleH);
			$iWidthNew=round($aSize[0]*$iSizeRelation);
			$iHeightNew=round($aSize[1]*$iSizeRelation);

			$iSizeH=$iSizeW=(($iWidthNew>$iHeightNew)?$iWidthNew:$iHeightNew);
			$iDestX=round(($iSizeW-$iWidthNew)/2);
			$iDestY=round(($iSizeH-$iHeightNew)/2);
	}

	$sFileFullPath = rtrim($sDirDest, '/').'/'.$sFileDest;

	if (($iWidthDest and $iWidthDest!=$aSize[0]) && !($iWidthDest > $aSize[0] && $iHeightDest > $aSize[1] && $iResizeMode==IMG_RESIZE_MAX)) {
	$img_dest=imagecreatetruecolor($iSizeW,$iSizeH);
	//$clr = imagecolorallocate($img_dest,255,255,255);
	//imagefill($img_dest,0,0,$clr);
	imagesavealpha($img_dest,true);
	imagealphablending($img_dest,false);
	if (imagecopyresampled($img_dest, $img_src, $iDestX, $iDestY, 0, 0,
                           $iWidthNew, $iHeightNew, $aSize[0], $aSize[1])) {
	  imagedestroy($img_src);
      switch ($aSize[2]) {
        case 3:
          if (imagepng($img_dest,$sFileFullPath,$quality)) {
            chmod($sFileFullPath,0666);
          }
          break;
        case 1:
          if (imagegif($img_dest,$sFileFullPath,$quality)) {
            chmod($sFileFullPath,0666);
          }
          break;
        case 2:
          if (imagejpeg($img_dest,$sFileFullPath,$quality)) {
            chmod($sFileFullPath,0666);
          }
          break;
        }
      imagedestroy($img_dest);
      return $sFileDest;
    }
  } else {
    if (copy($sFileSrc,$sFileFullPath)) {
      return $sFileDest;
    }
  }
return false;

}
// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
//  ----------------  ОПЕРАЦИИ С КАТАЛОГОМ
// Контактное лицо

function query_cat_full($id) {
	$query = ("SELECT * FROM cat WHERE id=$id LIMIT 1");
	$result = @mysql_query($query);
	$chekis = mysql_num_rows($result);
	while ($line = @mysql_fetch_assoc($result)) {
		global $title;
		global $pod;
		global $seo_title;
		global $seo_des;
		global $seo_key;
		global $seo_title_text;
		global $seo_text;
		global $chekis;
		global $chpu;
		global $compensation;
		global $hidelink;

		$title = $line['title'];
		$pod = $line['pod'];
		$seo_title = $line['seo_title'];
		$seo_des = $line['seo_des'];
		$seo_key = $line['seo_key'];
		$seo_title_text = $line['seo_title_text'];
		$seo_text = $line['seo_text'];
		$chpu = $line['chpu'];
		$compensation = $line['compensation'];
		$hidelink = $line['hidelink'];
	};
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------

function query_catalog($id) {
	$query = ("SELECT * FROM catalog WHERE id=$id LIMIT 1");
	$result = @mysql_query($query);
	$chekis = mysql_num_rows($result);
	while ($line = @mysql_fetch_assoc($result)) {
		global $adres;
		global $short;
		global $des;
		global $price;
		global $tip;
		global $cat;
		global $bal;
		global $et;
		global $gaz;
		global $gvoda;
		global $kom;
		global $mkad;
		global $naz;
		global $oto;
		global $sd;
		global $shosse;
		global $sk;
		global $srok;
		global $su;
		global $suz;
		global $svet;
		global $sz;
		global $tipd;
		global $tipk;
		global $voda;
		global $lico;
		global $lat;
		global $lng;
		global $spec;
		global $img;
		global $chekis;

		$adres = $line['adres'];
		$short = $line['short'];
		$des = $line['des'];
		$price = $line['price'];
		$tip = $line['tip'];
		$cat = $line['cat'];
		;
		$bal = $line['bal'];
		$et = $line['et'];
		$gaz = $line['gaz'];
		$gvoda = $line['gvoda'];
		$kom = $line['kom'];
		$mkad = $line['mkad'];
		$naz = $line['naz'];
		$oto = $line['oto'];
		$sd = $line['sd'];
		$shosse = $line['shosse'];
		$sk = $line['sk'];
		$srok = $line['srok'];
		$su = $line['su'];
		$suz = $line['suz'];
		$svet = $line['svet'];
		$sz = $line['sz'];
		$tipd = $line['tipd'];
		$tipk = $line['tipk'];
		$voda = $line['voda'];
		$lico = $line['lico'];
		$lat = $line['lat'];
		$lng = $line['lng'];
		$spec = $line['spec'];
		$img = $line['img'];
	};
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
// Функция транслита
function translit($stroka) {

	global $trans;
	$stroka = str_replace(") .", "", $stroka);
	$stroka = rtrim($stroka);
	$stroka = htmlspecialchars($stroka, ENT_QUOTES);
	$stroka = mb_strtolower($stroka);

	$stroka = str_replace("&quot;", "", $stroka);

	$stroka = str_replace("№", "", $stroka);

	$stroka = str_replace("\"", "-", $stroka);
	$stroka = str_replace("«", "-", $stroka);
	$stroka = str_replace("»", "-", $stroka);

	$stroka = str_replace("&laquo;", "-", $stroka);
	$stroka = str_replace("&raquo;", "-", $stroka);
	$stroka = str_replace("*", "-", $stroka);
	$stroka = str_replace("&", "-", $stroka);
	$stroka = str_replace("®", "", $stroka);
	$stroka = str_replace("(", "-", $stroka);
	$stroka = str_replace(")", "", $stroka);
	$stroka = str_replace(" .", "", $stroka);
	$stroka = str_replace("а", "a", $stroka);
	$stroka = str_replace("б", "b", $stroka);
	$stroka = str_replace("в", "v", $stroka);
	$stroka = str_replace("г", "g", $stroka);
	$stroka = str_replace("д", "d", $stroka);
	$stroka = str_replace("е", "e", $stroka);
	$stroka = str_replace("ё", "e", $stroka);
	$stroka = str_replace("ж", "zh", $stroka);
	$stroka = str_replace("з", "z", $stroka);
	$stroka = str_replace("и", "i", $stroka);
	$stroka = str_replace("й", "i", $stroka);
	$stroka = str_replace("к", "k", $stroka);
	$stroka = str_replace("л", "l", $stroka);
	$stroka = str_replace("м", "m", $stroka);
	$stroka = str_replace("н", "n", $stroka);
	$stroka = str_replace("о", "o", $stroka);
	$stroka = str_replace("п", "p", $stroka);
	$stroka = str_replace("р", "r", $stroka);
	$stroka = str_replace("с", "s", $stroka);
	$stroka = str_replace("т", "t", $stroka);
	$stroka = str_replace("у", "u", $stroka);
	$stroka = str_replace("ф", "f", $stroka);
	$stroka = str_replace("х", "h", $stroka);
	$stroka = str_replace("ц", "ts", $stroka);
	$stroka = str_replace("ч", "ch", $stroka);
	$stroka = str_replace("Ч", "ch", $stroka);
	$stroka = str_replace("ш", "sh", $stroka);
	$stroka = str_replace("щ", "sh", $stroka);
	$stroka = str_replace("ь", "", $stroka);
	$stroka = str_replace("ы", "y", $stroka);
	$stroka = str_replace("ъ", "", $stroka);
	$stroka = str_replace("э", "e", $stroka);
	$stroka = str_replace("ю", "yu", $stroka);
	$stroka = str_replace("я", "ya", $stroka);
	$stroka = str_replace("Я", "ya", $stroka);
	$stroka = str_replace(" ", "-", $stroka);
	$stroka = str_replace(".", "", $stroka);
	$stroka = str_replace(",", "-", $stroka);
	$stroka = str_replace("+", "-", $stroka);
	$stroka = str_replace("/", "-", $stroka);
	$stroka = str_replace('"', "-", $stroka);
	$stroka = str_replace("*", "-", $stroka);
	$stroka = str_replace(":", "-", $stroka);
	$stroka = str_replace('–', "-", $stroka);
	$stroka = str_replace("---", "-", $stroka);
	$stroka = str_replace("--", "-", $stroka);
	$stroka = str_replace("--", "-", $stroka);


	$trans = $stroka;
//echo $stroka;
}

// Транслит (версия2)
function translit_new($s, $charset = false) {
  	if (!$charset) {
  		$charset = mb_detect_encoding($s);	
  	}
	$s = (string) $s; // преобразуем в строковое значение
	$s = strip_tags($s); // убираем HTML-теги
	$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
	$s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
	$s = trim($s); // убираем пробелы в начале и конце строки
	// $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
	$s = mb_strtolower($s, $charset);
	$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
	$s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
	$s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
	return $s; // возвращаем результат
}



// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------

function query_cat_novinki($id) {


	global $novinki_id;
	global $novinki_title;
	global $novinki_chpu;
	global $novinki_short;
	global $novinki_cat;
	global $novinki_price;
	global $num_cat;
	global $num_per;

	global $q;
	global $chekis;

	$query = (" SELECT * FROM catalog WHERE novinka='1' ");
	$result = @mysql_query($query);
	$chekis = mysql_num_rows($result);
	while ($line = @mysql_fetch_assoc($result)) {
		$novinki_id[] = $line['id'];
		$novinki_title[] = $line['title'];
		$novinki_chpu[] = $line['chpu'];
		$novinki_cat[] = $line['cat'];
		$novinki_price[] = $line['price'];


		$t_title = $line['title'];
		$num_title = strlen($t_title);
		$n_short = 150 - $num_title;

		$novinki_short[] = substr($line['short'], 0, $n_short);
	};

	$num_cat = count($novinki_id);
	$num_pol = $num_cat / 2;
	$num_pol = intval($num_pol);
	$num_per = $num_cat - $num_pol;


	$q = count($novinki_id);
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------

function query_cat_spec($id) {
	global $spec_id;
	global $spec_title;
	global $spec_chpu;
	global $spec_short;
	global $spec_cat;
	global $spec_price;
	global $spec_cat;
	global $spec_per;
	global $num_cat;
	global $num_per;
	global $spec_novinka;

	global $q;
	global $chekis;

	$query = (" SELECT * FROM catalog WHERE hit = '1'");
	$result = @mysql_query($query);
	$chekis = mysql_num_rows($result);
	while ($line = @mysql_fetch_assoc($result)) {
		$spec_id[] = $line['id'];
		$spec_title[] = $line['title'];
		$spec_chpu[] = $line['chpu'];
		$spec_cat[] = $line['cat'];
		$spec_price[] = $line['price'];
		$spec_novinka[] = $line['novinka'];
		$t_title = $line['title'];
		$num_title = strlen($t_title);
		$n_short = 250 - $num_title;

		$spec_short[] = substr($line['short'], 0, $n_short);
	};

	$num_cat = count($spec_title);
	$num_pol = $num_cat / 2;
	$num_pol = intval($num_pol);
	$num_per = $num_cat - $num_pol;

	$q = count($spec_title);
}

function query_catalog_spec() {

	global $catalog_id;
	global $catalog_title;
	global $catalog_chpu;
	global $catalog_price;
	global $catalog_short;
	global $catalog_des;
	global $catalog_novinka;
	global $catalog_prior;

	global $num_cat;
	global $num_per;
	global $q;
	global $chekis;

	global $_cat_min_price;
	global $_cat_max_price;
	global $_cat_mid_price;
	global $_cat_count;
	global $_pf_cat_id;
	global $_cur_pf_min_price;
	global $_cur_pf_max_price;
	global $_pf_cat_name;

	// ----------- Проверяем задана ли сортировка
	// Если задана
	if (isset($_REQUEST["pf_min_price"]) && (isset($_REQUEST["pf_max_price"]))) {
		// Определяем переменные от и до
		$_cur_pf_min_price = $_REQUEST["pf_min_price"]; // от
		$_cur_pf_max_price = $_REQUEST["pf_max_price"]; // до

		$add_where = " AND ((price>=$_cur_pf_min_price) AND (price<=$_cur_pf_max_price))"; // добавляем условие

		$_cat_min_price = 9999999999;
		$_cat_max_price = -1;
		$_cat_count = 0;
		$_pf_cat_id = $now_cat_id;
		$_pf_cat_name = $id;

		// Задаем запрос на выборку товаров
		$query = ("SELECT * FROM catalog WHERE (hit = '1')  ORDER BY spec_rang");
		$result = @mysql_query($query);
		$num_cat = mysql_num_rows($result);
		while ($line = @mysql_fetch_assoc($result)) {
			$price = ($line['price']) * 1;
			if ($price > $_cat_max_price)
				$_cat_max_price = $price;
			if ($price < $_cat_min_price)
				$_cat_min_price = $price;
		};

		$query = ("SELECT * FROM catalog WHERE (hit = '1') $add_where order by price");
		$result = @mysql_query($query);
		$num_cat = mysql_num_rows($result);

		while ($line = @mysql_fetch_assoc($result)) {
			$price = ($line['price']) * 1;
			if ($price > $_cat_max_price)
				$_cat_max_price = $price;
			if ($price < $_cat_min_price)
				$_cat_min_price = $price;
			$catalog_id[] = $line['id'];
			$catalog_title[] = $line['title'];
			$catalog_chpu[] = $line['chpu'];
			$catalog_price[] = $line['price'];
			$catalog_art[] = $line['art'];
			$catalog_des[] = $line['des'];
			$catalog_novinka[] = $line['novinka'];
			$catalog_prior[] = $line['prior'];
			$tmp_short = $line['short'];

			$t_title = $line['title'];
			$num_title = strlen($t_title);
			$n_short = 150 - $num_title;

			$catalog_short[] = substr($line['short'], 0, $n_short);
			$_cat_count++;
		};
	}
	else {
		$add_where = "";
		$_cat_min_price = 9999999999;
		$_cat_max_price = -1;
		$_cat_count = 0;
		$_pf_cat_id = $now_cat_id;
		$_pf_cat_name = $id;
		$query = ("SELECT * FROM catalog WHERE (hit = '1') $add_where  ORDER BY spec_rang");
		$result = @mysql_query($query);
		$num_cat = @mysql_num_rows($result);

		while ($line = @mysql_fetch_assoc($result)) {
			$price = ($line['price']) * 1;
			if ($price > $_cat_max_price)
				$_cat_max_price = $price;
			if ($price < $_cat_min_price)
				$_cat_min_price = $price;
			$catalog_id[] = $line['id'];
			$catalog_title[] = $line['title'];
			$catalog_chpu[] = $line['chpu'];
			$catalog_price[] = $line['price'];
			$catalog_novinka[] = $line['novinka'];
			$catalog_prior[] = $line['prior'];
			//$price = $line['price'];
			//$sum = strlen($price);
			//$tochka = $sum  - '3';
			//if ($sum > '3' )
			//{
			//	$first_price = substr($price, 0, $tochka);
			//	$last_price = substr ($price, $tochka, $sum);
			//	$catalog_price[] = $first_price." ".$last_price;
			//}
			//$catalog_price[] = $price;


			$catalog_des[] = $line['des'];

			$tmp_short = $line['short'];

			$t_title = $line['title'];
			$num_title = strlen($t_title);
			$n_short = 150 - $num_title;

			$catalog_short[] = substr($line['short'], 0, $n_short);
			$_cat_count++;
		};
		$_cur_pf_min_price = $_cat_min_price;
		$_cur_pf_max_price = $_cat_max_price;
	}


	$_cat_mid_price = round(($_cat_max_price - $_cat_min_price) / 2) + $_cat_min_price;
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------

function check_pod($id) {

	global $num_pod;

	$query = ("SELECT * FROM cat WHERE pod=$id");
	$result = @mysql_query($query);
	$num_pod = mysql_num_rows($result);
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------


function query_all($tab) {
	global $all;

	$all = Array();
	$query = ("SELECT * FROM $tab ");
	$result = @mysql_query($query);
	while ($line = @mysql_fetch_assoc($result)) {
		$all[count($all)] = $line;
	};
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------

function add_monitor($act, $razd, $act_id, $title, $chpu) {
	$date = date("m.d.y");
	$time = date("H:i:s");
	$id = '0';
	$query = "INSERT INTO monitor VALUES('$id','$date','$time','$act','$razd','$act_id','$title','$chpu')";
	$result = mysql_query($query);
}

// --------------------------------------------------------------------------------  НУЖНОЕ ------------------------------------------------------------------------
//Depreceted
function add_monitor_sm($act, $act_id, $title, $dostavka) {
	$date = date("m.d.y");
	$time = date("H:i:s");
	$id = '0';
	$query = "INSERT INTO monitor_sm VALUES('$id','$date','$time','$act','$act_id','$title','$dostavka')";
	$result = mysql_query($query);
}

// ------------------------------------------------------------- Фильтр ссылок
function sql_catalogFilterDes($des) {
	$res = $des;

	preg_match_all("/(?:&lt;)(?:.*)a(?:.*)href=(?:&quot;)(?:.*)(http:\/\/)(?:.*)(?:&quot;)(?:.*)(?:&gt;)(.*)(?:&lt;)\/a(?:&gt;)/i", $res, $matches);

	foreach ($matches[0] as $key => $str) {
		$res = str_replace($str, $matches[2][$key], $res);
	}

	preg_match_all("/(?:<)(?:.*)a(?:.*)href=(?:\")(?:.*)(http:\/\/)(?:.*)(?:\")(?:.*)(?:>)(.*)(?:<)\/a(?:>)/i", $res, $matches);

	foreach ($matches[0] as $key => $str) {
		$res = str_replace($str, $matches[2][$key], $res);
	}
	return $res;
}

// --------------------------------------------------------------------------------  Фильтр сторонних сайтов  ------------------------------------------------------------------------

function sql_catalogFilterImages($des) {
	$res = $des;
	//preg_match_all ("/(?:&lt;)(?:.*)img(?:.*)src=(?:&quot;)(?:.*)(http:\/\/.*)(?:&quot;)(?:.*)(?:&gt;)/i", $res, $matches);
	preg_match_all("/(?:&lt;)(?:.*)img(?:.*)((?<=src=&quot;)http:\/\/.*)(?:(?:&quot;)(?=\s&nbsp;))/i", $res, $matches);
	foreach ($matches[1] as $str) {
		$image = new GetImage;

		$image->source = $str;
		$image->save_to = '../../img_about/'; // слэш на конце обязательно
		$get = $image->download('gd', true); // используем  GD
		$new_url = '/img_about/' . $image->new_name;

		if ($get) {
			$res = str_replace($str, $new_url, $res);
		}
	}

	preg_match_all("/(?:\<)(?:.*)img(?:.*)src=(?:\")((?:.*)(?:http:\/\/)(?:[^\"]*))(?:\")(?:.*)(?:\>)/i", $res, $matches);
	foreach ($matches[1] as $str) {
		$image = new GetImage;

		$image->source = $str;
		$image->save_to = '../../img_about/'; // слэш на конце обязательно
		$get = $image->download('gd', true); // используем  GD
		$new_url = '/img_about/' . $image->new_name;

		if ($get) {
			$res = str_replace($str, $new_url, $res);
		}
	}
	return $res;
}

// Считаем символы в тексте с пробелами
function num_simvol($text) {
	$text = strip_tags($text);
	$text = trim($text);
	//$text = str_replace(".", "", $text);
	//$text = str_replace(",", "", $text);
	//$text = str_replace("-", "", $text);
	//$text = str_replace("!", "", $text);
	//$text = str_replace(":", "", $text);
	//$text = str_replace(";", "", $text);
	$text = str_replace("&nbsp;", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	//$text = str_replace('\r\n', "", $text);
	//$text = str_replace('\n', "", $text);
	//$text = str_replace("\t", "", $text);
	$text = str_replace("  ", " ", $text);
	//echo $text;
	$col_simvol = strlen($text);
	echo $col_simvol;
}
//Конвертирует массив данных в строку, для использование в mysql запросе set, update
//На входе массива
//array(
//	"column" => "value",
//	...
//);
//

//Выбрать отзывы к товару
function getProductFeedback($catalog_id, $all = false) {
	if (!is_numeric($catalog_id) || $catalog_id <= 0) return false;
	
	$products = array();
	$confirm = $all ? '' : "AND `confirm` = '1'";
	
	$sel = mysql_query("SELECT * FROM `otzyv` WHERE `catalog_id` = '{$catalog_id}' {$confirm} ORDER BY `id` ASC");
	while ($res = mysql_fetch_assoc($sel)) {
		$products[] = $res;
	}
	
	return $products;
}

function getProductRating($feedback){
	    $fbcount = count($feedback);
        $rsum = 0;
        foreach ($feedback as $sum) {
        	$rsum += $sum['rating'];
        }
        $averagerating = round($rsum/$fbcount, 0);

        return $averagerating;
}

function getProductsFeedback($type = 'new') {
	switch ($type) {
            case 'old' :
			$sel = mysql_query("
				SELECT
					`otzyv`.*,
					`catalog`.`id` as `catalog_id`,
					`catalog`.`title` as `catalog_title`
				FROM
					`otzyv`
					LEFT JOIN `catalog` ON `otzyv`.`catalog_id` = `catalog`.`id`
				WHERE
					`otzyv`.`confirm` = '1'
				ORDER BY `otzyv`.`id` ASC");
			while ($res = mysql_fetch_assoc($sel)) {
				$products[] = $res;
			}
		break;
		case 'new' :
		default :
			$sel = mysql_query("
				SELECT
					`otzyv`.*,
					`catalog`.`id` as `catalog_id`,
					`catalog`.`title` as `catalog_title`
				FROM
					`otzyv`
					LEFT JOIN `catalog` ON `otzyv`.`catalog_id` = `catalog`.`id`
				WHERE
					`otzyv`.`confirm` = '0'
				ORDER BY `otzyv`.`id` ASC");
			while ($res = mysql_fetch_assoc($sel)) {
				$products[] = $res;
			}
		break;
	}
	return $products;
}

function addOptDeal($data) {
	$data['name'] = mysql_real_escape_string(htmlspecialchars($data['name']));
	$data['email'] = mysql_real_escape_string(htmlspecialchars($data['email']));
	$data['phone'] = mysql_real_escape_string(htmlspecialchars($data['phone']));
	$data['organization'] = mysql_real_escape_string(htmlspecialchars($data['organization']));
	$data['vol'] = mysql_real_escape_string(htmlspecialchars($data['vol']));
	$data['type'] = mysql_real_escape_string(htmlspecialchars($data['type']));
	$data['comment'] = mysql_real_escape_string(htmlspecialchars($data['comment']));
	$date = time();
	
	mysql_query("
		INSERT INTO
			`opt_deals`
		SET
			`name` = '{$data['name']}',
			`email` = '{$data['email']}',
			`phone` = '{$data['phone']}',
			`organization` = '{$data['organization']}',
			`vol` = '{$data['vol']}',
			`type` = '{$data['type']}',
			`comment` = '{$data['comment']}',
			`date` = '{$date}'
		");
	return mysql_affected_rows() > 0;

}


function addProductFeedback($catalog_id, $data) {
	if (!is_numeric($catalog_id)) return false;
	$data['name'] = mysql_real_escape_string(htmlspecialchars($data['name']));
	$data['rating'] = mysql_real_escape_string(htmlspecialchars($data['rating']));
	$data['comment'] = mysql_real_escape_string(htmlspecialchars($data['comment']));
	$date = time();
	
	mysql_query("
		INSERT INTO
			`otzyv`
		SET
			`catalog_id` = '{$catalog_id}',
			`name` = '{$data['name']}',
			`rating` = {$data['rating']},
			`comment` = '{$data['comment']}',
			`date` = '{$date}'
		");
	return mysql_affected_rows() > 0;

}



//Получает код заказа
function getOrderCode($id) {
	return Config::get('order.prefix').$id;
}
function fillProductsWithInfo( &$orderProducts ) {
	//Соберем id товаров для получения информации о них
	$productsIds = array();
	foreach ( $orderProducts as $p) $productsIds[] = $p['id'];
	
	$products = getProducts(false, array('id' => $productsIds));
	$products = $products['products'];
	
	//Наполняем $order['products'] данными о товаре
	foreach ($orderProducts as &$orderP) {
		foreach ($products as $p) {
			if ($orderP['id'] == $p['id']) {
				$orderP['info'] = $p;
				break;
			}
		}
	}
}
//Возвращает url стилизованый соответственно конфигу
function getTemplateLink($linkData, $linkType, $fullUrl = false) {
	$template = Config::get('template.link_' . $linkType);
	if ($template === false) return '';
	
	$linkData['id'] = (isset($linkData['id']))?$linkData['id']:0;
	//В шаблоне ссылки заменяется id или chpu
	$rep = array(
		'[id]' => $linkData['id'],
		'[chpu]' => $linkData['chpu']
	);
	
	return ($fullUrl ? rtrim(Config::get('site.web_addr'), '/') : '').strtr($template , $rep);
}

function getSetString($data) {
	$setArray = array();
	
	foreach($data as $column => $value) {
		$setArray[] = "`{$column}` = '{$value}'";
	}
	
	return implode("," , $setArray);
}

//Супер важная функция ОТПРАВКИ E-Mail. Yeah!
// старая функция 
function sendMail($params) {
	//Деволтные настройки
	$defaultParams = array(
		'emailTo' => '',
		'emailFrom' => Config::get('site.email_from'),
		'subject' => 'Сообщение от "'.Config::get('site.name').'"',
		'body' => ''
	);
	//Расширяем дефолтные настройки
	$params = array_merge($defaultParams, $params);
	//file_put_contents('mail.txt', var_export($params, true));
	return mail($params['emailTo'], $params['subject'], $params['body'],
				implode("\r\n", array(
				"From: {$params['emailFrom']}",
				//"Reply-To: admin@med-serdce.ru",
				"Content-type: text/html;\n\t charset=utf-8",
				"X-Mailer: PHP/" .phpversion() ) )
				);
}


/**
* Функция отправки письма на почту
*
* @param string $message Тело сообщения
* @param string $mailTo Email получателя письма
* @param string $subject Заголовок письма
*/
function sendMailPHPMailer($subject,$message,$mailTo) {
	require_once( ROOT_DIR . '/edit/mail/PHPMailerAutoload.php');
	$mail = new PHPMailer();
	$mail->CharSet = 'UTF-8';
	$mail->IsSMTP();
	$mail->SMTPDebug = 0;
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = 'ssl';
	$mail->Host = 'smtp.yandex.ru';
	$mail->Port = 465;
	$mail->Username = 'info@armed-market.ru';
	$mail->Password = 'S(680%%HhJo3';
	$mail->SetFrom('info@armed-market.ru', 'Armed-Market');
	$mail->Subject = $subject;
	$mail->IsHTML(true);
	$mail->Body = $message;
	$mail->AddAddress($mailTo);
	if(!$mail->Send()) {
	  $error = 0;
	} else {
	  $error = 1;
	}
	return $error;
}

//Возвращает серверный путь до каталога с изображениями 
function getImagePath($imageType) {
	return $_SERVER['DOCUMENT_ROOT'].'/'.Config::get('path.images_upload').Config::get('path.images_'.$imageType);
}
//Возвращает веб путь до каталога с изображениями
function getImageWebPath($imageType, $fullUrl = true) {
	if ($fullUrl) {
		return Config::get('site.web_addr').Config::get('path.images_upload').Config::get('path.images_'.$imageType);
	} else {
		return '/'.Config::get('path.images_upload').Config::get('path.images_'.$imageType);
	}
}
//Возвращает размеры изображения из конфига
function getConfigImageSize($imageType) {
	$size = Config::get('image_size.'.$imageType);
	//TODO ([0-9]+)(?:\*|x|X|х|Х|:){1}([0-9]+)
	$size = explode('x', $size);
	if (count($size) == 2) {
		$size = array(
			'width' => $size[0],
			'height' => $size[1]
		);
		return $size;
	} else {
		return false;
	}
}
//Возвращает качество изображения по конфигу
function getConfigImageQuality($imageType) {
	return Config::get('image_quality.'.$imageType);
}
//Возвращает флажек авторесайза изображения
function getConfigImageAutoresize($imageType) {
	$t = Config::get('image_autoresize.'.$imageType);
	return $t == '1';
}
//Deprecated. Use getMenu()
function zh_getMenuResource() {
	$maxpodcount = 9999;
	$query_str = ("
	SELECT
		CASE WHEN menuleft_el.pod=0 THEN
				1
			ELSE
				0
			END AS is_group,

		CASE WHEN menuleft_el.pod>0 THEN
				(menuleft_parent.rang + 1)*" . ($maxpodcount + 1) . "+menuleft_el.rang
			ELSE
				(menuleft_el.rang + 1)*" . ($maxpodcount + 1) . "
			END AS ex_rang,

		CASE WHEN menuleft_el.pod>0 THEN
				menuleft_el.pod
			ELSE
				menuleft_el.id
			END AS ex_pod,
		
		CASE
			WHEN menuleft_el.pod > 0 THEN (SELECT COUNT(*) FROM catalog WHERE cat = menuleft_el.cat_id)
			ELSE 0
		END as products_count,
		
		menuleft_el.pod,
		menuleft_el.title,
		menuleft_el.cat_id,
		menuleft_el.rang,
		menuleft_el.id,
		menuleft_el.chpu,
		cat.chpu as cat_chpu,
		cat.hidelink as hidelink
	FROM
		menuleft as menuleft_el
			LEFT JOIN menuleft AS menuleft_parent
			ON menuleft_el.pod = menuleft_parent.id
			LEFT JOIN cat AS cat
			ON cat.id = menuleft_el.cat_id
	WHERE
		CASE WHEN menuleft_el.pod>0 THEN
				(menuleft_parent.rang + 1)*" . ($maxpodcount + 1) . "+menuleft_el.rang
			ELSE
				(menuleft_el.rang + 1)*" . ($maxpodcount + 1) . "
			END >= " . ($maxpodcount + 1) . "
	ORDER BY
		ex_rang,
		ex_pod
	");

	return mysql_query($query_str);
}

//Возвращает все теги
function getTags() {
	$tags = array();
	$res = mysql_query("SELECT * FROM `tags` ORDER BY `title`");
	while ($row = mysql_fetch_assoc($res)) {
		$tags[] = $row;
	}
	return $tags;
}

//Возвращает доступные теги\псевдокатегории в категории
//доп. параметр $info = 'full' - возвращает полностью инфу от тегах,
//'id' - только id
function getTagsInCat($id, $info = 'full') {
	if ( !is_numeric($id) ) return false;
	$tags = array();
	switch($info) {
	case 'id':
		$query = "SELECT id FROM `tags` WHERE `cat_id` = '{$id}' ORDER BY `rang`";
		$sel = mysql_query($query);
		while($row = mysql_fetch_assoc($sel)) {
			$tags[] = $row['id'];
		}
		break;
	default:
		$query = "SELECT * FROM `tags` WHERE `cat_id` = '{$id}' ORDER BY `rang`";		
		$sel = mysql_query($query);
		while($row = mysql_fetch_assoc($sel)) {
			$tags[] = $row;
		}		
		break;	
	}	
	
	return $tags;	
}

//Возвращает информацию о теге\псевдокатегории по чпу или id
function getTagInfo($chpu) {
	if ( is_numeric($chpu) ) {
		return sqlFetch("SELECT * FROM `tags` WHERE `id` = '{$chpu}'");
	} else {
		$chpu = mysql_real_escape_string($chpu);
		return sqlFetch("SELECT * FROM `tags` WHERE `chpu` = '{$chpu}'");
	}
}
//Возвращает теги, которые присутствуют у товара
function getTagsInCatalog($catalogId) {
	if (!is_numeric($catalogId)) return false;
	
	$sel = mysql_query("
		SELECT
			`tags`.*
		FROM
			`tags_links`,
			`tags`
		WHERE
			`tags_links`.`tag_id` = `tags`.`id`
			AND
			`tags_links`.`catalog_id`  = '{$catalogId}'
	");
	$tags = array();
	while ($row = mysql_fetch_assoc($sel)) {
		$tags[] = $row;
	}
	return $tags;
}
//Отсоединить каталог от псевдокатегории
function unlinkCatalogAndTags($catalogId, $tags = 'all') {
	if ($tags == 'all') {
		$whereString = "`catalog_id` = '{$catalogId}'";
	} elseif (is_numeric($tags)) {
		$whereString = "`catalog_id` = '{$catalogId}' AND `tag_id` = '{$tags}'";
	} elseif (is_array($tags)) {
		$whereString = "`catalog_id` = '{$catalogId}' AND `tag_id` IN ('".implode("','", $tags)."')";
	} else {
		return false;
	}
	
	mysql_query ("
		DELETE FROM
			`tags_links`
		WHERE
			{$whereString}
	");
	return true;
}
//
function linkCatalogAndTags($catalogId, $tags) {
	$tags = (array) $tags;
	
	foreach ($tags as $tag) {
		if (!is_numeric($tag)) continue;
		mysql_query("
			DELETE FROM 
				`tags_links`
			WHERE
				`catalog_id` = '{$catalogId}' AND
				`tag_id` = '{$tag}'
		");
		mysql_query("
			INSERT INTO
				`tags_links`
			SET
				`catalog_id` = '{$catalogId}',
				`tag_id` = '{$tag}'
		");
	}
	return true;
}
//Возвращает пункты и подпункты меню, для категорий возвращает и кол-во товров в них
//$allowSeparator - флаг, указыывающей на поддержку разделителей в выводе в шаблон
//если $allowSeparator = false, разделители не будут включены в выборку.
//Признак разделителя в базе pod != 0, cat_id = 0
function getMenu($allowSeparator = false) {
	$items = array();
	//$sel = mysql_query("SELECT * FROM `menuleft` WHERE `cat_id` = '0' AND `pod` = '0' ORDER BY `rang`");
	$sel = mysql_query("SELECT * , (SELECT `chpu` FROM `cat` WHERE `id` = `menuleft`.`cat_id` ) as `cat_chpu` FROM `menuleft` WHERE `pod` = '0' ORDER BY `rang`");
	while($row = mysql_fetch_assoc($sel)) {
		if(!isset($row['hidelink'])):
		$row['hidelink'] = 0;
		endif;
		$items[$row['id']] = array(
			'id' => $row['id'],
			'cat_id' => $row['cat_id'],
			'title' => $row['title'],
			'chpu' => $row['chpu'],
			'hidelink' => $row['hidelink'],
			'cat_chpu' => $row['cat_chpu'],
			'submenu' => array()
		);
	}
	
	$sel = mysql_query("
		SELECT
		*,
		(SELECT COUNT(*) FROM `catalog` WHERE `cat` = `menuleft`.`cat_id`) as `products_count`,
		(SELECT `chpu` FROM `cat` WHERE `id` = `menuleft`.`cat_id`) as `cat_chpu` 
		FROM
			`menuleft`
		WHERE `pod` <> '0' ORDER BY `rang`
	");
	while($row = mysql_fetch_assoc($sel)) {
		$row['separator'] = false;
		//Если это разделитель
		//if ($row['cat_id'] == '0' && $row['pod'] != '0') {
		//	if ($allowSeparator)
				//$row['separator'] = true;
		//	else
				//continue;
		//}
		if ($row['chpu'] == '') $row['chpu'] = $row['cat_chpu'];
		$items[$row['pod']]['submenu'][] = $row;
	}
	return $items;
}

// возвращает ссылку на бэк элемента меню
function backendLinks($url){
	
	$tab = substr($url,0,5);
	$chpu = substr($url,5);
	$toView = '' ;
	$toEdit = '' ;
	$id = 0 ;
	
	switch($tab) {
		case '/tag/':
			$query = "SELECT id FROM tags WHERE chpu = '{$chpu}' ;" ;
			$toView = '/edit/m_catalog/list.php?tag_id=' ;
			$toEdit = '/edit/m_tags/edit.php?tag_id=' ;
		break;
		case '/cat/':
			$query = "SELECT id FROM cat WHERE chpu = '{$chpu}' ;" ;
			$toView = '/edit/m_catalog/list.php?id=' ;
			$toEdit = '/edit/m_cat/edit.php?id=' ;
		break;
		default:
			return [ false ] ;
		break;
	}
	
	$res = mysql_query($query);
	while ($row = mysql_fetch_assoc($res)) {
		$id = $row['id'] ;
	}
	
	return array( 'edit' =>  $toEdit . $id , 'view' => $toView . $id );
}
// получаем фотку категории меню
function getCatImagePath($url) {
	
	$tab = substr($url,0,5);
	$chpu = substr($url,5);
	$id = 0 ;
	
	switch($tab) {
		case '/cat/':
			$query = "SELECT id FROM cat WHERE chpu = '{$chpu}' ;" ;
		break;
		default:
			return getImageWebPath('cats_menu') . 'no-image.jpg' ;
		break;
	}

	$res = mysql_query($query);
	while ($row = mysql_fetch_assoc($res)) {
		$id = $row['id'] ;
	}
	
	$link = getImageWebPath('cats_menu');
	return  $link . $id . '.jpg';
}

//Возвращает категории, которых нет в меню
function getCatsNotInMenu() {
	$sel = mysql_query("
		SELECT 
			*,
			(SELECT COUNT(*) FROM `catalog` WHERE `catalog`.`cat` = `cat`.`id`) as `products_count`
		FROM
			`cat`
		WHERE
			`id` NOT IN (
				SELECT cat_id FROM menuleft
			)
	");
	$cats = array();
	while ($row = mysql_fetch_assoc($sel)) {
		$cats[] = $row;
	}
	return $cats;
}
//Возвращает список категорий в пункте меню
function getCatsInMenuItem($chpu) {
	$chpu = mysql_real_escape_string($chpu);
	$cats = array();
	$sel = mysql_query("
		SELECT
		*,
		(SELECT `title` FROM `cat` WHERE `cat`.`id` = `menuleft`.`cat_id`) as `title`
		FROM `menuleft`
		WHERE `chpu` = '{$chpu}'
	");
	if ($res = mysql_fetch_assoc($sel)) {
		$sel = mysql_query("
			SELECT
				*,
				(SELECT `title` FROM `cat` WHERE `cat`.`id` = `menuleft`.`cat_id`) as `title`,
				(SELECT `chpu` FROM `cat` WHERE `cat`.`id` = `menuleft`.`cat_id`) as `chpu`
			FROM `menuleft`
			WHERE `pod` = '{$res['id']}' ORDER BY `rang`");
	} else {
		return false;
	}
	while ($res = mysql_fetch_assoc($sel)) {
		$cats[] = $res;
	}
	return $cats;
}
//Возвращает информацию о пункте меню
function getMenuItemInfo($chpu) {
	if (is_numeric($chpu)) {
		$sel = mysql_query("SELECT * FROM `menuleft` WHERE `id` = '{$chpu}' ");
	} else {
		$sel = mysql_query("SELECT * FROM `menuleft` WHERE `chpu` = '".mysql_real_escape_string($chpu)."'");
	}
	if ($res = mysql_fetch_assoc($sel)) {
		return $res;
	} else {
		return false;
	}
}
//Возвращает параметр с минимальным и максимальным значением
/*
[params] => Array
        (
            [0] => Array
                (
                    [id] => 10
                    [param_id] => 3
                    [value] => 
                    [value_float] => 1
                    [cat_id] => 0
                )
			...
        )
*/
function getMinAndMaxParams($params) {
	$params = (array) $params;
	if (count($params) == 0)
		return false;
	if (count($params) == 1)
		return array('min' => $params[0], 'max' => $params[0]);
	else {
		$min = $max = $params[0];
		foreach ($params as $p) {
			if ($min['value_float'] > $p['value_float'])
				$min = $p;
			if ($max['value_float'] < $p['value_float'])
				$max = $p;
		}
		return array('min' => $min, 'max' => $max);
	}
}
//Возвращает список товаров
//function getProducts ($page = false, $filter = false, $sort = false) {
function getProducts ($select = false, $filter = false, $sort = false, $page = false) {
	if ($select == false) $select = 'catalog.*';
	$select = (array) $select;

	$fromString = '`catalog`';
	
	//Костыль для новой системы категорий
	if ($filter['cat']) {
		$filter['`catalog_2_cat`.`cat_id`'] = $filter['cat'];
		unset($filter['cat']);
		$fromString = "`catalog_2_cat`
						INNER JOIN `catalog` 
						ON `catalog_2_cat`.`catalog_id` = `catalog`.`id`";
		if (!$sort)
			//$sortClause = "ORDER BY `catalog_2_cat`.`spec_rang` ASC";
			$sortClause = "ORDER BY hitact DESC , `active_rests` DESC, best DESC, price DESC";
	} 
	
	$products = array();
	$pagesCount = 0;
	
	//Создаем WHERE учитывая фильтр
	$compareType = array('!', '<', '>', '>=', '<=');
	$whereClause = array();
	$useParams = false;
	
	if ($filter != false) {
		foreach ($filter as $name => $value) {
			//Если присутствует фильтрация по тегу(псевдокатегории), а не категории
			if ($name == 'tag') {
				$value = (array) $value;
				$value = "'" . implode("','", $value) . "'";
				if (!$sort)
					//$sortClause = "ORDER BY `catalog`.`catalog_rang` ASC";
					$sortClause = "ORDER BY hitact DESC , `active_rests` DESC, best DESC, price DESC";
				
				$fromString = "
					(SELECT * FROM `catalog`, `tags_links` WHERE
					`id` = `tags_links`.`catalog_id` AND `tags_links`.`tag_id` IN ( {$value} )
					) as `catalog` 
					INNER JOIN `catalog_2_cat` 
					ON `catalog`.id = `catalog_2_cat`.catalog_id";
					
				continue;
			}
			
			if (is_array($value)) {
				$whereClause[] = "{$name} IN ('".implode("','", $value)."')";
			} else {
				//Выбираем имя фильтра и тип сравнения
				$compare = '=';
				$type = substr($name, 0, 2);
				if (in_array($type, $compareType)) {
					$compare = $type;
					$name = substr($name, 2);
				} elseif (in_array($type[0], $compareType)) {
					$compare = ($type[0] == '!' ? '<>' : $type[0]);
					$name = substr($name, 1);
				}
				$whereClause[] = "{$name} {$compare} '{$value}'";
			}
		}
	}

	$whereString = '';
	if (count($whereClause) > 0) {
		$whereString = 'WHERE '.implode(' AND ', $whereClause);
	}
	//Сортировка
	if (!$sortClause) {
		$sortClause = '';
		switch ($sort) {
			case 'price_desc' : $sortClause = 'ORDER BY `price_after_discount` DESC';
				break;
			case 'price_asc' : $sortClause = 'ORDER BY `price_after_discount` ASC';
				break;
			case 'news_sale' : $sortClause = 'ORDER BY `novinka` DESC, `discount_value` DESC';
				break;
			default : $sortClause = 'ORDER BY hitact DESC , `active_rests` DESC, best DESC, price DESC';
		}
	}
	
	//Лимит для разбиения на страницы и экстреммумы цен
	$res = mysql_fetch_assoc(
			mysql_query("
				SELECT DISTINCT
					COUNT(*) as amount,
					MAX(`price_after_discount`) as max_price,
					MIN(`price_after_discount`) as min_price
				FROM
					{$fromString}
				{$whereString}
			")
		);
	$outOfBounds = false;
	
	echo ($_GET['fringe'] === '1')?"<!-- " . print_r($page). " -->":NULL;
	
	if ($page == false || !isset($page['page']) || $page['page'] == 'all' || !isset($page['onPage']) || !is_numeric($page['onPage'])) {
		$limit = '';
	} else {
		$pagesCount = ceil($res['amount'] / $page['onPage']);
		if ($page['page'] <= $pagesCount || $pagesCount == 0) {
			$limit = 'LIMIT '.(($page['page'] - 1) * $page['onPage']).",{$page['onPage']}";
		} else {
			//Текущая страница за пределами доступных
			$outOfBounds = true;
			$limit = 'LIMIT 0,'.$page['onPage'];
		}
	}
	$price = array(
		'min' => $res['min_price'],
		'max' => $res['max_price']
	);
	
	//Создаем строку выборки для SELECT
	$selectString = implode(',', $select);
	$sel = mysql_query("
		SELECT DISTINCT
			(SELECT `amount` FROM storage_rests WHERE catalog.id = storage_rests.catalog_id AND storage_rests.storage_id = '1') as rest_main,
			(SELECT `amount` FROM storage_rests WHERE catalog.id = storage_rests.catalog_id AND storage_rests.storage_id = '2') as rest_extra,
			(select SUM(d.`amount`) from `storage_rests` as d 
				left join `storages_new` as e on e.storage_id = d.storage_id 
				where e.is_active = 1 and d.catalog_id = `catalog`.id ) as `active_rests`, 
			(select SUM(d.`amount`) from `storage_rests` as d 
				left join `storages_new` as e on e.storage_id = d.storage_id 
				where e.is_active = 1 and d.catalog_id = `catalog`.id
				and catalog.best = 1) as `hitact`,
			{$selectString}
		FROM
			{$fromString}
		{$whereString}
		{$sortClause}
		{$limit}
		");
	while ($res = mysql_fetch_assoc($sel)) {
		
		$product_rests = getActiveStoragesAmount($res['id']);
		
		$res['rest_main'] = $product_rests[1] ;
		$res['rest_extra'] = ( isset($product_rests[17]) ) ? $product_rests[17] : 0 ;
		
		$res['rests_main'] = array(
			//Доступны ли данные о наличии
			'available' => ($res['rest_main'] != 0 || $res['rest_extra'] != 0) ? true : false,
			//Сумма по складам
			'summ' => $res['rest_main'] + $res['rest_extra'],
			//Основной склад
			//'main' => empty($res['rest_main']) ? 0 : $res['rest_main'],
			'main' => $res['rest_main'] ,
			//Дополнительный
			//'extra' => empty($res['rest_extra']) ? 0 : $res['rest_extra'],
			'extra' => $res['rest_extra'] ,
			);
		$res['discount_summ_value'] = $res['price'] - getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']);
		$res['discount_summ_percent'] = $res['discount_type'] == 'percent' ? $res['discount_value'] : 100 - round((getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']) * 100) / $res['price']);
		
		$products[] = $res;
	}
	
	return array( 
		'products' => $products,
		'page' => array('pagesCount' => $pagesCount, 'currentPage' => $page['page'], 'outOfBounds' => $outOfBounds),
		'price' => $price
		);
}
// получаем наличие со складов по id товара
function getActiveStoragesAmount($productId){
	
	$info = [];
	
	$q_get_storages = mysql_query("select storage_id from `storages_new` where is_active = 1 ;") ;
	while ($res1 = mysql_fetch_assoc($q_get_storages)) {
		$info[$res1['storage_id']] = 0;
	}

	$q_get_amount = "select a.storage_id,  a.amount
					from storage_rests as a
					left join storages_new as b on a.storage_id = b.storage_id
					where catalog_id = {$productId} and b.is_active = 1 ;";
	$query = mysql_query($q_get_amount);
	
	while ($res = mysql_fetch_assoc($query)) {
		$info[$res['storage_id']] = $res['amount'];
	}
	return $info;
}

//Выполняем сдвиг сортировки для доп. категорий, 
//чтобы воткнуть новый элемент в начало списка
function updateSpecRang($cats) {
	$cats = (array)$cats;
	$inString = implode(', ', $cats);
	
	foreach ($cats as $cat_id) {
		$query = "
			UPDATE 
				`catalog_2_cat`
			SET
				`spec_rang` = `spec_rang` + 1
			WHERE
				`cat_id` IN 
				({$inString})";
		mysql_query($query);
		
		if (!mysql_error()) 
			return true;
		
	}
	return false;
}

//Возвращает массив, содержащий ранг товара в разных категориях
//Ключи массива - айдишники категорий.
function getSpecRang($catalog_id) {
	if (is_numeric($catalog_id)) {
		
		$query = "SELECT * 
				  FROM `catalog_2_cat` 
				  WHERE `catalog_id` = {$catalog_id}";
		$res = mysql_query($query);
		
		if (mysql_error())
			return false;
		$rang = array();
		if (mysql_num_rows($res) != 0) {
			while ($row = mysql_fetch_assoc($res)) {
				$rang[$row['cat_id']] = $row['spec_rang'];
			}
		}
		return $rang;
	}
	return false;
}


//Добавление доп. категорий
function linkAdditionalCats($catalog_id, $cats){
	if (is_numeric($catalog_id)) {	
		$cats = (array)$cats;
		
		//Получаем массив, содержащий ранг товара во всех категориях.
		//Если товар добавляется впервые, вернет пустой массив.
		$spec_rang = getSpecRang($catalog_id);
		
		
		//Удаляем все слинковки с доп категориями
		$res = mysql_fetch_assoc(mysql_query("SELECT * FROM `catalog` WHERE `id` = '{$catalog_id}'"));
		if (!empty($res)) {
			mysql_query("DELETE FROM `catalog_2_cat` 
						 WHERE `catalog_id` = {$catalog_id}
						 AND `cat_id` != {$res['cat']}");
		}
		//Линк с доп категориями	
		foreach ($cats as $cat_id) {			
			
			//Добавляем товар в доп. категории
			if (array_key_exists($cat_id, $spec_rang))
				$rang = $spec_rang[$cat_id];
			else
				$rang = 0;
			
			$query = "INSERT INTO `catalog_2_cat` (`catalog_id`, `cat_id`, `spec_rang`)
										   VALUES ({$catalog_id}, {$cat_id}, {$rang})";
			
			mysql_query($query);
			
			if (mysql_error())
				return false;
						
		}
		return true;
	}
	return false;
}

//Возвращает массив, содержащий id доп. категорий для 
//данного товара
function getAdditionalCatsId($catalog_id) {
	if (is_numeric($catalog_id)) {
		$res = mysql_fetch_assoc(mysql_query("SELECT `cat` FROM `catalog` WHERE `id` = {$catalog_id}"));
		//Родная категория
		$cat_id = $res['cat'];
		//Выбор всех категорий, кроме родной
		$query = "SELECT `cat_id` 
				  FROM `catalog_2_cat` 
				  WHERE `catalog_id` = {$catalog_id}
				  AND `cat_id` != {$cat_id}";
		$res = mysql_query($query);
		$catsId = array();

		while ($row = mysql_fetch_assoc($res)) {			
			$catsId[] = $row['cat_id'];
		}
		return $catsId; 
	}
	return false;
}

//убрать товар из доп. категории
function unlinkFromAdditionalCat($catalog_id, $cat_id) {
	if (is_numeric($cat_id)) {
		$query = "DELETE FROM `catalog_2_cat` 
				  WHERE `catalog_id` = '{$catalog_id}'
				  AND `cat_id` = '{$cat_id}'";
		mysql_query($query);
		
		if (mysql_error())
			return false;
			
		return true;	
	}
	return false;
}

//Поисковая функция
function getSearchProducts($search_text, $page)
{
	
	// XML запрос Яндексу
    $doc = <<<DOC
<?xml version='1.0' encoding='utf-8'?>
<request>
    <query>{$search_text} host:med-serdce.ru</query>
    <groupings>
    	<groupby attr='' mode='flat' groups-on-page='1000' docs-in-group='1' />
    </groupings>
</request>
DOC;

    $context = stream_context_create(array(
        'http' => array(
            'method'=>"POST",
            'header'=>"Content-type: application/xml;charset=utf-8\r\n" .
                      "Content-length: " . strlen($doc),
            'content'=>$doc
        )
    ));
	
	$response = file_get_contents('http://xmlsearch.yandex.ru/xmlsearch?user='.Config::get('yandex_search.user').'&key='.Config::get('yandex_search.key'), false, $context);
	
	
	    if ( $response ) 
		{
			
			$xmldoc = new SimpleXMLElement($response);
			$error = $xmldoc->response->error;
			$found_all = $xmldoc->response->found;
			$found_all = (string)$found_all;
			
			$found = $xmldoc->xpath("response/results/grouping/group/doc");
			
			if (!$error) 			
			{
				
				
				// print_r($page);
				
				// echo $page['page'];
				
				$i = 0; //счетчик найденных товаров
				
				foreach ($found as $item) 
				{
					
					$item->url = ltrim($item->url, 'https:');
             		
					if (substr_count($item->url, 'catalog') > 0) 
					{
						
						$i++;
						
						$page['page'] = (is_numeric($_GET['page']))?$_GET['page']:1;
						//проверка товара на принадлежность к текущей странице
						if ($i <= $page['page'] * $page['onPage'] &&
							$i >  ($page['page'] - 1) * $page['onPage'])
						{
							$url = explode('/', $item->url);
							$chpu = str_replace('.html', '', $url[4]);
							$chpus[] = $chpu;
							$chpu_arr[] = "'" . $chpu . "'";
						}
					} 

				}  
				
				$pagesCount = ceil($i / $page['onPage']);
				//достаем из базы найденные товары
				
				$find_in_set = implode($chpus, ',');
				
				$in_str = implode($chpu_arr, ', ');
				
		
				$query = "SELECT * FROM `catalog`
						  WHERE `chpu` IN ({$in_str})
						  ORDER BY
						  FIND_IN_SET (chpu, '{$find_in_set}')";
				$res = mysql_query($query);
				
				while ($row = mysql_fetch_assoc($res))
				{
					$products['data'][] = $row;
				}
				
				// print_r($products['data']);
				
				//общее количество страниц, найденных Яндексом
				$products['found'] = $found_all;
				$products['page'] = array('currentPage' => $page['page'], 
										  'pagesCount' => $pagesCount);

				// print_r($products['page']);		
				
		
		file_put_contents('prodz.txt', var_export($products, true));
				  
				return $products;

			}
		} 
		
		return NULL;
}


//Возвращает список товаров, найденных с помощью дополнительного фильтра по товарам
function getProductsWithExtraFilter($select = false, $filter = false, $sort = false, $page = false) {
	if ($select == false) $select = '*';
	$select = (array) $select;

	$fromString = '`catalog`';
	$products = array();
	$pagesCount = 0;
	
	//Создаем WHERE учитывая фильтр
	$compareType = array('!', '<', '>', '>=', '<=');
	$whereClause = array();
	$useParams = false;
	
	if ($filter != false) {
		foreach ($filter as $name => $value) {
			//Если присутствует фильтрация по тегу(псевдокатегории), а не категории
			if ($name == 'tag') {
				$value = (array) $value;
				$value = "'" . implode("','", $value) . "'";
				
				$fromString = "
						(SELECT * FROM `catalog`, `tags_links` WHERE
						`id` = `tags_links`.`catalog_id` AND `tags_links`.`tag_id` IN ( {$value} )
						) as `catalog`";
				
				continue;
			}
			if (is_array($value)) {
				if ($name != 'params') {
					$whereClause[] = "{$name} IN ('".implode("','", $value)."')";
				}
			} else {
				//Выбираем тип сравнения
				$compare = '=';
				$type = substr($name, 0, 2);
				if (in_array($type, $compareType)) {
					$compare = $type;
					$name = substr($name, 2);
				} elseif (in_array($type[0], $compareType)) {
					$compare = ($type[0] == '!' ? '<>' : $type[0]);
					$name = substr($name, 1);
				}
				
				$whereClause[] = "{$name} {$compare} '{$value}'";
			}
		}
	}
	$whereString = '';
	if (count($whereClause) > 0) {
		$whereString = 'WHERE '.implode(' AND ', $whereClause);
	}
	
	//Соберем id значений параметров, чтобы потом определить самих параметров
	//Самих значений
	$valuesIds = array();
	/*
		$p = 
			array('type' => PARAM_VALUE, 'data' => 2)
			array('type' => PARAM_RANGE, 'data' => array('id' => 2, 'min' => 2, 'max' => 3))
	*/

	foreach($filter['params'] as $p) {
		if ($p['type'] == PARAM_VALUE || $p['type'] == PARAM_SET) {
			$valuesIds[] = $p['data'];
		} elseif ($p['type'] == PARAM_RANGE) {
			$paramsRange[] = $p;
		}
	}
	

	$paramsIn = "'".implode("','", $valuesIds)."'";
	
	//Разобьем id параметров по группам (группа = id типа параметра; цвет, вес, размер и тп.)
	$sel = mysql_query("
		SELECT 
			params_catalog_links.*,
			params_available_values.param_id as param_type_id
		FROM
			params_catalog_links
		LEFT JOIN params_available_values
			ON params_catalog_links.param_id = params_available_values.id
		WHERE
			params_catalog_links.param_id IN ({$paramsIn})
	");
	$groups = array();
	while ($res = mysql_fetch_assoc($sel)) {
		$groups[$res['param_type_id']][] = $res['param_id'];
	}
	//Удаляем дубликаты
	foreach ($groups as $key => $value) {
		$groups[$key] = array_unique($value);
		$newArray = array();
		$newArray['type'] = PARAM_VALUE;
		foreach ($groups[$key] as $z) {
			$newArray['data'][] = $z;
		}
		$groups[$key] = $newArray;
	}
	//Добавляем к группам типы диапазоны
	foreach ($paramsRange as $p) {
		$groups[$p['data']['id']] = array('type' => PARAM_RANGE, 'data' => $p['data']);
	}
	
	//Сортировка
	$sortClause = '';
	switch ($sort) {
		case 'price_desc' : $sortClause = 'ORDER BY `price_after_discount` DESC';
			break;
		case 'price_asc' : $sortClause = 'ORDER BY `price_after_discount` ASC';
			break;
		case 'news_sale' : $sortClause = 'ORDER BY `novinka` DESC, `discount_value` DESC';
			break;
		//default : $sortClause = 'ORDER BY `spec_rang` ASC';
	}
	
	//Подготоваливаем  JOIN'ы для запроса
	$inner = '';
	$field = 'catalog.id';
	$onString = array();
	foreach ($groups as $key => $value) {
		if ($value['type'] == PARAM_VALUE) {
			$inner .= " INNER JOIN ( SELECT catalog_id FROM `params_catalog_links` WHERE `param_id` IN ('".implode("', '", $value['data'])."') ) as main".$key;
		} else {
			$inner .= " INNER JOIN ( SELECT catalog_id FROM `params_catalog_links` WHERE `param_id` IN 
				( SELECT id FROM `params_available_values` WHERE `value_float` >= '{$value['data']['min']}' AND `value_float` <= '{$value['data']['max']}' AND  `param_id` = '{$key}' ) 
			) as main".$key;
		}
		$onString[] = $field . ' = main' . $key . '.catalog_id';
		$field = 'main' . $key . '.catalog_id';
	}
	$onString = implode(' AND ', $onString);
	
	//Лимит для разбиения на страницы и макс и мин цены
	//Попробуем замутить подсчет доступных товаров
	$temp = mysql_fetch_assoc(mysql_query("
			SELECT 
				COUNT(DISTINCT catalog.id) as c,
				MAX(`price_after_discount`) as max_price,
				MIN(`price_after_discount`) as min_price
			FROM 
				{$fromString}
			{$inner}
			ON {$onString}
			{$whereString}
	"));
	if ($page == false || !isset($page['page']) || $page['page'] == 'all' || !isset($page['onPage']) || !is_numeric($page['onPage'])) {
		$limit = '';
	} else {
		$pagesCount = ceil($temp['c'] / $page['onPage']);
		if ($page['page'] <= $pagesCount) {
			$limit = 'LIMIT '.(($page['page'] - 1) * $page['onPage']).",{$page['onPage']}";
		} else {
			$limit = 'LIMIT 0,'.$page['onPage'];
		}
	}
	
	$price = array(
		'min' => $res['min_price'],
		'max' => $res['max_price']
	);
	
	//Выбирам те товары, которые содержат все значения характеристик, выбранные в фильре
	$sel = mysql_query ("
			SELECT 
				DISTINCT(catalog.id),
				catalog.*
			FROM 
				{$fromString}
			{$inner}
			ON {$onString}
				{$whereString}
				{$sortClause}
				{$limit}
	");

	while ($res = mysql_fetch_assoc($sel)) {
		$res['discount_summ_value'] = $res['price'] - getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']);
		$res['discount_summ_percent'] = $res['discount_type'] == 'percent' ? $res['discount_value'] : 100 - round((getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']) * 100) / $res['price']);
		$products[] = $res;
	}
	return array( 
		'products' => $products,
		'page' => array('pagesCount' => $pagesCount, 'currentPage' => $page['page']),
		'price' => $price
		); 
}
//Возвращает информацию о товаре
function getProduct($idOrChpu) {
	$data = array();
	
	if (is_numeric($idOrChpu)) {
		$sel = mysql_query("SELECT * FROM `catalog` WHERE `id` = '{$idOrChpu}'");
	} else {
		$chpu = mysql_real_escape_string($idOrChpu);
		$sel = mysql_query("SELECT * FROM `catalog` WHERE `chpu` = '{$chpu}'");
	}
	
	//Данные по товару
	if ($line = mysql_fetch_assoc($sel)) {
		$data = $line;
	} else {
		return false;
	}

	//Дополнительные фото
	$data['extra_photo'] = array();
	
	$sel = mysql_query("SELECT * FROM `foto` WHERE `catalog_id` = '{$data['id']}' ORDER BY `rang`");
	while ($row = @mysql_fetch_assoc($sel)) {
		$data['extra_photo'][] = $row['id'];
	}
	
	//Информация о категории
	$data['cat_info'] = getCatInfo($data['cat']);
	
	//Информация о тегах (подакатегориях) категории
	$data['tags'] = getTagsInCat($data['cat_info']['id']);
	//Похожие товары
	$data['near'] = array();
	
	$productsCount = fetchOne("SELECT COUNT(*) FROM `catalog` WHERE	`cat` = '{$data['cat']}'");
	
	//Код криоват, но время не ждет
	if ($productsCount <= 5) {
		$sel = mysql_query("SELECT `id` FROM `catalog`	WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
			ORDER BY `spec_rang`
			
		");
	} else {
		//Выборка товаров по типу "Кольцо"
		$sel = mysql_query("SELECT `id` FROM `catalog`	WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
				AND `spec_rang` >= '{$data['spec_rang']}'
			ORDER BY `spec_rang`
			
		");
	}
	
	while ($row = mysql_fetch_assoc($sel)) {
		$data['near'][] = $row['id'];
	}
	//Дополняем товарами из начала (по сортировке)
	if (count($data['near']) < 4 && $productsCount > 5) {
		$lim = 4 - count($data['near']);
		$sel = mysql_query("SELECT `id` FROM `catalog` WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
				AND `spec_rang` >= '0'
			ORDER BY `spec_rang`
			LIMIT {$lim}
		");
		while ($row = mysql_fetch_assoc($sel)) {
			$data['near'][] = $row['id'];
		}
	}
	$tmp = getProducts(false, array('id' => $data['near']));
	$data['near'] = $tmp['products'];
	
	//Предыдущий\следующий товар
	$prev = sqlFetch("SELECT
				`id`, `title`, `chpu`, `price`, `price_after_discount`
			FROM `catalog`	WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
				AND `spec_rang` <= '{$data['spec_rang']}'
			ORDER BY `spec_rang` DESC
			LIMIT 1
		");
	if ($prev == false) {
		$prev = sqlFetch("SELECT 
				`id`, `title`, `chpu`, `price`, `price_after_discount`
				FROM `catalog` WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
			ORDER BY `spec_rang` DESC
			LIMIT 1
		");
	}
	$data['around'] = array(
		'prev' => $prev,
		'next' => $data['near'][0]
	);
	//Выдернуть параметры
	$sel = mysql_query("
		SELECT
			*,
			`params_available_values`.`param_id` as `param_type_id`,
			`params_available_values`.`id` as `param_available_id`
		FROM `params_catalog_links`
		INNER JOIN `params_available_values`
			 ON `params_catalog_links`.`param_id` =  `params_available_values`.`id`
		INNER JOIN `params_available`
			ON `params_available_values`.`param_id` =  `params_available`.`id`
			
		WHERE
			`catalog_id` = '{$data['id']}'
	");
	$params = array();
	while ($row = mysql_fetch_assoc($sel)) {
		$params[$row['param_type_id']]['title'] = $row['title'];
		$params[$row['param_type_id']]['type'] = $row['type'];
		$params[$row['param_type_id']]['type_id'] = $row['param_type_id'];
		$params[$row['param_type_id']]['unit'] = $row['unit'];
		$params[$row['param_type_id']]['values'][] = array(
			'value' => $row['type'] == PARAM_RANGE ? $row['value_float'] : $row['value'],
			'id' => $row['param_available_id']
		);
	}
	$data['recomm'] = getRecommendations($data['id']);

	// получаем сопутствующие товары
	$data['related_products'] = getRelatedProducts($data['id']);

	//Скинем ключи
	$params = array_merge($params);
	
	$data['params'] = $params;
	
	//Подсосать остатки
	$rests = array();
	$sel = mysql_query("SELECT * FROM `storage_rests` WHERE `catalog_id` = '{$data['id']}'");
	while ($row = mysql_fetch_assoc($sel)) {
		$rests[$row['storage_id']] = $row['amount'];
	}
	$data['rests'] = $rests;
	$data['rests_main'] = array(
		//Доступны ли данные о наличии
		'available' => isset($rests[1]) || isset($rests[2]),
		//Сумма по складам
		'summ' => $rests[1] + $rests[2],
		//Основной склад
		'main' => $rests[1],
		//Дополнительный
		'extra' => $rests[2]
	);
	
	//Просичитаем стоимость доставки по Москве
	if ($data['cat'] > 0 && $data['price_after_discount'] >= 0) {
		$data['deliveryPrice'] = deliveryPrice::getByCatAndPrice($data['cat'], $data['price_after_discount'], $data['id']);
	} else {
		$data['deliveryPrice'] = false;
	}
	
	//Дополнительные данные о скидке на товар
	$data['discount_summ_value'] = $data['price'] - getPriceAfterDiscount($data['price'], $data['discount_type'], $data['discount_value']);
	$data['discount_summ_percent'] = $data['discount_type'] == 'percent' ? $data['discount_value'] : 100 - round((getPriceAfterDiscount($data['price'], $data['discount_type'], $data['discount_value']) * 100) / $data['price']);
	
	return $data;
}

// получаем красивую строку гарантии 
function getWarrantyString($number){
	
	switch($number){
		case 1:
			$str = ' год' ;
		break;
		
		case 2:
		case 3:
		case 4:
			$str = ' года' ;
		break;
		default:
			$str = ' лет' ;
		break;
	}
	return " {$number} {$str} " ;
}


//Возвращает данные о категории по id или chpu
/*function getCatInfo($idOrChpu) {
	return mysql_fetch_assoc(mysql_query("SELECT * FROM `cat` WHERE `id` = " . $idOrChpu ));
}*/

function getCatInfo($idOrChpu) {
	$where = (is_numeric($idOrChpu) ? 'id' : 'chpu') . " = '".mysql_real_escape_string($idOrChpu)."'";
	return mysql_fetch_assoc(mysql_query("SELECT * FROM `cat` WHERE " . $where));
}

//Возвращает список категорий с подкатегориями
//2 уровня вложенности
function getCats() {
	$cats = array();
	
	/*$sel = mysql_query("SELECT * FROM `cat` ORDER BY `pod`, `title`");*/
	
	$sel = mysql_query("SELECT * FROM `cat` ORDER BY `title`");
	while ($line = mysql_fetch_assoc($sel)) {
		$cats[ $line['id'] ] = $line;
	
	/*	if ($line['pod'] == 0) {
			$cats[ $line['id'] ] = $line;
		} else {
			$cats[ $line['pod'] ]['subcats'][] = $line;
		}*/
	}
	return $cats;
}
/*
Добавляет заказ в базу, отправляет письма, короче обычное оформление

$productsInCart[ $productTemp[0] ] = array(
					'id' => $productTemp[0],
					'amount' => $productTemp[1]
					);
*/

function getPaymentTypes() {
	$payment_types = array();
	$sql = mysql_query("SELECT * FROM order_payment_types");
	while ($row = mysql_fetch_assoc($sql)) {
		$payment_types[] = $row;
	}
	return $payment_types;
}
function makeOrder($productsInCart, $orderInfo) {
	$autoreserve = true; // включаем авторезервирование
	//Если товаров в корзине нет, не пропускаем запрос
	if (count($productsInCart) <= 0 && !$orderInfo['backcall']) {
		return array(
			'error' => array(
				'code' => 1,
				'message' => 'empty cart'
			)
		);
	}
	$data['date'] = time();
	$data['name'] = mysql_real_escape_string($orderInfo['name']);
	$data['phone'] = mysql_real_escape_string($orderInfo['phone']);
	$data['email'] = mysql_real_escape_string($orderInfo['email']);
	$data['adress'] = mysql_real_escape_string($orderInfo['post_index'].' '.$orderInfo['adress']);
	$data['extra_information'] = mysql_real_escape_string($orderInfo['extra_information']);
	$data['delivery_type'] = mysql_real_escape_string($orderInfo['delivery_type']);
	$data['payment_type'] = mysql_real_escape_string($orderInfo['payment_type']);
	$data['msg'] = mysql_real_escape_string($orderInfo['msg']);
	
	$data['dev_type']=$orderInfo['dev_type'];
	if (empty($data['delivery_type'])) $data['delivery_type'] = 1;
	if (empty($data['payment_type'])) $data['payment_type'] = 1;
	if (isset($orderInfo['comment'])) $data['comment'] = mysql_real_escape_string($orderInfo['comment']);
	if ($orderInfo['backcall']) {
		$data['delivery_type'] = 0;
		$data['payment_type'] = 1;
		$data['extra_information'] = 'Обратный звонок';
		$autoreserve = false; // отлючаем авторезервирование, если заказан обратный звонок
	}
	
	//кука "Откуда пришел пользователь"	
	$data['h'] = mysql_real_escape_string($orderInfo["ref"]);
	
	
	if (strlen($data['name']) >= 3 && strlen($data['phone']) >= 5) {
		//Товары, которые пойдут в базу
		$productsToBase = array();
		//Товары и инфо о них, которые пойдут в письмо менеджеру
		$productsInfo = array();
		//Массив, который будет использоваться для передачи в JS
		$productsToResponse = array();
		//Суммарная стоимость с учетом скидок на товары
		$priceSumm = 0;
		$productsAmount = 0;
		
		$sel = mysql_query("
			SELECT
				*
			FROM
				`catalog`
			WHERE
				`id` IN ('". implode("','", array_keys($productsInCart)) ."')
		");
		while ($line = mysql_fetch_assoc($sel)) {
			$productsToBase[] = array(
				'id' => $line['id'],
				'amount' => $productsInCart[ $line['id'] ]['amount'],
				'price' => $line['price'],
				'price_after_discount' => $line['price_after_discount']
			);
			$productsToResponse[] = array(
				'id' => $line['id'],
				'amount' => $productsInCart[ $line['id'] ]['amount'],
				'price' => $line['price'],
				'price_after_discount' => $line['price_after_discount'],
				//'converted_title' =>  iconv("windows-1251", "utf-8", $line['title']),
				//'converted_title' =>  $line['title'],
				'title' => $line['title']
			);
			$priceSumm += $line['price_after_discount'] * $productsInCart[ $line['id'] ]['amount'];
			$productsAmount += $productsInCart[ $line['id'] ]['amount'];
			
			//Полные данные о товарах для вывода в письме менеджеру
			$productsInfo[] = array_merge(
								$line,
								array('amount' => $productsInCart[ $line['id'] ]['amount'])
							);
		}
		
		//Получение скидки и цены доставки для Курьера по москве
		$temp = cart::getDeliveryAndDiscount($productsToBase);
		
		$deliveryPrice = $temp['deliveryPrice'];
		$discount['value'] = $temp['discount'];
		$discount['type'] = 'percent';

		$price = array(
			'discount_type' => $discount['type'],
			'discount_value' => $discount['value'],
			'price_after_global_discount' => getPriceAfterDiscount($priceSumm, $discount['type'], $discount['value']),
			'price_before_global_discount' => $priceSumm,
			'delivery_price' => $temp['deliveryPrice']
		);
		$data['products'] = serialize($productsToBase);
		$data['order_price'] = serialize($price);
		
		/*
			products = 
			array(
				'id' =>
				'amount' =>
				'price' =>
				'price_after_discount' =>
			)
			order_price = 
			array(
				'discount_type' =>
				'discount_value' =>
				'price_after_global_discount' =>
			)
		*/

		$sqlParams = array();
		foreach ($data as $key => $value) {
			$sqlParams[] = "`{$key}` = '{$value}'";
		}
		mysql_query("
			INSERT INTO
				`orders`
				SET ".(implode(', ', $sqlParams))."
			");

		$orderId = mysql_insert_id();
		$orderCode = getOrderCode($orderId);
		
		
		//Подготавливаем отправку письма менеджеру
		query_config();
		$conf['site_url'] = Config::get('site.web_addr');
		$conf['support_email'] = Config::get('site.email_support');
		$conf['site_name'] = Config::get('site.name');
		//Тема письма
		$subject = "Заказ с сайта {$config_url} {$orderCode}";

		// Формируем заголовок от кого
		$header1 = "From: {$data['name']} <{$data['phone']}>\r\n";
		//Формируем текст письма
		$body = '<html>';
		
		//Доставка текстом
		$sd = mysql_query("SELECT * FROM `order_delivery_types` WHERE `id` = '{$data['delivery_type']}'");
		$deliveryName = mysql_fetch_assoc($sd);
		$deliveryName = $deliveryName['name'];
		
		
		foreach ($productsInfo as $p) { 
			$body .= '
			<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
			  <tr>
				<td width="80" align="left" valign="top"><a href="' . $conf['site_url'] . '/catalog/' . $p['chpu'] . '.html"><img src="/upload/small/' . $p['id'] . '.jpg"  border="0" align="left" /></a></td>
				<td align="left" valign="top"><a href="' . $conf['site_url'] . '/catalog/' .$p['chpu']. '.html" style="color:#333;">' . $p['title'] . '</a> <span style="font-size: 1.3em;">(' . $p['amount'] . ' шт.)</span><br />
				  <br />
				  <span style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;">' . $p['price_after_discount'] . '</span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">руб.</span>
				 </td>
				</tr>
			</table>';
		}
		$body .= '
		<span style="font-size: 1.2em;">Общая сумма: ' . $price['price_after_global_discount'] . ' (скидка ' . $price['discount_value']. ' ' .getDiscountTypeString($price['discount_type']) . ')</span><br />
		<br />
		<strong>Имя:</strong> ' . $data['name'] . '<br />
		<strong>Тел.:</strong> ' . $data['phone'] . '<br />
		<strong>Доставка:</strong> ' . $data['dev_type'] . '<br />
		<strong>Адрес:</strong> ' . $data['adress'] . '<br />';

		if ($data['email'] != '') $body .= ('<strong>E-mail:</strong> ' . $data['email'] . '<br />');
		$body .='<strong>Дополнительно:</strong> ' . $data['adress'] . '<br />';
		
		$body .= '</html>';

		// Отправляем заказ на почту сайта
		/*
		sendMail(array(
			'emailTo' => Config::get('site.email_manager'),
			'subject' => $subject,
			'body' => $body
		));
		*/
		sendMailPHPMailer($subject, $body, Config::get('site.email_manager'));
		// --- Отправляем письмо пользователю о том, что заказ оформлен
		// Если покупатель ввел email, то отправляем ему письмо о заказе 
		if (validateEmail($data['email'])) {
			$tema = "Вашему заказу присвоен номер {$orderCode} ({$conf['site_url']})";
			$pismo = "<html>".
			"Мы рады, что Вы выбрали именно наш интернет магазин.".
			"<br>".
			"Вашему заказу присвоен номер <strong>{$orderCode}</strong><br>".
			"В ближайшее время, для подтверждения заказа, с Вами свяжется наш менеджер.".
			"<br>".
			"Если с обработкой Вашего заказа возникли проблемы - пишите на {$conf['support_email']} .".
			"<br>".
			"С уважением, {$conf['site_name']}.".
			"</html>";
			/*
			sendMail(array(
				'emailTo' => $data['email'],
				'subject' => $tema,
				'body' => $pismo
			));
			*/
			sendMailPHPMailer($tema, $pismo, $data['email']);
		}
		return array(
			'orderId' => $orderId,
			'orderCode' => $orderCode,
			'deliveryName' => $data['dev_type'],
			'productsToBase' => $productsToBase,
			'price' => $price,
			'autoreserve' => $autoreserve,
			'username' => $data['name'],
			'adress' => $data['adress'],
			'userComment' => $data['extra_information'],
			'productsToResponse' => $productsToResponse ,
		);
	} else {
		return array(
			'error' => array(
				'code' => 2,
				'message' => 'empty main client info (name or phone)'
			)
		);
	}
}
//Если клиент оформляет кредит
function makeCreditOrder($productsInCart, $orderInfo) {
	$order = array();
	
	foreach($productsInCart as $product) {
		$i++;
		$order['items'][$i]['title'] = $product['title'];
		$catInfo = getCatInfo($product['cat']);
		$order['items'][$i]['category'] = $catInfo['title'];
		$order['items'][$i]['qty'] = $product['amount'];
		$order['items'][$i]['price'] = round($product['price_after_discount'] * $product['amount']);
	}
	
	$FIO = explode(' ', trim($orderInfo['name']));
	$order['details']['firstname'] = $FIO[0];
	$order['details']['middlename'] = $FIO[1];
	$order['details']['lastname'] = $FIO[2];
	$order['details']['email'] = $orderInfo['email'];
	$order['details']['cellphone'] = $orderInfo['phone'];
	$order['partnerId'] = $orderInfo['1-178YO4Z'];
	$order['partnerName'] = 'med-serdce.ru';
	$order['partnerOrderId'] = rand(999, 99999);
	
	return $order;
}


//Работа с параметрами урл
//Подготавливает параметры в ссылке (после ? (знака вопроса))
class queryString {
	public $params = array();
	protected $hash;
	public function __construct($url = false) {
		if ($url == false) {
			$this->params = $_GET;
			unset($this->params['c'], $this->params['t']);
		} else {
			$this->params = $url;
		}
	}
	public function getParam($key) {
		if (isset($this->params[$key]))
			return $this->params[$key];
		else
			return null;
	}
	public function setParam($key, $value) {
		$obj = clone $this;
		$obj->params[$key] = $value;
		return $obj;
	}
	public function removeParam($key) {
		$obj = clone $this;
		if (isset($obj->params[$key])) unset($obj->params[$key]);
		return $obj;
	}
	public function setHash($string) {
		$obj = clone $this;
		$obj->hash = $string;
		return $obj;
	}
	public function __toString() {
		if (count($this->params) > 0 ) {
			$temp = array();
			foreach ($this->params as $key => $value) {
				// if (($key=='page')&&($value==1)){
					// return '?';
				// }
				// else{
					$temp[] = $key . '=' . $value;
				// }
			}
			return '?' . implode('&', $temp).(!empty($this->hash) ? '#'.$this->hash : '');
		} else {
			return ''.(!empty($this->hash) ? '#'.$this->hash : '');
		}
	}
}

function getPage() {
	
	$page = $_REQUEST['page'];
	
	if ($page == 'all') {
		return 'all';
	} else if (is_numeric($page) && $page > 0) {
		return $page;
	} else {
		return 1;
	}
}
function generatePaginator($data, $qString = false, $onclick = false) {
	if ($data['pagesCount'] > 1) {
		if ($qString == false) $qString = new queryString();
		echo '<div class="paginator"><table border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="90" align="left"><em class="txt_sort">Страницы:</em></td>
			<td align="left">';
				for ($i = 1; $i <= $data['pagesCount']; $i++) { 
					if ($i ==  $data['currentPage']) {
						echo '<div class="divstr"><div><div class="divstrvns current">'.$i.'</div></div></div>';
					} else {
						if ($onclick != false) {
							echo '<div class="divstr"><a onclick="'.str_replace('[page]', $i, $onclick).'" href="#"><div class="divstrvn">'.$i.'</div></a></div>';
						} else {
							echo '<div class="divstr"><a href="'.$qString->setParam('page', $i).'"><div class="divstrvn">'.$i.'</div></a></div>';
						}
					}
				}
		echo '</td>';
		if ($onclick != false) {
			echo '<td width="70" align="right"><a onclick="'.str_replace('[page]', "'all'", $onclick).'" href="#"><em>все сразу</em></a></td>';
			} else {
			echo '<td width="70" align="right"><a href="'.$qString->setParam('page', 'all').'"><em>все сразу</em></a></td>';
			}
		 echo ' </tr>
		</table></div>';
	}
}
function showPaginator($data, $qString = false, $onclick = false) {
	if ($data['pagesCount'] > 1) {
		if ($qString == false) $qString = new queryString();
		echo '<div class="paginator">';
		if ($data['currentPage'] != 1) 
			echo '<a href="'.$qString->setParam('page', $data['currentPage'] - 1).'" title="" class="page-back"' . ($onclick ? 'onclick="'.str_replace('[page]', $data['currentPage'] - 1, $onclick).'"' : '') . '>Назад</a>';
		if ($data['currentPage'] != $data['pagesCount'])
			echo '<a href="'.$qString->setParam('page', $data['currentPage'] + 1).'" title="" class="page-forward"' . ($onclick ? 'onclick="'.str_replace('[page]', $data['currentPage'] + 1, $onclick).'"' : '') . '>Вперед</a>';

		echo '<div class="pages">';
			for ($i = 1; $i <= $data['pagesCount']; $i++) {
				if ($i == $data['currentPage']) {
					echo '<span>'.$i.'</span>';
				} else {
					echo '<a href="'.$qString->setParam('page', $i).'" title=""' . ($onclick ? 'onclick="'.str_replace('[page]', $i, $onclick).'"' : '') . '>' . $i . '</a>';
				}
			}
		echo '</div>';
		
		echo '</div>';
	}
	/*
	<div class="pager">
		<a href="#" title="" class="page-back">Назад</a>
		<a href="#" title="" class="page-forward">Вперед</a>
		<div class="pages">
			<a href="#" title="">1</a>
			<a href="#" title="">2</a>
			<a href="#" title="">3</a>
			<a href="#" title="">4</a>
			<a href="#" title="">5</a>
			<a href="#" title="">6</a>
			<span>. . . .</span>
			<a href="#" title="">25</a>
			<a href="#" title="">26</a>
		</div>
	</div>
	*/
}
function getSort() {
	$sort = (isset($_REQUEST['sort']))?$_REQUEST['sort']:' ';
	if (in_array($sort, array('price_asc', 'price_desc', 'news_sale'))) {
		return $sort;
	} else {
		return false;
	}
}
//Функция выставляет стоимость товара в базе и обновляет значение цены после скидки
function setPrice ($id, $price) {
	if (is_numeric($id) && is_numeric($price)) {
		mysql_query("
					UPDATE
						`catalog`
					SET
						`price` = '{$price}'
					WHERE
						`id` = '{$id}'
					LIMIT 1
				");

				$sel = mysql_query("SELECT `price`, `discount_type`, `discount_value` FROM `catalog` WHERE `id` = '{$id}'");
		if ($res = mysql_fetch_assoc($sel)) {
			$priceAfter = getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']);
			mysql_query("
				UPDATE
					`catalog`
				SET
					`price_after_discount` = '{$priceAfter}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
		}
		return true;
	} else {
		return false;
	}
}
//Возвращает стоимость товара без учета скидки
function getPrice($id) {
	if (!is_numeric($id)) return false;
	$temp = mysql_fetch_assoc(mysql_query("SELECT `price` FROM `catalog` WHERE `id` = '{$id}'"));
	return $temp['price'];
}
//Высчитывает стоимость с учетом скидки товара
function getPriceAfterDiscount($price, $discountType, $discountValue) {
	switch ($discountType) {
		case 'percent' : $price = round($price * ( 1 - $discountValue / 100)); break;
		case 'value' : $price = $price - $discountValue; break;
	}
	return $price;
}
//Возвращает "%" или "руб" в зависимости от типа скидки, либо пустую строку если тип скидки не известен
function getDiscountTypeString($type) {
	switch($type) {
		case 'percent' : return '%';
		case 'value' : return 'руб';
		default : return '';
	}
}
//Возвращает количество процентов от суммы. Округление спешел фо скидка
function getPercentForDiscount($summ, $price) {
	if ($price <= $summ)
		return round(($price * 100 / $summ));
	else 
		return 100;
}
//Высчитывает стоимость с учетом глобальной скидки
function getGlobalDiscount($count, $summ) {
	//Скидка по умолчанию. Т.е. без скидки
	$discount = array('type' => 'value', 'value' => 0);
	
	$sel = mysql_query("SELECT * FROM `order_discount_rules` ORDER BY `order` DESC");
	if (mysql_num_rows($sel) > 0) {
		while ($cond = mysql_fetch_assoc($sel)) {
			//Левая часть условия (по количеству товаров)
			switch ($cond['products_condition']) {
				case '>' : $firstCond = ($count > $cond['products_count']); break;
				case '>=' : $firstCond = ($count >= $cond['products_count']); break;
				case '<' : $firstCond = ($count < $cond['products_count']); break;
				case '<=' : $firstCond = ($count <= $cond['products_count']); break;
				case '=' : $firstCond = ($count == $cond['products_count']); break;
				default  : $firstCond = true; break;
			}
			
			//Правая часть условия (с суммой заказа)
			switch ($cond['price_condition']) {
				case '>' : $secondCond = ($summ > $cond['price_summ']); break;
				case '>=' : $secondCond = ($summ >= $cond['price_summ']); break;
				case '<' : $secondCond = ($summ < $cond['price_summ']); break;
				case '<=' : $secondCond = ($summ <= $cond['price_summ']); break;
				case '=' : $secondCond = ($summ == $cond['price_summ']); break;
				default  : $secondCond = true; break;
			}
			
			if ($firstCond && $secondCond) {
				return array('type' => $cond['discount_type'], 'value' => $cond['discount_value'] );
			}
		}
	}
	return $discount;
}
//Добавление значения параметра
function addAvailableParam($params) {
	$paramId = $params['paramId'];
	$catId = $params['catId'];
	$value = $params['value'];
	$value_float = $params['valueFloat'];
	
	if (!is_numeric($paramId) || !is_numeric($catId)) return false;
	//Приходится прибегать к псевдо-Replace дабы не плодить повторные параметры (нужно в основном для range)
	//см уникальные ключи
		mysql_query("
			INSERT INTO
				`params_available_values`
			SET
				`param_id` = '{$paramId}',
				`cat_id` = '{$catId}',
				`value` = '{$value}',
				`value_float` = '{$value_float}'
		");
	if (mysql_affected_rows() <= 0) {
		$findParamId = fetchOne("
			SELECT
				`id`
			FROM
				`params_available_values`
			WHERE
				`param_id` = '{$paramId}' AND
				`cat_id` = '{$catId}' AND
				`value` = '{$value}' AND
				`value_float` = '{$value_float}'
			LIMIT 1
		");
	} else {
		$findParamId = mysql_insert_id();
	}
	return $findParamId;
}
//Удаляет параметр из доступных
function deleteAvailableParam($paramId) {
	if (!is_numeric($paramId)) return false;
	mysql_query("
				DELETE FROM
					`params_available_values`
				WHERE
					`id` = '{$paramId}'
				LIMIT 1
			");
	return mysql_affected_rows > 0;
}
//Линкует параметр с категорией
function linkParamAndCat($paramId, $catId) {
	if (!is_numeric($paramId) || !is_numeric($catId)) return false;
	mysql_query("
		INSERT INTO
			`params_cat_links`
		SET
			`param_id` = '{$paramId}',
			`cat_id` = '{$catId}'
		");
	return true;
}
//Отвязка параметра от категории
function unlinkParamAndCat($paramId, $catId) {
	if (!is_numeric($paramId) || !is_numeric($catId)) return false;
	mysql_query("
		DELETE FROM
			`params_cat_links`
		WHERE
			`param_id` = '{$paramId}'
		AND
			`cat_id` = '{$catId}'
		");
	return true;
}
//Удаляет параметр из категории, сносит все связи товаро и категориц, связанных с этим параметром
function deleteParamFromCat($catId, $paramIds) {
	if (!is_numeric($catId)) return false;
	
	$paramIds = (array) $paramIds;
	
	//Удаляем связку и подчищаем следы
	
	//Выбираем id всех возможных значений параметра
	$paramsAvailableToDel = array();
	$sel = mysql_query("
		SELECT
			*
		FROM
			`params_available_values`
		WHERE
			`cat_id` = '{$catId}'
			AND
			`param_id` IN ('" . implode("', '", $paramIds) . "')
		");
	while ($res = mysql_fetch_assoc($sel)) {
		$paramsAvailableToDel[] = $res['id'];
	}
	//Удаляем связки этих значений с товарами
	if (count($paramsAvailableToDel) > 0) {
		mysql_query("
		DELETE FROM `params_catalog_links` WHERE `param_id` IN ('" . implode("', '", $paramsAvailableToDel) . "')
		");
	}
	//Удаляем возможные значения параметров
	mysql_query ("
		DELETE FROM `params_available_values` 
		WHERE
			`cat_id` = '{$catId}'
			AND
			`param_id` IN ('" . implode("', '", $paramIds) . "')
		");
	//Удаляем связки категорий и параметров
	mysql_query ("
		DELETE FROM
			`params_cat_links`
		WHERE
			`cat_id` = '{$catId}'
			AND
			`param_id` IN ('" . implode("', '", $paramIds) . "')
	");
}

//
function getParamInfo($paramId) {
	if (!is_numeric($paramId)) return false;
	return sqlFetch("SELECT * FROM `params_available` WHERE `id` = '{$paramId}'");
}
//Возвращает список доступных параметров в категории
//Параметры и доступные значения этих параметров
function getParamsInCat($id) {
	$info = array();
	
	$sel = mysql_query("
		SELECT
			*
		FROM 
			params_cat_links, params_available
		WHERE
			params_available.id = params_cat_links.param_id 
			AND
			cat_id = '{$id}'
	");
	
	while ($res = mysql_fetch_assoc($sel)) {
		$selParams = mysql_query("
			SELECT 
				*
			FROM
				params_available_values
			WHERE
				cat_id = '{$id}'
				AND
				param_id = '{$res['param_id']}'
		");
		$params = array();
		while ($param = mysql_fetch_assoc($selParams)) {
			$params[] = $param;
		}
		$info[] = array(
			'info' => $res,
			'params' => $params
		);
	}
	return $info;
}

//Возвращает только те параметры (размер, производитель) которые привязаны к категории,
//и только те значения парамеров у товара (45, 46, Россия), которые привязаны хотя бы к одному товару
function getRealParamsAndValuesInCat($id) {
	$info = array();
	
	$sel = mysql_query("
		SELECT
			*
		FROM 
			`params_cat_links`, `params_available`
		WHERE
			`params_available`.`id` = `params_cat_links`.`param_id`
			AND
			`params_cat_links`.`cat_id` = '{$id}'
			AND
			`params_cat_links`.`enable` = '1'
	");
	
	while ($res = mysql_fetch_assoc($sel)) {
		$selParams = mysql_query("
			SELECT 
				*
			FROM
				`params_available_values`
			WHERE
				`param_id` = '{$res['param_id']}'
				AND
				 (`cat_id` = '{$id}' OR `cat_id` = '0')
				AND
				`id` IN
					(SELECT `param_id` FROM `params_catalog_links` WHERE `catalog_id` IN 
						(SELECT `id` FROM `catalog` WHERE `cat` = '{$id}')
					)
		");
		$params = array();
		while ($param = mysql_fetch_assoc($selParams)) {
			$params[] = $param;
		}
		if (count($params) > 0) {
			$info[] = array(
				'info' => $res,
				'params' => $params
			);
		}
	}
	return $info;
}

function getRealParamsAndValuesInCats($ids) {
	$array = array();
	//Супер склейка с удалением дублей
	//Я не знаю как это работает, но это работает :)
	foreach ($ids as $id) {
		$temp = getRealParamsAndValuesInCat($id);
		foreach ($temp as $param) {
			$param_id = $param['info']['param_id'];
			$array[ $param_id ]['info'] = $param['info'];
			
			if ( isset($array[ $param_id ]['params']) ) {
				$array[ $param_id ]['params'] = array_merge($array[ $param_id ]['params'], $param['params']);
			} else {
				$array[ $param_id ]['params'] = $param['params'];
			}
		}
	}
	//Удаление дублей значений
	foreach ($array as &$param) {
		$temp = array();
		foreach ($param['params'] as $p) {
			$temp[$p['id']] = $p;
		}
		//array_merge - для сброса ключей
		$param['params'] = array_merge($temp);
	}
	return $array;
}
//Работа с параметрами товара
//Возвращает все возможные параметры
function getAvailableParams($onlyGloblals = false) {
	$params = array();
	
	$whereString = $onlyGloblals ? "WHERE `global` = '1'" : '';
	
	$sel = mysql_query("
		SELECT
			*
		FROM
			`params_available`
		{$whereString}
		");
	while ($line = mysql_fetch_assoc($sel)) {
		$params[] = $line;
	}
	return $params;
}
//Возвращает доступные параметры категории
//Без доступных значений
function getCatParams($id) {
	$params = array();
	
	$sel = mysql_query("
		SELECT
			*
		FROM
			`params_cat_links` as links, `params_available` as av
		WHERE
				( links.cat_id = '{$id}' OR links.cat_id = '0')
			AND
				links.enable = '1'
			AND
				av.id = links.param_id
		");
	while ($line = mysql_fetch_assoc($sel)) {
		$params[] = $line;
	}
	return $params;
}
//Возвращает доступные значения параметра в категории
function getCatAvailableValues($paramId, $catId, $productId = false) {
	$values = array();
	$global = fetchOne("SELECT `global` FROM `params_available` WHERE `id` = '{$paramId}'") == '1';
	
	if ($productId != false && $productId > 0) {
		//Супер запрос, вынимает все доступные параметры для категории
		//(учитывая таблицу params_cat_links, т.е. параметр должен быть соединен с категорией)
		//Плюс добавляет поле enabled = 1, в случае если товар имеет выбранное значение параметра
		if ($global) {
			$sql = "
				SELECT
				params_available_values.*,
				CASE
					WHEN params_catalog_links.catalog_id > '0'
						THEN 1
					ELSE	
						0
					END
				as `enable`
				FROM
					params_available_values 
				LEFT JOIN params_catalog_links ON
					params_available_values.id = params_catalog_links.param_id
					AND
					params_catalog_links.catalog_id = '{$productId}'
				WHERE
				params_available_values.cat_id = '0'
					AND
				params_available_values.param_id = '{$paramId}'	
			";
		} else {
			$sql = "
				SELECT
					params_available_values.*,
					
					CASE
						WHEN params_catalog_links.catalog_id > '0'
							THEN 1
						ELSE	
							0
						END
					as `enable`
				FROM
					params_available_values 
				INNER JOIN params_cat_links ON
					params_available_values.cat_id = '{$catId}'
					AND
					params_available_values.param_id = '{$paramId}'
					AND
					params_available_values.cat_id = params_cat_links.cat_id
					AND
					params_available_values.param_id = params_cat_links.param_id
				LEFT JOIN params_catalog_links ON
					params_available_values.id = params_catalog_links.param_id
					AND
					params_catalog_links.catalog_id = '{$productId}'
			";
		}
	} else {
		//Супер запрос, вынимает все доступные параметры для категории
		//(учитывая таблицу params_cat_links, т.е. параметр должен быть соединен с категорией)
		//Какую-то херню, по-моему, написал
		//это если параметр локальный
		if (!$global)
			$sql = "
				SELECT
					params_available_values.*
				FROM
					 params_available_values
				INNER JOIN params_cat_links ON
					params_available_values.cat_id = '{$catId}'
					AND
					params_available_values.param_id = '{$paramId}'
					AND
					params_available_values.cat_id = params_cat_links.cat_id
					AND
					params_available_values.param_id = params_cat_links.param_id
				";
		//А ежели глобальный
		else
			$sql = "
			SELECT
				params_available_values.*
			FROM
				params_available_values
			WHERE
				params_available_values.param_id = '{$paramId}'
				AND
				params_available_values.cat_id = '0'
		";
	}
	$sel = mysql_query($sql);
	while ($line = mysql_fetch_assoc($sel)) {
		$values[] = $line;
	}
	return $values;
}
//Удаляет значение параметра
function deleteParamValue($id) {
	if (!is_numeric($id)) return false;
	
	//Удаляем из доступных параметров
	mysql_query("
		DELETE FROM
			`params_available_values`
		WHERE
			`id` = '{$id}'
	");
	//Удаляем связки
	mysql_query("
		DELETE FROM
			`params_catalog_links`
		WHERE
			`param_id` = '{$id}'
	");
}
//Выбрать глобальные параметры
//добавление enable = 0|1 при $catId > 0
function getGlobalParams($catId = 0) {
	$params = array();
	
	if ($catId > 0) {
		$sql = "
			SELECT
				`params_available`.*,
				CASE
					WHEN `params_cat_links`.`enable` = '1'
						THEN 1
						ELSE 0
					END
				as enable
			FROM
				`params_available`
			LEFT JOIN
				`params_cat_links`
			ON
				`params_available`.`id` = `params_cat_links`.`param_id`
				AND
				`params_cat_links`.`cat_id` = '{$catId}'
			WHERE
				`global` = '1'
			ORDER BY `enable` DESC
		";
	} else {
		$sql = "	
			SELECT
				*
			FROM
				`params_available`
			WHERE
				`global` = '1'
		";
	}
	$sel = mysql_query($sql);
	while ($row = mysql_fetch_assoc($sel)) {
			$params[] = $row;
	}
	return $params;
}
//Выбрать локальные параметры
//добавление enable = 0|1
function getLocalParams($catId) {
	if (!is_numeric($catId)) return false;
	
	$params = array();
	
	$sql = "
		SELECT
			`params_available`.*,
			CASE
				WHEN `params_cat_links`.`enable` = '1'
					THEN 1
					ELSE 0
				END
			as enable
		FROM
			`params_available`
		INNER JOIN
			`params_cat_links`
		ON
			`params_available`.`id` = `params_cat_links`.`param_id`
			AND
			`params_cat_links`.`cat_id` = '{$catId}'
		WHERE
			`global` = '0'
		ORDER BY `enable` DESC
	";
	
	$sel = mysql_query($sql);
	while ($row = mysql_fetch_assoc($sel)) {
		$params[] = $row;
	}
	return $params;
}
//Выставляет параметры товару
function setProductsParams($productId, $paramsArray) {
	//Подготоваливаем и изменяем данные о параметрах товара
	mysql_query("DELETE FROM `params_catalog_links` WHERE `catalog_id` = '{$productId}'");
	
	//Массив с ID параметров
	$paramsIds = (array) $paramsArray;
	
	//Массив для подготовки sql запроса
	$paramsSqlIds = array();

	//Готовим sql запрос
	foreach ($paramsIds as $pid) {
		if (!is_numeric($pid)) continue;
		$paramsSqlIds[] = "('{$productId}', '{$pid}')";
	}
	//Добавляем параметры
	if (count($paramsSqlIds) > 0) {
		mysql_query("
			INSERT INTO
				`params_catalog_links` (`catalog_id`,`param_id`)
			VALUES 
			".implode(', ', $paramsSqlIds)
			);
	}
	return true;
}
//Перемещает товар и его параметры в другую категорию
//$id = id товара, $newId = id категории
function catalogChangeCat($id, $newId) {
	if (!is_numeric($id) || !is_numeric($newId)) return false;
	
	//// Определяем совпадающие параметры
	$oldId = mysql_fetch_assoc(mysql_query("SELECT * FROM `catalog` WHERE `id` = '{$id}'"));
	$oldId = $oldId['cat'];
	
	//Если категории совпадают, do nothing
	if ($oldId == $newId) return true;
	
	//Выбираем старые параметры
	$oldParams = array();
	$sel = mysql_query("SELECT * FROM `params_cat_links` WHERE `cat_id` = '{$oldId}'");
	while ($res = mysql_fetch_assoc($sel)) {
		$oldParams[] = $res['param_id'];
	}
	//Выбираем новые параметры
	$newParams = array();
	$sel = mysql_query("SELECT * FROM `params_cat_links` WHERE `cat_id` = '{$newId}'");
	while ($res = mysql_fetch_assoc($sel)) {
		$newParams[] = $res['param_id'];
	}
	//Находим одинаковые параметры
	$paramsEqual = array_merge(array_intersect($oldParams, $newParams));

	//// Удаляем в params_catalog_links связи с несовпадающими параметрами
	if ( count($paramsEqual) > 0) {
		//Если сопадение по параметрам есть, удаляем связи значений для несовпадающих параметров
		mysql_query("
			DELETE FROM 
				`params_catalog_links`
			WHERE
				`catalog_id` = '{$id}'
				AND
				`param_id` IN 
				(SELECT id FROM `params_available_values` WHERE `param_id` NOT IN ('" . implode(",", $paramsEqual) . "'))
		");
	} else {
		//Если вообще ни один параметр не совпадает, удаляем все значения
		mysql_query("
			DELETE FROM 
				`params_catalog_links`
			WHERE
				`catalog_id` = '{$id}'
		");
	}
	
	//// Выбираем id оставшихся значений параметров, и в params_available_values и дублируем их, но со значением другой категории
	
	$sel = mysql_query("
		SELECT
			`params_available_values`.*,
			`params_available_values`.`param_id` as `param_type_id`,
			`params_catalog_links`.`param_id` as `catalog_param_id`
		FROM
			`params_catalog_links`
		INNER JOIN `params_available_values`
		ON `params_available_values`.`id` = `params_catalog_links`.`param_id`
		WHERE
			`catalog_id` = '{$id}'
		");
	//echo mysql_error();
	//для каждого значения параметра
	while ($row = mysql_fetch_assoc($sel)) {
		
		//Проверяем, есть ли уже значение параметра, которое мы хотим добавить `params_available_values`
		$ssel = mysql_query("
			SELECT
				*
			FROM
				`params_available_values`
			WHERE
				`param_id` = '{$row['param_type_id']}' AND
				`value` = '{$row['value']}' AND
				`cat_id` = '{$newId}'
		");

		if ($srow = mysql_fetch_assoc($ssel) ) {
			//Если такое значение есть, то перекидываем id в связке на это значение
			$newParamAvailableId = $srow['id'];
		} else {
			//Ну а если нет, то дублируем
			mysql_query("
				INSERT INTO
					`params_available_values`
				SET
					`param_id` = '{$row['param_type_id']}',
					`value` = '{$row['value']}',
					`cat_id` = '{$newId}'
			");
			$newParamAvailableId = mysql_insert_id();
		}
		//Производим перелинковку
		mysql_query("
				UPDATE
					`params_catalog_links`
				SET
					`param_id` = '{$newParamAvailableId}'
				WHERE
					`catalog_id` = '{$id}' AND
					`param_id` = '{$row['catalog_param_id']}'
			");
	}
	
	
	//Изменяем категорию товара 
	mysql_query("
		UPDATE
			`catalog`
		SET
			`cat` = '{$newId}'
		WHERE
			`id` = '{$id}'
	");
	
	mysql_query("DELETE FROM 
						`catalog_2_cat`
				 WHERE 
						`catalog_id` = {$id}
				 AND
						`cat_id` = {$oldId}");
	
	mysql_query("INSERT INTO
						`catalog_2_cat`
				(`catalog_id`, `cat_id`) 
				VALUES
				({$id}, {$newId})");
				
	//Выписываем товар из тега
	unlinkCatalogAndTags($id);

	return true;
}
function query_cat_info($_id) {

	global $id;
	global $checkis;

	$checkis = 'adada';

	$id = $_id;

	return $id;
}
//Удаляет товар из магазина
//И картинки к нему и характеристики товара, и связки с псевдокатегориями
function deleteProduct($id) {
	if (mysql_fetch_assoc(mysql_query("SELECT `id` FROM `catalog` WHERE `id` = '{$id}'"))) {
		
		//Удалить доп фото если есть
		$sel = mysql_query("SELECT * FROM `foto` WHERE `catalog_id` = '{$id}'");
		while ($line = mysql_fetch_assoc($sel)) {
			deleteExtraPhoto($line['id']);
		}
		//Удалить основное фото
		deleteMainPhoto($id);
		//Удалить все значения параметров товара
		mysql_query("DELETE FROM `params_catalog_links` WHERE `catalog_id` = '{$id}'");
		//Удаляем слинковки тегов\псевдокатегорий
		unlinkCatalogAndTags($id);
		//Удалить из базы
		mysql_query("DELETE FROM `catalog` WHERE `id` = '{$id}'");
		mysql_query("DELETE FROM `catalog_2_cat` WHERE `catalog_id` = '{$id}'");
		
		return true;
	} else {
		return false;
	}
}
//Функции для работы с изображениями
//$newName без расширения
function saveMainImage($postFile, $newName) {
	
	$id = $newName;
	//Оригинал
	$imgPath = getImagePath('product_original');
	$sFileDest = $id;
	$iWidthDest = null;
	$iHeightDest = null;
	$iResizeMode = 1;
	$originalFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode);
	
	//Превью в карточке товара
	$imgPath = getImagePath('product_preview');
	$sFileDest = $id;
	$size = getConfigImageSize('product_preview');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_preview');
	$bigFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	//Medium фотки
	$imgPath = getImagePath('product_medium');
	$sFileDest = $id;
	$size = getConfigImageSize('product_medium');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_medium');
	$mediumFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	//Small фотки
	$imgPath = getImagePath('product_small');
	$sFileDest = $id;
	$size = getConfigImageSize('product_small');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_small');
	$smallFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	return !($bigFile == false || $mediumFile == false || $smallFile == false);
}
//Сохранение дополнительного фото
function saveExtraPhoto($postFile, $catalogId) {

	//Добавляем запись в базу и узнаем id будущей картинки
	mysql_query ("
		INSERT INTO
			`foto`
		SET
			`catalog_id` = '{$catalogId}'
		");
	$imgId = mysql_insert_id();
	
	//Большие картинки
	$imgPath = getImagePath('product_extra_original');
	$sFileDest = $imgId;
	$iWidthDest = null;
	$iHeightDest = null;
	$iResizeMode = 1;
	uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode);
	
	//Превью
	$imgPath = getImagePath('product_extra_preview');
	$sFileDest = $imgId;
	$size = getConfigImageSize('product_extra_preview');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_extra_preview');
	uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	//Средние
	$imgPath = getImagePath('product_extra_medium');
	$sFileDest = $imgId;
	$size = getConfigImageSize('product_extra_medium');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_extra_medium');
	uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	return $imgId;
}
//Удаление дополнительного фото
function deleteExtraPhoto($id) {
	
	mysql_query("DELETE FROM `foto` WHERE `id` = '{$id}'");
	@unlink(getImagePath('product_extra_original').$id.".jpg");
	@unlink(getImagePath('product_extra_preview').$id.".jpg");
	return ;
}
function deleteMainPhoto($catalogId) {
	//Оригинал
	@unlink(getImagePath('product_original').$catalogId.'.jpg');
	//Превью в карточке товара
	@unlink(getImagePath('product_preview').$catalogId.'.jpg');
	//Medium фотки
	@unlink(getImagePath('product_medium').$catalogId.'.jpg');
	//Small фотки
	@unlink(getImagePath('product_small').$catalogId.'.jpg');
}

function deleteCatImages($id) {
	@unlink(getImagePath('cats').$id.'.jpg');
	@unlink(getImagePath('cats_menu').$id.'.jpg');
}

//Выбирает список статей в категории
function getCatArticles($id) {
	if (!is_numeric($id)) return false;
	$data = array();
	
	$sel = mysql_query("
		SELECT
			*
		FROM
			`articles`
		WHERE
			`cat_id` = '{$id}'
	");
	while ($row = mysql_fetch_assoc($sel)) {
		$data[] = $row;
	}
	return $data;
}
//Возвращает все статьи
function getAllArticles($table = "articles", $order = "ORDER BY title") {
	$articles = array();
	$sel = mysql_query("SELECT * FROM {$table} {$order}");
	while ($row = mysql_fetch_assoc($sel)) {
		$articles[ $row['id'] ] = $row;
	}
	return $articles;
}
//получает данные о статье (саму статью)
function getArticle($id, $forceChpu = false, $table = "articles") {
	if (is_numeric($id) && !$forceChpu) {
		$sel = mysql_query("SELECT * FROM {$table} WHERE id = {$id}");
	} else {
		$chpu = mysql_real_escape_string($id); // rly? 
		$sel = mysql_query("SELECT * FROM {$table} WHERE `chpu` = '{$id}'");
	}
	return mysql_fetch_assoc($sel);
}
//Удаляет статью
function deleteArticle($id) {
	if (!is_numeric($id)) return false;
	mysql_query("
		DELETE FROM
			`articles`
		WHERE
			`id` = '{$id}'
	");
	return mysql_affected_rows() > 0;
}
//Возвращает массив товаров в корзине. С информации о количестве товара
//Размер изображения
//$forWeb если тру, выдает обрезанные данные о товаре (для использование js на странице) и конвертит буковки в утф
function getProductsInBasket($imageType = 'medium', $forWeb = false) {
	$cart = $_COOKIE['cart_products'];
	       
        
	//Распарсим список товаров в корзине
	$productsInCart = array();
	
	$temp = explode('|', $cart);
                
	foreach ($temp as $t) {
		$productTemp = explode(':', $t);
		if (is_numeric($productTemp[0]) && ($productTemp[0] > 0) && ($productTemp[1] > 0) && is_numeric($productTemp[1])) {
			$productsInCart[ $productTemp[0] ] = array(
				'id' => $productTemp[0],
				'amount' => $productTemp[1]
				);
		}
	}
	
	//Выберем товары для вывода
	$productsIds = array();
	foreach ($productsInCart as $p) {
		$productsIds[] = $p['id'];
	}
	$products = getProducts(false, array('id' => $productsIds));
	$products = $products['products'];
	
	//Сумма заказа не учитывая главную скидку при оформлении заказа
	$productsCount = count($products);
	
	for ($i = 0; $i < $productsCount; $i++) {
		if ($forWeb) {
			$products[$i] = array(
				'id' => $products[$i]['id'],
				// 'title' => iconv('windows-1251', 'utf-8', $products[$i]['title']),
				'title' => $products[$i]['title'],
				'price' => $products[$i]['price_after_discount'],
				'link' => getTemplateLink($products[$i], 'catalog'),
				'img_src' => getImageWebPath('product_' . $imageType) . $products[$i]['id'] . '.jpg'
			);
		}
		$products[$i]['amount'] = $productsInCart[ $products[$i]['id'] ]['amount'];
	}
	
	return $products;
}

// -------------------------------------------------------------------------------- Отзывы ------------------------------------------------------------------------
function addFeedBack() {
	//	Валидация формы
	if (
		isset($_POST['username']) &&
		!isLinksContain($_POST['username']) &&
		isset($_POST['usermail']) &&
		!isLinksContain($_POST['usermsg']) &&
		isset($_POST['usermsg']) &&
		preg_match('/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i', $_POST['usermail'])
		) {
		if (preg_match('/([<>\[\]]+)|(http:?\/?\/?)/m', $_POST['usermsg'])) {
			header('Location: /page/spam-filtr.html');
		} else {
			$date = date('d') . '.' . date('m') . '.' . date('Y');
			$uname = htmlspecialchars($_POST['username']);
			$umail = htmlspecialchars($_POST['usermail']);
			$umsg = htmlspecialchars($_POST['usermsg']);
			$uname = utf8_to_win($uname);
			$umail = utf8_to_win($umail);
			$umsg = utf8_to_win($umsg);
			$query = "INSERT INTO `shop_otzyv` (`id`, `date`, `name`, `mail` , `des`, `otvet`, `confirm`) VALUES (NULL, '" . $date . "' , '" . $uname . "', '" . $umail . "', '" . $umsg . "', '', '0')";
			$result = @mysql_query($query);
			if (mysql_affected_rows() > 0) {
				return "OK";
			} else {
				return NULL;
			}
		}
	} else {
		//	Форма не заполнена
		return NULL;
	}
}

function getMainBanners($all = false) {
	$data = array();

	$sel = mysql_query("SELECT * FROM `banners_main`" . ($all ? '' : " WHERE `active` = '1'") . " ORDER BY `sort` ASC, `id` ASC");
	while ($res = mysql_fetch_assoc($sel)) {
		$res['image_src'] = getImageWebPath('banner_main') . $res['id'] . '.jpg';
		$res['image_src_preview'] = getImageWebPath('banner_main') . $res['id'] . '_s.jpg';
		$data[] = $res;
	}

	return $data;
}

function getMainBanner($id) {
	if (!is_numeric($id))
		return false;
	return sqlFetch("SELECT * FROM `banners_main` WHERE `id` = '{$id}'");
}

//-------------- Конвертилка UTF8 в 1251
function utf8_to_win($string) {
	for ($c = 0; $c < strlen($string); $c++) {
		$i = ord($string[$c]);
		if ($i <= 127)
			@$out .= $string[$c];
		if (@$byte2) {
			$new_c2 = ($c1 & 3) * 64 + ($i & 63);
			$new_c1 = ($c1 >> 2) & 5;
			$new_i = $new_c1 * 256 + $new_c2;
			if ($new_i == 1025) {
				$out_i = 168;
			} else {
				if ($new_i == 1105) {
					$out_i = 184;
				} else {
					$out_i = $new_i - 848;
				}
			}
			@$out .= chr($out_i);
			$byte2 = false;
		}
		if (($i >> 5) == 6) {
			$c1 = $i;
			$byte2 = true;
		}
	}
	return $out;
}
//определяет, есть ли компенсация для товара
function isCompensate($item_id) {
	if (is_numeric($item_id)){
		$query = "SELECT  `cat`.`compensation` 
			  FROM `catalog` 
			  INNER JOIN `cat` 
			  ON `catalog`.`cat` = `cat`.`id` 
			  WHERE `catalog`.`id` = {$item_id} 
			  LIMIT 1";
		$res = mysql_fetch_assoc(mysql_query($query));
		return $res['compensation'];
	}
	return FALSE;
}

//----------------- Mail на отзывы
function feedbackSendMail($username, $usermail, $usermsg) {
	if ($usermail != '') {
		$subject = 'Интернет магазин Мед-Сердце - Ваш отзыв отправлен.';
		$mailbody = ('
			<h2>Уважаемый, ' . $username . ', мы благодарим Вас за отзыв.<br />
			Он будет опубликован после проверки модератором.</h2>
			&nbsp;<br />
			Вы написали:<br />"' . $usermsg . '"<br />
			<a href="http://med-serdce.ru/feedback/">Перейти к страничке отзывов интернет магазина Мед-Сердце.ру</a>.
		');
		mail($usermail, $subject, $mailbody, join("\r\n", array(
					"From: info@med-serdce.ru",
					"Reply-To: info@med-serdce.ru",
					"Content-type: text/html;\n\t charset=windows-1251",
					"X-Mailer: PHP/" . phpversion()))
		);
	}
}

// ------------------  Селект отзывов
function selectLastFeedback() {
	$query = "SELECT `name`, `date`, `des` FROM `shop_otzyv` WHERE `confirm`=1 ORDER BY `id` DESC LIMIT 2";
	$result = @mysql_query($query);
	$lastFeed = array();
	$cnt = 0;
	while ($row = @mysql_fetch_assoc($result)) {
		$lastFeed[$cnt]['name'] = $row['name'];
		$lastFeed[$cnt]['date'] = $row['date'];
		$lastFeed[$cnt]['des'] = $row['des'];
		$cnt++;
	}
	return $lastFeed;
}

function menu_createArray($msql_result, $no_group = false) {

	$arr = Array();
	$cur_group = -1;
	while ($line = mysql_fetch_assoc($msql_result)) {
		if ($line['is_group']) {
			if ($cur_group != $line['id']) {
				if (!$no_group) {

				}
				$cur_group = $line['id'];
			}
		} else {
			$arr[] = Array();
			$arr[count($arr) - 1][0] = $line['id'];
			$arr[count($arr) - 1][1] = $line['cat_id'];
			$arr[count($arr) - 1][2] = $cur_group;
		}
	}
	return $arr;
}

function multi_array_search($needle, $haystack) {

	foreach ($haystack as $key => $value) {
		//print_r($value);
		foreach ($value as $nkey => $nvalue) {
			if ($nvalue == $needle) {
				return 1;
			}
		}
	}

	return 0;
}

function menu_getMenu($_cat_id, $menuar) {
	$s_result = multi_array_search($_cat_id, $menuar);
	//echo $s_result;
	if (!$s_result) {
		$query = "select pod from cat where id=$_cat_id";
		$msql_result = mysql_query($query) or die(mysql_error());
		if ($line = mysql_fetch_assoc($msql_result)) {
			//echo "\n".$line['pod'];
			return menu_getMenu($line['pod'], $menuar);
		} else {
			return null;
		}
	} else {
		return $_cat_id;
	}
}

function menu_getActiveMenu($_cat_id) {

	//echo 1;
	$mysql_result = zh_getMenuResource();


	$menuar = menu_createArray($mysql_result, true);

	//print_r($menuar);
	$rcat_id = menu_getMenu($_cat_id, $menuar);

	if (!empty($rcat_id)) {
		return $rcat_id;
	} else {
		return $_cat_ids;
	}
}

// --------------------------------------------------------------------------------  ДЛЯ ПОИСКА YANDEX-XML ------------------------------------------------------------------------
//----------------- Выделение ключевых слов
function highlight_words($node) {
	$stripped = preg_replace('/<\/?(title|passage)[^>]*>/', '', $node->asXML());
	return str_replace('</hlword>', '</strong>', preg_replace('/<hlword[^>]*>/', '<strong>', $stripped));
}

//----------------------------------------------- Поиск картинки по ЧПУ товара или категории
function getInfoByChPU($url) {
	//	Если товар
	$result = array();
	if (substr_count($url, 'catalog')) {
		$parts = explode('/', $url);
		$last = str_replace('.html', '', $parts[count($parts) - 1]);
		$mquery = "SELECT `id`, `price` FROM `catalog` WHERE `chpu`='" . $last . "' LIMIT 1";
		$mresult = @mysql_query($mquery);
		//	Если поиск нашел товар
		if (mysql_affected_rows() > 0) {
			$mrow = @mysql_fetch_assoc($mresult);
			$img_href = '/upload/hit/' . ($mrow['id']) . '.jpg';
			$result['img_href'] = $img_href;
			$result['price'] = $mrow['price'];
			return $result;
		} else {
			return NULL;
		}
		//	Если категория
	} else {
		$parts = explode('/', $url);
		$chpu = str_replace('.html', '', $parts[4]);
		$mquery = "SELECT `id` FROM `cat` WHERE `chpu`='" . $chpu . "' LIMIT 1";
		$mresult = @mysql_query($mquery);
		if (mysql_affected_rows() > 0) {
			$mrow = @mysql_fetch_assoc($mresult);
			$img_href = '/upload/cat/' . $mrow['id'] . '.jpg';
			$result['img_href'] = $img_href;
			$mresult = @mysql_query('SELECT * FROM `catalog` WHERE `cat`=' . $mrow['id']);
			if (mysql_affected_rows() > 0) {
				$result['count'] = mysql_num_rows($mresult);
			}
			return $result;
		}
	}
	return NULL;
}

/* 	Полезные регулярки:
  $short_des = preg_replace('/([\.\:\-\,\;\!\?])([a-zA-Zа-яА-Я])/', '$1 $2', $short_des); - пробелы после знаков препинания
  $short_des = preg_replace('</?(?!br)[^>]+\s?/?>'
, '', $short_des); - убрать все теги кроме <br />
 */

//------------------------------------------------ Обрезка строк до N символов не разрывая слов:
function crop_str($string, $limit) {
	$substring_limited = substr($string, 0, $limit);		//режем строку от 0 до limit
	return substr($substring_limited, 0, strrpos($substring_limited, ' '));	//берем часть обрезанной строки от 0 до последнего пробела
}



function partnerCounter($pid, $counterType = array('follows', 'orders')) {
	$query = "SELECT * FROM `partners` WHERE `id`=$pid";
	$result = mysql_query($query);
	if(mysql_affected_rows() > 0) {
		$partner = mysql_fetch_assoc($result);
		$partner[$counterType]++;
	}
	$query = "UPDATE `partners` SET `$counterType`={$partner[$counterType]} WHERE `id`=$pid";
	$result = mysql_query($query);
}

function setPartnerCookie() {
	$query = "SELECT * FROM `partners` WHERE `id`=".$_GET['p'];
	$result = mysql_query($query);
	if(mysql_affected_rows() >0) {
		setcookie('fpid', $_GET['p'], time()+2000000, '/');
		partnerCounter($_GET['p'], 'follows');
	}
}


//Just a functions
function slashes($string) {
	return (get_magic_quotes_gpc() ? stripslashes($string) : $string);
}
function schars($string) {
	return htmlspecialchars(slashes($string), ENT_QUOTES);
}
function iconv_deep($e1, $e2, $value) {
   if (is_array($value)) {
      $item = null;
      foreach ($value as &$item) {
         $item = iconv_deep($e1, $e2, $item);
      }
      unset($item);
   } else {
      if (is_string($value)) $value = mb_convert_encoding($value, $e2, $e1);
   }
   return $value;
}
function translitIt($str) 
{
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
		' ' => '-', ',' => '-', '.' => '-', '/' => '-',
		'\\' => '-', '+' => '-'
    );
    $str = strtr($str,$tr);
	return preg_replace("/(-)+/is", '-', $str);
}
function validateEmail($email) {
	return preg_match('/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i',$email);
}
function isLinksContain($text) {
	return strpos($text, 'http://') !== false || preg_match('/(<a[^>]*)href=(\"?)([^\s\">]+?)(\"?)([^>]*>)/ism', $text) > 0; //|| preg_match('/([<>\[\]]+)|(http:?\/?\/?)/m', $text) > 0;
}
function toForm($data) {
	return htmlspecialchars(slashes($data), ENT_QUOTES);
}
function tf($name, $default = '') {
	if (isset($_POST[$name]))
		return toForm($_POST[$name]);
	else
		return $default;
}
function sqlFetch($query) {
	return mysql_fetch_assoc(mysql_query($query));
}
function fetchOne($query) {
	$var = mysql_fetch_array(mysql_query($query));
	return $var[0];
}
function redirect($url = '/', $code = false) {
	switch ($code) {
		case 301 : header("HTTP/1.0 301 Moved Permanently", true, 301);
			break;
		case 404 : header("HTTP/1.0 404 Not Found", true, 404);
			break;
	}
	header("Location: {$url}");
	die();
}
//Очищает описание от тегов, пробелов и другой херни, чтобы потом убедится, пустое ли описание
function trimDesc($str) {
	$str = strip_tags($str);
	$str = str_replace('&nbsp;','', $str);
	$str = trim($str);
	return $str;
}
//Обрезает html текст до длинны $symbolsLimit
function cropText($desc, $symbolsLimit = 200) {
	//	Режем все теги и переносы строк, кроме <br>, <br />
	$tmp_short = preg_replace('/(<\/?(?!br)[^>]+\s?>|\n)/', '', html_entity_decode($desc));

	//	Заменяем все <br>, <br /> на перенос строки
	$tmp_short = preg_replace('/<\/?(?=br)[^>]+\s?>/', "\n", $tmp_short);

	//	Ищем первую точку в конце текста
	$lastDot = @strpos($tmp_short, ".", $symbolsLimit - 50);

	//	Если точка нашлась и не далее $symbolsLimit символов - отрезаем
	if (($lastDot) && ($lastDot < $symbolsLimit)) {
		$tmp_short = substr($tmp_short, 0, $lastDot) . ".";
		//	Если не нашлась - отрезаем как можем
	} else {
		$tmp_short = substr($tmp_short, 0, $symbolsLimit) . "...";
	}
	return $tmp_short;
}
//Возаращает короткое описание для товара. (используется в шаблоне)
//Возможно в будущем вынести конфиг из шаблона в функции серии getProduct, а пятым параметром сделать Array $config
function getShortDesc($productData, $cropType = SHORT_IF_EXISTS_ELSE_CROP, $symbolsLimit = 200) {
	switch($cropType) {
		//Нарезаем короткое описание строго из полного
		case CROP_FROM_FULL:
			return cropText($productData['des'], $symbolsLimit);
		break;
		//Если короткое описние есть, возвращаем его. Если нет, возвращаем пустоту.
		case SHORT_IF_EXISTS_ELSE_NONE:
			if (isset($productData['short_des'])) {
				return $productData['short_des'];
			} else {
				return false;
			}
		break;
		//Если короткое описание есть в базе, выводим его, если нет - нарезаем из полного описания
		case SHORT_IF_EXISTS_ELSE_CROP:
		default :
			if (isset($productData['short_des'])) {
				$tmp = trim(strip_tags($productData['short_des'])); //fix, when CEditor added empty tags (p, br or something)
				if (empty($tmp))
					return cropText($productData['des'], $symbolsLimit);
				else 
					return $productData['short_des'];
			} else {
				return false;
			}
	}
}
//Преобразует формат числа для красивого вывода
//Example: 1235 -> 1 235
function moneyFormat($number, $fractional=false) {
    if ($fractional) {
        $number = sprintf('%.2f', $number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1 $2', $number);
        if ($replaced != $number) {
            $number = $replaced;
        } else {
            break;
        }
    }
    return $number;
}
function declOfNum($number, $titles) {
    $cases = array (2, 0, 1, 1, 1, 2);
    return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
}


function getOrderById($orderId) {
	if (is_numeric($orderId)) {
		$query = "SELECT * FROM `orders` 
				  WHERE `id` = {$orderId} 
				  LIMIT 1";
		$row = mysql_fetch_assoc(mysql_query($query));

		if (!$row) 
			return false;
			
		$data = array();
		foreach ($row as $key => $value) {
			$data[$key] = $value;
		} 
		$orderInfo['details'] = $data;
		$price = unserialize($row['order_price']);
		$items = unserialize($row['products']);
		
		$productsInCart = array();
		foreach($items as $item) {
			$productsInCart[ $item['id'] ] = array('id' => $item['id'], 'amount' => $item['amount']);
		}
		
		$query = "
			SELECT
				*
			FROM
				`catalog`
			WHERE
				`id` IN ('". implode("','", array_keys($productsInCart)) ."')
		";
		$res = mysql_query($query);
		while ($line = mysql_fetch_assoc($res)) {
			$productsToResponse[] = array(
				'id' => $line['id'],
				'amount' => $productsInCart[ $line['id'] ]['amount'],
				'price' => $line['price'],
				'price_after_discount' => $line['price_after_discount'],
//				'converted_title' =>  iconv("windows-1251", "utf-8", $line['title']),
				'converted_title' =>   $line['title'],
				'title' => $line['title']
			);		
		}
		$orderCode = getOrderCode($orderId);

		$orderInfo['items'] = array(
				'orderId' => $orderId,
				'orderCode' => $orderCode,
				'price' => $price,
				'productsToResponse' => $productsToResponse
		);
		
		return $orderInfo;
	}	
	return false;
}

//Возвращает информацию о заказе для банка Тинькова/КупиВКредит
function getBase64Order($orderInfo) {
	$k = 0;
	$order = array();
	
	
	foreach ($orderInfo['items']['productsToResponse'] as $product) {
		//$order['items'][$k]['title'] = iconv("Windows-1251", "UTF-8", $product['title']);
		$order['items'][$k]['title'] = $product['title'];
		$query = "SELECT `c2`.`title` FROM `catalog` as `c1` INNER JOIN `cat` as `c2` ON `c1`.`cat` = `c2`.`id` 
					WHERE `c1`.`id` = {$product['id']} LIMIT 1";
		$res = mysql_fetch_assoc(mysql_query($query));
		// $category = iconv("Windows-1251", "UTF-8", trim($res['title']));
		$category =  trim($res['title']);
		$order['items'][$k]['category'] = $category;
		$order['items'][$k]['qty'] = $product['amount'];
		$order['items'][$k]['price'] = $product['price_after_discount'];
		$k++;
	}
		
	$fio = explode(' ', $orderInfo['details']['name']);
	switch (count($fio))  {
		case 3:
			$firstname = $fio[0];
			$middlename = $fio[1];
			$lastname = $fio[2];
			break;
		case 2: 
			$firstname = $fio[0];
			$middlename = ' ';
			$lastname = $fio[1];
			break;
		case 1:
			$firstname = $fio[0];
			$middlename = ' ';
			$lastname = ' ';
			break;
		default:
			$name = $middlename = $lastname = ' ';
			break;
	}
//	$order['details']['firstname'] = iconv("Windows-1251", "UTF-8", $firstname);
//	$order['details']['middlename'] = iconv("Windows-1251", "UTF-8", $middlename);
//	$order['details']['lastname'] = iconv("Windows-1251", "UTF-8", $lastname);
	$order['details']['firstname'] = $firstname;
	$order['details']['middlename'] =  $middlename;
	$order['details']['lastname'] = $lastname;
	$order['details']['email'] = $orderInfo['details']['email'];
	$order['details']['cellphone'] = $orderInfo['details']['phone'];
	$order['partnerId'] = Config::get('credit.partner_id');
	$order['partnerName'] = 'med-serdce.ru';
	$order['partnerOrderId'] = $orderInfo['items']['orderId'];
	$order['deliveryType'] = '';
	$order['discount'] = 
				($orderInfo['items']['price']['price_before_global_discount'] - 
				$orderInfo['items']['price']['price_after_global_discount']);
	return $base64 = base64_encode(json_encode($order));
}

function signMessage($message, $salt, $iterationCount = 1102) {
	$message = $message.$salt;
	$result = md5($message).sha1($message);
	for($i = 0; $i < $iterationCount; $i++)
	$result = md5($result);
	return $result;
}

/**
 *	Возвращает список избранных товаров 
 *	в порядке их добавления
 */
function getFavorites($favorites) {
	$favorites = array_unique($favorites);
	$in_string = implode(',', $favorites);
	$in_string = mysql_real_escape_string($in_string);

	$query = "SELECT * FROM `catalog`
			  WHERE `id` IN ({$in_string})
			  ORDER BY FIND_IN_SET(`id`, '{$in_string}')"; 
	
	$res = mysql_query($query);		
	
	while ($row = mysql_fetch_assoc($res))
	{
		$products[] = $row;
	}			  
	
	return $products;
}

/**
 *	Возвращает топ избранных товаров
 *	
 */
function getTopFavorites($n = 10) {
    if (isset($n) && is_int($n) && ($n > 0)) {
        $fav = array();
        $query = "SELECT * FROM `catalog` "
                . "ORDER BY `favorites_rating` DESC "
                . "LIMIT {$n}";
        $res = mysql_query($query);
        while ($row = mysql_fetch_assoc($res)) {
            $fav[] = $row;
        }
        return $fav;
    }
    return false;
}


/**
 *	Возвращает информацию для сниппета товара
 */
function getSnippet($product) {
	if (is_array($product)) {
		$snippet = "";
		//HTML коды спец символов
		$spec_chars = array('phone' => '&#9742;',
							'mobile_phone' => '&#128241;',	
							'tick' => '&#10004;',
							'plane' => '&#9992;',
							'star' => '&#10047;',
							'car' => '&#128663;',
							'money_bag' => '&#128176;',
							'money' => '&#128184;',
							'speech_balloon' => '&#128172;',
							'camera' => '&#128247;',
							);
		$title = trim($product['title']) . ' ';
		$credit = $spec_chars['tick'] . 'Покупка в Кредит ';
		$price = $spec_chars['star'] . 'Цена ' . trim($product['price_after_discount']) . ' руб. ';
		//$moscow_delivery = $spec_chars['tick'] . 'Доставка 250р по Москве ';
		
		if ($product['deliveryPrice'] !== false) {
			$moscow_delivery = ($product['deliveryPrice'] > 0) ? 
				$spec_chars['tick'] . 'Доставка ' . $product['deliveryPrice'] . ' руб. ' : 
				$spec_chars['tick'] . 'Бесплатная доставка по Москве ';
			} else {
			$moscow_delivery = $spec_chars['tick'] . 'Доставка по Москве от 300 руб ';
		}
		
		$global_delivery = $spec_chars['plane'] . 'Доставка по всей России ';
		$phone = $spec_chars['phone'] . trim(Config::get('site.phone')) . ' ';
		$mobile_phone = $spec_chars['phone']  . '8 (800) 555-62-50' . ' '; //вынести в конфиг
		
		$snippet .= $title;
		$snippet .= $credit;
		$snippet .= $price;
		
		if (!empty($product['iframe'])) {
			$video = $spec_chars['tick'] . 'Видео ';		
			$snippet .= $video;
		}
		
		if (count($product['feedback']) > 0)
			$snippet .= $spec_chars['tick'] . 'Отзывы ';
			
		if ($product['sale'] == 1) 
			$snippet .= $spec_chars['tick'] . 'Акция ';
			
		$snippet .= $moscow_delivery;
		$snippet .= $global_delivery;
		
		//if ($product['rests_main']['summ'] > 0 || $product['fake_in_stock'] == 1) 
		//	$snippet .= $spec_chars['tick'] . ' ' . 'Есть в наличии ';
		
		$snippet .= $phone;
		$snippet .= $mobile_phone;
		
		//echo strlen($snippet) . "</br>";
		//echo $snippet;
		return $snippet;
	}
	return false;
}

function isJson($string) {
 json_decode($string);
 return (json_last_error() == JSON_ERROR_NONE);
}

/*
  Roman 19 марта. 
*/
function paf($arr){
	echo "<pre style='color: #000; background-color: #fff; font-size: 10px;'>".print_r($arr,1)."</pre>";
}
function getNews($limit = "0, 3", $order = "time ASC", $select = "*"){
	$query = "SELECT $select FROM news ORDER BY $order LIMIT $limit";
	$row = mysql_query($query) or die(mysql_error());
	while ($o = mysql_fetch_assoc($row)) {
		$arr[] = $o;
	}
	return $arr;
} 									

function getDescription($id){
	$des = mysql_result(mysql_query("SELECT des FROM catalog WHERE id = '{$id}'"), 0);
	return $des;
}

// старая функция получение наличия на фронте
function getAmount_old($product_id) {
	$amount = mysql_result(mysql_query("SELECT amount FROM storage_rests WHERE catalog_id = '{$product_id}'"), 0);
	return $amount;
}

// получение наличия включая поставщиков
function getAmount($product_id) {
	$rests = getActiveStoragesAmount($product_id);
	$extra = (isset($rests[17]) ) ? $rests[17] : 0 ;
	$amount = $rests[1] + $extra ;
	//$amount = mysql_result(mysql_query("SELECT amount FROM storage_rests WHERE catalog_id = '{$product_id}'"), 0);
	return $amount;
}


function makeAutoreserve($orderId, $name, $adress, $comment, $products){
	// 1. Отключаем кэширование для SOAP. Если этого не сделать, 
	// функции веб-сервисов будут работать некорректно. 
	ini_set("soap.wsdl_cache_enabled", "0" );

	// 2. Устанавливаем soap-соединение 
	$client = new SoapClient("http://217.194.255.193:8080/work/ws/ws1.1cws?wsdl", 
		array( 
		'login' => 'WebServiceUser_3', 	//логин 1C
		'password' => 'npBKdW', 		//пароль 
		'trace' => true, 
		'features' => SOAP_USE_XSI_ARRAY_TYPE, 
		//'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 5 
		) 
	);
	$phone = '89'.rand(11,99).rand(103, 978).rand(10,99).rand(10,99);
	$json_info = '{
		"order_id" : 	"'.$orderId.'",
		"full_name": 	"'.$name.'",
		"address": 		"'.$adress.'",
		"phone_number": "'.$phone.'",
		"comment": 		"'.$comment.'",
		"lines": [';
		foreach ($products as $p) {
			$json_info .= '
				{
					"nomen_code":	"'.$p['nomencl_10med'].'",
					"amount"	: 	"'.$p['amount'].'",
					"price"		:	"'.$p['price'].'"
				},
			';
		}
	$json_info .= ']
	}';
	$params["key"] = "2CF71B5E-C389-465B-BFE0-04652BBE4D5C";  // ключ подразделения armed
	$params["json_info"] = $json_info;

	// 4. Выполняем операцию getPromoOrdersInfo 
	try{
		$result = $client->createOrderJSON($params); 
		// 6. Выводим результат на экран 
		// var_dump($result->return);
		$res = json_decode($result->return);
		// file_put_contents('/var/www/armed/data/www/armed-market.ru/func/log.txt', print_r($res,true), FILE_APPEND);
		if( $res->state === "reserved" ){ // резервирование прошло удачно
			// сохраняем id резерва из 1С в `orders`.`reserved`
			mysql_query('update `orders` set `reservedId`="'.$res->order_1cid.'" where `id`='.$orderId);
		}
	}catch(Exception $e){
		// echo '<pre>';
		// var_dump($e);
		// echo '</pre>';
	}
}

// стилизуем номер 
function getStyledPhoneNumb($numb){
	$pattern = '/(\+?[0-9]?[ ]?\(?[0-9]{3}\)?)[ ]?([0-9\-]+)/' ;
	$pattern2 = '/(\+?[0-9])?[ ]?\(?([0-9]{3})\)?/' ;
	$pattern3 = '/([0-9]{3})[ -]?([0-9]{2})[ -]?([0-9]{2})/' ;
	
	$numb = (string)$numb ;
	$result = array();
	if ( preg_match($pattern, $numb, $matches) ){
		
		preg_match($pattern2, $matches[1], $first_matches) ;
		preg_match($pattern3, $matches[2], $second_matches) ;
		
		$first_str = $first_matches[1] . ' (' . $first_matches[2] . ')' ;
		$second_str = $second_matches[1] . '-' . $second_matches[2] . '-' . $second_matches[3] ;
		
		$result['html'] = '<span>' . $first_str . '</span> ' . $second_str ;
		$result['numb'] = $first_str . ' ' . $second_str ;
		
		return $result ;
	} else {
		return '' ;
	}
}

function getRecommendations($prodId){
	$result = [];

	$recCount = 0;
	$count = 20;
	$notArr = [];
	if(is_array($prodId)){
		$recQ = mysql_query('SELECT `catalog_id2`,`s1`.`amount` as `a1`, `s2`.`amount` as `a2`, `count`, `catalog`.* FROM `same_products_final` LEFT JOIN `catalog` ON `catalog`.`id` = `same_products_final`.`catalog_id2` LEFT JOIN `storage_rests` as `s1` ON `s1`.`catalog_id` = `same_products_final`.`catalog_id2` and `s1`.`storage_id`=1
						LEFT JOIN `storage_rests` as `s2` ON `s2`.`catalog_id` = `same_products_final`.`catalog_id2` and `s2`.`storage_id`=2 WHERE `count` <> 1 AND `catalog_id1` in ('.implode(',',$prodId).') ORDER BY `count` DESC LIMIT '.$count);
	}else{
		$recQ = mysql_query('SELECT `catalog_id2`,`s1`.`amount` as `a1`, `s2`.`amount` as `a2`, `count`, `catalog`.* FROM `same_products_final` LEFT JOIN `catalog` ON `catalog`.`id` = `same_products_final`.`catalog_id2` LEFT JOIN `storage_rests` as `s1` ON `s1`.`catalog_id` = `same_products_final`.`catalog_id2` and `s1`.`storage_id`=1
						LEFT JOIN `storage_rests` as `s2` ON `s2`.`catalog_id` = `same_products_final`.`catalog_id2` and `s2`.`storage_id`=2 WHERE `count` <> 1 AND `catalog_id1` = '.$prodId.' ORDER BY `count` DESC LIMIT '.$count);
	}
	while ($recR = mysql_fetch_array($recQ)) {
		if ($recR['title'] != '') {
			$result[] = $recR;
			$notArr[] = $recR['id'];
			$recCount++;
		}
	}
	unset($recR);
	unset($recQ);
	if ($recCount < $count) {
		$limit = $count - $recCount;
	if(is_array($prodId)){
		foreach($prodId as $p){
			$notArr[] = $p;
		}
		$recQ = mysql_query('SELECT `same_products_cat`.`catalog_id`,`s1`.`amount` as `a1`, `s2`.`amount` as `a2`,`catalog`.* FROM `same_products_cat` LEFT JOIN `catalog` ON `catalog`.`id`=`same_products_cat`.`catalog_id` LEFT JOIN `storage_rests` as `s1` ON `s1`.`catalog_id` = `same_products_cat`.`catalog_id` and `s1`.`storage_id`=1
						LEFT JOIN `storage_rests` as `s2` ON `s2`.`catalog_id` = `same_products_cat`.`catalog_id` and `s2`.`storage_id`=2 WHERE `cat_id` in (select `cat` from `catalog` where `id` in ('.implode(',', $prodId).') ) AND `same_products_cat`.`catalog_id` NOT IN (' . implode(',', $notArr) . ')  ORDER BY `count` DESC LIMIT ' . $limit);
	 }else{
		$notArr[] = $prodId;
		$recQ = mysql_query('SELECT `same_products_cat`.`catalog_id`,`s1`.`amount` as `a1`, `s2`.`amount` as `a2`,`catalog`.* FROM `same_products_cat` LEFT JOIN `catalog` ON `catalog`.`id`=`same_products_cat`.`catalog_id` LEFT JOIN `storage_rests` as `s1` ON `s1`.`catalog_id` = `same_products_cat`.`catalog_id` and `s1`.`storage_id`=1
						LEFT JOIN `storage_rests` as `s2` ON `s2`.`catalog_id` = `same_products_cat`.`catalog_id` and `s2`.`storage_id`=2 WHERE `cat_id`=(select `cat` from `catalog` where `id`='.$prodId.' limit 1) AND `same_products_cat`.`catalog_id` NOT IN (' . implode(',', $notArr) . ')  ORDER BY `count` DESC LIMIT ' . $limit);
	}
	while ($recR = mysql_fetch_array($recQ)) {
			if ($recR['title'] != '') {
				$result[] = $recR;
			}
		}
	}
	unset($recR);
	unset($recQ);

	return $result;
}


function getRelatedProducts($productId){

    $res = mysql_query("SELECT relation.id as link_id , c.* 
                        FROM catalog_related as relation
                        LEFT JOIN catalog as c ON relation.related_product_id = c.id
                        WHERE relation.catalog_id = {$productId} ;");
    $result = array();

    while ($row = mysql_fetch_assoc($res)){
        $result[] = $row;
    }

    return $result;
}

// получение пути до инструкции
function getInstructionPath($product_id) {
    return  '/upload/files/' . $product_id . '/' ;
}

// инструкции - сохранение
function saveInstruction($file, $product_id){

    if ($file['error']) return false ;

    if (!isset($file['name']) || $file['tmp_name'] == '' ) return false ;

    $result = array();

    $path = $_SERVER['DOCUMENT_ROOT'] . getInstructionPath($product_id) ;

    if (!is_dir($path)) {
        mkdir($path, 0777) ;
    }
    $filename = $file['name'] ;
    $upload_name = $path . $filename ;

    if (!move_uploaded_file($file['tmp_name'], $upload_name)){
        $result['error'] = true ;
        $result['msg'] = "Не могу сохранить файл " . $file['tmp_name'] . " в " . $upload_name ;
    } else {
        $result['error'] = false ;
    }

    if (!$result['error']) {
        $q = "UPDATE `catalog` SET `instruction` = '{$filename}' WHERE `id` = {$product_id} LIMIT 1 ; " ;
        mysql_query($q) ;
    }
    return $result ;
}


// удаление инструкции
function deleteInstruction($id) {

    $instruction_name = mysql_fetch_assoc(mysql_query("SELECT id, instruction FROM `catalog` WHERE `id` = '{$id}'"));

    if ($instruction_name['instruction'] == '') {
        return false ;
    }

    $path = $_SERVER['DOCUMENT_ROOT'] . getInstructionPath($id) . $instruction_name['instruction'] ;
    if (file_exists($path)){
        @unlink($path) ;
    } else {
        return false ;
    }
    return true ;
}


// переводим список товаров в таблицу
function productInfoRestruction($productInfo){

    $array = productInfoParsing($productInfo);

    return productInfoRendering($array);
}

// парсим список характеристик товара и переводим в массив
function productInfoParsing($productInfo){
    $pattern = '/<li>\s(?<name>[^:]+):(?<value>[^<]+)<\/li>/u';

    preg_match_all($pattern, $productInfo, $matches);

    $result = array();

    foreach($matches[0] as $ind => $match){
        $result[] = array(
            'name' => trim($matches['name'][$ind]),
            'value' => $matches['value'][$ind],
        );
    }

    return $result;
}

// рендерим массив тех характеристик товара в виде таблицы
function productInfoRendering($array){

    if (count($array) < 1) return '' ;

    $html = '<table class="product_info">' ;

    foreach($array as $row) {
        $html .= '<tr><td>' . $row['name'] . '</td><td>' . $row['value'] . '</td></tr>';
    }

    $html .= '</table>';

    return $html ;
}


// провередение запроса оптового прайса
function makeWholeSalerOrder($orderInfo){

    $data['date'] = time();
    $data['name'] = mysql_real_escape_string($orderInfo['name']);
    $data['phone'] = mysql_real_escape_string($orderInfo['phone']);
    $data['email'] = mysql_real_escape_string($orderInfo['email']);
    $data['extra_information'] = mysql_real_escape_string("Заказ оптового прайса " . $orderInfo['extra_information']);

    $data['adress'] = '';
    $data['delivery_type'] = 1;
    $data['payment_type'] = 1;

    $data['products'] = serialize(array());
    $data['order_price'] = serialize(array());

    if (strlen($data['name']) >= 3 ) {

        $sqlParams = array();
        foreach ($data as $key => $value) {
            $sqlParams[] = "`{$key}` = '{$value}'";
        }

        mysql_query("
			INSERT INTO
				`orders`
				SET ".(implode(', ', $sqlParams))."
			");
        $orderId = mysql_insert_id();


        // посылаем письмо на почту
        $subject = 'Поступило новое оптовое предложение';
        $message = 'Имя:' . $data['name'] . '<br/>'.
            'Почта:' . $data['email'] . '<br/>'.
            'Телефон:' . $data['phone'] . '<br/>'.
            'Текст сообщения:' . $data['extra_information'] . '<br/>';

        $mail = sendMailPHPMailer($subject, $message, 'info@armed-market.ru');

        return array(
            'orderId'   => $orderId,
            'mail'      => $mail,
        );
    } else {
        return array(
            'error' => array(
                'code' => 2,
                'message' => 'empty main client info (name or phone)'
            )
        );
    }

}