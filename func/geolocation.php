<?php

/**
 * <pre>
 * Fringe Interactive
 * Kinetic Management System (KMS) v0.1.8 (beta)
 * Geolocation Component Executable Wrapper
 * Last Updated: 14.01.14 0:04
 * </pre>
 *
 * @author 		$Author: V. Nesterov $
 * @copyright		(c) 2014 Fringe Interactive
 * @license		http://www.kinetic.pro/info/license
 * @package		KMS
 * @link		http://www.kinetic.pro
 * @version		$Rev: 3 $
 *
 */
if (!defined('IN_KINETIC')) {
    define('IN_KINETIC', TRUE);
}

error_reporting('E_ALL, E_NOTICE');

mb_internal_encoding('utf-8');
// header('Content-Type: text/html; charset=utf-8');


// We are not in shell!
if (!defined('IN_KINETIC_SHELL')) {
    define('IN_KINETIC_SHELL', FALSE);
}

/**
 * Path to the front controller of Geolocation Component (this file)
 */
if (!defined('KMS_GLC_ROOT')) {
    define('KMS_GLC_ROOT', dirname(__FILE__) . '/');
    //echo 'Set init file dir: <br />' . KMS_GLC_ROOT . '<br /><br />';
}

/**
 * Path to the KMS Geolocation Component Directory
 */
if (!defined('KMS_GLC_CORE')) {
    define('KMS_GLC_CORE', KMS_GLC_ROOT . 'geolocation/');
    //echo 'Set core dir: <br />' . KMS_GLC_CORE . '<br /><br />';
}

/**
 * Path to the KMS Geolocation Regions Directory
 */
if (!defined('KMS_GLC_RGS')) {
    define('KMS_GLC_RGS', KMS_GLC_CORE . 'regions/');
    //echo 'Set regions dir: <br />' . KMS_GLC_RGS . '<br /><br />';
}

/**
 * Path to the Geolocation Component cache directory
 */
if (!defined('KMS_GLC_CACHE')) {
    define('KMS_GLC_CACHE', KMS_GLC_CORE . 'data_cache/');
    //echo 'Set cache dir: <br />' . KMS_GLC_CACHE . '<br /><br />';
}

/*
 *  Load the KMS Geolocation Component config file
 * -----------------------------------------------------------------------------
 */
$GLConfig = array();
$GLConfigFile = KMS_GLC_CORE . 'geolocation.config.php';
if (file_exists($GLConfigFile)) {
    require ( $GLConfigFile );
    //echo 'Get config: <br />' . $GLConfigFile . '<br /><br />';
}


/*
 * Get current php version
 */
$php_version = PHP_VERSION;
//echo 'Get current php version: <br />' . $php_version . '<br /><br />';

/*
 * Get current time
 */
$php_time = date('l jS \of F Y h:i:s A');
//echo 'Get current time: <br />' . $php_time . '<br /><br />';

/*
 *  Load the KMS Geolocation Component Class file
 * -----------------------------------------------------------------------------
 */
$GLClassFile = KMS_GLC_CORE . 'geolocation.class.php';
if (file_exists($GLClassFile)) {
    require ( $GLClassFile );
    //echo 'Get class: <br />' . $GLClassFile . '<br /><br />';
    $GLObj = new kmsGeolocation();
} else {
    exit();
}

/*
 *  Set UTF-8 data encoding
 * -----------------------------------------------------------------------------
 */

function to_utf8($in) {
    if (is_array($in)) {
	foreach ($in as $key => $value) {
	    $out[to_utf8($key)] = to_utf8($value);
	}
    } elseif (is_string($in)) {
	return iconv('CP1251', 'UTF8', $in);
    } else {
	return $in;
    } 
    return $out;
}

function isRobot() {

$_config_['robots'] = array(
    "abot",
    "dbot",
    "ebot",
    "hbot",
    "kbot",
    "lbot",
    "mbot",
    "nbot",
    "obot",
    "pbot",
    "rbot",
    "sbot",
    "tbot",
    "vbot",
    "ybot",
    "zbot",
    "bot.",
    "bot/",
    "_bot",
    ".bot",
    "/bot",
    "-bot",
    ":bot",
    "(bot",
    "crawl",
    "slurp",
    "spider",
    "seek",
    "accoona",
    "acoon",
    "adressendeutschland",
    "ah-ha.com",
    "ahoy",
    "altavista",
    "ananzi",
    "anthill",
    "appie",
    "arachnophilia",
    "arale",
    "araneo",
    "aranha",
    "architext",
    "aretha",
    "arks",
    "asterias",
    "atlocal",
    "atn",
    "atomz",
    "augurfind",
    "backrub",
    "bannana_bot",
    "baypup",
    "bdfetch",
    "big brother",
    "biglotron",
    "bjaaland",
    "blackwidow",
    "blaiz",
    "blog",
    "blo.",
    "bloodhound",
    "boitho",
    "booch",
    "bradley",
    "butterfly",
    "calif",
    "cassandra",
    "ccubee",
    "cfetch",
    "charlotte",
    "churl",
    "cienciaficcion",
    "cmc",
    "collective",
    "comagent",
    "combine",
    "computingsite",
    "csci",
    "curl",
    "cusco",
    "daumoa",
    "deepindex",
    "delorie",
    "depspid",
    "deweb",
    "die blinde kuh",
    "digger",
    "ditto",
    "dmoz",
    "docomo",
    "download express",
    "dtaagent",
    "dwcp",
    "ebiness",
    "ebingbong",
    "e-collector",
    "ejupiter",
    "emacs-w3 search engine",
    "esther",
    "evliya celebi",
    "ezresult",
    "falcon",
    "felix ide",
    "ferret",
    "fetchrover",
    "fido",
    "findlinks",
    "fireball",
    "fish search",
    "fouineur",
    "funnelweb",
    "gazz",
    "gcreep",
    "genieknows",
    "getterroboplus",
    "geturl",
    "glx",
    "goforit",
    "golem",
    "grabber",
    "grapnel",
    "gralon",
    "griffon",
    "gromit",
    "grub",
    "gulliver",
    "hamahakki",
    "harvest",
    "havindex",
    "helix",
    "heritrix",
    "hku www octopus",
    "homerweb",
    "htdig",
    "html index",
    "html_analyzer",
    "htmlgobble",
    "hubater",
    "hyper-decontextualizer",
    "ia_archiver",
    "ibm_planetwide",
    "ichiro",
    "iconsurf",
    "iltrovatore",
    "image.kapsi.net",
    "imagelock",
    "incywincy",
    "indexer",
    "infobee",
    "informant",
    "ingrid",
    "inktomisearch.com",
    "inspector web",
    "intelliagent",
    "internet shinchakubin",
    "ip3000",
    "iron33",
    "israeli-search",
    "ivia",
    "jack",
    "jakarta",
    "javabee",
    "jetbot",
    "jumpstation",
    "katipo",
    "kdd-explorer",
    "kilroy",
    "knowledge",
    "kototoi",
    "kretrieve",
    "labelgrabber",
    "lachesis",
    "larbin",
    "legs",
    "libwww",
    "linkalarm",
    "link validator",
    "linkscan",
    "lockon",
    "lwp",
    "lycos",
    "magpie",
    "mantraagent",
    "mapoftheinternet",
    "marvin/",
    "mattie",
    "mediafox",
    "mediapartners",
    "mercator",
    "merzscope",
    "microsoft url control",
    "minirank",
    "miva",
    "mj12",
    "mnogosearch",
    "moget",
    "monster",
    "moose",
    "motor",
    "multitext",
    "muncher",
    "muscatferret",
    "mwd.search",
    "myweb",
    "najdi",
    "nameprotect",
    "nationaldirectory",
    "nazilla",
    "ncsa beta",
    "nec-meshexplorer",
    "nederland.zoek",
    "netcarta webmap engine",
    "netmechanic",
    "netresearchserver",
    "netscoop",
    "newscan-online",
    "nhse",
    "nokia6682/",
    "nomad",
    "noyona",
    "nutch",
    "nzexplorer",
    "objectssearch",
    "occam",
    "omni",
    "open text",
    "openfind",
    "openintelligencedata",
    "orb search",
    "osis-project",
    "pack rat",
    "pageboy",
    "pagebull",
    "page_verifier",
    "panscient",
    "parasite",
    "partnersite",
    "patric",
    "pear.",
    "pegasus",
    "peregrinator",
    "pgp key agent",
    "phantom",
    "phpdig",
    "picosearch",
    "piltdownman",
    "pimptrain",
    "pinpoint",
    "pioneer",
    "piranha",
    "plumtreewebaccessor",
    "pogodak",
    "poirot",
    "pompos",
    "poppelsdorf",
    "poppi",
    "popular iconoclast",
    "psycheclone",
    "publisher",
    "python",
    "rambler",
    "raven search",
    "roach",
    "road runner",
    "roadhouse",
    "robbie",
    "robofox",
    "robozilla",
    "rules",
    "salty",
    "sbider",
    "scooter",
    "scoutjet",
    "scrubby",
    "search.",
    "searchprocess",
    "semanticdiscovery",
    "senrigan",
    "sg-scout",
    "shai'hulud",
    "shark",
    "shopwiki",
    "sidewinder",
    "sift",
    "silk",
    "simmany",
    "site searcher",
    "site valet",
    "sitetech-rover",
    "skymob.com",
    "sleek",
    "smartwit",
    "sna-",
    "snappy",
    "snooper",
    "sohu",
    "speedfind",
    "sphere",
    "sphider",
    "spinner",
    "spyder",
    "steeler/",
    "suke",
    "suntek",
    "supersnooper",
    "surfnomore",
    "sven",
    "sygol",
    "szukacz",
    "tach black widow",
    "tarantula",
    "templeton",
    "/teoma",
    "t-h-u-n-d-e-r-s-t-o-n-e",
    "theophrastus",
    "titan",
    "titin",
    "tkwww",
    "toutatis",
    "t-rex",
    "tutorgig",
    "twiceler",
    "twisted",
    "ucsd",
    "udmsearch",
    "url check",
    "updated",
    "vagabondo",
    "valkyrie",
    "verticrawl",
    "victoria",
    "vision-search",
    "volcano",
    "voyager/",
    "voyager-hc",
    "w3c_validator",
    "w3m2",
    "w3mir",
    "walker",
    "wallpaper",
    "wanderer",
    "wauuu",
    "wavefire",
    "web core",
    "web hopper",
    "web wombat",
    "webbandit",
    "webcatcher",
    "webcopy",
    "webfoot",
    "weblayers",
    "weblinker",
    "weblog monitor",
    "webmirror",
    "webmonkey",
    "webquest",
    "webreaper",
    "websitepulse",
    "websnarf",
    "webstolperer",
    "webvac",
    "webwalk",
    "webwatch",
    "webwombat",
    "webzinger",
    "wget",
    "whizbang",
    "whowhere",
    "wild ferret",
    "worldlight",
    "wwwc",
    "wwwster",
    "xenu",
    "xget",
    "xift",
    "xirq",
    "yandex",
    "yanga",
    "yeti",
    "yodao",
    "zao/",
    "zippp",
    "zyborg",
    "...."
);


	foreach ($_config_['robots'] as $spider) {

	    if (stripos($_SERVER['HTTP_USER_AGENT'], $spider) !== false) {
		return true;
	    }
	}

    return false;
}

function getFullUrl($s) {

    $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true : false;

    $sp = strtolower($s['SERVER_PROTOCOL']);

    $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');

    $port = $s['SERVER_PORT'];
    $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;

    $host = isset($s['HTTP_X_FORWARDED_HOST']) ? $s['HTTP_X_FORWARDED_HOST'] : isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : $s['SERVER_NAME'];

    return $protocol . '://' . $host . $port . $s['REQUEST_URI'];
}

// Get current data
$GLDataResult = to_utf8($GLObj->getRecord($GLObj->get_client_ip()));

//print_r($GLDataResult);
