<?php
require_once('classes/sphinxapi.php');
/**
 *  обертка ShinxClient для OlesyaCMS (работает на старом mysql драйвере)
 */
class mShinxClient extends SphinxClient
{

    private $host = 'localhost';
    private $mode = SPH_MATCH_EXTENDED;
    private $port = 3312;
    private $c;
    private $index_prefix;

    /**
     * инифиализация клиента  Sphinx
     */
    function __construct($index_prefix = '', $host = false, $port = false, $mode = false) {
        if ($host) {
            $this->host = $host;
        }
        if ($port) {
            $this->port = $port;
        }
        if ($mode) {
            $this->mode = $mode;
        }

        $this->index_prefix = $index_prefix;

        $this->c = new SphinxClient();
        $this->c->SetServer($this->host, $this->port);
        $this->c->SetMatchMode($this->mode);

        parent::__construct();
    }

    /**
     * поиск через клиент sphinx
     */
    public function mSearch($string, $index = false)
    {
        $result = $this->c->query($string, $index);
        //print_r($result);
        if ($result === false ) {
            echo "Query failed: " . $this->c->GetLastError() . ".\n";
        }

        return array_keys($result['matches']);
    }

    /**
     * вывод данных о товарах, найденных через поиск
     */
    public  function getProducts($string, $limit = false, $offset = 0, $index = false)
    {
        if ($index === false){
            $index = $this->index_prefix . '_product_index';
        }

        $result = implode(',', $this->mSearch($string, $index));
        $query = "SELECT `catalog`.*,
            (select SUM(d.`amount`) from `storage_rests` as d 
				left join `storages_new` as e on e.storage_id = d.storage_id 
				where e.is_active = 1 and d.catalog_id = `catalog`.id ) as `rests_main`
            FROM `catalog`
            WHERE `catalog`.`is_hide` = 0 AND `catalog`.`id` in ({$result}) 
            ORDER BY FIELD(id, {$result})";
        if ($limit) {
            $query .= " LIMIT {$limit}";
            $query .= " OFFSET {$offset}"; 
        }

        //echo $query;
        $result = @mysql_query($query);
        $products = [];
        while ($line = @mysql_fetch_assoc($result)) {
            $products[] = $line;
        }
        return $products;
    }

    /**
     * 
     */
    public function getCategories($string, $limit = false, $offset = 0, $index = false)
    {
        if ($index === false){
            $index = $this->index_prefix . '_category_index';
        }   
        $result = implode(',', $this->mSearch($string, $index));
        $query = "SELECT * FROM `cat` 
            WHERE `id` in ({$result})
            ORDER BY FIELD(id, {$result})";
        if ($limit) {
            $query .= " LIMIT {$limit}";
            $query .= " OFFSET {$offset}"; 
        }
        //echo $query;
        $result = @mysql_query($query);
        $categories = [];
        while ($line = @mysql_fetch_assoc($result)) {
            $categories[] = $line;
        }
        return $categories;

    }

    /**
     * 
     */
    public function getTags($string, $limit = false, $offset = 0, $index = false)
    {
        if ($index === false){
            $index = $this->index_prefix . '_tag_index';
        }
        $result = implode(',', $this->mSearch($string, $index));   
        $query = "SELECT * FROM `tags` 
            WHERE `id` in ({$result})
            ORDER BY FIELD(id, {$result})";
        if ($limit) {
            $query .= " LIMIT {$limit}";
            $query .= " OFFSET {$offset}"; 
        }
        //echo $query;
        $result = @mysql_query($query);
        $tags = [];
        while ($line = @mysql_fetch_assoc($result)) {
            $tags[] = $line;
        }
        return $tags;

    }

}