<?
function UniqGeneratePaginator($data, $qString = false, $onclick = false) {
	if ($data['pagesCount'] > 1) {
		echo '<div class="mm-paging-wrap">';
		if (isset($_GET['c']))
			$chpu = $_GET['c'];
		else {
			$chpu = $_POST['cat'];
		}
		$firstPage = getTemplateLink(Array('chpu' => $chpu), 'cat');

		if ($qString == false)
			$qString = new queryString();
		echo '<a href="'.$qString -> setParam('page','all').'" class="mm-all">Показать все »</a>';
		if ($data['currentPage'] != 1) {// стрелка влево
			if ($data['currentPage'] - 1 >= 1) {
				echo '<a href="' . $qString -> setParam('page', $data['currentPage'] - 1) . '" title="" ' . ($onclick ? 'onclick="return ' . str_replace('[page]', $data['currentPage'] - 1, $onclick) . '"' : '') . ' class="mm-prev">prev</a>';
			} else {
				$firstPage = str_replace('?page=1', '', $firstPage);
				echo '<a href="' . $firstPage . '" title="" ' . ($onclick ? 'onclick="return ' . str_replace('[page]', $data['currentPage'] - 1, $onclick) . '"' : '') . ' class="mm-all">Показать все »</a>';
			}
		}
		echo '<ul class="paging">';
		$s = $qString -> setParam('page', $data['currentPage'] - 1);
		for ($i = 1; $i <= $data['pagesCount']; $i++) {
			if ($i == $data['currentPage']) {//текущая страница
				echo '<li class="active"><a nohref>' . $i . '</a></li>';
			} else {
				if ($i == 1)
					echo '<li><a href="' . $firstPage . '" title=""' . ($onclick ? 'onclick="return ' . str_replace('[page]', $i, $onclick) . '"' : '') . '>' . $i . '</a></li>';
				else
					echo '<li><a href="' . $qString -> setParam('page', $i) . '" title=""' . ($onclick ? 'onclick="return ' . str_replace('[page]', $i, $onclick) . '"' : '') . '>' . $i . '</a></li>';
			}
		}
		echo '</ul>';
		if ($data['currentPage'] != $data['pagesCount'])//стелка вправо
			echo '<a href="' . $qString -> setParam('page', $data['currentPage'] + 1) . '" title="" class="mm-next next-page"' . ($onclick ? 'onclick="return ' . str_replace('[page]', $data['currentPage'] + 1, $onclick) . '"' : '') . ' class="mm-next">next</a>';
		echo '</div>';
	}
}
