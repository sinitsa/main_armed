<?php
//error_reporting(E_ALL ^ E_NOTICE);
//error_reporting(E_ALL);

if (!defined('ROOT_DIR'))
    define('ROOT_DIR', dirname(__FILE__).'/');

session_start();
/*
 * определяем тип устройства, выбор темы под этот тип
 * Временно: если выбрана мобильная версия , переход на новую версию Движка
*/
if($_REQUEST['type'] == "pda")
	{
		setcookie("theme", 2, 0);
	}
	
$theme = filter_input(INPUT_COOKIE, 'theme');
	
if ($theme == 2) {
	define('THEME', 'mobile');
	require_once 'oc_core/bootstrap.php';
	die();	
} elseif ($theme == 1) {
} else {
	require_once('oc_core/plugins/Mobile_Detect.php');
	$detect = new Mobile_Detect;
	if ($detect->isMobile() and !$detect->isTablet()) {
		setcookie("theme", 2, 0);
		define('THEME', 'mobile');
	    require_once 'oc_core/bootstrap.php';
	    die();
	} else {
	    setcookie("theme", 1, 0);
	}
}



/**
 *  Старая версия СMS
 */
include ("connect.php");
include ("func/core.php");
if (!defined('ROOT_DIR'))
    define('ROOT_DIR', dirname(__FILE__).'/');

// Берем данные о роутинге
$tab = isset($_GET['t']) ? $_GET['t'] : 'main';
$content = isset($_GET['c']) ? $_GET['c'] : '';

// $append_info = "в Москве и Санкт-Петербурге";

//Подгружаем общие данные конфига
query_config();


//query_cat_menu ();

if ((isset($_GET['p'])) && (!isset($_COOKIE['fpid']))) {
	setPartnerCookie();
}
global $actionPayIncoming;
	$actionPayIncoming = false;
if ((isset($_GET['apclick'])) && ((isset($_GET['apsource']))) && ((isset($_GET['source'])))) {
	
	if (!isset($_COOKIE['actionpay'])) {
		$actionPayIncoming = true;
		setcookie('actionpay', $_GET['apclick'] . '.' . $_GET['apsource'], time() + 2592000, '/');
	}
}
// var_dump( $GRM_UserRealData['user_region']);
//Med vera hook
if (file_exists('./hooks/index.php')) {
	include('./hooks/index.php');
}

//Флажек "страница не найдена"
$pageNotFound = false;

// Hide msk phone 495 if $hidePhone495 == true; extend класс "костыли" (если не работают телефоны)
$hidePhone495 = false;

// Выполняем запрос данных в зависимости от раздела
switch ($tab) {
	//-------------------------------- Страницы
	case "page":
	
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';
		
		if (preg_match("/punkty-samovyvoza/i", $content)) {
			// исключение для страницы "Пункты самовывоза"
			$data = getStaticPage('punkty-samovyvoza', true);
					$region = substr($content, 18);
			$query ="SELECT title, adresses, count,  center, zoom, seo_title, seo_description FROM deliveryPickups ORDER BY sort";
			$result = mysql_query($query);
			$arrPoints = array();
				while ($row = mysql_fetch_array($result)) {
					
					$arrPoints[]= $row;
					if (translit_new($row['title']) == $region) {
						
						
						//$data['seo_title'].= " ".$row['title'];
						
						// сео для пунктов самовывоза
						$default_descr = 'Официальный магазин Армед, доставка по всей России, +7 (495) 230-62-72' ;
						$default_title = 'Армед в г. ' . $row['title'] . ' - инвалидные коляски, медицинские кровати и другая реабилитационная техника' ;

						$data['seo_title'] = (strlen($row['seo_title']) > 10 ) ? $row['seo_title'] : $default_title ;
						$data['seo_des'] = (strlen($row['seo_des']) > 10 ) ? $row['seo_des'] : $default_descr ;


						
					}					
				}
		} else {
			// Запрашиваем данные для страницы
			$data = getStaticPage($content, true);
		}

		if ($data == false) {
			$pageNotFound = true;
			break;
		}



		$seo_title = $data['seo_title'];
		$seo_key = $data['seo_key'];
		$seo_des = $data['seo_des'];
		
		break;

	//-------------------------------- Отзывы
	case "feedback":
	
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';
		
		global $fb_validation;
		$fb_validation = addFeedBack();
		$tab = 'feedback';
		$seo_title = 'Оставить отзыв';
		$seo_des = 'Оставить отзыв';
		$seo_key = 'Оставить отзыв';
		$seo_title_text = '';
		$seo_text = '';
		if ($fb_validation == null) {
			$currentPage = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;
			//Отзывов на странцу
			$onPage = 20;
			if ($_GET['page'] == 'all') {
				$limitString = '';
			} else {
				//Всего отзывов
				$feedsCount = fetchOne("SELECT COUNT(*) FROM `shop_otzyv` WHERE `confirm` = '1'");
				//Всего страниц
				$allPages = ceil($feedsCount / $onPage);
				
				//Если номер текущей страницы больше, чем страниц всего, то текущая страница 1
				if ($currentPage > $allPages) {
					$currentPage = 1;
				}
				//Высчитываем offset записей
				$limit = ($currentPage - 1) * $onPage;
				//Подготавливаем строку для запроса
				$limitString = 'LIMIT '.$limit.','.$onPage;
				
				//Подготавливаем данные для скармливания pageGenerator'у
				$paginData = array(
					'pagesCount' => $allPages,
					'currentPage' => $currentPage
				);
			}
			$qString = new queryString();
			$qString = $qString->setHash('read');
			
			$messages = array();
			$sel = mysql_query("SELECT * FROM `shop_otzyv` WHERE `confirm` = '1' ORDER BY `id` DESC {$limitString}");
			while ($res = mysql_fetch_assoc($sel)) {
				$messages[] = array(
					'id' => $res['id'],
					'name' => $res['name'],
					'des' => $res['des'],
					'otvet' => $res['otvet'],
					'date' => $res['date']
				);
			}
		}
		break;

	 //Новости 
    case "news":
        $seo_title = "Новости";
        $seo_des = $seo_title;
        $seo_key = $seo_title;
        // $content = "novosti";
        $tab = $content;
    break;
	//-------------------------------- Статьи
	case "articles":
	
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';
		
		if ($content == '') {
			$tab = 'articles';
			$seo_title = 'Статьи о здоровье человека';
			$seo_des = 'Интересные статьи о здоровье человека ';
			$seo_key = 'здоровье человека статьи';
			$seo_title_text = '';
			$seo_text = '';
		} else {
			$tab = 'artread';

			$seo_key = 'Интересные статьи';
			$seo_title_text = '';
			$seo_text = '';

			global $article;
			$article = getArticle($content);
			$seo_title = $article['seo_title'];
			$seo_des = strip_tags(htmlspecialchars_decode($article['announce']));
		}
		break;

	case "articles2":

		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';
		
		if ($content == '') {
			$tab = 'articles2';
			$seo_title = 'Интересные статьи';
			$seo_des = 'Интересные статьи';
			$seo_key = 'Интересные статьи';
			$seo_title_text = '';
			$seo_text = '';
		} else {
			$tab = 'artread2';

			$seo_key = 'Интересные статьи';
			$seo_title_text = '';
			$seo_text = '';

			global $article;
			$article = getArticle($content);
			$seo_title = $article['seo_title'];
			$seo_des = strip_tags(htmlspecialchars_decode($article['announce']));
		}
		break;

	//-------------------------------- Категории
	case "cat":

		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'category';
		$ecomm_totalvalue = '';
		
		global $cur_cat;

		
		//echo $content;	
			
		$temp = getCatInfo($content);
		if ($temp == false) {
			//Категории нет
			$pageNotFound = true;
			break;
		}

		
		$data['catInfo'] = $temp;

		
		$seo_title_text = $temp['seo_title_text'];
		$seo_text = $temp['seo_text'];
			

		
		$seo_title = $temp['seo_title'] . ' ' .$append_info;
		$seo_des = $temp['seo_des'] . ' ' .$append_info;
		$seo_key = $temp['seo_key'];
		
		$title = $temp['title'];
		
		//Если выбрана какая то страница (в урл ?page), то не выводить сео текст и заголовк поменять
		if (isset($_GET['page'])) {
			$seo_title = $data['catInfo']['title'];
			$seo_text = '';
			$seo_title_text = '';
		}
		
		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		$filter['cat'] = $temp['id'];
		if (isset($_REQUEST['min_price']) && is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price'];
		}
		if (isset($_REQUEST['max_price']) && is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price'];
		}
		$temp = getProducts(false, $filter, getSort(), $page);
		$data['products'] = $temp['products'];
		$data['page'] = $temp['page'];
		//Страницы под номером N не существует
		if ($data['page']['outOfBounds']) {
			$pageNotFound = true;
			break;
		}
		$data['tags'] = getTagsInCat($filter['cat']);
		$data['tab'] = 'cat';
		//Запрашиваем максимальную и минимальную цены
		$data['price_range'] = $temp['price'];
		
		//Выберем доступные параметры (для фильтра)
		$data['params'] = getRealParamsAndValuesInCat($filter['cat']);
		
		$data['paramsFormAction'] = 'cat';
		$data['paramsHidden'] = array('cat' => $content);

		$qString = new queryString;
		$tab = "catlist";
		
		/*// 404 если ничего не нашли
		if ($chekis == '') {
			header("Status: 404 Not Found");
			query_pages('404');
			$tab = "page";
		}*/
		break;
	//-------------------------------- Категории
	case "deals":

		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'category';
		$ecomm_totalvalue = '';
		
		$seo_title_text = '';
		$seo_text = '';
		global $cur_cat;
		$cur_cat = $content;

		switch($content) {
			case 'best' : 
					$filter['best'] = 1;
					$title = 'Самое лучшее, новинки';
				break;
			case 'sale' : 
					$filter['sale'] = 1;
					$title = 'Сезонные распродажи';
				break;
		}
		$seo_title_text = $temp['seo_title_text'] . ' ' .$append_info;
		$seo_text = $temp['seo_text'] . ' ' .$append_info;
		
		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);

		//тут то мы и добавим фильтр
		$temp = getProducts(false, $filter, getSort(), $page);
		$data['products'] = $temp['products'];
		$data['page'] = $temp['page'];
		
		$qString = new queryString;
		$tab = "deals";
		/*// 404 если ничего не нашли
		if ($chekis == '') {
			header("Status: 404 Not Found");
			query_pages('404');
			$tab = "page";
		}*/
		break;

	//-------------------------------- Категории в пункте меню
	case "mcat":
		
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'category';
		$ecomm_totalvalue = '';
		
		$seo_title_text = '';
		$seo_text = '';
		global $cur_cat;
		$cur_cat = $content;
		
		// Запрашиваем данные для страницы
		$data['menuItem'] = getMenuItemInfo($content);
		if ($data['menuItem'] == false) {
			$pageNotFound = true;
			break;
		}
		$data['catInfo']['title'] = $data['menuItem']['title'];
		$data['cats'] = getCatsInMenuItem($content);
		
		$cats = array();
		$data['params'] = array(); 
		
		foreach ($data['cats'] as $cat) {
			$cats[] = $cat['cat_id'];
		}
		
		$data['params'] = getRealParamsAndValuesInCats($cats);
		
		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		$filter['cat'] = $cats;
		if (isset($_REQUEST['min_price']) && is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price'];
		}
		if (isset($_REQUEST['max_price']) && is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price'];
		}
		$temp = getProducts(false, $filter, getSort(), $page);
		$data['products'] = $temp['products'];
		$data['page'] = $temp['page'];
		$data['tab'] = 'mcat';
		
		//Запрашиваем максимальную и минимальную цены
		$data['price_range'] = $temp['price'];
		
		$seo_title = $data['menuItem']['seo_title'] . ' ' .$append_info;
		$seo_text = $data['menuItem']['seo_text'] . ' ' .$append_info;
		$seo_des = '';
		$seo_key = '';
		
		//Если выбрана какая то страница (в урл ?page), то не выводить сео текст и заголовк поменять
		if (isset($_GET['page'])) {
			$seo_title = $data['menuItem']['title'];
			$seo_text = '';
		}
		
		$qString = new queryString;
		$tab = "mcat";
		break;



	//Теги\псевдокатегории
	case 'tags' :
	
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'category';
		$ecomm_totalvalue = '';
	
		global $cur_cat;
		$cur_cat = $content;
		
		$tag = getTagInfo($content);
		if ($tag == false ) {
			$pageNotFound = true;
			break;
		}
		$seo_title = $tag['seo_title'] . ' ' .$append_info;
		$seo_key = '';
		$seo_des = $tag['seo_description'] . ' ' .$append_info;
		$seo_title_text = '';
		$seo_text = $tag['seo_text'];
		
		$title = $tag['title'];
		$data['tagInfo'] = $tag;
		$data['catInfo'] = getCatInfo($tag['cat_id']);
		
		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		
		$filter = array();
		$filter['tag'] = $tag['id'];
		
		$temp = getProducts(false, $filter, getSort(), $page);
	
		$data['tab'] = 'tags';
		
		//соседние тег
		$data['tags'] = getTagsInCat($data['catInfo']['id']);
		
		$data['products'] = $temp['products'];
		$data['page'] = $temp['page'];
				
		//максимальную и минимальную цены
	
		$data['price_range'] = $temp['price'];
		
		//Выберем доступные параметры (для фильтра)
		$data['params'] = getRealParamsAndValuesInCat($tag['cat_id']);
		
		$data['paramsFormAction'] = 'tag';
		$data['paramsHidden'] = array('tag' => $tag['id']);
		
		$qString = new queryString;
		$tab = "catlist";
	break;
	//-------------------------------- Спецпредложения
	case "spec":
	
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'category';
		$ecomm_totalvalue = '';
	
		$seo_title = 'Лидеры продаж интернет магазина медтехники Мед-Сердце';
		$seo_title_text = 'Какие товары попадают в этот раздел?';
		$seo_text = 'В разделе "Лучшее решение" представлены товары, отобранные из различных категорий. Их можно назвать лучшими и по ценовому уровню и по качеству. Эти товары всегда есть в наличии.  Выражаясь простым языком, эти товары - лучшие из лучших. Они имеют высокую степень надежности. Вы никогда не разочаруетесь в своем выборе.';
		// Запрашиваем данные для страницы   
		query_catalog_spec();
		//if ($chekis == ''){ header("Status: 404 Not Found"); query_pages ('404'); $tab = "page";	}
		break;


	//-------------------------------- Каталог
	 case "catalog":
        $seo_title_text = '';
        $seo_text = '';



        // Запрашиваем данные для страницы
        //query_catalog_vn($content); выпилина, переписана
        //file_put_contents('cookie.txt', $_SESSION['lastCat']);

        $product = getProduct($content);
        if ($product == false) {
            $pageNotFound = true;
            break;
        }
        //компенсация
        $isCompensate = isCompensate($product['id']);

        //данные для Google Adwords
        $ecomm_prodid = $product['id'];
        $ecomm_pagetype = 'product';
        $ecomm_totalvalue = (float) $product['price_after_discount'];

        //последняя посещенная категория
        if (isset($_SESSION['lastCat']) && is_numeric($_SESSION['lastCat']) &&
                in_array($_SESSION['lastCat'], getAdditionalCatsId($product['id'])))
            $product['cat_info'] = getCatInfo($_SESSION['lastCat']);

        $cat = $product['cat_info'];
        $data['tags'] = getTagsInCat($cat['id']);

        $data['feedAdded'] = false;
        $data['feedError'] = false;
        $data['feedErrorSpam'] = false;

        if (isset($_REQUEST['add_product_feedback'])) {
            $name = $_REQUEST['name'];
            $email = $_REQUEST['email'];
            $comment = $_REQUEST['comment'];

            if (strlen($name) > 3 && strlen($comment) > 3 && !isLinksContain($name) && !isLinksContain($email) && !isLinksContain($comment)) {
                addProductFeedback($product['id'], array('name' => $name, 'email' => $email, 'comment' => $comment));
                $data['feedAdded'] = true;
            } else {
                $data['feedError'] = true;
                if (isLinksContain($name) || isLinksContain($comment) || isLinksContain($email)) {
                    $data['feedErrorSpam'] = true;
                }
            }
        }


        $data['feedback'] = getProductFeedback($product['id']);

        $seo_title = $product['seo_title'];
        //$seo_des = $product['seo_des'];
        $des = $product;
        $des['feedback'] = $data['feedback'];

        $seo_des = getSnippet($des);
        $seo_key = $product['seo_key'];
        $feedback = getProductFeedback($product['id'], false);
        $productrating = getProductRating($feedback);

        break;



	//-------------------------------- Обратная связь
	case "feed":
	
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';	
	
		$seo_title = 'Обратная связь';
		$seo_des = 'Обратная связь';
		$seo_key = 'обратная связь';

		if ($content == "ok_send") {
			$tab = "ok_send";
			$seo_title = 'Ваше сообщение принято';
			$seo_des = 'Ваше сообщение принято';
			$seo_key = 'Ваше сообщение принято';
		}
		if ($content == "ok_obratnyi") {
			$tab = "ok_obratnyi";
			$seo_title = 'Мы вам перезвоним';
			$seo_des = 'Мы вам перезвоним';
			$seo_key = 'Мы вам перезвоним';
		}
		if ($content == "ok_otzyv") {
			$tab = "ok_otzyv";
			$seo_title = 'Ваш отзыв отправлен';
			$seo_des = 'Ваш отзыв отправлен';
			$seo_key = 'Ваш отзыв отправлен';
		}
		$seo_title_text = '';
		$seo_text = '';
		break;

	//-------------------------------- Обратный звонок
	case "obratnyi":
	
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';	
	
		$seo_title = 'Обратный звонок';
		$seo_des = 'Обратный звонок';
		$seo_key = 'обратный звонок';
		$seo_title_text = '';
		$seo_text = '';
		break;

	//-------------------------------- Поиск
	case "search":
	
		$seo_title = 'Поиск';
		$seo_des = 'Поиск';
		$seo_key = 'Поиск';
		$seo_title_text = '';
		$seo_text = '';
		$search_text = '';

		include ("func/search.php");
		$search = filter_input(INPUT_GET, 'q');

		//$search_query = iconv('windows-1251','utf-8', $search);
		$query = new mShinxClient('armedmarket');
		$products = $query->getProducts($search, 20);
		$categories = $query->getCategories($search);
		$tags = $query->getTags($search);

		break;
		
		
	//-------------------------------- Генерация счета
	case "invoice":
		// print $_SERVER['QUERY_STRING'];
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';
	
		$seo_title = 'Оплата';
		$seo_des = 'Оплата';
		$seo_key = 'Оплата';
		$seo_title_text = '';
		$seo_text = '';
		break;	
 
	//-------------------------------- Корзина
	case "pay":
        $tab = $content;
        session_start();

        //Типы доставки
        $delivery = array();

        $sel = mysql_query("SELECT * FROM `order_delivery_types` ORDER BY `id` ASC");
        while ($row = mysql_fetch_assoc($sel)) {
            $delivery[] = $row;
        }
        //Товары в корзине
        $products = getProductsInBasket();

        //Сумма заказа не учитывая главную скидку при оформлении заказа
        $priceSumm = 0;
        $productsCount = count($products);
        $productsAmount = 0;

        $productsInCart = array();

        //данные для Google Adwords
        $ecomm_prodid = array();
        $ecomm_pagetype = 'cart';
        $ecomm_totalvalue = array();

        $recomArr = [];
        for ($i = 0; $i < $productsCount; $i++) {
            $productsInCart[$products[$i]['id']] = array(
                'id' => $products[$i]['id'],
                'amount' => $products[$i]['amount']
            );
            $recomArr[] = $products[$i]['id'];
            $priceSumm += $products[$i]['price_after_discount'] * $products[$i]['amount'];
            $productsAmount += $products[$i]['amount'];
            $ecomm_prodid[$i] = $products[$i]['id'];
            $ecomm_totalvalue[$i] = $priceSumm;
        }

        if ($productsCount === 1) {
            $ecomm_prodid = $ecomm_prodid[0];
            $ecomm_totalvalue = $ecomm_totalvalue[0];
        } else {
            $ecomm_prodid = json_encode($ecomm_prodid);
            $ecomm_totalvalue = json_encode($ecomm_totalvalue);
        }

        $data['recomm'] = getRecommendations($recomArr);
        $temp = cart::getDeliveryAndDiscount($products);

        $deliveryPrice = $temp['deliveryPrice'];
        $discount['value'] = $temp['discount'];
        $discount['type'] = 'percent';

        //depreceted
        //Общая сумма заказа с учетом скидки
        //$discount = getGlobalDiscount($productsAmount, $priceSumm);

        $discountValue = $discount['value'];
        switch ($discount['type']) {
            case 'percent' : $discountTypeString = '%';
                break;
            case 'value' : $discountTypeString = 'руб';
                break;
            default : $discountTypeString = '';
        }
        $priceSummWithGlobalDiscount = getPriceAfterDiscount($priceSumm, $discount['type'], $discount['value']);

        //Обратный звонок (заказ обратного звонка, записывается как
        $backCall = isset($_REQUEST['backcall']);

        //Если открыт первый шаг оформления корзины
        if ($content == 'korzina') {
            // СЕО данные для страницы оформления заказа
            $seo_title = 'Оформление заказа';
            $seo_des = 'Оформление заказа';
            $seo_key = 'Оформление заказа';
            $seo_title_text = '';
            $seo_text = '';
        } elseif ($content == 'vybor') {
            //Второй щаг оформления заказа. Выбор доставки
            $seo_title = 'Оформление заказа';
            $seo_des = 'Оформление заказа';
            $seo_key = 'Оформление заказа';
            $seo_title_text = '';
            $seo_text = '';

            if (count($productsInCart) <= 0) {
                header("Location: " . getTemplateLink(array('chpu' => 'korzina'), 'cart'));
                die();
            }
        } elseif ($content == 'finish') {
            //Третий шаг оформления заказа. Заполнение реквизитов
            //$tab = '';
            //Если корзина пустая, какое оформление заказа? ты чо?
            //Не пускать на страницу

            if (count($productsInCart) <= 0) {
                header("Location: " . getTemplateLink(array('chpu' => 'korzina'), 'cart'));
                die();
            }

            $deliveryType = $_POST['radio'];
            if (isset($_POST['delivery_type']))
                $deliveryType = $_POST['delivery_type'];
            elseif (isset($_POST['radio']))
                $deliveryType = $_POST['radio'];

            if (isset($_POST['make_order']) || isset($_POST['make_order.x']) || isset($_POST['make_order_x'])) {

                $data = array();
                $data['name'] = schars($_POST['name']);
                $data['phone'] = schars($_POST['phone']);
                $data['email'] = schars($_POST['email']);
                $data['post_index'] = schars($_POST['post_index']);
                $data['adress'] = schars($_POST['adress']);
                $data['extra_information'] = schars($_POST['extra_information']);
                $data['delivery_type'] = $deliveryType;
                $data['backcall'] = $backCall;

                // Вытаскиваем куку "Откуда пришел пользователь"	
                $data['ref'] = $_COOKIE["ref"];


                if ($_SESSION['capcha'] == $_POST['capcha']) {

                    $orderStatus = makeOrder($productsInCart, $data);

                    if (!isset($orderStatus['error'])) {
                        // Обнуляем корзину
                        setcookie("total_amount", "", time() + 7200, "/");
                        setcookie("cart_products", "", time() + 7200, "/");

                        //На всякий. Мож в шаблоне ok.tpl понадобятся данные о сумме, скидках и тд
                        extract($orderStatus);

                        $tab = 'ok';
                    } else {
                        //Поля бы нужно заполнить
                        $error = true;
                    }
                } else {
                    //По капче не прошел
                    $error = true;
                }
            }
        } elseif ($content == "ok") {
            # ok.tpl
            if(!$_COOKIE['order_id']){
                header("Location: /");
            }

            $seo_title = 'Ваш заказ принят!';
            $seo_des = 'Ваш заказ принят!';
            $seo_key = 'Ваш заказ принят!';

            # end ok.tpl 
            
            //if ($_COOKIE['order_id'])
                //header("Location: /");
            //else {
                //file_put_contents('cookie.txt', var_export($_COOKIE, true));
                //$orderId = unserialize($_COOKIE['order_id']);
                //$orderInfo = getOrderById($orderId);
                //unset($_COOKIE['order_id']);
                //setcookie('order_id', NULL, time() - 3600, '/');

                //file_put_contents('orderInfo.txt', var_export($orderInfo, true));
                //$base64 = getBase64Order($orderInfo);
                //$sig = signMessage($base64, Config::get('credit.api_secret'));
            //}
            break;
        } else {
            $pageNotFound = true;
            break;
        }
        //Блокировка повторного отправления заказа по F5 ("отправить данные")
        $_SESSION['capcha'] = rand(9999, 9999999);
        break;

////////////////////////////////////////////////////////////////////////////////
	//-------------------------------- Новый поиск
	case "newsearch":
	
		
		break;
////////////////////////////////////////////////////////////////////////////		
	//Главная
	default:
		$tab = "main";
		
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'home';
		$ecomm_totalvalue = '';		
		
		//Тескт в нижней части главной страницы выбираем из page
		$staticPage = getStaticPage('page-on-main');
		 
		$seo_title = Config::get('site.name');
		$seo_des = Config::get('site.des');
		$seo_key = Config::get('site.key');
		/*
		  $num_cat = count($cat_title);
		  $num_pol = $num_cat/2;
		  $num_pol = intval ($num_pol);
		  $num_per = $num_cat - $num_pol;
		 */
		break;
}
if ($pageNotFound) {
	$ecomm_prodid = '';
	$ecomm_pagetype = 'other';
	$ecomm_totalvalue = '';	
	$data = getStaticPage('404', true);
	$seo_title_text = $data['seo_title'];
	$seo_text = isset($data['seo_desc']) ? $data['seo_desc'] : '';
	$tab = 'page';
	header('HTTP/1.1 404 Not Found');
	header("Status: 404 Not Found");
}
// ---------------------------------------------   ОТЛАДКА
//Грузим данные для страницы
//echo "<pre>"; print_r($spec_id); echo "</pre>";echo $num_cat;
// Пишем куки откуда пришел пользователь
$h = isset($_COOKIE["ref"]) ? $_COOKIE["ref"] : '';
if ($h == '') {
	$h = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
	setcookie("ref", "$h", time() + 2000000, "/");
}
?>

<?php include ('tpl/index.tpl.php'); ?>