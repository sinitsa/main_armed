<?php
// error_reporting(E_ALL ^ E_NOTICE);
error_reporting( E_ERROR);

// Берем данные о роутинге
$tab = $_GET['t'];
$content = $_GET['c'];





if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', dirname(__FILE__) . '/');
}

ini_set("log_errors", 0);
$php_errors_file = ROOT_PATH . 'php-error.txt';
ini_set("error_log", $php_errors_file);	

		
// Инициализация компонента
require ( ROOT_PATH . 'components/geolocation/GRM_GL.php' );



if(trim($GRM_UserRealData['user_path'] , '/') === 'category.php'){
	
	$tab = 'cat';
	$_cont_arr = explode('&',$GRM_UserRealData['user_query']);
	
	$content_arr = explode('=',$_cont_arr[0]);
	
	$content = $content_arr[1];
	
	if(!in_array($content, array(35, 11, 12, 13, 14))) $content = $content;
	
}

if(trim($GRM_UserRealData['user_path'] , '/') === 'product.php'){
	
	$tab = 'catalog';
	
	$_cont_arr = explode('&',$GRM_UserRealData['user_query']);
	
	$content_arr = explode('=',$_cont_arr[0]);
	
	$content = $content_arr[1];
	
	//echo $content;

}
$tab_aksii = false;
if(trim($GRM_UserRealData['user_path'] , '/') === 'novinki.php'){
    $tab = "cat";
    $tab_aksii = true;
    $content = "novinki";
}

if(strpos($GRM_UserRealData['user_query'], "page=1") !== FALSE):
	
	$new_path = str_replace("page=1", "", $GRM_UserRealData['user_query']);
	//echo $new_path;
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: http://test.med-vera.ru" . $GRM_UserRealData['user_path'] );
	
endif;


include ("connect.php");
include ("func/core.php");

define('ROOT_DIR', dirname(__FILE__) . '/');
define('EXTRA_DIR', ROOT_DIR . 'components/');


// MULTIDOMAIN WITH GEOGRAPHY READY
if (file_exists(EXTRA_DIR . 'multidomain/common.php')) {
    // define('MIXDOM', TRUE);
    // include ( EXTRA_DIR . 'multidomain/common.php' );
}



//Подгружаем общие данные конфига
query_config();


session_start();
//query_cat_menu ();

if ((isset($_GET['p'])) && (!isset($_COOKIE['fpid']))) {
    setPartnerCookie();
}
global $actionPayIncoming;
$actionPayIncoming = false;
if ((isset($_GET['apclick'])) && ((isset($_GET['apsource']))) && ((isset($_GET['source'])))) {

    if (!isset($_COOKIE['actionpay'])) {
        $actionPayIncoming = true;
        setcookie('actionpay', $_GET['apclick'] . '.' . $_GET['apsource'], time() + 2592000, '/');
    }
}
//Med vera hook
if (file_exists('./hooks/index.php')) {
    include('./hooks/index.php');
}

if (isset($_COOKIE['fav_products']) && !empty($_COOKIE['fav_products'])) {
    $fav_array = explode('|', $_COOKIE['fav_products']);
    foreach ($fav_array as $product_info) {
        $product = explode(':', $product_info);
        $favorites[] = $product[0];
    }
}


//Флажек "страница не найдена"
$pageNotFound = false;
// Выполняем запрос данных в зависимости от раздела
switch ($tab) {
    //-------------------------------- Страницы
    case "page":
        // Запрашиваем данные для страницы
        $data = getStaticPage($content, true);
        if ($data == false) {
            $pageNotFound = true;
            break;
        }

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'other';
        $ecomm_totalvalue = '';

        $seo_title = $data['seo_title'];
        $seo_key = $data['seo_key'];
        $seo_des = $data['seo_des'];

        break;

    //-------------------------------- Отзывы
    case "feedback":

		//данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'other';
        $ecomm_totalvalue = '';

       	$currentPage = (isset($_REQUEST['page']) && is_numeric($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
		
		//Отзывов на странцу
		$onPage = Config::get('feedback.onPage');
		if ($_REQUEST['page'] == 'all') {
			$limitString = '';
		} else {
			//Всего отзывов
			$feedsCount = fetchOne("SELECT COUNT(*) FROM `shop_otzyv` WHERE `confirm` = '1'");
			//Всего страниц
			$allPages = ceil($feedsCount / $onPage);
			
			//Если номер текущей страницы больше, чем страниц всего, то текущая страница 1
			if ($currentPage > $allPages) {
				$currentPage = 1;
			}
			//Высчитываем offset записей
			$limit = ($currentPage - 1) * $onPage;
			//Подготавливаем строку для запроса
			$limitString = 'LIMIT '.$limit.','.$onPage;
			
			//Подготавливаем данные для скармливания pageGenerator'у
			$paginData = array(
				'pagesCount' => $allPages,
				'currentPage' => $currentPage
			);
		}
		$sel = mysql_query("SELECT * FROM `shop_otzyv` WHERE `confirm` = '1' ORDER BY `id` DESC {$limitString}");
		while ($res = mysql_fetch_assoc($sel)) {
			$messages[] = array(
				'id' => $res['id'],
				'name' => trim($res['name']),
				'des' => trim($res['des']),
				'otvet' => trim($res['otvet']),
				'date' => $res['date']
			);
		}
	
	break;

    //-------------------------------- Статьи
    case "articles":

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'other';
        $ecomm_totalvalue = '';

        if ($content == '') {
            $tab = 'articles';
            $seo_title = 'Статьи о здоровье человека';
            $seo_des = 'Интересные статьи о здоровье человека ';
            $seo_key = 'здоровье человека статьи';
            $seo_title_text = '';
            $seo_text = '';
        } else {
            $tab = 'artread';

            $seo_key = 'Интересные статьи';
            $seo_title_text = '';
            $seo_text = '';

            global $article;
            $article = getArticle($content);
            $seo_title = $article['seo_title'];
            $seo_des = strip_tags(htmlspecialchars_decode($article['announce']));
        }
        break;

    case "articles2":

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'other';
        $ecomm_totalvalue = '';

        if ($content == '') {
            $tab = 'articles2';
            $seo_title = 'Интересные статьи';
            $seo_des = 'Интересные статьи';
            $seo_key = 'Интересные статьи';
            $seo_title_text = '';
            $seo_text = '';
        } else {
            $tab = 'artread2';

            $seo_key = 'Интересные статьи';
            $seo_title_text = '';
            $seo_text = '';

            global $article;
            $article = getArticle($content);
            $seo_title = $article['seo_title'];
            $seo_des = strip_tags(htmlspecialchars_decode($article['announce']));
        }
        break;

    //-------------------------------- Категории
    case "cat":
				
        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'category';
        $ecomm_totalvalue = '';

        global $cur_cat;

        if (isset($_COOKIE['fav_products']) && !empty($_COOKIE['fav_products'])) {
            $fav_array = explode('|', $_COOKIE['fav_products']);
            foreach ($fav_array as $product_info) {
                $product = explode(':', $product_info);
                $favorites[] = $product[0];
            }
        }

        $temp = getCatInfo($content);
				
        if ($temp == false) {
            //Категории нет
            $cur_cat = $content;
            $tag = getTagInfo($content);
            if ($tag == false) {
                //Подкатегории нет
                //$pageNotFound = true;
                break;
            }
            $seo_title = $tag['seo_title'];
            $seo_key = $tag['seo_key'];
            $seo_des = $tag['seo_description'];
            $seo_title_text = '';
            $seo_text = $tag['seo_text'];

            $title = $tag['title'];
            $data['tagInfo'] = $tag;
            $data['catInfo'] = getCatInfo($tag['cat_id']);
          
            //последняя посещенная категория
            $_SESSION['lastCat'] = $data['catInfo']['id'];

            $page = array(
                'page' => getPage(),
                'onPage' => Config::get('catalog.products_on_page')
            );

            $filter = array();
            $filter['tag'] = $tag['id'];

            $temp = getProducts(false, $filter, getSort(), $page);
            $data['tab'] = 'tags';

            //соседние тег
            $data['tags'] = getTagsInCat($data['catInfo']['id']);

            $data['products'] = $temp['products'];
            $data['page'] = $temp['page'];

            //максимальную и минимальную цены

            $data['price_range'] = $temp['price'];

            //Выберем доступные параметры (для фильтра)
            $data['params'] = getRealParamsAndValuesInCat($tag['cat_id']);

            $data['paramsFormAction'] = 'tag';
            $data['paramsHidden'] = array('tag' => $tag['id']);

            $qString = new queryString;
            $tab = "catlist";
            break;
        }
        $data['catInfo'] = $temp;
        //последняя посещенная категория
        $_SESSION['lastCat'] = $data['catInfo']['id'];

        $seo_title_text = $temp['seo_title_text'];
        $seo_text = $temp['seo_text'];

        $seo_title = $temp['seo_title'];
        $seo_des = $temp['seo_des'];
        $seo_key = $temp['seo_key'];

        $title = $temp['title'];

        //Если выбрана какая то страница (в урл ?page), то не выводить сео текст и заголовк поменять
        if (isset($_GET['page'])) {
            $seo_title = $data['catInfo']['title'];
            $seo_text = '';
            $seo_title_text = '';
        }

        $page = array(
            'page' => getPage(),
            'onPage' => Config::get('catalog.products_on_page')
        );

        $filter['cat'] = $temp['id'];
        if (isset($_REQUEST['min_price']) && is_numeric($_REQUEST['min_price'])) {
            $filter['>=price_after_discount'] = $_REQUEST['min_price'];
        }
        if (isset($_REQUEST['max_price']) && is_numeric($_REQUEST['max_price'])) {
            $filter['<=price_after_discount'] = $_REQUEST['max_price'];
        }
        $temp = getProducts(false, $filter, getSort(), $page);

   		$data['products'] = $temp['products'];
        $data['page'] = $temp['page'];
        //Страницы под номером N не существует
        if ($data['page']['outOfBounds']) {
            //$pageNotFound = true;
            break;
        }
        $data['tags'] = getTagsInCat($filter['cat']);
        $data['tab'] = 'cat';
        //Запрашиваем максимальную и минимальную цены
        $data['price_range'] = $temp['price'];

        //Выберем доступные параметры (для фильтра)
        $data['params'] = getRealParamsAndValuesInCat($filter['cat']);

        $data['paramsFormAction'] = 'cat';
        $data['paramsHidden'] = array('cat' => $content);
		//Выбираем статьи для данной категории
		$data['articles'] = getCatArticles($filter['cat']);
        $qString = new queryString;
        $tab = "catlist";

        /* // 404 если ничего не нашли
          if ($chekis == '') {
          header("Status: 404 Not Found");
          query_pages('404');
          $tab = "page";
          } */
        break;

    //-------------------------------- Избранное
    case "favorites":

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'other';
        $ecomm_totalvalue = '';

        $seo_title = 'Избранное';
        $seo_des = 'Избранное';
        $seo_key = 'Избранное';
        $seo_title_text = '';
        $seo_text = '';

        if (isset($_COOKIE['fav_products']) && !empty($_COOKIE['fav_products'])) {
            $fav_array = explode('|', $_COOKIE['fav_products']);
            foreach ($fav_array as $product_info) {
                $product = explode(':', $product_info);
                $favorites[] = $product[0];
            }
        }
        $data['products'] = getFavorites($favorites);
        $tab = "favorites";
        break;



    //-------------------------------- Категории в пункте меню
    case "mcat":

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'category';
        $ecomm_totalvalue = '';

        $seo_title_text = '';
        $seo_text = '';
        global $cur_cat;
        $cur_cat = $content;

        // Запрашиваем данные для страницы
        $data['menuItem'] = getMenuItemInfo($content);
        if ($data['menuItem'] == false) {
            $pageNotFound = true;
            break;
        }
        $data['cats'] = getCatsInMenuItem($content);

        $cats = array();
        $data['params'] = array();

        foreach ($data['cats'] as $cat) {
            $cats[] = $cat['cat_id'];
        }

        $data['params'] = getRealParamsAndValuesInCats($cats);

        $page = array(
            'page' => getPage(),
            'onPage' => Config::get('catalog.products_on_page')
        );
        $filter['cat'] = $cats;
        if (isset($_REQUEST['min_price']) && is_numeric($_REQUEST['min_price'])) {
            $filter['>=price_after_discount'] = $_REQUEST['min_price'];
        }
        if (isset($_REQUEST['max_price']) && is_numeric($_REQUEST['max_price'])) {
            $filter['<=price_after_discount'] = $_REQUEST['max_price'];
        }
        $temp = getProducts(false, $filter, getSort(), $page);
        $data['products'] = $temp['products'];
        $data['page'] = $temp['page'];
        $data['tab'] = 'mcat';

        //Запрашиваем максимальную и минимальную цены
        $data['price_range'] = $temp['price'];

        $seo_title = $data['menuItem']['seo_title'];
        $seo_text = $data['menuItem']['seo_text'];
        $seo_des = '';
        $seo_key = '';

        //Если выбрана какая то страница (в урл ?page), то не выводить сео текст и заголовк поменять
        if (isset($_GET['page'])) {
            $seo_title = $data['menuItem']['title'];
            $seo_text = '';
        }

        $qString = new queryString;

        break;

    //-------------------------------- Спецпредложения
    case "spec":

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'other';
        $ecomm_totalvalue = '';

        $seo_title = 'Лидеры продаж интернет магазина медтехники Мед-Сердце';
        $seo_title_text = 'Какие товары попадают в этот раздел?';
        $seo_text = 'В разделе "Лучшее решение" представлены товары, отобранные из различных категорий. Их можно назвать лучшими и по ценовому уровню и по качеству. Эти товары всегда есть в наличии.  Выражаясь простым языком, эти товары - лучшие из лучших. Они имеют высокую степень надежности. Вы никогда не разочаруетесь в своем выборе.';
        // Запрашиваем данные для страницы   
        query_catalog_spec();
        //if ($chekis == ''){ header("Status: 404 Not Found"); query_pages ('404'); $tab = "page";	}
        break;


    //-------------------------------- Каталог
    case "catalog":
        $seo_title_text = '';
        $seo_text = '';



        // Запрашиваем данные для страницы
        //query_catalog_vn($content); выпилина, переписана
        //file_put_contents('cookie.txt', $_SESSION['lastCat']);

        $product = getProduct($content);
        if ($product == false) {
            $pageNotFound = true;
            break;
        }
        //компенсация
        $isCompensate = isCompensate($product['id']);

        //данные для Google Adwords
        $ecomm_prodid = $product['id'];
        $ecomm_pagetype = 'product';
        $ecomm_totalvalue = (float) $product['price_after_discount'];

        //последняя посещенная категория
        if (isset($_SESSION['lastCat']) && is_numeric($_SESSION['lastCat']) &&
                in_array($_SESSION['lastCat'], getAdditionalCatsId($product['id'])))
            $product['cat_info'] = getCatInfo($_SESSION['lastCat']);

        $cat = $product['cat_info'];
        $data['tags'] = getTagsInCat($cat['id']);

        $data['feedAdded'] = false;
        $data['feedError'] = false;
        $data['feedErrorSpam'] = false;

        if (isset($_REQUEST['add_product_feedback'])) {
            $name = $_REQUEST['name'];
            $email = $_REQUEST['email'];
            $comment = $_REQUEST['comment'];

            if (strlen($name) > 3 && strlen($comment) > 3 && !isLinksContain($name) && !isLinksContain($email) && !isLinksContain($comment)) {
                addProductFeedback($product['id'], array('name' => $name, 'email' => $email, 'comment' => $comment));
                $data['feedAdded'] = true;
            } else {
                $data['feedError'] = true;
                if (isLinksContain($name) || isLinksContain($comment) || isLinksContain($email)) {
                    $data['feedErrorSpam'] = true;
                }
            }
        }


        $data['feedback'] = getProductFeedback($product['id']);

        $seo_title = $product['seo_title'];
        //$seo_des = $product['seo_des'];
        $des = $product;
        $des['feedback'] = $data['feedback'];

        $seo_des = getSnippet($des);
        $seo_key = $product['seo_key'];

        break;


    //-------------------------------- Обратная связь
    case "feed":
        $seo_title = 'Обратная связь';
        $seo_des = 'Обратная связь';
        $seo_key = 'обратная связь';

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'other';
        $ecomm_totalvalue = '';

        if ($content == "ok_send") {
            $tab = "ok_send";
            $seo_title = 'Ваше сообщение принято';
            $seo_des = 'Ваше сообщение принято';
            $seo_key = 'Ваше сообщение принято';
        }
        if ($content == "ok_obratnyi") {
            $tab = "ok_obratnyi";
            $seo_title = 'Мы вам перезвоним';
            $seo_des = 'Мы вам перезвоним';
            $seo_key = 'Мы вам перезвоним';
        }
        if ($content == "ok_otzyv") {
            $tab = "ok_otzyv";
            $seo_title = 'Ваш отзыв отправлен';
            $seo_des = 'Ваш отзыв отправлен';
            $seo_key = 'Ваш отзыв отправлен';
        }
        $seo_title_text = '';
        $seo_text = '';
        break;

    //-------------------------------- Обратный звонок
    case "obratnyi":
        $seo_title = 'Обратный звонок';
        $seo_des = 'Обратный звонок';
        $seo_key = 'обратный звонок';

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'other';
        $ecomm_totalvalue = '';

        $seo_title_text = '';
        $seo_text = '';
        break;

    //-------------------------------- Поиск
    case "search":
        $seo_title = 'Поиск';
        $seo_des = 'Поиск';
        $seo_key = 'Поиск';

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'searchresults';
        $ecomm_totalvalue = '';

        $seo_title_text = '';
        $seo_text = '';
        $search_text = $_POST['search_text'];

        if (strlen($search_text) >= 3) {
            $page = array(
                'page' => getPage(),
                'onPage' => Config::get('catalog.products_on_page')
            );

            $temp = getSearchProducts($search_text, $page);
			//print_r ($temp);
			
            $data['products'] = $temp['data'];
            $data['found'] = $temp['found'];
            $data['page'] = $temp['page'];
			
            //Страницы под номером N не существует
            if ($data['page']['outOfBounds']) {
                $pageNotFound = true;
                break;
            }

            $qString = new queryString;
            $tab = "search";
        }
        break;
		
		
	//-------------------------------- Генерация счета
	case "invoice":
		//данные для Google Adwords
		$ecomm_prodid = '';
		$ecomm_pagetype = 'other';
		$ecomm_totalvalue = '';
	
		$seo_title = 'Оплата';
		$seo_des = 'Оплата';
		$seo_key = 'Оплата';
		$seo_title_text = '';
		$seo_text = '';
		break;

    //-------------------------------- Корзина
    case "pay":
        $tab = $content;
        session_start();

        //Типы доставки
        $delivery = array();

        $sel = mysql_query("SELECT * FROM `order_delivery_types` ORDER BY `id` ASC");
        while ($row = mysql_fetch_assoc($sel)) {
            $delivery[] = $row;
        }
        //Товары в корзине
        $products = getProductsInBasket();

        //Сумма заказа не учитывая главную скидку при оформлении заказа
        $priceSumm = 0;
        $productsCount = count($products);
        $productsAmount = 0;

        $productsInCart = array();

        //данные для Google Adwords
        $ecomm_prodid = array();
        $ecomm_pagetype = 'cart';
        $ecomm_totalvalue = array();

        for ($i = 0; $i < $productsCount; $i++) {
            $productsInCart[$products[$i]['id']] = array(
                'id' => $products[$i]['id'],
                'amount' => $products[$i]['amount']
            );
            $priceSumm += $products[$i]['price_after_discount'] * $products[$i]['amount'];
            $productsAmount += $products[$i]['amount'];
            $ecomm_prodid[$i] = $products[$i]['id'];
            $ecomm_totalvalue[$i] = $priceSumm;
        }

        if ($productsCount === 1) {
            $ecomm_prodid = $ecomm_prodid[0];
            $ecomm_totalvalue = $ecomm_totalvalue[0];
        } else {
            $ecomm_prodid = json_encode($ecomm_prodid);
            $ecomm_totalvalue = json_encode($ecomm_totalvalue);
        }

        $temp = cart::getDeliveryAndDiscount($products);

        $deliveryPrice = $temp['deliveryPrice'];
        $discount['value'] = $temp['discount'];
        $discount['type'] = 'percent';

        //depreceted
        //Общая сумма заказа с учетом скидки
        //$discount = getGlobalDiscount($productsAmount, $priceSumm);

        $discountValue = $discount['value'];
        switch ($discount['type']) {
            case 'percent' : $discountTypeString = '%';
                break;
            case 'value' : $discountTypeString = 'руб';
                break;
            default : $discountTypeString = '';
        }
        $priceSummWithGlobalDiscount = getPriceAfterDiscount($priceSumm, $discount['type'], $discount['value']);

        //Обратный звонок (заказ обратного звонка, записывается как
        $backCall = isset($_REQUEST['backcall']);

        //Если открыт первый шаг оформления корзины
        if ($content == 'korzina') {
            // СЕО данные для страницы оформления заказа
            $seo_title = 'Оформление заказа';
            $seo_des = 'Оформление заказа';
            $seo_key = 'Оформление заказа';
            $seo_title_text = '';
            $seo_text = '';
        } elseif ($content == 'vybor') {
            //Второй щаг оформления заказа. Выбор доставки
            $seo_title = 'Оформление заказа';
            $seo_des = 'Оформление заказа';
            $seo_key = 'Оформление заказа';
            $seo_title_text = '';
            $seo_text = '';

            if (count($productsInCart) <= 0) {
                header("Location: " . getTemplateLink(array('chpu' => 'korzina'), 'cart'));
                die();
            }
        } elseif ($content == 'finish') {
            //Третий шаг оформления заказа. Заполнение реквизитов
            //$tab = '';
            //Если корзина пустая, какое оформление заказа? ты чо?
            //Не пускать на страницу

            if (count($productsInCart) <= 0) {
                header("Location: " . getTemplateLink(array('chpu' => 'korzina'), 'cart'));
                die();
            }

            $deliveryType = $_POST['radio'];
            if (isset($_POST['delivery_type']))
                $deliveryType = $_POST['delivery_type'];
            elseif (isset($_POST['radio']))
                $deliveryType = $_POST['radio'];

            if (isset($_POST['make_order']) || isset($_POST['make_order.x']) || isset($_POST['make_order_x'])) {

                $data = array();
                $data['name'] = schars($_POST['name']);
                $data['phone'] = schars($_POST['phone']);
                $data['email'] = schars($_POST['email']);
                $data['post_index'] = schars($_POST['post_index']);
                $data['adress'] = schars($_POST['adress']);
                $data['extra_information'] = schars($_POST['extra_information']);
                $data['delivery_type'] = $deliveryType;
                $data['backcall'] = $backCall;

                // Вытаскиваем куку "Откуда пришел пользователь"	
                $data['ref'] = $_COOKIE["ref"];


                if ($_SESSION['capcha'] == $_POST['capcha']) {

                    $orderStatus = makeOrder($productsInCart, $data);

                    if (!isset($orderStatus['error'])) {
                        // Обнуляем корзину
                        setcookie("total_amount", "", time() + 7200, "/");
                        setcookie("cart_products", "", time() + 7200, "/");

                        //На всякий. Мож в шаблоне ok.tpl понадобятся данные о сумме, скидках и тд
                        extract($orderStatus);

                        $tab = 'ok';
                    } else {
                        //Поля бы нужно заполнить
                        $error = true;
                    }
                } else {
                    //По капче не прошел
                    $error = true;
                }
            }
        } elseif ($content == "ok") {
            # ok.tpl
            if(!$_COOKIE['order_id']){
                header("Location: /");
            }
            # end ok.tpl 
            
            //if ($_COOKIE['order_id'])
                //header("Location: /");
            //else {
                //file_put_contents('cookie.txt', var_export($_COOKIE, true));
                //$orderId = unserialize($_COOKIE['order_id']);
                //$orderInfo = getOrderById($orderId);
                //unset($_COOKIE['order_id']);
                //setcookie('order_id', NULL, time() - 3600, '/');

                //file_put_contents('orderInfo.txt', var_export($orderInfo, true));
                //$base64 = getBase64Order($orderInfo);
                //$sig = signMessage($base64, Config::get('credit.api_secret'));
            //}
            break;
        } else {
            $pageNotFound = true;
            break;
        }
        //Блокировка повторного отправления заказа по F5 ("отправить данные")
        $_SESSION['capcha'] = rand(9999, 9999999);
        break;
////////////////////////////////////////////////////////////////////////////////
    //-------------------------------- Новый поиск
    case "newsearch":
        $seo_title = 'Поиск';
        $seo_des = 'Поиск';
        $seo_key = 'Поиск';

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'searchresults';
        $ecomm_totalvalue = '';

        $seo_title_text = '';
        $seo_text = '';
        $search_error = '';
        if (isset($_POST['sub_ok'])) {
            try {
                if (isset($_POST['search_text'])) {
                    $search_text = $_POST['search_text'];
                } else {
                    $search_text = '';
                }
                if (empty($search_text))
                    throw new Exception('Нет поисковой фразы');
                $host = ltrim($_SERVER['HTTP_HOST'], 'www.');
                $esc = htmlspecialchars($search_text);
                $ehost = htmlspecialchars($host);
                $page = 0;
                $found = 0;

                $search_tail = htmlspecialchars(" site:$ehost");

                // XML запрос
                $doc = "<?xml version='1.0' encoding='utf-8'?>
			<request>
				<query>" . $esc . " " . $search_tail . "</query>
				<groupings>
					<groupby attr='' mode='flat' groups-on-page='100'  docs-in-group='1' />
				</groupings>
				<page>" . $page . "</page>
			</request>";
                // конвертируем в UTF-8
                //$doc = @iconv("windows-1251","UTF-8//IGNORE", $doc);

                $context = stream_context_create(array(
                    'http' => array(
                        'method' => "POST",
                        'header' => "Content-type: application/xml\r\n" .
                        "Content-length: " . strlen($doc),
                        'content' => $doc
                    ),
                    'socket' => array(
                        'bindto' => $_SERVER['SERVER_ADDR'] . ':0'
                    )
                ));
                $response = file_get_contents('http://xmlsearch.yandex.ru/xmlsearch?user=' . Config::get('yandex_search.user') . '&key=' . Config::get('yandex_search.key'), false, $context);

                if (!$response)
                    throw new Exception('Не удалось получить xml yandex.ru');

                if (strlen($response) < 350) {
                    //$response = @iconv("UTF-8","windows-1251//IGNORE", $response);
                    throw new Exception('Ошибка: ' . $response);
                }

                $xmldoc = new SimpleXMLElement($response);
                $error = $xmldoc->response->error;
                $found_all = $xmldoc->response->found;
                $found = $xmldoc->xpath("response/results/grouping/group/doc");
                if ($error)
                    throw new Exception('Ошибка: ' . $error[0]);
                /*  Подготавливаем урл для сравнения и шаблон */
                $replace = array(
                    '/' => '\/',
                    '|' => '\|',
                    '.' => '\.',
                    '?' => '\?',
                    '*' => '\*',
                    '[' => '\[',
                    ']' => '\]',
                    '(' => '\(',
                    ')' => '\)',
                    '[chpu]' => '([a-zA-Z_\-.0-9]+)',
                    '[id]' => '([0-9]+)'
                );

                $catRegExp = '/^' . strtr(Config::get('template.link_cat'), $replace) . '$/i';
                $catalogRegExp = '/^' . strtr(Config::get('template.link_catalog'), $replace) . '$/i';

                //Регекспы для старых ссылок. Обратная совместимость такая, епта
                $a = Config::get('template.link_cat_old');
                $catRegExpOld = !empty($a) ? '/^' . strtr(Config::get('template.link_cat_old'), $replace) . '$/i' : false;
                $a = Config::get('template.link_catalog_old');
                $catalogRegExpOld = !empty($a) ? '/^' . strtr(Config::get('template.link_catalog_old'), $replace) . '$/i' : false;
                /* */
                $i = 0;
                foreach ($found as $item) {
                    //Парсим линк, приводим к нормальному виду
                    $newUrl = parse_url($item->url);
                    $newUrl = $newUrl['path'] . (isset($newUrl['query']) ? '?' . $newUrl['query'] : '');
                    $out = null;

                    //Чекаем ссылку регулярками, чтоб совпадало с шаблоном из конфига
                    if (preg_match($catRegExp, $newUrl, $out) || ($catRegExpOld && preg_match($catRegExpOld, $newUrl, $out))) { // категория
                        $value = $out[1]; //значение chpu или id
                        $column = is_numeric($value) ? 'id' : 'chpu';

                        // узнаем категорию
                        $res = mysql_query("SELECT `catalog`.`id` as id_item, `cat`.`id` as id_cat, `cat`.`title` FROM `catalog`, `cat` WHERE `cat`.`{$column}`='{$value}' AND `cat`.`id`=`catalog`.`cat` LIMIT 1");
                        if (mysql_num_rows($res) == 0)
                            continue;
                        $r = mysql_fetch_array($res);
                        $id_item = $r['id_item'];
                        $id_cat = $r['id_cat'];
                        $catTitle = $r['title'];

                        // узнаем мин. цену
                        $res = mysql_query("SELECT MIN(`price_after_discount`) FROM `catalog` WHERE `cat`='$id_cat'");
                        $price = mysql_result($res, 0);
                        // получение url и поиск дубликатов
                        $search_url = (string) $item->url;
                        if (preg_match('/\?/', $search_url)) {
                            $search_url = preg_replace('/\?.*$/', '', $search_url);
                        }
                        if (isset($search)) {
                            foreach ($search as $k => $v) {
                                // если данный url уже есть в $search - обработываем следующую ссылку с Яндекса
                                if ($search_url == $v['url'])
                                    continue(2);
                            }
                        }

                        $search[$i]['id'] = $id_item;
                        $search[$i]['price'] = 'От ' . $price;
                        $search[$i]['img'] = getImageWebPath('medium') . $id_item . '.jpg';
                        $search[$i]['url'] = $search_url;
                        $search[$i]['type'] = 'cat';
                        $search[$i]['original_title'] = $catTitle;
                    } elseif (preg_match($catalogRegExp, $newUrl, $out) || ($catalogRegExpOld && preg_match($catalogRegExpOld, $newUrl, $out))) { // товар
                        $value = $out[1];
                        $column = is_numeric($value) ? 'id' : 'chpu';

                        $res = mysql_query("SELECT * FROM `catalog` WHERE `{$column}`='{$value}'");
                        if (mysql_num_rows($res) == 0)
                            continue;
                        $r = mysql_fetch_array($res);
                        $search[$i]['id'] = $r['id'];
                        $search[$i]['price'] = $r['price_after_discount'];
                        $search[$i]['img'] = getImageWebPath('medium') . $r['id'] . '.jpg';
                        $search[$i]['url'] = (string) $item->url;
                        $search[$i]['type'] = 'catalog';
                        $search[$i]['original_title'] = $r['title'];
                    } else {
                        continue;
                    }

                    $s_title = highlight_words($item->title);
//				$search[$i]['title'] = @iconv("UTF-8","windows-1251//IGNORE", $s_title);
                    $search[$i]['title'] = $s_title;

                    $s_des = '';
                    if ($item->passages) {
                        foreach ($item->passages->passage as $passage) {
                            $s_des .= highlight_words($passage);
                        }
                    }
//				$search[$i]['des'] = @iconv("UTF-8","windows-1251//IGNORE", $s_des);
                    $search[$i]['des'] = $s_des;

                    $i++;
                }
            } catch (Exception $e) {
                // обработка исключения
                $search_error = $e->getMessage();
            }
        }
        break;
////////////////////////////////////////////////////////////////////////////		
    //Новости 
    case "news":
        $seo_title = "Новости";
        $seo_des = $seo_title;
        $seo_key = $seo_title;
        // $content = "novosti";
        $tab = $content;
    break;
    //Главная
    default:
        $tab = "main";

        //данные для Google Adwords
        $ecomm_prodid = '';
        $ecomm_pagetype = 'home';
        $ecomm_totalvalue = '';

        //Тескт в нижней части главной страницы выбираем из page
        $staticPage = getStaticPage('page-on-main');

        $seo_title = Config::get('site.name');
        $seo_des = $seo_title;
        $seo_key = $seo_title;
        /*
          $num_cat = count($cat_title);
          $num_pol = $num_cat/2;
          $num_pol = intval ($num_pol);
          $num_per = $num_cat - $num_pol;
         */
        break;
}
if ($pageNotFound) {
    $data = getStaticPage('404', true);
    $seo_title_text = $data['seo_title'];
    $seo_text = $data['seo_desc'];

    $ecomm_prodid = '';
    $ecomm_pagetype = 'other';
    $ecomm_totalvalue = '';

    $tab = 'page';
    header('HTTP/1.1 404 Not Found');
    header("Status: 404 Not Found");
}
// ---------------------------------------------   ОТЛАДКА
//Грузим данные для страницы
// echo "<pre>"; print_r($spec_id); echo "</pre>";echo $num_cat;
// Пишем куки откуда пришел пользователь
$h = $_COOKIE["ref"];
if ($h == '') {
    $h = $_SERVER['HTTP_REFERER'];
    setcookie("ref", "$h", time() + 2000000, "/");
}
?>

<?php include ('tpl/index.tpl.php'); ?>

