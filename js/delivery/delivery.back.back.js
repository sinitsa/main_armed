(function($) {
	$.ajaxSetup({
		url: '/edit/m_cat/delivery/controller.php',
		dataType: 'json'
	});

	$(function(){
		var catTable = $('#table-1');


		function processPriceOld(data){
			var table = $('#table-1'),
				input = '<div class="inline"><label for="#name#value">#name</label><input type="number" id="#name#value" name="fields[#name]" value="#value"/></div>';
				option = '<option value="#value">#title</option>';
			table.find('tr[id]').each(function(){
				var html = ['<form class="ajaxAutoSafe">'],
					row = [],
					id = this.id,
					newRow = false;

				if (id in data.rows) row = data.rows[id];
				else newRow = true;
				
				for (i in data.columns) {
					var name = data.columns[i],
						price = row[i] || 0;
						inputHtml = input.replace(/#name/g, name).replace(/#value/g, price);
					html.push(inputHtml);
				}
				if (newRow) html.push('<input type="hidden" name="newRow" value="1" />');
				html.push('<input type="hidden" name="id" value="#id" />'.replace('#id', id));
				html.push('<input type="submit" value="Сохранить" />');
				html.push('</form>');
				$(this).find('td:eq(1)').append(html.join(''));

			});
			table.find('form.ajaxAutoSafe').on('submit', function(){
				var data = $(this).serialize();
				$.ajax({
					url		: '/edit/m_cat/delivery.php',
					data	: data,
					dataType: 'json',
					success	: function(data){
						if (data.result) { // всё ок
							if ('removeNewRow' in data) {
								var id = data['removeNewRow'];
								$('#'+id).find('form.ajaxAutoSafe').find('input[newRow]').remove();
							}
						} else { // не сохранено

						}
					}
				})

				return false;
			})
		}
		

		function processDeliveryGroup(data) {
			if (data) {
				var	form = document.createElement('form'),
					select = document.createElement('select'),
					option = document.createElement('option'),
					inputID = document.createElement('input'),
					inputAction = document.createElement('input');

				inputID.type = 'hidden';
				inputID.name = 'category_id';
				
				inputAction.type = 'hidden';
				inputAction.name = 'action';
				inputAction.value = 'updateCatalog2DeliveryGroup';

				form.classList.add('ajaxAutoSafe');
				form.appendChild(inputID);
				form.appendChild(inputAction);
				form.appendChild(select);
				
				option.value = 0;
				option.textContent = 'Не установлена';
				
				select.name = 'delivery_group_id';
				select.appendChild(option);

				for (id in data.deliveryGroup) {
					var DG = data.deliveryGroup[id],
						option = document.createElement('option');

					option.value = id;
					option.textContent = DG.name;
					select.appendChild(option);
				}

				catTable.find('tr[id]').each(function(){
					var catForm = form.cloneNode(true);
					if (this.id in data.category2deliveryGroup)
						catForm['delivery_group_id'].value = data.category2deliveryGroup[this.id];
					catForm['category_id'].value = this.id;
					catForm.addEventListener('change', function(){
						$.ajax({
							data : $(this).serialize(),
							success : function(data) {
								console.log(data);
							}
						})
					}, false);
					$(this).find('td:eq(1)').append(catForm);
				})
			} else console.log('failed in /edit/m_cat/delivery/get_delivery_group_json.php');
		}

		var $modal = $('#twitter-modal'),
			alert = document.createElement('div'),
			message = document.createElement('p');

		alert.appendChild(message);
		alert.lock = {hide:false, show:false};
		alert.hide = function(){
			alert.lock.hide = true;
			$(alert).animate({opacity: 0}, 2000, function() {// Animation complete.
  				if (alert.lock.hide) {
	  				alert.classList.add('hide');
	  				$(alert).css({opacity: 1});
	  				alert.lock.hide = false;
	  			}
  			});
		}
		alert.show = function(){
			alert.lock.show = true;
			if (alert.lock.hide) {
				$(alert).stop().css({opacity: 1});
				alert.lock.hide=false;
			}
			alert.classList.remove('hide');
			alert.lock.show = false;
		}
		alert.message = function(text){
			alert.className = 'alert alert-success hide';
			message.innerHTML = text;
			alert.show();
			alert.hide();
		}
		alert.confirm = function (text, callbackConfirm, callbackCancel){
			var form = this;
			alert.className = 'alert alert-danger hide';
			message.innerHTML = text + '<br />';
			var buttonConfirm = document.createElement('button'),
				buttonCancel = document.createElement('button'),
				functionConfirm = callbackConfirm,
				functionCancel = callbackCancel;

			buttonConfirm.className = 'btn btn-warning';
			buttonConfirm.appendChild(document.createTextNode('Да'));
			buttonConfirm.addEventListener('click', function(){
				if (functionConfirm instanceof Function)
					functionConfirm(form);
				alert.hide();
			}, false);

			buttonCancel.className = 'btn btn-info';
			buttonCancel.appendChild(document.createTextNode('Отмена'));
			buttonCancel.addEventListener('click', function(){
				if (functionCancel instanceof Function)
					functionCancel(form);
				alert.hide();
			}, false);

			message.appendChild(buttonConfirm);
			message.appendChild(buttonCancel);

			alert.show();
		} 

		$modal.$header = $modal.find('h3');
		$modal.$body   = $modal.find('.modal-body');
		$modal.$footer = $modal.find('.modal-footer');
		$modal.$addForm= $('#delivery-group-add');
		$modal.$editForm = $('#delivery-group-edit');

		var deliveryGroupForm = document.createElement('form'),
			div = document.createElement('div'),
			nameInput = document.createElement('input'),
			sortInput = document.createElement('input'),
			submitButton = document.createElement('button'),
			idInput = document.createElement('input'),
			deleteButton = document.createElement('button'),
			icon_ok = document.createElement('i'),
			icon_remove = document.createElement('i');

		deliveryGroupForm.className = 'ajaxAutoSafe';

		div.className = 'input-append';

		nameInput.name = 'delivery_group[name]';
		nameInput.type = 'text';
		nameInput.required = true;
		nameInput.className = 'span2';
		
		sortInput.name = 'delivery_group[sort]';
		sortInput.type = 'number';
		sortInput.required = true;
		sortInput.className = 'span1';

		submitButton.setAttribute('type', 'submit');
		submitButton.className = 'btn btn-success submit';
		icon_ok.className = 'icon-ok';
		submitButton.appendChild(icon_ok);

		idInput.type = 'hidden';
		idInput.name = 'delivery_group[id]';

		deleteButton.setAttribute('type', 'button');
		deleteButton.className = 'btn-danger btn delete icon-remove';
		icon_remove.className = 'icon-remove';
		deleteButton.appendChild(icon_remove);

		div.appendChild(nameInput);
		div.appendChild(sortInput);
		div.appendChild(submitButton);
		div.appendChild(idInput);
		div.appendChild(deleteButton);
		deliveryGroupForm.appendChild(div);


		var dropdownMenu = $('#delivery-group ul.dropdown-menu');
		
		function validate(form){
			var field, i = 0, errors = [];
			while (field = form[i++]) {
				if (field instanceof HTMLInputElement && field.willValidate) {
					if (field.validity.valid)
						field.classList.remove('error');
					else {
						field.classList.add('error');
						errors.push(field.validationMessage);
					}
				}
			}
			if (errors.length) {
				alert.message(errors.join('<br />'));
				return false;
			} else return true;
		}

		function DeliveryGroupForm(data) {
			var div	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanName = document.createElement('span'),
				spanSort = document.createElement('span'),
				spanBasePrice = document.createElement('span'),
				form	= document.createElement('form'),
				inputID = document.createElement('input'),
				inputAction = document.createElement('input'),
				inputName	= document.createElement('input'),
				inputSort	= document.createElement('input'),
				inputBasePrice = document.createElement('input'),
				buttonSubmit = document.createElement('button'),
				buttonDelete = document.createElement('button'),
				icon_ok		= document.createElement('i'),
				icon_remove = document.createElement('i'),
				dataValues = data || null;

			inputID.type = 'hidden';
			inputID.name = 'id';

			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputName.required = true;
			inputName.type = 'text';
			inputName.name = 'name';
			
			inputSort.required = true;
			inputSort.type = 'number';
			inputSort.name = 'sort';

			inputBasePrice.required = true;
			inputBasePrice.type = 'number';
			inputBasePrice.name = 'base_price';

			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';

			buttonSubmit.className = 'btn btn-success';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			buttonSubmit.addEventListener('click', updateRow, false);

			buttonDelete.className = 'btn btn-danger';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);
			buttonDelete.addEventListener('click', deleteRow, false);
			
			spanName.className = 'add-on text';
			spanName.appendChild(document.createTextNode('Имя'));
			spanSort.className = 'add-on number';
			spanSort.appendChild(document.createTextNode('Сортировка'));
			spanBasePrice.className = 'add-on number';
			spanBasePrice.appendChild(document.createTextNode('Базовая цена'));

			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanName);
			legengDiv.appendChild(spanSort);
			legengDiv.appendChild(spanBasePrice);

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputName);
			div.appendChild(inputSort);
			div.appendChild(inputBasePrice);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);

			form.className = 'ajaxAutoSafe';
			form.appendChild(legengDiv);
			form.appendChild(div);

			if (dataValues) for (i in dataValues) {
				form[i].value = dataValues[i];
				form[i].placeholder = dataValues[i];
			} else {
				var newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value = 1;
				div.appendChild(newRow);
			}

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				form['action'].value = 'deleteDeliveryGroup';
				
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						form.parentNode.removeChild(form);
					});
				} else {
					$.ajax({
						data	: $(form).serialize(),
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								form.parentNode.removeChild(form);
								dropdownMenu.find('a[data-id=#id]'.replace('#id', form['id'].value)).parent().remove();
								$modal.modal('hide');
							} else {
								form['delete'].disabled = true;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				if ('newRow' in form)
					form['action'].value = 'addDeliveryGroup';
				else
					form['action'].value = 'updateDeliveryGroup';

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД';
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
							if ('newRow' in form) {
								form['newRow'].parentNode.removeChild(form['newRow']);
								var inputID = document.createElement('input');
								inputID.type = 'hidden';
								inputID.name = 'id';
								inputID.value = data.success.id;
								form.appendChild(inputID);
								menu.add({
									name: form['name'].value,
									id	: data.success.id,
									href: '#editDeliveryGroup'
								});
							}
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
			}

			return form;
		}
		function DeliveryPriceForms(data, delivery_group_id) {
			var forms	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanFrom = document.createElement('span'),
				spanPrice = document.createElement('span'),
				legend	= document.createElement('legend'),
				a		= document.createElement('a'),
				div		= document.createElement('div'),
				form	= document.createElement('form'),
				inputID	= document.createElement('input'),
				inputFrom	= document.createElement('input'),
				inputPrice	= document.createElement('input'),
				inputAction = document.createElement('input'),
				inputDeliveryGroup = document.createElement('input'),
				buttonSubmit	= document.createElement('button'),
				buttonDelete	= document.createElement('button'),
				icon_ok			= document.createElement('i'),
				icon_remove		= document.createElement('i'),
				icon_plus		= document.createElement('i'),
				DeliveryGroupID = delivery_group_id;

			inputID.type	= 'hidden';
			inputID.name	= 'id';
			
			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputFrom.type	= 'number';
			inputFrom.name	= 'from';
			inputFrom.className	= 'span1';
			inputFrom.required	= true;

			inputPrice.type = 'number';
			inputPrice.name = 'price';
			inputPrice.className= 'span1';
			inputPrice.required= true;

			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';
			icon_plus.className = 'icon-plus icon-white';

			buttonSubmit.className = 'btn btn-success submit';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			
			buttonDelete.className = 'btn btn-danger delete';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);

			inputDeliveryGroup.type = "hidden";
			inputDeliveryGroup.name = "delivery_group_id";
			inputDeliveryGroup.value = DeliveryGroupID;

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputDeliveryGroup);
			div.appendChild(inputFrom);
			div.appendChild(inputPrice);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);	

			legend.appendChild(document.createTextNode('Диапазоны цен'));

			a.className = 'btn btn-primary add';
			a.appendChild(icon_plus);
			a.appendChild(document.createTextNode(" Добавить новый диапазон"));
			a.addEventListener('click', function(){
				var newForm = form.cloneNode(true),
					newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value= 1;
				newForm.appendChild(newRow);
				newForm['delivery_group_id'].value = DeliveryGroupID;
				newForm['delete'].addEventListener('click', deleteRow,false);
				newForm['submit'].addEventListener('click', updateRow,false);
				
				if (forms.childElementCount == 2)
					newForm.insertBefore(legengDiv, newForm.firstElementChild);

				forms.appendChild(newForm);
			}, false);			

			form.className = 'ajaxAutoSafe';
			form.appendChild(div);
			

			spanFrom.className = 'add-on number';
			spanFrom.appendChild(document.createTextNode('От'));
			spanPrice.className = 'add-on number';
			spanPrice.appendChild(document.createTextNode('Цена'));

			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanFrom);
			legengDiv.appendChild(spanPrice);

			forms.className = 'forms';
			forms.appendChild(legend);
			forms.appendChild(a);


			data.forEach(function(item, i) {
				var DPForm = form.cloneNode(true);
					DPForm['id'].value = item.id;
					DPForm['from'].value = item.from;
					DPForm['price'].value = item.price;
					//DPForm['delivery_group_id]'].value = item.delivery_group_id;
					DPForm['delete'].addEventListener('click', deleteRow, false);
					DPForm['submit'].addEventListener('click', updateRow, false);
				if (i == 0) DPForm.insertBefore(legengDiv, DPForm.firstElementChild);
				forms.appendChild(DPForm);
			});

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						forms.removeChild(form);
					});
				} else {
					var data = {action:'deleteDeliveryPrice', id: form['id'].value};
					$.ajax({
						data	: data,
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								forms.removeChild(form);
							} else {
								form['delete'].disabled = false;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				if ('newRow' in form)
					form['action'].value = 'addDeliveryPrice';
				else
					form['action'].value = 'updateDeliveryPrice';

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД'; 
						if ('newRow' in form)
							form['newRow'].parentNode.removeChild(form['newRow']);
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
				return false;
			}
			return forms;		
		}


		function FormFactory(){
			var form = this;
			this.setTable = function(table){
				form.table = table;
			}
			this.getTableMeta = function(){
				return $.ajax({
					data	:'action=meta&table='+form.table,
					success	: function(data){
						form.columns = data.columns;
					}

				})
			}


		}
		var catGabarits = new FormFactory;
			catGabarits
				.setTable('cat_gabarits')
				.getTableMeta().success(function(){
					legengDiv = document.createElement('div'),
					legengDiv.className = 'input-prepend';
					legengDiv.appendChild(spanName);
					legengDiv.appendChild(spanSort);
					legengDiv.appendChild(spanBasePrice);

					form.columns.forEach(function(column){
						var columnInput = document.createElement('input');

						switch(column.type){
							case ''
						}
					})
				})


		function DeliveryGroupForm(data) {
			var div	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanName = document.createElement('span'),
				spanSort = document.createElement('span'),
				spanBasePrice = document.createElement('span'),
				form	= document.createElement('form'),
				inputID = document.createElement('input'),
				inputAction = document.createElement('input'),
				inputName	= document.createElement('input'),
				inputSort	= document.createElement('input'),
				inputBasePrice = document.createElement('input'),
				buttonSubmit = document.createElement('button'),
				buttonDelete = document.createElement('button'),
				icon_ok		= document.createElement('i'),
				icon_remove = document.createElement('i'),
				dataValues = data || null;

			inputID.type = 'hidden';
			inputID.name = 'id';

			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputName.required = true;
			inputName.type = 'text';
			inputName.name = 'name';
			
			inputSort.required = true;
			inputSort.type = 'number';
			inputSort.name = 'sort';

			inputBasePrice.required = true;
			inputBasePrice.type = 'number';
			inputBasePrice.name = 'base_price';

			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';

			buttonSubmit.className = 'btn btn-success';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			buttonSubmit.addEventListener('click', updateRow, false);

			buttonDelete.className = 'btn btn-danger';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);
			buttonDelete.addEventListener('click', deleteRow, false);
			
			spanName.className = 'add-on text';
			spanName.appendChild(document.createTextNode('Имя'));
			spanSort.className = 'add-on number';
			spanSort.appendChild(document.createTextNode('Сортировка'));
			spanBasePrice.className = 'add-on number';
			spanBasePrice.appendChild(document.createTextNode('Базовая цена'));

			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanName);
			legengDiv.appendChild(spanSort);
			legengDiv.appendChild(spanBasePrice);

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputName);
			div.appendChild(inputSort);
			div.appendChild(inputBasePrice);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);

			form.className = 'ajaxAutoSafe';
			form.appendChild(legengDiv);
			form.appendChild(div);

			if (dataValues) for (i in dataValues) {
				form[i].value = dataValues[i];
				form[i].placeholder = dataValues[i];
			} else {
				var newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value = 1;
				div.appendChild(newRow);
			}

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				form['action'].value = 'deleteDeliveryGroup';
				
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						form.parentNode.removeChild(form);
					});
				} else {
					$.ajax({
						data	: $(form).serialize(),
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								form.parentNode.removeChild(form);
								dropdownMenu.find('a[data-id=#id]'.replace('#id', form['id'].value)).parent().remove();
								$modal.modal('hide');
							} else {
								form['delete'].disabled = true;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				if ('newRow' in form)
					form['action'].value = 'addDeliveryGroup';
				else
					form['action'].value = 'updateDeliveryGroup';

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД';
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
							if ('newRow' in form) {
								form['newRow'].parentNode.removeChild(form['newRow']);
								var inputID = document.createElement('input');
								inputID.type = 'hidden';
								inputID.name = 'id';
								inputID.value = data.success.id;
								form.appendChild(inputID);
								menu.add({
									name: form['name'].value,
									id	: data.success.id,
									href: '#editDeliveryGroup'
								});
							}
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
			}

			return form;
		}		

		dropdownMenu.on('click', function(e){
			var a = e.target;
			$(a).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
			switch(a.hash) {
				case '#edit':
					$modal.$header.html('Редактировать группу доставки');
					$.ajax({
						data	: 'action=getDeliveryGroupList',
						success	: function(data){
							var div = document.createElement('div');
							div.className = "delivery-group-list";
							
							var	DPselect = document.createElement('select'),
								option = document.createElement('option');
							DPselect.name = 'delivery_group[delivery_price]';
							DPselect.classList.add('span2');

							for (id in data.DPList) {
								var DPoption = option.cloneNode(true);
									DP = data.DPList[id];
								DPoption.value = id;
								DPoption.textContent = DP.from + ' - ' + DP.till;
								DPselect.appendChild(DPoption);
							}


							data.DGList.forEach(function(group){
								var form = deliveryGroupForm.cloneNode(true);
								form['delivery_group[name]'].value		= group.name;
								form['delivery_group[name]'].placeholder= group.name;
								form['delivery_group[sort]'].value		= group.sort;
								form['delivery_group[sort]'].placeholder= group.sort;
								form['delivery_group[id]'].value		= group.id;
								
								for (i in data.DG2DPList[group.id]) {
									var DPID = data.DG2DPList[group.id][i],
										select = DPselect.cloneNode(true);

									select.value = DPID;
									form.firstElementChild.insertBefore(select, form['delivery_group[sort]']);
								}

								div.appendChild(form);
							});
							$modal.$body.html(div);
							$modal.$body.find('.ajaxAutoSafe').on('change', function(){
								$(this).find('button.submit').removeClass('btn-success btn-info').addClass('btn-warning');
							}).on('submit', function(){
								var $form = $(this);
								$form.$submit = $form.find('button.submit');
								$form.remove = $form.find('button.delete').get(0);
								$.ajax({
									data	: 'action=updateDeliveryGroup&' + $(this).serialize(),
									beforeSend	: function(jqXHR, settings){
										$form.$submit.removeClass('btn-success btn-info').addClass('btn-warning');
										$form.remove.disabled = true;
									},
									success	: function(data) {
										var message = 'Ошибка, на сервере БД'; 
										if ('rowsAffected' in data.success) {
											switch (data.success.rowsAffected) {
												case 0	: message = 'Нечего обновлять.'; break;
												case 1	: message = 'Группа доставки успешно обновлена.'; break;
												default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
											}
											$form.$submit.removeClass('btn-warning btn-info btn-danger')
														 .addClass('btn-success').prop('disabled', false);
											$form.remove.disabled = false;
										} else {
											$form.$submit.removeClass('btn-warning btn-info btn-success')
														 .addClass('btn-danger').prop('disabled', true);
										}
										alert.message(message);
									}
								});
								return false;
							})
						}
					});
					$modal.$footer.html(alert);
					break;
				case '#add':
					$modal.$header.html('Добавить группу доставки');
					$modal.$body.html(DeliveryGroupForm());
					$modal.$footer.html(alert);
					break;
				case '#addSSS':
					$modal.$header.html('Добавить группу доставки');
					$modal.$footer.html(alert);

					$modal.$body.html($modal.$addForm);
					$modal.$addForm.on('submit', function(){
						var $form = $(this);
							$form.$submit = $form.find('button.submit');
						$.ajax({
							data	: 'action=addDeliveryGroup&' + $(this).serialize(),
							beforeSend	: function(jqXHR, settings){
								$form.$submit.removeClass('btn-success btn-info').addClass('btn-warning').prop('disabled', true);
							},
							success	: function(data){
								var message = 'Ошибка, на сервере БД'; 
								if ('rowsAffected' in data.success) {
									switch (data.success.rowsAffected) {
										case 0	: message = 'Нечего добавлять.'; break;
										case 1	: message = 'Группа доставки успешно добавлена.'; break;
										default	: message = data.success.rowsAffected + ' строк добавлено. Ошибка';
									}
									$form.$submit.removeClass('btn-warning btn-info btn-danger')
												 .addClass('btn-success').prop('disabled', false);
								} else {
									$form.$submit.removeClass('btn-warning btn-info btn-success')
												 .addClass('btn-danger').prop('disabled', true);
								}
								alert.message(message);
							}
						})
						return false;
					})
					break;
				case '#editDeliveryGroup':
					$modal.$header.html('Редактировать группу доставки');
					$modal.$footer.html(alert);
					$.ajax({
						data	: 'action=getDeliveryGroup&id=' + $(a).data('id'),
						beforeSend	: function(jqXHR, settings) {
							alert.message('Загружаем информацию');
						},
						success	: function(data) {
							var div = document.createElement('div');
							div.appendChild(DeliveryGroupForm(data.DG));
							div.appendChild(DeliveryPriceForms(data.DPList, data.DG.id));
							$modal.$body.html(div);
							alert.message('Информация загружена');
						}
					});
					break;
				case ''
				default: console.log('no case for', a.hash, a);
			}
			$modal.modal();
			return false;
		});
	
		var menu = {
			add : function(item){
				var li = menu.li.cloneNode(true),
					a = li.lastElementChild;

				if ('href' in item)
					a.href = item.href;
				a.lastChild.nodeValue = ' ' + item.name;
				a.setAttribute('data-id', item.id);
				menu.ul.appendChild(li);
			},
			ul:'',
			li:'',
		};

		(function(menu, dropdownMenu){
			var ul	= dropdownMenu.get(0),
			li	= document.createElement('li'),
			a	= document.createElement('a'),
			i	= document.createElement('i'),
			divider = li.cloneNode(true);

			i.classList.add('icon-pencil');
			a.href="#editDeliveryGroup";
			a.appendChild(i);
			a.appendChild(document.createTextNode(''));
			li.appendChild(a);

			divider.classList.add('divider');
			ul.appendChild(divider);

			menu.ul = ul;
			menu.li = li;
		})(menu, dropdownMenu);


		$.ajax({
			data	: 'action=getDeliveryGroupForMenu',
			success : function(data) {
				data.list.forEach(menu.add);
			}
		})
		$.ajax({
			url	: '/edit/m_cat/delivery/get_delivery_group_json.php',
			dataType: 'json',
			success : processDeliveryGroup
		});
	})
})(jQuery)