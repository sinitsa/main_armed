(function($) {
	$.ajaxSetup({
		url: '/edit/m_delivery/delivery/controller.php',
		dataType: 'json'
	});

	$(function(){
		var catTable = $('#table-1');

		function processDeliveryGroup(data) {
			if (data) {
				var	form = document.createElement('form'),
					select = document.createElement('select'),
					option = document.createElement('option'),
					inputID = document.createElement('input'),
					inputAction = document.createElement('input');

				inputID.type = 'hidden';
				inputID.name = 'category_id';
				
				inputAction.type = 'hidden';
				inputAction.name = 'action';
				inputAction.value = 'updateCatalog2DeliveryGroup';

				form.classList.add('ajaxAutoSafe');
				form.appendChild(inputID);
				form.appendChild(inputAction);
				form.appendChild(select);
				
				option.value = 0;
				option.textContent = 'Не установлена';
				
				select.name = 'delivery_group_id';
				select.appendChild(option);

				for (id in data.deliveryGroup) {
					var DG = data.deliveryGroup[id],
						option = document.createElement('option');

					option.value = id;
					option.textContent = DG.name;
					select.appendChild(option);
				}

				catTable.find('tr[id]').each(function(){
					var catForm = form.cloneNode(true);
					if (this.id in data.category2deliveryGroup)
						catForm['delivery_group_id'].value = data.category2deliveryGroup[this.id];
					catForm['category_id'].value = this.id;
					catForm.addEventListener('change', function(){
						$.ajax({
							data : $(this).serialize(),
							success : function(data) {
								console.log(data);
							}
						})
					}, false);
					$(this).find('td:eq(1)').append(catForm);
				})
			} else console.log('failed in /edit/m_delivery/delivery/get_delivery_group_json.php');
		}

		var $modal = $('#twitter-modal'),
			alert = document.createElement('div'),
			message = document.createElement('p');

		alert.appendChild(message);
		alert.lock = {hide:false, show:false};
		alert.hide = function(){
			alert.lock.hide = true;
			$(alert).animate({opacity: 0}, 2000, function() {// Animation complete.
  				if (alert.lock.hide) {
	  				alert.classList.add('hide');
	  				$(alert).css({opacity: 1});
	  				alert.lock.hide = false;
	  			}
  			});
		}
		alert.show = function(){
			alert.lock.show = true;
			if (alert.lock.hide) {
				$(alert).stop().css({opacity: 1});
				alert.lock.hide=false;
			}
			alert.classList.remove('hide');
			alert.lock.show = false;
		}
		alert.message = function(text){
			alert.className = 'alert alert-success hide';
			message.innerHTML = text;
			alert.show();
			alert.hide();
		}
		alert.confirm = function (text, callbackConfirm, callbackCancel){
			var form = this;
			alert.className = 'alert alert-danger hide';
			message.innerHTML = text + '<br />';
			var buttonConfirm = document.createElement('button'),
				buttonCancel = document.createElement('button'),
				functionConfirm = callbackConfirm,
				functionCancel = callbackCancel;

			buttonConfirm.className = 'btn btn-warning';
			buttonConfirm.appendChild(document.createTextNode('Да'));
			buttonConfirm.addEventListener('click', function(){
				if (functionConfirm instanceof Function)
					functionConfirm(form);
				alert.hide();
			}, false);

			buttonCancel.className = 'btn btn-info';
			buttonCancel.appendChild(document.createTextNode('Отмена'));
			buttonCancel.addEventListener('click', function(){
				if (functionCancel instanceof Function)
					functionCancel(form);
				alert.hide();
			}, false);

			message.appendChild(buttonConfirm);
			message.appendChild(buttonCancel);

			alert.show();
		} 

		$modal.$header = $modal.find('h3');
		$modal.$body   = $modal.find('.modal-body');
		$modal.$footer = $modal.find('.modal-footer');
		$modal.$addForm= $('#delivery-group-add');
		$modal.$editForm = $('#delivery-group-edit');



		var dropdownMenu = $('#delivery-group ul.dropdown-menu');
		var dropdownMenuG = $('#gabarits-group ul.dropdown-menu');
		
		function validate(form){
			var field, i = 0, errors = [];
			while (field = form[i++]) {
				if (field instanceof HTMLInputElement && field.willValidate) {
					if (field.validity.valid)
						field.classList.remove('error');
					else {
						field.classList.add('error');
						errors.push(field.validationMessage);
					}
				}
			}
			if (errors.length) {
				alert.message(errors.join('<br />'));
				return false;
			} else return true;
		}

		function DeliveryGroupForm(data, Gabarits) {
			var div	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanName = document.createElement('span'),
				spanSort = document.createElement('span'),
				
				spanGabarits = document.createElement('span'),
				
				form	= document.createElement('form'),
				inputID = document.createElement('input'),
				inputAction = document.createElement('input'),
				inputName	= document.createElement('input'),
				inputSort	= document.createElement('input'),
				
				selectGabarits = document.createElement('select'),
				
				buttonSubmit = document.createElement('button'),
				buttonDelete = document.createElement('button'),
				icon_ok		= document.createElement('i'),
				icon_remove = document.createElement('i'),
				dataValues = data || null;

			inputID.type = 'hidden';
			inputID.name = 'id';

			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputName.required = true;
			inputName.type = 'text';
			inputName.name = 'name';
			
			inputSort.required = true;
			inputSort.type = 'number';
			inputSort.name = 'sort';

			selectGabarits.required = true;
			selectGabarits.type = 'number';
			selectGabarits.name = 'cat_gabarits_id';

			if (typeof Gabarits != 'undefined') {
				Gabarits.forEach(function(item) {
					var option = document.createElement('option');
						option.value = item.id;
						option.appendChild(document.createTextNode(item.name));
					selectGabarits.appendChild(option);
				})
			}



			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';

			buttonSubmit.className = 'btn btn-success';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			buttonSubmit.addEventListener('click', updateRow, false);

			buttonDelete.className = 'btn btn-danger';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);
			buttonDelete.addEventListener('click', deleteRow, false);
			
			spanName.className = 'add-on text';
			spanName.appendChild(document.createTextNode('Имя'));
			spanSort.className = 'add-on number';
			spanSort.appendChild(document.createTextNode('Сортировка'));
			spanGabarits.className = 'add-on text';
			spanGabarits.appendChild(document.createTextNode('Группа габаритов'));

			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanName);
			legengDiv.appendChild(spanSort);
			legengDiv.appendChild(spanGabarits);

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputName);
			div.appendChild(inputSort);
			div.appendChild(selectGabarits);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);

			form.className = 'ajaxAutoSafe';
			form.appendChild(legengDiv);
			form.appendChild(div);

			if (dataValues) for (i in dataValues) {
				form[i].value = dataValues[i];
				form[i].placeholder = dataValues[i];
			} else {
				var newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value = 1;
				div.appendChild(newRow);
			}

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				form['action'].value = 'deleteDeliveryGroup';
				
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						form.parentNode.removeChild(form);
					});
				} else {
					$.ajax({
						data	: $(form).serialize(),
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								form.parentNode.removeChild(form);
								$(deliveryGroupMenu.ul).find('a[data-id=#id]'.replace('#id', form['id'].value)).parent().remove();
								$modal.modal('hide');
							} else {
								form['delete'].disabled = true;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				if ('newRow' in form)
					form['action'].value = 'addDeliveryGroup';
				else
					form['action'].value = 'updateDeliveryGroup';

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД';
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
							if ('newRow' in form) {
								form['newRow'].parentNode.removeChild(form['newRow']);
								var inputID = document.createElement('input');
								inputID.type = 'hidden';
								inputID.name = 'id';
								inputID.value = data.success.id;
								form.appendChild(inputID);
								deliveryGroupMenu.add({
									name: form['name'].value,
									id	: data.success.id,
									href: '#edit'
								});
							}
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
			}

			return form;
		}
		function GabaritGroupForm(data) {
			var div	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanName = document.createElement('span'),
				spanSort = document.createElement('span'),
				spanBasePrice = document.createElement('span'),
				form	= document.createElement('form'),
				inputID = document.createElement('input'),
				inputAction = document.createElement('input'),
				inputName	= document.createElement('input'),
				inputSort	= document.createElement('input'),
				inputBasePrice = document.createElement('input'),
				buttonSubmit = document.createElement('button'),
				buttonDelete = document.createElement('button'),
				icon_ok		= document.createElement('i'),
				icon_remove = document.createElement('i'),
				dataValues = data || null;

			inputID.type = 'hidden';
			inputID.name = 'id';

			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputName.required = true;
			inputName.type = 'text';
			inputName.name = 'name';

			inputSort.required = true;
			inputSort.type = 'number';
			inputSort.name = 'sort';
			

			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';

			buttonSubmit.className = 'btn btn-success';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			buttonSubmit.addEventListener('click', updateRow, false);

			buttonDelete.className = 'btn btn-danger';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);
			buttonDelete.addEventListener('click', deleteRow, false);
			
			spanName.className = 'add-on text';
			spanName.appendChild(document.createTextNode('Имя'));

			spanSort.className = 'add-on number';
			spanSort.appendChild(document.createTextNode('Сортировка'));


			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanName);
			legengDiv.appendChild(spanSort);

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputName);
			div.appendChild(inputSort);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);

			form.className = 'ajaxAutoSafe';
			form.appendChild(legengDiv);
			form.appendChild(div);

			if (dataValues) for (i in dataValues) {
				form[i].value = dataValues[i];
				form[i].placeholder = dataValues[i];
			} else {
				var newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value = 1;
				div.appendChild(newRow);
			}

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				form['action'].value = 'deleteGabaritGroup';
				
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						form.parentNode.removeChild(form);
					});
				} else {
					$.ajax({
						data	: $(form).serialize(),
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								form.parentNode.removeChild(form);
								$(deliveryGroupMenu.ul).find('a[data-id=#id]'.replace('#id', form['id'].value)).parent().remove();
								$modal.modal('hide');
							} else {
								form['delete'].disabled = true;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				if ('newRow' in form)
					form['action'].value = 'addGabaritGroup';
				else
					form['action'].value = 'updateGabaritGroup';

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД';
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
							if ('newRow' in form) {
								form['newRow'].parentNode.removeChild(form['newRow']);
								var inputID = document.createElement('input');
								inputID.type = 'hidden';
								inputID.name = 'id';
								inputID.value = data.success.id;
								form.appendChild(inputID);
								gabaritsGroupMenu.add({
									name: form['name'].value,
									id	: data.success.id,
									href: '#editGabaritGroup'
								});
							}
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
			}

			return form;
		}
		function DeliveryPriceForms(data, delivery_group_id, gabarits) {
			var forms	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanFrom = document.createElement('span'),
				spanPrice = document.createElement('span'),
				legend	= document.createElement('legend'),
				a		= document.createElement('a'),
				div		= document.createElement('div'),
				form	= document.createElement('form'),
				inputID	= document.createElement('input'),
				inputFrom	= document.createElement('input'),
				inputPrice	= document.createElement('input'),
				inputAction = document.createElement('input'),
				inputDeliveryGroup = document.createElement('input'),
				inputCatGabarits = document.createElement('input'),
				buttonSubmit	= document.createElement('button'),
				buttonDelete	= document.createElement('button'),
				icon_ok			= document.createElement('i'),
				icon_remove		= document.createElement('i'),
				icon_plus		= document.createElement('i'),
				DeliveryGroupID = delivery_group_id,
				Gabarits = typeof gabarits != 'undefined' && gabarits == 'gabarits',
				Discount = typeof gabarits != 'undefined' && gabarits == 'discounts';

			inputID.type	= 'hidden';
			inputID.name	= 'id';
			
			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputFrom.type	= 'number';
			inputFrom.name	= 'from';
			inputFrom.className	= 'span1';
			inputFrom.required	= true;

			inputPrice.type = 'number';
			inputPrice.name = 'price';
			inputPrice.className= 'span1';
			inputPrice.required= true;

			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';
			icon_plus.className = 'icon-plus icon-white';

			buttonSubmit.className = 'btn btn-success submit';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			
			buttonDelete.className = 'btn btn-danger delete';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);


			inputDeliveryGroup.type = "hidden";
			inputDeliveryGroup.name = "delivery_group_id";
			inputDeliveryGroup.value = DeliveryGroupID;

			inputCatGabarits.type = "hidden";
			inputCatGabarits.name = "cat_gabarits_id";
			inputCatGabarits.value = DeliveryGroupID;

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputDeliveryGroup);
			div.appendChild(inputCatGabarits);
			div.appendChild(inputFrom);
			div.appendChild(inputPrice);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);	

			if (Gabarits)
				legend.appendChild(document.createTextNode('Диапазоны цен для доставки'));
			else if (Discount)
				legend.appendChild(document.createTextNode('Диапазоны скидок'));
			else
				legend.appendChild(document.createTextNode('Диапазоны цен'));


			a.className = 'btn btn-primary add';
			a.appendChild(icon_plus);
			a.appendChild(document.createTextNode(" Добавить новый диапазон"));
			a.addEventListener('click', function(){
				var newForm = form.cloneNode(true),
					newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value= 1;
				newForm.appendChild(newRow);
				newForm['delivery_group_id'].value = DeliveryGroupID;
				newForm['cat_gabarits_id'].value = DeliveryGroupID;
				newForm['delete'].addEventListener('click', deleteRow,false);
				newForm['submit'].addEventListener('click', updateRow,false);
				
				if (forms.childElementCount == 2)
					newForm.insertBefore(legengDiv, newForm.firstElementChild);

				forms.appendChild(newForm);
			}, false);			

			form.className = 'ajaxAutoSafe';
			form.appendChild(div);
			

			spanFrom.className = 'add-on number';
			spanFrom.appendChild(document.createTextNode('От'));
			spanPrice.className = 'add-on number';
			if (Discount)
				spanPrice.appendChild(document.createTextNode('Скидка, %'));
			else
				spanPrice.appendChild(document.createTextNode('Цена'));

			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanFrom);
			legengDiv.appendChild(spanPrice);

			forms.className = 'forms';
			forms.appendChild(legend);
			forms.appendChild(a);

			if (typeof data != 'undefined' && data.length) data.forEach(function(item, i) {
				var DPForm = form.cloneNode(true);
					DPForm['id'].value = item.id;
					DPForm['from'].value = item.from;
					DPForm['price'].value = item.price;
					//DPForm['delivery_group_id]'].value = item.delivery_group_id;
					DPForm['delete'].addEventListener('click', deleteRow, false);
					DPForm['submit'].addEventListener('click', updateRow, false);
				if (i == 0) DPForm.insertBefore(legengDiv, DPForm.firstElementChild);
				forms.appendChild(DPForm);
			});

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						forms.removeChild(form);
					});
				} else {
					var action = 'deleteDeliveryPrice';
					//if (Gabarits) action = 'deleteDeliveryPriceGabarits';

					form['action'] = action;

					var data = {action: action, id: form['id'].value};
					$.ajax({
						data	: data,
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								forms.removeChild(form);
							} else {
								form['delete'].disabled = false;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				var action = 'updateDeliveryPrice';
				if ('newRow' in form) {
					action = 'addDeliveryPrice';
					if (Gabarits) action += 'Gabarits';
					if (Discount) action += 'Discount';
				}
				

				form['action'].value = action;

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД'; 
						if ('newRow' in form && 'parentNode' in form['newRow'])
							form['newRow'].parentNode.removeChild(form['newRow']);
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
				return false;
			}
			return forms;		
		}


	
		function menu(id){
			var ul	= document.querySelector('#'+id+' ul'),
				li	= document.createElement('li'),
				a	= document.createElement('a'),
				i	= document.createElement('i'),
				divider = li.cloneNode(true);

			i.classList.add('icon-pencil');
			a.href="#edit";
			a.appendChild(i);
			a.appendChild(document.createTextNode(''));
			li.appendChild(a);

			divider.classList.add('divider');
			ul.appendChild(divider);

			this.ul = ul;
			this.add = function(item){
				var newLi = li.cloneNode(true),
					a = newLi.lastElementChild;

				if ('href' in item) a.href = item.href;
				a.lastChild.nodeValue = ' ' + item.name;
				a.setAttribute('data-id', item.id);
				ul.appendChild(newLi);
			}
		}
		var deliveryGroupMenu = new menu('delivery-group');
		var gabaritsGroupMenu = new menu('gabarits-group');

		var cache = {};
		var global = {};
		$(deliveryGroupMenu.ul).on('click', function(e){
			var a = e.target;
			$(a).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
			switch(a.hash) {
				case '#add':
					$modal.$header.html('Добавить группу доставки');
					$modal.$body.html(DeliveryGroupForm(null, global.GabaritList.list));
					$modal.$footer.html(alert);
					break;
				case '#edit':
					$modal.$header.html('Редактировать группу доставки');
					$modal.$footer.html(alert);
					$.ajax({
						data	: 'action=getDeliveryGroup&id=' + $(a).data('id'),
						beforeSend	: function(jqXHR, settings) {
							alert.message('Загружаем информацию');
						},
						success	: function(data) {
							var div = document.createElement('div');
							div.appendChild(DeliveryGroupForm(data.DG, data.Gabarits));
							div.appendChild(DeliveryPriceForms(data.DPList, data.DG.id));
							$modal.$body.html(div);
							alert.message('Информация загружена');
						}
					});
					break;
				default: console.log('no case for', a.hash, a);
			}
			$modal.modal();
			return false;
		});
		$(gabaritsGroupMenu.ul).on('click', function(e){
			var a = e.target;
			$(a).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
			switch(a.hash) {
				case '#add':
					$modal.$header.html('Добавить группу доставки');
					$modal.$body.html(GabaritGroupForm());
					$modal.$footer.html(alert);
					break;
				case '#edit':
					$modal.$header.html('Редактировать группу доставки');
					$modal.$footer.html(alert);
					$.ajax({
						data	: 'action=getGabaritGroup&id=' + $(a).data('id'),
						beforeSend	: function(jqXHR, settings) {
							alert.message('Загружаем информацию');
						},
						success	: function(data) {
							var div = document.createElement('div');
							div.appendChild(GabaritGroupForm(data.DG));
							div.appendChild(DeliveryPriceForms(data.DPList, data.DG.id, 'gabarits'));
							div.appendChild(DeliveryPriceForms(data.DiscountsList, data.DG.id, 'discounts'));
							$modal.$body.html(div);
							alert.message('Информация загружена');
						}
					});
					break;
				
				default: console.log('no case for', a.hash, a);
			}
			$modal.modal();
			return false;
		});



		$.ajax({
			data	: { action : 'select', table : 'delivery_group'},
			success : function(data) {
				data.list.forEach(deliveryGroupMenu.add);
			}
		})
		$.ajax({
			data	: { action : 'select', table : 'cat_gabarits'},
			success : function(data) {
				global.GabaritList = data;
				data.list.forEach(gabaritsGroupMenu.add);
			}
		})
		$.ajax({
			url	: '/edit/m_delivery/delivery/get_delivery_group_json.php',
			dataType: 'json',
			success : processDeliveryGroup
		});
	})
})(jQuery)