(function($) {
	$.ajaxSetup({
		url		: '/edit/m_cat/delivery/controller.php',
		dataType: 'json',
		cache	: false
	});

	$(function(){
		var catTable = $('#table-1');


		function processPriceOld(data){
			var table = $('#table-1'),
				input = '<div class="inline"><label for="#name#value">#name</label><input type="number" id="#name#value" name="fields[#name]" value="#value"/></div>';
				option = '<option value="#value">#title</option>';
			table.find('tr[id]').each(function(){
				var html = ['<form class="ajaxAutoSafe">'],
					row = [],
					id = this.id,
					newRow = false;

				if (id in data.rows) row = data.rows[id];
				else newRow = true;
				
				for (i in data.columns) {
					var name = data.columns[i],
						price = row[i] || 0;
						inputHtml = input.replace(/#name/g, name).replace(/#value/g, price);
					html.push(inputHtml);
				}
				if (newRow) html.push('<input type="hidden" name="newRow" value="1" />');
				html.push('<input type="hidden" name="id" value="#id" />'.replace('#id', id));
				html.push('<input type="submit" value="Сохранить" />');
				html.push('</form>');
				$(this).find('td:eq(1)').append(html.join(''));

			});
			table.find('form.ajaxAutoSafe').on('submit', function(){
				var data = $(this).serialize();
				$.ajax({
					url		: '/edit/m_cat/delivery.php',
					data	: data,
					dataType: 'json',
					success	: function(data){
						if (data.result) { // всё ок
							if ('removeNewRow' in data) {
								var id = data['removeNewRow'];
								$('#'+id).find('form.ajaxAutoSafe').find('input[newRow]').remove();
							}
						} else { // не сохранено

						}
					}
				})

				return false;
			})
		}
		

		function processDeliveryGroup(data) {
			if (data) {
				var	form = document.createElement('form'),
					select = document.createElement('select'),
					option = document.createElement('option'),
					inputID = document.createElement('input'),
					inputAction = document.createElement('input');

				inputID.type = 'hidden';
				inputID.name = 'category_id';
				
				inputAction.type = 'hidden';
				inputAction.name = 'action';
				inputAction.value = 'updateCatalog2DeliveryGroup';

				form.classList.add('ajaxAutoSafe');
				form.appendChild(inputID);
				form.appendChild(inputAction);
				form.appendChild(select);
				
				option.value = 0;
				option.textContent = 'Не установлена';
				
				select.name = 'delivery_group_id';
				select.appendChild(option);

				for (id in data.deliveryGroup) {
					var DG = data.deliveryGroup[id],
						option = document.createElement('option');

					option.value = id;
					option.textContent = DG.name;
					select.appendChild(option);
				}

				catTable.find('tr[id]').each(function(){
					var catForm = form.cloneNode(true);
					if (this.id in data.category2deliveryGroup)
						catForm['delivery_group_id'].value = data.category2deliveryGroup[this.id];
					catForm['category_id'].value = this.id;
					catForm.addEventListener('change', function(){
						$.ajax({
							data : $(this).serialize(),
							success : function(data) {
								console.log(data);
							}
						})
					}, false);
					$(this).find('td:eq(1)').append(catForm);
				})
			} else console.log('failed in /edit/m_cat/delivery/get_delivery_group_json.php');
		}

		var $modal = $('#twitter-modal'),
			alert = document.createElement('div'),
			message = document.createElement('p');

		alert.appendChild(message);
		alert.lock = {hide:false, show:false};
		alert.hide = function(){
			alert.lock.hide = true;
			$(alert).animate({opacity: 0}, 2000, function() {// Animation complete.
  				if (alert.lock.hide) {
	  				alert.classList.add('hide');
	  				$(alert).css({opacity: 1});
	  				alert.lock.hide = false;
	  			}
  			});
		}
		alert.show = function(){
			alert.lock.show = true;
			if (alert.lock.hide) {
				$(alert).stop().css({opacity: 1});
				alert.lock.hide=false;
			}
			alert.classList.remove('hide');
			alert.lock.show = false;
		}
		alert.message = function(text){
			alert.className = 'alert alert-success hide';
			message.innerHTML = text;
			alert.show();
			alert.hide();
		}
		alert.confirm = function (text, callbackConfirm, callbackCancel){
			var form = this;
			alert.className = 'alert alert-danger hide';
			message.innerHTML = text + '<br />';
			var buttonConfirm = document.createElement('button'),
				buttonCancel = document.createElement('button'),
				functionConfirm = callbackConfirm,
				functionCancel = callbackCancel;

			buttonConfirm.className = 'btn btn-warning';
			buttonConfirm.appendChild(document.createTextNode('Да'));
			buttonConfirm.addEventListener('click', function(){
				if (functionConfirm instanceof Function)
					functionConfirm(form);
				alert.hide();
			}, false);

			buttonCancel.className = 'btn btn-info';
			buttonCancel.appendChild(document.createTextNode('Отмена'));
			buttonCancel.addEventListener('click', function(){
				if (functionCancel instanceof Function)
					functionCancel(form);
				alert.hide();
			}, false);

			message.appendChild(buttonConfirm);
			message.appendChild(buttonCancel);

			alert.show();
		} 

		$modal.$header = $modal.find('h3');
		$modal.$body   = $modal.find('.modal-body');
		$modal.$footer = $modal.find('.modal-footer');
		$modal.$addForm= $('#delivery-group-add');
		$modal.$editForm = $('#delivery-group-edit');

		var deliveryGroupForm = document.createElement('form'),
			div = document.createElement('div'),
			nameInput = document.createElement('input'),
			sortInput = document.createElement('input'),
			submitButton = document.createElement('button'),
			idInput = document.createElement('input'),
			deleteButton = document.createElement('button'),
			icon_ok = document.createElement('i'),
			icon_remove = document.createElement('i');

		deliveryGroupForm.className = 'ajaxAutoSafe';

		div.className = 'input-append';

		nameInput.name = 'delivery_group[name]';
		nameInput.type = 'text';
		nameInput.required = true;
		nameInput.className = 'span2';
		
		sortInput.name = 'delivery_group[sort]';
		sortInput.type = 'number';
		sortInput.required = true;
		sortInput.className = 'span1';

		submitButton.setAttribute('type', 'submit');
		submitButton.className = 'btn btn-success submit';
		icon_ok.className = 'icon-ok';
		submitButton.appendChild(icon_ok);

		idInput.type = 'hidden';
		idInput.name = 'delivery_group[id]';

		deleteButton.setAttribute('type', 'button');
		deleteButton.className = 'btn-danger btn delete icon-remove';
		icon_remove.className = 'icon-remove';
		deleteButton.appendChild(icon_remove);

		div.appendChild(nameInput);
		div.appendChild(sortInput);
		div.appendChild(submitButton);
		div.appendChild(idInput);
		div.appendChild(deleteButton);
		deliveryGroupForm.appendChild(div);


		var dropdownMenu = $('#delivery-group ul.dropdown-menu');
		
		function validate(form){
			var field, i = 0, errors = [];
			while (field = form[i++]) {
				if (field instanceof HTMLInputElement && field.willValidate) {
					if (field.validity.valid)
						field.classList.remove('error');
					else {
						field.classList.add('error');
						errors.push(field.validationMessage);
					}
				}
			}
			if (errors.length) {
				alert.message(errors.join('<br />'));
				return false;
			} else return true;
		}

		function DeliveryGroupForm(data) {
			var div	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanName = document.createElement('span'),
				spanSort = document.createElement('span'),
				spanBasePrice = document.createElement('span'),
				form	= document.createElement('form'),
				inputID = document.createElement('input'),
				inputAction = document.createElement('input'),
				inputName	= document.createElement('input'),
				inputSort	= document.createElement('input'),
				inputBasePrice = document.createElement('input'),
				buttonSubmit = document.createElement('button'),
				buttonDelete = document.createElement('button'),
				icon_ok		= document.createElement('i'),
				icon_remove = document.createElement('i'),
				dataValues = data || null;

			inputID.type = 'hidden';
			inputID.name = 'id';

			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputName.required = true;
			inputName.type = 'text';
			inputName.name = 'name';
			
			inputSort.required = true;
			inputSort.type = 'number';
			inputSort.name = 'sort';

			inputBasePrice.required = true;
			inputBasePrice.type = 'number';
			inputBasePrice.name = 'base_price';

			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';

			buttonSubmit.className = 'btn btn-success';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			buttonSubmit.addEventListener('click', updateRow, false);

			buttonDelete.className = 'btn btn-danger';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);
			buttonDelete.addEventListener('click', deleteRow, false);
			
			spanName.className = 'add-on text';
			spanName.appendChild(document.createTextNode('Имя'));
			spanSort.className = 'add-on number';
			spanSort.appendChild(document.createTextNode('Сортировка'));
			spanBasePrice.className = 'add-on number';
			spanBasePrice.appendChild(document.createTextNode('Базовая цена'));

			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanName);
			legengDiv.appendChild(spanSort);
			legengDiv.appendChild(spanBasePrice);

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputName);
			div.appendChild(inputSort);
			div.appendChild(inputBasePrice);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);

			form.className = 'ajaxAutoSafe';
			form.appendChild(legengDiv);
			form.appendChild(div);

			if (dataValues) for (i in dataValues) {
				form[i].value = dataValues[i];
				form[i].placeholder = dataValues[i];
			} else {
				var newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value = 1;
				div.appendChild(newRow);
			}

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				form['action'].value = 'deleteDeliveryGroup';
				
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						form.parentNode.removeChild(form);
					});
				} else {
					$.ajax({
						data	: $(form).serialize(),
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								form.parentNode.removeChild(form);
								dropdownMenu.find('a[data-id=#id]'.replace('#id', form['id'].value)).parent().remove();
								$modal.modal('hide');
							} else {
								form['delete'].disabled = true;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				if ('newRow' in form)
					form['action'].value = 'addDeliveryGroup';
				else
					form['action'].value = 'updateDeliveryGroup';

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД';
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
							if ('newRow' in form) {
								form['newRow'].parentNode.removeChild(form['newRow']);
								var inputID = document.createElement('input');
								inputID.type = 'hidden';
								inputID.name = 'id';
								inputID.value = data.success.id;
								form.appendChild(inputID);
								menu.add({
									name: form['name'].value,
									id	: data.success.id,
									href: '#editDeliveryGroup'
								});
							}
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
			}

			return form;
		}
		function DeliveryPriceForms(data, delivery_group_id) {
			var forms	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanFrom = document.createElement('span'),
				spanPrice = document.createElement('span'),
				legend	= document.createElement('legend'),
				a		= document.createElement('a'),
				div		= document.createElement('div'),
				form	= document.createElement('form'),
				inputID	= document.createElement('input'),
				inputFrom	= document.createElement('input'),
				inputPrice	= document.createElement('input'),
				inputAction = document.createElement('input'),
				inputDeliveryGroup = document.createElement('input'),
				buttonSubmit	= document.createElement('button'),
				buttonDelete	= document.createElement('button'),
				icon_ok			= document.createElement('i'),
				icon_remove		= document.createElement('i'),
				icon_plus		= document.createElement('i'),
				DeliveryGroupID = delivery_group_id;

			inputID.type	= 'hidden';
			inputID.name	= 'id';
			
			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputFrom.type	= 'number';
			inputFrom.name	= 'from';
			inputFrom.className	= 'span1';
			inputFrom.required	= true;

			inputPrice.type = 'number';
			inputPrice.name = 'price';
			inputPrice.className= 'span1';
			inputPrice.required= true;

			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';
			icon_plus.className = 'icon-plus icon-white';

			buttonSubmit.className = 'btn btn-success submit';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			
			buttonDelete.className = 'btn btn-danger delete';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);

			inputDeliveryGroup.type = "hidden";
			inputDeliveryGroup.name = "delivery_group_id";
			inputDeliveryGroup.value = DeliveryGroupID;

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputDeliveryGroup);
			div.appendChild(inputFrom);
			div.appendChild(inputPrice);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);	

			legend.appendChild(document.createTextNode('Диапазоны цен'));

			a.className = 'btn btn-primary add';
			a.appendChild(icon_plus);
			a.appendChild(document.createTextNode(" Добавить новый диапазон"));
			a.addEventListener('click', function(){
				var newForm = form.cloneNode(true),
					newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value= 1;
				newForm.appendChild(newRow);
				newForm['delivery_group_id'].value = DeliveryGroupID;
				newForm['delete'].addEventListener('click', deleteRow,false);
				newForm['submit'].addEventListener('click', updateRow,false);
				
				if (forms.childElementCount == 2)
					newForm.insertBefore(legengDiv, newForm.firstElementChild);

				forms.appendChild(newForm);
			}, false);			

			form.className = 'ajaxAutoSafe';
			form.appendChild(div);
			

			spanFrom.className = 'add-on number';
			spanFrom.appendChild(document.createTextNode('От'));
			spanPrice.className = 'add-on number';
			spanPrice.appendChild(document.createTextNode('Цена'));

			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanFrom);
			legengDiv.appendChild(spanPrice);

			forms.className = 'forms';
			forms.appendChild(legend);
			forms.appendChild(a);


			data.forEach(function(item, i) {
				var DPForm = form.cloneNode(true);
					DPForm['id'].value = item.id;
					DPForm['from'].value = item.from;
					DPForm['price'].value = item.price;
					//DPForm['delivery_group_id]'].value = item.delivery_group_id;
					DPForm['delete'].addEventListener('click', deleteRow, false);
					DPForm['submit'].addEventListener('click', updateRow, false);
				if (i == 0) DPForm.insertBefore(legengDiv, DPForm.firstElementChild);
				forms.appendChild(DPForm);
			});

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						forms.removeChild(form);
					});
				} else {
					var data = {action:'deleteDeliveryPrice', id: form['id'].value};
					$.ajax({
						data	: data,
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								forms.removeChild(form);
							} else {
								form['delete'].disabled = false;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				if ('newRow' in form)
					form['action'].value = 'addDeliveryPrice';
				else
					form['action'].value = 'updateDeliveryPrice';

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД'; 
						if ('newRow' in form)
							form['newRow'].parentNode.removeChild(form['newRow']);
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
				return false;
			}
			return forms;		
		}


		function FormFactory(table, menu){
			var instance = this;
				instance.table = table;
				instance.foreignField = table + '_id';
			$.ajax({
				data	:'action=meta&table='+table,
				success	: function(data){
					getForm(data.columns);
				}
			})

			function getForm(columns) {
				var prependSpan = document.createElement('div');
					prependSpan.className = 'input-prepend';
				
				var appendInput = document.createElement('fieldset');
					appendInput.className = 'input-append';
					appendInput.name = 'inputContainer';

				var submitButton = new Button;
					submitButton.setType('success').setName('submit').setIcon('ok').setCallback(updateRow);

				var deleteButton = new Button;
					deleteButton.setType('danger').setName('delete').setIcon('remove').setCallback(deleteRow);

				columns.forEach(function(column){
					var columnInput = document.createElement('input');
						columnInput.type = column.type;
						columnInput.name = column.name;
					
					if ('required' in column && column.required)
						columnInput.required = true;
					
					if (column.type != 'hidden' && 'title' in column && column.title) {
						var columnSpan = document.createElement('span');
							columnSpan.className = 'add-on text';
							columnSpan.appendChild(document.createTextNode(column.title));
						prependSpan.appendChild(columnSpan);
					}
					appendInput.appendChild(columnInput);
				});

				appendInput.appendChild(submitButton.DOMElement);
				appendInput.appendChild(deleteButton.DOMElement);

				form = document.createElement('form');
				form.className = 'ajaxAutoSafe';
				form.appendChild(prependSpan);
				form.appendChild(appendInput);

				form['table'].value = instance.table;
				
				var newRow = document.createElement('input');
					newRow.type = 'hidden';
					newRow.name = 'newRow';
					newRow.value = 1;
					appendInput.appendChild(newRow);

				instance.form = form;
				instance.newRow = newRow;
				instance.prependSpan = prependSpan;

				//if (instance.done instanceof Function) instance.done.call(form);
				instance.onDone.call(form);
			}
			this.doneCallback = [];
			this.onDoneCompleted = false;
			this.done = function(callback){
				if (instance.onDoneCompleted)
					callback.call(instance.form);
				else
					instance.doneCallback.push(callback);
			}
			this.onDone = function(){
				var context = this;
				if (instance.doneCallback.length)
					instance.doneCallback.forEach(function(callback){
						callback.call(context);
					})
				this.onDoneCompleted = true;
			}
			this.setData = function(data) {
				var row = data.list[0];
				if ('id' in row)
					instance.newRow.parentNode.removeChild(instance.newRow);
				for (column in row)
					instance.form[column].value = row[column];
			}
			this.setRelatedTable = function(table){
				instance.relatedTable = table;
				instance.relatedForeignField = table + '_id';
				//instance.relatedForm = new FormFactory(table);
			}
			this.relatedForms = [];
			this.setRelatedTableLegend = function(text){
				instance.relatedTableLegend = text;
			}
			this.setRelatedTableAddText = function(text){
				instance.relatedTableAddText = text;
			}
			this.setForeignKeysTable = function(table){
				instance.foreignKeysTable = table;
				$.ajax({
					data	: {
						action	: 'select',
						table	: instance.foreignKeysTable,
						where	: instance.foreignField + '=' + instance.id
					},
					success	: function(data) {
						var ID = [];
						data.list.forEach(function(item) {
							ID.push(item[instance.relatedForeignField]);
						})
						if (ID.length > 0) {
							if (ID.length === 1) ID = ID[0];
							$.ajax({
								data : {
									action	: 'select',
									table	: instance.relatedTable,
									id		: ID
								},
								success : function(data) {
									var index = 0;
									data.list.forEach(function(item){
										var newRelatedForm = new FormFactory(instance.relatedTable);
											newRelatedForm.done(function(){
												newRelatedForm.setData({list:[item]});
												if (index > 0) newRelatedForm.form.removeChild(newRelatedForm.prependSpan);
												if (instance.relatedFormDone instanceof Function)
													instance.relatedFormDone.call(newRelatedForm.form);
												index++;
											});
									});
									
								} 
							})
						}

						var legend = document.createElement('legend');
							legend.appendChild(document.createTextNode(instance.relatedTableLegend));
						var i = document.createElement('i');
							i.className = 'icon-white icon-plus';
						var a = document.createElement('a');
							a.className = 'btn btn-primary add';
							a.appendChild(i);
							a.appendChild(document.createTextNode(' ' + instance.relatedTableAddText));
							a.addEventListener('click', function(){
								var newForm = new FormFactory(instance.relatedTable);
									newForm.done(function(){
										//newRelatedForm.form.removeChild(newRelatedForm.prependSpan);
										if (instance.relatedFormDone instanceof Function)
											instance.relatedFormDone.call(this);
									})
							}, false);
						
						instance.relatedFormLegend = legend;
						instance.relatedFormA = a;
						
						instance.done(function(){
							instance.form.appendChild(instance.relatedFormLegend);
							instance.form.appendChild(instance.relatedFormA);
						})
					}
				})
			}
			this.setID = function(id){
				instance.id = id;
			}

			this.htmlInserted = false;

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						form.parentNode.removeChild(form);
					});
				} else {
					var data = {
						action	: 'delete',
						table	: instance.table,
						id		: form['id'].value
					};
					$.ajax({
						data	: data,
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								form.parentNode.removeChild(form);
							} else {
								form['delete'].disabled = false;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate, action = 'update';
				if (!validate(form)) return false;
				if ('newRow' in form) action = 'insert';
				form['action'].value = action;

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД'; 
						if ('newRow' in form)
							form['newRow'].parentNode.removeChild(form['newRow']);
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
				return false;
			}

		}
		function Button(){
			var button = document.createElement('button');
			button.setAttribute('type', 'button');
			
			this.DOMElement = button;
			
			this.setType = function(type){
				button.className = 'btn btn-'+type;
				return this;
			}
			this.setName = function(name){
				button.name = name;
				return this;
			}
			this.setIcon = function(type){
				var icon = document.createElement('i');
				icon.className = 'icon-white icon-'+type;
				button.appendChild(icon);
				return this;
			}
			this.setCallback = function(callback){
				button.addEventListener('click', callback, false)
				return this;
			}
		}

		


		function DeliveryGroupForm(data) {
			var div	= document.createElement('div'),
				legengDiv = document.createElement('div'),
				spanName = document.createElement('span'),
				spanSort = document.createElement('span'),
				spanBasePrice = document.createElement('span'),
				form	= document.createElement('form'),
				inputID = document.createElement('input'),
				inputAction = document.createElement('input'),
				inputName	= document.createElement('input'),
				inputSort	= document.createElement('input'),
				inputBasePrice = document.createElement('input'),
				buttonSubmit = document.createElement('button'),
				buttonDelete = document.createElement('button'),
				icon_ok		= document.createElement('i'),
				icon_remove = document.createElement('i'),
				dataValues = data || null;

			inputID.type = 'hidden';
			inputID.name = 'id';

			inputAction.type = 'hidden';
			inputAction.name = 'action';

			inputName.required = true;
			inputName.type = 'text';
			inputName.name = 'name';
			
			inputSort.required = true;
			inputSort.type = 'number';
			inputSort.name = 'sort';

			inputBasePrice.required = true;
			inputBasePrice.type = 'number';
			inputBasePrice.name = 'base_price';

			icon_ok.className = 'icon-ok icon-white';
			icon_remove.className = 'icon-remove icon-white';

			buttonSubmit.className = 'btn btn-success';
			buttonSubmit.name = 'submit';
			buttonSubmit.setAttribute('type', 'button');
			buttonSubmit.appendChild(icon_ok);
			buttonSubmit.addEventListener('click', updateRow, false);

			buttonDelete.className = 'btn btn-danger';
			buttonDelete.name = 'delete';
			buttonDelete.setAttribute('type', 'button');
			buttonDelete.appendChild(icon_remove);
			buttonDelete.addEventListener('click', deleteRow, false);
			
			spanName.className = 'add-on text';
			spanName.appendChild(document.createTextNode('Имя'));
			spanSort.className = 'add-on number';
			spanSort.appendChild(document.createTextNode('Сортировка'));
			spanBasePrice.className = 'add-on number';
			spanBasePrice.appendChild(document.createTextNode('Базовая цена'));

			legengDiv.className = 'input-prepend';
			legengDiv.appendChild(spanName);
			legengDiv.appendChild(spanSort);
			legengDiv.appendChild(spanBasePrice);

			div.className = 'input-append';
			div.appendChild(inputID);
			div.appendChild(inputAction);
			div.appendChild(inputName);
			div.appendChild(inputSort);
			div.appendChild(inputBasePrice);
			div.appendChild(buttonSubmit);
			div.appendChild(buttonDelete);

			form.className = 'ajaxAutoSafe';
			form.appendChild(legengDiv);
			form.appendChild(div);

			if (dataValues) for (i in dataValues) {
				form[i].value = dataValues[i];
				form[i].placeholder = dataValues[i];
			} else {
				var newRow = document.createElement('input');
				newRow.type = 'hidden';
				newRow.name = 'newRow';
				newRow.value = 1;
				div.appendChild(newRow);
			}

			function beforeSend(){
				this['delete'].disabled = true;
				this['submit'].disabled = true;
			}
			function deleteRow(e){
				var form = this.form;
				alert.confirm.call(form, 'Удалить?', deleteRowConfirmed);
			}
			function deleteRowConfirmed(formToDelete) {
				var form = formToDelete;
				form['action'].value = 'deleteDeliveryGroup';
				
				if ('newRow' in form) {
					$(form).animate({opacity:0}, 1000, function(){
						form.parentNode.removeChild(form);
					});
				} else {
					$.ajax({
						data	: $(form).serialize(),
						beforeSend: beforeSend.apply(form),
						success	: function(data) {
							var message = 'Ошибка, на сервере БД'; 
							if ('rowsAffected' in data.success) {
								switch (data.success.rowsAffected) {
									case 0	: message = 'Нечего удалять.'; break;
									case 1	: message = 'Диапазон цен успешно удален.'; break;
									default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
								}
								form.parentNode.removeChild(form);
								dropdownMenu.find('a[data-id=#id]'.replace('#id', form['id'].value)).parent().remove();
								$modal.modal('hide');
							} else {
								form['delete'].disabled = true;
								form['submit'].disabled = true;
							}
							alert.message(message);
						}
					})
				}
			}
			function updateRow(e) {
				var form = this.form;
				alert.confirm.call(form, 'Обновить?', updateRowConfirmed);
			}
			function updateRowConfirmed(formToUpdate) {
				var form = formToUpdate;
				if (!validate(form)) return false;
				if ('newRow' in form)
					form['action'].value = 'addDeliveryGroup';
				else
					form['action'].value = 'updateDeliveryGroup';

				$.ajax({
					data	: $(form).serialize(),
					beforeSend: beforeSend.apply(form),
					success	: function (data){
						var message = 'Ошибка, на сервере БД';
						if ('rowsAffected' in data.success) {
							switch (data.success.rowsAffected) {
								case 0	: message = 'Нечего обновлять.'; break;
								case 1	: message = 'Диапазон цен успешно обновлен.'; break;
								default	: message = data.success.rowsAffected + ' строк затронуто. Ошибка';
							}
							form['submit'].disabled = false;
							form['delete'].disabled = false;
							if ('newRow' in form) {
								form['newRow'].parentNode.removeChild(form['newRow']);
								var inputID = document.createElement('input');
								inputID.type = 'hidden';
								inputID.name = 'id';
								inputID.value = data.success.id;
								form.appendChild(inputID);
								menu.add({
									name: form['name'].value,
									id	: data.success.id,
									href: '#editDeliveryGroup'
								});
							}
						} else {
							form['delete'].disabled = true;
							form['submit'].disabled = true;
						}
						alert.message(message);
					}
				})
			}

			return form;
		}		

		dropdownMenu.on('click', function(e){
			var a = e.target;
			$(a).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
			switch(a.hash) {
				case '#add':
					$modal.$header.html('Добавить группу доставки');
					$modal.$body.html(DeliveryGroupForm());
					$modal.$footer.html(alert);
					break;
				case '#editDeliveryGroup':
					$modal.$header.html('Редактировать группу доставки');
					$modal.$footer.html(alert);
					$.ajax({
						data	: 'action=getDeliveryGroup&id=' + $(a).data('id'),
						beforeSend	: function(jqXHR, settings) {
							alert.message('Загружаем информацию');
						},
						success	: function(data) {
							var div = document.createElement('div');
							div.appendChild(DeliveryGroupForm(data.DG));
							div.appendChild(DeliveryPriceForms(data.DPList, data.DG.id));
							$modal.$body.html(div);
							alert.message('Информация загружена');
						}
					});
					break;
				default: console.log('no case for', a.hash, a);
			}
			$modal.modal();
			return false;
		});


		function menu(id){
			var ul	= document.querySelector('#'+id+' ul'),
				li	= document.createElement('li'),
				a	= document.createElement('a'),
				i	= document.createElement('i'),
				divider = li.cloneNode(true);

			i.classList.add('icon-pencil');
			a.href="#edit";
			a.appendChild(i);
			a.appendChild(document.createTextNode(''));
			li.appendChild(a);

			divider.classList.add('divider');
			ul.appendChild(divider);

			this.ul = ul;
			this.add = function(item){
				var newLi = li.cloneNode(true),
					a = newLi.lastElementChild;

				if ('href' in item) a.href = item.href;
				a.lastChild.nodeValue = ' ' + item.name;
				a.setAttribute('data-id', item.id);
				ul.appendChild(newLi);
			}
		}
		var deliveryGroupMenu = new menu('delivery-group');
		var gabaritsGroupMenu = new menu('gabarits-group');

		var cache = {};
		$(deliveryGroupMenu.ul).on('click', function(e){
			var a = e.target;
			$(a).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
			switch(a.hash) {
				case '#add':
					$modal.$header.html('Добавить группу доставки');
					$modal.$body.html(DeliveryGroupForm());
					$modal.$footer.html(alert);
					break;
				case '#edit':
					$modal.$header.html('Редактировать группу доставки');
					$modal.$footer.html(alert);
					$.ajax({
						data	: 'action=getDeliveryGroup&id=' + $(a).data('id'),
						beforeSend	: function(jqXHR, settings) {
							alert.message('Загружаем информацию');
						},
						success	: function(data) {
							var div = document.createElement('div');
							div.appendChild(DeliveryGroupForm(data.DG));
							div.appendChild(DeliveryPriceForms(data.DPList, data.DG.id));
							$modal.$body.html(div);
							alert.message('Информация загружена');
						}
					});
					break;
			}
			$modal.modal();
			return false;
		});
		$(gabaritsGroupMenu.ul).on('click', function(e){
			var a = e.target;
			$(a).closest('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
			switch(a.hash) {
				case '#add':
					$modal.$header.html('Добавить группу габаритов');
					$modal.$footer.html(alert);
					var cat_gabarits = new FormFactory('cat_gabarits');
					cat_gabarits.done(function(){
						$modal.$body.html(this);
					});
					break;
				case '#edit':
					$modal.$header.html('Редактировать группу габаритов');
					var id = $(a).data('id');
					var catGabarits = new FormFactory('cat_gabarits');
						catGabarits.done(function(){
							$modal.$body.html(this);
							$.ajax({
								data	: {
									action : 'select',
									table  : 'cat_gabarits',
									id	   : id
								},
								success : catGabarits.setData
							})
						});
						catGabarits.setID(id);
						catGabarits.setRelatedTable('delivery_price');
						catGabarits.setRelatedTableLegend('Диапазоны цен');
						catGabarits.setRelatedTableAddText('Добавить новый диапазон');
						catGabarits.setForeignKeysTable('cat_gabarits_2_delivery_price');

					
					catGabarits.relatedFormDone = function(){
						$modal.$body.append(this);
					}
					
					$modal.$footer.html(alert);
					break;
			}
			$modal.modal();
			return false;
		});



		$.ajax({
			data	: { action : 'select', table : 'delivery_group'},
			success : function(data) {
				data.list.forEach(deliveryGroupMenu.add);
			}
		})
		$.ajax({
			data	: { action : 'select', table : 'cat_gabarits'},
			success : function(data) {
				data.list.forEach(gabaritsGroupMenu.add);
			}
		})
		$.ajax({
			url	: '/edit/m_cat/delivery/get_delivery_group_json.php',
			dataType: 'json',
			success : processDeliveryGroup
		});
	})
})(jQuery)