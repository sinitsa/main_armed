<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');

if (!defined('IN_UFO')) {
    define('IN_UFO', TRUE);
}

if (!defined('ROOT_DIR')) {
    define('ROOT_DIR', dirname(__FILE__) . '/');
}

if (!defined('EXTRA_DIR')) {
    define('EXTRA_DIR', ROOT_DIR . 'components/');
}

if (file_exists(EXTRA_DIR . 'multidomain/common.php')) {
    define('MIXDOM', TRUE);
    include ( EXTRA_DIR . 'multidomain/common.php' );
} else{
    echo 'Initialize file doesn`t exists or some paths doesn`t set correctly!';
}