<?php
namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;
use app\models\ConfigModel;

class BasketModel extends BaseModel {

	public $data;

	function __construct() {
		$this->data = self::getData();
	}
		
	public static function getData() {

		$basket = json_decode(filter_input(INPUT_COOKIE, 'basket'));
		$list=[];
        
		if ($basket) {
			foreach ($basket->items as $value) {
				$list[$value->id] = $value->count;
			}
			return ['count' => $basket->tcount, 'summ' => $basket->tprice, 'list' => $list];
		}
		return ['count' => 0, 'summ' => 0, 'list' => []];
	}

	public function getProducts() {	
		$list = '';
		$output = [];
		if (count($this->data['list'])) {
			foreach ($this->data['list'] as $key => $value) {
				$list = $list . (string)((int)$key) . ',';
			}
			$list = substr($list, 0, -1);
			$query = DB::query(
				"SELECT 
					P.`title`,
					P.`price`,
					P.`id`,
					P.`chpu`,
                    C.`title` AS cat_title
				FROM 
					`catalog` AS P
                LEFT JOIN
                    `cat` AS C
                ON
                    P.`cat` = C.`id`
				WHERE 
					P.`id` IN ({$list})");
			while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
				$output[] = array_merge($row, ['count' => $this->data['list'][$row['id']]]);
			}
		}
		return $output; 
	}

	/**
	 * [getSummProducts description]
	 * @param  [type] $list [description]
	 * @return [type]       [description]
	 */
	public function getSummProducts($list) {
		$summ = 0;
		foreach ($list as $value) {
			$summ = $summ + ($value['price'] * $value['count']); 
		}
		return $summ;
	}

	/**
	 * [deliverySumm расчет доставки]
	 * @param  [int] $type [тип доставки] - пока не работает
	 * @param  [int] $summ [сумма заказа]
	 * @return [int]       [стоимость доставки]
	 */
	private static function deliverySumm($summ, $type = 0) {
		if ($summ < 2000) {
			return 300;
		}
		return 0;
	}

	/**
	 * [sqlSet формирование строки из массива  для  SQL INSERT/UPDATE]
	 * @param  [array] $array
	 * @return [string]       
	 */
	private static function sqlSet($array) {
		$set = '';
		foreach ($array as $key => $value) {
			$set .= "`{$key}` =:{$key}, ";
		}
		return substr($set, 0, -2);
	}

	/**
	 * [mailToClient отправка письма клиенту о совершенном заказе]
	 * @param  [string] $name    [Имя]
	 * @param  [string] $mail    [description]
	 * @param  [int] $orderId [номер заказа]
	 * @return [boolean]          []
	 */
	private static function mailToClient($name, $mail, $orderId) {

		$config = new ConfigModel();	
		$from =  $config->get('site.email_from');
		$email_support = $config->get('site.email_support');
		$site_name =  $config->get('site.name');
		$site_url =  $config->get('site.web_addr');	
		$orderKey = 'B-' . $orderId;

		$to      = $mail;
		$subject = "Вашему заказу присвоен номер {$orderKey} ({$site_url})";
		
		// Дополнительные заголовки
    	$headers = "Content-type: text/html; charset=utf-8 \r\n";
    	$headers .= "To: {$name} {$mail}\r\n";
    	$headers .= "From: {$from}\r\n";

    	$message = "
	    	<html>
	    	<head>
	    	  <title>Оформлен заказ на {$site_name}</title>
	    	</head>
	    	<body>
	    	  <p>{$name}, мы рады, что вы выбрали именно наш интернет магазин. Вашему заказу присвоен номер <strong>{$orderKey}</strong></p>
	    	  <p>Если с обработкой Вашего заказа возникли проблемы - пишите на {$email_support}</p>
	    	  <p>В ближайшее время, для подтверждения заказа, с Вами свяжется наш менеджер.</p>
	    	  <br>
	    	  <br>
	    	  <p>С уважением, {$site_name}.</p
	    	</body>
	    	</html>";

    	unset($config);
		return mail($to, $subject, $message, $headers);

	}

	/**
	 * [makeOrder оформление заказа]
	 * @param  [string] $name        [description]
	 * @param  [string] $phone       [description]
	 * @param  [string] $email       [description]
	 * @param  [int]    $pay_type    [description]
	 * @param  [int]    $del_type    [description]
	 * @param  [string] $comment     [description]
	 * @param  [string] $ref         [description]
	 * @param  [array]  $basket_list [description]
	 * @param  [array]  $other       [description]
	 * @return [array]               [description]
	 */
	public static function makeOrder($name, $phone, $email, $adress, $date_delivery, $time_ot, $time_do, $index, $pay_type, $del_type, $comment, $ref, $basket_list, $other = []) {
		$messages =[]; 
		$db_products = [];

		// формирование запрса о всех товарах корзины
		
		foreach ($basket_list as $key => $value) {
			$db_products[] = $value['id']; 
		}
		$db_products = implode(',', $db_products);
		$query = DB::query(
			"SELECT
				P.`id`,
				P.`title`,
				P.`price`,
                C.`title` AS cat_title
 			FROM
				`catalog` AS P
            LEFT JOIN
                `cat` AS C
            ON 
                P.`cat` = C.`id` 
			WHERE
				P.`id` IN ({$db_products})");
		$db_products = [];
		while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$db_products[$row['id']] = [
				'id' 	=> $row['id'], 
				'price' =>  $row['price'], 
				'title' => $row['title'],
                'cat_title' => $row['cat_title'],
			];
		}

		// заполнение данными products
		$products = [];
        
        $output = [];
		$summ = 0;
		foreach ($basket_list as $key => $value) {
			//проверка на существовании цены  товара в базе
			if (!$db_products[$value['id']]['price'] > 0) {
				$message[] = 'Найдена ошибка в данных заказа. Заказ не оформлен!';
				return ['id' => 0, 'error' => true, 'message' => $messages];
			}
			$products[] = [
				'id' => $value['id'],
    			'amount' => $value['count'],
    			'price' => $db_products[$value['id']]['price'],
    			'price_after_discount' => $db_products[$value['id']]['price'],
			];
			$summ += $db_products[$value['id']]['price']*$value['count'];
            
            // массивна выход для статистики
            $output[] = [
				'id' => $value['id'],
                'name' => $db_products[$value['id']]['title'],
                'category' => $db_products[$value['id']]['cat_title'],
    			'quantity' => $value['count'],
    			'price' => $db_products[$value['id']]['price'],
                
			];
		}

		// заполнение данными order price
		$price =[
			'price_after_global_discount' => $summ,
			'price_before_global_discount' => $summ,
			'delivery_price' => self::deliverySumm($summ),
		];

		if ($other['extra'] == 'fast_order') {
			$extra = 'Быстрый заказ';
		} else {
			$extra = '';
		}

		$data = [
			'date' 			=> time(),
			'products'	 	=> serialize($products),
			'order_price'	=> serialize($price), 
			'name'			=> $name, 
			'delivery_type' => $del_type,  
			'payment_type'	=> $pay_type, 
			'phone'			=> $phone, 
			'email'			=> $email, 
			'adress'		=> $adress." индекс ".$index, 
			//'time_date'		=> $date_delivery." время с ".$time_ot." до ".$time_do,
			'extra_information' => $extra,
			'comment' 		=> $comment,
			'h'				=> $ref,
		];
        
		$query	= DB::prepare("INSERT INTO `orders` SET" . self::sqlSet($data));
		if ($query->execute($data)) {
			$lastId = DB::query("SELECT LAST_INSERT_ID()")->fetch(\PDO::FETCH_NUM);

			if ($email) {
				self::mailToClient($name, $email, $lastId[0]);
			}
           
			return ['id' => $lastId, 'error' => false, 'message' => $messages, 'products' => $output, 'summ' => $summ];
		} else {
			$message[] = 'Данные заказа не записались!'; 
			return ['id' => 0, 'error' => true, 'message' => $messages];
		}
		return true;
	}
}

?>