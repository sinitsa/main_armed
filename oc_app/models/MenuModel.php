<?php 
namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;

/**
* 
*/
class MenuModel extends BaseModel
{
	public function getMenu() {

		$menu = [];
		$query = DB::query("SELECT id, pod, title, cat_id, chpu, rang FROM menuleft ORDER BY `rang` ");
		while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$menu[$row['id']] = $row;
		}
		return $this->getTree($menu);
	}

	//Функция построения дерева из массива от Tommy Lacroix
	function getTree($dataset) {
		$tree = array();

		foreach ($dataset as $id => &$node) {  

			//Если нет вложений
			if (!$node['pod']){
				$tree[$id] = &$node;
			}else{ 
				//Если есть потомки то перебераем массив
	            $dataset[$node['pod']]['submenu'][$id] = &$node;
			}
		}
		return $tree;
	}

    /*
     * вывод данных главной категории
     */    
    public function getMcatInfo($index) {
        
        // выгрузка данных         
        $sql1 = DB::prepare("SELECT * FROM `menuleft` WHERE chpu = ? LIMIT 1");
        $sql1->execute([$index]);
        $mcat = $sql1->fetch(\PDO::FETCH_ASSOC);

        if (isset($mcat['id'])) {
            $sql2 = DB::prepare(
                "SELECT
                    * 
                FROM 
                    `menuleft` 
                WHERE 
                    pod = ? 
					AND (cat_id > 0 OR pod = 0)
                ORDER BY
                    rang");
            $sql2->execute([$mcat['id']]);

            $subcats = [];
            while ($row = $sql2->fetch(\PDO::FETCH_ASSOC)) {
                $subcats[] = $row;
            }
            return [
                'id' => $mcat['id'],
                'title' => $mcat['title'],
                'chpu' => $mcat['chpu'],
                'seo_title' => $mcat['seo_title'],
                'sub_cats' => $subcats,
            ];
        }
        return [];
    }


}