<?php

namespace app\models;

use core\lib\DB;
use core\lib\BaseModel;

class ProductModel extends BaseModel {
    
    public $dbdata;
    
    function __construct($index) {
        $this->getData($index);
    }
    
    /**
     * [getData: чтение банных о товаре из БД и запись  в переменную $dbdata]
     * @param  [строка/число] $index [ индекс товара: чпу или id]
     * @return [boolean]    []
     */
    private function getData($index) {
        $key = 'chpu';
        $query = DB::prepare(
            "SELECT 
                C.*,
                R.`amount`,
                S.title AS cat_title
            FROM 
                `catalog` AS C
            LEFT JOIN
                `cat` AS S
            ON
                C.`cat` = S.`id`
            LEFT JOIN
               `storage_rests` AS R
            ON
                C.`id` = R.`catalog_id`
            WHERE
                C.`{$key}` = :chpu
            LIMIT 1");
        $query->execute([':chpu' => $index]);
        $this->dbdata = $query->fetch(\PDO::FETCH_OBJ);
        return true;
    }
    
    /**
     * [getBreadcrumbs: вывод списка "Хлебных крошек" ]
     * @return [массив] []
     */
    public function getBreadcrumbs() {
		$query = DB::prepare(
			"SELECT 
                A.title, A.chpu, B.title, B.chpu
			FROM 
                menuleft AS A
			LEFT JOIN 
                menuleft AS B 
            ON 
                A.pod = B.id
			WHERE 
                A.cat_id = ? limit 1");
        $query->execute([$this->dbdata->cat]);
        $result = $query->fetch(\PDO::FETCH_NUM);
        return array(
			array('title' => $result[2], 'link' => '/mcat/' . $result[3] . '.html'),
			array('title' => $result[0], 'link' => '/cat/' . $result[1] . '.html'),
		);
    }
    
    /**
     * [getImages: вывод списка всех картинок товара]
     * @return [массив] [список URL на картинки, первый элемент - главная картинка]
     */
    public function getImages() {
        $links = ['/images/' . $this->dbdata->id . '.jpg'];
        $query = DB::prepare(
            "SELECT
                id
            FROM
                `foto`
            WHERE
                `catalog_id` = :id");
        
        $query->execute([':id' => $this->dbdata->id]);
        while ($row = $query->fetchColumn()) {
            $links[] = '/images/more/' . $row . '.jpg'; 
        }
        return $links;
    }
    
    /**
     * пользовательская функция конвертации строки  cp1251 -> utf-8
     * @param type $str
     * @return type
     */
    private function conv($str) {
        
        if (mb_detect_encoding($str,'utf-8', true)) {
            return $str;
        }
        return iconv('cp1251','utf-8', $str);
        
    }


    /**
     * [getFeatures: вывод списка "Особенности товара"]
     * @return [массив] []
     */
    public function getFeatures() { 
        $list = @unserialize($this->dbdata->features); 
        
        // супер мего костыль для наших сериализованных ГОВНОданных
        $list = @unserialize($this->dbdata->features);
        if ($list === false) {
            $list = @unserialize(iconv('utf-8','cp1251', $this->dbdata->features));
            $list = array_map([$this, 'conv'], $list);
        }


        $output = [];
        if (!empty($list['feat'])) {
            $output['Характеристики'] = preg_split('/(\n)/', $list['feat']);
        }
        if (!empty($list['char'])) {
            $output['Особенности'] = preg_split('/(\n)/', $list['char']);
        }
        if (!empty($list['compl'])) {
            $output['Комплектация'] = preg_split('/(\n)/', $list['compl']);
        }
        return $output;
    }

    /**
     * [getFeedbacks: вывод списка отзывов о товаре]
     * @return [массив]
     */
    public function getFeedbacks() {
        $list =[];
        $query = DB::prepare(
            "SELECT
                `name`,
                `date`,
                `comment`             
            FROM 
                `otzyv`
            WHERE
                `confirm` = 1 AND `catalog_id` = ?");
        $query->execute([$this->dbdata->id]);
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $list[] = [
                'name'      => $row['name'],
                'date'      => $row['rand_date'],
                'content'   => $row['comment']
            ];
        }
        return array_reverse($list);
    }

    /**
     * [getAnotherProducts вывод похожих товаров]
     * @param  [type] $count [description]
     * @return [type]        [description]
     */
    public function getAnotherProducts($count) {
        $rang = $this->dbdata->spec_rang;
        $query = DB::prepare(
            "SELECT 
                `spec_rang`
            FROM
                `catalog`
            WHERE
                cat = ?
            ORDER BY
                `spec_rang` DESC
            LIMIT 1");
        $query->execute([$this->dbdata->cat]);
        $max_rang = $query->fetchColumn();
        $min = $rang-(round($count/2, 0, PHP_ROUND_HALF_UP));
        $max = $rang+(round($count/2, 0, PHP_ROUND_HALF_DOWN));
        
        if ($max > $max_rang) {
            $max = $max_rang;
            $min = $min - $max + $max_rang - 1;
        }

        if ($min < 0 ) {
            $max = $max - $min;
            $min = 0; 
        }

        $list = range($min, $max);
        unset($list[array_search($rang, $list)]);
        $list = implode(',', $list);
        $query = DB::prepare(
            "SELECT
                `id`,
                `title`,
                `price`,
                `chpu`
            FROM
                `catalog`
            WHERE spec_rang IN ({$list}) AND `cat` = :cat ");
        $query->bindValue(':cat', $this->dbdata->cat, \PDO::PARAM_INT);
        $query->execute();
        $list = [];
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            $list[] = [
                'id'    => $row['id'],
                'title' => $row['title'],
                'chpu'  => $row['chpu'],
                'price' => $row['price'],
            ];
        }
        return $list;
    }

    public function getRecommendProducts($index, $limit) {

        $result = [];
        $notArr = [];

        if (is_array($index)) {
            $index = implode(',', $index);  
        } 

        $query = DB::query( 
            "SELECT 
                `catalog_id2`,
                `s1`.`amount` as `a1`, 
                `s2`.`amount` as `a2`, 
                `count`, `catalog`.*,
                (SELECT 
                    count(`otzyv`.`id`) 
                FROM 
                    `otzyv` 
                WHERE 
                    `otzyv`.`catalog_id` = `same_products_final`.`catalog_id2` AND `confirm` = 1) as feedbackCount

            FROM 
                `same_products_final` 
            LEFT JOIN 
                `catalog` 
            ON 
                `catalog`.`id` = `same_products_final`.`catalog_id2` 
            LEFT JOIN 
                `storage_rests` as `s1` 
            ON 
                `s1`.`catalog_id` = `same_products_final`.`catalog_id2` and `s1`.`storage_id`=1
            LEFT JOIN 
                `storage_rests` as `s2` 
            ON 
                `s2`.`catalog_id` = `same_products_final`.`catalog_id2` and `s2`.`storage_id`=2 
            WHERE 
                `count` <> 1 AND `catalog_id1` in ({$index}) 
            ORDER BY 
                `count` DESC 
            LIMIT 
                {$limit}");

        
        while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
            if (!empty($row['title'])) {
                    $result[] = [
                        'chpu'  => $row['chpu'],
                        'title' => $row['title'],
                        'price' => $row['price'],
                        'id'    => $row['id'],
                    ];
                    $notArr[] = $row['id'];
               }   
        }

        $count = count($result);
        if ($count < $limit) {
         
            $limit = $limit - $count;

            if (is_array($index)) {
               $notArr = array_merge($notArr, $index);  
            } else {
                $notArr[] = $index;
            }

            $notArr = implode(',', $notArr);

            $query = DB::query( 
                "SELECT 
                    `same_products_cat`.`catalog_id`,
                    `s1`.`amount` as `a1`, 
                    `s2`.`amount` as `a2`,
                    `catalog`.* ,
                    (SELECT 
                        count(`otzyv`.`id`) 
                    FROM 
                        `otzyv` 
                    WHERE 
                        `otzyv`.`catalog_id` = `same_products_cat`.`catalog_id` AND `confirm` = 1)
                AS 
                    feedbackCount
                FROM 
                    `same_products_cat` 
                LEFT JOIN 
                    `catalog` 
                ON 
                    `catalog`.`id`=`same_products_cat`.`catalog_id` 
                LEFT JOIN 
                    `storage_rests` as `s1` 
                ON 
                    `s1`.`catalog_id` = `same_products_cat`.`catalog_id` AND `s1`.`storage_id`=1
                LEFT JOIN 
                    `storage_rests` as `s2` ON `s2`.`catalog_id` = `same_products_cat`.`catalog_id` AND `s2`.`storage_id`=2 
                WHERE 
                    `cat_id` IN (SELECT `cat` FROM `catalog` WHERE `id` IN ({$index})) AND `same_products_cat`.`catalog_id` NOT IN ({$notArr})  
                ORDER BY 
                    `count` DESC 
                LIMIT  
                    {$limit}");

            
            while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
                if (!empty($row['title'])) {
                    $result[] = [
                        'chpu'  => $row['chpu'],
                        'title' => $row['title'],
                        'price' => $row['price'],
                        'id'    => $row['id'],
                    ];
                }   
            }

        }

        return $result;
    }
}
?>