<?php

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;

class SearchModel extends BaseModel 
{
    private $cl;
    private $index_prefix;
    
    function __construct($index_prefix='') {
        $this->sphinxInit($index_prefix);
    }
    
    /**
     * инициализация  sphinx
     */
    private function sphinxInit($index_prefix) {
        
        // подгрузка библиотеки      
        require_once (ROOT_DIR . 'oc_app/components/sphinxapi.php');
       
        $this->cl = new \SphinxClient();
        $this->index_prefix = $index_prefix;
		// настройки
        $this->cl->SetServer("127.0.0.1", 3312);
		$this->cl->SetConnectTimeout( 1 );
		$this->cl->SetMaxQueryTime(1000);
		$this->cl->SetLimits(0, 5, 5);
		$this->cl->SetMatchMode(SPH_MATCH_ANY);
		// $cl->SetMatchMode(SPH_MATCH_EXTENDED2);
		$this->cl->SetSortMode(SPH_SORT_RELEVANCE);
		$this->cl->SetFieldWeights(array('title' => 20, 'des' => 10));
    }
    /**
     * поиск  sphinx 
     * @param type $query
     * @return array (список id найденых полей)
     */
    private function sphinxSearch($query, $index=false) {
        
		$result = $this->cl->Query($query,$index);
		if( $result === false ){
			if( $this->cl->GetLastWarning() ) {
				die('WARNING: '.$this->cl->GetLastWarning());
			}
            return [];
			//die('ERROR: '.$this->cl->GetLastError());
		}
        return array_keys($result['matches']);
    } 
    
    /**
     * Вывод списка товаров по запросу из поиска
     * @param string $query
     * @return array
     */
    public function getProducts($query) {
        
        if (empty($query)) {
            return [];
        }

        $index = $this->index_prefix . '_product_index';

        $sphinxResult = $this->sphinxSearch($query, $index);
        
        if (count($sphinxResult)) {
            $list = implode(',', $sphinxResult);
            $sql = DB::query(
            "SELECT
                P.`id`,
                P.`title`,
                P.`price`,
                P.`chpu`,
                P.`fake_in_stock`,
                R.`amount` 
            FROM
                `catalog` AS P
            LEFT JOIN 
                storage_rests AS R
            ON 
                R.`catalog_id` = P.`id`    
            WHERE 
                `id` in ({$list}) 
            ORDER BY FIELD (`id`, {$list})");
            $products=[];
            while ($row = $sql->fetch(\PDO::FETCH_ASSOC)) {
                $products[] = $row;
            }    
            return $products;
        }
        return [];   
    }   
}
