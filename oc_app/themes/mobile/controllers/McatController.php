<?php 
namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\CatModel;
use app\models\BasketModel;

/**
* 
*/
class McatController extends BaseController
{

	function action_index(){

		$config = new ConfigModel();	
		$limit = $config->get('catalog.products_on_page_mobile');
		$menu = new MenuModel();
        $menuData = $menu->getMcatInfo($this->content);
		if (isset($menuData['id'])) {
                    
            $data = [
                'basket' => BasketModel::getData(),
                'phone' => $config->get('site.phone'),
				'phone_msk' => $config->get('site.phone_msk'),
				'phone_spb' => $config->get('site.phone_spb'),
                'work_mode'=> $config->get('site.work_mode'),
                'title' => $menuData['title'],
                'menu' => $menu->getMenu(),
                'pagesMenu' => PageModel::getMenu(),
                'subcats' => $menuData['sub_cats'],
                'seo' => [
                    'title' => $menuData['seo_title'], 
                    'describtion' => '', 
                    'keywords' => '',
                ], 
            ];

            $this->view->render('index', 'mcat', $data);		
		} else {
			$this->get404();
		}		
	} 
}
?>