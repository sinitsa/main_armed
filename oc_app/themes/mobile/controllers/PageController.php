<?php 

namespace app\themes\mobile\controllers;

use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\BasketModel;

/**
* 
*/
class PageController extends BaseController
{
	function action_index(){

		$config = new ConfigModel();
		$menu = new MenuModel();
		$page = new PageModel($this->content);

		if (!isset($page->dbdata->title)) {
			$this->get404();
			
		} else {
			$data = [
				'basket' => BasketModel::getData(),
				'breadcrumbs' => [
						
				],
				'phone' => $config->get('site.phone'),
				'phone_msk' => $config->get('site.phone_msk'),
				'phone_spb' => $config->get('site.phone_spb'),
				'work_mode'=> $config->get('site.work_mode'),
				'menu' => $menu->getMenu(),
				'title' => $page->dbdata->title,
				'text' => $page->dbdata->des,
				'pagesMenu' => PageModel::getMenu(),
				'breadcrumbs' => [
					['title' => $page->dbdata->title, 'link' => '/page/'.$page->dbdata->chpu.'.html']
				],
				'seo' => [
					'title' => $page->dbdata->seo_title, 
					'describtion' => $page->dbdata->seo_des, 
					'keywords' => $page->dbdata->seo_key
				]
			];
			$this->view->render('index', 'page', $data);	
		}
	}

	function action_404() {
		$this->get404();		
	}


}