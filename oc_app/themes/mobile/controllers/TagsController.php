<?php 
namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\TagModel;
use app\models\BasketModel;

/**
* 
*/
class TagsController extends BaseController
{

	function action_index(){

		$config = new ConfigModel();	
		$limit = $config->get('catalog.products_on_page_mobile');
		$menu = new MenuModel();
		$tag = new TagModel($this->content);
		if (isset($tag->dbdata->id)) {
			$products = $tag->getProductList($tag->page, $limit);
			$paginator = $tag->getPaginator($limit);

			$checkFilter = filter_input(INPUT_GET, 'page');

			if ((($checkFilter >= 1) and ($checkFilter <= $paginator['max'])) or $checkFilter == null) {
		        
				$data = [
					'basket' => BasketModel::getData(),
					'phone' => $config->get('site.phone'),
					'phone_msk' => $config->get('site.phone_msk'),
					'phone_spb' => $config->get('site.phone_spb'),
					'work_mode'=> $config->get('site.work_mode'),
					'products' => $products,
					'paginator' => $paginator,
                    'id' => $tag->dbdata->id,
		            'title' => $tag->dbdata->title,
					'menu' => $menu->getMenu(),
					'pagesMenu' => PageModel::getMenu(),
					'breadcrumbs' => $tag->getBreadcrumbs(),
					'seo' => [
						'title' => $tag->dbdata->seo_title, 
						'describtion' => $tag->dbdata->seo_description, 
						'keywords' => ''
					], 
				];
				
				$this->view->render('index', 'tag', $data);
			} else {
				$this->get404();
			}		
		} else {
			$this->get404();
		}			
	} 
}
?>