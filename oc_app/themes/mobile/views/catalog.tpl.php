<?php /*
<div class="top">
	<div>
		<a class="back" href="javascript: void(0);">Назад</a>
		<a class="next" href="javascript: void(0);">Следующий товар</a>
	</div>
</div>
 */
?>

<div class="container">
	<ul class="breadcrumbs">
		<li><a href="/">Главная</a> </li>
		<?php foreach ($data['breadcrumbs'] as $value): ?>
			<li>&nbsp;- <a href="<?=$value['link']?>"><?=$value['title']?></a></li>
		<?php endforeach ?>
	</ul>
	<h1 class="cat_title"><?=$data['title'] ?></h1>
    <div class="cart">
        <div class="info">
            <div class="price">
                <span>Цена: </span>
                <span> <?=$data['price']?></span>
                <span>руб. </span>
            </div>
            <div class="available">
                <?php if ((filter_var($data['amount']) > 0) || (filter_var($data['fake_in_stock']) == 1)): ?>
				<div class="info_flags_true available">В наличии</div>
				<?php else: ?>
				<div class="info_flags_false available">на заказ</div>
				<?php endif ?>
            </div>
        </div>
        <div class="lazy_slider">
            <?php foreach ($data['images'] as $image): ?>
            <div class="image"><img class="s_element" data-lazy="<?=$image ?>"></div>
            <?php endforeach ?>               
        </div>
        <a class="in_basket" data-id="<?=$data['id']?>" data-title="<?=$data['title']?>"  data-cat_title="<?=$data['cat_title']?>"  data-price="<?=$data['price']?>" href="javascript: void(0);">В КОРЗИНУ</a>
        <a class="callphone" href="tel:+78005556165">
            <span class="phnicon"></span>
            <span class="phntext">
                <span><?=$data['phone'];?></span></br>
                <span>Позвонить бесплатно</span>
            </span>
        </a>
    </div>
</div>
<div class="panel2">
    <div class="block">
        <div class="delivery">
            <span>Доставка по Москве:</span>
            <?php if($data['price'] >= 2000): ?>
            <span>Бесплатно</span>
            <?php else: ?>
            <span>От 300 руб.</span>    
            <?php endif; ?>   
        </div>
        <div class="time_delivery">
            <span>В течение:</span>
            <span>1-3 дней</span>
        </div>
    </div>
    <div class="separator">
        
    </div>
</div>
<div class="product_des">
    <div class="subtitle">
        <h3>Описание</h3>
    </div>
    <div class="wrapper">
        <div class="t1 active">
            <div class="des_area">
                <?=$data['describtion'];?>
            </div>
        </div>
    </div>
    <?php if(!empty($data['iframe'])) { ?>
        <div class="subtitle"><h3>Видео</h3></div>
        <div id="video_cont"><?=$data['iframe']?></div>
    <?php } ?>
    <?php if (!empty($data['features'])):?>
        <div class="subtitle">
            <h3>Особенности</h3>
        </div>
    <div class="wrapper">
        <div class="t1 feat_area active">
            <?php foreach($data['features'] as $key => $list): ?>
            <h4><?=$key?></h4>
            <ul>
                <?php foreach($list as $item): ?>
                <li>
                    <span><?=$item ?></span>
                </li>
                <?php endforeach;?>
            </ul>
            <?php endforeach; ?>
        </div>
    </div>
    <?php endif;?>
</div>

<!-- Callback button -->



<!-- feedbacks -->
<?php if (!empty($data['feedbacks'])): ?>
<div class="subtitle">
    <h3>Отзывы покупателей</h3>
</div>    
<div class="feedbacks">
    <?php foreach ($data['feedbacks'] as $item): ?>
    <div class="wrap">
        <div class="head">
            <span class="name"><?=$item['name']?></span>
            <span class="date"><?=$item['date']?></span>
        </div>
        <div class="body">
            <?=$item['content']?>
        </div>
    </div>   
    <?php endforeach ?>
</div>   
<?php endif ?>
<!-- Personal recommendation -->
<?php if (!empty($data['recommendProducts'])): ?>
<div class="subtitle">
    <h3>Персональные Рекомендации</h3>
</div>    
<div class="bot_listing">
    <?php foreach ($data['recommendProducts'] as $item): ?>
    <div class="element">
        <a class="img" href="/catalog/<?=$item['chpu']?>.html">
            <img class="lazy" data-src="/images/<?=$item['id']?>.jpg" src="" alt="<?=$item['title'] ?>">
        </a>
        <p class="name"><?=$item['title']?></p>
        <p class="price"><?=$item['price']?> руб.</p>
    </div>    
    <?php endforeach ?>
</div>
<?php endif ?>
<!-- Another Products -->
<?php if (!empty($data['anotherProducts'])): ?>
<div class="subtitle">
    <h3>Похожие товары</h3>
</div>    
<div class="bot_listing">
    <?php foreach ($data['anotherProducts'] as $item): ?>
    <div class="element">
        <a class="img" href="/catalog/<?=$item['chpu']?>.html">
            <img class="lazy" data-src="/images/<?=$item['id']?>.jpg" src="" alt="<?=$item['title'] ?>">
        </a>
        <p class="name"><?=$item['title']?></p>
        <p class="price"><?=$item['price']?> руб.</p>
    </div>    
    <?php endforeach ?>
</div>
<?php endif ?>

<div class="bottom_separator"></div>

<div class="container">
    <?php
        //  блок информации о сайте: телефоны, время работы, способы оплаты
        include 'widgets/site_info.tpl.php'
    ?>
</div>