
<div class="catalog_area">
	<?php foreach ($data['menu'] as $section): ?>
		<?php foreach ($section['submenu'] as $category): ?>
			<?php if ($category['cat_id'] == 0) continue ; ?> 
			<div class="category">
				<?php if (file_exists($_SERVER["DOCUMENT_ROOT"]."/images/cat/pod/{$category['cat_id']}.jpg")): ?>
					<a class="image lazy" data-src="/images/cat/pod/<?=$category['cat_id']?>.jpg" href="<?=$category['chpu']?>.html"></a>
				<?php else: ?>
					<a class="image lazy <?=$category['cat_id']?>" data-src="/mobile/asset/img/noimage.png" href="<?=$category['chpu']?>.html"></a>
				<?php endif ?>
				<a class="name" href="<?=$category['chpu']?>.html">

					<?=$category['title'] ?>
				</a>
			</div>
		<?php endforeach ?>
	<?php endforeach ?>

</div>
<div class="main_container">
	<a class="show_all_catalogs" href="javascript:void(0);" ><span> Показать еще</span> <span class="arrow">&nbsp;</span></a>

	<ul class="pages_list">
		<?php foreach ($data['pagesMenu'] as $value): ?>
			<li><a href="/page/<?=$value['chpu']?>.html"><?=$value['title']?></a></li>
		<?php endforeach ?>
	</ul>

	<?php
        //  блок информации о сайте: телефоны, время работы, способы оплаты
        include ('widgets/site_info.tpl.php') ;
    ?>
</div>
