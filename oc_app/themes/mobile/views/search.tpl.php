<?php 

// поиск категории по chpu

// $breadcrumb = getBreadcrumbs( $data['catInfo']['id']);
// $array_phone = explode(')', $phone);
// $array_phone_spb = explode(')', $phone_spb);
// $available = $product['rests_main']['summ'];
$sort = filter_input(INPUT_GET, 'sort');
 ?>

<div class="top">
	<div>
		<a class="back" href="javascript: void(0);">Назад</a>
		<!-- <a href="javascript: void(0);">Фильтр</a> -->
	</div>
</div>
<div class="container">
	<h1 class="cat_title"><?=$data['title'] ?>:</h1>

    <?php if (count($data['products'])): ?>
	<ul class="catlist">
		<?php foreach ($data['products'] as $item): ?>
			<li>
				<div class="list_element_cart">
			
						<a class="img" href="/catalog/<?=$item['chpu']?>.html">
							<img class="lazy" data-src="/images/<?=$item['id']?>.jpg" src="" alt="<?=$item['title'] ?>">
						</a>
					
					<div class="info">
						<a href="/catalog/<?=$item['chpu']?>.html" class="title"><?=$item['title'] ?></a>
						<div>
							<?php if ((filter_var($item['amount']) > 0) || (filter_var($item['fake_in_stock']) == 1)): ?>
								<div class="info_flags_true available">В наличии</div>
							<?php else: ?>
								<div class="info_flags_false available">на заказ</div>
							<?php endif ?>
								
							<?php if ($item['price'] > 2000): ?>
								<div class="info_flags_true delivery">Бесплатно</div>
							<?php else: ?>
								<div class="info_flags_false delivery">от 300 руб.</div>
							<?php endif ?>
								
						</div>
						<div class="price">
							<span>Цена:</span>
							<span><?=$item['price'] ?></span>
							<span>руб.</span>
						</div>
						<a class="in_basket" data-id="<?=$item['id']?>" data-price="<?=$item['price']?>" href="javascript: void(0);">В корзину</a>
						<!--<a href="javascript: void(0);" class="feedback_count">34 отзыва</a>-->
					</div>
				</div>
				
			</li>
		<?php endforeach ?>	
	</ul>
    <?php else: ?>
    <p>ничего не найдено</p>
    <?php endif ?>
</div>
<div class="bottom_separator"></div>
    
	<!--<a class="show_all_products" href="javascript:void(0);" ><span> Показать еще</span> <span class="arrow">&nbsp;</span></a>-->
<div class="container">
    <?php
        //  блок информации о сайте: телефоны, время работы, способы оплаты
        include 'widgets/site_info.tpl.php'
    ?>
</div>
</div>