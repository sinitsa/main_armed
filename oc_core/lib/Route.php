<?php 

namespace core\lib;

use core\lib\Route;
/**
* 
*/
class Route {

	private $theme = THEME;

	// роутинг для старой версии (использует старый  htaccess)
	public static function start_old() {

		$tab = isset($_GET['t']) ? $_GET['t'] : 'main';
		$content = isset($_GET['c']) ? $_GET['c'] : '';

		$classController = 'app\themes\mobile\controllers\\' . ucfirst($tab . "Controller");

		if ($tab == 'ajax') {
			$action = 'action_' . $content;
		} elseif ($tab == 'page') {
			$action = $content == '404' ? 'action_404' : 'action_index';
		} else {
			$action = 'action_index';
		}

		$controller  = new  $classController($content, $tab);

		if(method_exists($controller, $action)){
			$controller->$action();
		} else {
			Route::page404();
		}

		return true;
	}

	// роутинг для новой версии
	public static function start() {
		$routes = explode('/', $_SERVER['REQUEST_URI']);
		print_r($routes);	
		return false;
	}

	public static function page404() {
		header('HTTP/1.1 404 Not Found');
		$controller  = new  PageController();
		$controller->action_404();
		return false;
	}

}