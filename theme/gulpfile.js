var gulp = require('gulp');
var concat = require('gulp-concat');
// var uglify = require('gulp-uglify');
var csso = require('gulp-csso');
// var imagemin = require('gulp-imagemin');

gulp.task('minImg', function(){
	gulp.src('img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('img-min/'));
});

gulp.task('minBig', function(){
	gulp.src('../upload/big/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('../upload/big-min/'));
});
gulp.task('minMedium', function(){
	gulp.src('../upload/medium/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('../upload/medium-min/'));
});
gulp.task('minSmall', function(){
	gulp.src('../upload/small/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('../upload/small-min/'));
});
gulp.task('minOrigin', function(){
	gulp.src('../upload/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('../upload/origin-min/'));
});

// доп фото
gulp.task('minMoreOrigin', function(){
	gulp.src('../upload/more/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('../upload/more/origin-min/'));
});
gulp.task('minMoreBig', function(){
	gulp.src('../upload/more/big/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('../upload/more/big-min/'));
});
gulp.task('minMoreMedium', function(){
	gulp.src('../upload/more/medium/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('../upload/more/medium-min/'));
});
gulp.task('minMoreSmall', function(){
	gulp.src('../upload/more/small/*.jpg')
		.pipe(imagemin())
		.pipe(gulp.dest('../upload/more/small-min/'));
});

	// gulp.src('../upload/medium/*.jpg')
	// 	.pipe(imagemin())
	// 	.pipe(gulp.dest('../upload/medium-min/'));

	// gulp.src('../upload/small/*.jpg')
	// 	.pipe(imagemin())
	// 	.pipe(gulp.dest('../upload/small-min/'));

	// gulp.src('../upload/more/*.jpg')
	// 	.pipe(imagemin())
	// 	.pipe(gulp.dest('../upload/more/origin-min/'));

	// gulp.src('../upload/more/big/*.jpg')
	// 	.pipe(imagemin())
	// 	.pipe(gulp.dest('../upload/more/big-min/'));

	// gulp.src('../upload/more/medium/*.jpg')
	// 	.pipe(imagemin())
	// 	.pipe(gulp.dest('../upload/more/medium-min/'));

	// gulp.src('../upload/more/small/*.jpg')
	// 	.pipe(imagemin())
	// 	.pipe(gulp.dest('../upload/more/small-min/'));
// });

/* ------------ js start ------------ */
gulp.task('minLibs', function(){
	return gulp.src(['js/mm-jquery.lightboxCustom.js',
			  'js/jquery.form.js',
			  'js/jquery.fancybox.js',
			  'js/mm-popups.js',
			  'js/mm-input.js',
			  'js/mm-with-drop.js',
			  'js/mm-jquery.slideshow.js',
			  'js/mm-ion.rangeSlider.js',
			  'js/mm-jquery.carousel.js',
			  'js/mm-jquery.tabs.js',
			  'js/mm-jcf.js',
			  'js/mm-jquery.openclose.js',
			  'js/mm-jcf.select.js',
			  'js/mm-form.js',
			  'js/mm-cart-calc.js',
			  'js/jquery.mCustomScrollbar.concat.min.js',
			  ])
		.pipe(concat('libs.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('js_prod/'));
});

gulp.task('minJsBasketMain', ['minLibs'], function(){
	return gulp.src(['js/basket.js','js/main.js'])
		.pipe(concat('main.temp.js'))
		.pipe(uglify())
		.pipe(gulp.dest('js_prod/'));
});

gulp.task('concatJsFinish', ['minJsBasketMain'], function(){
	return gulp.src(['js/jquery-1.8.3.min.js',
		'js_prod/libs.min.js',
		'js/mm-ion.rangeSlider.min.js',
		'js/jquery.arcticmodal-0.3.min.js',
		'js_prod/main.temp.js',])
		.pipe(concat('main.min.js'))
		.pipe(gulp.dest('js_prod/'));
});
/* ------------ js end ------------ */


gulp.task('watch_f', function() {
    //css
    gulp.watch([
        'css/*.css', 
                ], function(event) {
        setTimeout(function () {
            gulp.run('minCss');            
        }, 800);
    });
	//js
    gulp.watch([
        'js/*.js', 
                ], function(event) {
        setTimeout(function () {
            gulp.run('concatJsFinish');            
        }, 800);
    });
});

gulp.task('minCss', function(){
	gulp.src('css/*.css')
		.pipe(concat('main.min.css'))
		.pipe(csso())
		.pipe(gulp.dest('css_prod/'));
});


gulp.task('scripts', function(){
	gulp.run('concatJsFinish');
});