function setCookie (name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}
function getCookie(c_name) {
	if (document.cookie.length > 0 ) {
		c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length+1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) c_end = document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}

function isInbasket(tmpbasket, productId) {
  return tmpbasket.some(function(el){
    return el.id == productId;
  });
}

function parseProductsString( string ) {
	var returnArray = new Array();
	
	if (string.length > 0 ) {
		var products = string.split('|');
		for (var i in products) {
			var temp = products[i].split(':');
			
			returnArray.push({
				'id' : parseInt(temp[0]),
				'amount' : parseInt(temp[1])
			});
		}
	}
	return returnArray;
}
function generateProductsString( products ) {
	var tempArray = new Array();
	for (var i in products) {
		tempArray.push(products[i].id + ':' +  products[i].amount);
	}
	return tempArray.join('|');
}
function addProductToBasket(productId, price) {
	//получим данные из кук
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount += price;
	} else {
		totalAmount = price;
	}
	
	//Если товар уже есть в куках, добавляем ему количество
	//Заодно подсчитаем количество всех товаров
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount++;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		products.push({
				"id" : productId,
				"amount" : 1
			});
		productsCount += 1;
	}
	
 
	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//Изменить оформление
	refreshBasketInfo();
        
        
}
function deleteProductFromBasket(productId, price) {
	//получим данные из кук
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount -= price;
	} else {
		totalAmount = 0;
	}
	
	//Если товар уже есть в куках, убавляем ему количество
	//Заодно подсчитаем количество всех товаров
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount--;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		//do nothing
	}
	
	
	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//Изменить оформление
	refreshBasketInfo();
}

function addToFavorites(productId) {
	//получаем данные из кук
	var products = parseProductsString( getCookie('fav_products') );
	products.push({
		"id" : productId,
		"amount" : 1
	});
	setCookie('fav_products', generateProductsString(products), '', '/');
        $.ajax({
            url : "/ajax/products.php?action=add_to_fav",
            type : "POST",
            dataType : "json",
            data : {"id" : productId},
            beforeSend: function() {},
            success : function (data, textStatus, jqXHR) {
                console.log('success');
            },
            complete : function() {},
            error : function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
	return false;
} 

function deleteFromFavorites(productId) {
	var products = parseProductsString( getCookie('fav_products') );
	var new_products = [];
	for (var i in products) {
		if (products[i].id != productId) {
			new_products.push({
				"id" : productId,
				"amount" : 1
			});			
		}		
	}
	setCookie('fav_products', generateProductsString(new_products), '', '/');
	return false;	
}

function setBasketInfo(amount , summ) {
	//Обратная совместимость
	refreshBasketInfo();
}
function refreshBasketInfo() {
	//получим данные из кук
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (isNaN(totalAmount)) {
		totalAmount = 0;
                $( ".mm-cart-wrap" ).addClass( "cart-empty" );
				$( ".mm-cart-wrap a" ).hide();
	}else if(totalAmount == 0){
                $( ".mm-cart-wrap" ).addClass( "cart-empty" );
				$( ".mm-cart-wrap a" ).hide();
            }else{
                $( ".mm-cart-wrap" ).removeClass( "cart-empty" );
				$( ".mm-cart-wrap a" ).show();
        }
	var productsCount = 0;
	for (var i in products) {
		productsCount += products[i].amount;
	}
	
	$('.mm-cart-wrap span').text(productsCount/*+' '+declOfNum(productsCount, ['товар', 'товара', 'товаров'])+' - '+moneyFormat(totalAmount) + ' руб.'*/);
}

function recalculateBasket() {
	var productsCount = 0;
	var priceSumm = 0;
	var products = new Array();
	
	var _break = false;
	//Блокируем изменение инпутов до завершения аякса
	$('.amount_input').attr('disabled', 'disabled');
	
	$('.mm-cart-table tbody tr').each(function (index, element) {
		if (_break == false) {
			var amount = $(element).find('input[name="amount"]').val();
			var price = $(element).find('input[name="price"]').val();
			var pid = $(element).find('input[name="id"]').val();
			
			if (amount == '0' || amount == '') {
				$(element).find('input[name="amount"]').val(1);
				amount = '1';
			}

			if (amount.match(/^[0-9]{1,3}$/)) {
				amount = parseInt(amount);
				productsCount += amount;
				priceSumm += parseInt(price) * amount;
				//
				$(element).find('.mm-sum').html(moneyFormat(parseInt(price) * amount)+' <span class="currency">руб.</span>');
				//
				products.push({
					"id" : pid,
					"amount" : amount
				});
				//console.dir(products);
			} else {
				productsCount = '-';
				priceSumm = '-';
				_break = true;
			}
		}
	});

	//Если ошибок не было
	if (_break == false) {
		//Записываем куки
		setCookie('cart_products', generateProductsString(products), '', '/');
		setCookie('total_amount', priceSumm, '', '/');
		
		//Изменяем данные в блоке корзины
		setBasketInfo(productsCount, priceSumm);
		
		var coverField = $('#cart-items');
		var cover = $('<div></div>').css({
			'position' : 'absolute',
			'left' : coverField.offset().left,
			'top' : coverField.offset().top,
			'width' : coverField.width(),
			'height' : coverField.height(),
			'zIndex' : 100,
			'background' : '#fff',
			'opacity' : .6
		  }).html('<div style="text-align:center;">Обновляются данные...</div>').appendTo('body');
		
		//Запрос на стоимость доставкаи и размер скидки
		
		$.ajax({
			url: '/ajax/basket.php?method=get_delivery_and_discount',
			type : 'POST',
			dataType : 'json',
			data : {"products" : products},
			beforeSend : function () {
				$('#delivery-1 .value, #delivery-2 .value').html('<img src="/img/ajax_loading.gif" />');
			},
			success : function (data, textStatus, jqXHR) {
				orderPrice = {};
				orderPrice.delivery = intVal(data.deliveryPrice);
				orderPrice.discount = intVal(data.discount);
				orderPrice.summ = intVal(priceSumm);
				//showPriceInfo();
				
				//Изменяем данные на странице корзины
				$('.itog .mm-sum').html(moneyFormat(priceSumm));
				$('.mm-sale .value').html(orderPrice.discount);
				/* недовыпилинные скидки ??? 
				if (orderPrice.discount > 0) {
					$('.cart-items-sum .discount').show();
				} else {
					$('.cart-items-sum .discount').hide();
				}
				*/
				$('#basket-products-count').html(productsCount);
	
			},
			complete : function () {
				cover.remove();
				//Разблокируем инпуты для ввода
				$('.amount-input').removeAttr('disabled');
			},
			error : function (jqXHR, textStatus, errorThrown) {}
		});
		
	}
	if (_break == false && productsCount > 0) {
		$('#continue-order').removeAttr('disabled');
	} else {
		$('#continue-order').attr('disabled','disabled');
		$('.amount-input').removeAttr('disabled');
	}
}


var deliveryType;
$(function(){
	var body = $('body');

	var tmpbasket = parseProductsString(getCookie('cart_products'));


	//Проверка товара "в корзине" и присвоение статуса "в корзине"
	$('.buy-it-listing').each(function(){
	 	var productId = $(this).data('product_id');
	 	if (isInbasket(tmpbasket, productId)) {
	     	//$(this).removeClass('item-buy').closest('.mm-popup-wrap').addClass('mm-popup-wrap-selected');
	        $(this).parent().addClass('mm-popup-wrap-selected');
			$(this).removeClass('item-buy').addClass('item-incart').html('Оформить');
			$(this).addClass("in-back");
	  }
	});

	//Из листинга
	$('.buy-it-listing').on('click', function() {
		if ( $(this).hasClass('item-buy') ) {
			var link = $(this);
			var price = link.data('product_price') || link.data("product-price");
			var productId = link.data('product_id');
			var productTitle = link.data('product_title');
			var prodTitle = $(this).parent().parent().parent().find('a.product_title').html();
			var productChpu = link.data('product_chpu');
			addProductToBasket(productId, price);
			//console.log(prodTitle);
			reportToYaMetrika('add',[productId,prodTitle,price]);
			if( $('#jsUpdate').length > 0 ){
				var str = '<tr>\
                            <td class="mm-name">\
                                <div class="mm-image">\
                                    <img src="http://armed-market.ru/images/small/'+productId+'.jpg" alt="'+productTitle+'" title="'+productTitle+'">\
                                </div>\
                                <div class="mm-text-wrap">\
                                    <a href="/catalog/'+productChpu+'.html" class="mm-title">'+productTitle+'</a>\
                                    <a href="javascript:void(0);" data-productId="'+productId+'" class="mm-delete cart-item-close">Удалить</a>\
                                </div>\
                            </td>\
                            <td class="mm-price"><span>'+price+'</span></td>\
                            <input type="hidden" name="price" value="'+price+'">\
                            <input type="hidden" name="id" value="'+productId+'">\
                            <td class="mm-number">\
                                <a href="#" class="minus">-</a>\
                                <input type="text" class="text amount_input" value="1" name="amount">\
                                <a href="#" class="plus">+</a>\
                            </td>\
                            <td class="mm-sum">'+moneyFormat(price)+'</td>\
                        </tr>';
				$('#cart-items').append(str);
				recalculateBasket();
			}
			$(this).parent().addClass('mm-popup-wrap-selected');
			$(this).removeClass('item-buy').addClass('item-incart').html('Оформить');
			$(this).siblings().show();
			$(this).addClass("in-back");
			return false;
		}
		return true;
	});
	$('.mm-price-holder').hover(function(){}, function(){
		$($($($(this)[0]).children().get(0)).children().get(1)).hide();
	});

	body.on('click','.minus', function () {
			var $input = $(this).parent().find('input');
			var count = parseInt($input.val()) - 1;
			count = count < 1 ? 1 : count;
			$input.val(count);
			$input.change();
			recalculateBasket();
			return false;

	});
	body.on('click', '.plus', function () {
			var $input = $(this).parent().find('input');
			$input.val(parseInt($input.val()) + 1);
			$input.change();
			recalculateBasket();
			return false;
	});

	//Листенер кнопки "Удалить"
	body.on('click', '.cart-item-close', function () {
		if (confirm('Вы действительно хотите удалить товар из корзины?')) {
			$(this).closest('tr').remove();
			recalculateBasket();
			var productId = $(this).attr('data-productId');
			var product = $(this).closest('a').html();
			var prodInfo = [productId , name ] ;
			reportToYaMetrika('remove',prodInfo);
			
		}
	});
	/*
	body.on('click', '.mm-price', function () {
		products = getProductsFromCookies();
		console.log(products);
		reportToYaMetrika('purchase',products);
	});
	*/
	function getProductsFromCookies(){
		
		var prods = getCookie('cart_products').split('|');
		var products = [] ;
		prods.forEach(function(e){
			var prodInfo = {} ;
			var product = e.split(':');
			prodInfo.id = product[0];
			prodInfo.quantity = product[1] - 0;
			products.push(prodInfo);
		});
		for (var i = 0 ; i < products.length ; i++){
			var num = i + 1 ;
			var productTilte = $('.mm-order-table').find('tr:nth-child('+num+')').find('.mm-name').find('a').html();
			products[i].name = productTilte + '' ;
		}
		return products;
	}
	//Лисенер кнопки "избранное"
	$('.favorites-heart').bind('click', function(){
		var big_heart = $('.favorites-heart.big');
		var modal = $(this).find('div.heart-popup-left');
		var link = $(this).find('a');
		var id = link.data('product_id');
		
		if ( $(this).hasClass('empty') && !$(this).hasClass('big')) {
			modal.html('Уже&nbsp;в&nbsp;избранном');
			$(this).removeClass('empty');
			big_heart.removeClass('empty');
			addToFavorites(id);
		}	
		else {
			href = link.attr('href');
			location.href = href;
		} 
		return false;
	});	
	
	//Листенер изменение количества товаров в корзине
	var recalcTimer;
	body.on('change', '.amount_input', function (e) {
		var code = (e.keyCode ? e.keyCode : e.which);

		/*
		LEFT=   37
		UP=     38
		RIGHT=  39
		DOWN=   40
		SHIFT=  16
		CONTROL=    17
		ALT=    18
		*/
		var keys = [16,17,18,37,38,39,40];
		//Не реагировать на нажатие клавиш, обозначеных выше
		if (recalcTimer != undefined) clearInterval(recalcTimer);
		if (!(keys.indexOf(code) >= 0)) { 
			recalcTimer = setTimeout('recalculateBasket();', 1000);
		}
	});
	deliveryType = $('#order-form select[name="delivery_type"]').val();
	
	$('#order-form select[name="delivery_type"]').bind('change', function(){
		deliveryType = $(this,':checked').val();
		showPriceInfo();
		console.log("dfdsf");
	});
	
	function changeCity()
	{
			$('.dev_destination>div>span.unsee').each(function(){
			if($(this).text()==$('.dev_city>.select-area>.center').text()) {
				$('[name="dev_des"]').val($(this).parent().find('span.center').text());
				$(this).parent().removeClass('unsee');
				}
			else { $(this).parent().addClass('unsee');
				}
			});
	}
	
		/*For basket dev type destenation*/
	$('[name="dev_type"]').change(function(){	
		var val_dev = $('[name="dev_type"]:checked').val();
		
		
		if(val_dev == 'Пункт самовывоза') {
			
			$('.delivery_dest').addClass('unsee');
			$('[name="dev_city"]').val($('.dev_city>.select-area>.center').text());
			$('.dev_city').removeClass("unsee");
			$('.dev_destination').removeClass("unsee");}
			
		else {
			
			$('.delivery_dest').removeClass('unsee');
			$('.dev_city').addClass("unsee");
			$('.dev_destination').addClass('unsee');
			
			}

			changeCity();
		
	});
	
	
	/* $('dev_city').on("change", "select", function(){	
	
		console.log("sdsd");
		changeCity();
	}); */
	
	$('#order-form').bind('submit', function(){
			var form = $('#order-form');
			var name = form.find('input[name="name"]');
			var phone = form.find('input[name="phone"]');
			//var payment = form.find('input[name="payment_type"]');
			var email = form.find('input[name="email"]');
			var loading = $('.order-loading');
			var sendButton = $('#accept-order-button');
			var inProcess = sendButton.hasClass('in-process');
			var arr = form.serializeArray();
			console.log(arr);
			var payTemp = 0;
			$(form.serializeArray()).each(function(ind,element){
				if (element.name=='payment_type'){payTemp = element.value;}
			});
			
			if (inProcess)
				return false;
			
			var order_error = $('#order-error').find('.modal-popup-content');
			order_error.empty();
			var error;
			var isError = false;
			var new_err = ""; 

			if (name.val().length <= 3) {
				isError = true;
				error = $('<div>').addClass('modal-error-msg');
				error.text('Не указаны ФИО');
				new_err += 'Не указаны ФИО; '
				order_error.append(error);
			}	
			if (phone.val().length < 5) {
				isError = true;
				error = $('<div>').addClass('modal-error-msg');
				error.text('Пожалуйста, укажите номер вашего телефона');
				new_err += 'Пожалуйста, укажите номер вашего телефона; ';
				order_error.append(error);
			}
			if (payTemp == '2') {
				if ( !isValidEmail(email.val()) ) {
					isError = true;
					error = $('<div>').addClass('modal-error-msg');
					error.text('Укажите e-mail');
					new_err += 'Укажите e-mail; ';
					order_error.append(error);
				}
			}
			/*
			if(!$("#id50").prop("checked")){
				isError = true;
				new_err += 'Пожалуйста примите условия продажи; ';
			}
			*/
			if (isError) {				
				//modalOpen('order-error');
				alert(new_err);
				return false;
			}
			
			$.ajax({
					url: '/ajax/basket.php?method=make_order',
					type : 'POST',
					dataType : 'json',
					data : form.serialize(),
					beforeSend : function () {
						//console.log(1);
						loading.show();
						form.find('input').attr('disabled','disabled');
						form.find('textarea').attr('disabled','disabled');
						sendButton.addClass('in-process');
						products = getProductsFromCookies();
						
					},
					success : function (data, textStatus, jqXHR) {
						console.dir(data);
						if(!data.error){
							
							var productsArr = [];
							$(data.products).each(function(i, e){
								productsArr.push({
									id: e.id,
									name: e.title,
									price: e.price,
									quantity: e.amount
								});
							});
							reportToYaMetrika('purchase',[productsArr, [ data.order_id ] ]);
						
							setCookie('order_code', data.order_code,  '', '/');
							setCookie('order_id', data.order_id,  '', '/');
							setCookie('price', data.price,  '', '/');
							setCookie('name', data.name,  '', '/');
							location.href = "ok.html";
						}else{
							alert(data.error);
						}
					},
					complete : function () {						
						loading.hide();
						form.find('input').removeAttr('disabled');
						form.find('textarea').removeAttr('disabled');
						sendButton.removeClass('in-process');
					},
					error : function (jqXHR, textStatus, errorThrown) {console.log(textStatus);}
				});
			return false;
	   });
});
function getNewSize(oldWidth, oldHeight, destWidth, destHeight) {
	if (destWidth) {var iScaleW = destWidth / oldWidth;} else {var iScaleW = 1;}
	if (destHeight) {var iScaleH = destHeight / oldHeight;} else {var iScaleH = 1;}

	var iSizeRelation = ( (iScaleW < iScaleH) ? iScaleW : iScaleH);
	var iWidthNew = Math.round(oldWidth * iSizeRelation);
	var iHeightNew = Math.round(oldHeight * iSizeRelation);

	var iSizeW = iWidthNew;
	var iSizeH = iHeightNew;
	iDestX =0;
	iDestY =0;
	
	return {"width" : iSizeW, "height" : iSizeH};
}
function moneyFormat(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ' ' + '$2');
	}
	return x1 + x2;
}
function declOfNum(number, titles) {  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}
function intVal( mixed_var, base ) {
	    var tmp;
	 
	    if( typeof( mixed_var ) == 'string' ){
	        tmp = parseInt(mixed_var);
			if(isNaN(tmp)){
	            return 0;
	        } else{
	            return tmp.toString(base || 10);
	        }
	    } else if( typeof( mixed_var ) == 'number' ){
	        return Math.floor(mixed_var);
	    } else{
	        return 0;
	    }
}
function reportToYaMetrika(action,array){
	switch(action){
		case 'add':
		
			try {
				dataLayer.push({
					"ecommerce": {
						"add": {
							"products": [
							{
								"id": array[0] + '' ,
								"name": array[1] + '',
								"price": array[2],
							}
							]
						}
					}
				});
			}
			catch (e) {}
		
		break;
		case 'remove':
		
			try {
				dataLayer.push({
					"ecommerce": {
						"remove": {
							"products": [
							{
								"id": array[0] + '' ,
								"name": array[1] + '' ,
							}
							]
						}
					}
				});
			}
			catch (e) {}
		
		break;
		case 'purchase':
			
			try {
				dataLayer.push({
					"ecommerce": {
						"purchase": {
							"actionField": {
								"id" : array[1][0],
								"goal_id" : "7830906"
							},
							"products": array[0]
						}
					}
				});
			}
			catch (e) {}
		break;
		case 'fast_order':
			
			try {
				dataLayer.push({
					"ecommerce": {
						"purchase": {
							"actionField": {
								"id" : array[0],
								"goal_id" : "7830881"
							},
							"products": [
							{
								"id": array[1][0] + '' ,
								"name":array[1][1] + '' ,
								"quantity": 1
							}
							]}
					}
				});
			}
			catch (e) {}
		break;
	}
}
