;(function( $ ) {

	$.fn.aeImageResize = function( params ) {

		var aspectRatio = 0;

		// We cannot do much unless we have one of these
		if ( !params.height && !params.width ) {
			return this;
		}

		// Calculate aspect ratio now, if possible
		if ( params.height && params.width ) {
			aspectRatio = params.width / params.height;
		}

		// Attach handler to load
		// Handler is executed just once per element
		// Load event required for Webkit browsers
		return this.one( "load", function() {

			// Remove all attributes and CSS rules
			this.removeAttribute( "height" );
			this.removeAttribute( "width" );
			this.style.height = this.style.width = "";

			var imgHeight = this.height
				, imgWidth = this.width
				, imgAspectRatio = imgWidth / imgHeight
				, bxHeight = params.height
				, bxWidth = params.width
				, bxAspectRatio = aspectRatio;

			// Work the magic!
			// If one parameter is missing, we just force calculate it
			if ( !bxAspectRatio ) {
				if ( bxHeight ) {
					bxAspectRatio = imgAspectRatio + 1;
				} else {
					bxAspectRatio = imgAspectRatio - 1;
				}
			}

			// Only resize the images that need resizing
			if ( (bxHeight && imgHeight > bxHeight) || (bxWidth && imgWidth > bxWidth) ) {

				if ( imgAspectRatio > bxAspectRatio ) {
					bxHeight = ~~ ( imgHeight / imgWidth * bxWidth );
				} else {
					bxWidth = ~~ ( imgWidth / imgHeight * bxHeight );
				}

				this.height = bxHeight;
				this.width = bxWidth;
			}
		})
		.each(function() {

			// Trigger load event (for Gecko and MSIE)
			if ( this.complete ) {
				$( this ).trigger( "load" );
			}

			// This fixes IE9 issue
			this.src = this.src;
		});
	};



})( jQuery );

function whereAmI(){
	var href = window.location.pathname ; 
	$('ul.mm-top-nav').find('a[href="'+href+'"]').css({'font-weight':'bold', 'font-size': '14px'}); 
}

function tagsListEvents(){
	var cont = $('.sub-categories-cont');
	var list = $('.mm-sub-categories');
	var button = $('.show-more');
	
	var initial_height = 71;
	
	if (list.height() > initial_height) button.show();

    button.on("click", function(){
		if(cont.height() > initial_height ){
            cont.animate({height:initial_height+'px'}, 500 );
		} else {
            cont.animate({height:list.height()}, 500 );
		}
       
    });
}; 


function catModalOpenLink(item) {

    $.ajax({
	url: "/ajax/modal_item.php",
	cache: false,
	type: "POST",
	data: ({item: item}),
	// dataType: "html",
	success: function(html) {
	    $("#mdlcls").append(html).show();
	},
	complete: function(){
		$(".product-item-view-detailed .dynamic-cont .dynamic-setof-items").carouFredSel({
			circular: false,
			infinite: false,
			align: "left",
			width: 350,
			height: 123,
			items: 3,
			auto: false,
			prev: '.prev',
			next: '.next',
			scroll: {
				items: 1
			}
		});
		$('.product-item-view-detailed .dynamic-setof-items .image-wrap2 a').bind('click', function() {
			var container = $('.product-item-img');
			var _this = this;

			container.empty();
			
			container.append($('<img>').attr('src', $(this).data('original')));
			
			// $('.product	-item-view-detailed .dynamic-cont .dynamic-setof-items a').each(function(index, element) {
				// var link = $('<a>').attr('href', $(element).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
				// if (_this == element)
				// link.append(
					// $('<img>').attr('src', $(element).attr('href'))
					// );
				// alert(link);
				// container.append(link);
			// });
			return false;
		});
		//$('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a:first').click();
		
		$('#mdlcls').css('overflow-y','auto');
		$('body').css('overflow','hidden').css('position','relative').css('left','-7px');
	}
    });

    return false;

}

function searchAutocomplete(search, autocomplete){
    //console.log(search);
	var autocomplete = autocomplete || $('.mm-search-form').find('ul.autocomplete');
    $.ajax({
        url: '/ajax/autocomplete.php',
        type:'POST',
        data:{action: 'autocomplete', search: search },
        dataType:'json',
        success:function(data){
            var html = renderAutocomplete(data);
            console.log(html);
            autocomplete.html(html);
            autocomplete.slideDown(300);
            $('ul.autocomplete').bind('mouseleave',function(){
                $(this).slideUp(300);
            });
        }
    });
}
function renderAutocomplete(data){
    
    var html = "";
    for (var i = 0 ; i < data.length ; i++){
        html += '<li><a href="/catalog/'+data[i].chpu+'.html" >'+data[i].title+'</a></li>';
    }
    
    return html ;
}

$(document).ready(function() {
	

	
	// определяем где мы и подсвечиваем в хедере 
	whereAmI(); 
	
	// кнопка "еще" для тегов в категории 
	tagsListEvents();
	
	$('.cat_modal-open-link').on('click', function(){
		var item = $(this).closest('.catalogue-item-wrap').find('.catalogue-item-btn a').data('product_id');
		catModalOpenLink(item);
	});
	
	// купить в 1 клик
	$('.one-click ').click(function(e){
		$('#popup2 h2').html('Купить в 1 клик!');
		$('#popup2 .mm-price').html($(this).data('price')+'<span class="currency"> руб.</span>');
		$('#popup2 .mm-title').text($(this).data('title'));
		$('#popup2 #product_id').val($(this).data('id'));
		$('#popup2 .mm-title').attr('href','/catalog/'+$(this).data('chpu')+'.html');
	
		var buttons = $('#fast-order').find('.mm-btns');
		buttons.show();
		$('.after-send').hide();
		$('.mm-text').show();
	  $('.mm-text-wrap').show();
	  $('#popup2 .mm-title-holder').html('<h2>Купить в один клик!</h2>');
	  $('#popup2 .mm-submit').attr('data-is-credit','0');
	  $('.mm-info').show();

	});
	
	// купить в кредит
	$('.mm-kredit').click(function(){
        $('#popup2 h2').html('Купить в кредит')
		$('#popup2 .mm-price').html($(this).data('price')+'<span class="currency"> руб.</span>');
		$('#popup2 .mm-title').text($(this).data('title'));
		$('#popup2 #product_id').val($(this).data('id'));
		$('#popup2 .mm-title').attr('href','/catalog/'+$(this).data('chpu')+'.html');
		 
		var buttons = $('#fast-order').find('.mm-btns');
		buttons.show();
		$('.after-send').hide();
		$('.mm-text').show();
	  $('.mm-text-wrap').show();
	  $('#popup2 .mm-title-holder').html('<h2>Купить в кредит!</h2>');
	  $('#popup2 .mm-submit').attr('data-is-credit','1');
	  $('.mm-info').show();
	  
	});
	
	// автокомплит поиска
	$('.mm-search-form').find('input.mm-text').bind('keyup', function(){
        var search = $(this).val() + '' ;
        var autocomplete = $('.mm-search-form').find('ul.autocomplete');
        autocomplete.html('');
        if (search.length > 2) {
            searchAutocomplete(search, autocomplete);
        }
    });

    // инициализация  start rating -  bootstrap
    $(".input-rating").rating({
        clearCaption: '',
        step: 1,
        starCaptions: {1: 'очень плохо', 2: 'плохо', 3: 'нейтрально', 4: 'хорошо', 5: 'отлично'},
        starCaptionClasses: {1: 'text-danger', 2: 'text-warning', 3: 'text-info', 4: 'text-primary', 5: 'text-success'}
    });


    // отправка отзыва из карточки товара
	$('.mm-feedback-form').submit(function (e) {
		e.preventDefault();

		var form = $('.mm-feedback-form'),
			submitBtn = form.find('input[type="submit"]'),
			params = {
				id: form.find('input[name="id"]').val() - 0,
                name:form.find('input[name="name"]').val(),
                comment: form.find('textarea[name="comment"]').val(),
                rating: form.find('input[name="rating"]').val() - 0,
                "g-recaptcha-response": form.find('textarea[name="g-recaptcha-response"]').val()
			};

		//console.log(params);

		if (submitBtn.hasClass('disabled')) return false ;

        $.ajax({
            url: '/ajax/basket.php?method=feedback',
            type: 'POST',
            dataType: 'json',
            data: params,
            beforeSend: function() {
                submitBtn.addClass('disabled');
            },
            success: function(data, textStatus, jqXHR) {
            	//console.log(data);
            	if (data.result) {
                    form.trigger('reset');
            		alert("Спасибо за ваш отзыв, он появится на сайте после проверки модератором");
				} else {
                    alert("Ошибка, проверьте правильность вводимых данных");
                    console.log(data);
				}
            },
            complete: function() {
                submitBtn.removeClass('disabled');
            }
        });

        return false;
    })


    //Close popups if click outside the window
    $('.cat_modal-overlay').click(function(e) {
		$('#mdlcls').css('display', 'none');
		$('body').css('overflow','auto').css('position','').css('left','0px');
		$('#mdlcls').css('overflow-y','hidden');
		$('#mdlcls').html('');
		return false;
    }).on('click','.cat_modal-body', function(e) { 
		e.stopPropagation();
    }).on('click','.cat_modal-close', function(e) { 
		$('#mdlcls').css('display', 'none');
		$('body').css('overflow','auto').css('position','').css('left','0px');
		$('#mdlcls').css('overflow-y','hidden');
		$('#mdlcls').html('');
		return false;
    });
	
    //Close popups if click outside the window
    $('.modal-bg-overlay').click(function() {
		$('.modal-bg-overlay').css('display', 'none');
		$('.modal-popup').css('display', 'none');
		return false;
    });

    // // Styling
    // Header. More Menu
    $('.header-more-menu .btn-tpl').click(function(event) {
	$(this).closest('li').find('ul').show();

	return false;
    });
    // Header. More Menu v2
    $('.header-more-menu > a').click(function(event) {
	$(this).closest('li').find('ul').show();
	return false;
    });
    $(document).click(function(event) {
	$('.header-more-menu').find('ul').hide();
    });

    // Catalogue Sidebar
    $('li.catalogue-list-item').children('span').click(function(event) {
	_elem = $(this).parents('li.catalogue-list-item');

	$(_elem).toggleClass('active');
	$(_elem).find('ul.main-slider-endlist').toggle();
	$(this).parent().next('li.catalogue-list-item').toggleClass('after-active');

	return false;
    });

    // Catalogue Product
    $('.catalogue-item, .catalogue-item a img').mouseenter(function(event) {
	title = $(this).parents('.catalogue-item').find('.catalogue-item-title');

	$(this).addClass('active');
	$(title).addClass('active');
	$(this).find('.description-overlay').hide();
    }).mouseleave(function(event) {
	title = $(this).parents('.catalogue-item').find('.catalogue-item-title');

	$(this).removeClass('active');
	$(title).removeClass('active');
	$(this).find('.description-overlay').show();
    });

    $('.item-stock, .item-delivery').hover(function() {
	$(this).next().find('.item-tooltip').css('display', 'inline-block');
    }, function() {
	$(this).next().find('.item-tooltip').hide();
    });

    // Articles
    $('.article-item-title a, .g-img-cont').mouseenter(function(event) {
	imgt = $(this).parents('.footer-more-item, .announce-item, .article').find('.g-img-cont');

	$(imgt).addClass('active');
	$(this).prev().addClass('active');
    }).mouseleave(function(event) {
	$(imgt).removeClass('active');
	$(this).prev().removeClass('active');
    });

    // Filter More _btn
    $('.filter-more-btn').click(function(event) {
	$('.filter-more-container').slideToggle(100);
	$(this).find('.btn-tpl').toggle();
	$(this).toggleClass('active')

	return false;
    });

    // Tags Cloud List
    $('.journal-tags-switch li').click(function(event) {
	$('.journal-tags-switch li').toggleClass('active');
	$('.journal-tags-list').toggleClass('active');

	return false;
    });

    //// Cart
    // Cart - Additional service
    $('.cart-addservice-check input.styled').show();
    $('.cart-addservice-check input').on('click', function(event) {
	var _val = $(this).attr('value');
	$(this).parents('.cart-addservice-check').find('div :checkbox').not(this).removeAttr('checked');

	var _tmp = $(this).parents('.cart-item').find('.cart-addservice-setof-price span.' + _val)
	_tmp.toggleClass('active');
	$(this).parents('.cart-item').find('.cart-addservice-setof-price span').not(_tmp).removeClass('active');
    });

    // Cart - Items
 //    $('.cart-item .cart-item-close').click(function() {
	// _elem = $(this).parents('.cart-item').find('ul li input');

	// if ((_elem.val()) != (0)) {
	//     currv = $(this).parents('.cart-item').find('ul li input').val();
	//     _elem.val(0);
	//     $(this).parent('.cart-item').addClass('inactive');
	// } else {
	//     _elem.val(currv);
	//     $(this).parent('.cart-item').removeClass('inactive');
	// }

	// return false;
 //    });

    // Placeholder
    $('input.placeholder, textarea.placeholder').focus(function() {
	_input_val = $(this).attr('value');

	$(this).attr('value', '');
	if ($(this).is("textarea")) {
	    $(this).empty();
	}
	;
    });
    $('input.placeholder, textarea.placeholder').blur(function() {
	$(this).attr('value', _input_val);
	if ($(this).is("textarea")) {
	    $(this).html('Ваше сообщение:');
	}
	;
    });


    // Carousel


//if($('.product-item-view-detailed .dynamic-setof-items .image-wrap').length) {
    // $(".product-item-view-detailed .dynamic-cont .dynamic-setof-items").carouFredSel({
	// circular: false,
	// infinite: false,
	// align: "left",
	// width: 350,
	// height: 120,
	// items: 3,
	// auto: false,
	// prev: '.prev',
	// next: '.next',
	// scroll: {
	    // items: 1
	// }
    // });

    $('.product-item-view-detailed .dynamic-setof-items .image-wrap a').bind('click', function() {
	var container = $('.product-item-img');
	var _this = this;

	container.empty();

        var itemTitle = $('.product-title h1').html();
        
	$('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a').each(function(index, element) {
	    var link = $('<a>').attr('href', $(element).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
	    var img = $('<img>');
            if (_this == element) {
		img.attr('src', $(element).attr('href'));
		img.attr('alt', itemTitle);
                link.append(
			img
			);
            }    
	    container.append(link);
	});
	return false;
    });
    $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a:first').click();
    // $("a.fgallery").fancybox();
//}	
    /*	if($('.product-item-view-detailed .dynamic-setof-items a').length) {
     var s8pos = 0;
     var s8col = $('.product-item-view-detailed .dynamic-setof-items a').length;
     var imgWidth = 126 + 5;
     var imgOnSlide = 3;
     var lastPos = s8col - imgOnSlide - 1;
     if (lastPos < 0) lastPos = 0; 
     
     $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items').css('width', s8col*imgWidth);
     $('.product-item-view-detailed .image-extra-wrap').css('overflow', 'hidden');
     //$('.product-item-view-detailed .dynamic-setof-items').css('width', s8col*imgWidth);
     
     $('.photo-slide li.next a').click(function(){
     s8pos++;
     if(s8pos>lastPos) s8pos=0;
     $('.product-item-view-detailed .dynamic-setof-items').animate({'left':-(s8pos*imgWidth)}, 300);
     return false;
     });
     
     $('.photo-slide li.prev a').click(function(){
     s8pos--;
     if(s8pos<0) s8pos=lastPos;
     $('.product-item-view-detailed .dynamic-setof-items').animate({'left':-(s8pos*imgWidth)}, 300);
     return false;
     });
     
     $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a').bind('click', function () {
     var container = $('.product-item-img');
     var _this = this;
     
     container.empty();
     
     
     $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a').each(function(index, element){
     var link = $('<a>').attr('href', $(element).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
     if (_this == element)
     link.append(
     $('<img>').attr('src', $(element).attr('href'))
     );
     container.append(link);
     });
     return false;
     });
     $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a:first').click();
     $("a.fgallery").fancybox();
     }
     */



    //product

    $('#fast-order-send-button').bind('click', function() {
	var wrap = modal = $('#fast-order');
	// var loading = modal.find('.loading');
	var buttons = modal.find('.mm-btns');

	// var answer = modal.find('.answer');
	// var response = modal.find('.response');

	var name = wrap.find('input[name="name"]');
	var phone = wrap.find('input[name="phone"]');
	var email = wrap.find('input[name="email"]');
	//var email =	wrap.find('input[name="email"]');
	var product_id = wrap.find('input[name="product_id"]');
	//var button = $(this);
	var product_title = wrap.find('a.mm-title').html();
	if (
		name.val().length <= 1 ||
		phone.val().length < 5 ||
		$('#fast-order').find('input[name="name"]').val() == 'ФИО, Компания:' ||
		$('#fast-order').find('input[name="phone"]').val() == 'Контактный телефон:'
		) {
	    alert('Некоторые поля заполнены неправильно или пустые.');
	    return false;
	}
	if (wrap.hasClass('in-process')) {
	    return false;
	}
	
	var isCredit = $(this).attr('data-is-credit');
	
	//console.log(product_title);
	$.ajax({
	    url: '/ajax/basket.php?method=make_order',
	    type: 'POST',
	    dataType: 'json',
	    data: {'name': name.val(), "phone": phone.val(), "email": email.val(), "product_id": product_id.val(), "fast_order": 1 , 'is_credit': Number(isCredit) },
	    beforeSend: function() {
		name.attr('disabled', 'disabled');
		phone.attr('disabled', 'disabled');
		email.attr('disabled', 'disabled');
		// loading.show();
		buttons.hide();

		wrap.addClass('in-process');
	    },
	    success: function(data, textStatus, jqXHR) {
			// response.find('.name').text(name.val());
			// response.show();
			$('.after-send .order-pn').text('Здравствуйте, ' + data.name);
			$('.after-send .order-code').html('Вашему заказу присвоен номер <b>' + data.order_code + '</b>');
			reportToYaMetrika('fast_order',[ data.order_code , [product_id.val(),product_title]]);
	    },
	    complete: function() {
		name.removeAttr('disabled');
		phone.removeAttr('disabled');
		email.removeAttr('disabled');

	  $('.mm-text').hide();
	  $('.mm-text-wrap').hide();
	  $('.mm-modal').find('.mm-title-holder').html('<h2>Ваш заказ принят!</h2>');
	  $('.mm-modal').find('.mm-info').hide();
		// buttons.hide().text('Ваш заказ принят!').show();
		$('.after-send').show();
		wrap.removeClass('in-process');
	    }
	});
	return false;

    });

    //callback
    $('#callback .modal-cancel').bind('click', function() {
	var wrap = $('#callback');
	wrap.find('.loading').hide();

	wrap.find('.answer').show();
	wrap.find('.response').hide();

	modalClose('callback');
    });

    $('#callback .modal-submit').bind('click', function() {
	var wrap = $('#callback');
	var phone = wrap.find('input[name="phone"]');
	var name = wrap.find('input[name="name"]');
//		var email = wrap.find('input[name="email"]');
//		var msg = wrap.find('textarea[name="msg"]');

	var loading = wrap.find('.loading');

	var answer = wrap.find('.answer');
	var response = wrap.find('.response');



	if (
		name.val().length <= 1 ||
		phone.val().length < 5 ||
		name.val() == name.prop('defaultValue') ||
		phone.val() == phone.prop('defaultValue')
		) {
	    alert('Введите Ваше имя и телефон');
	    return false;
	}

	/*if (
	 !isValidEmail(email.val()) 
	 ) {
	 alert('Не правильный E-mail');
	 return false;
	 }*/

	/*if (
	 msg.val().length < 3 ||
	 msg.val().length > 999
	 ) {
	 alert('Длина сообщения должна составлять от 3 до 999 символов!');
	 return false;
	 }
	 */

	if (wrap.hasClass('in-process')) {
	    return false;
	}

	$.ajax({
	    url: '/ajax/basket.php?method=make_order',
	    type: 'POST',
	    dataType: 'json',
	    data: {
		'name': name.val(),
		"phone": phone.val(),
		"backcall": 'true'
	    },
	    beforeSend: function() {
		name.attr('disabled', 'disabled');
		phone.attr('disabled', 'disabled');

		loading.show();

		wrap.addClass('in-process');
	    },
	    success: function(data, textStatus, jqXHR) {
		answer.hide();
		response.show();
	    },
	    complete: function() {
		name.removeAttr('disabled');
		phone.removeAttr('disabled');

		loading.hide();
		wrap.removeClass('in-process');
	    }
	});

	return false;

    });
    $('#shop-feedback .ask-question .form-tpl-submit .btn-tpl').bind('click', function() {
	var wrap = $('#shop-feedback');
	var form = $('#shop-feedback .ask-question form');
	var loading = wrap.find('.loading2');
	//var buttons = modal.find('.fast-order-modal-submit');

	var answer = wrap.find('.ask-question');
	var response = wrap.find('.ask-response');

	var name = wrap.find('input[name="name"]');
	var email = wrap.find('input[name="email"]');
	var msg = wrap.find('textarea[name="msg"]');
	//var product_id = wrap.find('input[name="product_id"]');
	//var button = $(this);

	if (
		name.val().length <= 1 ||
		!isValidEmail(email.val()) ||
		msg.val().length < 3 ||
		name.val() == name.prop('defaultValue') ||
		msg.val() == msg.prop('defaultValue') ||
		email.val() == email.prop('defaultValue')
		) {
	    alert('Введите Ваше имя, email и текст сообщения');
	    return false;
	}
	if (wrap.hasClass('in-process')) {
	    return false;
	}
	$.ajax({
	    url: '/ajax/system.php?method=addshopfeedback',
	    type: 'POST',
	    dataType: 'json',
	    data: {
		'username': name.val(),
		'usermail': email.val(),
		"usermsg": msg.val(),
	    },
	    beforeSend: function() {
		name.attr('disabled', 'disabled');
		msg.attr('disabled', 'disabled');
		email.attr('disabled', 'disabled');
		//button.attr('disabled', 'disabled');
		loading.show();
		//buttons.hide();

		wrap.addClass('in-process');
	    },
	    success: function(data, textStatus, jqXHR) {
		answer.hide();
		response.show();
	    },
	    complete: function() {
		name.removeAttr('disabled');
		msg.removeAttr('disabled');
		email.removeAttr('disabled');

		//buttons.show();
		loading.hide();
		wrap.removeClass('in-process');
	    }
	});
	return false;
    });

    //Shop feed back
    $('#shop-feedback-button').bind('click', function() {
	var wrap = $('#shop-feedback');
	var loading = wrap.find('.loading');
	var feeds = wrap.find('.feeds');
	var pagin = wrap.find('.paginator');

	modalOpen('shop-feedback');



	loadShopFeeds(1);

	function loadShopFeeds(page) {
	    $.ajax({
		url: '/ajax/system.php',
		type: 'POST',
		dataType: 'json',
		data: {"method": 'getshopfeedback', 'page': page},
		beforeSend: function() {
		    loading.show();
		},
		success: function(data, textStatus, jqXHR) {
		    feeds.empty();

		    if (data.messages.length > 0) {
			//feeds
			var first = true;
			for (var i in data.messages) {
			    feeds.append(makeFeed(data.messages[i], first));
			    first = false;
			}
			//pagin
			pagin.empty();
			pagin.append(
				makePagin(data.pagin_data.currentPage, data.pagin_data.pagesCount)
				);
			$(document).scrollTop($('#feeds').offset().top - 100);

		    } else {
			feeds.append($('<div>').text('Отзывов нет. Станьте первым.'));
		    }
		},
		complete: function() {
		    loading.hide();
		}
	    });
	}

	function makeFeed(message, first) {
	    var m = $('<div>').addClass('dialog-item');
	    if (first)
		m.addClass('first');

	    m.append(
		    $('<div>').addClass('dialog-item-title').append(
		    $('<span>').text(message.date)
		    ).append(' ' + message.name)
		    ).append(
		    $('<p>').text(message.des)
		    );
	    if (message.otvet.length > 0) {
		m.append($('<div>').addClass('answer').append(
			$('<div>').addClass('caption').text('Ответ')
			).append(
			$('<div>').addClass('body').text(message.otvet)
			)
			);
	    }

	    /*
	     <div class="dialog-item first">
	     <div class="dialog-item-title"><span>28.01.2013</span> Дмитрий</div>
	     <p>Отличный магазин, прекрасные сотрудники! Спасибо!</p>
	     <div class="answer">
	     <div class="caption">Ответ</div>
	     <div class="body">Не хуй собачий!</div>
	     </div>
	     </div>
	     */

	    return m;
	}

	function makePagin(current, count) {
	    var ul = $('<ul>');

	    if (count > 1) {
		for (i = 1; i <= count; i++) {
		    var li = $('<li>');

		    li.append(
			    $('<span>').append(
			    $('<a>').text(i)
			    )
			    ).data('page', i);
		    if (i == current)
			li.addClass('active');
		    else
			li.bind('click', function() {
			    loadShopFeeds($(this).data('page'));
			});

		    ul.append(li);
		}
	    }

	    return ul;
	    /*
	     <ul>
	     <li class="prev"><span><a href="./"></a></span></li>
	     
	     <li><span><a href="./">1</a></span></li>
	     ...
	     <li class="active"><span><a href="./">5</a></span></li>
	     ...
	     <li><span><a href="./">27</a></span></li>
	     
	     <li class="next"><span><a href="./"></a></span></li>
	     </ul>
	     */
	}

	//Close feedback, when click outside the form.	
	$('.modal-bg-overlay').click(function() {
	    $('.modal-bg-overlay').css('display', 'none');
	    $('.modal-popup').css('display', 'none');
	});

    });


    // Adaptive Size
    function checkWidth(init) {
	if ($(window).width() < 1330) {
	    $('body').addClass('w960');
	} else {
	    if (!init) {
		$('body').removeClass('w960');
	    }
	}
	;
    }

    checkWidth(true);

    $(window).resize(function() {
	checkWidth(false);
    });

    $('.customScroll').mCustomScrollbar({
        axis: "y",
    });

});

function reloadProducts(page, sort) {
    var formId = '#filter-form';
    var productsFieldId = '#products-list-and-paginator';
    var novinki = $("input[name=novinki]").val();
    if (page == undefined)
	page = (defaultPage == undefined) ? 1 : defaultPage;
    if (sort == undefined) {
	sort = (defaultSort == undefined) ? 'none' : defaultSort;
    }
    $(formId).ajaxSubmit({
	data: {"page": page, "sort": sort, "novinki": novinki},
		beforeSend: function() {
		    $(productsFieldId).css('opacity', 0.3);
	},
	success: function(data, statusText, xhr, element) {
	    $(productsFieldId).find('.mm-catalog').empty().append(data).append($('<div>').addClass('clearfix'));
	    var paginDiv = $(productsFieldId).find('.pagin').clone();
	    $(productsFieldId).find('.pagin').remove();

	    $('.products-list-and-paginator').html(paginDiv);
	    $(productsFieldId).css('opacity', 1);

	    ajaxMode = true;

	    //Регистрируем функцию на сортировку
		//   $('#sort-filter').on('change').on('change', function() {
		// var sort = $(this, ':checked').val();
		// reloadProducts(page, sort);
		// defaultSort = sort;
		// return false;
		//   });
	}
    });

    return false;
}

function reloadSearchProducts(page, search) {
    //var search_text = '#search-result'.text();
    var productsFieldId = '#products-list-and-paginator';

    if (page == undefined)
	page = 1;

    $.ajax({
		url: "/ajax/search.php",
		type: "POST",
		data: {"page": page, "search_text": search},
		beforeSend: function() {
		    $(productsFieldId).css('opacity', .3);
		},
		success: function(data, statusText, xhr, element) {
			console.log(statusText);
		    $(productsFieldId).find('.catalogue-list').empty().append(data).append($('<div data-x="test">').addClass('clearfix'));
		    var paginDiv = $(productsFieldId).find('.navigation').clone();
		    // console.log(data);
		    $(productsFieldId).find('.navigation').html('');
		    $('#paginator-container').html(paginDiv);
		    $(productsFieldId).css('opacity', 1);
		}
    });
    
    return false;
}


function isValidEmail(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function SliderInit(e) {
    e.Blocked = false;
    e.SlideWidth = $(e.Slides).eq(0).outerWidth(true);
    e.SlidesWidth = ($(e.Slides).length * e.SlideWidth);
    $(e.SlidesContainer).css('width', e.SlidesWidth + 'px');
    $(e.SlidesContainer).css('left', 0);
    $(e.SliderLeft).click(function() {
	if (e.Blocked == true)
	    return console.log("Don't do it!");
	e.Blocked = true;
	e.NewLeft = $(e.SlidesContainer).position().left + e.SlideWidth;
	$(e.SlidesContainer).animate(
		{left: e.NewLeft + 'px'},
	{duration: 300,
	    progress: function() {
		e.Blocked = true;
	    },
	    complete: function() {
		e.Blocked = false;
	    }}
	);
	if (e.NewLeft > 0) {
	    e.Blocked = true;
	    $(e.SlidesContainer).animate(
		    {left: '-' + (e.SlidesWidth - (e.SlideWidth * e.SlidesOnScreen)) + 'px'},
	    {duration: 500,
		progress: function() {
		    e.Blocked = true;
		},
		complete: function() {
		    e.Blocked = false;
		}}
	    );
	}
    });
    $(e.SliderRight).click(function() {
	if (e.Blocked == true)
	    return console.log("Don't do it!");
	e.Blocked = true;
	e.NewLeft = $(e.SlidesContainer).position().left - e.SlideWidth;
	$(e.SlidesContainer).animate(
		{left: e.NewLeft + 'px'},
	{duration: 300,
	    progress: function() {
		e.Blocked = true;
	    },
	    complete: function() {
		e.Blocked = false;
	    }}
	);
	if (e.NewLeft < -(e.SlidesWidth - (e.SlideWidth * e.SlidesOnScreen))) {
	    e.Blocked = true;
	    $(e.SlidesContainer).animate(
		    {left: '0px'},
	    {duration: 500,
		progress: function() {
		    e.Blocked = true;
		},
		complete: function() {
		    e.Blocked = false;
		}}
	    );
	}
    });
}

$(function(){
	$(".mm-cancel").on("click", function(){
		$(".close").click();
	});

	$(".mm-submit-price").on("click", function(){
		var form = $(this).parent();
		var name = form.find("input[name=name]").val();
		var phone = form.find("input[name=phone]").val();
		if (name == ("ФИО, Компания:"||"Введите ФИО!"||"ФИО должно быть не короче 3 символов!") && phone == ("Телефон:"||"Введите телефон!"||"Телефон должен быть не меньше 5 символов!")){
			form.find("input[name=name]").val("");
			form.find("input[name=phone]").val("");
			form.find("input[name=name]").attr("placeholder","Введите ФИО!");
			form.find("input[name=phone]").attr("placeholder","Введите телефон!");
		}
		else if (name.length <= 3 && phone.length <= 5 ) {
			form.find("input[name=name]").val("");
			form.find("input[name=phone]").val("");
			form.find("input[name=name]").attr("placeholder","ФИО должно быть не короче 3 символов!");
			form.find("input[name=phone]").attr("placeholder","Телефон должен быть не меньше 5 символов!");
		}
		else {
		$.ajax({
			url: "/ajax/basket.php?method=make_order",
			type: "POST",
			data: {name: name, phone: phone, "backcall": 1},
			success: function(data){
					var modal = form.parent();
					form.empty();
					modal.find("h2").text("Ваша заявка принята.");
					modal.find("p").text("В скором времени, мы с Вами свяжемся.");
				},
		});
		}
		return false;
	});
	
	function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
	}

	function getCookie(c_name) {
	    var i, x, y, ARRcookies = document.cookie.split(";");
	    for (i = 0; i < ARRcookies.length; i++) {
	        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
	        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
	        x = x.replace(/^\s+|\s+$/g, "");
	        if (x == c_name) {
	            return unescape(y);
	        }
	    }
	}
	$(".acsii").on("click", function(){
		var r = /^\w+@\w+\.\w{2,4}$/i;
		var mail = $(this).siblings().val();
		if(r.test(mail)){
			$.ajax({
				url: "/ajax/price.php?method=email_podpiska_aksii", 
				type: "POST", 
				data: {mail:mail},
				success: function(data){
					$("#popup3").find("p").text("Поздравляем, Вы успешно подписались на рассылку новых акций.");
					setCookie("acsii", 1);
					$(".mm-contact-form").find(".mm-row").remove();
					$(".mm-contact-form").append("<div style='text-align: center;'>Вы уже подписаны.</div>");
				}
			});
		}else{
			$("#popup3").find("p").text("Пожалуйста введите правильный емайл");
		}
		return false;
	});
	
function reportToYaMetrika(action,array){
	switch(action){
		case 'add':
		
			try {
				dataLayer.push({
					"ecommerce": {
						"add": {
							"products": [
							{
								"id": array[0] + '' ,
								"name": array[1] + '',
								"price": array[2],
							}
							]
						}
					}
				});
			}
			catch (e) {}
		
		break;
		case 'remove':
		
			try {
				dataLayer.push({
					"ecommerce": {
						"remove": {
							"products": [
							{
								"id": array[0] + '' ,
								"name": array[1] + '' ,
							}
							]
						}
					}
				});
			}
			catch (e) {}
		
		break;
		case 'purchase':
			
			try {
				dataLayer.push({
					"ecommerce": {
						"purchase": {
							"actionField": {
								"id" : array[1][0],
								"goal_id" : "7830906"
							},
							"products": array[0]
						}
					}
				});
			}
			catch (e) {}
		break;
		case 'fast_order':
			
			try {
				dataLayer.push({
					"ecommerce": {
						"purchase": {
							"actionField": {
								"id" : array[0],
								"goal_id" : "7830881"
							},
							"products": [
							{
								"id": array[1][0] + '' ,
								"name":array[1][1] + '' ,
								"quantity": 1
							}
							]}
					}
				});
			}
			catch (e) {}
		break;
	}
}

	
});