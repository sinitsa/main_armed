// page init
jQuery(function() {
	initRangeSlider();
});
price = {};
price.max = 0;
price.min = 100;

// range slider init
function initRangeSlider() {
	if (typeof price == 'undefined'){
		var maxPrice = 0;
		var minPrice = 999999;
	}else{
		var maxPrice = price.max;
		var minPrice = price.min;
	}
	
	var staticMin = minPrice;
	var staticMax = maxPrice;
	var suffix = ' p.';
	var minVal = jQuery('#min');
	var maxVal = jQuery('#max');

	var slider = jQuery("#range_1").ionRangeSlider({
		min: price.min,
		max: price.max,
		from: price.min,
		to: price.max,
		type: 'double',
		step: 1,
		postfix: "р.",
		prettify: false,
		hasGrid: false, 
		onLoad: function(obj) {
			minVal.on('change', function() {
				updateSlider();
			});
			maxVal.on('change', function() {
				updateSlider();
			});

			function updateSlider() {
				var currMin = parseInt(minVal.val(), 10);
				var currMax = parseInt(maxVal.val(), 10);

				if(jQuery.isNumeric(currMin) && jQuery.isNumeric(currMax) && currMin >= staticMin && currMin <= staticMax && currMax >= staticMin && currMax <= staticMax && currMin <= currMax) {
					slider.ionRangeSlider('update', {
						from: currMin,
						to: currMax
					});
				}
			}

			minVal.val(obj.fromNumber + suffix);
			maxVal.val(obj.toNumber + suffix);
		},
		onChange: function(obj) {
			minVal.val(obj.fromNumber + suffix);
			maxVal.val(obj.toNumber + suffix);
		},
		onFinish: function(obj){
			reloadProducts();
		}
	});
}