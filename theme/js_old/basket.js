function setCookie (name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}
function getCookie(c_name) {
	if (document.cookie.length > 0 ) {
		c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length+1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) c_end = document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}

function parseProductsString( string ) {
	var returnArray = new Array();
	
	if (string.length > 0 ) {
		var products = string.split('|');
		for (var i in products) {
			var temp = products[i].split(':');
			
			returnArray.push({
				'id' : parseInt(temp[0]),
				'amount' : parseInt(temp[1])
			});
		}
	}
	return returnArray;
}
function generateProductsString( products ) {
	var tempArray = new Array();
	for (var i in products) {
		tempArray.push(products[i].id + ':' +  products[i].amount);
	}
	return tempArray.join('|');
}
function addProductToBasket(productId, price) {
	//получим данные из кук
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount += price;
	} else {
		totalAmount = price;
	}
	
	//Если товар уже есть в куках, добавляем ему количество
	//Заодно подсчитаем количество всех товаров
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount++;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		products.push({
				"id" : productId,
				"amount" : 1
			});
		productsCount += 1;
	}
	
 
	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//Изменить оформление
	refreshBasketInfo();
        
        
}
function deleteProductFromBasket(productId, price) {
	//получим данные из кук
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount -= price;
	} else {
		totalAmount = 0;
	}
	
	//Если товар уже есть в куках, убавляем ему количество
	//Заодно подсчитаем количество всех товаров
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount--;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		//do nothing
	}
	
	
	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//Изменить оформление
	refreshBasketInfo();
}

function addToFavorites(productId) {
	//получаем данные из кук
	var products = parseProductsString( getCookie('fav_products') );
	products.push({
		"id" : productId,
		"amount" : 1
	});
	setCookie('fav_products', generateProductsString(products), '', '/');
        $.ajax({
            url : "/ajax/products.php?action=add_to_fav",
            type : "POST",
            dataType : "json",
            data : {"id" : productId},
            beforeSend: function() {},
            success : function (data, textStatus, jqXHR) {
                console.log('success');
            },
            complete : function() {},
            error : function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
	return false;
} 

function deleteFromFavorites(productId) {
	var products = parseProductsString( getCookie('fav_products') );
	var new_products = [];
	for (var i in products) {
		if (products[i].id != productId) {
			new_products.push({
				"id" : productId,
				"amount" : 1
			});			
		}		
	}
	setCookie('fav_products', generateProductsString(new_products), '', '/');
	return false;	
}

function setBasketInfo(amount , summ) {
	//Обратная совместимость
	refreshBasketInfo();
}
function refreshBasketInfo() {
	//получим данные из кук
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (isNaN(totalAmount)) {
		totalAmount = 0;
                $( "#small-cart-link" ).addClass( "inactive" );
	}else if(totalAmount == 0){
                $( "#small-cart-link" ).addClass( "inactive" );
            }else{
                $( "#small-cart-link" ).removeClass( "inactive" );
        }
	var productsCount = 0;
	for (var i in products) {
		productsCount += products[i].amount;
	}
	
	$('#basket-summ-products').text(productsCount);
	$('#basket-summ-products-float').text(productsCount);
	$('#basket-summ-price').text(moneyFormat(totalAmount) + ' руб.');
	$('#basket-summ-price-float').text(moneyFormat(totalAmount) + ' руб.');
	$('#basket-summ-products-text').text(declOfNum(productsCount, ['товар', 'товара', 'товаров']));
}

function recalculateBasket() {
	var productsCount = 0;
	var priceSumm = 0;
	var products = new Array();
	
	var _break = false;
	//Блокируем изменение инпутов до завершения аякса
	$('.amount-input').attr('disabled', 'disabled');
	
	$('#cart-items .basket-item').each(function (index, element) {
		if (_break == false) {
			var amount = $(element).find('input[name="amount"]').val();
			var price = $(element).find('input[name="price"]').val();
			var pid = $(element).find('input[name="id"]').val();
			
			if (amount == '0' || amount == '') {
				$(element).find('input[name="amount"]').val(1);
				amount = '1';
			}
			
			if (amount.match(/^[0-9]{1,3}$/)) {
				amount = parseInt(amount);
				productsCount += amount;
				priceSumm += parseInt(price) * amount;
				//
				$(element).find('.sum').text(moneyFormat(parseInt(price) * amount));
				//
				products.push({
					"id" : pid,
					"amount" : amount
				});
			} else {
				productsCount = '-';
				priceSumm = '-';
				_break = true;
			}
		}
	});

	//Если ошибок не было
	if (_break == false) {
		//Записываем куки
		setCookie('cart_products', generateProductsString(products), '', '/');
		setCookie('total_amount', priceSumm, '', '/');
		
		//Изменяем данные в блоке корзины
		setBasketInfo(productsCount, priceSumm);
		
		var coverField = $('#cart-items');
		var cover = $('<div></div>').css({
			'position' : 'absolute',
			'left' : coverField.offset().left,
			'top' : coverField.offset().top,
			'width' : coverField.width(),
			'height' : coverField.height(),
			'zIndex' : 100,
			'background' : '#fff',
			'opacity' : .6
		  }).html('<div style="text-align:center;">Обновляются данные...</div>').appendTo('body');
		
		//Запрос на стоимость доставкаи и размер скидки
		
		$.ajax({
			url: '/ajax/basket.php?method=get_delivery_and_discount',
			type : 'POST',
			dataType : 'json',
			data : {"products" : products},
			beforeSend : function () {
				$('#delivery-1 .value, #delivery-2 .value').html('<img src="/img/ajax_loading.gif" />');
			},
			success : function (data, textStatus, jqXHR) {
				
				orderPrice.delivery = intVal(data.deliveryPrice);
				orderPrice.discount = intVal(data.discount);
				orderPrice.summ = intVal(priceSumm);
				//showPriceInfo();
				
				//Изменяем данные на странице корзины
				$('.cart-items-sum .total-price-sum').html(moneyFormat(priceSumm));
				$('.cart-items-sum .discount .value').html(orderPrice.discount);
				if (orderPrice.discount > 0) {
					$('.cart-items-sum .discount').show();
				} else {
					$('.cart-items-sum .discount').hide();
				}
				
				$('#basket-products-count').html(productsCount);
	
			},
			complete : function () {
				cover.remove();
				//Разблокируем инпуты для ввода
				$('.amount-input').removeAttr('disabled');
			},
			error : function (jqXHR, textStatus, errorThrown) {}
		});
		
	}
	if (_break == false && productsCount > 0) {
		$('#continue-order').removeAttr('disabled');
	} else {
		$('#continue-order').attr('disabled','disabled');
		$('.amount-input').removeAttr('disabled');
	}
}


var deliveryType;
$(function(){

	//Листенер на кнопку "Купить"
	/*$('.buy-it-product a').bind('click', function() {
		return false;
	});*/
	//С карточки товара
	$('.buy-it-product, .buy-it-slider').bind('click', function() {
		setCookie('is_credit', 0, '', '/');
		yaCounter10746907.reachGoal('BUY');
		var button = $(this).find('div');
		if ($('#vkredit-btn .ico').length > 0){
			var credit_box = $('#vkredit-btn .ico');
			if (!credit_box.hasClass('empty')) {
				modalOpen('credit');		
				return false;
			}
		}
		if ( button.hasClass('buy') ) {
			var link = $(this);
			var price = link.data('product_price');
			var productId = link.data('product_id');
			
			addProductToBasket(productId, price);
			
			button.removeClass('buy').addClass('in-cart');
			
			return false;
		}
	});
	
	$('#vkredit-btn .credit-info').bind('click', function() {
		modalOpen('credit');		
		return false;
	}); 

	$('#vkredit-btn .ico').bind('click', function() {
		if ($(this).hasClass('empty'))
			$(this).removeClass('empty');
		else
			$(this).addClass('empty');	
		return false;
	}); 
	
	$('.modal-credit-submit').bind('click', function() {
		var link = $(this);
		var price = link.data('product_price');
		var productId = link.data('product_id');		
		
		addProductToBasket(productId, price);
		setCookie('is_credit', 1, '', '/');
		location.href = '/pay/korzina.html';			
		return false;
	});
	
	//Из листинга
	$('.buy-it-listing').bind('click', function() {
		yaCounter10746907.reachGoal('BUY');
		if ( $(this).hasClass('item-buy') ) {
			var link = $(this).find('a');
			var price = link.data('product_price');
			var productId = link.data('product_id');
			
			addProductToBasket(productId, price);
			
			$(this).removeClass('item-buy').addClass('item-incart');
			link.text('В корзине');
			
			return false;
		}
	});

	//Листенер кнопки "Удалить"
	$('.basket-item .cart-item-close').bind('click', function () {
		if (confirm('Вы действительно хотите удалить товар из корзины?')) {
			$(this).closest('.basket-item').remove();
			recalculateBasket();
		}
	});

	//Лисенер кнопки "избранное"
	$('.favorites-heart').bind('click', function(){
		var big_heart = $('.favorites-heart.big');
		var modal = $(this).find('div.heart-popup-left');
		var link = $(this).find('a');
		var id = link.data('product_id');
		
		if ( $(this).hasClass('empty') && !$(this).hasClass('big')) {
			modal.html('Уже&nbsp;в&nbsp;избранном');
			$(this).removeClass('empty');
			big_heart.removeClass('empty');
			addToFavorites(id);
		}	
		else {
			href = link.attr('href');
			location.href = href;
		} 
		return false;
	});	
	
	//Листенер изменение количества товаров в корзине
	var recalcTimer;
	$('.amount-input').bind('keyup', function (e) {
		var code = (e.keyCode ? e.keyCode : e.which);

		/*
		LEFT=   37
		UP=     38
		RIGHT=  39
		DOWN=   40
		SHIFT=  16
		CONTROL=    17
		ALT=    18
		*/
		var keys = [16,17,18,37,38,39,40];
		//Не реагировать на нажатие клавиш, обозначеных выше
		if (recalcTimer != undefined) clearInterval(recalcTimer);
		if (!(keys.indexOf(code) >= 0)) { 
			recalcTimer = setTimeout('recalculateBasket();', 1000);
		}
	});
	deliveryType = $('#order-form select[name="delivery_type"]').val();
	
	$('#order-form select[name="delivery_type"]').bind('change', function(){
		deliveryType = $(this,':checked').val();
		showPriceInfo();
	});
	/*
	$('.delivery-checkoption input').change( function() {
                console.log($(this).data("id"));
                if ($(this).data("id") == 5) {   
                   $('#self-export').show();
                }   
                else $('#self-export').hide();

        }); */
	//Обычный заказ
	$('#accept-order-button').bind('click', function () {
		yaCounter10746907.reachGoal('STEP2');
		$('#order-form').submit();
	});
	$('#order-form').bind('submit', function(){
			var form = $('#order-form');
			var name = form.find('input[name="name"]');
			var phone = form.find('input[name="phone"]');
			var payment = form.find('input[name="payment_type"]');
			var email = form.find('input[name="email"]');
			var loading = $('.order-loading');
			var sendButton = $('#accept-order-button');
			var inProcess = sendButton.hasClass('in-process');
			var arr = form.serializeArray();
			var payTemp = 0;
			$(form.serializeArray()).each(function(ind,element){
				if (element.name=='payment_type'){payTemp = element.value;}
			});
			
			if (inProcess)
				return false;
			
			var order_error = $('#order-error').find('.modal-popup-content');
			order_error.empty();
			var error;
			var isError = false;
			
			if (name.val().length <= 3) {
				isError = true;
				error = $('<div>').addClass('modal-error-msg');
				error.text('Не указаны ФИО');
				order_error.append(error);
			}	
			if (phone.val().length < 5) {
				isError = true;
				error = $('<div>').addClass('modal-error-msg');
				error.text('Пожалуйста, укажите номер вашего телефона');
				order_error.append(error);
			}
			if (payTemp == '2') {
				if ( !isValidEmail(email.val()) ) {
					isError = true;
					error = $('<div>').addClass('modal-error-msg');
					error.text('Укажите e-mail');
					order_error.append(error);
				}
			}
			
			if (isError) {				
				modalOpen('order-error');
				return false;
			}
			
			$.ajax({
					url: '/ajax/basket.php?method=make_order',
					type : 'POST',
					dataType : 'json',
					data : form.serialize(),
					beforeSend : function () {
						loading.show();
						form.find('input').attr('disabled','disabled');
						form.find('textarea').attr('disabled','disabled');
						sendButton.addClass('in-process');
					},
					success : function (data, textStatus, jqXHR) {
						yaCounter10746907.reachGoal('COMPLETE');
						var orderWin = $('#order-accept');
						orderWin.find('.urorder-info .name').text(name.val());
						orderWin.find('.urorder-info .phone').text(phone.val());
						orderWin.find('.urorder-info .order-number').text(data.order_code);
						
						orderWin.find('.uroder-toget-info .toget-info-price .sum').text(moneyFormat(data.price));
						
						if (data.payment == 'vkredit') {
							var exit_btn = $('#exit-btn');
							exit_btn.html(''); 
							exit_btn.html('Перейти к оформлению кредита'); 
							exit_btn.attr('href', '/pay/credit.html'); 
						}
						var itemsList = orderWin.find('.order-urorder .ordered-items')
						
						for(var i in data.products){
							var item = $('<div>').addClass('order-urorder-item');
							item
							.append(
								$('<span>').addClass('uroder-item-name').append(
									$('<a>').text(data.products[i].converted_title)
								)
							).append(
								$('<span>').addClass('urorder-item-price').html(
									moneyFormat(data.products[i].price_after_discount * data.products[i].amount) + ' <span>руб.</span>'
								)	
							);
							itemsList.append(item);
						}
						modalOpen('order-accept');
						/*
						var goodsArray = new Array();
						for(var i in data.products){
							goodsArray.push ({
							'id':data.products[i].id,
							'price':data.products[i].price,
							'name':data.products[i].converted_title,
							'quantity':data.products[i].amount,
							});
						}
						var yaParams = {
											  order_id: data.order_id,
											  order_price: data.price, 
											  currency: "RUR",
											  exchange_rate: 1,
											  goods: goodsArray
											};
						yaCounter21201121.reachGoal('ORDER', yaParams);
						*/
					},
					complete : function () {						
						loading.hide();
						form.find('input').removeAttr('disabled');
						form.find('textarea').removeAttr('disabled');
						sendButton.removeClass('in-process');
					},
					error : function (jqXHR, textStatus, errorThrown) {console.log(textStatus);}
				});
			return false;
	   });
});
function getNewSize(oldWidth, oldHeight, destWidth, destHeight) {
	if (destWidth) {var iScaleW = destWidth / oldWidth;} else {var iScaleW = 1;}
	if (destHeight) {var iScaleH = destHeight / oldHeight;} else {var iScaleH = 1;}

	var iSizeRelation = ( (iScaleW < iScaleH) ? iScaleW : iScaleH);
	var iWidthNew = Math.round(oldWidth * iSizeRelation);
	var iHeightNew = Math.round(oldHeight * iSizeRelation);

	var iSizeW = iWidthNew;
	var iSizeH = iHeightNew;
	iDestX =0;
	iDestY =0;
	
	return {"width" : iSizeW, "height" : iSizeH};
}
function moneyFormat(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ' ' + '$2');
	}
	return x1 + x2;
}
function declOfNum(number, titles) {  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}
function intVal( mixed_var, base ) {
	    var tmp;
	 
	    if( typeof( mixed_var ) == 'string' ){
	        tmp = parseInt(mixed_var);
			if(isNaN(tmp)){
	            return 0;
	        } else{
	            return tmp.toString(base || 10);
	        }
	    } else if( typeof( mixed_var ) == 'number' ){
	        return Math.floor(mixed_var);
	    } else{
	        return 0;
	    }
}