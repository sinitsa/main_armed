$(document).ready(function() {
	
	// Content Switcher
	var items = $('.dynamic-cont').find('.dynamic-setof-items');
	var index = 0;

	for (i=0; i<items.length; i++)
	{
		$(items[i]).addClass("dynamic-setof-items_"+i);
	}

	$('.dynamic-cont-controls-c li').click(function(event) {

		if ( $(this).hasClass('next') ) {
			if ( index < (items.length-1) ) {
				index += 1;
			} else{
				index = 0;
			};
		} else {
			if ( index <= (items.length-1) && index >= 1  ) {
				index -= 1;
			} else{
				index = items.length-1;
			};
		};

		cswitch(index);

		return false;
	});
	
	function cswitch (num) {
		items.hide();
		$('.dynamic-setof-items_' + num).fadeIn(500);
	}

});