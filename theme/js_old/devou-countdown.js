(function($){

	jQuery.fn.devouCountdown = function(options){

		options = $.extend({
			end_date: new Date("00 Jule 0000, 00:00:00"),
			digit_height: "33"
		}, options);

		//var make = function(){

                var cnt_height = options.digit_height;
                var msPerDay = 24 * 60 * 60 * 1000;

                var timeElem = {
                        timeElemD : $('.cnt-dwn-d .cnt-dwn-digit'),
                        timeElemH : $('.cnt-dwn-h .cnt-dwn-digit'),
                        timeElemM : $('.cnt-dwn-m .cnt-dwn-digit'),
                        timeElemS : $('.cnt-dwn-s .cnt-dwn-digit')
                }
                //флаг = true, если время акции истекло
                var end = false;    
                window.setInterval(function(){
                        var today = new Date();
                        var timeLeft = (options.end_date.getTime() - today.getTime());
                       
                        
                        //если время акции истекло, изменяем цену на прежнюю
                        if ((timeLeft <= 0) && (end === false)) {
                            end = true;
                            var id = options.id.split('countdown')[1];
                            $.ajax({
                                url: "/ajax/countdown.php?method=action_complete",
                                method: "POST",
                                dataType: "json",
                                data: {"id" : id},
                                success: function(data, textStatus, jqXHR){
                                    
                                },
                                complete: function(){}
                            });
                            return false;
                        }
                        var e_daysLeft = timeLeft / msPerDay;
                        var daysLeft = Math.floor(e_daysLeft);

                        var e_hrsLeft = (e_daysLeft - daysLeft)*24;
                        var hrsLeft = Math.floor(e_hrsLeft);

                        var e_minsLeft = (e_hrsLeft - hrsLeft)*60;
                        var minsLeft = Math.floor(e_minsLeft);

                        var e_secsLeft = (e_minsLeft - minsLeft)*60;
                        var secsLeft = Math.floor(e_secsLeft);

                        var timeString = daysLeft + " : " + hrsLeft + " : " + minsLeft + " : " + secsLeft;

                        init(daysLeft, hrsLeft, minsLeft, secsLeft);

                }, 1000);

                function init (daysLeft, hrsLeft, minsLeft, secsLeft, digit_height) {
                        hc = Math.round(hrsLeft * 0.22 + hrsLeft);

                        timeElem.timeElemD.html('<span>' + daysLeft + '</span>').css("background-position", "0px " + - (options.digit_height * 2 * daysLeft - options.digit_height) + "px");;
                        timeElem.timeElemH.html('<span>' + hrsLeft + '</span>').css("background-position", "0px " + - (options.digit_height * 2 * hc) + "px");
                        timeElem.timeElemM.html('<span>' + minsLeft + '</span>').css("background-position", "0px " + - (options.digit_height * minsLeft) + "px");
                        timeElem.timeElemS.html('<span>' + secsLeft + '</span>').css("background-position", "0px " + - (options.digit_height * secsLeft) + "px");
                }

		//};

		//return this.each(make); 

	};

})(jQuery);