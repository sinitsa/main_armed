$(document).ready(function() {
	// Ajax Modal Popup
//	$('.modal-call').click(function(event) {
//		$('.modal-popup-content').empty().append("<img src='/img/load.gif' alt='Loading' />");
//
//		$.ajax({
//			url: this.href,
//			complete: function(xhr, textStatus) {
//				console.log('AJAX:COMPLETE');
//			},
//			success: function(data, textStatus, xhr) {
//				// build modal window
//				$('.modal-popup-content').empty().append(data);
//				$('.modal-bg-overlay, .modal-popup').show();
//
//				var width = $('.modal-popup-content').outerWidth();
//				$('.modal-popup').width(width);
//				$('.modal-popup').css('margin-left', - width / 2);  
//				$('.modal-popup-content').css('float', 'none');  
//
//				console.log('AJAX:SUCCES');
//			},
//			error: function(xhr, textStatus, errorThrown) {
//				console.log('AJAX:ERROR');
//			}
//		});
//
//		return false;
//	});

	
	/*
	// build modal window
	$('.modal-bg-overlay, .modal-popup').show();

	var width = $('.modal-popup-content').outerWidth();
	$('.modal-popup').width(width);
	$('.modal-popup').css('margin-left', - width / 2);
	$('.modal-popup-content').css('float', 'none');
	*/
	
	// close modal popup
	$('.modal-popup-close a').on("click", function(event){
		$('.modal-bg-overlay, .modal-popup').hide();

		return false;
	});
});

function modalOpen(stringId) {
		// build modal window
		$('.modal-bg-overlay, #' + stringId + '.modal-popup').show();

		var content = $('#' + stringId).find('.modal-popup-content');
		var popup = $('#' + stringId);
		
		var width = content.outerWidth();
		popup.width(width);
		popup.css('margin-left', - width / 2);
		
		popup.css('top', $(document).scrollTop() + 50);
		
		content.css('float', 'none');
}
function modalClose(stringId) {
	$('.modal-bg-overlay, #' + stringId + '.modal-popup').hide();
}