$(document).ready(function(){
	$('.cat_modal-open-link').click(function(){
		var $id = $(this).attr('href').replace('#','');
		$('#'+$id).fadeToggle();
	});
	$('.cat_modal-bg-overlay').click(function(){
		$(this).parent().fadeOut();
	});
	$('.cat_modal-close').click(function(){
		$(this).parent().parent().fadeOut();
	});
});