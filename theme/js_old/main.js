
function catModalOpenLink(item) {

    $.ajax({
	url: "/ajax/modal_item.php",
	cache: false,
	type: "POST",
	data: ({item: item}),
	// dataType: "html",
	success: function(html) {
	    $("#mdlcls").append(html).show();
	},
	complete: function(){
		$(".product-item-view-detailed .dynamic-cont .dynamic-setof-items").carouFredSel({
			circular: false,
			infinite: false,
			align: "left",
			width: 350,
			height: 120,
			items: 3,
			auto: false,
			prev: '.prev',
			next: '.next',
			scroll: {
				items: 1
			}
		});
		$('.product-item-view-detailed .dynamic-setof-items .image-wrap2 a').bind('click', function() {
			var container = $('.product-item-img');
			var _this = this;

			container.empty();
			
			container.append($('<img>').attr('src', $(this).data('original')));
			
			// $('.product	-item-view-detailed .dynamic-cont .dynamic-setof-items a').each(function(index, element) {
				// var link = $('<a>').attr('href', $(element).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
				// if (_this == element)
				// link.append(
					// $('<img>').attr('src', $(element).attr('href'))
					// );
				// alert(link);
				// container.append(link);
			// });
			return false;
		});
		//$('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a:first').click();
		
		$('#mdlcls').css('overflow-y','auto');
		$('body').css('overflow','hidden').css('position','relative').css('left','-7px');
	}
    });

    return false;

}

$(document).ready(function() {
 /* $('.catalogue-item').each(function(){
		if ($(this).find('.catalogue-item-countdown').length) {
			$(this).find('.catalogue-item-description').hide();
		}
	});
	$('.catalogue-item').mouseover(function(){
		if ($(this).find('.catalogue-item-countdown').length) {
			$(this).find('.catalogue-item-description').show();
		}	
	});
	$('.catalogue-item').mouseleave(function(){
		if ($(this).find('.catalogue-item-countdown').length) {
			$(this).find('.catalogue-item-description').hide();
		}	
	}); */
	
	$('.cat_modal-open-link').on('click', function(){
		var item = $(this).closest('.catalogue-item-wrap').find('.catalogue-item-btn a').data('product_id');
		catModalOpenLink(item);
	});
	
    //Close popups if click outside the window
    $('.cat_modal-overlay').click(function(e) {
		$('#mdlcls').css('display', 'none');
		$('body').css('overflow','auto').css('position','').css('left','0px');
		$('#mdlcls').css('overflow-y','hidden');
		$('#mdlcls').html('');
		return false;
    }).on('click','.cat_modal-body', function(e) { 
		e.stopPropagation();
    }).on('click','.cat_modal-close', function(e) { 
		$('#mdlcls').css('display', 'none');
		$('body').css('overflow','auto').css('position','').css('left','0px');
		$('#mdlcls').css('overflow-y','hidden');
		$('#mdlcls').html('');
		return false;
    });
	
    //Close popups if click outside the window
    $('.modal-bg-overlay').click(function() {
		$('.modal-bg-overlay').css('display', 'none');
		$('.modal-popup').css('display', 'none');
		return false;
    });

    // // Styling
    // Header. More Menu
    $('.header-more-menu .btn-tpl').click(function(event) {
	$(this).closest('li').find('ul').show();

	return false;
    });
    // Header. More Menu v2
    $('.header-more-menu > a').click(function(event) {
	$(this).closest('li').find('ul').show();
	return false;
    });
    $(document).click(function(event) {
	$('.header-more-menu').find('ul').hide();
    });

    // Catalogue Sidebar
    $('li.catalogue-list-item').children('span').click(function(event) {
	_elem = $(this).parents('li.catalogue-list-item');

	$(_elem).toggleClass('active');
	$(_elem).find('ul.main-slider-endlist').toggle();
	$(this).parent().next('li.catalogue-list-item').toggleClass('after-active');

	return false;
    });
    /*
     $('.catalogue-sidemenu-lvl2 li ul li a').click(function(event) {
     $(this).parent('li').toggleClass('active');
     
     return false;
     });*/

    // Catalogue Product
    $('.catalogue-item, .catalogue-item a img').mouseenter(function(event) {
	title = $(this).parents('.catalogue-item').find('.catalogue-item-title');

	$(this).addClass('active');
	$(title).addClass('active');
	$(this).find('.description-overlay').hide();
    }).mouseleave(function(event) {
	title = $(this).parents('.catalogue-item').find('.catalogue-item-title');

	$(this).removeClass('active');
	$(title).removeClass('active');
	$(this).find('.description-overlay').show();
    });

    $('.item-stock, .item-delivery').hover(function() {
	$(this).next().find('.item-tooltip').css('display', 'inline-block');
    }, function() {
	$(this).next().find('.item-tooltip').hide();
    });

    // Articles
    $('.article-item-title a, .g-img-cont').mouseenter(function(event) {
	imgt = $(this).parents('.footer-more-item, .announce-item, .article').find('.g-img-cont');

	$(imgt).addClass('active');
	$(this).prev().addClass('active');
    }).mouseleave(function(event) {
	$(imgt).removeClass('active');
	$(this).prev().removeClass('active');
    });

    // Filter More _btn
    $('.filter-more-btn').click(function(event) {
	$('.filter-more-container').slideToggle(100);
	$(this).find('.btn-tpl').toggle();
	$(this).toggleClass('active')

	return false;
    });

    // Tags Cloud List
    $('.journal-tags-switch li').click(function(event) {
	$('.journal-tags-switch li').toggleClass('active');
	$('.journal-tags-list').toggleClass('active');

	return false;
    });

    //// Cart
    // Cart - Additional service
    $('.cart-addservice-check input.styled').show();
    $('.cart-addservice-check input').on('click', function(event) {
	var _val = $(this).attr('value');
	$(this).parents('.cart-addservice-check').find('div :checkbox').not(this).removeAttr('checked');

	var _tmp = $(this).parents('.cart-item').find('.cart-addservice-setof-price span.' + _val)
	_tmp.toggleClass('active');
	$(this).parents('.cart-item').find('.cart-addservice-setof-price span').not(_tmp).removeClass('active');
    });

    // Cart - Items
    $('.cart-item .cart-item-close').click(function() {
	_elem = $(this).parents('.cart-item').find('ul li input');

	if ((_elem.val()) != (0)) {
	    currv = $(this).parents('.cart-item').find('ul li input').val();
	    _elem.val(0);
	    $(this).parent('.cart-item').addClass('inactive');
	} else {
	    _elem.val(currv);
	    $(this).parent('.cart-item').removeClass('inactive');
	}

	return false;
    });

    // Placeholder
    $('input.placeholder, textarea.placeholder').focus(function() {
	_input_val = $(this).attr('value');

	$(this).attr('value', '');
	if ($(this).is("textarea")) {
	    $(this).empty();
	}
	;
    });
    $('input.placeholder, textarea.placeholder').blur(function() {
	$(this).attr('value', _input_val);
	if ($(this).is("textarea")) {
	    $(this).html('Ваше сообщение:');
	}
	;
    });


    // Carousel


//if($('.product-item-view-detailed .dynamic-setof-items .image-wrap').length) {
    $(".product-item-view-detailed .dynamic-cont .dynamic-setof-items").carouFredSel({
	circular: false,
	infinite: false,
	align: "left",
	width: 350,
	height: 120,
	items: 3,
	auto: false,
	prev: '.prev',
	next: '.next',
	scroll: {
	    items: 1
	}
    });

    $('.product-item-view-detailed .dynamic-setof-items .image-wrap a').bind('click', function() {
	var container = $('.product-item-img');
	var _this = this;

	container.empty();

        var itemTitle = $('.product-title h1').html();
        
	$('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a').each(function(index, element) {
	    var link = $('<a>').attr('href', $(element).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
	    var img = $('<img>');
            if (_this == element) {
		img.attr('src', $(element).attr('href'));
		img.attr('alt', itemTitle);
                link.append(
			img
			);
            }    
	    container.append(link);
	});
	return false;
    });
    $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a:first').click();
    $("a.fgallery").fancybox();
//}	
    /*	if($('.product-item-view-detailed .dynamic-setof-items a').length) {
     var s8pos = 0;
     var s8col = $('.product-item-view-detailed .dynamic-setof-items a').length;
     var imgWidth = 126 + 5;
     var imgOnSlide = 3;
     var lastPos = s8col - imgOnSlide - 1;
     if (lastPos < 0) lastPos = 0; 
     
     $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items').css('width', s8col*imgWidth);
     $('.product-item-view-detailed .image-extra-wrap').css('overflow', 'hidden');
     //$('.product-item-view-detailed .dynamic-setof-items').css('width', s8col*imgWidth);
     
     $('.photo-slide li.next a').click(function(){
     s8pos++;
     if(s8pos>lastPos) s8pos=0;
     $('.product-item-view-detailed .dynamic-setof-items').animate({'left':-(s8pos*imgWidth)}, 300);
     return false;
     });
     
     $('.photo-slide li.prev a').click(function(){
     s8pos--;
     if(s8pos<0) s8pos=lastPos;
     $('.product-item-view-detailed .dynamic-setof-items').animate({'left':-(s8pos*imgWidth)}, 300);
     return false;
     });
     
     $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a').bind('click', function () {
     var container = $('.product-item-img');
     var _this = this;
     
     container.empty();
     
     
     $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a').each(function(index, element){
     var link = $('<a>').attr('href', $(element).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
     if (_this == element)
     link.append(
     $('<img>').attr('src', $(element).attr('href'))
     );
     container.append(link);
     });
     return false;
     });
     $('.product-item-view-detailed .dynamic-cont .dynamic-setof-items a:first').click();
     $("a.fgallery").fancybox();
     }
     */



    //product

    $('#fast-order-send-button').bind('click', function() {
	var wrap = modal = $('#fast-order');
	var form = modal.find('form');
	var loading = modal.find('.loading');
	var buttons = modal.find('.fast-order-modal-submit');

	var answer = modal.find('.answer');
	var response = modal.find('.response');

	var name = wrap.find('input[name="name"]');
	var phone = wrap.find('input[name="phone"]');
	//var email =	wrap.find('input[name="email"]');
	var product_id = wrap.find('input[name="product_id"]');
	//var button = $(this);

	if (
		name.val().length <= 1 ||
		phone.val().length < 5 ||
		name.val() == name.prop('defaultValue') ||
		phone.val() == phone.prop('defaultValue')
		) {
	    alert('Введите Ваше имя и телефон');
	    return false;
	}
	if (wrap.hasClass('in-process')) {
	    return false;
	}
	$.ajax({
	    url: '/ajax/basket.php?method=make_order',
	    type: 'POST',
	    dataType: 'json',
	    data: {'name': name.val(), "phone": phone.val(), "product_id": product_id.val(), "fast_order": 1},
	    beforeSend: function() {
		name.attr('disabled', 'disabled');
		phone.attr('disabled', 'disabled');
		//email.attr('disabled', 'disabled');
		//button.attr('disabled', 'disabled');
		loading.show();
		buttons.hide();

		wrap.addClass('in-process');
	    },
	    success: function(data, textStatus, jqXHR) {
			yaCounter10746907.reachGoal('FAST_ORDER');
			answer.hide();
			response.find('.name').text(name.val());
			response.find('.number').text(data.order_code);
			response.show();
	    },
	    complete: function() {
		name.removeAttr('disabled');
		phone.removeAttr('disabled');
		//email.removeAttr('disabled');

		buttons.show();
		loading.hide();
		wrap.removeClass('in-process');
	    }
	});
	return false;

    });

    //callback
    $('#callback .modal-cancel').bind('click', function() {
	var wrap = $('#callback');
	wrap.find('.loading').hide();

	wrap.find('.answer').show();
	wrap.find('.response').hide();

	modalClose('callback');
    });

    $('#callback .modal-submit').bind('click', function() {
	var wrap = $('#callback');
	var phone = wrap.find('input[name="phone"]');
	var name = wrap.find('input[name="name"]');
//		var email = wrap.find('input[name="email"]');
//		var msg = wrap.find('textarea[name="msg"]');

	var loading = wrap.find('.loading');

	var answer = wrap.find('.answer');
	var response = wrap.find('.response');



	if (
		name.val().length <= 1 ||
		phone.val().length < 5 ||
		name.val() == name.prop('defaultValue') ||
		phone.val() == phone.prop('defaultValue')
		) {
	    alert('Введите Ваше имя и телефон');
	    return false;
	}

	/*if (
	 !isValidEmail(email.val()) 
	 ) {
	 alert('Не правильный E-mail');
	 return false;
	 }*/

	/*if (
	 msg.val().length < 3 ||
	 msg.val().length > 999
	 ) {
	 alert('Длина сообщения должна составлять от 3 до 999 символов!');
	 return false;
	 }
	 */

	if (wrap.hasClass('in-process')) {
	    return false;
	}

	$.ajax({
	    url: '/ajax/basket.php?method=make_order',
	    type: 'POST',
	    dataType: 'json',
	    data: {
		'name': name.val(),
		"phone": phone.val(),
		"backcall": 'true'
	    },
	    beforeSend: function() {
		name.attr('disabled', 'disabled');
		phone.attr('disabled', 'disabled');

		loading.show();

		wrap.addClass('in-process');
	    },
	    success: function(data, textStatus, jqXHR) {
		answer.hide();
		response.show();
	    },
	    complete: function() {
		name.removeAttr('disabled');
		phone.removeAttr('disabled');

		loading.hide();
		wrap.removeClass('in-process');
	    }
	});

	return false;

    });
    $('#shop-feedback .ask-question .form-tpl-submit .btn-tpl').bind('click', function() {
	var wrap = $('#shop-feedback');
	var form = $('#shop-feedback .ask-question form');
	var loading = wrap.find('.loading2');
	//var buttons = modal.find('.fast-order-modal-submit');

	var answer = wrap.find('.ask-question');
	var response = wrap.find('.ask-response');

	var name = wrap.find('input[name="name"]');
	var email = wrap.find('input[name="email"]');
	var msg = wrap.find('textarea[name="msg"]');
	//var product_id = wrap.find('input[name="product_id"]');
	//var button = $(this);

	if (
		name.val().length <= 1 ||
		!isValidEmail(email.val()) ||
		msg.val().length < 3 ||
		name.val() == name.prop('defaultValue') ||
		msg.val() == msg.prop('defaultValue') ||
		email.val() == email.prop('defaultValue')
		) {
	    alert('Введите Ваше имя, email и текст сообщения');
	    return false;
	}
	if (wrap.hasClass('in-process')) {
	    return false;
	}
	$.ajax({
	    url: '/ajax/system.php?method=addshopfeedback',
	    type: 'POST',
	    dataType: 'json',
	    data: {
		'username': name.val(),
		'usermail': email.val(),
		"usermsg": msg.val(),
	    },
	    beforeSend: function() {
		name.attr('disabled', 'disabled');
		msg.attr('disabled', 'disabled');
		email.attr('disabled', 'disabled');
		//button.attr('disabled', 'disabled');
		loading.show();
		//buttons.hide();

		wrap.addClass('in-process');
	    },
	    success: function(data, textStatus, jqXHR) {
		answer.hide();
		response.show();
	    },
	    complete: function() {
		name.removeAttr('disabled');
		msg.removeAttr('disabled');
		email.removeAttr('disabled');

		//buttons.show();
		loading.hide();
		wrap.removeClass('in-process');
	    }
	});
	return false;
    });

    //Shop feed back
    $('#shop-feedback-button').bind('click', function() {
	var wrap = $('#shop-feedback');
	var loading = wrap.find('.loading');
	var feeds = wrap.find('.feeds');
	var pagin = wrap.find('.paginator');

	modalOpen('shop-feedback');



	loadShopFeeds(1);

	function loadShopFeeds(page) {
	    $.ajax({
		url: '/ajax/system.php',
		type: 'POST',
		dataType: 'json',
		data: {"method": 'getshopfeedback', 'page': page},
		beforeSend: function() {
		    loading.show();
		},
		success: function(data, textStatus, jqXHR) {
		    feeds.empty();

		    if (data.messages.length > 0) {
			//feeds
			var first = true;
			for (var i in data.messages) {
			    feeds.append(makeFeed(data.messages[i], first));
			    first = false;
			}
			//pagin
			pagin.empty();
			pagin.append(
				makePagin(data.pagin_data.currentPage, data.pagin_data.pagesCount)
				);
			$(document).scrollTop($('#feeds').offset().top - 100);

		    } else {
			feeds.append($('<div>').text('Отзывов нет. Станьте первым.'));
		    }
		},
		complete: function() {
		    loading.hide();
		}
	    });
	}

	function makeFeed(message, first) {
	    var m = $('<div>').addClass('dialog-item');
	    if (first)
		m.addClass('first');

	    m.append(
		    $('<div>').addClass('dialog-item-title').append(
		    $('<span>').text(message.date)
		    ).append(' ' + message.name)
		    ).append(
		    $('<p>').text(message.des)
		    );
	    if (message.otvet.length > 0) {
		m.append($('<div>').addClass('answer').append(
			$('<div>').addClass('caption').text('Ответ')
			).append(
			$('<div>').addClass('body').text(message.otvet)
			)
			);
	    }

	    /*
	     <div class="dialog-item first">
	     <div class="dialog-item-title"><span>28.01.2013</span> Дмитрий</div>
	     <p>Отличный магазин, прекрасные сотрудники! Спасибо!</p>
	     <div class="answer">
	     <div class="caption">Ответ</div>
	     <div class="body">Не хуй собачий!</div>
	     </div>
	     </div>
	     */

	    return m;
	}

	function makePagin(current, count) {
	    var ul = $('<ul>');

	    if (count > 1) {
		for (i = 1; i <= count; i++) {
		    var li = $('<li>');

		    li.append(
			    $('<span>').append(
			    $('<a>').text(i)
			    )
			    ).data('page', i);
		    if (i == current)
			li.addClass('active');
		    else
			li.bind('click', function() {
			    loadShopFeeds($(this).data('page'));
			});

		    ul.append(li);
		}
	    }

	    return ul;
	    /*
	     <ul>
	     <li class="prev"><span><a href="./"></a></span></li>
	     
	     <li><span><a href="./">1</a></span></li>
	     ...
	     <li class="active"><span><a href="./">5</a></span></li>
	     ...
	     <li><span><a href="./">27</a></span></li>
	     
	     <li class="next"><span><a href="./"></a></span></li>
	     </ul>
	     */
	}

	//Close feedback, when click outside the form.	
	$('.modal-bg-overlay').click(function() {
	    $('.modal-bg-overlay').css('display', 'none');
	    $('.modal-popup').css('display', 'none');
	});

    });


    // Adaptive Size
    function checkWidth(init) {
	if ($(window).width() < 1330) {
	    $('body').addClass('w960');
	} else {
	    if (!init) {
		$('body').removeClass('w960');
	    }
	}
	;
    }

    checkWidth(true);

    $(window).resize(function() {
	checkWidth(false);
    });
});

function reloadProducts(page, sort) {
    var formId = '#filter-form';
    var productsFieldId = '#products-list-and-paginator';

    var loadingImage = $('#filter-loading').clone();
    var ss;

    if (page == undefined)
	page = (defaultPage == undefined) ? 1 : defaultPage;
    if (sort == undefined) {
	sort = (defaultSort == undefined) ? 'none' : defaultSort;
    }
    $(formId).ajaxSubmit({
	data: {"page": page, "sort": sort},
	beforeSend: function() {
	    $(productsFieldId).css('opacity', .3);
	    //show loading
	    loadingImage.appendTo('body');
	    loadingImage.css({
		'display': 'block',
		'position': 'absolute',
		'left': $(productsFieldId).position().left + 20,
		'top': $(productsFieldId).position().top
	    });
	    ss = $('body, html').mousemove(function(e) {
		loadingImage.css({
		    'left': (e.pageX + 10) + 'px',
		    'top': (e.pageY + 20) + 'px'
		});
	    });
	},
	success: function(data, statusText, xhr, element) {
	    $(productsFieldId).find('.catalogue-list').empty().append(data).append($('<div>').addClass('clearfix'));

	    var paginDiv = $(productsFieldId).find('.navigation').clone();
	    $(productsFieldId).find('.navigation').remove();

	    $('#paginator-container').html(paginDiv);

	    $(productsFieldId).css('opacity', 1);

	    //Hide loading
	    ss.unbind('mousemove');
	    loadingImage.remove();

	    ajaxMode = true;

	    //Регистрируем функцию на сортировку
	    $('#sort-filter').unbind('change').bind('change', function() {
		var sort = $(this, ':checked').val();
		reloadProducts(page, sort);
		defaultSort = sort;
		return false;
	    });
	}
    });

    return false;
}

function reloadSearchProducts(page, search) {
    //var search_text = '#search-result'.text();
    var productsFieldId = '#products-list-and-paginator';

    if (page == undefined)
	page = 1;

    $.ajax({
		url: "/ajax/search.php",
		type: "POST",
		data: {"page": page, "search_text": search},
		beforeSend: function() {
		    $(productsFieldId).css('opacity', .3);
		},
		success: function(data, statusText, xhr, element) {
			console.log(statusText);
		    $(productsFieldId).find('.catalogue-list').empty().append(data).append($('<div data-x="test">').addClass('clearfix'));
		    var paginDiv = $(productsFieldId).find('.navigation').clone();
		    // console.log(data);
		    $(productsFieldId).find('.navigation').html('');
		    $('#paginator-container').html(paginDiv);
		    $(productsFieldId).css('opacity', 1);
		}
    });
    

    
    return false;
}


function isValidEmail(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function SliderInit(e) {
    e.Blocked = false;
    e.SlideWidth = $(e.Slides).eq(0).outerWidth(true);
    e.SlidesWidth = ($(e.Slides).length * e.SlideWidth);
    $(e.SlidesContainer).css('width', e.SlidesWidth + 'px');
    $(e.SlidesContainer).css('left', 0);
    $(e.SliderLeft).click(function() {
	if (e.Blocked == true)
	    return console.log("Don't do it!");
	e.Blocked = true;
	e.NewLeft = $(e.SlidesContainer).position().left + e.SlideWidth;
	$(e.SlidesContainer).animate(
		{left: e.NewLeft + 'px'},
	{duration: 300,
	    progress: function() {
		e.Blocked = true;
	    },
	    complete: function() {
		e.Blocked = false;
	    }}
	);
	if (e.NewLeft > 0) {
	    e.Blocked = true;
	    $(e.SlidesContainer).animate(
		    {left: '-' + (e.SlidesWidth - (e.SlideWidth * e.SlidesOnScreen)) + 'px'},
	    {duration: 500,
		progress: function() {
		    e.Blocked = true;
		},
		complete: function() {
		    e.Blocked = false;
		}}
	    );
	}
    });
    $(e.SliderRight).click(function() {
	if (e.Blocked == true)
	    return console.log("Don't do it!");
	e.Blocked = true;
	e.NewLeft = $(e.SlidesContainer).position().left - e.SlideWidth;
	$(e.SlidesContainer).animate(
		{left: e.NewLeft + 'px'},
	{duration: 300,
	    progress: function() {
		e.Blocked = true;
	    },
	    complete: function() {
		e.Blocked = false;
	    }}
	);
	if (e.NewLeft < -(e.SlidesWidth - (e.SlideWidth * e.SlidesOnScreen))) {
	    e.Blocked = true;
	    $(e.SlidesContainer).animate(
		    {left: '0px'},
	    {duration: 500,
		progress: function() {
		    e.Blocked = true;
		},
		complete: function() {
		    e.Blocked = false;
		}}
	    );
	}
    });
}
