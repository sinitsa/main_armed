/**
 * Window width variable value for HD screent
 * 
 * @default 1300
 */
var HDState = 1300;

/**
 * Get DOM element width
 * 
 * @param elm
 * @example <caption>Example usage of getWidth()</caption> getWidth('#sample');
 * @returns {Number} Returns the value of '#sample' DOM element width.
 */
function getWidth(elm) {
	return $((!elm) ? document : elm).width();
}

/**
 * Adapt for different screen resolutions
 * 
 * @param width
 * @example <caption>Example usage of adaptScreen()</caption>
 * @example adaptScreen(1200);
 * @todo Set for different DOM elements
 */
function adaptScreen(width) {
	// If no width, exit.
	if (!width) {
		return;
	}
	// default DOM element
	var elm = 'body';
	// Check HD resolution
	if (width > HDState) {
		// is true
		$(elm).addClass('HD');
	} else {
		// or false
		$(elm).removeClass('HD');
	}
}

/**
 * Set equal height for different DOM elements
 * 
 * @param columns
 * @example <caption>Example usage of adaptScreen()</caption>
 * @example setEqualHeight('.column');
 */
function setEqualHeight(columns) {
	// Set tallest element to zero
	var tallestcolumn = 0;
	//
	columns.each(function() {
		// Clear height value to auto to current column
		// NFO: strongly recommended for resize window state
		$(this).height('auto');
		// Get current element height value
		currentHeight = $(this).height();
		if (currentHeight > tallestcolumn) {
			// set tallest element
			tallestcolumn = currentHeight;
		}
	});
	// set height for columns group by the tallest element height value
	columns.height(tallestcolumn);
}

function smartColumns() {

	$("ul.column").css({
		'width' : "100%"
	});
	var colWrap = $("ul.column").width();
	var colNum = Math.floor(colWrap / 120);
	var colFixed = Math.floor(colWrap / colNum);
	$("ul.column").css({
		'width' : colWrap
	});
	$("ul.column li").css({
		'width' : colFixed
	});
}

jQuery.fn.extend({
	insertAtCaret : function(myValue) {

		return this.each(function(i) {

			if (document.selection) {
				this.focus();
				var sel = document.selection.createRange();
				sel.text = myValue;
				this.focus();
			} else if (this.selectionStart || this.selectionStart == '0') {
				var startPos = this.selectionStart;
				var endPos = this.selectionEnd;
				var scrollTop = this.scrollTop;
				this.value = this.value.substring(0, startPos) + myValue
						+ this.value.substring(endPos, this.value.length);
				this.focus();
				this.selectionStart = startPos + myValue.length;
				this.selectionEnd = startPos + myValue.length;
				this.scrollTop = scrollTop;
			} else {
				this.value += myValue;
				this.focus();
			}
		})
	}
});

$(document)
		.ready(
				function() {
					// Content Switcher
					var items = $('#splash-container').find('.item');
					var index = 0;
					var before = 0;
					var after = 0;

					for (i = 0; i < items.length; i++) {
						$(items[i]).addClass("item_" + i);
					}

					$('#splash-container .pagination a')
							.click(
									function(event) {

										if ($(this).hasClass('next')) {
											if (index < (items.length - 1)) {
												index += 1;
												before = index - 1;
												after = index + 1;
											} else {
												index = 0;
												before = items.length - 1;
												after = index + 1;
											}

										} else {
											if (index <= (items.length - 1)
													&& index >= 1) {
												index -= 1;
												before = index - 1;
												after = index + 1;
											} else {
												index = items.length - 1;
												before = index - 1;
												after = index + 1;
											}

											if (index == 0) {
												before = items.length - 1;
												after = index + 1;
											}
										}

										if (index == items.length - 1) {
											after = 0;
										}

										var beforeImg = $('.item_' + before)
												.find('img').attr('src');

										var afterImg = $('.item_' + after)
												.find('img').attr('src');
										$(
												'#splash-container .pagination .before')
												.find('img')
												.fadeOut(
														1,
														function() {

															$(
																	'#splash-container .pagination .before')
																	.find('img')
																	.attr(
																			'src',
																			beforeImg);
														});
										$(
												'#splash-container .pagination .before')
												.find('img').fadeIn(400);

										$(
												'#splash-container .pagination .after')
												.find('img')
												.fadeOut(
														1,
														function() {

															$(
																	'#splash-container .pagination .after')
																	.find('img')
																	.attr(
																			'src',
																			afterImg);
														});
										$(
												'#splash-container .pagination .after')
												.find('img').fadeIn(400);

										cswitch(index);

										return false;
									});

					function cswitch(num) {

						items.hide();
						$('.item_' + num).fadeIn(500);
					}

				});
