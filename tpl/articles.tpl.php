



	<!-- Main Container -->
	<div class="main-cont">
		<!-- Sidebar -->
		<div class="main-sider">
		<!--	<div class="subscribe btn-tpl big green">
				<span><a href="/">Подписаться на рассылку</a></span>
			</div>

			<div class="widget-facebook">
				<div class="fb-like-box" data-href="https://www.facebook.com/FacebookDevelopers" data-width="239" data-show-faces="true" data-stream="false" data-show-border="true" data-header="true"></div>
			</div>-->

			<!-- VK Widget
			<div class="widget-vk">
				<div id="vk_groups"></div>
			</div> -->
		
		</div>
		<!-- Content -->
		<div class="content">
			


			<div class="title title-journal">
				<h1>Интересные статьи</h1>
			</div>
			<!--
			<div class="journal-item-list">
				
				<div class="journal-item article">
					<span class="item-breadcrumbs">
						<span class="breadcrumbs-b"><a href="/">Дом и семья</a></span>
						<span class="breadcrumbs-title article-item-title">
							<a href="/">Как лучше оформить детскую комнату? Взгляд дилетанта</a>
						</span>
					</span>

					<div class="announce-item-imgcont g-img-cont">
						<a href="./"><img src="./img/content/article-item_big_0.gif" alt="" title="Как лучше оформить детскую комнату? Взгляд дилетанта"></a>
					</div>						

					<div class="joournal-item-info">						

						<p>Все в прошлом: теснота, домашние ссоры из-за ребенка, которому нет места для строительства железной дороги, и ворчание старой бабули. Долгожданные свободные метры: своя спальня, огромная кухня и самое главное – любопытное создание не будет устраиваться между вами в раннее утро. У создания своя отдельная комната.</p>
						
						<div class="article-ico big author male">
							<span><a href="/">Александр Усенко</a></span>
						</div>

						<div class="article-ico big comments">
							<span><a href="/">28</a></span>
						</div>

					</div>
				</div>

				<div class="journal-item article">
					<span class="item-breadcrumbs">
						<span class="breadcrumbs-b"><a href="/">Красота и здоровье</a></span>
						<span class="breadcrumbs-title article-item-title">
							<a href="/">Как бороться с лишним весом? Рекомендации для снижения аппетита</a>
						</span>
					</span>

					<div class="announce-item-imgcont g-img-cont">
						<a href="./"><img src="./img/content/article-item_big_1.gif" alt="" title="Как бороться с лишним весом? Рекомендации для снижения аппетита"></a>
						<div class="footer-more-imgmark"><span>На правах рекламы</span></div>
					</div>					

					<div class="joournal-item-info">						

						<p>Для того чтобы организм не получил сильный стресс, вес необходимо сбрасывать последовательно. При этом нужно взять в свои руки контроль над аппетитом. В нашем организме есть много естественных чувств, и одним из них является – голод. И именно голод часто принуждает нас поглощать калорийные продукты. </p>
						
						<div class="article-ico big author female">
							<span><a href="/">Ирина Лопатухина</a></span>
						</div>

						<div class="article-ico big comments">
							<span><a href="/">256</a></span>
						</div>

					</div>
				</div>

				<div class="journal-item article">
					<span class="item-breadcrumbs">
						<span class="breadcrumbs-b"><a href="/">Дом и семья</a></span>
						<span class="breadcrumbs-title article-item-title">
							<a href="/">Как лучше оформить детскую комнату? Взгляд дилетанта</a>
						</span>
					</span>

					<div class="announce-item-imgcont g-img-cont">
						<a href="./"><img src="./img/content/article-item_big_0.gif" alt="" title="Как лучше оформить детскую комнату? Взгляд дилетанта"></a>
					</div>						

					<div class="joournal-item-info">						

						<p>Все в прошлом: теснота, домашние ссоры из-за ребенка, которому нет места для строительства железной дороги, и ворчание старой бабули. Долгожданные свободные метры: своя спальня, огромная кухня и самое главное – любопытное создание не будет устраиваться между вами в раннее утро. У создания своя отдельная комната.</p>
						
						<div class="article-ico big author male">
							<span><a href="/">Александр Усенко</a></span>
						</div>

						<div class="article-ico big comments">
							<span><a href="/">28</a></span>
						</div>

					</div>
				</div>

				<div class="journal-item article">
					<span class="item-breadcrumbs">
						<span class="breadcrumbs-b"><a href="/">Красота и здоровье</a></span>
						<span class="breadcrumbs-title article-item-title">
							<a href="/">Как бороться с лишним весом? Рекомендации для снижения аппетита</a>
						</span>
					</span>

					<div class="announce-item-imgcont g-img-cont">
						<a href="./"><img src="./img/content/article-item_big_1.gif" alt="" title="Как бороться с лишним весом? Рекомендации для снижения аппетита"></a>
						<div class="footer-more-imgmark"><span>Статья дня</span></div>
					</div>					

					<div class="joournal-item-info">						

						<p>Для того чтобы организм не получил сильный стресс, вес необходимо сбрасывать последовательно. При этом нужно взять в свои руки контроль над аппетитом. В нашем организме есть много естественных чувств, и одним из них является – голод. И именно голод часто принуждает нас поглощать калорийные продукты. </p>
						
						<div class="article-ico big author female">
							<span><a href="/">Ирина Лопатухина</a></span>
						</div>

						<div class="article-ico big comments">
							<span><a href="/">256</a></span>
						</div>

					</div>
				</div>

			</div> 
			-->
			<ul class="articles-list">
			<?php 
				$query = "SELECT * FROM `articles` ORDER BY id ASC";
				$res = mysql_query($query);
				while ($row = mysql_fetch_assoc($res)) 
				{
			?>
				<li class="articles-list-item"><a href="<?=getTemplateLink(array('chpu' => "{$row['chpu']}"), 'articles')?>"><?=$row['title']?></a><li>
			<?php
				} 
			?>
			</ul>
			<!-- Paginator 
			<div class="paginator">
				<ul>
					<li class="prev"><span><a href="./"></a></span></li>

					<li><span><a href="./">1</a></span></li>
					<li><span><a href="./">2</a></span></li>
					<li><span><a href="./">3</a></span></li>
					<li><span><a href="./">4</a></span></li>
					<li class="active"><span><a href="./">5</a></span></li>
					<li><span><a href="./">6</a></span></li>
					<li class="separator"><span>...</span></li>
					<li><span><a href="./">25</a></span></li>
					<li><span><a href="./">26</a></span></li>
					<li><span><a href="./">27</a></span></li>

					<li class="next"><span><a href="./"></a></span></li>
				</ul>
			</div>-->

		</div>

		<div class="clearfix"></div>
	</div>


<script type="text/javascript">
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

VK.Widgets.Group("vk_groups", {mode: 0, width: "239", height: "390"}, 20003922);

$('select').selectBox();
</script>

</html>