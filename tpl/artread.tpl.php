





	<!-- Main Container -->
	<div class="main-cont">
		<!-- Sidebar -->
			<?php include('block_left_menu.tpl.php'); ?>
		
		<!-- Content -->
		<div class="content">
			<? /*
			<div class="journal-article-tag-title">
				<span>В рубрике<a href="/">Психология</a></span>
			</div>
			
			<div class="journal-article-info">
				<ul>
					<li class="article-info-item-list author-name female"><span><a href="/">Юлия орлова</a></span></li>
					<li class="article-info-item-list date-public"><span>Опубликовано: <span>20.03.2013</span></span></li>
					<li class="article-info-item-list comments-count"><span><a href="/">179</a></span></li>
					<li class="article-info-item-list views-count"><span>12347</span></li>
					<li class="article-info-item-list toprint"><span><a href="/">Распечатать</a></span></li>
				</ul>
			</div> 

			<div class="journal-article">
				<div class="journal-article-title">
					<h1>Как улучшить качество сна?</h1>
				</div>

				<img src="./img/content/article-img_0.jpg" alt="" title="Как улучшить качество сна?">

				<p class="journal-article-heading">О том, как важен хороший сон, очень хорошо знает тот, кто часто страдает его отсутствием. Таким людям легкое утреннее пробуждение, бодрость и хорошее настроение разве что только снится. Если это про вас – не все потеряно. Соблюдение нескольких рекомендаций поможет исправить ситуацию. Проблемы со сном часто сопутствуют любителям кофе и крепкого чая.</p>

				<span class="article-heading-title">Постельный режим</span>
				<p class="jounal-article-text">Для того чтобы засыпать быстро, а не крутиться в кровати до утра, старайтесь ложиться спать в одно и то же время. Самый <a href="/">хороший сон</a> – это тот, что начинается до полуночи. Если вы закоренелая «сова», то и вам переход в режим «жаворонка» пойдет на пользу. Помните, что для полноценного отдыха человеку нужно 6-8 часов сна. Начните ложиться и вставать на полчаса раньше, чем прежде, с каждым днем приближаясь к намеченному времени, и через пару недель вы увидите, что перемены понравились вашему организму.</p>

				<span class="article-heading-title">Прощай, кофеин</span>
				<p class="jounal-article-text">Проблемы со сном часто сопутствуют любителям кофе и крепкого чая. Дело в том, что кофеин вызывает состояние, очень похожее на состояние тревоги. Если у вас проблемы со сном, то лучше вообще отказаться от употребления содержащих его напитков. Чтобы лучше спалось, можно выпить стакан теплого молока с медом, экстракт пустырника или валерианы.</p>

				<span class="article-heading-title">Сны</span>
				<p class="jounal-article-text">Нашими врагами могут стать и наши сновидения. Кошмары, неприятные сюжеты из жизни, запутанные лабиринты событий – все это вызывает изнеможение по утрам. Чтобы избавиться от навязчивых снов, попробуйте понять их. Проанализируйте сюжеты, обратите особенное внимание на те из них, которые неоднократно повторяются. Не забывайте, что таким образом в наше сознание прорывается голос бессознательного. С какими событиями и людьми из реальной жизни ассоциируются у вас ваши сны? На какую проблему они пытаются обратить ваше внимание? Как только вы поймете зашифрованное послание, ваши сны перестанут вас беспокоить.</p>

				<span class="article-heading-title">Свобода от навязчивых мыслей</span>
				<p class="jounal-article-text">Часто нам не дают заснуть вполне реальные проблемы. Что ж, лучшим способом <a href="/">избавиться от бессонницы</a> в этом случае будет решение этих проблем. Если это не представляется возможным сделать сию минуту, дайте себе мысленное указание в стиле «Унесенных ветром»: «Подумаю об этом завтра». И постарайтесь заменить тяжелые думы приятными мыслями и мечтами.</p>
			</div>

			<div class="journal-article-share">
				<div class="share">
					<span>Поделиться:</span>

					<a href="/"><img src="./img/journal-share_ok.png" alt=""></a>
					<a href="/"><img src="./img/journal-share_f.png" alt=""></a>
					<a href="/"><img src="./img/journal-share_vk.png" alt=""></a>
					<a href="/"><img src="./img/journal-share_tw.png" alt=""></a>

					<div class="share-code">
						<span><a href="/">Кад для вставки в блог или форум</a></span>
					</div>
				</div>
			</div> */?>
			<div class="journal-article">
				<?$txt = htmlspecialchars_decode($article['text']);?>
				<?=$txt?>
			</div>
			


		</div>

		<div class="clearfix"></div>
	</div>


	





<script type="text/javascript">
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

VK.Widgets.Group("vk_groups", {mode: 0, width: "239", height: "390"}, 20003922);
</script>

</html>