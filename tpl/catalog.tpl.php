
<script>
    $(document).ready(function(){
    	$('.mm-tooltip').fadeOut(5000);
        $("#range_1").ionRangeSlider({
            min: 0,
            max: 999999,
            from: 1000,
            to: 600000,
            type: 'double',
            step: 1,
            postfix: "р.",
            prettify: false,
            hasGrid: false
        });
        $(".mm-one-click").click(function(){
        	$(".fader").show();
        	$(".fader").width("100%");
        	$(".fader").height("100%");
        	$("#popup2").animate({}, function(){
        		$("#popup2").show();
        	});
        	return false;
        });
        $("#popup2 .close").click(function(){
        	$(".fader").hide();
        	$("#popup2").hide();
        });
    });
$(document).ready(function() {
	$(".image.featured").fancybox();
	$('.mm-popup-wrap a').click(function() { 
		$('.mm-tooltip').fadeOut(5000);
	});
	$('.mm-submit').click(function() {});
});


</script>

		<div class="mm-intro-type slim_box">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList" class="mm-breadcrumbs">
					<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="/"><span itemprop="name" >Каталог</span><meta itemprop="position" content="1"></a>
					</li>
					<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="<?= getTemplateLink($product['cat_info'],'cat'); ?>"><span itemprop="name" ><?=$product['cat_info']['title']?></span><meta itemprop="position" content="2"></a>
					</li>
					<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem" ><span itemprop="item"><?=$product['title']?></span><meta itemprop="position" content="3"></li>
				</ul>
			</div>


			<section class="mm-main slim_box">
				<div class="mm-main-holder">
					<div class="mm-item-holder">
					<div itemscope itemtype="http://schema.org/Product">
						<h1 itemprop="name"><?=$product['title'];?></h1>
						<div class="mm-tab-content">
							<div class="microdata" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
								<div class="microdata">
	                        		<span itemprop="ratingValue">
	                        		<?=$productrating?></span> из 
	                        		<span itemprop="bestRating">5</span>
	                        		<span itemprop="ratingCount"><?=count($feedback)?></span>
	                        	</div>
	                        </div>
							<div class="mm-stick"></div>
								<div id="tabs-1" style="line-height: 685px;">
								<a href="<?=getImageWebPath('product_big').$product['id']?>.jpg" itemprop="image"  rel="group" class="image featured" style="display: inline-block;">	<img src="<?=getImageWebPath('product_big').$product['id']?>.jpg" alt="image description"  style="max-width: 100%;
	vertical-align: middle; z-index: 1;
	position: absolute;
	margin: auto;
	top: 0px;
	bottom: 0px;
	right: 0px;
	left: 0px;"  /> </a>
								</div>
								<?
									$num=1;
									foreach($product['extra_photo'] as $id){
										$num++;
								?>
										<div id="tabs-<?=$num?>">
										<a itemprop="image" href="<?=getImageWebPath('product_extra_original').$id?>.jpg" rel="group" class="image featured">	<img src="<?=getImageWebPath('product_extra_original').$id;?>.jpg" alt="image description" width="409" style="width: 100%;
	vertical-align: middle;z-index: 1;
	position: absolute;
	margin: auto;
	top: 0px;
	bottom: 0px;
	right: 0px;
	left: 0px;"  /> </a>
										</div>
								<?
									}
								?>
							</div>
							<div class="gallery-holder carousel2">
								<div class="gallery">
									<div class="gholder">
										<a class="btn-prev icon-back" href="#"></a>
										<div class="gmask-center">
											<div class="gmask">
												<div class="slideset tabset">
													<div class="slide">
														<a href="#tabs-1" class="active" style="position:relative;"><img src="<?=getImageWebPath('product_small').$product['id']?>.jpg" alt="image description" style="max-width: 80%;padding-left:7px;
	max-height:116px; z-index: 1;
	position: absolute;
	margin: auto;
	top: 0px;
	bottom: 0px;
	right: 0px;
	left: 0px;"/></a>
													</div>
													<?
														$num=1;
														foreach($product['extra_photo'] as $id){
															$num++;
													?>
															<div class="slide">
																<a href="#tabs-<?=$num?>" style="position:relative;"><img src="<?=getImageWebPath('product_extra_preview').$id?>.jpg" alt="image description" style="max-width: 80%;padding-left:7px;
	max-height:116px;z-index: 1;
	position: absolute;
	margin: auto;
	top: 0px;
	bottom: 0px;
	right: 0px;
	left: 0px;" /></a>
															</div>
													<?
														}
													?>
												</div>
											</div>
										</div>
										<a class="btn-next icon-back" href="#"></a>
									</div>
								</div>
							</div>
							<div class="mm-text-box">		
								
							<div class="hide-des-for-yandex" style="display:none;" itemprop="description"><?=$product['des']?></div>
								<div itemprop="offers" itemscope itemtype="http://schema.org/Offer"> 
									<div class="mm-price-holder">
										<strong class="mm-price"><span ><?=moneyFormat($product['price_after_discount'])?></span> руб. <span style="display:none;" itemprop="priceCurrency">RUB</span></strong>
										<span style="display:none;" itemprop="price" content="<?=$product['price_after_discount']?>"></span>
													<div class="catalog-cart">
														<div class="mm-popup-wrap">
															<a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>" class="mm-cart icon-cart item-buy buy-it-listing" data-product_id='<?=$product['id']?>' data-product-price='<?=$product['price_after_discount']?>'>В корзину</a>
															<!--<div class="mm-tooltip">
																<span>Товар добавлен в корзину!</span>
																<a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>" onclick="yaCounter28334251.reachGoal('NORMAL_ORDER');">Оформить</a>
															</div>-->
														</div>
													</div>
								</div>
								<?php $amount = getAmount($product['id']); ?>
								<div class="mm-status-holder">
									<div class="over_block">
										<a href="#popup2" class="one-click btn-request mm-one-click but_default" onclick="yaCounter28334251.reachGoal('FAST_ORDER'); _gaq.push(['_trackEvent', 'button', 'FAST_ORDER']); return true;" data-id='<?=$product['id']?>' data-title='<?=$product['title']?>' data-chpu='<?=$product['chpu']?>' data-price='<?=moneyFormat($product['price_after_discount'])?>'>Быстрый заказ</a>
										<a class="mm-kredit btn-request but_default"  href="#popup2" data-id='<?=$product['id']?>' data-title='<?=$product['title']?>' data-chpu='<?=$product['chpu']?>' data-price='<?=moneyFormat($product['price_after_discount'])?>' >Купить в кредит</a>
									</div>
									<!--<div>
										<?php if ( $product['rests_main']['available'] ) :?>
											<div class="amount-info">
												<div class="icon"></div>
												<div class="icon-text">В наличии</div>
											</div>
										<?php endif; ?>
									</div>-->
									<div class="over_block">
										<? // получаем доставку по Москве, до этого было статикой "Бесплатная доставка по Москве"
										$delivery_cost = ($product['deliveryPrice']) ? $product['deliveryPrice']." руб." : "<span class='free_text'>бесплатно</span>" ; ?>
											 
										<div class="delivery-info">	
											<div class="icon-text">Доставка по Москве - <?=$delivery_cost?></div>
										</div>
									</div>
									
									<div class="mm-soc-docs over_block">
										<div class="icon"></div><div > Документы соц компенсации (по запросу)</div>
									</div>
									<div class="mm-opt over_block">
										<div class="icon"></div><a href="/page/optovikam.html">Спецпредложение для оптовиков</a>
										<?php if ($product['warranty'] > 0): ?>
										<?php $warranty = getWarrantyString($product['warranty']);?>
										<div class="mm-guarantee " >
											<div class="icon"></div><div >Гарантия : <span><?=$warranty?></span> </div>
										</div>	
										<?php endif; ?>
									</div>
									<a href="#popup" class="mm-price-list one-click but_default" onclick="yaCounter28334251.reachGoal('CALLBACK_BTN');_gaq.push(['_trackEvent', 'button', 'CALLBACK_BTN']);">Заказать звонок</a>
									<!--<div class="mm-payments over_block">
										<div class="icon"></div><div >Оплата: наличными, банковскими картами, в кредит, безналичные переводы</div>
									</div>-->	
									
								
								</div>

								<? /*
								<ul class="mm-info-list">
								<?php $under_price_des = explode('</li>',$product['like_desc']);
								$i = 0;
										foreach ($under_price_des as $upd) {
											if ($i <= 5) {
									echo $upd . '</li>';
								
								}
									$i++;
								}
								?>
								</ul>
								*/ ?>
								</div>
							</div>
						</div>
					</div>
					<ul class="tabset2">
		
						<li><a href="#tab-7" class="active">Технические характеристики</a></li>

						<!-- <li><a href="#tab-8">Описание</a></li> -->
						
						<li><a href="#tab-9">Оплата и Доставка</a></li>
						<li><a href="#tab-10">Возврат</a></li> 
						<li><a href="#tab-11">Отзывы (<?=count($feedback)?>)</a></li>
						<?php if (strlen($product['instruction']) > 2): ?>
						<li><a href="#tab-12">Инструкция</a></li>
						<?php endif ?>
					</ul>
					<div class="mm-char-tab" id="tab-7">
						<h2>Описание:</h2>
						<div><?=$product['under_price_des']?>
						</div>
						
						<?php if(!empty($product['iframe'])) { ?>
							<h2>Видео:</h2>
							<div id="video_cont"><?=$product['iframe']?></div>
						<?php } ?>
						
						<div class="two-column-text">
							
								<div class="tech-characters">
									<div class="mm-title-holder">
										<h2>Технические характеристики:</h2>			
									</div>
                                    <?php /*
									<ul class="mm-info">
										<?=$product['like_desc']?>
									</ul>
                                     */ ?>
                                    <?=productInfoRestruction($product['like_desc']) ?>
								</div>
								<div class="advantages" >
									<div class="mm-title-holder">
										<?php if (!empty($product['adv_des'])) {?>
											<h2>Комплектация:</h2>
										<?php } ?>
									</div>
									<ul class="mm-info">
										<?=$product['adv_des']?>
									</ul>
								</div>

								<div class="tech-description">
									<?=$product['des']?>
								</div>					
						</div>
					</div>
					<!--
					<div class="mm-char-tab" id="tab-8">
						
						<!-- <div class="mm-brand-left">
							<img src="/theme/img/mm-img09.png" alt="image description" width="193" height="48" />
						</div>
						<p>На сегодняшний день наша компания предлагает населению и корпоративным клиентам низкие цены и широкий ассортимент медицинской техники, ортопедических изделий, средств ухода за больными и реабилитационного оборудования отечественных и зарубежных производителей. Медтехника Москва - это прямой поставщик товаров медицинского назначения и оборудования для реабилитации китайского производства. </p>
						<p>На сегодняшний день наша компания предлагает населению и корпоративным клиентам низкие цены и широкий ассортимент медицинской техники, ортопедических изделий, средств ухода за больными и реабилитационного оборудования отечественных и зарубежных производителей. Медтехника Москва - это прямой поставщик товаров медицинского назначения и оборудования для реабилитации китайского производства. Наши специалисты следят за развитием производства медицинского и реабилитационного оборудования в Китае, посещяют крупнейшие фабрики и заводы. Результатом этого является то, что Медтехника Москва представляет отечественному покупателю качественное оборудование по приемлемым ценам. </p> 
					</div>
					-->
			
					<!-- Для таба "Доставка и оплата" берем текст с аналогичной страницы на сайте -->
					<?php $page_delivery = getStaticPage('dostavka-i-oplata',true); ?>

					<div class="mm-char-tab" id="tab-9">
			<!-- 			<div class="mm-title-holder">
				<h2>Оплата и Доставка:</h2>
			</div> -->
						<?= $page_delivery['des']; ?>
						</div>
					
						<div class="mm-char-tab" id="tab-10">
						<?php $page_return = getStaticPage('obmen-i-vozvrat-tovarov', true); ?>
						<?= $page_return['des']; ?>
						</div>


						<ul class="mm-refund">
						</ul>


                    <?php //отзывы ?>
                    <div class="mm-char-tab" id="tab-11">
                        <div class="mm-feedbacks-cont">
                        	<?php
                            if ($productrating > 3):?>
                            <div class="feedback-container fbrating">
	                            <div title="Средний рейтинг : <?=$productrating?> из 5" class="name-date-feed">
	                            	<span class="avaragerating">Рейтинг : </span>
	                                <?php for($ind = 1; $ind<=5; $ind++): ?>
	                                    <?php if($ind <= $productrating): ?>
	                                        <span class="glyphicon glyphicon-star"></span>
	                                    <?php else: ?>
	                                        <span class="glyphicon glyphicon-star-empty"></span>
	                                    <?php endif ?>
	                                <?php endfor ?>
	                            </div>
	                        </div>
                            <?php
                        	endif;
                            foreach ($feedback as $feed) { ?>
                                <div class="feedback-container">
                                    <div class="name-date-feed">
                                        <span class="name"><?=$feed['name']?></span>
                                        <span class="date"><?=date('d.m.Y', $feed['date'])?></span>
                                    </div>
                                    <div class="feedback-question">
                                        <?=$feed['comment']?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <form action="" class="mm-feedback-form" method="post">
                            <fieldset>
                                <div class="form-check form-check-inline">
                                    Ваша оценка <input type="text" name="rating" class="input-rating rating-loading"  data-size="xs" required>
                                </div>
                                <div class="mm-row">
                                    <div class="mm-text-wrap">
                                        <input type="text" name="name"   class="mm-text"  placeholder="Ваше имя:" REQUIRED/>
                                        <input type="hidden" name = "id" value="<?=$product['id']?>">
                                    </div>
                                </div>
                                <div class="mm-row">
                                    <div class="mm-textarea">
                                        <textarea name="comment" cols="1" rows="1"  placeholder="Ваш отзыв:" required></textarea>
                                    </div>
                                </div>
                                <div class="g-recaptcha" data-sitekey="6LdbrzQUAAAAAJcNmKvox2cQB3ooOr9tgOXxHWge"></div>
                                <input type="submit" class="but_default mm-submit" value="Отправить" />
                            </fieldset>
                        </form>
                        <div class="clearfix"></div>
                    </div>
					<?php if (strlen($product['instruction']) > 2): ?>
                    <div class="mm-char-tab" id="tab-12">
                    	 <div class="mm-title-holder">
                            <h3>Инструкция : "<?=$product['title'];?>"</h3>
                        </div>
                        <div>
                            <a class="instructionbtn" href="<?=getInstructionPath($product['id']) . urldecode($product['instruction'])?>" target="_blank" download >Скачать</a>
                        </div>
                        <div class = "pdficon"></div>
                    </div>
					<?php endif ?>
				</div>
		



				<?php
				$currProd = $product;
				?>

                <div class="hotline_banner"></div>

                <?php if (count($currProd['related_products'])>0): ?>
                    <div class="mm-content-box mm-content-box2 analog carusel_box">
                        <div class=" analog-prod">
                            <h2>Сопутствующие товары:</h2>
                        </div>
                        <div class="gallery-holder mm-slideshow2 ">
                            <div class="gallery">
                                <a class="btn-prev" href="#">&lt;</a>
                                <a class="btn-next" href="#">&gt;</a>
                                <div class="gmask">
                                    <div class="slideset">
                                        <div class="slide">
                                            <?php
                                            $ne = 1;
                                            foreach ($currProd['related_products'] as $product) :

                                                include('element_product_recomm_list.tpl.php');

                                                $ne++;
                                                if ($ne>4){
                                                    $ne=1;
                                                    print '</div><div class="slide">';
                                                }
                                            endforeach
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

				<?php if (count($currProd['recomm'])>0){?>
					<div class="mm-content-box mm-content-box2 analog carusel_box">
						<div class=" analog-prod">
							<h2>Рекомендуем с этим товаром:</h2>
						</div>
						<div class="gallery-holder mm-slideshow2 ">
							<div class="gallery">
								<a class="btn-prev" href="#">&lt;</a>
								<a class="btn-next" href="#">&gt;</a>
								<div class="gmask">
									<div class="slideset">
										<div class="slide">
                                            <?php
                                            $ne = 1;
                                            foreach ($currProd['recomm'] as $product) :

                                                include('element_product_recomm_list.tpl.php');

                                                $ne++;
                                                if ($ne>4){
                                                    $ne=1;
                                                    print '</div><div class="slide">';
                                                }
                                            endforeach
                                            ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php }?>
			</section>