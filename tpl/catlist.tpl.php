<?php

?>

<script type="text/javascript">
	var price = {
		"min" : <?php echo isset($data['price_range']['min']) ? $data['price_range']['min'] : 0 ?>,
		"max" : <?php echo ($data['price_range']['max'] > 0)? $data['price_range']['max'] : 1?>
		};
	var defaultPage = <?php echo (isset($_GET['page']) && $_GET['page'] == 'all') ? "'all'" : ((isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1); ?>;
	var defaultSort = '<?php echo isset($_GET['sort']) ? $_GET['sort'] : 'none'; ?>';


	$(function(){
		$("#range_1").ionRangeSlider({
            min: price.min,
            max: price.max,
            from: price.min,
            to: price.max,
            type: 'double',
            step: 1,
            postfix: "р.",
            prettify: false,
            hasGrid: false,
           	hideMinMax: true
        });
	});


</script>

<!-- Костыль для фильтра -->
<?php if ($data['price_range']['min'] != 0) { ?>
<style>
	.irs-min{
		left: -34px !important;
	}
</style>
<?php } ?>
<!-- <?= ($data['catInfo']['id']) ? 'style="font-weight: 700; color:#ca2032;"' : '' ; ?>><?=$data['catInfo']['title']?> -->
			<div class="slim_box mm-intro-type">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList" class="mm-breadcrumbs">
					<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
						<a itemprop="item" href="/"><span itemprop="name" >Главная</span></a>
					</li>
					<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem" >
						<a itemprop="item" href="/cat/<?=$data['catInfo']['chpu']?>.html" <?= (!$data['tagInfo']['id']) ? 'class="active"' : '' ; ?>><span itemprop="name" ><?=$data['catInfo']['title']?></span></a>
					</li>
					<? if(!empty($data['tagInfo']['title'])){ ?>
					<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem" class="active">
						<span itemprop="item"><?=$data['tagInfo']['title']?></span>
					</li>
					<? } ?>
				</ul>
			</div>
			
			<section class="slim_box mm-main">
				<div class="mm-main-holder">
					<div class="flex_title">
						<div class="mm-title-holder">
							<h1><?=$tag['title'] ? $tag['title']:$data['catInfo']['title'].":"?></h1>
						</div>
						<div id="cat-hidden">
							    <div class="sub-categories-cont">
                                    <ul class="mm-sub-categories">
                                        <? #var_dump($data['catInfo']['id']);

                                        $tags = getTagsInCat($data['catInfo']['id']);
                                        foreach($tags as $tag){
                                            ?>
                                            <li><a href="<?=getTemplateLink($tag, 'tag');?>" class="<?=($tag['chpu'] == $_GET['c'])?"active_tag":"" ?>"><?=$tag['short']?></a></li>
                                        <?}?>
                                    </ul>
                                </div>
                            <div class="show-more">&uarr; Показать еще &uarr;</div>
						</div>	
					</div>
					<!--<div class="eshe">ЕЩЕ &dArr;</div>-->
					<?php
					//action="/ajax/products.php?action=cat"
					?>
					<form method='post' action="/ajax/products.php?action=cat" class="mm-filter" id="filter-form">
						<input type="hidden" name="cat" value="<?=$data['catInfo']['id']?>">
						<input type="hidden" name="paginator_type" value="uniq">

							<div class="mm-row">
								<div class="mm-sort">
									<!--<div class="mm-sort-title">
										<strong>Сортировка:</strong><span>&nbsp;</span>
									</div>-->
									<ul class="mm-sort-list">
										<li><a  <?if ($_GET['sort']==''){print'class="active"';}?> href="?sort=" id="price-desc">По популярности</a></li>
										<li><a <?if ($_GET['sort']=='price_asc'){print'class="active"';}?> href="?sort=price_asc" id="price-asc">По минимальной цене</a></li>
										<li><a  <?if ($_GET['sort']=='price_desc'){print'class="active"';}?> href="?sort=price_desc" id="price-desc">По максимальной цене</a></li>
										
									</ul>
								</div>
								<div class="mm-price-range">
									<!--<div class="mm-sort-title">
										<strong>Диапазон Цен:</strong><span>&nbsp;</span>
									</div>-->
									<div class="mm-range">
										<!--<label for="min">от:</label>-->
										<input readonly type="text" class="mm-text default" id="min" name='min_price' value="<?=$data['price_range']['min']?> р." />
										<label for="max"> — </label>
										<input readonly type="text" class="mm-text default" id="max" name='max_price' value="<?=$data['price_range']['max']?> р." />
										<input type="text" id="range_1" /> 
									</div>
								</div>
							</div>
							<!--<div class="mm-slide">
								<div class="mm-slide-holder">
									<div class="mm-column">
										<div class="mm-sort-title">
											<strong>Диапазон Цен:</strong><span>&nbsp;</span>
										</div>
										<select title="-- Не выбрано --">
											<option>100 (4)</option>
											<option>120 (3)</option>
											<option>140 (2)</option>
											<option>160 (1)</option>
										</select>
									</div>
									<div class="mm-column">
										<div class="mm-sort-title">
											<strong>Вес:</strong><span>&nbsp;</span>
										</div>
										<select title="-- Не выбрано --">
											<option>100 (4)</option>
											<option>120 (3)</option>
											<option>140 (2)</option>
											<option>160 (1)</option>
										</select>
									</div>
									<div class="mm-column">
										<div class="mm-sort-title">
											<strong>Нагрузка:</strong><span>&nbsp;</span>
										</div>
										<select title="-- Не выбрано --">
											<option>100 (4)</option>
											<option>120 (3)</option>
											<option>140 (2)</option>
											<option>160 (1)</option>
										</select>
									</div>
								</div>
							</div>-->
							<?php  if ($product['params_available']) {	?>
							<a href="#" class="mm-open">показать/скрыть</a>
							<?php } ?>
						
					</form>
					
					<div id='products-list-and-paginator'>
						<ul class="mm-catalog">
							<?
								foreach ($data['products'] as $product) {
									if ($product['is_hide'] != 1) {
										include('element_product_list.tpl.php');
									}
								}
							?>
						</ul>
						<div class="pagin">
							<?echo UniqGeneratePaginator($data['page'], new queryString(array('action' => 'cat')), 'reloadProducts([page])'); ?>
						</div>
					</div>
					
				</div>
                <div class="hotline_banner"></div>
				<div class="mm-twocolumn">
					<!-- <?php include_once("right_block.tpl.php");?> -->
					<section class="mm-content">
						<div class="mm-content-box">
<!-- 							<div class="mm-title-holder">
	<h2><?=$data['catInfo']['title']?>:</h2>
</div> -->
							<div class="mm-text-box static_page des_section">
								<?=$seo_text?>
							</div>
						</div>
					</section>
				</div>
			</section>

<script type="text/javascript">
	$('#sort-filter').bind('change', function() {
		var uri = $(this).find('option:checked').data('uri');
		window.location = uri;
	});

	// $(document).ready(function() {
		// SliderInit({
			// Slides:'.footer-more-item',
			// SlidesContainer:'#slides-container',
			// SliderLeft:'#slider-left',
			// SliderRight:'#slider-right',
			// SlidesOnScreen:3
		// });
	// });
	
	//
	//
	
	$('.filter-more-manufacturer input').bind('change', function(){
		reloadProducts();
	});
	
	$( document ).on( "click", ".filter-more-manufacturer span.checkbox", function() {
	  $(this).next('input').trigger('change');
	});
	// Sliders
	// $('#slider-price').slider({
		// range: true,
		// min: <?=$data['price_range']['min'];?>,
		// max: <?=$data['price_range']['max'];?>,
		// values: [ <?=$data['price_range']['min'];?>, <?=$data['price_range']['max'];?> ],
		// slide: function( event, ui ) {
			// $( '#price-range-from' ).val( ui.values[ 0 ] );
			// $( '#price-range-to' ).val( ui.values[ 1 ] );
		// },
		// change : function () {
			// //alert('asd');
			// reloadProducts();
		// }
	// });

	$('.filter-more-slider').each(function(index, element) {
		var _this = $(element);
		var sliderBody = _this.find('.slider-body');
		
		var dataField = _this.find('.slider-field');
		var minValue = dataField.data('min');
		var maxValue = dataField.data('max');
		var paramId = dataField.data('param_id');
		var unit = dataField.data('unit');
		
		// sliderBody.slider({
			// range: true,
			// min: minValue,
			// max: maxValue,
			// step: 1,
			// values: [minValue, maxValue],
			// slide : function(event, ui){
				// sliderBody.find(".fms3").text(ui.values[0] + ' ' + unit);
				// sliderBody.find(".fms4").text(ui.values[1] + ' ' + unit);
			// },
			// change : function(event, ui) {
				// dataField.empty();
				// dataField.append(
					// $('<input>').attr({
						// "type" : "hidden",
						// "name" : "filter_range[" + paramId + "][min]",
						// "value" : ui.values[0]
					// })
				// );
				// dataField.append(
					// $('<input>').attr({
						// "type" : "hidden",
						// "name" : "filter_range[" + paramId + "][max]",
						// "value" : ui.values[1]
					// })
				// );
				// reloadProducts();
				
			// }
		// });
		
		sliderBody.find('.ui-slider-range').html('<div class="fms-weight"><span class="fms3">' + minValue +' ' + unit + '</span><span class="fms4">' +maxValue  +' ' + unit + '</span></div>');
		handle = sliderBody.find('A.ui-slider-handle'); 
		handle.eq(0).addClass('first-handle');   
		handle.eq(1).addClass('second-handle');   
		handle.eq(2).addClass('first-handle');
		handle.eq(3).addClass('second-handle');
		
		
		//sliderBody.find('.ui-slider-range').append('<input type="text" class="weight-min" disabled /><input type="text" class="weight-max" disabled />');
		// sliderBody.find('.weight-min').val(sliderBody.slider('values', 0));
		// sliderBody.find('.weight-max').val(sliderBody.slider('values', 1));
		
	});
	//
	
	
	// $( '#price-range-from' ).val( $( '#slider-price' ).slider( 'values', 0 ) );
	// $( '#price-range-to' ).val( $( '#slider-price' ).slider( 'values', 1 ) );
	$('#price-range-from').change(function(event) {
		var data = $('#price-range-from').val();
		// $('#slider-price').slider('values', 0, data);
	});

	$('#price-range-to').change(function(event) {
		var data = $('#price-range-to').val();
		// $('#slider-price').('values', 1, data);
		// console.log(data);
	});

	handle = $('#slider-price A.ui-slider-handle, #slider-weight A.ui-slider-handle'); 
	handle.eq(0).addClass('first-handle');   
	handle.eq(1).addClass('second-handle');   
	handle.eq(2).addClass('first-handle');
	handle.eq(3).addClass('second-handle');

	
	// default
	// $('select').selectBox();
	
</script>