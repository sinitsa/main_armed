<div class="main-cont">	
	<!-- Sidebar -->
	<?php include('block_left_menu.tpl.php'); ?>

	<div class="content order-info">
		<h1>Ваш заказ принят</h1>	
		<div class="urorder-info">
			<p>Здравствуйте, <span class="name"><?=$orderInfo['details']['name'];?></span>
			Вашему заказу присвоен номер <span class="order-number"><?=$orderInfo['items']['orderCode'];?></span>
			Наш менеджер перезвонит Вам на номер <span class="phone"><?=$orderInfo['details']['phone'];?></span> в течение нескольких часов (в рабочее время) для согласования времени доставки и уточнения деталей комплектации заказа. Пожалуйста, не отключайте телефон.</p>
		</div>

		<div class="order-urorder">
			<span class="title">Вы заказали:</span>

			<div class="ordered-items">
			<? foreach($orderInfo['items']['productsToResponse'] as $product) {?>
				<div class="order-urorder-item">
					<span class="uroder-item-name"><a href="/"><?=$product['title'];?></a></span>
					<span class="urorder-item-price"><?=($product['price_after_discount'] * $product['amount']);?><span> руб.</span></span>
				</div>
			<?}?>		
			</div>
		</div>
			
		<div class="uroder-toget-info">
			<span class="toget-info-price">Сумма: <span><b class="sum"><?=$orderInfo['items']['price']['price_after_global_discount'];?></b> <span> руб.</span></span></span>
			<!--<span>Способ оплаты: <span>банковский кредит</span></span>
			<span>Способ доставки: <span>PickPoint - г. Санкт-Петербург (ул. Александра Невского, 27)</span></span>-->
			<span class="toget-info-thanks">Спасибо за Ваш заказ! Интернет-магазин мед-техники Сердце.РУ.</span>
		</div>


		<form class="credit-btn-form" action="">

			<div id="credit_btn">

			</div>

		</form>
		<!--
		<div class="credit-exit-btn">
			<div class="btn-tpl big green">
				<span><a href="/">Вернуться в каталог товаров</a></span>
			</div>
		</div> -->
		<div class="btn-tpl mid green" style="margin-top: 15px;">

    <span>
        <a id="exit-btn" href="/pay/credit.html">

            Вернуться в каталог товаров

        </a>
		</span>

</div>
		<div class="clearfix"></div>
		
	</div>
	<div class="clearfix"></div>
</div>	
<script> 
	vkredit = new VkreditWidget(
		1, /*цвет кнопки*/
		<?=$orderInfo['items']['price']['price_after_global_discount'];?>, /*запрашиваемая сумма - служит для расчета суммы, отображаемой на кнопке.*/
		{
			order: "<?php echo $base64; ?>",
			sig: "<?php echo $sig; ?>"
		}
	);
	vkredit.insertButton("credit_btn"); 
	$(window).load(function(){
		vkredit.openWidget(); 
	});

</script>