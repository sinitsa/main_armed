<?
$discount = '';
if ($product['discount_value'] > 0) {
    if ($product['discount_type'] == 'percent') {
	$discount = '-' . $product['discount_value'] . '%';
    } elseif ($product['discount_type'] == 'value') {
	$discount = '-' . getPercentForDiscount($product['price'], $product['discount_value']) . '%';
    }
}

$product['deliveryPrice'] = deliveryPrice::getByCatAndPrice($product['cat'], $product['price_after_discount'], $product['id']);
	
?>
						<li>
							<div class="mm-prod-box <?if($product['novinka']==1){print 'mm-new';}?>">
                                <?php if ($product['best']==1): ?>
                                <div class="best-icon"></div>
                                <?php endif ?>
								<div class="mm-holder">
									<div class="image">
										<a href="<?=getTemplateLink($product, 'catalog');?>" class="mm-title"><img src="<?=getImageWebPath('product_medium').$product['id']; ?>.jpg" alt="image description" width="188" style="max-height: 175px;" /></a>
									</div>
									<a href="<?=getTemplateLink($product, 'catalog');?>" class="mm-title product_title"><?=$product['title']?></a>
									<? /*
									<a href="#popup2" class="one-click btn-request" data-id='<?=$product['id']?>' data-title='<?=$product['title']?>' data-chpu='<?=$product['chpu']?>' data-price='<?=moneyFormat($product['price_after_discount'])?>'>заказ в 1 клик</a>
									*/ ?>
									<div class="mm-catalog-info-icons">
										<?php if ( isset($product['rests_main']['available']) && $product['rests_main']['available'] || getAmount($product['id']) > 0 ) :?>
										<div class="amount-info icon-check">
											<!--<div class="icon"></div>-->
											<div class="icon-text">В наличии</div>
										</div>
										<?php endif; ?>
										<?php 
										$delivery = deliveryPrice::getByCatAndPrice($product['cat'], $product['price'], $product['id']);
										if ( $delivery == 0 ) :
										 ?>
										<div class="delivery-info icon-truck">	
											<!--<div class="icon"></div>-->
											<div class="icon-text">Бесп. доставка по Мск</div>
										</div>
										<?php endif; ?>
									
									</div>
									<div class="mm-price-holder">
										<div class="mm-popup-wrap">
											<a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>" class="mm-cart item-buy buy-it-listing icon-cart" data-product_id='<?=$product['id']?>' data-product-price='<?=$product['price_after_discount']?>'></a>
											<!--<div class="mm-tooltip">
												<span>Товар добавлен в корзину!</span>
												<a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>">Оформить</a>
											</div>-->
										</div>
										<strong class="mm-price"><?=moneyFormat($product['price_after_discount'])?> р.</strong>
									</div>
									
									<!--a href="<?=getTemplateLink($product,'catalog')?>" class="mm-title"><?=$product['title']?></a-->
									<div class="mm-char">
									 <?=$product['short_des']?> 
									</div>
								</div>
								<div class="stick"></div>
							</div>
						</li>