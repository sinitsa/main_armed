<div class="mm-prod-box <?if($product['novinka']==1){print 'mm-new';}elseif($product['best']==1){print 'mm-hit';}?>">
    <div class="mm-holder">
        <div class="image">
            <a href="<?=getTemplateLink($product,'catalog')?>"><img src="<?=getImageWebPath('product_medium').$product['id']; ?>.jpg" alt="<?=$product['title']?>" width="188" height="169" /></a>
        </div>
		<a href="<?=getTemplateLink($product,'catalog')?>" class="mm-title product_title"><?=$product['title']?></a>
        <div class="mm-price-holder">
            <div class="mm-popup-wrap">
                <a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>" class="icon-cart mm-cart item-buy buy-it-listing" data-product_id='<?=$product['id']?>' data-product-price='<?=$product['price_after_discount']?>'></a>																
				<!--<div class="mm-tooltip">
                    <span>Товар добавлен в корзину!</span>
                    <a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>">Оформить</a>
                </div>-->
            </div>
            <strong class="mm-price"><?=moneyFormat($product['price_after_discount'])?> р.</strong>
        </div>
        
        <ul  class="mm-char customScroll">
            <?=$product['short_des']?>
        </ul>
        <div class="stick"></div>
    </div>
</div>
