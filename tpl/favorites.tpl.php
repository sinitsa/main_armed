<!--RESIZE IMAGES -->

<script type="text/javascript">
//	if ( $(".image").height() > 195 ){
		$(function() {
			$( ".image" ).aeImageResize({ height: 195, width: 188 });
    });
//	}
</script>

	<!-- Main Container -->
	<div class="main-cont">
		<!-- Sidebar -->
		<?php include('block_left_menu.tpl.php'); ?>
		<!-- Content -->
		<div class="content">

			<div class="search-result-title">
				<h1><? if (!isset($favorites) || empty($favorites)) 
							echo "У вас еще нет избранных товаров.";
					   else 
							echo "Избранные товары: "; 							
					?>
				</h1>
			</div>
			<!--
			<div class="search-bar">

				<form action="">
					<div class="search-bar-what">
						<div class="input-tpl mid">
							<input type="text" value="">
						</div>
					</div>
					
					<div class="search-bar-where">
						<select id="search-bar-where">
							<option>Искать в товарах</option>
							<option>Общий по умолчанию</option>
						</select>
					</div>
					
					<div class="btn-tpl mid gray">
						<span>
							<a href="/">Искать</a>
						</span>
					</div>
				</form>

			</div>
			<div class="clearfix"></div>
			-->
			<? if(count($data['products']) > 0) {?>
			
			<div id="products-list-and-paginator">
				<!-- Catalogue Items -->
				<div class="catalogue-list">
					
					<? foreach ($data['products'] as $product) {
						include('element_product_list.tpl.php');
					} ?>
					
					<div class="clearfix"></div>
				</div>
				
			</div>
			<? } ?>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<div id="mdlcls" class="cat_modal-overlay"></div>
	</div>


<script type="text/javascript">
	// Countdowns
	$('#countdown_0').countdown({
		format: "s",
		startTime: '2:4:0',
		stepTime: 1,
		digitImages: 6,
		digitWidth: 33,
		digitHeight: 33,	
		image: "img/cd/countdown.png"
	});

	// Selectbox
	$('select').selectBox();
</script>

</html>

