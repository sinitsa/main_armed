<?php
if ($deliveryPrice === false ) {
	$deliveryText = 'от 300';
	$deliveryUnitShow = true;
	$total = 'false';
} elseif ($deliveryPrice > 0) {
	$deliveryText = $deliveryPrice;
	$deliveryUnitShow = true;
	$total = $priceSummWithGlobalDiscount + $deliveryPrice;
} else {
	$deliveryText = 'Бесплатно';
	$deliveryUnitShow = false;
	$total = $priceSummWithGlobalDiscount;
}

$baseDeliveryPrice = $deliveryText . ($deliveryUnitShow ? ' руб.' : '');	
?>
				<section class="mm-main">
				<div class="mm-main-holder">
					<div class="mm-title-holder">
						<h2>Корзина моих товаров</h2>
					</div>
					<form action="" class="mm-cart-form" id="order-form" method="POST">
						<fieldset>
							<table class="mm-cart-table mm-order-table">
								<thead>
									<tr>
										<td class="mm-name"><span>Наименование товара:</span></td>
										<td class="mm-price"><span>Цена:</span></td>
										<td class="mm-number"><span>Количество:</span></td>
										<td class="mm-sum"><span>Стоимость:</span></td>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<td colspan="2" class="mm-btn">&nbsp;</td>
										<td colspan="2" class="mm-total">
											<dl class="mm-total-list">
												<dt>Итого:</dt>
												<dd><?=moneyFormat($priceSummWithGlobalDiscount);?></dd>
												<dt>Стоимость доставки:</dt>
												<dd>Бесплатно</dd>
												<?php /* 
												<dt class="mm-sale" style="<?=($discountValue <= 0?'display:none;':'')?>">Скидка:</dt>
												<dd class="mm-sale" style="<?=($discountValue <= 0?'display:none;':'')?>">-<?php round($product['amount'] * $discountValue)?> руб.</dd>
												*/ ?>
											</dl>
										</td>
									</tr>
								</tfoot>
								<tbody>
									<?php foreach ($products as $product) { ?>
										<tr>
											<td class="mm-name">
												<a href="<?=getTemplateLink($product, 'catalog')?>" class="mm-title"><?=$product['title']?></a>
											</td>
											<td class="mm-price"><span><?=moneyFormat(round($product['price_after_discount']));?></span></td>
											<td class="mm-number">
												<input type="text" class="text" value="<?=$product['amount']?>" disabled="disabled"/>
											</td>
											<td class="mm-sum"><?=moneyFormat(round($product['price_after_discount'] * $product['amount']));?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>

							<div class="payment_type">
              <span class="payment_type_t">Способ оплаты:</span>
              <?php 
              $payment_types = getPaymentTypes(); 
              foreach ($payment_types as $p_type) { ?>
                  <label class="custom-radio"><input type="radio" name="payment_type" value="<?=$p_type['id']?>" <?=($p_type['id']==1)?'checked':''?>><?=$p_type['name']?> </label>                       
              <?php }?>
              </div>
				<div class="dev_type">
              <span class="dev_type_t">Способ получения:</span>
				<input type="text" name="dev_type" value="Доставка курьером" class="unsee">
                  <select>
					<option>Доставка курьером</option>
					<option>Пункт самовывоза</option>
				  </select>
				  <div class ="dev_city unsee">
				  <span class="dev_city_t">Выберите город:</span>
				  <input type="text" name="dev_city" value="" class="unsee">
				  <select>
				  <?
				  $sql_dev_city=mysql_query("SELECT id, title, adresses FROM deliveryPickups ORDER BY title ASC");
				  while($arr_dev_city=mysql_fetch_assoc($sql_dev_city)){
				 ?><option name="<?=$arr_dev_city['id']?>"><?=$arr_dev_city['title']?></option><?
				  }
				  ?>
				  </select>
				  </div>
				  <div class="dev_destination unsee">
				  <span class="dev_destination_t">Выберите пункт:</span>
				  <input type="text" name="dev_des" value="" class="unsee">
				  <?
				  $sql_dev_city=mysql_query("SELECT id, title, adresses FROM deliveryPickups ORDER BY title ASC");
				  while($arr_dev_city=mysql_fetch_assoc($sql_dev_city)){
				  $city_dest=explode("&",$arr_dev_city['adresses']);
				  ?>
				  <div class = "unsee" name="<?=$arr_dev_city['id']?>">
				  <span class="unsee"><?=$arr_dev_city['title']?></span>
				  <select>
				  <?
				  foreach($city_dest as $dest) {
				  ?>
				  <option><?=$dest?></option>
				  <?
				  }
				  ?>
				  </select>
				  </div>
				  <?}?>
				  </div>
					<div style="clear:both"></div>
              </div>
							<div class="mm-order-form">
								<div class="mm-row">
									<div class="mm-form-box">
										<strong class="mm-title">Контактная информация:</strong>
										<div class="mm-text-wrap">
											<div class="wrap"><input type="text" name="name" class="mm-text" placeholder="Ваше имя (ФИО):" /></div>
										</div>
										<div class="mm-text-wrap">
											<div class="wrap"><input type="text" name="phone" class="mm-text" placeholder="Контактный телефон:"/></div>
										</div>
										<div class="mm-text-wrap">
											<div class="wrap"><input type="text" name="email" class="mm-text" placeholder="E-mail:"/></div>
										</div>
									</div>
									<div class="mm-form-box">
										<strong class="mm-title">Данные для доставки:</strong>
										<div class="wrap">
											<select>
												<option>Россия</option>
											</select>
										</div>
										<div class="mm-text-wrap">
											<div class="wrap"><input type="text" class="mm-text" placeholder="Индекс:"/></div>
										</div>
										<div class="mm-text-wrap">
											<div class="wrap"><input type="text" name="adress" class="mm-text" placeholder="Адрес:"/></div>
										</div>
									</div>
									<div class="mm-form-box">
										<strong class="mm-title">Примечания к заказу:</strong>
										<div class="mm-textarea">
											<textarea cols="1" rows="1" name="extra_information" placeholder="Текст сообщения:" ></textarea>
										</div>
									</div>
								</div>
								<div class="btn-wrap">
									<input type="submit" class="mm-submit" id="accept-order-button" onclick = "yaCounter28334251.reachGoal('STEP2');_gaq.push(['_trackEvent', 'button', 'STEP2']);" value="Оформить заказ" />
									<a href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>" class="mm-back">Вернуться в корзину</a>
									<? /*
									<input type="checkbox" class="mm-checkbox" id="id50" style="margin-top: 14px" checked="checked" />
									<label for="id50">Я согласен с <a href="<?=getTemplateLink(array('chpu'=>'publichnaya-oferta'), 'page');?>">условиями 
									продажи</a></label>
									*/ ?>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<?php
					$__i = 0; 
					foreach($product['near'] as $product) {
						$__i++;
						if($__i > 4)  
							break;
					?>
					<div class="mm-content-box mm-content-box2">
						<div class="mm-title-holder">
							<h2>Похожие товары:</h2>
						</div>
						<div class="gallery-holder mm-slideshow2">
							<div class="gallery">
								<a class="btn-prev" href="#">&lt;</a>
								<a class="btn-next" href="#">&gt;</a>
								<div class="gmask">
									<div class="slideset">
										<div class="slide">
										<?
											$ne = 1;
											foreach ($product['near'] as $product) {
										?>
												<div class="mm-prod-box <?if($product['novinka']==1){print 'mm-new';}elseif($product['best']==1){print 'mm-hit';}?>">
													<div class="mm-holder">
														<div class="image">
															<a href="<?=getTemplateLink($product,'catalog')?>"><img src="<?=getImageWebPath('product_medium').$product['id']; ?>.jpg" alt="<?=$product['title']?>" width="188" height="169" /></a>
														</div>
														<div class="mm-price-holder">
															<div class="mm-popup-wrap">
																<a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>" class="mm-cart item-buy buy-it-listing" data-product_id='<?=$product['id']?>' data-price='<?=$product['price_after_discount']?>'>в корзину</a>
																<div class="mm-tooltip">
																	<span>Товар добавлен в корзину!</span>
																	<a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>">Оформить</a>
																</div>
															</div>
															<strong class="mm-price"><?=moneyFormat($product['price_after_discount'])?> руб.</strong>
														</div>
														<a href="<?=getTemplateLink($product,'catalog')?>" class="mm-title"><?=$product['title']?></a>
														<ul class="mm-char">
															<li>Массажный стол</li>
															<li>Основа - многослойная легкая фанера</li>
															<li class="mm-hide">Гидравлический подъем головной и ножной секций</li>
															<li class="mm-hide">Поверхность - высокачественный винил</li>
															<li class="mm-hide">Наполнение - плотный пенополиуретан тол. 7,5 см</li>
														</ul>
														<div class="stick"></div>
													</div>
												</div>
										<?
												$ne++;
												if ($ne>4){
													$ne=1;
													print '</div><div class="slide">';
												}
											}
										?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?}?>
			</section>