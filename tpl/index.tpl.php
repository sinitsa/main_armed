<?php 
// Костыль для скрытых товаров. Перенаправление на страницу 404, если товар скрыт.
if ($product['is_hide'] == 1) {
    $data = getStaticPage('404', true);
    $seo_title_text = $data['seo_title'];
    $seo_text = isset($data['seo_desc']) ? $data['seo_desc'] : '';
    $tab = 'page';
    header('HTTP/1.1 404 Not Found');
    header("Status: 404 Not Found");
}
# Костыль для вывода seo параметров, если находимся в разделе меню
if ($_GET['t'] == 'mcat') {
    $seo_title = $data['menuItem']['title'];
    $seo_des = $seo_title;
    $seo_key = $seo_title;
} elseif ($_GET['t'] == 'catalog') {
    $seo_des = $product['seo_des'];
}

// инфа для корзины 
$summPrice = isset($_COOKIE['total_amount']) && !empty($_COOKIE['total_amount']) ? htmlspecialchars($_COOKIE['total_amount']) : 0;
$summCount = 0;
foreach (explode('|', $_COOKIE['cart_products']) as $productData) {
    $temp = explode(':', $productData);
    $summCount += $temp[1];
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <title><?=$seo_title?></title>
    <meta name="description" content="<?php echo $seo_des;?>"/>
  <meta name="keywords" content="<?php echo $seo_key;?>"/>
    <link type="text/css" rel="stylesheet" href="/theme/css_new/icons.css">
    <link type="text/css" rel="stylesheet" href="/theme/css_new/star-rating.min.css">
	<link type="text/css" rel="stylesheet" href="/theme/css_new/jquery.mCustomScrollbar.css">
	<link type="text/css" rel="stylesheet" href="/theme/css_new/fancybox.css">
	<link type="text/css" rel="stylesheet" href="/theme/css_new/mm-ion.rangeSlider.css">
	<link type="text/css" rel="stylesheet" href="/theme/css_new/mm-ion.rangeSlider.skinFlat.css">
    <link type="text/css" rel="stylesheet" href="/theme/css_new/mm-jcf.css">
	<link type="text/css" rel="stylesheet" href="/theme/css_new/style.css?v=2.0">
    <link rel="shortcut icon" href="/favicon.ico"/>
	<script type="text/javascript" src="/theme/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="/theme/js_prod/libs.min.js"></script>
    <script type="text/javascript" src="/theme/js_prod/star-rating.min.js"></script>
	<script type="text/javascript" src="/theme/js/mm-jquery.lightboxCustom.js"></script>
	<script type="text/javascript" src="/theme/js/jquery.form.js"></script>
	<script type="text/javascript" src="/theme/js/jquery.fancybox.js"></script>
	<script type="text/javascript" src="/theme/js/mm-popups.js"></script>
	<script type="text/javascript" src="/theme/js/mm-input.js"></script>
	<script type="text/javascript" src="/theme/js/mm-with-drop.js"></script>
	<script type="text/javascript" src="/theme/js/mm-jquery.slideshow.js"></script>
	<script type="text/javascript" src="/theme/js/mm-ion.rangeSlider.min.js"></script>
	<script type="text/javascript" src="/theme/js/mm-ion.rangeSlider.js"></script>
	<script type="text/javascript" src="/theme/js/mm-jquery.carousel.js"></script>
	<script type="text/javascript" src="/theme/js/mm-jquery.openclose.js"></script>
	<script type="text/javascript" src="/theme/js/mm-jcf.select.js"></script>
	<script type="text/javascript" src="/theme/js/mm-form.js"></script>
	<script type="text/javascript" src="/theme/js/mm-cart-calc.js"></script>
	<script type="text/javascript" src="/theme/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="/theme/js/basket.js"></script>
	<script type="text/javascript" src="/theme/js/main.js"></script>
	
    <!--<script type="text/javascript" src="/theme/js_prod/main.min.js?20150402"></script>-->
    <!--[if lt IE 9]><script type="text/javascript" src="/theme/js/mm-html5.js"></script><![endif]-->
    <script src="https://regmarkets.ru/js/r17.js"; async type="text/javascript"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=ru'></script>
	
	

	
	
	
</head>
<body>
    <div class="mm-page clearfix">
        <div class="mm-container">
            <header class="mm-header">
                <div class="mm-top">
                    <ul class="mm-top-nav">
						<li><a href="<?=getTemplateLink(array('chpu'=>'about'),'page')?>">О нас</a></li>
                        <li><a href="<?=getTemplateLink(array('chpu'=>'punkty-samovyvoza'),'page')?>">Самовывоз</a></li>
                        <li><a href="<?=getTemplateLink(array('chpu'=>'dostavka-i-oplata'),'page')?>">Доставка и оплата</a></li>
                        <li><a href="<?=getTemplateLink(array('chpu'=>'optovikam'),'page')?>">Оптовикам</a></li>
                        <li><a href="<?=getTemplateLink(array('chpu'=>'kompensaciya-invalidam'),'page')?>">Компенсация инвалидам</a></li>
                        <li><a href="<?=getTemplateLink(array('chpu'=>'obmen-i-vozvrat-tovarov'),'page')?>">Возврат</a></li>
						<li><a href="<?=getTemplateLink(array('chpu'=>'contacts'),'page')?>" onclick="yaCounter28334251.reachGoal('CONTACTS');">Контакты</a></li>
                    </ul>   
					
                </div>
                <div class="mm-head-box">
                    <strong class="mm-logo"><a href="/"><img src="/theme/img_new/new_logo.png" alt="logo"><span class="offical">Официальный магазин</span></a></strong>
                    <!--
                    <div class="mm-geolocation">
                        <span>Ваш город:</span><a href="javascript:void(0);"><?//=$GLDataResult['city'];?></a>
                    </div>-->
                    <?php
                        $query = "SELECT * FROM config WHERE config.key = 'phone' LIMIT 1";
                        $row = mysql_query($query) or die(mysql_error());
                        $row = mysql_fetch_assoc($row);
                        $phones = $row['value'];
                        $phones = explode(",", $phones);
                    ?>
                    <?php
                        
                    ?>
                    <div class="header-phones">
                            <?php /*
                            if (Config::get('site.phone')) {
                                $part1 = substr(trim(Config::get('site.phone')),0,8);
                                $part2 = substr(trim(Config::get('site.phone')),8);
                                echo    '
                                            <span>' . $part1 . '</span>' . $part2 .
                                        '';
                            } */ ?>
							
							<?php 
								$numb = Config::get('site.phone') ;
								$numb2 = Config::get('site.phone_msk') ;
								$numb3 = Config::get('site.phone_spb') ;
								
								$phone = getStyledPhoneNumb($numb);
								$phone_msk = getStyledPhoneNumb($numb2);
								$phone_spb = getStyledPhoneNumb($numb3);
							?>
							
							<div class="mm-phone">
								<div><?=$phone['html']?></div>
								<div class="phone-text">Бесплатный по РФ</div>
							</div>
                        <div class="mm-phone">
                            <div><?=$phone_msk['html']?></div>
                            <div class="phone-text">в Москве</div>
                        </div>
                        <div class="mm-phone">
                            <div><?=$phone_spb['html']?></div>
                            <div class="phone-text">в Санкт-Петербурге</div>
                        </div>
						<div class="mm-phone w-hours"><div>с 9 до 22 </div>
						 <div class="phone-text">по Москве</div></div>	
                    </div>
					<?php /*
                    <div class="mm-contacts mm-box-type">
					  
                        <span class="mm-schedule"><span>Режим работы</span> 9:00–21:00 Мск</span></br>
                      
                        <!--<span class="mm-email">Е-mail: <span><a href="mailto:info@med-mos.ru">info@armed-market.ru</a></span></span>-->
                    </div>
					*/?>
					
                </div>
				<div class="mm-search-cart">
					<form class="mm-search-form icon-search">

							
								<input type="text" class="mm-text" placeholder="Поиск по каталогу">
								<input type="submit" class="mm-submit " id="testseach" value="">

								<script>
									$('#testseach').on('click', function() {
										var text;
										text = $('.mm-search-form .mm-text').val()
										location.href = "/search?q=" + text;
										//console.log(text);
										return false;
									});
								</script>
							
							<ul class="autocomplete"></ul>
					</form>
					<div class="mm-info-box">
                        <a href="#popup" class="mm-price-list mm-price-listbtn" onclick="yaCounter28334251.reachGoal('CALLBACK_BTN');_gaq.push(['_trackEvent', 'button', 'CALLBACK_BTN']);">Заказать звонок</a>
                    </div>
					<div class="mm-cart-wrap <?=$summCount>0?"active":"" ?> <?php if ($summCount == 0){print 'cart-empty';}?>">
						<a class="basket_link icon-cart <?=$summCount>0?"active":"" ?>" href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>" style="<?php //echo($summCount===0)?'color:inherit;':'';?>">	<span><?=$summCount?> <?//=($summPrice!==0)?' на '.$summPrice." рублей":"";?></span></a>
						</br>
						<?php /*
						<a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>" class="<?php echo($summCount===0)?'issue':'issue';?>">Оформить</a>
						*/ ?>
					</div>
				</div>
                
                <?php include ('block_left_menu.tpl.php');?>
            
            </header>
              
            <?
                define('TPL_DIR', dirname(__FILE__) . '/');
                switch ($tab) {
                    case 'main' :
                        $path = TPL_DIR . $tab . '.tpl.php';
                        if (file_exists($path) && !$tab_aksii) {
                            include($path);
                        }elseif($tab_aksii){
                            $path =  TPL_DIR.'novinki.tpl.php';
                            include($path);
                        }else {
                            include ('main.tpl.php');
                        }
                        break;

                    case 'mcat' :
                        include "maincat.tpl.php";
                        break;

                    default :
                        $path = TPL_DIR . $tab . '.tpl.php';
                        if (file_exists($path) && !$tab_aksii) {
                            include($path);
                        }elseif($tab_aksii){
                            $path =  TPL_DIR.'novinki.tpl.php';
                            include($path);
                        }else {
                            include ('main.tpl.php');
                        }
                        break;
                }
            ?>
           
				<div class="banner">
					<img src="/theme/img_new/banner_armed_fot.png" alt='Банер армед'>
				</div>
				<footer class="mm-footer">
					<div class="mini-ban">
						<div class="ban-inner">
							<div><span class="ban_icon"></span>Доставка до двери</div>
							<div><span class="ban_icon"></span>Подъем на этаж</div>
							<div><span class="ban_icon"></span>Помощь в сборке</div>
							<div><span class="ban_icon"></span>Бесплатная доставка*</div>
						</div>
					</div>
					<div class="mm-footer-holder">
						<div class="footer-nav">
							<!--<div class="footer-nav-title">Наш магазин</div>-->
							<ul class="mm-footer-nav">
									<li><a href="<?=getTemplateLink(array('chpu'=>'about'),'page')?>">О нас</a></li>
									<li><a href="<?=getTemplateLink(array('chpu'=>'punkty-samovyvoza'),'page')?>">Самовывоз</a></li>
									<li><a href="<?=getTemplateLink(array('chpu'=>'dostavka-i-oplata'),'page')?>">Доставка и оплата</a></li>
									<li><a href="<?=getTemplateLink(array('chpu'=>'optovikam'),'page')?>">Оптовикам</a></li>
									<li><a href="<?=getTemplateLink(array('chpu'=>'kompensaciya-invalidam'),'page')?>">Компенсация инвалидам</a></li>
									<li><a href="<?=getTemplateLink(array('chpu'=>'obmen-i-vozvrat-tovarov'),'page')?>">Возврат</a></li>
									<li><a href="<?=getTemplateLink(array('chpu'=>'contacts'),'page')?>" onclick="yaCounter28334251.reachGoal('CONTACTS');">Контакты</a></li>
							
							</ul>
						</div>
						<div class="yandex-market">
							<!--<div class="footer-nav-title">Мы на Яндекс Маркете</div>-->
							<a href="http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=1248/*http://market.yandex.ru/shop/278136/reviews/add">
								<img src="/theme/img_new/market.png" border="0" alt="Оцените качество магазина на Яндекс.Маркете." />
							</a>
						</div>
						
						<div class="payment-social">
							<div class="payment-info">
								<!--<div class="footer-nav-title">Мы принимаем</div>-->
								<img src="/theme/img_new/MasterCard_logo.png" alt="Карта MasterCard"><br>
								<img src="/theme/img_new/Visa_logo.png" alt="Карта Visa">
							</div>
							<?php /*
							<div class="social-info">
								<div class="footer-nav-title">Мы в соц сетях</div>
								<div class="social-icons">
									<a class="social-icon vk" href="#" ></a>
									<a class="social-icon fb" href="#" ></a>
								</div>
							</div>
							*/ ?>
						</div>
						
						<div class="footer-contacts">
							<!--<div class="footer-nav-title">Наши контакты</div>-->
							<ul class="mm-footer-nav">
								<?php /* <li><?=$phone['numb']?></li> */ ?>
								<li><?=$phone_msk['numb']?></li>
								<li><?=$phone_spb['numb']?></li>
								<li><a href="mailto:info@armed-market.ru" class="mail_fot">info@armed-market.ru</a></li>
							</ul>
						</div>
						
					</div>
				</footer>
        </div>
		
		
    </div>
    
	
    <div class="mm-modal" id="popup">
        <div class="">
            <h2>Заказать обратный звонок!</h2>
        </div>
       <form action="#" method="post">
            
                <div class="mm-text-wrap">
                    <!-- <label for="id106">ФИО, Компания:</label> -->
                    <input type="text" class="mm-text" name="name" id="id106" placeholder="ФИО, Компания:">
                </div>
                <div class="mm-text-wrap">
                    <!-- <label for="id105">E-mail адрес:</label> -->
                    <input type="text" name="phone" class="mm-text" id="id105" placeholder="Телефон:">
                </div>
                <input type="submit" class="mm-submit-price mm-submit-pricebtn" value="Отправить">
            
        </form>
        <span class="close"></span>
    </div>
    <div class="mm-modal" id="popup2">
        <div class="">
            <h2>Купить в 1 клик!</h2>
        </div>
        <form action="#" method='post' id='fast-order'>
           
                <div class="mm-info">
					<a href="" class="mm-title">Стол массажный стационарный FIX-0A</a>
                    <strong class="mm-price">19 440 руб.</strong>
                    
                </div>
				<div class="field_cont">
					<div class="mm-text-wrap">
						<input type="text" name='name' class="mm-text" id="id100" placeholder="ФИО, Компания:">
					</div>
					<div class="mm-text-wrap">
						<input type="text" name='email' class="mm-text" id="id101" placeholder="Email адрес:">
					</div>
					<div class="mm-text-wrap">
						<input type="text" name='phone' class="mm-text" id="id102" placeholder="Контактный телефон:">
					</div>
					<div class="after-send" style="display:none;"><span class="order-pn"></span><br/><span class="order-code"></span></div>
					<div class="mm-btns">
						<input type="button" id='fast-order-send-button' class="mm-submit but_default" value="Заказать" onclick = "yaCounter28334251.reachGoal('FAST_ORDER_FINAL');">
						<!--<input type="button" class="mm-cancel" value="Отменить">-->
					</div>
				</div>
            <input type="hidden" id='product_id' name="product_id" value="1905">
			 <p class="info_modal">Наш менеджер позвонит Вам на номер в течении нескольких часов (в рабочее время) для согласования времени доставки и уточнения деталей комплектации заказа. Пожалуйста не отключайте телефон!</p>
        </form>
       
        <span class="close"></span>
    </div>
    <div class="mm-modal" id="popup3" style="display:none;">
        <div class="">
            <h2>Подписка на акции</h2>
        </div>
        <p>Поздравляем, Вы успешно подписались на рассылку новых акций.</p>
        <span class="close"></span>
    </div>

<?php include('counters.php') ?>

</body>
</html>