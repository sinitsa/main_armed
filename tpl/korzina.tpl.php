
            <section class="mm-main slim_box">
                <div class="mm-main-holder">
                    <div class="">
                        <h1>Корзина моих товаров</h1>
                    </div>
                    <form action="#" class="mm-cart-form">
                      
                            <table class="mm-cart-table">
                               <!-- <thead>
                                    <tr>
                                        <td class="mm-name"><span>Наименование товара:</span></td>
                                        <td class="mm-price"><span>Цена:</span></td>
                                        <td class="mm-number"><span>Количество:</span></td>
                                        <td class="mm-sum"><span>Стоимость:</span></td>
                                    </tr>
                                </thead>-->
                             
                                <tbody id="cart-items">
                                    <?php foreach ($products as $product) { ?>
                                        <tr>
                                            <td >
                                               
                                                    <img src="<?=getImageWebPath('product_small').$product['id']?>.jpg" alt="<?=$product['title']?>" title="<?=$product['title']?>" />
                                           
                                              
                                            </td>
											<td class="mm-name">
												  <div class="mm-text-wrap">
                                                    <a href="<?=getTemplateLink($product, 'catalog')?>" class="mm-title"><?=$product['title']?></a><br><br>
                                                    <a href="javascript:void(0);" class="mm-delete cart-item-close">Удалить</a>
                                                </div>
											</td>
                                           <!-- <td class="mm-price"><span><?=moneyFormat(round($product['price_after_discount']));?></span></td>-->
                                            <input type="hidden" name="price" value="<?=$product['price_after_discount']?>">
                                            <input type="hidden" name="id" value="<?=$product['id']?>">
                                            <td class="mm-number">
                                                <a href="#" class="minus">-</a>
                                                <input type="text" class="text amount_input" value="<?=$product['amount']?>" name="amount"/>
                                                <a href="#" class="plus">+</a>
                                            </td>
                                            <td class="mm-sum"><?=moneyFormat(round($product['price_after_discount'] * $product['amount']));?> <span class="currency">руб. </span></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
								  <!--<tfoot>
                                        <tr>
                                            <td colspan="2" class="mm-btn">
                                                <input type="submit" class="mm-submit" onclick = "yaCounter28334251.reachGoal('STEP1'); location.href = '/pay/finish.html'; return false;" value="Оформить заказ" />
                                            </td>
                                            <td colspan="2" class="mm-total">
                                                <dl class="mm-total-list">
                                                   
                                                    
													<dd><?=moneyFormat($priceSummWithGlobalDiscount);?></dd>
													<dt>Доставка:</dt>
													<dd>Бесплатно</dd>
													 <dt>Итого:</dt>
                                                        <?=moneyFormat($priceSummWithGlobalDiscount);?>
														<? /*
                                                    <dt class="mm-sale" style="<?=($discountValue <= 0?'display:none;':'')?>">Скидка:</dt>
                                                    <dd class="mm-sale" style="<?=($discountValue <= 0?'display:none;':'')?>">- <span class="value"><?php round($product['amount'] * $discountValue)?></span> руб.</dd>
													*/ ?>
                                                </dl>
                                            </td>
                                        </tr>
                                </tfoot>-->
                            </table>
							<ul class="itog">
								<?php if ($priceSummWithGlobalDiscount > 0) {?>
								<li>Доставка <?php if ($priceSummWithGlobalDiscount > 2000) { ?>
									<span class="value_itog">Бесплатно</span> <?php
								} else { ?> 
									<span class="value_itog">300 руб.</span><?php }?>
								</li> <?php }?>
								<li>Итого <span class="mm-sum"><?=moneyFormat($priceSummWithGlobalDiscount);?></span> <span class="currency">руб.</span></li>
							</ul>
                        
                    </form>
                </div>
				<form action="" class="mm-cart-form" id="order-form" method="POST">
						<h1>Оформление</h1>
							
			<div class="container_finish">				
				<div class="column_finish">
					<div class="payment_type">
						 <!-- <span class="payment_type_t">Способ оплаты:</span>-->
						
						  <?php 
						  $payment_types = getPaymentTypes(); 
						  foreach ($payment_types as $p_type) { ?>
						<div class="cont_radio">	 
								  <input type="radio" name="payment_type" value="<?=$p_type['id']?>" <?=($p_type['id']==1)?'checked':''?> id="<?=$p_type['id']?>">  
							<label class="custom-radio" for="<?=$p_type['id']?>">	
								<?=$p_type['name']?>							
							</label>	
					
						</div>	
						
						  <?php }?>
						  
					</div>
					<div class="mm-order-form">
							<div class="mm-row">
									<div class="mm-form-box">
										<!-- <strong class="mm-title">Контактная информация:</strong>-->
										<div class="mm-text-wrap">
											<span class="mm-wrap-title">Введите ваше имя (ФИО) :</span>
											<div class="wrap"><input title="Имя должно составлять не меньше трех символов" type="text" name="name" class="mm-text"/></div>
										</div>
										<div class="mm-text-wrap">
											<span class="mm-wrap-title">Введите ваш телефон :</span>
											<div class="wrap"><input title="Телефон должен составлять не меньше 5 цифр" type="text" name="phone" class="mm-text"/></div>
										</div>
										<div class="mm-text-wrap">
											<span class="mm-wrap-title">Введите ваш E-mail :</span>
											<div class="wrap"><input title="Ваш настоящий E-mail позволит связаться с вами в случае если мобильная связь отсутствует" type="text" name="email" class="mm-text"/></div>
										</div>
										</div>
									<div class="mm-form-box">
										<!--<strong class="mm-title">Данные для доставки:</strong>
										<div class="wrap">
											<select>
												<option>Россия</option>
											</select>
										</div>-->
									
									</div>
									<div class="mm-form-box">
										<!--<strong class="mm-title">Примечания к заказу:</strong>-->
										<div class="mm-textarea">
											<textarea cols="1" rows="1" name="extra_information" placeholder="Текст сообщения:" ></textarea>
										</div>
									</div>
							</div>
							
					</div>
				</div>
				<div class="column_finish">
					<div class="dev_type">
					  <!--<span class="dev_type_t">Способ получения:</span>-->
						<div class="dev_type_radio">
							<input type="radio" name="dev_type" value="Доставка курьером" id="del_corier" checked>
							<label class="custom-radio" for="del_corier">	
							Доставка курьером
							</label>
							<input type="radio" name="dev_type" value="Пункт самовывоза" id="del_punkt">
							<label class="custom-radio" for="del_punkt">	
							Пункт самовывоза
							</label>
						</div>	
						  <!--<select>
							<option>Доставка курьером</option>
							<option>Пункт самовывоза</option>
						  </select>-->
						  <div class ="dev_city unsee">
							 <!-- <span class="dev_city_t">Выберите город:</span>-->
							  <input type="text" name="dev_city" value="" class="unsee">
							  <select>
							  <?
							  $sql_dev_city=mysql_query("SELECT id, title, adresses FROM deliveryPickups ORDER BY title ASC");
							  while($arr_dev_city=mysql_fetch_assoc($sql_dev_city)){
							 ?><option name="<?=$arr_dev_city['id']?>"><?=$arr_dev_city['title']?></option><?
							  }
							  ?>
							  </select>
						  </div>
						  <div class="dev_destination unsee">
							<!-- <span class="dev_destination_t">Выберите пункт:</span>-->
							<input type="text" name="dev_des" value="" class="unsee">
							  <?
							  $sql_dev_city=mysql_query("SELECT id, title, adresses FROM deliveryPickups ORDER BY title ASC");
							  while($arr_dev_city=mysql_fetch_assoc($sql_dev_city)){
							  $city_dest=explode("&",$arr_dev_city['adresses']);
							  ?>
							  <div class = "unsee" name="<?=$arr_dev_city['id']?>">
								  <span class="unsee"><?=$arr_dev_city['title']?></span>
								  <select>
								  <?
								  foreach($city_dest as $dest) {
								  ?>
								  <option><?=$dest?></option>
								  <?
								  }
								  ?>
								  </select>
							  </div>
							  <?}?>
						  </div>
							<div class="delivery_dest">
								<div class="mm-text-wrap">
											<div class="wrap"><input type="text" class="mm-text" placeholder="Индекс:"/></div>
										</div>
										<div class="mm-text-wrap">
											<div class="wrap"><input type="text" name="adress" class="mm-text" placeholder="Адрес:"/></div>
										</div>
							</div>			
						<div style="clear:both"></div>
					</div>
					<div class="btn-wrap">
									<input type="submit" class="mm-submit but_default" id="accept-order-button" onclick = "yaCounter28334251.reachGoal('STEP2');_gaq.push(['_trackEvent', 'button', 'STEP2']);" value="Оформить заказ" />
									<!--<a href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>" class="mm-back">Вернуться в корзину</a>-->
									<? /*
									<input type="checkbox" class="mm-checkbox" id="id50" style="margin-top: 14px" checked="checked" />
									<label for="id50">Я согласен с <a href="<?=getTemplateLink(array('chpu'=>'publichnaya-oferta'), 'page');?>">условиями 
									продажи</a></label>
									*/ ?>
							</div>
				</div>	
			</div>
						
						
					</form>
                <?if (count($data['recomm'])>0){?>
                    <div class="mm-content-box mm-content-box2 analog carusel_box" id="jsUpdate">
                        <div class=" analog-prod">
                            <h2>Рекомендуем с этим товаром:</h2>
                        </div>
                        <div class="gallery-holder mm-slideshow2 analog jsUpdate">
                            <div class="gallery">
                                <a class="btn-prev" href="#">&lt;</a>
                                <a class="btn-next" href="#">&gt;</a>
                                <div class="gmask">
                                    <div class="slideset">
                                        <div class="slide">
                                        <?
                                            $ne = 1;
                                            foreach ($data['recomm'] as $product) {
                                        ?>
                                                <div class="mm-prod-box <?if($product['novinka']==1){print 'mm-new';}elseif($product['best']==1){print 'mm-hit';}?>">
                                                    <div class="mm-holder">
                                                        <div class="image">
                                                            <a href="<?=getTemplateLink($product,'catalog')?>"><img src="<?=getImageWebPath('product_medium').$product['id']; ?>.jpg" alt="<?=$product['title']?>" width="188" height="169" /></a>
                                                        </div>
														<a href="<?=getTemplateLink($product,'catalog')?>" class="mm-title product_title"><?=$product['title']?></a>
                                                        <div class="mm-price-holder">
                                                            <div class="mm-popup-wrap">
<a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>" class="mm-cart item-buy icon-cart buy-it-listing" data-product_id='<?=$product['id']?>' data-product_title='<?=$product['title']?>' data-product_chpu='<?=$product['chpu']?>' data-product-price='<?=$product['price_after_discount']?>'></a>                                                              
																<!--<div class="mm-tooltip">
                                                                    <span>Товар добавлен в корзину!</span>
                                                                    <a href="<?=getTemplateLink(array('chpu'=>'korzina'),'cart')?>">Оформить</a>
                                                                </div>-->
                                                            </div>
                                                            <strong class="mm-price"><?=moneyFormat($product['price_after_discount'])?> р.</strong>
                                                        </div>
                                                        
                                                        <ul class="mm-char">
                                                        <?=$product['short_des']?>
                                                        </ul>
                                                        <div class="stick"></div>
                                                    </div>
                                                </div>
                                        <?
                                                $ne++;
                                                if ($ne>4){
                                                    $ne=1;
                                                    print '</div><div class="slide">';
                                                }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?}?>
</section>
