<link href="/css/filters.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	var price = {
		"min" : <?php echo $data['price_range']['min']?>,
		"max" : <?php echo $data['price_range']['max']?>
		};
	var defaultPage = <?php echo $_GET['page'] == 'all' ? "'all'" : 1; ?>;
	var defaultSort = '<?php echo isset($_GET['sort']) ? $_GET['sort'] : 'none'; ?>';
</script>
<script type="text/javascript" src="/js/filters.js" ></script>
<div id="filter-loading" style="display: none;"><img src="/img/ajax_loading_2.gif" alt="Загрузка"/></div>
<table width="92%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="200" align="left" valign="top">
			<?php include('block_basket_info.tpl.php'); ?>
			<?php include('block_left_menu.tpl.php'); ?>
			<div style="margin-top: 25px;"><?php include('banner_season_sale.tpl.php'); ?></div>
			<div style="margin-top: 10px;"><?php include('banner_best_and_new.tpl.php'); ?></div>
        </td>
        <td align="left" valign="top">
        
        
        <table width="220" border="0" align="right" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top">
    <div style="margin-left:15px;">
    <div class="txtzagpar">Подбор по параметрам </div>
	<form id="filter-form" action="/ajax/products.php?action=mcat" method="post">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		  <tr>
			<td class="txt_srazm">Цена:</td>
			<td class="txt_zaksm">от</td>
			<td><input name="min_price" type="text" class="prinput" id="amount1" value="<?php echo $data['price_range']['min']?>" /></td>
			<td class="txt_zaksm">до</td>
			<td><input name="max_price" type="text" class="prinput" id="amount2" value="<?php echo $data['price_range']['max']?>" /></td>
			</tr>
		</table> 
		<div style="margin-top:9px;">
			<div class="value-slider">
				<div id="slider-range"></div>
				<div class="min-price-filter"><?php echo $data['price_range']['min']?></div>
				<div class="max-price-filter"><?php echo $data['price_range']['max']?></div>
			</div>
		</div>
		<!-- -->
		<?php foreach ($data['params'] as $param) { ?>
		<div class="txt_srazm" style="margin-top:10px;"><?php echo $param['info']['title'];?>:</div>
		<div style="margin-top:8px;">
			<?php foreach($param['params'] as $value) { ?>
				<div class="filter-checkbox-wrapper">
					<label>
						<div class="filter-checkbox filter-checkbox-off">
							<input class="hidden-checkbox" type="checkbox" name="filter[<?php echo $value['id'];?>]" />
						</div>
						<div class="divrazmer">&nbsp;<?php echo $value['value']; ?></div>
					</label>
				</div>
			<?php } ?>
			<div style="clear: both;"></div>
		</div>
		<?php } ?>
		<!-- -->
		<div class="txt_srazm" style="margin-top:10px;">Категории:</div>
		<div style="margin-top:8px;">
			<input type="hidden" name="menu_item" value="<?php echo $cur_cat; ?>" />
			<?php foreach($data['cats'] as $cat) { ?>
				<div class="filter-checkbox-wrapper">
					<label>
						<div class="filter-checkbox filter-checkbox-off">
							<input class="hidden-checkbox" type="checkbox" name="filter[cat][<?php echo $cat['cat_id'];?>]" />
						</div>
						<div class="divrazmer">&nbsp;<?php echo $cat['title']; ?></div>
					</label>
				</div>
			<?php } ?>
			<div style="clear: both;"></div>
		</div>
		<div class="divsbros"><a href="?"><img src="/img/sbros.gif" width="102" height="22" border="0"></a></div>
	</form>
	<div style="text-align: center;"><?php include('banner_how_to_buy.tpl.php'); ?></div>
	<?php include('block_feedback.tpl.php'); ?>
      
      </div>
       </td>
  </tr>
</table>
<div style="margin-left:20px;"><h1><?php echo $data['menuItem']['title']; ?></h1>
	<?php if (count($data['cats']) > 0 ) { ?>
		<div style="margin-top:20px; margin-left:-12px; margin-bottom:-15px;">
		<?php foreach($data['cats'] as $cat) { ?>
			<div class="divpodkat"><a href="<?php echo getTemplateLink($cat, 'cat'); ?>"><div><img src="<? echo getImageWebPath('cats').$cat['cat_id']; ?>.jpg" /></div><?php echo $cat['title']; ?></a></div>
		<?php } ?>
		</div>
	<?php } ?>
<div style="margin-top:18px; margin-bottom:20px;" id="sort-panel"><span class="txt_sort"><em>Сортировка:</em></span> &nbsp;&nbsp;<a class="sort-asc" href="<?php echo $qString->setParam('sort', 'price_asc'); ?>"><em>сначала дешевые</em></a>&nbsp;&nbsp;&nbsp; <a class="sort-desc" href="<?php echo $qString->setParam('sort', 'price_desc'); ?>"><em>сначала дорогие</em></a>&nbsp;&nbsp;&nbsp; <a class="sort-news-sale" href="<?php echo $qString->setParam('sort', 'news_sale'); ?>"><em>новинки и скидки</em></a></div></div>
<div name="products-field" id="products-field">
	<?php foreach ($data['products'] as $product) {
		include('element_product_list.tpl.php');
	}
	?>

	<div style="margin-top:25px; margin-bottom:25px; margin-left:20px;">
	<?php generatePaginator($data['page']); ?>
	</div>
</div>

<div style="margin-top:25px;margin-left:20px;"> 
	<?php echo $seo_text; ?>
</div>
        
        </td>
      </tr>
    </table>