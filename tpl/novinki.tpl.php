<?php
// $query = "SELECT column_name FROM information_schema.columns WHERE table_name = 'catalog'";
$query = "SELECT * FROM catalog WHERE novinka = 1;";
$row = mysql_query($query) or die(mysql_error());
$temp_arr = array();
while ($wor = mysql_fetch_assoc($row)) {
	$temp_arr[] = $wor;
}
unset($data);
$data = array();
$data['price_range']['min'] = 100000000;
$data['price_range']['max'] = -1;
foreach ($temp_arr as $item) {
	if($data['price_range']['min'] > $item['price']){
		$data['price_range']['min'] = $item['price'];
	}
	if($data['price_range']['max'] < $item['price']){
		$data['price_range']['max'] = $item['price'];
	}
}
?>

<script type="text/javascript">
	var price = {
		"min" : <?php echo isset($data['price_range']['min']) ? $data['price_range']['min'] : 0 ?>,
		"max" : <?php echo isset($data['price_range']['max']) ? $data['price_range']['max'] : undefined ?>
		};
	var defaultPage = <?php echo (isset($_GET['page']) && $_GET['page'] == 'all') ? "'all'" : ((isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1); ?>;
	var defaultSort = '<?php echo isset($_GET['sort']) ? $_GET['sort'] : 'none'; ?>';

	$(function(){
		$("#range_1").ionRangeSlider({
            min: price.min,
            max: price.max,
            from: price.min,
            to: price.max,
            type: 'double',
            step: 1,
            postfix: "р.",
            prettify: false,
            hasGrid: false,
           	hideMinMax: true
        });
	});
</script>


			<div class="mm-intro-box mm-intro-type">
				<ul class="mm-breadcrumbs">
					<li><a href="/">Главная страница</a></li>
					<li>Новинки</li>
					<li><?=$data['catInfo']['title']?></li>
				</ul>
			</div>
			<section class="mm-main">
				<div class="mm-main-holder">
					<div class="mm-title-holder">
						<h2>Новинки:</h2>
					</div>
					<ul class="mm-sub-categories">
						<?
							foreach($data['tags'] as $tag){
						?>
							<li><a href="<?=getTemplateLink($tag,'tags')?>"><?=$tag['title']?></a></li>
						<?}?>
					</ul>
					<?php
					//action="/ajax/products.php?action=cat"
					?>
					<form method='post' action="/ajax/products.php?action=cat" class="mm-filter" id="filter-form">
						<input type="hidden" name="cat" value="<?=$data['catInfo']['chpu']?>">
						<input type="hidden" name="paginator_type" value="uniq">
						<fieldset>
							<div class="mm-row">
								<div class="mm-sort">
									<div class="mm-sort-title">
										<strong>Сортировка:</strong><span>&nbsp;</span>
									</div>
									<ul class="mm-sort-list">
										<li <?if ($_GET['sort']=='price_asc'){print'class="active"';}?>><a href="?sort=price_asc">По минимальной цене</a></li>
										<li <?if ($_GET['sort']=='price_desc'){print'class="active"';}?>><a href="?sort=price_desc">По максимальной цене</a></li>
									</ul>
								</div>
								<div class="mm-price-range">
									<div class="mm-sort-title">
										<strong>Диапазон Цен:</strong><span>&nbsp;</span>
									</div>
									<div class="mm-range">
										<label for="min">от:</label>
										<input type="text" class="mm-text default" id="min" name='min_price' value="<?=$data['price_range']['min']?> р." />
										<label for="max">до:</label>
										<input type="text" class="mm-text default" id="max" name='max_price' value="<?=$data['price_range']['max']?> р." />
										<input type="text" id="range_1" />
										<input type="hidden" value="1" name="novinki" />
									</div>
								</div>
							</div>
							<div class="mm-slide">
								<div class="mm-slide-holder">
									<div class="mm-column">
										<div class="mm-sort-title">
											<strong>Диапазон Цен:</strong><span>&nbsp;</span>
										</div>
										<select title="-- Не выбрано --">
											<option>100 (4)</option>
											<option>120 (3)</option>
											<option>140 (2)</option>
											<option>160 (1)</option>
										</select>
									</div>
									<div class="mm-column">
										<div class="mm-sort-title">
											<strong>Вес:</strong><span>&nbsp;</span>
										</div>
										<select title="-- Не выбрано --">
											<option>100 (4)</option>
											<option>120 (3)</option>
											<option>140 (2)</option>
											<option>160 (1)</option>
										</select>
									</div>
									<div class="mm-column">
										<div class="mm-sort-title">
											<strong>Нагрузка:</strong><span>&nbsp;</span>
										</div>
										<select title="-- Не выбрано --">
											<option>100 (4)</option>
											<option>120 (3)</option>
											<option>140 (2)</option>
											<option>160 (1)</option>
										</select>
									</div>
								</div>
							</div>
							<a href="#" class="mm-open">показать/скрыть</a>
						</fieldset>
					</form>
					
					<div id='products-list-and-paginator'>
						<ul class="mm-catalog">
							<?
								foreach ($temp_arr as $product) {
									include('element_product_list.tpl.php');
								}
							?>
						</ul>
						<div class="pagin">
							<?echo UniqGeneratePaginator($data['page'], false, false); ?>
						</div>
					</div>
					
				</div>
				<div class="mm-twocolumn">
					<?php include_once("right_block.tpl.php");?>
				</div>
			</section>

<script type="text/javascript">
	$('#sort-filter').bind('change', function() {
		var uri = $(this).find('option:checked').data('uri');
		window.location = uri;
	});

	// $(document).ready(function() {
		// SliderInit({
			// Slides:'.footer-more-item',
			// SlidesContainer:'#slides-container',
			// SliderLeft:'#slider-left',
			// SliderRight:'#slider-right',
			// SlidesOnScreen:3
		// });
	// });
	
	//
	//
	
	$('.filter-more-manufacturer input').bind('change', function(){
		reloadProducts();
	});
	
	$( document ).on( "click", ".filter-more-manufacturer span.checkbox", function() {
	  $(this).next('input').trigger('change');
	});
	// Sliders
	// $('#slider-price').slider({
		// range: true,
		// min: <?=$data['price_range']['min'];?>,
		// max: <?=$data['price_range']['max'];?>,
		// values: [ <?=$data['price_range']['min'];?>, <?=$data['price_range']['max'];?> ],
		// slide: function( event, ui ) {
			// $( '#price-range-from' ).val( ui.values[ 0 ] );
			// $( '#price-range-to' ).val( ui.values[ 1 ] );
		// },
		// change : function () {
			// //alert('asd');
			// reloadProducts();
		// }
	// });

	$('.filter-more-slider').each(function(index, element) {
		var _this = $(element);
		var sliderBody = _this.find('.slider-body');
		
		var dataField = _this.find('.slider-field');
		var minValue = dataField.data('min');
		var maxValue = dataField.data('max');
		var paramId = dataField.data('param_id');
		var unit = dataField.data('unit');
		
		// sliderBody.slider({
			// range: true,
			// min: minValue,
			// max: maxValue,
			// step: 1,
			// values: [minValue, maxValue],
			// slide : function(event, ui){
				// sliderBody.find(".fms3").text(ui.values[0] + ' ' + unit);
				// sliderBody.find(".fms4").text(ui.values[1] + ' ' + unit);
			// },
			// change : function(event, ui) {
				// dataField.empty();
				// dataField.append(
					// $('<input>').attr({
						// "type" : "hidden",
						// "name" : "filter_range[" + paramId + "][min]",
						// "value" : ui.values[0]
					// })
				// );
				// dataField.append(
					// $('<input>').attr({
						// "type" : "hidden",
						// "name" : "filter_range[" + paramId + "][max]",
						// "value" : ui.values[1]
					// })
				// );
				// reloadProducts();
				
			// }
		// });
		
		sliderBody.find('.ui-slider-range').html('<div class="fms-weight"><span class="fms3">' + minValue +' ' + unit + '</span><span class="fms4">' +maxValue  +' ' + unit + '</span></div>');
		handle = sliderBody.find('A.ui-slider-handle'); 
		handle.eq(0).addClass('first-handle');   
		handle.eq(1).addClass('second-handle');   
		handle.eq(2).addClass('first-handle');
		handle.eq(3).addClass('second-handle');
		
		
		//sliderBody.find('.ui-slider-range').append('<input type="text" class="weight-min" disabled /><input type="text" class="weight-max" disabled />');
		// sliderBody.find('.weight-min').val(sliderBody.slider('values', 0));
		// sliderBody.find('.weight-max').val(sliderBody.slider('values', 1));
		
	});
	//
	
	
	// $( '#price-range-from' ).val( $( '#slider-price' ).slider( 'values', 0 ) );
	// $( '#price-range-to' ).val( $( '#slider-price' ).slider( 'values', 1 ) );
	$('#price-range-from').change(function(event) {
		var data = $('#price-range-from').val();
		// $('#slider-price').slider('values', 0, data);
	});

	$('#price-range-to').change(function(event) {
		var data = $('#price-range-to').val();
		// $('#slider-price').('values', 1, data);
		// console.log(data);
	});

	handle = $('#slider-price A.ui-slider-handle, #slider-weight A.ui-slider-handle'); 
	handle.eq(0).addClass('first-handle');   
	handle.eq(1).addClass('second-handle');   
	handle.eq(2).addClass('first-handle');
	handle.eq(3).addClass('second-handle');

	
	// default
	// $('select').selectBox();
	
</script>