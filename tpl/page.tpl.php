<div class=" mm-intro-type">
				<ul itemscope itemtype="http://schema.org/BreadcrumbList" class="mm-breadcrumbs">
					<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
						<a  itemprop="item" href="/">
							<span itemprop="name">Главная страница</span>
						</a>
					</li>
					<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">
						<span itemprop="item" ><?php echo $data['title']; ?></span>
					</li>
				</ul>
			</div>
			<div style="padding-bottom:133px;" class="static_page">
	<?php

	 // error_reporting(-1) ; // включить все виды ошибок, включая  E_STRICT
	 // ini_set('display_errors', 'On');  // вывести на экран помимо логов

	// Вывод пунктов самовывоза с картами Yandex
	if (preg_match("/punkty-samovyvoza/i", $content)) {
		$output = '<h1> Пункты самовывоза:</h1>';

		
		empty($region) ? $title_region = '<span>Выберите город</span>' : $title_region = '<a href="punkty-samovyvoza.html">Выберите город</a>';

		if (empty($region)) {
			$output .= 	'<div class="delivery_block1">
							<div><span>Выберите город:</span></div>
							<div>
								<ul>';

			foreach($arrPoints as $row){
				$output .= 	'<li>
								<a href="punkty-samovyvoza-' . translit_new($row['title']) . '.html" >' 
									. $row['title'] . 
								'</a>
							</li>';
			}	

			$output .= '	</ul>
						</div>
					</div>
					<div class="map_block">
						<div id="map_area" data-map_center="55.748523,37.650156" data-map_zoom="10" data-map_adresses=""></div>
					</div>
					<div style="clear: both"></div>';			

		} else {
			

			$center = '';
			$zoom = '';
			$saveAdresses = '';
			foreach($arrPoints as $row){
				if (translit_new($row['title']) == $region) {
					$center = $row['center'];
					$zoom = $row['zoom'];
					$saveAdresses = $row['adresses'];
					$adresses = explode('&', $row['adresses']);	
					$output .= 	'<div class="delivery_block1">
									<div><a href="punkty-samovyvoza.html">' . $row['title'] . '</a></div>
									<div>
										<div itemscope itemtype="http://schema.org/Organization">
											<div style="display: none">
												<span itemprop="name">' . Config::get('site.name') . '</span>
												<span itemprop="telephone">' . Config::get('site.phone') . '</span>
											</div>';

					foreach ($adresses as $value) {
						preg_match('/\((.+)\)/', $value, $code);
						$arr = explode(',' , $value);
						if ($code) {
							array_pop($arr);
						}
						$output .= 	'<p class="adress" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
						$city = array_shift($arr);
						// $code = array_pop($arr);
						
						
						// echo $code[0] . "<br/";
						$output .= '<span itemprop="addressLocality">' . $city . ' </span>, ';
						$output .= '<span itemprop="streetAddress">' . implode(',', $arr) . ' </span> <br/>';
						$output .= '<span itemprop="postalCode">' . $code[1] . ' </span>';
						$output .= 	'</p>';

					}				
					break;
				}
			}
			$output .= '	</div>
						</div>
					</div>
					<div class="map_block">
						<div id="map_area" data-map_center=' . $center . ' data-map_zoom=' . $zoom. ' data-map_adresses="' . $saveAdresses . '" ></div>
					</div>
					<div style="clear: both"></div>';

		}


		

		echo $output;
	?>
	
	<style type="text/css">
		
		.delivery_block1 {
			width: 315px;
			height: 405px;
			float: left;
			position: relative;
		}
		.delivery_block1 > div:nth-child(1) {
			background: #CA2032;
			height: 45px;
			width: 315px;
			top: -45px;
			left: -2px;
			overflow: hidden;
		}
		.delivery_block1 > div:nth-child(2) {
			/*overflow-y: scroll;*/
			height: 357px;
			width: auto;
			border: 2px solid #CA2032;
			border-top: none;
		}
		.delivery_block1 > div:nth-child(2) > div, 
		.delivery_block1 ul {
			padding: 0px 10px;
			display: block;
			height: 345px;
			overflow-y: scroll;
			margin: 0px;
		}
		.delivery_block1 ul > li {
			display: block;
			height: 35px;
			border-bottom: 1px solid #EFEFEF;
			margin: 0px;
		}
		.delivery_block1 ul > li:last-child {
			border-bottom: none;
		}
		.delivery_block1 ul > li > a {
			display: block;
			padding: 5px 10px;
			font-size: 22px;
			line-height: 25px;
			font-weight: bold;
			color: #4D2833;
			text-decoration: none;
		}
		.delivery_block1 > div > span,
		.delivery_block1 > div > a {
			display: block;
			height: 45px;
			line-height: 45px;
			color: #ffffff;
			font-size: 22px;
			font-weight: bold;
			padding: 0px 15px;
			background: #CA2032;
			text-decoration: none;
		}
		.map_block {
			width: 670px;
			height: 400px;
			border: 2px solid #CA2032;
			float: right;
			overflow: hidden;
		}
		#map_area {
			width: 670px;
			height: 400px;
		}

		.adress > span:last-of-type {
			font-size: 0.8em;
			color: gray;
		}

	</style>

	<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<script type="text/javascript">

		ymaps.ready(init);

		function init() {



			var mapCenter 	= $('#map_area').data('map_center').split(','),
				mapZoom  	= $('#map_area').data('map_zoom'),
				mapAdresses = $('#map_area').data('map_adresses').split('&');

			var myCollection = new ymaps.GeoObjectCollection();

		    var myMap = new ymaps.Map('map_area', {
		        center: mapCenter,
		        zoom: mapZoom,
		        controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
		    });

		    myCollection.removeAll();

		    for (var i = mapAdresses.length - 1; i >= 0; i--) {
		    	
			    // Поиск координат центра Нижнего Новгорода.
			    ymaps.geocode(mapAdresses[i], {
			        /**
			         * Опции запроса
			         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
			         */
			        // Сортировка результатов от центра окна карты.
			        // boundedBy: myMap.getBounds(),
			        // strictBounds: true,
			        // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
			        // Если нужен только один результат, экономим трафик пользователей.
			        results: 1
			    }).then(function (res) {
		            // Выбираем первый результат геокодирования.
		            var firstGeoObject = res.geoObjects.get(0),
		                // Координаты геообъекта.
		                coords = firstGeoObject.geometry.getCoordinates(),
		                // Область видимости геообъекта.
		                balloonText = firstGeoObject.properties.get('balloonContent');

		                console.log(balloonText); 

		            // Добавляем первый найденный геообъект на карту.
		            // myMap.geoObjects.add(firstGeoObject);
		            // // Масштабируем карту на область видимости геообъекта.
		            // myMap.setBounds(bounds, {
		            //     // Проверяем наличие тайлов на данном масштабе.
		            //     checkZoomRange: true
		            // });

		            /**
		             * Если нужно добавить по найденным геокодером координатам метку со своими стилями и контентом балуна, создаем новую метку по координатам найденной и добавляем ее на карту вместо найденной.
		             */

		            myCollection.add(new ymaps.Placemark(coords,{
							balloonContent: balloonText
						},{
							hideIconOnBalloonOpen: true,
							preset: 'twirl#Icon',
	                        iconColor: 'gray'
						}
					));    
	             
		        });


		    };

		    myMap.geoObjects.add(myCollection);
		    
		}

	</script>

	<?php } else {
		echo $data['des'];
	} ?>
	
	<?php  if ($data['title'] == 'Оптовикам') { ?>
<form action="" class="mm-question-form" method="post">
		<div class="mm-row whole-sale">
			<div class="mm-text-wrap" style="width:265px;">
				<input class="mm-text" name="name" placeholder="Ваше имя:" required="" style="width:279px;" type="text" /></div>
			<div class="mm-text-wrap" style="width:265px;">
				<input class="mm-text" name="email" placeholder="E-mail адрес:" required="" style="width:279px;" type="text" /></div>
			<div class="mm-text-wrap" style="width:265px;">
				<input class="mm-text" name="phone" placeholder="Ваш телефон:" style="width:279px;" type="text" /></div>
		</div>
		<div class="mm-row">
			<div class="mm-text-wrap" style="width:424px">
				<input class="mm-text" name="company" placeholder="Ваша организация:" required="" style="width:438px;" type="text" /></div>
			<div class="mm-text-wrap" style="width:424px">
				<input class="mm-text" name="vol" placeholder="Примерные объемы:" style="width: 438px;" type="text" /></div>
		</div>
		<div class="mm-row">
			<span style="float:left;padding-right:10px;">Оплата: </span> <input checked="checked" id="pay_1" name="type" type="radio" value="1" /><label for="pay_1">Наличными</label> <input id="pay_2" name="type" type="radio" value="1" /><label for="pay_2">Безналичными</label></div>
		<br />
		<div class="mm-row">
			<div class="mm-textarea">
				<textarea cols="1" name="comment" placeholder="Текст сообщения:" rows="1"></textarea></div>
		</div>
		<input class="mm-submit one-click  but_default" name="submit" type="submit" >
	</form>
<script>
$(document).ready(function() {
    $(".mm-question-form").submit(function(){
        var form = $(this);
        var data = form.serialize();
        $.ajax({
            type: 'POST',
            url: '/ajax/deals.php?method=addDeals',
            dataType: 'json',
            data: data,
            success: function(data){
                console.log(data);
                /*
                $('#modal-answer').arcticmodal();
                $('#modal-answer span').text(form.find('input[name=name]').val());
              */
                alert("Ваша заявка отправлена! В скором времени с вами свяжется наш менеджер");
                form.trigger('reset');
            },
            complete: function(data) {
                form.find('input, textarea').each( function(){
                    $(this).val() == '';
                });
            }
        });
        return false;
    });
});
</script>
<?php }  ?>
</div>



<!-- Отправка формы со страницы "Оптовикам" -->
<div style="display: none;">
    <div class="box-modal" id="modal-answer" style="background: #FFF;padding: 30px;border-radius: 10px;text-align:center;">
        Здравствуйте, <span>test</span><br/>
        Ваша заявка принята!<br/>
        Она будет обработана в ближайшее время!
    </div>
</div>

