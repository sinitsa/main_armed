<aside class="mm-aside">
	<div class="mm-news-box">
		<div class="mm-title-box"><h2>Новости:</h2></div>
		<ul class="mm-news-list">
			<?php foreach ($arr_news as $news) {?>
			<li>
				<div>
					<span class="mm-date"><?=date("d.m.Y", $news['time'])?></span>
					<a href="/news/novosti.html?news=<?=$news['id']?>" class="mm-title"><?=$news['title']?></a>
				</div>
			</li>
			<?php }?>
		</ul>
		<a href="/news/novosti.html" class="mm-all">Все новости  »</a>
	</div>
	<form class="mm-contact-form" onsubmit="return false;">
		<fieldset>
			<span class="mm-title">Подписка на акции:</span>
			<?php if(empty($_COOKIE['acsii'])){?>
			<div class="mm-row">
				<input type="text" class="mm-text" name="email" value="Введите ваш email:" />
				<input type="submit" href="#popup3" class="mm-submit acsii" value="ок" />
			</div>
			<?php }else{?>
				<div style='text-align: center;'>Вы уже подписаны.</div>
			<?php } ?>
		</fieldset>
	</form>
</aside>