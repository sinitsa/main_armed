<?PHP

// ��������� ����� ��� �����
include('connect.php');
mysql_query('SET NAMES utf8');

class SiteMap {
		
		const http_root = 'http://armed-market.ru';
		// ��� ����� ��-��������� (��� ��� ����� �������� ��� ���������...[���^n]������� - ������ ��������� �����)
		var $filename;
		// ���� � �����
		var $path;
		// ���� ��� �������� DOM
		var $document;
		// �������� ���� URLSET
		var $urlset;

		// ��� ����� ��� ���, ������� ����������� ������... � ���� �� ���
		function __construct() {
				// ����... ��������� ���� ����� ������ � ������
				$this->filename = "sitemap.xml";
				$this->path = $_SERVER['DOCUMENT_ROOT'];

				// ���� ������� DOM ������ XML
				$this->document = new DomDocument('1.0');

				// ������� ��� URLSET � ������� � ����
				$this->urlset = $this->document->createElement("urlset");
				$attr = $this->document->createAttribute("xmlns");
				$attr->value = "http://www.sitemaps.org/schemas/sitemap/0.9";
				$this->urlset->appendChild($attr);

				// ��������� �������� ������
				$this->addURL(self::http_root);
				$this->addURL(self::http_root.'/articles/');
				$this->addURL(self::http_root.'/feedback/');
				//$this->addURL(self::http_root.'/catalog/index.html');

				// ��������� ������������ ������
				$this->addPages();
				$this->addCatRoot();
				$this->addCatalog();

				// ��������� �������� ����
				$this->document->appendChild($this->urlset);
				$this->document->save($this->path . "/" . $this->filename);
		}

		// ��������� ���� � URLSET
		function addURL($_url_text) {
				// ������� ���� URL
				$url = $this->document->createElement("url");

				// Cjplftv eptk LOC
				$loc = $this->document->createElement("loc");
				// ��������� ����� � LOC
				$loc->appendChild($this->document->createtextNode($_url_text));
				// ������� LOC � URL
				$url->appendChild($loc);

				// ������� URL � URLSET
				$this->urlset->appendChild($url);
		}

		// ��������� ������
		function addPages() {
				$query = "SELECT * FROM pages";
				$result = mysql_query($query) or die(mysql_error());
				while ($line = mysql_fetch_assoc($result)) {
						$furl = $line['chpu'];
						if (($furl != 'main') && ($furl != 'otzyv') && ($furl != 'mail') && ($furl != '404') && ($furl != 'fast') && ($furl != 'spam-filtr')) {
								if ($furl != '')
										$this->addURL(self::http_root . '/page/' . $line['chpu'] . '.html');
						}
				}
		}

		// ������� ������� CAT
		function addCatRoot() {
				$query = "SELECT * FROM cat WHERE 1";
				$result = mysql_query($query) or die(mysql_error());
				while ($line = mysql_fetch_assoc($result)) {
						if ($line['chpu'] != '')
								$this->addURL(self::http_root . '/cat/' . $line['chpu'] . '.html');
				}
		}

		// ������� CATALOG
		function addCatalog() {
				$query = "SELECT * FROM catalog";
				$result = mysql_query($query) or die(mysql_error());
				while ($line = mysql_fetch_assoc($result)) {
						if ($line['chpu'] != '')
								$this->addURL(self::http_root . '/catalog/' . $line['chpu'] . '.html');
				}
		}
		
		// ������� CATALOG
		function addArticles() {
				$query = "SELECT * FROM articles";
				$result = mysql_query($query) or die(mysql_error());
				while ($line = mysql_fetch_assoc($result)) {
						if ($line['chpu'] != '')
								$this->addURL(self::http_root . '/articles/' . $line['chpu'] . '.html');
				}
		}

}

// ������ 
$sm = new SiteMap();
echo $_SERVER['DOCUMENT_ROOT'];
?>